# Welcome #

This is the repository for **STS Integrated Courier System** project.

## Development Guidelines ##
* Follow the [PHP Coding Standards](https://docs.google.com/a/softwareseni.com/document/d/1kXOzKHRJgw2Ls0R7El5iejb4IduYjEyW-fuLbxwn9_s/edit?usp=sharing)
* Use proper git branching (one branch per one task):
    * *feature-xxx* for new feature
    * *hotfix-xxx* for bugfixing

## Team ##
* @hendri_v1 (PM)
* Ginanjar
* @ss_haryadi (PHP)