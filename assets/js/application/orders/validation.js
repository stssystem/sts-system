function form_validation() {

    var validation = true;

    $('.package-error').remove();
    $('.has-error').removeClass('has-error');
    global_message = '<ul>';

    if (params['branch_id_origin'] == '' || params['branch_id_origin'] == 0 || typeof params['branch_id_origin'] == 'undefined') {
        validation = set_message($('select[name="branch_id_origin"]'), 'Cabang asal pengiriman belum dipilih');
    }
    if (params['branch_id_destination'] == '' || params['branch_id_destination'] == 0 || typeof params['branch_id_destination'] == 'undefined') {
        validation = set_message($('select[name="branch_id_destination"]'), 'Cabang tujuan pengiriman belum dipilih');
    }
    if (params['package_weight'] == '' || params['package_weight'] == 0 || typeof params['package_height'] == 'undefined') {
        validation = set_message($('input[name="package_weight"]'), 'Berat paket belum diisi');
    }
    if (params['det_order_qty'] == '' || params['det_order_qty'] == 0 || typeof params['package_height'] == 'undefined') {
        validation = set_message($('input[name="det_order_qty"]'), 'Jumlah paket belum diisi');
    }

    if (params['package_title'] == '' || params['package_title'] == 0 || typeof params['package_title'] == 'undefined') {
        validation = set_message($('input[name="package_title"]'), 'Nama paket belum diisi');
    }

    if (params['package_type'] == '' || params['package_type'] == 0 || typeof params['package_type'] == 'undefined') {
        validation = set_message($('select[name="package_type"]'), 'Tipe paket belum diisi');
    }

//    if (params['package_content'] == '' || params['package_content'] == 0 || typeof params['package_content'] == 'undefined') {
//        validation = set_message($('textarea[name="package_content"]'), 'Isi paket belum diisi');
//    }

    if (validation == false) {
        $(".package-validation-trigger").trigger("click");
        $('#modal-package-validation').on('shown.bs.modal', function (e) {
            $(this).find('.modal-body p').empty();
            $(this).find('.modal-body p').append(global_message + '</ul>');
        });
    }

    return validation;

}

function set_message(tag, message, parent) {
    global_message += '<li>' + message + '</li>';

    if (typeof parent == 'undefined') {
        $(tag).parent().parent().addClass('has-error');
    } else {
        $(tag).parent().addClass('has-error');
    }

    $(tag).after('<span id="helpBlock2" class="help-block package-error">' + message + '</span>');
    return false;
}