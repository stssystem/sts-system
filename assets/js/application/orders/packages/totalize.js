/**
 * All function related about package operation 
 * describe on here
 */

/**
 * Totalize package weight
 */
function sum_package_weight() {

    /** Collect data from form */
    var t_pweight = $('input[name="package_weight"]').val();
    var t_oqty = $('input[name="det_order_qty"]').val();

    /** Sum and insert into form when not empty find requirement forms */
    if (t_pweight != '' && t_oqty != '') {
        var sum = '(' + t_pweight + '*' + t_oqty + ')';
        sum = math.eval(sum);
        $('input[name="package_weight_total"]').val(sum);
    }

}

/** Show modal package size field */
function modal_package_size() {
    $('#modal-package-size').modal();
}

function sum_package_size() {

    var length = $('input[name="tpackage_lenght"]').val();
    var width = $('input[name="tpackage_width"]').val();
    var height = $('input[name="tpackage_height"]').val();
    
    var sum = '(' + length + '*' + width + '*' + height + ')';
    sum = math.eval(sum);
    
    var qty = 1;
    if ($('input[name="det_order_qty"]').val() != '') {
        qty = $('input[name="det_order_qty"]').val();
    }
    sum = math.eval(sum + '*' + qty);

    var unit = $('select[name="tunit"]').val();
    
    $('input[name="package_size"]').val(math.eval(sum + ' / 1000000'));

}

$('.total-package-size').on('keyup change', function () {
    sum_package_size();
});

function total_size() {

    if ($('input[name="package_size"]').val() != '' && $('input[name="det_order_qty"]').val() != '') {
        var sum = $('input[name="package_size"]').val();
        var qty = $('input[name="det_order_qty"]').val();

        sum = math.eval(sum + '*' + qty);

        $('input[name="package_size"]').val(sum);
    }

}

$('.btn-enter-volume').click(function () {
    sum_package_size();
    grap_price_params();
});

$('#modal-package-size').on('hidden.bs.modal', function (e) {
    $('input[name="package_size"]').focus();
});
