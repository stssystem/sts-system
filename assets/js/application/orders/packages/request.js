/**
 * All function related about package server request POST / GET 
 * describe on here
 */

/** Get price from server   */
function grap_price_params() {

    /** Get total package weight */
    sum_package_weight();

    /** Get collected data from form */
    collect_package_pre();

    /** Send price parameter into server and get price list */
    var result = JSON.stringify(params);
    $.post(url_order_helper_price,
            {price_params: result}, function (data) {
                
        var totals = parseFloat(data);
        $('input[name="total"]').val(data);
        $('.total-price').empty();
        $('.total-price').text(numeral(totals).format('0,0'));
        $('input[name="det_order_sell_total"]').val(totals);
        $('input[name="det_order_sell_total_temp"]').val(totals);
        
    });

}

function form_submit() {

    /** Collect data from form */
    collect_package_post();

    /** Image handle */
    var img = {};
    var index = 0;
    $('.input-image').each(function () {
        img[index] = $(this).val();
        index++;
    });
    params['package_pic'] = img;

    /** Validation */
    if (form_validation()) {

        /** Send data collected into server */
        var result = JSON.stringify(params);

        $.post(url_orders_store,
                {data: result}, function (data) {
            data_tables();
        });

        /** Reset form to clear condition */
        collect_reset();

    }

}

function form_update() {

    /** Origin and destination */
    collect_package_pre();
    collect_package_post();
    collect_image();

    if (form_validation()) {

        /** Send data collected into server */
        var result = JSON.stringify(params);

        $.post(url_update_package,
                {data: result}, function (data) {

            var button = '<button type="button" class="btn btn-info btn-sm btn-flat btn-submit btn-action btn-add-package"  data-toggle="tooltip" data-placement="top" title="Simpan" >'
                    + '<i class="fa fa-save"></i></button>';

            $('.btn-action').replaceWith(button);

            $('.btn-submit').click(function () {
                form_submit();
            });

            data_tables();

        });

        /** Reset form to clear condition */
        collect_reset();

    }

}

/** Delete extra order */
function delete_extra_order(obj) {
    var data = $(obj).parent().parent().find('input').val();
    var data = JSON.parse(data);

    $.get(url_destroy_order_extra + '/' + data['det_order_extra_id'],
            {data: null}, function (data) {
        order_extra_data_table();
    });
}

function get_geobranch(branch_id) {
    $.post(url_geo_branch + '/' + branch_id,
            {data: null}, function (data) {
        $('input[name="origin_geobranch"]').val(data);
    });
}

function get_destination_geobranch(branch_id) {
    $.post(url_geo_branch + '/' + branch_id,
            {data: null}, function (data) {
        $('input[name="destination_geobranch"]').val(data);
    });
}

function get_image_list(package_id) {
    $.post(url_image_list + '/' + package_id,
            {data: null}, function (data) {

        data = JSON.parse(data);

        $('.image-frame').empty();

        data.forEach(function (item) {
            var img = '<div class="col-md-6 img-list"><a class="thumbnail" id="photo_profile">'
                    + '<img src="' + item + '" style="width: 100%; height:150px;"><i class="fa fa-times img-delete"></i>'
                    + '<input name="image[]" class="input-image" type="hidden" value="' + item + '" ></a>'
                    + '</div>';

            $('.image-frame').prepend(img);

        });

    });
}


