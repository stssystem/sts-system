/**
 * All function related about collected data
 */

/** Collect package data form form */
function collect_package_pre() {
    params['customer_price'] = $('select[name="customer_price"] option:selected').val();
    params['customer_id'] = $('input[name="customer_id"]').val();
    params['branch_id_origin'] = $('select[name="branch_id_origin"] option:selected').val();
    params['branch_id_destination'] = $('select[name="branch_id_destination"] option:selected').val();
    params['package_width'] = $('input[name="tpackage_width"]').val();
    params['package_height'] = $('input[name="tpackage_height"]').val();
    params['package_lenght'] = $('input[name="tpackage_lenght"]').val();
    params['package_weight'] = $('input[name="package_weight"]').val();
    params['det_order_qty'] = $('input[name="det_order_qty"]').val();
    params['package_size'] = $('input[name="package_size"]').val();
    params['unit'] = $('select[name="tunit"]').val();

    if ($('input[name="package_id"]').val() != '') {
        params['package_id'] = $('input[name="package_id"]').val();
    }

}

function collect_package_post() {
    params['package_title'] = $('input[name="package_title"]').val();
    params['package_type'] = $('select[name="package_type"] option:selected').val();
    params['package_content'] = $('textarea[name="package_content"]').val();
    params['order_id'] = $('input[name="order_id"]').val();

    var ods = $(".order-sell-total").attr("name");
    if (ods == 'det_order_sell_total_temp') {
        params['det_order_sell_total'] = $('input[name="det_order_sell_total_temp"]').val();
    } else {
        params['det_order_sell_total'] = $('input[name="det_order_sell_total"]').val();
    }

    params['total'] = $('input[name="total"]').val();
    params['det_order_qty'] = $('input[name="det_order_qty"]').val();

}

function collect_reset() {
    $('input[name="package_id"]').val('');
    $('input[name="package_title"]').val('');
    $('textarea[name="package_content"]').val('');
    $('input[name="package_width"]').val('');
    $('input[name="package_height"]').val('');
    $('input[name="package_lenght"]').val('');
    $('input[name="package_weight"]').val('');
    $('select[name="package_type"]').val('');
    $('input[name="package_size"]').val('');
    $('input[name="det_order_sell_total"]').val('');
    $('input[name="det_order_qty"]').val(1);
    $('input[name="package_weight_total"]').val('');
    $('input[name="tpackage_lenght"]').val('');
    $('input[name="tpackage_width"]').val('');
    $('input[name="tpackage_height"]').val('');

    setTimeout(function () {
        $('input[name="det_order_sell_total_temp"]').attr('name', 'det_order_sell_total');
        $('input[name="det_order_sell_total"]').val('');
    }, 500);

    $('.total-price').text('0');
    $('.img-list').remove();

}

function collect_image() {
    var img = {};
    var index = 0;
    $('.input-image').each(function () {
        img[index] = $(this).val();
        index++;
    });
    params['package_pic'] = img;
}

function clear_origin() {
    $('textarea[name="order_origin_text"]').val('');
}

function clear_destination() {
    $('textarea[name="order_destination_text"]').val('');
}