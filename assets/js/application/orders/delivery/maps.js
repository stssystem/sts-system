/**
 * All  fucntion related about maps operation listed in here
 */

function initialize() {

    if (($('input[name="origin_geobranch"]').val() != '' && $('input[name="origin_geocustomer"]').val() != '') ||
            ($('input[name="destination_geobranch"]').val() != '' && $('input[name="destination_geocustomer"]').val() != '')) {

        var OriginLatLng = '';
        var CustomerLatLng = '';

        if ($('input[name="mode"]').val() == 'origin') {
            OriginLatLng = $('input[name="origin_geobranch"]').val();
            CustomerLatLng = $('input[name="origin_geocustomer"]').val();
        } else {
            OriginLatLng = $('input[name="destination_geobranch"]').val();
            CustomerLatLng = $('input[name="destination_geocustomer"]').val();
        }

        OriginLatLng = OriginLatLng.split(",");
        CustomerLatLng = CustomerLatLng.split(",");

        var origin = new google.maps.LatLng(parseFloat(OriginLatLng[0]), parseFloat(OriginLatLng[1]));
        var destination = new google.maps.LatLng(parseFloat(CustomerLatLng[0]), parseFloat(CustomerLatLng[1]));

        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
                {
                    origins: [origin],
                    destinations: [destination],
                    travelMode: google.maps.TravelMode.DRIVING
                }, callback);

    }

    function callback(response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK) {
            var origins = response.originAddresses;
            var destinations = response.destinationAddresses;

            for (var i = 0; i < origins.length; i++) {
                var results = response.rows[i].elements;
                for (var j = 0; j < results.length; j++) {
                    var element = results[j];
                    var distance = element.distance.text;
                    var duration = element.duration.text;
                    var mode = $('input[name="mode"]').val();
                    $('.' + mode + '-distance').empty();
                    $('.' + mode + '-distance').append(distance + ' - ' + duration);
                }
            }
        }
    }
}

function get_geocode(mode) {

    /** Collected data from form */
    var address = '';
    if ($('select[name="city_id_' + mode + '"] option:selected').val() != '') {
        var city = $('select[name="city_id_' + mode + '"] option:selected').text();
        city = city.trim();
        address += city;
    }
    if ($('select[name="districts_id_' + mode + '"] option:selected').val() != '') {
        var district = $('select[name="districts_id_' + mode + '"] option:selected').text();
        district = district.trim();
        address += ',' + district;
    }
    if ($('select[name="village_id_' + mode + '"] option:selected').val() != '') {
        var village = $('select[name="village_id_' + mode + '"] option:selected').text();
        village = village.trim();
        address += ',' + village;

    }
    if ($('textarea[name="order_' + mode + '_text"]').val() != '') {
        var street = $('textarea[name="order_' + mode + '_text"]').val();
        street = street.trim();
        address += ',' + street;
    }

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            var mode_ = $('input[name="mode"]').val();
            if (mode_ == 'origin') {
                $('input[name="origin_geocustomer"]').val(latitude + ',' + longitude);
            } else {
                $('input[name="destination_geocustomer"]').val(latitude + ',' + longitude);
            }
        }
    });
}

function geo_graps() {
    $('.origin_geobranch').find('select').on('change', function () {
        callback_geograps('origin');
    });
    $('.origin_geobranch').find('textarea').on('focusout', function () {
        setTimeout(function () {
            callback_geograps('origin');
        }, 100);
    });
}

function geo_destination_graps() {
    $('.destination_geobranch').find('select').on('change', function () {
        callback_geograps('destination');
    });
    $('.destination_geobranch').find('textarea').on('focusout', function () {
        setTimeout(function () {
            callback_geograps('destination');
        }, 100);
    });
}

function callback_geograps(mode) {

    $('.' + mode + '-distance').empty();
    $('.' + mode + '-distance').append('<span class="fa fa-spinner fa-spin"></span>');

    $('input[name="mode"]').val(mode);

    if (mode == 'origin') {
        get_geobranch($('select[name="branch_id_' + mode + '"]').val());
    } else {
        get_destination_geobranch($('select[name="branch_id_' + mode + '"]').val());
    }

    setTimeout(function () {
        get_geocode(mode);
        initialize();
    }, 100);

}


