<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public $config;

    public function __construct() {
        parent ::__construct();

        $this->load->model('finance_report_model');
        $this->load->model('regionals_model');
        $this->load->model('orders_model');
        $this->load->model('customers_transaction_model');
        $this->load->model('customers_transaction_detail_model');
        $this->load->model('armadas_model');
        $this->load->model('franco_model');
        $this->load->model('franco_list_model');
        $this->load->model('orders_temp_model');
        $this->load->model('packages_model');
        $this->load->model('prices_model');
        $this->load->model('report_tax_model');

        $this->load->helper('urlstore');
    }

    public function index() {
        $this->load->model('report_sale_branch_origin_model', 'd');
        $d = $this->d->get_sum(1)->result();
        print_r($d);
    }

    public function lol() {
        $this->load->model('report_sale_branch_destination_model', 'x');
        $d = $this->x->get_sum(1);
        print_r($d);
    }

    public function change_id() {
        ini_set('memory_limit', '1024M'); // or you could use 1G
        $query = $this->db->get('customers');
        $i = 492;
        foreach ($query->result() as $rows) {
            $this->db->set('customer_id', $i);
            $this->db->where('customer_id', $rows->customer_id);
            $this->db->update('customers');
            echo $rows->customer_id . ' => ' . $i . "<br />";
            $i++;
        }
    }

    public function send() {

        // Load all resource needed
        $this->config->load('email2');
        $this->load->library('email');

        $this->email->initialize($this->config->item('email'));

        $this->email->from('paketstsindonesia@gmail.com', 'noreply');
        $this->email->to('haryadi@softwareseni.com');
        $this->email->subject('Notification for Order Approval');
        $this->email->message('User requested to edit order');
        if ($this->email->send()) {
            echo "Mail Sent!";
            echo $this->email->print_debugger();
        } else {
            echo "There is error in sending mail!";
            echo $this->email->print_debugger();
        }
    }

    // Temporary Code : packages/Packages.php
    public function final_unloading_process() {
        /**
         * @var Array Collection of order id that purpose to group package per order id, 
         * then prevent redundant when inserting the last location of order 
         */
        $temp_collection = [];
        /**
         * @var String Result the function proccess, 'true' when function is success 
         * and 'false' when function is not success caused by not data available 
         */
        $result = '';
        /** @var Array List of package-id that get from user form. */
        $data = $this->input->post('data');
        /**
         * @var String Switch the type of loading, 'true' when unloading is just transit 
         * and 'false' when loading is finish. 
         */
        $unloading_type = $this->input->post('unloading_type');
        /** Update status when data is not empty */
        if (!empty($data)) {
            /** Break down array to update data */
            foreach ($data as $key => $item) {
                /** Data preparing */
                $temp['package_status'] = ($unloading_type == 'true') ? 6 : 5;
                $temp['package_id'] = $item['value'];
                /** Update package status */
                $this->packages_model->update($temp);
                /** Update last position */
                if (!in_array($this->packages_model->get_order_id($item['value']), $temp_collection)) {
                    orderLastPosition($this->packages_model->get_order_id($item['value']), 'Barang telah diturunkan');
                    $temp_collection[] = $this->packages_model->get_order_id($item['value']);
                }
            }
            /** Set return value with true when update status is success */
            $result = 'true';
        } else {
            /** Set return value with false when data is empty */
            $result = 'false';
        }
        /** Return the result of process */
        echo $result;
    }

    public function stores() {

        /** Initialization */
        $armada = $this->input->post('delivery_armada_id');
        $armada_text = $this->input->post('delivery_armada_text');
        $courier = $this->input->post('delivery_courier');
        $courier_text = $this->input->post('delivery_courier_text');
        $depart_time = $this->input->post('delivery_depart_time');
        $order = $this->input->post('order_id');
        $str_list_package = '';
        $tr_list = '';
        $str_warning = '&nbsp;<span class="label label-warning">'
                . '<i class="fa fa-warning"></i> '
                . 'Paket ini tidak ditemukan di daftar barang</span>';

        /** Check whether the package is still in warehouse */
        $package = $this->packages_model->get_joined([
                    'det_order.order_id' => $order,
                        #'packages.package_status' => 5 #Deactive, until condition stable
                ])->result();

        /** Update package status and insert into delivery table when package available */
        if (!empty($package)) {

            /**
             * Break down package available to deliver,
             * then insert into deliveries table and listed it to message purpose
             */
            if (!empty($package)) {
                foreach ($package as $key => $item) {

                    /** Listing packages that was success to delivering */
                    $tr_list .= "<tr>"
                            . "<td>$item->package_id</td>"
                            . "<td>$item->package_title </td >"
                            . "<td>$armada_text </td >"
                            . "<td>$courier_text </td >"
                            . "<td>$depart_time </td >"
                            . "<td>"
                            . ($item->package_status != 5 ? $str_warning : '')
                            . "</td>"
                            . "<td  class='actions' >"
                            . "<input type='hidden' name='package_id[]' value='{$item->package_id}' />"
                            . "<input type='hidden' name='armada_id[]' value='{$armada}' />"
                            . "<input type='hidden' name='courier_id[]' value='{$courier}' />"
                            . "<input type='hidden' name='depart_time[]' value='{$depart_time}' />"
                            . "<div class='btn-group btn-sm pull-right'>"
                            . "<button type='button' class='btn btn-danger btn-delete btn-sm'><i class='fa fa-times'></i></button>"
                            . "</div>"
                            . "</td>"
                            . "</tr>";
                }
            }

            echo $tr_list;
        } else {

            /** Return error message */
            echo 'false';
        }
    }

}
