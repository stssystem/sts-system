<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_web extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'customers_web';
    public $controller_path = 'customers/customers_web';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "1";
            $this->id = $this->input->post('customer_id');

            $this->load->model('customers_web_model');
            $this->load->model('users_meta_model');
            $this->load->library('datatable/dt_customers_web');
            $this->load->library('request/request_customers_web');
            
        } else {
            redirect('program/login');
        }
    }

    public function index() {
        $this->data['title'] = "Daftar Nama Pelanggan";
        if ($this->input->is_ajax_request()) {
            echo $this->dt_customers_web->get($this, $_REQUEST);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function edit($id = 0) {

        $this->data['title'] = "Edit Data Pelanggan";
        $this->data['account'] = $this->customers_web_model->get(['ID' => $id])->row_array();
        $this->data['profile'] = $this->users_meta_model->get_profile($id);
        
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        
    }
    
    public function update($id = null){
        
        if (!$this->request_customers_web->validation()) {
            redirect($this->controller_path . '/edit/' . $id, 'refresh');
        } else {

            /** Update Profile */
            $meta = array(
                'first_name' => $this->input->post('first_name'),
                'company' => $this->input->post('company'),                
                'phone' => $this->input->post('phone'),
                'address' => $this->input->post('address')
            );
            
            $this->users_meta_model->update_profile($id, $meta);
            
            /** Update Password */
            $new_pass = $this->input->post('new_password');
            $confirm_pass = $this->input->post('confirm_password');
            
            if(!$this->customers_web_model->update_password($id, $new_pass, $confirm_pass)){
                set_alert('alert-danger', 'Password baru dengan password konfirmasi tidak sama');
                redirect($this->controller_path . '/edit/' . $id, 'refresh');
            }
            
            set_alert('alert-success', "Data pelanggan <strong>".$this->input->post('full_name')."</strong> telah diperbarui");

            redirect($this->controller_path);
            
        }
        
    }

    public function destroy($id = null) {
        
        /** Delete user data */
        $data = $this->customers_web_model->destroy($id);
        
        /** Delete user meta data */
        $this->users_meta_model->destroy($id);
        
        set_alert('alert-danger', 'Data pelanggan dengan nama <strong>' . $data . '</strong> telah dihapus');
        
        redirect($this->controller_path);
        
    }
    
    public function reset(){
        $this->session->unset_userdata('dt_customer_web');
        redirect($this->controller_path);
    }
    
}
