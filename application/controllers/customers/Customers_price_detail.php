<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_price_detail extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'customers_price_detail';
    public $controller_path = 'customers/customers_price_detail';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_price_detail";
            $this->id = $this->input->post('price_id');

            $this->load->model('customers_price_model');
            $this->load->model('costumers_model');
            $this->load->model('cities_model');
            $this->load->model('branches_model');

            $this->load->library('request/request_customers_price_detail');
            $this->load->library('datatable/dt_customers_price_detail');
        } else {
            redirect('program/login');
        }
    }

    public function index($customer_id = null, $origin = null, $destination = null) {

        $this->data['title'] = "Detail Daftar Harga ";
        $this->data['customer_id'] = $customer_id;
        $this->data['origin'] = $origin;
        $this->data['destination'] = $destination;

        $this->set_title($customer_id, $origin, $destination);

        if ($this->input->is_ajax_request()) {
            echo $this->dt_customers_price_detail->get($this, $customer_id, $origin, $destination);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add($customer_id = null, $origin = null, $destination = null) {

        $this->data['title'] = "Tambah Data Harga Pelanggan ";

        $this->data['customer_id'] = $customer_id;
        $this->data['origin'] = $origin;
        $this->data['destination'] = $destination;

        $this->set_title($customer_id, $origin, $destination);

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store($customers_id = null, $origin = null, $destination = null) {

        $validation = $this->validation();

        if (!$this->request_customers_price_detail->validation($validation)) {
            redirect($this->controller_path . '/add/' . $customers_id . '/' . $origin . '/' . $destination, 'refresh');
        } else {

            $this->customers_price_model->insert_price(
                    $customers_id, $this->input->post('branch_origin'), $this->input->post('branch_destination'), $this->input->post('customer_price')
            );

            set_alert('alert-success', 'Data harga baru berhasil ditambahkan');

            redirect($this->controller_path . '/index/' . $customers_id . '/' . $origin . '/' . $destination, 'refresh');
        }
    }

    public function edit($id, $customers_id, $origin, $destination) {

        $this->data['title'] = "Edit Data Harga Pelanggan : ";
        $this->data['customer_id'] = $customers_id;
        $this->data['origin'] = $origin;
        $this->data['destination'] = $destination;
        $this->data['data'] = $this->customers_price_model->get(['customer_price_id' => $id])->row();

        $this->set_title($customers_id, $origin, $destination);

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update($id, $customers_id, $origin, $destination) {

        if (!$this->request_customers_price_detail->validation()) {
            redirect($this->controller_path . '/edit/' . $id . '/' . $customers_id . '/' . $origin . '/' . $destination, 'refresh');
        } else {

            $this->customers_price_model->update_price($id, $this->input->post('customer_price'));

            set_alert('alert-success', 'Data harga telah diperbarui');

            redirect($this->controller_path . '/index/' . $customers_id . '/' . $origin . '/' . $destination, 'refresh');
        }
    }

    public function destroy($id, $customers_id, $origin, $destination) {
        $data = $this->customers_price_model->destroy($id);
        set_alert('alert-success', 'Data harga telah berhasil dihapus');
        redirect($this->controller_path . '/index/' . $customers_id . '/' . $origin . '/' . $destination, 'refresh');
    }

    private function set_title($customer_id, $origin, $destination) {
        $this->data['title'] .= ' Untuk Pelanggan ' . $this->costumers_model->get_name($customer_id);
        $this->data['title'] .= ' : ' . $this->cities_model->get_name($origin);
        $this->data['title'] .= ' - ' . $this->cities_model->get_name($destination);
    }

    private function validation() {
        $validation = array(
            [
                'field' => 'branch_origin',
                'label' => 'Pilih Cabang Asal',
                'rules' => 'required'
            ], [
                'field' => 'branch_destination',
                'label' => 'Pilih Cabang Tujuan',
                'rules' => 'required'
            ]
        );
        return $validation;
    }

}
