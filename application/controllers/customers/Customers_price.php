<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_price extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'customers_price';
    public $controller_path = 'customers/customers_price';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "1";
            $this->id = $this->input->post('customer_id');

            $this->load->model('customers_price_model');
            $this->load->model('costumers_model');
            $this->load->model('cities_model');
            $this->load->model('branches_model');
            $this->load->library('request/request_customers_price');
            $this->load->library('datatable/dt_customers_price');

            $this->data['cities'] = $this->cities_model->get_available()->result_array();
        } else {
            redirect('program/login');
        }
    }

    public function index($customer_id = 0) {

        $this->data['title'] = "Daftar Harga Pelanggan : ";
        $this->data['customer_id'] = $customer_id;

        $this->set_title($customer_id);

        if ($this->input->is_ajax_request()) {
            echo $this->dt_customers_price->get($this, $customer_id);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add($customer_id = 0) {
        $this->data['title'] = "Membuat Harga Pelanggan : ";
        $this->data['customer_id'] = $customer_id;
        $this->set_title($customer_id);

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store($customer_id = 0) {

        $config_validation = array([
                'field' => 'city_origin',
                'label' => 'Kota Asal',
                'rules' => 'required'
            ], [
                'field' => 'city_destination',
                'label' => 'Kota Tujuan',
                'rules' => 'required'
        ]);

        if (!$this->request_customers_price->validation($config_validation)) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            /** Get branch available */
            $branches_origin = $this->branches_model->get([
                        'branch_city' => $this->input->post('city_origin')
                    ])->result();

            $branches_destination = $this->branches_model->get([
                        'branch_city' => $this->input->post('city_destination')
                    ])->result();

            /** Get city name */
            $origin_city_name = $this->cities_model->get([
                        'city_id' => $this->input->post('city_origin')
                    ])->row()->city_name;

            $destination_city_name = $this->cities_model->get([
                        'city_id' => $this->input->post('city_destination')
                    ])->row()->city_name;

            /**  Set error message when branch is not available */
            $error_message = '';
            $error = false;

            if (empty($branches_origin)) {
                $error_message .= 'Tidak ada cabang di Kota Asal : ' . $origin_city_name . '<br/>';
                $error = true;
            }

            if (empty($branches_destination)) {
                $error_message .= 'Tidak ada  cabang di Kota Tujuan : ' . $destination_city_name . '<br/>';
                $error = true;
            }

            if ($error == true) {
                set_alert('alert-error', $error_message);
                redirect($this->controller_path . '/add', 'refresh');
                return false;
            }

            $this->customers_price_model->mass_insert($customer_id, $branches_origin, $branches_destination, $this->input->post('customer_price'));

            set_alert('alert-success', 'Data harga langganan berhasil ditambahkan');

            redirect($this->controller_path . '/index/' . $customer_id, 'refresh');
        }
    }

    public function edit($customer_id = 0, $origin_city = 0, $destination_city = 0) {

        $this->data['title'] = "Perbarui Data Pelanggan : ";

        $this->data['origin_city'] = $origin_city;
        $this->data['destination_city'] = $destination_city;
        $this->data['customer_id'] = $customer_id;

       $this->set_title($customer_id);

        $this->data['data'] = $this->customers_price_model->get_joined([
                    'cities_origin.city_id' => $origin_city,
                    'cities_destination.city_id' => $destination_city
                ])->row();

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update($customer_id = 0, $origin_city = 0, $destination_city = 0) {

        if (!$this->request_customers_price->validation()) {
            redirect($this->controller_path . '/edit/' . $customer_id . '/' . $origin_city . '/' . $destination_city, 'refresh');
            return false;
        } else {

            /** Delete all old data */
            $this->customers_price_model->destroy_perdelivery($customer_id, $origin_city, $destination_city);

            /** Get branch available */
            $branches_origin = $this->branches_model->get([
                        'branch_city' => $origin_city
                    ])->result();

            $branches_destination = $this->branches_model->get([
                        'branch_city' => $destination_city
                    ])->result();

            /** Get city name */
            $origin_city_name = $this->cities_model->get([
                        'city_id' => $this->input->post('city_origin')
                    ])->row()->city_name;

            $destination_city_name = $this->cities_model->get([
                        'city_id' => $this->input->post('city_destination')
                    ])->row()->city_name;

            /**  Set error message when branch is not available */
            $error_message = '';
            $error = false;

            if (empty($branches_origin)) {
                $error_message .= 'Tidak ada cabang di Kota Asal : ' . $origin_city_name . '<br/>';
                $error = true;
            }

            if (empty($branches_destination)) {
                $error_message .= 'Tidak ada  cabang di Kota Tujuan : ' . $destination_city_name . '<br/>';
                $error = true;
            }

            if ($error == true) {
                set_alert('alert-error', $error_message);
                redirect($this->controller_path . '/edit/' . $customer_id . '/' . $origin_city . '/' . $destination_city, 'refresh');
                return false;
            }

            $this->customers_price_model->mass_insert($customer_id, $branches_origin, $branches_destination, $this->input->post('customer_price'));

            set_alert('alert-success', 'Data harga langganan berhasil ditambahkan');

            redirect($this->controller_path . '/index/' . $customer_id);
        }
    }

    public function destroy($cusomter_id = null, $origin = null, $destination = null) {

        $this->customers_price_model->destroy_perdelivery($cusomter_id, $origin, $destination);

        set_alert('alert-success', 'Data harga langganan telah dihapus');

        redirect($this->controller_path . '/index/' . $cusomter_id);
    }

    private function set_title($customer_id = null) {
        $this->data['title'] .= $this->costumers_model->get_name($customer_id);
    }

}
