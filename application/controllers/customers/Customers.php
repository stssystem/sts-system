<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'customers';
    public $controller_path = 'customers/customers';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "1";
            $this->id = $this->input->post('customer_id');

            $this->load->model('costumers_model');
            $this->load->model('cities_model');
            $this->load->model('provinces_model');
            $this->load->model('districts_model');
            $this->load->model('villages_model');
            $this->load->model('branches_model');
            $this->load->library('request/request_costumer');
            $this->load->library('datatable/dt_customers');

            $this->data['provinces']        = $this->provinces_model->get_available()->result_array();
            $this->data['cities']           = $this->cities_model->get()->result_array();
            $this->data['districts']        = $this->districts_model->get()->result_array();
            $this->data['villages']         = $this->villages_model->get()->result_array();
            $this->data['user_branch_id']   = profile()->branch_id;
            $this->data['user_branch_name'] = profile()->branch_name;
            $this->data['list_branches']    = $this->branches_model->get_joined()->result();
        } else {
            redirect('program/login');
        }
    }

    public function index() {
        $this->data['title']        = "Daftar Nama Pelanggan";
        if ($this->input->is_ajax_request()) {
            echo $this->dt_customers->get($this, $_REQUEST);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add() {
        $this->data['title'] = "Masukkan Data Pelanggan";
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store() {

        if (!$this->request_costumer->validation()) {
            return redirect($this->controller_path . '/add', 'refresh');
        } else {

            $branch_created     = $this->input->post('user_branch_id');
            $new_branch_created = $this->input->post('branch_created_id');

            /** Customers create */
            $set_array = array(
                'customer_referral'                 => $this->input->post('customer_referral'),
                'customer_code'                     => $this->input->post('customer_code'),
                'customer_name'                     => $this->input->post('customer_name'),
                'customer_phone'                    => $this->input->post('customer_phone'),
                'customer_address'                  => $this->input->post('customer_address'),
                'customer_province_id'              => $this->input->post('customer_province_id'),
                'customer_city'                     => $this->input->post('customer_city_id'),
                'customer_districts_id'             => $this->input->post('customer_districts_id'),
                'customer_village_id'               => $this->input->post('customer_village_id'),
                'customer_id_type'                  => $this->input->post('customer_id_type'),
                'customer_id_type_other'            => $this->input->post('customer_id_type_other'),
                'customer_id_number'                => $this->input->post('customer_id_number'),
                'customer_type'                     => $this->input->post('customer_type'),
                'customer_company_type'             => $this->input->post('customer_company_type'),
                'customer_company_type_other'       => $this->input->post('customer_company_type_other'),
                'customer_company_type_of_business' => $this->input->post('customer_company_type_of_business'),
                'customer_web_id'                   => $this->input->post('customer_web_id'),
                'customer_freq_sending'             => $this->input->post('customer_freq_sending'),
                'customer_sending_quota'            => $this->input->post('customer_sending_quota'),
                
                'pic_first_name'                    => $this->input->post('pic_first_name'),
                'pic_last_name'                     => $this->input->post('pic_last_name'),
                'pic_identity_type'                 => $this->input->post('pic_identity_type'),
                'pic_identity_type_other'           => $this->input->post('pic_identity_type_other'),
                'pic_identity_number'               => $this->input->post('pic_identity_number'),
                'pic_birth_date'                    => (!empty($this->input->post('pic_birth_date'))) ? strtotime($this->input->post('pic_birth_date')) : ' ',
                'pic_gender'                        => $this->input->post('pic_gender'),
                'pic_phone'                         => $this->input->post('pic_phone'),
                'pic_email'                         => $this->input->post('pic_email'),
                'pic_religion'                      => $this->input->post('pic_religion'),
                'pic_address'                       => $this->input->post('pic_address'),
                'pic_province_id'                   => $this->input->post('pic_province_id'),
                'pic_city_id'                       => $this->input->post('pic_city_id'),
                'pic_districts_id'                  => $this->input->post('pic_districts_id'),
                'pic_village_id'                    => $this->input->post('pic_village_id'),

                'customer_last_transaction'         => time(),
                'branch_created'                    => (!empty($branch_created)) ? $branch_created : $new_branch_created,
                'created_at'                        => time()
                
            );
            
            $this->costumers_model->insert($set_array);

            set_alert('alert-success', 'Data pelanggan <strong>' . $this->input->post('customer_name') . '</strong> berhasil ditambahkan');

            return redirect($this->controller_path, 'refresh');
        }
    }

    public function edit($id = 0) {

        $this->data['title'] = "Edit Data Pelanggan";
        $this->data['data'] = $this->costumers_model->get(['customer_id' => $id])->row();
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update() {

        if (!$this->request_costumer->validation()) {
            redirect($this->controller_path . '/edit/' . $this->id, 'refresh');
        } else {

            $branch_created     = $this->input->post('user_branch_id');
            $new_branch_created = $this->input->post('branch_created_id');

            $where_array = array(
                'customer_id'                       => $this->id,
                'customer_referral'                 => $this->input->post('customer_referral'),
                'customer_code'                     => $this->input->post('customer_code'),
                'customer_name'                     => $this->input->post('customer_name'),
                'customer_phone'                    => $this->input->post('customer_phone'),
                'customer_address'                  => $this->input->post('customer_address'),
                'customer_province_id'              => $this->input->post('customer_province_id'),
                'customer_city'                     => $this->input->post('customer_city_id'),
                'customer_districts_id'             => $this->input->post('customer_districts_id'),
                'customer_village_id'               => $this->input->post('customer_village_id'),
                'customer_id_type'                  => $this->input->post('customer_id_type'),
                'customer_id_type_other'            => $this->input->post('customer_id_type_other'),
                'customer_id_number'                => $this->input->post('customer_id_number'),
                'customer_type'                     => $this->input->post('customer_type'),
                'customer_company_type'             => $this->input->post('customer_company_type'),
                'customer_company_type_other'       => $this->input->post('customer_company_type_other'),
                'customer_company_type_of_business' => $this->input->post('customer_company_type_of_business'),
                'customer_web_id'                   => $this->input->post('customer_web_id'),
                'customer_freq_sending'             => $this->input->post('customer_freq_sending'),
                'customer_sending_quota'            => $this->input->post('customer_sending_quota'),
                
                'pic_first_name'                    => $this->input->post('pic_first_name'),
                'pic_last_name'                     => $this->input->post('pic_last_name'),
                'pic_identity_type'                 => $this->input->post('pic_identity_type'),
                'pic_identity_type_other'           => $this->input->post('pic_identity_type_other'),
                'pic_identity_number'               => $this->input->post('pic_identity_number'),
                'pic_birth_date'                    => (!empty($this->input->post('pic_birth_date'))) ? strtotime($this->input->post('pic_birth_date')) : ' ',
                'pic_gender'                        => $this->input->post('pic_gender'),
                'pic_phone'                         => $this->input->post('pic_phone'),
                'pic_email'                         => $this->input->post('pic_email'),
                'pic_religion'                      => $this->input->post('pic_religion'),
                'pic_address'                       => $this->input->post('pic_address'),
                'pic_province_id'                   => $this->input->post('pic_province_id'),
                'pic_city_id'                       => $this->input->post('pic_city_id'),
                'pic_districts_id'                  => $this->input->post('pic_districts_id'),
                'pic_village_id'                    => $this->input->post('pic_village_id'),

                'customer_last_transaction'         => time(),
                'branch_created'                    => (!empty($branch_created)) ? $branch_created : $new_branch_created,
                'updated_at'                        => time()
                
                // 'customer_company_name'             => str_replace(' ', '', $this->input->post('customer_company_name')),
                // 'customer_company_website'          => $this->input->post('customer_company_website'),
                // 'customer_company_telp'             => $this->input->post('customer_company_telp'),
                // 'customer_company_email'            => $this->input->post('customer_company_email'),
                // 'customer_company_address'          => $this->input->post('customer_company_address'),
                // 'customer_company_owner_first_name'  => $this->input->post('customer_company_owner_first_name'),
                // 'customer_company_owner_last_name'   => $this->input->post('customer_company_owner_last_name'),
                // 'customer_company_type'             => $this->input->post('customer_company_type'),
                // 'customer_company_type_other'       => $this->input->post('customer_company_type_other'),
            );

            $this->costumers_model->update($where_array);

            set_alert('alert-success', 'Data pelanggan <strong>' . $this->input->post('customer_name') . '</strong> telah diperbarui');

            redirect($this->controller_path);
        }
    }

    public function destroy($id = null) {
        $data = $this->costumers_model->delete(['customer_id' => $id]);
        set_alert('alert-danger', 'Data pelanggan <strong>' . $data->customer_name . '</strong> telah dihapus');
        redirect($this->controller_path);
    }
    
    public function reset(){
        $this->session->unset_userdata('dt_customers');
        redirect($this->controller_path);        
    }

    public function filter_customers() {

        $data['date_start']         = $this->input->post('date_start') != '' ? $this->input->post('date_start') : '';
        $data['date_end']           = $this->input->post('date_end') != '' ? $this->input->post('date_end') : '';
        
        $data['customer_name'] = $this->input->post('customer_name') != '' ? $this->input->post('customer_name') : '';
        
        $data['customer_city'] = $this->input->post('customer_city') != '' ? $this->input->post('customer_city') : '';
        
        $data['customer_address'] = $this->input->post('customer_address') != '' ? $this->input->post('customer_address') : '';
        
        $data['pic_religion'] = $this->input->post('pic_religion') != '' ? $this->input->post('pic_religion') : '';
        
        $data['customer_company_type'] = $this->input->post('customer_company_type') != '' ? $this->input->post('customer_company_type') : '';
        
        $data['customer_company_type_of_business'] = $this->input->post('customer_company_type_of_business') != '' ? $this->input->post('customer_company_type_of_business') : ''; 

        $this->session->set_userdata('customers', $data);

        redirect($this->controller_path);
    }

    public function clear_customers() {
        $this->session->unset_userdata('customers');
        $this->session->unset_userdata('dt_customers');
        redirect($this->controller_path);
    }

}
