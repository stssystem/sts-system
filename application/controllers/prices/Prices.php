<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prices extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'prices';
    public $controller_path = 'prices/prices';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_price";
            $this->id = $this->input->post('price_id');

            $this->load->model('prices_model');
            $this->load->model('cities_model');
            $this->load->model('branches_model');
            $this->load->library('request/request_prices');
            $this->load->library('datatable/dt_prices');

            $this->data['cities'] = $this->cities_model->get()->result_array();
            $this->data['branches'] = $this->branches_model->get()->result_array();
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        $this->data['title'] = "Pricelist Management";

        if ($this->input->is_ajax_request()) {
            echo $this->dt_prices->get($this);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add() {
        $this->data['title'] = "Tambah Data Harga";
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store() {

        if (!$this->request_prices->validation()) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            /** Get branch available */
            $branches_origin = $this->branches_model->get([
                        'branch_city' => $this->input->post('city_origin')
                    ])->result();

            $branches_destination = $this->branches_model->get([
                        'branch_city' => $this->input->post('city_destination')
                    ])->result();

            /** Get city name */
            $origin_city_name = $this->cities_model->get([
                        'city_id' => $this->input->post('city_origin')
                    ])->row()->city_name;

            $destination_city_name = $this->cities_model->get([
                        'city_id' => $this->input->post('city_destination')
                    ])->row()->city_name;

            /**  Set error message when branch is not available */
            $error_message = '';
            $error = false;
            if (empty($branches_origin)) {
                $error_message .= 'Tidak ada cabang di Kota Asal : ' . $origin_city_name . '<br/>';
                $error = true;
            }
            if (empty($branches_destination)) {
                $error_message .= 'Tidak ada  cabang di Kota Tujuan : ' . $destination_city_name . '<br/>';
                $error = true;
            }

            if ($error == true) {
                set_alert('alert-error', $error_message);
                redirect($this->controller_path . '/add', 'refresh');
                return false;
            }

            if (!empty($branches_origin)) {
                foreach ($branches_origin as $key => $item) {

                    if (!empty($branches_destination)) {
                        foreach ($branches_destination as $k => $i) {

                            /** Prices create */
                            $set_array = array(
                                'price_branch_origin_id' => $item->branch_id,
                                'price_branch_destination_id' => $i->branch_id,
                                'price_kg_retail' => $this->input->post('price_kg_retail'),
                                'price_kg_partai' => $this->input->post('price_kg_partai'),
                                'price_volume_retail' => $this->input->post('price_volume_retail'),
                                'price_volume_partai' => $this->input->post('price_volume_partai'),
                                'price_minimum' => $this->input->post('price_minimum'),
                                'created_at' => time(),
                            );

                            $id = $this->prices_model->insert($set_array);
                        }
                    }
                }
            }

            set_alert('alert-success', 'Data harga baru berhasil ditambahkan');
            redirect($this->controller_path, 'refresh');
        }
    }

    public function edit($origin_city = 0, $destination_city = 0) {

        $this->data['title'] = "Edit Data Harga";
        $this->data['origin_city'] = $origin_city;
        $this->data['destination_city'] = $destination_city;
        $this->data['data'] = $this->prices_model->get_joined([
                    'cities_origin.city_id' => $origin_city,
                    'cities_destination.city_id' => $destination_city
                ])->row();

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update($origin, $destination) {

        if (!$this->request_prices->validation()) {
            redirect($this->controller_path . '/edit/' . $this->id, 'refresh');
        } else {

            /** Get branch available */
            $branches_origin = $this->branches_model->get([
                        'branch_city' => $this->input->post('city_origin')
                    ])->result();

            $branches_destination = $this->branches_model->get([
                        'branch_city' => $this->input->post('city_destination')
                    ])->result();

            /** Get city name */
            $origin_city_name = $this->cities_model->get([
                        'city_id' => $this->input->post('city_origin')
                    ])->row()->city_name;

            $destination_city_name = $this->cities_model->get([
                        'city_id' => $this->input->post('city_destination')
                    ])->row()->city_name;

            /**  Set error message when branch is not available */
            $error_message = '';
            $error = false;
            if (empty($branches_origin)) {
                $error_message .= 'Tidak ada cabang di Kota Asal : ' . $origin_city_name . '<br/>';
                $error = true;
            }
            if (empty($branches_destination)) {
                $error_message .= 'Tidak ada  cabang di Kota Tujuan : ' . $destination_city_name . '<br/>';
                $error = true;
            }

            if ($error == true) {
                set_alert('alert-error', $error_message);
                redirect($this->controller_path . '/edit/' . $origin . '/' . $destination, 'refresh');
                return false;
            }

            /** Remove all prices */
            $prices = $this->prices_model->get_joined([
                        'cities_origin.city_id' => $origin,
                        'cities_destination.city_id' => $destination
                    ])->result();

            /** Deleting all first */
            if (!empty($prices)) {
                foreach ($prices as $key => $item) {
                    $this->prices_model->destroy($item->price_id);
                }
            }

            if (!empty($branches_origin)) {
                foreach ($branches_origin as $key => $item) {

                    if (!empty($branches_destination)) {
                        foreach ($branches_destination as $k => $i) {

                            /** Prices create */
                            $set_array = array(
                                'price_branch_origin_id' => $item->branch_id,
                                'price_branch_destination_id' => $i->branch_id,
                                'price_kg_retail' => $this->input->post('price_kg_retail'),
                                'price_kg_partai' => $this->input->post('price_kg_partai'),
                                'price_volume_retail' => $this->input->post('price_volume_retail'),
                                'price_volume_partai' => $this->input->post('price_volume_partai'),
                                'price_minimum' => $this->input->post('price_minimum'),
                                'created_at' => time(),
                            );

                            $id = $this->prices_model->insert($set_array);
                        }
                    }
                }
            }

            set_alert('alert-success', 'Data harga telah diperbarui');

            redirect($this->controller_path);
        }
    }

    public function destroy($origin, $destination) {

        /** Remove all prices */
        $prices = $this->prices_model->get_joined([
                    'cities_origin.city_id' => $origin,
                    'cities_destination.city_id' => $destination
                ])->result();

        /** Deleting all first */
        if (!empty($prices)) {
            foreach ($prices as $key => $item) {
                $this->prices_model->destroy($item->price_id);
            }
        }

        set_alert('alert-success', 'Data harga telah dihapus');

        redirect($this->controller_path);
    }

}
