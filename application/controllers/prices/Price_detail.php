<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Price_detail extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'price_detail';
    public $controller_path = 'prices/price_detail';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_price_detail";
            $this->id = $this->input->post('price_id');

            $this->load->model('prices_model');
            $this->load->model('cities_model');
            $this->load->model('branches_model');

            $this->load->library('request/request_prices_detail');
            $this->load->library('datatable/dt_prices_detail');
        } else {
            redirect('program/login');
        }
    }

    public function index($origin = null, $destination = null) {

        $this->data['title'] = "Detail Daftar Harga : ";
        $this->data['origin'] = $origin;
        $this->data['destination'] = $destination;

        $this->set_title($origin, $destination);

        if ($this->input->is_ajax_request()) {
            echo $this->dt_prices_detail->get($this, $origin, $destination);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add($origin = null, $destination = null) {

        $this->data['title'] = "Tambah Data Kota : ";

        $this->data['origin'] = $origin;
        $this->data['destination'] = $destination;

        $this->set_title($origin, $destination);

        $this->load->helper('cities');
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store($origin = null, $destination = null) {

        $validation = $this->validation();
        
        if (!$this->request_prices_detail->validation($validation)) {
            redirect($this->controller_path . '/add/' . $origin . '/' . $destination, 'refresh');
        } else {

            /** Prices insert */
            $set_array = array(
                'price_branch_origin_id' => $this->input->post('branch_origin'),
                'price_branch_destination_id' => $this->input->post('branch_destination'),
                'price_kg_retail' => $this->input->post('price_kg_retail'),
                'price_kg_partai' => $this->input->post('price_kg_partai'),
                'price_volume_retail' => $this->input->post('price_volume_retail'),
                'price_volume_partai' => $this->input->post('price_volume_partai'),
                'price_minimum' => $this->input->post('price_minimum'),
                'created_at' => time(),
            );

            $id = $this->prices_model->insert($set_array);

            set_alert('alert-success', 'Data harga baru berhasil ditambahkan');

            redirect($this->controller_path . '/index/' . $origin . '/' . $destination, 'refresh');
        }
    }

    public function edit($id = null, $origin, $destination) {

        $this->data['title'] = "Edit Data Harga : ";
        $this->data['origin'] = $origin;
        $this->data['destination'] = $destination;
        $this->data['data'] = $this->prices_model->get(['price_id' => $id])->row();

        $this->set_title($origin, $destination);

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update($id = null, $origin, $destination) {

        if (!$this->request_prices_detail->validation()) {
            redirect($this->controller_path . '/edit/' . $id . '/' . $origin . '/' . $destination, 'refresh');
        } else {

            /** Prices update */
            $set_array = array(
                'price_id' => $id,
                'price_kg_retail' => $this->input->post('price_kg_retail'),
                'price_kg_partai' => $this->input->post('price_kg_partai'),
                'price_volume_retail' => $this->input->post('price_volume_retail'),
                'price_volume_partai' => $this->input->post('price_volume_partai'),
                'price_minimum' => $this->input->post('price_minimum'),
                'created_at' => time(),
            );

            $id = $this->prices_model->update($set_array);

            set_alert('alert-success', 'Data harga telah diperbarui');

            redirect($this->controller_path . '/index/' . $origin . '/' . $destination, 'refresh');
        }
    }

    public function destroy($id = null, $origin, $destination) {
        $data = $this->prices_model->destroy($id);
        set_alert('alert-danger', 'Data harga telah berhasil dihapus');
        redirect($this->controller_path . '/index/' . $origin . '/' . $destination, 'refresh');
    }

    private function set_title($origin, $destination) {
        $this->data['title'] .= $this->cities_model->get_name($origin);
        $this->data['title'] .= ' - ' . $this->cities_model->get_name($destination);
    }
    
    private function validation(){
        $validation = array(
            [
                'field' => 'branch_origin',
                'label' => 'Pilih Cabang Asal',
                'rules' => 'required'
            ], [
                'field' => 'branch_destination',
                'label' => 'Pilih Cabang Tujuan',
                'rules' => 'required'
            ]
        );
        return $validation;
    }

}
