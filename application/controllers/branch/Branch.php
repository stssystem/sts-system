<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'branches';
    public $controller_path = 'branch/branch';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('branches_model');
            $this->load->model('users_model');
            $this->load->model('cities_model');

            $this->load->library('request/request_branches');
            $this->load->library('datatable/dt_branches');

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_branch";
            $this->data['users'] = $this->users_model->get_profile()->result_array();
            $this->data['cities'] = $this->cities_model->get()->result_array();
            $this->data['branches'] = $this->branches_model->get()->result_array();

            $this->data['users_options'] = $this->users_model
                    ->get_profile(['userprofile.branch_id' => null])
                    ->result_array();

            $this->id = $this->input->post('branch_id');
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        $this->data['title'] = "Daftar Cabang";

        if ($this->input->is_ajax_request()) {
            echo $this->dt_branches->get($this, $_REQUEST);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add() {
        $this->data['title'] = "Tambah Data Cabang";
        $this->load->helper('regional');
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store() {

        if (!$this->request_branches->validation()) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            /** route create */
            $set_array = array(
                'branch_name'               => $this->input->post('branch_name'),
                'branch_address'            => $this->input->post('branch_address'),
                'branch_geo'                => $this->input->post('branch_geo'),
                'branch_parent'             => $this->input->post('branch_parent'),
                'branch_manager'            => $this->input->post('branch_manager'),
                'branch_status'             => $this->input->post('branch_status'),
                'branch_city'               => $this->input->post('branch_city'),
                'branch_code'               => $this->input->post('branch_code'),
                'branch_type'               => $this->input->post('branch_type'),
                'branch_regional_id'        => $this->input->post('branch_regional_id'),
                'branch_pickup_available'   => $this->input->post('branch_pickup_available') ? 1 : 0,
                'branch_phone'              => $this->input->post('branch_phone'), //phone
                'branch_email'              => $this->input->post('branch_email'), //email
                'branch_comission'              => $this->input->post('branch_comission'), //email
                'created_at'                => time(),
            );

            $this->branches_model->insert($set_array);

            /** Insert detail trips */
            set_alert('alert-success', 'Data cabang <strong>' . $this->input->post('branch_name') . '</strong> berhasil ditambahkan');

            redirect($this->controller_path, 'refresh');
        }
    }

    public function edit($id = 0) {

        $this->data['title'] = "Edit Data Cabang";
        $this->data['data'] = $this->branches_model->get(['branch_id' => $id])->row();
        $this->load->helper('regional');

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        
    }

    public function update() {

        if (!$this->request_branches->validation()) {
            redirect($this->controller_path . '/edit/' . $this->id, 'refresh');
        } else {

            $where_array = array(
                'branch_id'                 => $this->id,
                'branch_name'               => $this->input->post('branch_name'),
                'branch_address'            => $this->input->post('branch_address'),
                'branch_geo'                => $this->input->post('branch_geo'),
                'branch_parent'             => $this->input->post('branch_parent'),
                'branch_manager'            => $this->input->post('branch_manager'),
                'branch_status'             => $this->input->post('branch_status'),
                'branch_city'               => $this->input->post('branch_city'),
                'branch_code'               => $this->input->post('branch_code'),
                'branch_type'               => $this->input->post('branch_type'),
                'branch_regional_id'        => $this->input->post('branch_regional_id'),
                'branch_pickup_available'   => $this->input->post('branch_pickup_available') ? 1 : 0,
                'branch_phone'              => $this->input->post('branch_phone'),
                'branch_email'              => $this->input->post('branch_email'),
                'branch_comission'              => $this->input->post('branch_comission'), //email
                'updated_at' => time()
            );

            $this->branches_model->update($where_array);

            set_alert('alert-success', 'Data cabang <strong>' . $this->input->post('branch_name') . '</strong> telah diperbarui');

            redirect($this->controller_path);
            
        }
    }

    public function destroy($id = null) {

        $data = $this->branches_model->delete(['branch_id' => $id]);

        set_alert('alert-danger', 'Data cabang <strong>' . $data->branch_name . '</strong> telah dihapus');

        redirect($this->controller_path);
    }

    public function staff($id = 0) {

        $this->load->model('packages_model');
        $this->load->model('orders_model');

        $this->data['title'] = "Kelola Staff Cabang";
        $this->data['data'] = $this->branches_model->get_joined(['branches.branch_id' => $id])->row();
        $this->data['users_available'] = $this->users_model->get_profile(['userprofile.branch_id' => $id])->result();
        $this->data['count_staff'] = count($this->data['users_available']);
        $this->data['count_package'] = $this->packages_model->get(
                        ['package_origin' => $id, 'package_status' => 1], ['select' => 'count(*) as total']
                )->row()->total;
        $this->data['count_order'] = $this->orders_model->get(
                        ['order_origin' => $id, 'order_status' => 0], ['select' => 'count(*) as total']
                )->row()->total;

        $this->load->view($this->view_path . '/staff', $this->data);
        
    }

    public function get_options() {
        echo $this->form->select([
            'name' => 'branch_user_id',
            'label' => '',
            'value' => $this->data['users_options'],
            'keys' => 'user_id',
            'values' => 'name',
            'multiple' => true,
            'class' => 'select2',
            'display_default' => false
        ]);
    }

}
