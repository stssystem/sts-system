<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Regionals extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'regionals';
    public $controller_path = 'branch/regionals';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_regionals";
            $this->id = $this->input->post('regional_id');

            $this->load->model('regionals_model');
            $this->load->library('request/request_regionals');
            $this->load->library('datatable/dt_regionals');
            
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        $this->data['title'] = "Daftar Regional";

        if ($this->input->is_ajax_request()) {
            echo $this->dt_regionals->get($this);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add() {
        
        /** Load all resource needed */
        $this->load->helper('users');
        
        $this->data['title'] = "Tambah Data Regional";
        
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        
    }

    public function store() {

        if (!$this->request_regionals->validation()) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            /** Cities create */
            $set_array = array(
                'regional' => $this->input->post('regional'),
                'regional_manager' => $this->input->post('regional_manager'),
                'created_at' => time(),
            );

            $this->regionals_model->insert($set_array);

            set_alert('alert-success', 'Data kota <strong>' . $this->input->post('city_name') . '</strong> berhasil ditambahkan');

            redirect($this->controller_path, 'refresh');
            
        }
    }

    public function edit($id = 0) {
        
        /** Load all resource needed */
        $this->load->helper('users');

        $this->data['title'] = "Edit Data Regional";
        $this->data['data'] = $this->regionals_model->get(['regionals.regional_id' => $id])->row();

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        
    }

    public function update() {

        if (!$this->request_regionals->validation()) {
            redirect($this->controller_path . '/edit/' . $this->id, 'refresh');
        } else {

            $where_array = array(
                'regional_id' => $this->id,
                'regional' => $this->input->post('regional'),
                'regional_manager' => $this->input->post('regional_manager'),
                'updated_at' => time()
            );
            
            $this->regionals_model->update($where_array);

            set_alert('alert-success', 'Data Regional <strong>' . $this->input->post('regional_name') . '</strong> telah diperbarui');

            redirect($this->controller_path);
            
        }
    }

    public function destroy($id = null) {
        $data = $this->regionals_model->delete(['regional_id' => $id]);
        set_alert('alert-success', 'Data Regional <strong>' . $data->regional . '</strong> telah dihapus');
        redirect($this->controller_path);
    }

    public function staff($id = 0) {

        /** Load all resource needed */
        $this->load->model('users_model');
        $this->load->model('packages_model');
        $this->load->model('orders_model');
        $this->load->helper('users');

        $this->data['title'] = "Kelola Staff Regional";
        $this->data['regional_id'] = $id;
        $this->data['data'] = $this->regionals_model->get_regional_manager($id);
        $this->data['regional_staff'] = $this->users_model->get_regional_staff($id)->result();
        $this->data['count_staff'] = count($this->data['regional_staff']);
        $this->data['count_package'] = $this->packages_model->count_regional_package($id);
        $this->data['count_order'] = $this->orders_model->count_regional_order($id);

        $this->load->view($this->view_path . '/staff', $this->data);
        
    }

}
