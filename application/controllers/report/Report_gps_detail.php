<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_gps_detail extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_gps_detail';
    public $controller_path = 'report/report_gps_detail';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index($armada_id = '') {

        if ($this->input->is_ajax_request()) {
            $this->load->library('datatable/dt_report_gps_detail');
            echo $this->dt_report_gps_detail->get($this, $_REQUEST, $armada_id);
        } else {

            // Load all resource needed
            $this->load->model('armadas_model');
            
            // Collect data that will shown in page
            $this->data['armadas'] = $this->armadas_model->get(['armadas.armada_id' => $armada_id])->row();
            $this->data['title'] = "Laporan Detail GPS";
            $this->data['armada_id'] = $armada_id;
            
            // Load view 
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
            
        }
    }

    public function filter() {

        $this->load->model('branches_model');
        $this->load->model('cities_model');

        $data['date_start'] = $this->input->post('start_date') != '' ? strtotime($this->input->post('start_date')) : '';
        $data['date_end'] = $this->input->post('end_date') != '' ? strtotime($this->input->post('end_date')) : '';

        $data['branch_id'] = $this->input->post('branches');
        $data['branch_name'] = $this->branches_model
                ->get_branch_name($this->input->post('branches'));

        $data['city_id'] = $this->input->post('cities');
        $data['city_name'] = $this->cities_model
                ->get_name($this->input->post('cities'));

        $this->session->set_userdata('customers_report_filter', $data);

        redirect($this->controller_path);
    }

    public function clear($armada_id = 0) {
        $this->session->unset_userdata('dt_report_gps_detail');
        redirect($this->controller_path.'/index/'.$armada_id);
    }
    
    // Download detail gps report
    public function download($armada_id = 0){
        
          // Variable Initialization      
        $data = null;

        // Load model sale comission branch destination needed
        $this->load->model('report_gps_detail_model');
        $data = $this->report_gps_detail_model->download($armada_id);

        // Load resource needed
        $this->load->helper('report');

        // return data as excel file
        download_report($data, 'Laporan Detail GPS');
        
    }

}
