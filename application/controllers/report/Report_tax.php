<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_tax extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_tax';
    public $controller_path = 'report/report_tax';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "report_tax";
        } else {
            redirect('program/login');
        }
    }

    public function index() {
        
        if ($this->input->is_ajax_request()) {

            // Load all resource needed
            $this->load->library('datatable/dt_report_tax');
            
            echo $this->dt_report_tax->get($this, $_REQUEST);
            
        } else {
            
            // Reset session 
            $this->session->unset_userdata('tax_report_detail_filter');

            $this->data['title'] = 'Laporan Pajak';
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
            
        }
    }

    public function filter() {
        $data['date_start'] = str_replace('/', '-', $this->input->post('date_start_filter'));
        $data['date_end'] = str_replace('/', '-', $this->input->post('date_end_filter'));
        $this->session->set_userdata('tax_report_filter', $data);
        redirect($this->controller_path);
    }

    public function clear() {
        $this->session->unset_userdata('tax_report_filter');
        $this->session->unset_userdata('dt_report_tax');
        redirect($this->controller_path);
    }

    public function get_total() {
        $this->load->model('report_tax_model');
        $result = $this->report_tax_model->get_sum();
        $result['dpp'] = number_format(round($result['dpp'], 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
        $result['ppn'] = number_format(round($result['ppn'], 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
        $result['total'] = number_format(round($result['total'], 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
        echo json_encode($result);
    }
    
    public function donwload(){
        
        // Load all resource needed
        $this->load->model('report_tax_model');
        $this->load->helper('report');
        
        $data = $this->report_tax_model->get_download();
        download_report($data, 'Laporan Pajak');
        
    }

}
