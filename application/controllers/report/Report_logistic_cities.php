<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_logistic_cities extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_logistic/cities_cargo';
    public $controller_path = 'report/report_logistic_cities';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('report_logistic_model');
            $this->load->model('report_logistic_cities_model');
            $this->load->model('branches_model');

            $this->data['path']         = $this->controller_path;
            $this->data['title2']       = "";
            $this->data['active_menu']  = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index($city_id = 0) {
        if ($this->input->is_ajax_request()) {
            $this->load->library('datatable/dt_report_logistic_cities_detail');
            echo $this->dt_report_logistic_cities_detail->get($this, $_REQUEST, $city_id);
        } else {

            $this->data['title']          = "Detail Laporan Logistik ";
            $this->data['value_cities']   = $this->report_logistic_model->get_logistic_cities(['city_id' => $city_id])->row();
            $this->data['city_id']        = $city_id;

            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function filter($city_id = 0) {

        $data['date_start']     = $this->input->post('date_start') != '' ? $this->input->post('date_start') : '';
        $data['date_end']       = $this->input->post('date_end') != '' ? $this->input->post('date_end') : '';

        $data['total_weight']   = $this->input->post('total_weight') != '' ? round($this->input->post('total_weight')) : '';
        $data['total_size']     = $this->input->post('total_size') != '' ? round($this->input->post('total_size') * 1000000, 2) : '';

        $this->session->set_userdata('cities', $data);

        redirect($this->controller_path . '/index/' . $city_id);
    }

    public function clear($city_id = 0) {
        $this->session->unset_userdata('cities');
        $this->session->unset_userdata('dt_report_logistic_cities_detail');
        redirect($this->controller_path . '/index/' . $city_id);
    } 

}
