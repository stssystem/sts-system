<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_gps extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_gps';
    public $controller_path = 'report/report_gps';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        if ($this->input->is_ajax_request()) {

            // Load all resource needed
            $this->load->library('datatable/dt_report_gps');

            echo $this->dt_report_gps->get($this, $_REQUEST);
        } else {

            $this->data['title'] = "Laporan GPS";

            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function filter() {

        $this->load->model('branches_model');
        $this->load->model('cities_model');

        $data['date_start'] = $this->input->post('start_date') != '' ? strtotime($this->input->post('start_date')) : '';
        $data['date_end'] = $this->input->post('end_date') != '' ? strtotime($this->input->post('end_date')) : '';

        $data['branch_id'] = $this->input->post('branches');
        $data['branch_name'] = $this->branches_model
                ->get_branch_name($this->input->post('branches'));

        $data['city_id'] = $this->input->post('cities');
        $data['city_name'] = $this->cities_model
                ->get_name($this->input->post('cities'));

        $this->session->set_userdata('customers_report_filter', $data);

        redirect($this->controller_path);
    }

    public function clear() {
        $this->session->unset_userdata('dt_report_gps');
        redirect($this->controller_path);
    }

    // Download payment order
    public function download($branch_id = 0) {

        // Variable Initialization      
        $data = null;

        // Load model sale comission branch destination needed
        $this->load->model('report_gps_model');
        $data = $this->report_gps_model->download($branch_id);

        // Load resource needed
        $this->load->helper('report');

        // return data as excel file
        download_report($data, 'Laporan Detail GPS');
        
    }

}
