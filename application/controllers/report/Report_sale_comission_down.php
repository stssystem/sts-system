<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_sale_comission_down extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_sale/comission_down';
    public $controller_path = 'report/Report_sale_comission_down';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('report_sale_model');
            $this->load->model('branches_model');

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index($branch_id = 0) {
        if ($this->input->is_ajax_request()) {
            $this->load->library('datatable/dt_report_sale_comission_down_detail');
            echo $this->dt_report_sale_comission_down_detail->get($this, $_REQUEST, $branch_id);
        } else {

            $this->data['title']            = "Detail Laporan Komisi Agen ";
            $this->data['data_branches']    = $this->branches_model->get_joined(['branches.branch_id' => $branch_id])->row();
            $this->data['value_branches']   = $this->report_sale_model->get_value_comission_down(['sale_comission_report_down.branch_id' => $branch_id])->row();
            $this->data['branch_id']        = $branch_id;

            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function filter($branch_id = 0) {

        $data['date_start']         = $this->input->post('date_start') != '' ? $this->input->post('date_start') : '';
        $data['date_end']           = $this->input->post('date_end') != '' ? $this->input->post('date_end') : '';

        $data['branch_origin']      = $this->input->post('origin_filter');
        $data['branch_origin_name'] = $this->branches_model->get_branch_name($this->input->post('origin_filter'));

        $data['branch_destination'] = $this->input->post('destination_filter');
        $data['branch_destination_name'] = $this->branches_model->get_branch_name($this->input->post('destination_filter'));

        $this->session->set_userdata('comission_down', $data);

        redirect($this->controller_path . '/index/' . $branch_id);
    }

    public function clear($branch_id = 0) {
        $this->session->unset_userdata('comission_down');
        $this->session->unset_userdata('dt_report_sale_comission_down_detail');
        redirect($this->controller_path . '/index/' . $branch_id);
    } 

}
