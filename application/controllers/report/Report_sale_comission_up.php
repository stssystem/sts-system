<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_sale_comission_up extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_sale/comission_up';
    public $controller_path = 'report/Report_sale_comission_up';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('report_sale_model');
            $this->load->model('branches_model');

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index($branch_id = 0) {
        if ($this->input->is_ajax_request()) {
            $this->load->library('datatable/dt_report_sale_comission_up_detail');
            echo $this->dt_report_sale_comission_up_detail->get($this, $_REQUEST, $branch_id);
        } else {

            $this->data['title'] = "Detail Laporan Komisi Agen ";
            $this->data['data_branches'] = $this->branches_model->get_joined(['branches.branch_id' => $branch_id])->row();
            $this->data['value_branches'] = $this->report_sale_model->get_value_comission_up(['sale_comission_report_up.branch_id' => $branch_id])->row();
            $this->data['branch_id'] = $branch_id;

            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function filter($branch_id = 0) {

        $data['date_start'] = $this->input->post('date_start') != '' ? $this->input->post('date_start') : '';
        $data['date_end'] = $this->input->post('date_end') != '' ? $this->input->post('date_end') : '';

        $data['branch_origin'] = $this->input->post('origin_filter');
        $data['branch_origin_name'] = $this->branches_model->get_branch_name($this->input->post('origin_filter'));

        $data['branch_destination'] = $this->input->post('destination_filter');
        $data['branch_destination_name'] = $this->branches_model->get_branch_name($this->input->post('destination_filter'));

        $this->session->set_userdata('comission_up', $data);

        redirect($this->controller_path . '/index/' . $branch_id);
    }

    public function clear($branch_id = 0) {
        $this->session->unset_userdata('comission_up');
        $this->session->unset_userdata('dt_report_sale_comission_up_detail');
        redirect($this->controller_path . '/index/' . $branch_id);
    }

    /**
     * Get get total sum of omset and agen comission
     * 
     * @param string Json format
     */
    public function get_sum($branch_id = 0) {

        // Load report_sale_branch_origin_model model
        $this->load->model('report_sale_comission_up_model', 'sale_package_load');
        
        // Get result from model
        $result = $this->sale_package_load->get_sum($branch_id);
        
        // Send result
        echo json_encode($result);
    }

}
