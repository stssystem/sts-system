<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_logistic_armadas extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_sale/armadas_cargo';
    public $controller_path = 'report/report_logistic_armadas';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('report_logistic_armadas_model');

            $this->data['path']         = $this->controller_path;
            $this->data['title2']       = "";
            $this->data['active_menu']  = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index($branch_id = 0) {
        if ($this->input->is_ajax_request()) {
            $this->load->library('datatable/dt_report_logistic_armadas_2');
            echo $this->dt_report_logistic_armadas_2->get($this, $_REQUEST, $branch_id);
        } else {

            $this->data['title']            = "Detail Laporan Komisi Agen ";
            $this->data['data_branches']    = $this->branches_model->get_joined(['branches.branch_id' => $branch_id])->row();
            $this->data['value_branches']   = $this->report_logistic_armadas->get_value_armadas_cargo(['sale_comission_armadas_cargo.branch_id' => $branch_id])->row();
            $this->data['branch_id']        = $branch_id;

            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function filter($branch_id = 0) {

        $data['date_start']         = $this->input->post('date_start') != '' ? $this->input->post('date_start') : '';
        $data['date_end']           = $this->input->post('date_end') != '' ? $this->input->post('date_end') : '';

        $data['branch_origin']      = $this->input->post('origin_filter');
        $data['branch_origin_name'] = $this->branches_model->get_branch_name($this->input->post('origin_filter'));

        $data['armadas_cargo'] = $this->input->post('destination_filter');
        $data['armadas_cargo_name'] = $this->branches_model->get_branch_name($this->input->post('destination_filter'));

        $this->session->set_userdata('armadas_cargo', $data);

        redirect($this->controller_path . '/index/' . $branch_id);
    }

    public function clear($branch_id = 0) {
        $this->session->unset_userdata('armadas_cargo');
        $this->session->unset_userdata('dt_report_logistic_armadas_2');
        redirect($this->controller_path . '/index/' . $branch_id);
    } 

}
