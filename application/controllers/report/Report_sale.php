<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_sale extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_sale';
    public $controller_path = 'report/report_sale';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('branches_model');
            $this->load->model('cities_model');
            $this->load->model('report_sale_model');

            $this->load->helper('cities');

            $this->load->library('datatable/dt_branches');
            $this->load->library('datatable/dt_report_sale_comission_up');
            $this->load->library('datatable/dt_report_sale_comission_up_detail');
            $this->load->library('datatable/dt_report_sale_comission_down');
            $this->load->library('datatable/dt_report_order_payment');
            $this->load->library('datatable/dt_report_sale_branch_origin');
            $this->load->library('datatable/dt_report_sale_branch_destination');

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";
            $this->data['cities'] = $this->branches_model->get_joined_cities()->result_array();
        } else {
            redirect('program/login');
        }
    }

    public function index($id = 0) {
        $this->data['title'] = "Laporan Penjualan";

        if ($this->input->is_ajax_request()) {

            if (!empty($id)) {
                $status = $id;
            }

            if ($status == 1) {

                // Laporan komisi agen paket naik
                echo $this->dt_report_sale_comission_up->get($this, $_REQUEST);
            } else if ($status == 2) {

                // Laporan komisi agen paket turun
                echo $this->dt_report_sale_comission_down->get($this, $_REQUEST);
            } else if ($status == 3) {

                // Laporan status pembayaran
                echo $this->dt_report_order_payment->get($this, $_REQUEST);
            } else if ($status == 4) {

                //  Laporan komisi agen cabang asal
                echo $this->dt_report_sale_branch_origin->get($this, $_REQUEST);
            } else if ($status == 5) {

                // Laporan komisi agen cabang tujuan
                echo $this->dt_report_sale_branch_destination->get($this, $_REQUEST);
            }
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function filter_order_payment() {

        $data['date_start'] = $this->input->post('date_start') != '' ? $this->input->post('date_start') : '';
        $data['date_end'] = $this->input->post('date_end') != '' ? $this->input->post('date_end') : '';

        $data['branch_origin'] = $this->input->post('origin_filter');
        $data['branch_origin_name'] = $this->branches_model->get_branch_name($this->input->post('origin_filter'));
        $data['branch_destination'] = $this->input->post('destination_filter');
        $data['branch_destination_name'] = $this->branches_model->get_branch_name($this->input->post('destination_filter'));

        $data['type_filter'] = $this->input->post('type_filter') != '' ? $this->input->post('type_filter') : '';
        $data['status_filter'] = $this->input->post('status_filter') != '' ? $this->input->post('status_filter') : '';

        $data['due_date'] = $this->input->post('due_date') != '' ? $this->input->post('due_date') : '';

        $this->session->set_userdata('report_filter_order_payment', $data);

        redirect($this->controller_path . '/index/3');
    }

    public function clear_order_payment() {
        $this->session->unset_userdata('report_filter_order_payment');
        $this->session->unset_userdata('dt_report_order_payment');
        redirect($this->controller_path . '/index/3');
    }

    public function clear_report_comission() {
        $this->session->unset_userdata('dt_package_load');
        $this->session->unset_userdata('dt_package_drop');
        $this->session->unset_userdata('dt_cms_branch_origin');
        $this->session->unset_userdata('dt_cms_branch_destination');
        redirect($this->controller_path);
    }

    // Get total comission
    public function get_total_comission($tab = '') {

        // Variable initialization
        $data = array();

        switch ($tab) {
            case 1 :
                // Load model sale comission branch origin needed
                $this->load->model('report/sale_comission_branch_origin');
                $data = $this->sale_comission_branch_origin->get_sum();
                break;
            case 2 :
                // Load model sale comission package load needed
                $this->load->model('report/sale_comission_package_load');
                $data = $this->sale_comission_package_load->get_sum();
                break;
            case 3 :
                // Load model sale comission package drop needed
                $this->load->model('report/sale_comission_package_drop');
                $data = $this->sale_comission_package_drop->get_sum();
                break;
            case 4 :
                // Load model sale comission branch destination needed
                $this->load->model('report/sale_comission_branch_destination');
                $data = $this->sale_comission_branch_destination->get_sum();
                break;
        }

        echo json_encode($data);
        
    }

    // Get total omset
    public function get_total_omzet($tab = '') {
        
    }

    // List download function
    public function download_comission($type = '') {

        // Variable Initializtion
        $title = '';
        $data = null;

        // Load resource needed
        $this->load->helper('report');

        switch ($type) {
            case 1 :
                // Load model sale comission branch origin needed
                $this->load->model('report/sale_comission_branch_origin');
                $data = $this->sale_comission_branch_origin->download();
                $title = 'Cabang Asal';
                break;
            case 2 :
                // Load model sale comission package load needed
                $this->load->model('report/sale_comission_package_load');
                $data = $this->sale_comission_package_load->download();
                $title = 'Paket Naik';
                break;
            case 3 :
                // Load model sale comission package drop needed
                $this->load->model('report/sale_comission_package_drop');
                $data = $this->sale_comission_package_drop->download();
                $title = 'Paket Turun';
                break;
            case 4 :
                // Load model sale comission branch destination needed
                $this->load->model('report/sale_comission_branch_destination');
                $data = $this->sale_comission_branch_destination->download();
                $title = 'Cabang Tujuan ';
                break;
        }

        // return data as excel file
        download_report($data, 'Laporan Komisi ' . $title);
    }

    // Download payment order
    public function download_payment() {

        // Variable Initializtion
        $title = '';
        $data = null;

        // Load model sale comission branch destination needed
        $this->load->model('report_order_payment_model');
        $data = $this->report_order_payment_model->download();

        // Load resource needed
        $this->load->helper('report');

        // return data as excel file
        download_report($data, 'Laporan Status Pembayaran');
    }

}
