<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_customers_detail extends CI_Controller {

    private $data = [];
    public $id;
    private $view_path = 'report_customers_detail';
    public $controller_path = 'report/report_customers_detail';

    public function __construct() {

        parent::__construct();
        if ($this->session->userdata('logged')) {
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index($costumer_id = 0) {
        if ($this->input->is_ajax_request()) {

            // Load all reousurce needed 
            $this->load->library('datatable/dt_report_customers_detail');

            echo $this->dt_report_customers_detail->get($this, $_REQUEST, $costumer_id);
        } else {

            // Load all reousurce needed 
            $this->load->helper('cities');
            $this->load->model('costumers_model');

            $this->data['title'] = "Detail Laporan Costumer ";
            $this->data['data_customer'] = $this->costumers_model->get_joined(['customers.customer_id' => $costumer_id])->row();
            $this->data['customers_id'] = $costumer_id;

            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function filter($customer_id = 0) {

        $this->load->model('branches_model');
        
        $data['date_start'] = $this->input->post('start_date') != '' ? strtotime(str_replace('/', '-', $this->input->post('start_date'))) : '';
        $data['date_end'] = $this->input->post('end_date') != '' ? strtotime(str_replace('/', '-', $this->input->post('end_date'))) : '';
        
        $data['branch_origin_id'] = $this->input->post('branch_origin');
        $data['branch_origin_name'] = $this->branches_model
                ->get_branch_name($this->input->post('branch_origin'));

        $data['branch_destination_id'] = $this->input->post('branch_destination');
        $data['branch_destination_name'] = $this->branches_model
                ->get_branch_name($this->input->post('branch_destination'));

        $this->session->set_userdata('customers_report_detail_filter', $data);

        redirect($this->controller_path . '/index/' . $customer_id);
        
    }

    public function clear($customer_id = 0) {
        $this->session->unset_userdata('customers_report_detail_filter');
        redirect($this->controller_path . '/index/' . $customer_id);
    }
    
    public function download($customer_id = 0){
        
         // Load all resource needed
        $this->load->model('customers_transaction_detail_model');
        $this->load->helper('report');

        $data = $this->customers_transaction_detail_model->get_download($customer_id);
        download_report($data, 'Laporan Detail Transaksi Pelanggan');
        
    }

}
