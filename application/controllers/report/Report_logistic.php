<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_logistic extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_logistic';
    public $controller_path = 'report/report_logistic';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

             $this->load->model('branches_model');
             $this->load->model('report_logistic_model');
             $this->load->model('armadas_model');

             $this->load->library('datatable/dt_report_logistic_cities');
             $this->load->library('datatable/dt_report_logistic_armadas');
             $this->load->library('datatable/dt_report_logistic_deliveries_1');
             $this->load->library('datatable/dt_report_logistic_deliveries_2');
             $this->load->library('datatable/dt_report_logistic_deliveries_3');
             $this->load->library('datatable/dt_report_logistic_pickup_orders');

             $this->data['path'] = $this->controller_path;
             $this->data['title2'] = "";
             $this->data['active_menu'] = "m1";
             $this->data['cities'] = $this->branches_model->get_joined_cities()->result_array();
        } else {
            redirect('program/login');
        }
    }

    public function index($id = 0) {
        $this->data['title'] = "Laporan Logistik";
        if ($this->input->is_ajax_request()) {

            if (!empty($id)) {
                $status = $id;
            } 

            if ($status == 1) {
                echo $this->dt_report_logistic_cities->get($this, $_REQUEST);
            } else if ($status == 2){
                echo $this->dt_report_logistic_armadas->get($this, $_REQUEST); 
            } else if ($status == 3) {
                echo $this->dt_report_logistic_deliveries_1->get($this, $_REQUEST);
            } else if ($status == 4) {
                echo $this->dt_report_logistic_deliveries_2->get($this, $_REQUEST);
            } else if ($status == 5) {
                echo $this->dt_report_logistic_deliveries_3->get($this, $_REQUEST);
            } else if ($status == 6) {
                echo $this->dt_report_logistic_pickup_orders->get($this, $_REQUEST);
            } 

        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function view_logistic_cities($id = '')
    {
        $this->data['title'] = "Laporan Logistik > Muatan Per Kota > Detail Order";
        $this->data['order'] = $this->report_logistic_model->view_logistic_cities(['city_id' => $id])->result();
        $this->load->view($this->view_path . '/tab_cities_view', $this->data);
    }

    public function view_logistic_armadas($id = '')
    {
        $this->data['title'] = "Laporan Logistik > Muatan Per Armada ";
        $this->data['order'] = $this->report_logistic_model->view_logistic_armadas(['report_logistic_packages.armadatrip_armada_id' => $id])->result();
        $this->data['data_armadas']    = $this->armadas_model->get_joined(['armadas.armada_id' => $id])->row();
        $this->load->view($this->view_path . '/tab_armadas_view', $this->data);
    }


}
