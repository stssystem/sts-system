<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_tax_detail extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_tax_detail';
    public $controller_path = 'report/report_tax_detail';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "report_tax_detail";
        } else {
            redirect('program/login');
        }
    }

    public function index($order_origin = '') {

        if ($this->input->is_ajax_request()) {

            $this->load->library('datatable/dt_report_tax_detail');
            echo $this->dt_report_tax_detail->get($this, $order_origin, $_REQUEST);
        } else {

            /** Load all resource needed */
            $this->data['title'] = 'Laporan Pajak Detail';
            $this->data['order_origin'] = $order_origin;

            // Set session tax_report_filter
            if (!isset($this->session->userdata('tax_report_detail_filter')['date_start'])) {
                $data['date_start'] = $this->session->userdata('tax_report_filter')['date_start'];
            }

            if (!isset($this->session->userdata('tax_report_detail_filter')['date_end'])) {
                $data['date_end'] = $this->session->userdata('tax_report_filter')['date_end'];
            }

            if (!isset($this->session->userdata('tax_report_detail_filter')['order_origin'])) {
                $data['order_origin'] = $order_origin;
                $this->session->set_userdata('tax_report_detail_filter', $data);
            }

            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
            
        }
    }

    public function filter($order_origin = '') {

        $data = array();
        $data['date_start'] = str_replace('/', '-', $this->input->post('date_start_filter'));
        $data['date_end'] = str_replace('/', '-', $this->input->post('date_end_filter'));

        if ($this->input->post('branch_filter') != '') {
            $data['order_origin'] = $this->input->post('branch_filter');
        } else {
            $data['order_origin'] = $order_origin;
        }

        $this->session->set_userdata('tax_report_detail_filter', $data);

        redirect($this->controller_path . '/index/' . $data['order_origin']);
    }

    public function clear($order_origin) {
        $this->session->unset_userdata('tax_report_detail_filter');
        $this->session->unset_userdata('dt_report_tax_detail');
        redirect($this->controller_path . '/index/' . $order_origin);
    }

    public function get_total($order_origin = '') {
        $this->load->model('report_tax_detail_model');
        $result = $this->report_tax_detail_model->get_sum($order_origin);
        $result['dpp'] = number_format(round($result['dpp'], 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
        $result['ppn'] = number_format(round($result['ppn'], 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
        $result['total'] = number_format(round($result['total'], 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
        echo json_encode($result);
    }

    public function download($order_origin = '') {
        
        // Load all resource needed
        $this->load->model('report_tax_detail_model');
        $this->load->helper('report');

        $data = $this->report_tax_detail_model->get_download($order_origin);
        $this->session->set_userdata('f', $this->db->last_query());
        download_report($data, 'Laporan Detail Pajak');
        
    }

}
