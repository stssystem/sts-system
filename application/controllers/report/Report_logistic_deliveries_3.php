<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_logistic_deliveries_3 extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_logistic/deliveries_cargo';
    public $controller_path = 'report/report_logistic_deliveries_3';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('report_logistic_model');
            $this->load->model('report_logistic_deliveries_3_model');
            $this->load->model('branches_model');

            $this->data['path']         = $this->controller_path;
            $this->data['title2']        = "";
            $this->data['active_menu']  = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index($branch_id = 0) {
        if ($this->input->is_ajax_request()) {
            $this->load->library('datatable/dt_report_logistic_deliveries_3_detail');
            echo $this->dt_report_logistic_deliveries_3_detail->get($this, $_REQUEST, $branch_id);
        } else {

            $this->data['title']            = "Detail Laporan Logistik ";
            $this->data['data_branches']    = $this->branches_model->get_joined(['branches.branch_id' => $branch_id])->row();
            $this->data['value_branches']   = $this->report_logistic_model->get_logistic_deliveries_3(['branch_id' => $branch_id])->row();
            $this->data['branch_id']        = $branch_id;

            $this->load->view($this->view_path . '/index_3', $this->data);
        }
    }

    public function filter($branch_id = 0) {

        $data['date_start']         = $this->input->post('date_start') != '' ? $this->input->post('date_start') : '';
        $data['date_end']           = $this->input->post('date_end') != '' ? $this->input->post('date_end') : '';

        $this->session->set_userdata('deliveries_3', $data);

        redirect($this->controller_path . '/index/' . $branch_id);
    }

    public function clear($branch_id = 0) {
        $this->session->unset_userdata('deliveries_3');
        $this->session->unset_userdata('dt_report_logistic_armadas_3_detail');
        redirect($this->controller_path . '/index/' . $branch_id);
    } 

}
