<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_filter extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_filter';
    public $controller_path = 'report/report_filter';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index() {
        $this->data['title'] = "Filter Download Laporan";
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function export()
    {
        $type       = $this->input->post('type-export');
        $report     = $this->input->post('filter-report');
        $column     = ($this->input->post('column-export') ? $this->input->post('column-export') : array());
        $date_start = $this->input->post('date_start') != '' ? $this->input->post('date_start') : '';
        $date_end   = $this->input->post('date_end') != '' ? $this->input->post('date_end') : '';

        if ($type == 1) {
            
            # export PDF
            set_alert('alert-danger', 'Format file download belum tersedia !');

        } else if ($type == 2) {
            
            # export EXCEL
            set_alert('alert-danger', 'Format file download belum tersedia !');

            // if (!empty($column)) {
            //     $column_export = $this->get_column($report, $column);
            // }
            // if (!empty($report)) {
            //     $data = $this->get_value($report, $column_export)->result_array();
            // }

            // if (!empty($data)) {
            //     $this->export_XLS($data);
            //     set_alert('alert-success', 'Download sukses.');
            // } else {
            //     set_alert('alert-danger', 'Download gagal.');
            // } 


        } else if ($type == 3) {

            # export CSV
            if (!empty($column)) {
                $column_export = $this->get_column($report, $column);
            }
            if (!empty($report)) {
                $data = $this->get_value($report, $column_export, $date_start, $date_end);
            }
            if (!empty($data)) {
                $this->export_CSV($data);
                set_alert('alert-success', 'Download sukses.');
            } else {
                set_alert('alert-danger', 'Download gagal.');
            } 

        } else {
            set_alert('alert-warning', 'Pilih data yang akan di export terlebih dahulu !');
        }
        
        return redirect('report/report_filter', 'refresh');    
    }

    public function get_value($report, $column_export, $date_start, $date_end)
    {
        $data = [];
        
        $this->load->model('finance_report_model');
        $this->load->model('customers_transaction_model');
        $this->load->model('report_sale_model');
        $this->load->model('report_order_payment_model');
        $this->load->model('report_logistic_model');
        $this->load->model('pickup_orders_model');
        $this->load->model('report_gps_model');
        
        if ($report == '1') {
            $data = $this->finance_report_model->get_data(array(), '', '', '', $column_export, $date_start, $date_end);
        } else if ($report == '2') {
            $data = $this->customers_transaction_model->get_data(array(), '', '', '', $column_export, $date_start, $date_end);
        } else if ($report == '3a') {
            $data = $this->report_sale_model->get_branch_origin(array(), array(), $column_export);
        } else if ($report == '3b') {
            $data = $this->report_sale_model->get_comission_up(array(), array(), $column_export);
        } else if ($report == '3c') {
            $data = $this->report_sale_model->get_comission_down(array(), array(), $column_export);
        } else if ($report == '3d') {
            $data = $this->report_sale_model->get_branch_destination(array(), array(), $column_export);
        } else if ($report == '4') {
            $data = $this->report_order_payment_model->get(array(), array(), array(), $column_export, $date_start, $date_end);
        } else if ($report == '5') {
            $data = $this->report_logistic_model->get_logistic_cities(array(), array(), $column_export);
        } else if ($report == '6') {
            $data = $this->report_logistic_model->get_logistic_armadas(array(), array(), $column_export);
        } else if ($report == '7a') {
            $data = $this->report_logistic_model->get_logistic_deliveries_1(array(), array(), $column_export);
        } else if ($report == '7b') {
            $data = $this->report_logistic_model->get_logistic_deliveries_2(array(), array(), $column_export);
        } else if ($report == '7c') {
            $data = $this->report_logistic_model->get_logistic_deliveries_3(array(), array(), $column_export);
        } else if ($report == '7d') {
            $data = $this->pickup_orders_model->get_report_logistik_pickup_orders(array(), array(), $column_export);
        } else if ($report == '8') {
            $data = $this->report_gps_model->get_data(array(), '', '', '', $column_export);
        }

        return $data;
    }

    public function get_column($report, $column)
    {

        if ($report == '1') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'finance_report.order_id AS ID_Order';
                } else if ($value == 2) {
                    $column_2 = 'FROM_UNIXTIME(finance_report.order_date, "%d %M %Y") AS Tanggal_Order';
                } else if ($value == 3) {
                    $column_3 = 'finance_report.customer_name AS Nama_Pelanggan';
                } else if ($value == 4) {
                    $column_4 = 'finance_report.branch_name_origin as Cabang/Agen_Asal';
                } else if ($value == 5) {
                    $column_5 = 'finance_report.branch_name_destination as Cabang/Agen_Tujuan';
                } else if ($value == 6) {
                    $column_6 = 'finance_report.order_sell_total AS Total';
                } else if ($value == 7) {
                    $column_7 = 'finance_report.extra_total as Total_Ekstra';
                } else if ($value == 8) {
                    $column_8 = 'IF(finance_report.order_status = 0, "Baru_Diterima", 
                                    IF(finance_report.order_status = 1, "Dalam_Proses", 
                                        IF(finance_report.order_status = 2, "Selesai", 
                                            IF(finance_report.order_status = 3, "Belum_Bayar", "Akan_Sampai")
                                            )
                                        )
                                    ) AS Status';
                }
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;
            $column_4 = (!empty($column_4)) ? $column_4 : '' ;
            $column_5 = (!empty($column_5)) ? $column_5 : '' ;
            $column_6 = (!empty($column_6)) ? $column_6 : '' ;
            $column_7 = (!empty($column_7)) ? $column_7 : '' ;
            $column_8 = (!empty($column_8)) ? $column_8 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3, $column_4, $column_5, $column_6, $column_7, $column_8);

        } else if ($report == '2') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'customers_transaction_report.customer_id AS ID_Customer';
                } else if ($value == 2) {
                    $column_2 = 'customers_transaction_report.customer_name AS Nama_Customer';
                } else if ($value == 3) {
                    $column_3 = 'customers_transaction_report.city_name AS Kota_Asal';
                } else if ($value == 4) {
                    $column_4 = 'customers_transaction_report.branch_name AS Cabang_Asal';
                } else if ($value == 5) {
                    $column_5 = 'customers_transaction_report.customer_referral AS No_Referal';
                } else if ($value == 6) {
                    $column_6 = 'customers_transaction_report.total_transaction AS Jumlah_Transaksi';
                } else if ($value == 7) {
                    $column_7 = 'FROM_UNIXTIME(customers_transaction_report.last_transaction, "%d %M %Y - %h:%i:%s") AS Tanggal_Transaksi_Terakhir';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;
            $column_4 = (!empty($column_4)) ? $column_4 : '' ;
            $column_5 = (!empty($column_5)) ? $column_5 : '' ;
            $column_6 = (!empty($column_6)) ? $column_6 : '' ;
            $column_7 = (!empty($column_7)) ? $column_7 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3, $column_4, $column_5, $column_6, $column_7);

        } else if ($report == '3a') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'branches.branch_id AS ID Cabang';
                } else if ($value == 2) {
                    $column_2 = 'branches.branch_name AS Nama Cabang';
                } else if ($value == 3) {
                    $column_3 = 'sale_comission_branch_origin.total_order AS Total Order';
                } else if ($value == 4) {
                    $column_4 = 'sale_comission_branch_origin.total_omset AS Total Omset';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;
            $column_4 = (!empty($column_4)) ? $column_4 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3, $column_4);

        } else if ($report == '3b') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'branches.branch_id AS ID Cabang';
                } else if ($value == 2) {
                    $column_2 = 'branches.branch_name AS Nama Cabang';
                } else if ($value == 3) {
                    $column_3 = 'sale_comission_report_up.total_order AS Total Order';
                } else if ($value == 4) {
                    $column_4 = 'sale_comission_report_up.total_omset AS Total Omset';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;
            $column_4 = (!empty($column_4)) ? $column_4 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3, $column_4);

        } else if ($report == '3c') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'branches.branch_id AS ID Cabang';
                } else if ($value == 2) {
                    $column_2 = 'branches.branch_name AS Nama Cabang';
                } else if ($value == 3) {
                    $column_3 = 'sale_comission_report_down.total_order AS Total Order';
                } else if ($value == 4) {
                    $column_4 = 'sale_comission_report_down.total_omset AS Total Omset';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;
            $column_4 = (!empty($column_4)) ? $column_4 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3, $column_4);

        } else if ($report == '3d') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'branches.branch_id AS ID Cabang';
                } else if ($value == 2) {
                    $column_2 = 'branches.branch_name AS Nama Cabang';
                } else if ($value == 3) {
                    $column_3 = 'sale_comission_branch_destination.total_order AS Total Order';
                } else if ($value == 4) {
                    $column_4 = 'sale_comission_branch_destination.total_omset AS Total Omset';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;
            $column_4 = (!empty($column_4)) ? $column_4 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3, $column_4);

        } else if ($report == '4') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'FROM_UNIXTIME(order_payment_report.order_date, "%d %M %Y") AS Tanggal_Transaksi';
                } else if ($value == 2) {
                    $column_2 = 'order_payment_report.order_id AS Nomor_Resi';
                } else if ($value == 3) {
                    $column_3 = 'order_payment_report.customer_name AS Nama_Pengirim';
                } else if ($value == 4) {
                    $column_4 = 'order_payment_report.branch_origin_name as Cabang/Agen_Asal';
                } else if ($value == 5) {
                    $column_5 = 'order_payment_report.branch_destination_name as Cabang/Agen_Tujuan';
                } else if ($value == 6) {
                    $column_6 = 'IF(order_payment_report.order_payment_type = 1, "Cash/Tunai", 
                                    IF(order_payment_report.order_payment_type = 2, "Franko", 
                                        IF(order_payment_report.order_payment_type = 3, "Tunai_Tempo", "Tagih_Temp")
                                        )
                                    ) AS Sistem_Pembayaran';
                } else if ($value == 7) {
                    $column_7 = 'FROM_UNIXTIME(order_payment_report.orders_due_date, "%d %M %Y - %h:%i:%s") as Tanggal_Jatuh_Tempo';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;
            $column_4 = (!empty($column_4)) ? $column_4 : '' ;
            $column_5 = (!empty($column_5)) ? $column_5 : '' ;
            $column_6 = (!empty($column_6)) ? $column_6 : '' ;
            $column_7 = (!empty($column_7)) ? $column_7 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3, $column_4, $column_5, $column_6, $column_7);

        } else if ($report == '5') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'report_logistic_cities_total.city_id AS ID Kota';
                } else if ($value == 2) {
                    $column_2 = 'report_logistic_cities_total.city_name AS Nama Kota';
                } else if ($value == 3) {
                    $column_3 = 'report_logistic_cities_total.total_weight AS Total Berat - kg';
                } else if ($value == 4) {
                    $column_4 = 'report_logistic_cities_total.total_size AS Total Volume - cm3';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;
            $column_4 = (!empty($column_4)) ? $column_4 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3, $column_4);

        } else if ($report == '6') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'report_logistic_armadas.armada_id AS ID_Armada';
                } else if ($value == 2) {
                    $column_2 = 'report_logistic_armadas.armada_name AS Nama_Armada';
                } else if ($value == 3) {
                    $column_3 = 'IF(report_logistic_armadas.armada_status = 1, "Aktif", "Tidak-Aktif") AS Status_Armada';
                } else if ($value == 4) {
                    $column_4 = 'report_logistic_armadas.start_name AS Asal';
                } else if ($value == 5) {
                    $column_5 = 'report_logistic_armadas.stop_name AS Tujuan';
                } else if ($value == 6) {
                    $column_6 = 'FROM_UNIXTIME(report_logistic_armadas.trip_start_date, "%d %M %Y - %h:%i:%s") AS Jadwal_Berangkat';
                } else if ($value == 7) {
                    $column_7 = 'FROM_UNIXTIME(report_logistic_armadas.trip_end_date, "%d %M %Y - %h:%i:%s") AS Jadwal_Tiba';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;
            $column_4 = (!empty($column_4)) ? $column_4 : '' ;
            $column_5 = (!empty($column_5)) ? $column_5 : '' ;
            $column_6 = (!empty($column_6)) ? $column_6 : '' ;
            $column_7 = (!empty($column_7)) ? $column_7 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3, $column_4, $column_5, $column_6, $column_7);

        } else if ($report == '7a') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'report_logistic_deliveries_1.branch_id_origin AS ID Cabang';
                } else if ($value == 2) {
                    $column_2 = 'report_logistic_deliveries_1.branch_name_origin AS Nama Cabang';
                } else if ($value == 3) {
                    $column_3 = 'report_logistic_deliveries_1.total_order AS Total Order';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3);

        } else if ($report == '7b') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'report_logistic_deliveries_2.branch_id AS ID Cabang';
                } else if ($value == 2) {
                    $column_2 = 'report_logistic_deliveries_2.branch_origin_name AS Nama Cabang';
                } else if ($value == 3) {
                    $column_3 = 'report_logistic_deliveries_2.total_order AS Total Order';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3);

        } else if ($report == '7c') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'report_logistic_deliveries_3.branch_id AS ID Cabang';
                } else if ($value == 2) {
                    $column_2 = 'report_logistic_deliveries_3.branch_origin_name AS Nama Cabang';
                } else if ($value == 3) {
                    $column_3 = 'report_logistic_deliveries_3.total_order AS Total Order';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3);

        } else if ($report == '7d') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'wp_ss_pick_up_order.asal AS Nama Cabang';
                } 
                // else if ($value == 2) {
                //     $column_2 = 'count(wp_ss_pick_up_order.no_resi) AS Total Order';
                // } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2);

        } else if ($report == '8') {

            foreach ($column as $key => $value) {
                if ($value == 1) {
                    $column_1 = 'armadas.armada_id AS armada_id';
                } else if ($value == 2) {
                    $column_2 = 'armadas.armada_name AS armada_name';
                } else if ($value == 3) {
                    $column_3 = 'armadas.armada_license_plate AS armada_license_plate';
                } else if ($value == 4) {
                    $column_4 = 'armadas.traccar_device_id AS traccar_device_id';
                } else if ($value == 5) {
                    $column_5 = 'armadas.armada_last_positin AS  armada_last_positin ';
                } 
            }

            $column_1 = (!empty($column_1)) ? $column_1 : '' ;
            $column_2 = (!empty($column_2)) ? $column_2 : '' ;
            $column_3 = (!empty($column_3)) ? $column_3 : '' ;
            $column_4 = (!empty($column_4)) ? $column_4 : '' ;
            $column_5 = (!empty($column_5)) ? $column_5 : '' ;

            $column_export = array();
            array_push($column_export, $column_1, $column_2, $column_3, $column_4, $column_5);

        }

        return $column_export;
    }

    public function export_PDF($report)
    {
        # code...
    }

    public function export_XLS($data)
    {   
        // $this->load->view('report_filter/xls_view', $data);
    }

    public function export_CSV($data)
    {
        $this->load->helper('download');
        $this->load->dbutil();
        $report     = $data;
        $delimiter  = ",";
        $newline    = "\r\n";
        $new_report = $this->dbutil->csv_from_result($report, $delimiter, $newline);
        $name       = 'laporan_'.date('Y-m-d h-i-s').'.csv';
        force_download($name, $new_report);
    }


}
