<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_logistic_pickup_orders extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_logistic/pickup_cargo';
    public $controller_path = 'report/report_logistic_pickup_orders';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('pickup_orders_model');
            $this->load->model('orders_model');
            $this->load->model('report_logistic_model');
            $this->load->model('report_logistic_pickup_orders_model');
            $this->load->model('branches_model');

            $this->data['path']         = $this->controller_path;
            $this->data['title2']        = "";
            $this->data['active_menu']  = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index($asal = 0) {
        if ($this->input->is_ajax_request()) {
            $this->load->library('datatable/dt_report_logistic_pickup_orders_detail');
            echo $this->dt_report_logistic_pickup_orders_detail->get($this, $_REQUEST, $asal);
        } else {

            $this->data['title']            = "Detail Laporan Logistik ";
            // $this->data['data_branches']    = $this->branches_model->get_joined(['branches.branch_id' => $branch_id])->row();
            $this->data['value_branches']   = $this->pickup_orders_model->get_report_logistik_pickup_orders(['asal' => $asal])->row();
            $this->data['asal']        = $asal;

            $this->load->view($this->view_path . '/index', $this->data);
        }
    }

    public function filter($asal = 0) {

        $data['date_start']         = $this->input->post('date_start') != '' ? $this->input->post('date_start') : '';
        $data['date_end']           = $this->input->post('date_end') != '' ? $this->input->post('date_end') : '';

        $this->session->set_userdata('report_pickup_orders', $data);

        redirect($this->controller_path . '/index/' . $asal);
    }

    public function clear($asal = 0) {
        $this->session->unset_userdata('report_pickup_orders');
        $this->session->unset_userdata('dt_report_pickup_orders_detail');
        redirect($this->controller_path . '/index/' . $asal);
    } 

}
