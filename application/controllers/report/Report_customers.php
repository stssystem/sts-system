<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_customers extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_customers';
    public $controller_path = 'report/report_customers';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";
            $this->profile = profile();
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        if ($this->input->is_ajax_request()) {
            $this->load->library('datatable/dt_report_customers');
            echo $this->dt_report_customers->get($this, $_REQUEST);
        } else {
            $this->load->helper('cities');
            $this->data['title'] = "Laporan Costumer";
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function filter() {

        $this->load->model('branches_model');
        $this->load->model('cities_model');

        $data['date_start'] = str_replace('/', '-', $this->input->post('start_date'));
        $data['date_end'] = str_replace('/', '-', $this->input->post('end_date'));

        $data['branch_id'] = $this->input->post('branches');
        $data['branch_name'] = $this->branches_model
                ->get_branch_name($this->input->post('branches'));

        $data['city_id'] = $this->input->post('cities');
        $data['city_name'] = $this->cities_model
                ->get_name($this->input->post('cities'));

        $this->session->set_userdata('customers_report_filter', $data);

        redirect($this->controller_path);
    }

    public function clear() {
        $this->session->unset_userdata('customers_report_filter');
        $this->session->unset_userdata('dt_report_customers');
        redirect($this->controller_path);
    }

    public function download() {

        // Load all resource needed
        $this->load->model('customers_transaction_model');
        $this->load->helper('report');

        $user_type_id = $this->profile->user_type_id;

        $user_regional = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }
        $user_branch = profile()->branch_id;

        if ($user_type_id == 99) { // super admin & Admin Pusat
            $where = [];
        } else if (profile()->regional_id != '') { // Regional
            $where = [
                'customer_regional_id' => $user_regional
            ];
        } else if (profile()->branch_id != '') { // Cabang
            $where = [
                'customer_branch_id' => $user_branch
            ];
        }

        $data = $this->customers_transaction_model->get_download($where);

        download_report($data, 'Laporan Transaksi Pelanggan');
        
    }

}
