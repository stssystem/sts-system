<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_order extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_order';
    public $controller_path = 'report/report_order';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        if ($this->input->is_ajax_request()) {

            $this->load->library('datatable/dt_report_order');
            echo $this->dt_report_order->get($this, $_REQUEST);
        } else {

            /** Load all resource needed */
            $this->load->model('branches_model');

            $this->data['title'] = "Laporan Keuangan";
            $this->data['cities'] = $this->branches_model->get_joined_cities()->result_array();

            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function filter() {
        $this->load->model('branches_model');
        $data['date_start'] = str_replace('/', '-', $this->input->post('date_start_filter'));
        $data['date_end'] = str_replace('/', '-', $this->input->post('date_end_filter'));
        $data['branch_origin'] = $this->input->post('origin_filter');
        $data['branch_origin_name'] = $this->branches_model->get_branch_name($this->input->post('origin_filter'));
        $data['branch_destination'] = $this->input->post('destination_filter');
        $data['branch_destination_name'] = $this->branches_model->get_branch_name($this->input->post('destination_filter'));
        $this->session->set_userdata('filter', $data);
        redirect($this->controller_path);
    }

    public function clear() {
        $this->session->unset_userdata('filter');
        $this->session->unset_userdata('dt_report_order');
        redirect($this->controller_path);
    }

    public function destroy($id = 0) {
        $this->load->model('orders_model');
        $data = $this->orders_model->delete(['order_id' => $id]);
        /** Todo : hapus semua data yang berkaitan dengan order */
        set_alert('alert-danger', 'Order dengan kode resi <strong>' . $data->row()->order_id . '</strong> telah dihapus');
        redirect($this->controller_path);
    }

    public function get_sum() {
        
        $this->load->model('finance_report_model');
        $temp = $this->finance_report_model->get_sum();
        
        $this->session->set_userdata('d', $this->db->last_query());
        
        $result = array();
        $result['total_sell'] = number_format($temp->order_sell_total, 0, ',', '.');
        $result['total_extra'] = number_format($temp->extra_total, 0, ',', '.');
        $result['total_sell_extra'] = number_format($temp->order_sell_total + $temp->extra_total, 0, ',', '.');

        echo json_encode($result);
        
    }
    
    public function download(){
        
        // Load all reource needed
        $this->load->model('finance_report_model');
        $this->load->helper('report');
        
        $data = $this->finance_report_model->get_download();
        download_report($data, 'keuangan');
        
    }

}
