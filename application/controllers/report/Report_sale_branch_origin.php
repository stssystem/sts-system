<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_sale_branch_origin extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_sale/branch_origin';
    public $controller_path = 'report/Report_sale_branch_origin';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('report_sale_model');
            $this->load->model('branches_model');

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index($branch_id = 0) {

        if ($this->input->is_ajax_request()) {
            $this->load->library('datatable/dt_report_sale_branch_origin_detail');
            echo $this->dt_report_sale_branch_origin_detail->get($this, $_REQUEST, $branch_id);
        } else {

            // Load model 
            $this->load->model('report/sale_comission_branch_origin', 'comission_branch_origin');

            $this->data['title'] = "Detail Laporan Komisi Agen ";
            $this->data['data_branches'] = $this->branches_model->get_joined(['branches.branch_id' => $branch_id])->row();
            $this->data['value_branches'] = $this->report_sale_model->get_value_branch_origin(['sale_comission_branch_origin.branch_id' => $branch_id])->row();
            $this->data['data_sum'] = $this->comission_branch_origin->get_sum($branch_id);
            $this->data['branch_id'] = $branch_id;

            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function filter($branch_id = 0) {

        $data['date_start'] = $this->input->post('date_start') != '' ? $this->input->post('date_start') : '';
        $data['date_end'] = $this->input->post('date_end') != '' ? $this->input->post('date_end') : '';

        $data['branch_destination'] = $this->input->post('destination_filter');
        $data['branch_destination_name'] = $this->branches_model->get_branch_name($this->input->post('destination_filter'));

        $this->session->set_userdata('filter_report_sale_branch_origin', $data);

        redirect($this->controller_path . '/index/' . $branch_id);
    }

    public function clear($branch_id = 0) {
        $this->session->unset_userdata('filter_report_sale_branch_origin');
        $this->session->unset_userdata('dt_report_sale_branch_origin_detail');
        redirect($this->controller_path . '/index/' . $branch_id);
    }

    // Download payment order
    public function download($branch_id = 0) {

        // Variable Initialization      
        $data = null;

        // Load model sale comission branch destination needed
        $this->load->model('report_sale_branch_origin_model');
        $data = $this->report_sale_branch_origin_model->download($branch_id);

        // Load resource needed
        $this->load->helper('report');

        // return data as excel file
        download_report($data, 'Laporan Detail Komisi Cabang / Agen');
    }

    /**
     * Get get total sum of omset and agen comission
     * 
     * @param string Json format
     */
    public function get_sum($branch_id = 0) {

        // Load report_sale_branch_origin_model model
        $this->load->model('report_sale_branch_origin_model', 'sale_branch_origin');

        // Get result from model
        $result = $this->sale_branch_origin->get_sum($branch_id);

        // Send result
        echo json_encode($result);
    }

}
