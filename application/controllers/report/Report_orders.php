<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_orders extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'report_order';
    public $controller_path = 'report/report_orders';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_cities";
            $this->id = $this->input->post('city_id');

            $this->load->model('orders_model');
            $this->load->library('datatable/dt_report_orders');
        } else {
            redirect('program/login');
        }
    }

    public function index($year = 0) {
        
        $this->data['title'] = "Laporan Order";

        if ($this->input->is_ajax_request()) {
            echo $this->dt_report_orders->get($year);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add() {
        $this->data['title'] = "Tambah Data Kota";
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store() {

        if (!$this->request_cities->validation()) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            /** Cities create */
            $set_array = array(
                'city_name' => $this->input->post('city_name'),
                'created_at' => time(),
            );

            $this->cities_model->insert($set_array);

            set_alert('alert-success', 'Data kota <strong>' . $this->input->post('city_name') . '</strong> berhasil ditambahkan');

            redirect($this->controller_path, 'refresh');
        }
    }

    public function edit($id = 0) {

        $this->data['title'] = "Edit Data Kota";
        $this->data['data'] = $this->cities_model->get(['city_id' => $id])->row();

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update() {

        if (!$this->request_cities->validation()) {
            redirect($this->controller_path . '/edit/' . $this->id, 'refresh');
        } else {

            $where_array = array(
                'city_id' => $this->id,
                'city_name' => $this->input->post('city_name'),
                'updated_at' => time()
            );

            $this->cities_model->update($where_array);

            set_alert('alert-success', 'Data kota <strong>' . $this->input->post('city_name') . '</strong> telah diperbarui');

            redirect($this->controller_path);
        }
    }

    public function destroy($id = null) {
        $data = $this->cities_model->delete(['city_id' => $id]);
        set_alert('alert-danger', 'Data kota <strong>' . $data->city_name . '</strong> telah dihapus');
        redirect($this->controller_path);
    }

}
