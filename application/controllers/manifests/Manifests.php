<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manifests extends CI_Controller {

    var $data = array();
    private $view_path = 'manifests';
    public $controller_path = 'manifests/manifests';

    public function __construct() {

        parent::__construct();
        $this->load->model('packages_model');
        $this->load->model('branches_model');
        $this->load->model('trips_model');
        $this->load->model('pre_manifest_model');
        $this->load->model('manifest_model');
        $this->data['title2'] = "";
        $this->data['active_menu'] = "manifesting";
        $this->data['path'] = $this->controller_path;
    }

    public function index() {
        $this->data['title'] = "Generate Daftar Paket";
        $this->data['data'] = $this->packages_model->get_joined(['package_status' => 1])->result();
        $this->data['last_generate'] = mdate('%d-%m-%Y pukul %H:%i', $this->pre_manifest_model->get_last_generate());
        $this->load->view($this->view_path . '/index', $this->data);
    }

    public function generate_result() {

        $this->data['title'] = "Daftar Paket";

        /**
         *  Check from package table, wheter the table is empty or not.
         *  when the table not emtpy then do it 
         */
        $package = $this->packages_model->get(
                        [ 'package_status' => 1]
                )->result();

        if (count($package) == 0) {
            set_alert('alert-danger', "<strong><i class='fa fa-warning'></i> Perhatian!</strong> Daftar Paket tidak dapat dihasilkan karena tidak ada data paket");
            redirect($this->controller_path);
        }

        /** Get data from branch table */
        $branch = $this->branches_model->get(null, ['select' => 'branch_id'])->result();

        /** Update pre_manifest_status from 1 to 2  */
        $this->pre_manifest_model->update(
                ['pre_manifest_status' => 2], ['pre_manifest_status' => 1]
        );

        if (!empty($branch)) {
            foreach ($branch as $key => $value) {

                /** Get data from package rely on branch id */
                $package = $this->packages_model->get(
                                ['package_origin' => $value->branch_id, 'package_status' => 1], ['order' => ['package_destination' => 'ASC']]
                        )->result();

                /** Insert package data into pre-manifest table */
                if (!empty($package)) {
                    foreach ($package as $key_i => $value_i) {

                        $data = [
                            'pre_manifest_date' => time(),
                            'pre_manifest_exdate' => time() + 86400,
                            'pre_manifest_created_by' => profile()->user_id,
                            'pre_manifest_origin' => $value_i->package_origin,
                            'pre_manifest_destination' => $value_i->package_destination,
                            'pre_manifest_status' => 1,
                            'pre_manifest_package_id' => $value_i->package_id,
                            'created_at' => time()
                        ];

                        $pre_manifest_id = $this->pre_manifest_model->insert($data);

                        /** Update package_pre_manifest_id on packages table */
                        $this->packages_model->update([
                            'package_id' => $value_i->package_id,
                            'package_pre_manifest_id' => $pre_manifest_id
                        ]);
                    }
                }
            }
        }


        /** Update package status from 1 to 2  */
        $this->packages_model->update(
                ['package_status' => 2], ['package_status' => 1]
        );

        redirect($this->controller_path . '/pre_manifest_list');
    }

    public function show_package_per_pre($origin = 0, $destination = 0) {

        $where = [
            'pre_manifest_status' => 1,
            'pre_manifest_origin' => $origin,
        ];

        if ($destination != 0) {
            $where['pre_manifest_destination'] = $destination;
        }

        $result = $this->pre_manifest_model->get_joined($where);

        $this->data['data'] = $result->result();

        if (count($this->data['data']) == 0) {
            redirect($this->controller_path . '/pre_manifest_list', 'refresh');
        }

        $this->data['origin'] = $origin;

        if ($destination != 0) {
            $result = $result->row();
            $this->data['rute'] = "{$result->city_name_origin} - {$result->city_name_destination}";
            $this->data['destination'] = $destination;
        } else {
            $info = $this->helper_get_destination($result);
            $this->data['rute'] = "<br/> {$info['origin']} - ({$info['destination']})";
        }

        $this->data['title'] = "<p>Daftar Data Paket Rute : {$this->data['rute']}</p>";

        $this->load->view($this->view_path . '/package_list', $this->data);
    }

    public function pre_manifest_list() {

        $this->data['title'] = "Daftar Paket";
        $this->data['data'] = $this->pre_manifest_model->get_pre_manifest()->result();
        $this->data['last_generate'] = mdate('%d-%m-%Y pukul %H:%i', $this->pre_manifest_model->get_last_generate());

        $this->load->view($this->view_path . '/pre_manifest_list', $this->data);
    }

    public function manifest() {

        /** Catch data from request */
        $packages = $this->input->post('package_id');
        $pre_manifests = $this->input->post('pre_manifest_id');
        $package_size = $this->input->post('package_size');
        $package_weight = $this->input->post('package_weight');

        $trips = $this->input->post('armada_id');
        $origins = $this->input->post('origin_branch_id');
        $destinations = $this->input->post('destination_branch_id');

        /** Grouping by destination and armada ( as trips ) */
        $manifests = [];
        if (!empty($destinations)) {
            foreach ($destinations as $key => $value) {

                $needs = [
                    'origin' => $origins[$key],
                    'destination' => $value,
                    'trip_id' => $trips[$key],
                ];

                if (!in_array($needs, $manifests)) {
                    $manifests[$key]['origin'] = $origins[$key];
                    $manifests[$key]['destination'] = $value;
                    $manifests[$key]['trip_id'] = $trips[$key];
                }
            }
        }

        /** Break down manifest posibility */
        if (!empty($manifests)) {
            foreach ($manifests as $key => $value) {

                /** Insert manifest into table */
                $armada = $this->trips_model->get(['trip_id' => $value['trip_id']])->row()->armada_id;

                $row = [
                    'manifest_date' => time(),
                    'manifest_origin' => $value['origin'],
                    'manifest_destination' => $value['destination'],
                    'manifest_trip_id' => $value['trip_id'],
                    'manifest_armada_id' => $armada,
                    'manifest_created_by' => profile()->user_id,
                    'manifest_status' => 1,
                    'created_at' => time()
                ];

                $manifest_id = $this->manifest_model->insert($row);

                /** Trips paramater  */
                $trip_loaded_volume = 0;
                $trip_loaded_weight = 0;

                /** Break down pre-manifest and packages */
                if (!empty($pre_manifests)) {
                    foreach ($pre_manifests as $k => $v) {
                        if ($value['trip_id'] == $trips[$k] && $value['origin'] == $origins[$k] && $value['destination'] == $destinations[$k]) {

                            /** Update Pre-Manifest */
                            $this->pre_manifest_model->update([
                                'pre_manifest_id' => $v,
                                'pre_manifest_manifest_id' => $manifest_id,
                                'pre_manifest_status' => 2
                            ]);

                            /** Update Package  */
                            $this->packages_model->update([
                                'package_id' => $packages[$k],
                                'package_manifest_id' => $manifest_id,
                                'package_status' => 3
                            ]);

                            /** Calculate loaded volume and weight */
                            $trip_loaded_volume += $package_size[$k];
                            $trip_loaded_weight += $package_weight[$k];
                            
                        }
                    }
                }

                /** Update load volume and load weight in trips table */
                $this->trips_model->update([
                    'trip_id' => $value['trip_id'],
                    'trips_loaded_weight' => $trip_loaded_weight,
                    'trips_loaded_volume' => $trip_loaded_volume
                ]);
                
            }
        }
        
        /** Update status order */
        $this->load->model('det_orders_model');
        $order_id = $this->det_orders_model->get_order_id($packages);
        orders_update($order_id, 1);

        set_alert('alert-success', 'Manifest berhasil dibuat');

        if ($this->input->post('destination') != '') {
            redirect($this->controller_path . '/show_package_per_pre/' .
                    $this->input->post('origin') . '/' . $this->input->post('destination'), 'refresh'
            );
        } else {
            redirect($this->controller_path . '/show_package_per_pre/' . $this->input->post('origin'), 'refresh');
        }
    }

    public function helper_get_destination($object) {

        /** Generate list of destination city */
        $result['destination'] = '';
        $temp_array = [];
        if (!empty($object->result())) {
            foreach ($object->result() as $key => $value) {
                if (!in_array($value->destination_branch_city, $temp_array)) {
                    $result['destination'] .= $value->city_name_destination . ', ';
                    $temp_array[] = $value->destination_branch_city;
                }
            }
        }
        $result['destination'] = rtrim($result['destination'], ', ');

        /** Get destination list  */
        $result['destination_list'] = $temp_array;

        /** Get orgin city */
        $result['origin'] = $object->row()->city_name_origin;

        return $result;
    }

}
