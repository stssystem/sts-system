<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manifest extends CI_Controller {

    var $data = array();
    private $view_path = 'manifest';
    public $controller_path = 'manifests/manifest';

    public function __construct() {

        parent::__construct();

        $this->data['title2'] = "";
        $this->data['active_menu'] = "paket-ops";
        $this->data['path'] = $this->controller_path;

        $this->load->model('manifest_model');
        $this->load->model('packages_model');
    }

    public function index() {
        $this->data['title'] = "Loading Manifest";
        $this->data['data'] = $this->manifest_model->get_joined(['manifest.manifest_status' => 1])->result();
        $this->load->view($this->view_path . '/index', $this->data);
    }

    public function view($id = 0) {

        $this->data['title'] = "Manifest " . $id;

        $this->data['data'] = $this->packages_model->get(
                        ['package_manifest_id' => $id])->result();

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function printout() {
        $this->data['data'] = $this->build_printout();
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    private function build_printout() {

        $array = $this->manifest_model->get_joined(['manifest.manifest_status' => 1])->result();

        $result = [];
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                $result[$key]['manifest'] = $value;
                $result[$key]['manifest_detail'] = $this->packages_model->get_joined(['package_manifest_id' => $value->manifest_id])->result();
            }
        }

        return $result;
    }

}
