<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Packages extends CI_Controller {

    var $data = array();
    private $view_path = 'packages';
    public $controller_path = 'packages/packages';

    public function __construct() {
        parent::__construct();
        $this->data['title2'] = "";
        $this->data['active_menu'] = "paket-ops";
        $this->data['path'] = $this->controller_path;
        $this->load->model('packages_model');
        $this->load->model('orders_model');
        $this->load->model('armada_trips_model');

    }

    public function index() {
        $this->data['title'] = "Loading";
        $this->load->view($this->view_path . '/index', $this->data);

    }

    public function manifest_detail() {
        $this->data['title'] = "Manifest 001";
        $this->load->view($this->view_path . '/manifest_detail', $this->data);

    }

    /**
     * Load view of package loading
     */
    public function loading_barang() {

        // Reset session 
        $this->session->set_userdata('order_session', array());
        $this->session->set_userdata('package_session', array());

        // Fill data 
        $this->data['title'] = "Loading Barang";

        // Render view
        $this->load->view('manifests/loading_barang', $this->data);

    }

    /**
     * Process load up per package
     */
    public function process_loadup() {

        /** Get value of unloading type form form */
        $unloading_type = $this->input->post('unloading_type');

        /** Check whether the package is still in warehouse */
        $where = [];
        $set_warning = '';
        if ($unloading_type == 'true') {
            $set_warning = '&nbsp;<span class="label label-warning"><i class="fa fa-warning"></i> Paket ini tidak ditemukan di daftar barang transit</span>';
            #$where['package_status'] = 6; #deactive, until condition stable
        } else {
            $set_warning = '&nbsp;<span class="label label-warning"><i class="fa fa-warning"></i> Paket ini tidak ditemukan di daftar barang siap angkut</span>';
            #$where['package_status'] = 3; #deactive, until condition stable
        }

        // Get detail package
        $where['packages_view_detail.package_id'] = $this->input->post('package_id');
        $package = $this->packages_model->get_packages_view_detail($where)->row();

        // Get order id for the package
        $order_id_ = $this->packages_model->get_order_id($this->input->post('package_id'));

        if (isset($package) && !in_array($this->input->post('package_id'), $this->session->userdata('package_session')) && !in_array($order_id_, $this->session->userdata('order_session'))) {

            // Session insert 
            $check_session = $this->session->userdata('package_session');
            $check_session[] = $this->input->post('package_id');
            $this->session->set_userdata('package_session', $check_session);

            // Get all parent of package
            $parent_id = $this->packages_model->get_parents($this->input->post('package_id'));
            $all_child = $this->packages_model->get_all_child($parent_id);
            $input_list = '';
            foreach ($all_child as $key => $item) {
                $input_list .= "<input type='hidden' name='package_id[]' value='{$item->package_id}' />";
            }

            /** Return success message */
            $str = "<tr>"
                    . "<td>$package->package_id</td>"
                    . "<td>$package->package_title </td >"
                    . "<td width='10%'>"
                    . "<label class='label label-info' >Paket</label>"
                    . "</td>"
                    . "<td>"
                    . ((($package->package_status == 3 && $unloading_type != 'true' ) || ($package->package_status == 6 && $unloading_type == 'true')) ? '' : $set_warning)
                    . "</td>"
                    . "<td >"
                    . $package->origin_branch_name . " - " . $package->destination_branch_name
                    . "</td>"
                    . "<td>$package->qty koli</td>"
                    . "<td>$package->total_package_size m3</td>"
                    . "<td>$package->total_package_weight Kg</td>"
                    . "<td  class='actions' >"
                    . "<input type='hidden' name='package_session' value='" . $this->input->post('package_id') . "' /> "
                    . "<input type='hidden' name='order_session' value='' /> "
                    . $input_list
                    . "<div class='btn-group pull-right'>"
                    . "<button type='button' class='btn btn-danger btn-delete btn-sm'><i class='fa fa-times'></i></button>"
                    . "</div>"
                    . "</td>"
                    . "</tr>";

            echo $str;
        } else {

            $result = 'false';

            if (in_array($this->input->post('package_id'), $this->session->userdata('package_session')) || in_array($order_id_, $this->session->userdata('order_session'))) {
                $result = 'null';
            }

            echo $result;
        }

    }

    /**
     * Process load up per number resi
     */
    public function process_resi_number() {

        /** Get value of unloading type form form */
        $unloading_type = $this->input->post('unloading_type');

        /** Check whether the package is still in warehouse */
        $where = [];
        $set_warning = '';
        if ($unloading_type == 'true') {
            $set_warning = '&nbsp;<span class="label label-warning"><i class="fa fa-warning"></i> Paket ini tidak ditemukan di daftar barang transit</span>';
            #$where['package_status'] = 6; #deactive, until condition stabil
        } else {
            $set_warning = '&nbsp;<span class="label label-warning"><i class="fa fa-warning"></i> Paket ini tidak ditemukan di daftar barang siap angkut</span>';
            #$where['package_status'] = 3; #deactive, until condition stabil
        }

        // Check no resi manual
        $order_id_valid = $this->orders_model->get_order_id($this->input->post('order_id'));

        // Get detail order
        $where_order = ['orders.order_id' => $order_id_valid];
        $order_detail = $this->orders_model
                ->get_joined($where_order)
                ->row();

        // Get all package in the order
        $where['det_order.order_id'] = $order_id_valid;
        $package = $this->packages_model->get_joined($where)->result();

        /**
         * Break down package available to deliver, 
         * then listed it to success message purpose
         */
        if (!empty($order_detail) && !in_array($order_id_valid, $this->session->userdata('order_session'))) {

            // Variable initiation
            $str = '';
            $str_input = '';
            $total_koli = 0;
            $total_size = 0;
            $total_weight = 0;

            // Session insert 
            $check_session = $this->session->userdata('order_session');
            $check_session[] = $this->input->post('order_id');
            $this->session->set_userdata('order_session', $check_session);

            // Generate all package as input, total size, and total weight
            if (!empty($package)) {
                foreach ($package as $key => $item) {
                    $str_input .= "<input type='hidden' name='package_id[]' value='{$item->package_id}' />";
                    $total_koli++;
                    $total_size += $item->package_size;
                    $total_weight += $item->package_weight;
                }
            }

            $total_size = round($total_size / 1000000, 2);

            $str .= "<tr>"
                    . "<td>$order_detail->order_id</td>"
                    . "<td>$order_detail->customer_name</td >"
                    . "<td width='10%'>"
                    . "<label class='label label-success' >Resi</label>"
                    . "</td>"
                    . "<td ></td>"
                    . "<td >"
                    . $order_detail->branch_name_origin . " - " . $order_detail->branch_name_destination
                    . "</td>"
                    . "<td>{$total_koli} koli</td>"
                    . "<td>{$total_size} m3</td>"
                    . "<td>{$total_weight} Kg</td>"
                    . "<td class='actions'>"
                    . "<div class='btn-group pull-right'>"
                    . "<input type='hidden' name='package_session' value='' /> "
                    . "<input type='hidden' name='order_session' value='" . $this->input->post('order_id') . "' /> "
                    . $str_input
                    . "<button type='button' class='btn btn-danger btn-delete btn-sm'><i class='fa fa-times'></i></button>"
                    . "</div>"
                    . "</td>"
                    . "</tr>";

            echo $str;
        } else {

            $result = 'false';

            if (in_array($this->input->post('order_id'), $this->session->userdata('order_session'))) {
                $result = 'null';
            }

            echo $result;
        }

    }

    public function final_loading_proccess() {

        $data = $this->input->post('data');
        $trip_id = $this->input->post('trip');
        $temp_collection = [];

        if (!empty($data)) {
            foreach ($data as $key => $item) {

                if ($item['name'] == 'package_id[]') {
                    /** Update package status into 4 */
                    $this->packages_model->update([
                        'package_id' => $item['value'],
                        'package_status' => 4
                    ]);

                    /** Insert into armada trip */
                    $this->armada_trips_model->insert([
                        'armadatrip_armada_id' => $trip_id,
                        'armadatrip_package_id' => $item['value'],
                        'created_at' => time()
                    ]);
                }

                if ($item['name'] == 'order_session') {
                    /** Update last position */
                    if (!in_array($item['value'], $temp_collection)) {

                        orderLastPosition($item['value'], 'Barang telah diangkut');
                        $this->orders_model->update_status($item['value'], 1);
                        $this->orders_model->update_load_date($item['value'], time());
                        $temp_collection[] = $item['value'];

                        // Load all resource needed 
                        $this->load->model('package_management_model');
                        $this->package_management_model->update_loading($item['value'], $trip_id);
                    }
                }
            }

            echo "true";
        } else {

            echo "false";
        }

    }

    public function is_correct_armada() {

        $trip_id = $this->input->post('trip_id');
        $package_id = $this->input->post('package_id');
        $order_id = $this->input->post('order_id');

        /** Check armada from package */
        if ($package_id != '' && $order_id == '') {
            $this->load->model('packages_model');
            $package_detail = $this->packages_model->get(['package_id' => $package_id])->row();

            if (isset($package_detail)) {
                $trips_list = get_armadas($package_detail->package_origin, $package_detail->package_destination);

                if (key_exists($trip_id, $trips_list)) {
                    echo "true";
                } else {
                    echo "false";
                }
            } else {
                echo "false";
            }
        }

        /** Check armada from order */
        if ($package_id == '' && $order_id != '') {
            $this->load->model('orders_model');
            $order_detail = $this->orders_model->get(['order_id' => $order_id])->row();
            if (isset($order_detail)) {
                $trips_list = get_armadas($order_detail->order_origin, $order_detail->order_destination);
                if (key_exists($trip_id, $trips_list)) {
                    echo "true";
                } else {
                    echo "false";
                }
            } else {
                echo "false";
            }
        }

    }

    public function unloading_barang() {

        // Reset session 
        $this->session->set_userdata('unloading_order_session', array());
        $this->session->set_userdata('unloading_package_session', array());

        $this->data['title'] = "Unloading Barang";

        $this->load->view('manifests/unloading_barang', $this->data);

    }

    public function unload_process_perpackage() {

        // Check whether the package is still in warehouse
        $package_manifest = $this->packages_model->get_joinmanifest([
                    'packages.package_id' => $this->input->post('package_id'),
                    'manifest.manifest_trip_id' => $this->input->post('trips_id'),
                    'packages.package_status' => 4
                ])->row();
        $set_warning = '';
        if (!isset($package_manifest)) {
            $set_warning = '&nbsp;<span class="label label-warning"><i class="fa fa-warning"></i> Paket ini tidak ditemukan di armada yang dimaksud</span>';
        }

        // Check loading package
        $packages_isload = $this->packages_model->get_checkloaded($this->input->post('package_id'), 4);

        // Get detail package
        $where['packages_view_detail.package_id'] = $this->input->post('package_id');
        $package = $this->packages_model->get_packages_view_detail($where)->row();

        $unloading_type = $this->input->post('unloading_type');

        //isset($package) && $packages_isload && 
        if ($packages_isload && !in_array($this->input->post('package_id'), $this->session->userdata('unloading_package_session'))) {

            // Session insert 
            $check_session = $this->session->userdata('unloading_package_session');
            $check_session[] = $this->input->post('package_id');
            $this->session->set_userdata('unloading_package_session', $check_session);

            // Get all parent of package
            $parent_id = $this->packages_model->get_parents($this->input->post('package_id'));
            $all_child = $this->packages_model->get_all_child($parent_id);
            $input_list = '';
            foreach ($all_child as $key => $item) {
                $input_list .= "<input type='hidden' name='package_id[]' value='{$item->package_id}' />";
            }

            $str = "<tr>"
                    . "<td> $package->package_id</td>"
                    . "<td>$package->package_title </td >"
                    . "<td>"
                    . "<label class='label label-info' >Paket</label>"
                    . "</td>"
                    . "<td>"
                    . $set_warning
                    . "</td>"
                    . "<td >"
                    . $package->origin_branch_name . " - " . $package->destination_branch_name
                    . "</td>"
                    . "<td>$package->qty koli</td>"
                    . "<td>$package->total_package_size m3</td>"
                    . "<td>$package->total_package_weight Kg</td>"
                    . "<td>"
                    . "<input type='checkbox' name='transit_{$this->input->post('package_id')}' value='' />"
                    . "</td>"
                    . "<td  class='actions' >"
                    . "<input type='hidden' name='package_session' value='" . $this->input->post('package_id') . "' /> "
                    . "<input type='hidden' name='order_session' value='' /> "
                    . $input_list
                    . "<div class='btn-group pull-right'>"
                    . "<button type='button' class='btn btn-danger btn-delete btn-sm'><i class='fa fa-times'></i></button>"
                    . "</div>"
                    . "</td>"
                    . "</tr>";

            echo $str;
        } else {

            $result = 'error_00';

            if (in_array($this->input->post('package_id'), $this->session->userdata('unloading_package_session'))) {
                $result = 'error_01';
            }

            if (!$packages_isload) {
                $result = 'error_02';
            }

            echo $result;
        }

    }

    public function unload_process_perorder() {

        /** Get data from form request */
        $unloading_type = $this->input->post('unloading_type');

        /** Check whether the package is still in warehouse */
        $package_manifest = $this->packages_model->get_joinmanifest([
                    'det_order.order_id' => $this->input->post('order_id'),
                    'manifest.manifest_trip_id' => $this->input->post('trips_id'),
                    'packages.package_status' => 4
                ])->result();

        $set_warning = '';
        if (empty($package_manifest)) {
            $set_warning = '&nbsp;<span class="label label-warning"><i class="fa fa-warning"></i> Paket ini tidak ditemukan di armada yang dimaksud</span>';
        }

        // Check manual
        $order_id_valid = $this->orders_model->get_order_id($this->input->post('order_id'));

        // Get detail order
        $where_order = ['orders.order_id' => $order_id_valid, 'package_status' => '4'];
        $order_detail = $this->orders_model
                ->get_joined($where_order)
                ->row();

        $where['order_id'] = $order_id_valid;
        $package = $this->packages_model->get_joined($where)->result();

        if (!empty($order_detail) && !in_array($order_id_valid, $this->session->userdata('unloading_order_session'))) {

            // Variable initiation
            $str = '';
            $str_input = '';
            $total_size = 0;
            $total_weight = 0;

            // Session insert 
            $check_session = $this->session->userdata('unloading_order_session');
            $check_session[] = $order_id_valid;
            $this->session->set_userdata('unloading_order_session', $check_session);

            $koli = 0;
            if (!empty($package)) {
                foreach ($package as $key => $item) {
                    $str_input .= "<input type='hidden' name='package_id[]' value='{$item->package_id}' />";
                    $koli++;
                    $total_size += $item->package_size;
                    $total_weight += $item->package_weight;
                }
            }

            $total_size = round($total_size / 1000000, 2);

            $str .= "<tr>"
                    . "<td>$order_detail->order_id</td>"
                    . "<td>$order_detail->customer_name</td >"
                    . "<td>"
                    . "<label class='label label-success' >Resi</label>"
                    . "</td>"
                    . "<td>"
                    . $set_warning
                    . "</td>"
                    . "<td>"
                    . $order_detail->branch_name_origin . " - " . $order_detail->branch_name_destination
                    . "</td>"
                    . "<td>$koli koli</td>"
                    . "<td>$total_size m3</td>"
                    . "<td>$total_weight Kg</td>"
                    . "<td>"
                    . "<input type='checkbox' class='is_transit' name='transit_{$order_id_valid}' value='' />"
                    . "<input type='hidden' name='transit' value='{\"id\":\"{$order_id_valid}\", \"value\":\"false\"}' />"
                    . "</td>"
                    . "<td  class='actions' >"
                    . "<div class='btn-group pull-right'>"
                    . "<input type='hidden' name='package_session' value='' /> "
                    . "<input type='hidden' name='order_session' value='" . $order_id_valid . "' /> "
                    . $str_input
                    . "<button type='button' class='btn btn-danger btn-delete btn-sm'><i class='fa fa-times'></i></button>"
                    . "</div>"
                    . "</td>"
                    . "</tr>";

            echo $str;
        } else {

            $result = 'false';

            if (in_array($order_id_valid, $this->session->userdata('unloading_order_session'))) {
                $result = 'null';
            }

            echo $result;
        }

    }

    /**
     *  An ajax function to update package status 
     * 
     * Update status package from on loaded process (sign as 4) into unloading process, 
     * that can as transit (sign as 6) or finish (sign as 5)
     * 
     * @return String 'true' when function is success 
     *      and 'false' when function is not success caused by not data available 
     * 
     */
    public function final_unloading_process() {

        $temp_collection = [];
        $result = '';
        $data = $this->input->post('data');
        $trip_id = $this->input->post('trip');
        $unloading_type = $this->input->post('unloading_type');

        $this->session->set_userdata('k', $data);

        /** Update status when data is not empty */
        if (!empty($data)) {

            /** Break down array to update data */
            foreach ($data as $key => $item) {

                if ($item['name'] == 'package_id[]') {
                    /** Data preparing */
                    $temp['package_status'] = ($unloading_type == 'true') ? 6 : 5;
                    $temp['package_id'] = $item['value'];

                    /** Update package status */
                    $this->packages_model->update($temp);
                }

                if ($item['name'] == 'order_session') {
                    /** Update last position */
                    if (!in_array($item['value'], $temp_collection)) {
                        orderLastPosition($item['value'], 'Barang telah diturunkan');
                        $temp_collection[] = $item['value'];
                        $this->orders_model->update_unload_date($item['value'], time());
                    }
                }

                if ($item['name'] == 'transit' && $item['value'] != '') {

                    // Convert into php array
                    $st = json_decode($item['value'], true);

                    // Load all resource needed 
                    $this->load->model('package_management_model');

                    // Record into package management
                    if ($st['value'] == 'true') {
                        $this->package_management_model->update_transit($st['id'], $trip_id);
                    } else {
                        $this->package_management_model->update_unloading($st['id'], $trip_id);
                    }
                }
            }

            $result = 'true';
        } else {
            $result = 'false';
        }

        echo $result;

    }

    // Reset session loading package 
    public function loading_unset() {

        // Collected data from user form
        $order_session = $this->input->post('order_session');
        $package_session = $this->input->post('package_session');

        // Clear package session
        if ($package_session != '') {
            $data = $this->session->userdata('package_session');
            $key = array_search($package_session, $data);
            unset($data[$key]);
            $this->session->set_userdata('package_session', $data);
        }

        // Clear order session
        if ($order_session != '') {
            $data = $this->session->userdata('order_session');
            $key = array_search($order_session, $data);
            unset($data[$key]);
            $this->session->set_userdata('order_session', $data);
        }

    }

    /**
     * Function to unset all session of loading
     */
    public function loading_unset_all() {

        // Ajax validation 
        if ($this->input->is_ajax_request()) {

            // Reset session
            $this->session->set_userdata('order_session', array());
            $this->session->set_userdata('package_session', array());
        } else {

            // Redirect into loading package page
            redirect($this->controller_path . '/loading_barang');
        }

    }

    // Reset session loading package 
    public function unloading_unset() {

        // Ajax validation 
        if ($this->input->is_ajax_request()) {

            // Collected data from user form
            $order_session = $this->input->post('order_session');
            $package_session = $this->input->post('package_session');

            // Clear package session
            if ($package_session != '') {
                $data = $this->session->userdata('unloading_package_session');
                $key = array_search($package_session, $data);
                unset($data[$key]);
                $this->session->set_userdata('unloading_package_session', $data);
            }

            // Clear order session
            if ($order_session != '') {
                $data = $this->session->userdata('unloading_order_session');
                $key = array_search($order_session, $data);
                unset($data[$key]);
                $this->session->set_userdata('unloading_order_session', $data);
            }
        } else {

            // Redirect into unloading page
            redirect($this->controller_path . '/unloading_barang');
        }

    }

    /**
     * Function to unset all session of unloading
     */
    public function unloading_unset_all() {

        // Ajax validation 
        if ($this->input->is_ajax_request()) {

            // Reset session 
            $this->session->set_userdata('unloading_order_session', array());
            $this->session->set_userdata('unloading_package_session', array());
        } else {

            // Redirect into loading package page
            redirect($this->controller_path . '/unloading_barang');
        }

    }

}
