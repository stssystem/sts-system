<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package_management extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'package_management';
    public $controller_path = 'packages/package_management';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_package_management";
            $this->id = $this->input->post('city_id');
        } else {
            redirect('program/login');
        }
    }

    public function index($status = '') {
        if ($this->input->is_ajax_request()) {
            $this->load->library('datatable/dt_package_management');
            echo $this->dt_package_management->get($this, $_REQUEST, $status);
        } else {
            
            // Load all resource needed
            $this->load->model('branches_model');
            
            $this->data['title'] = "Daftar Kelola Paket";
            $this->data['cities'] = $this->branches_model->get_joined_cities()->result_array();
            
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
            
        }
    }

    public function filter() {
        $this->load->model('branches_model');
        $data['date_start'] = str_replace('/', '-', $this->input->post('date_start_filter'));
        $data['date_end'] = str_replace('/', '-', $this->input->post('date_end_filter'));
        $data['branches'] = $this->input->post('branches');
        $this->session->set_userdata('package-management-filter', $data);
        redirect($this->controller_path);
    }

    public function clear() {
        $this->session->unset_userdata('package-management-filter');
        $this->session->unset_userdata('dt_package_management');
        redirect($this->controller_path);
    }

}
