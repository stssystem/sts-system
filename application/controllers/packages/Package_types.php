<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package_types extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'package_types';
    public $controller_path = 'packages/package_types';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_package_types";
            $this->id = $this->input->post('package_type_id');

            $this->load->model('package_types_model');
            $this->load->library('request/request_package_types');
            $this->load->library('datatable/dt_package_types');
            
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        $this->data['title'] = "Daftar Tipe Paket";

        if ($this->input->is_ajax_request()) {
            echo $this->dt_package_types->get($this);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add() {
        $this->data['title'] = "Tambah Tipe Paket";
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store() {

        if (!$this->request_package_types->validation()) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            /** Cities create */
            $set_array = array(
                'package_type_name' => $this->input->post('package_type_name'),
                'created_at' => time(),
            );

            $this->package_types_model->insert($set_array);

            set_alert('alert-success', 'Data tipe paket <strong>' . $this->input->post('package_type_name') . '</strong> berhasil ditambahkan');

            redirect($this->controller_path, 'refresh');
        }
    }

    public function edit($id = 0) {

        $this->data['title'] = "Edit Tipe Paket";
        $this->data['data'] = $this->package_types_model->get(['package_type_id' => $id])->row();

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update() {

        if (!$this->request_package_types->validation()) {
            redirect($this->controller_path . '/edit/' . $this->id, 'refresh');
        } else {

            $where_array = array(
                'package_type_id' => $this->id,
                'package_type_name' => $this->input->post('package_type_name'),
                'updated_at' => time()
            );

            $this->package_types_model->update($where_array);

            set_alert('alert-success', 'Data tipe paket <strong>' . $this->input->post('package_type_name') . '</strong> telah diperbarui');

            redirect($this->controller_path);
            
        }
    }

    public function destroy($id = null) {
        $data = $this->package_types_model->delete(['package_type_id' => $id]);
        set_alert('alert-danger', 'Data tipe paket<strong>'.$data->package_type_name.'</strong> telah dihapus');
        redirect($this->controller_path);
    }

}
