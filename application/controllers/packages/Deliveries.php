<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Deliveries extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'deliveries';
    public $controller_path = 'packages/deliveries';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_deliveries";
            $this->id = $this->input->post('delivery_id');

            $this->load->model('deliveries_model');
            $this->load->model('packages_model');
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        $this->data['title'] = "Pengantaran Paket";

        // Reset session 
        $this->session->set_userdata('order_delivery_session', array());
        $this->session->set_userdata('package_delivery_session', array());

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store() {

        /** Initialization */
        $package = $this->input->post('package_id');
        $armada = $this->input->post('delivery_armada_id');
        $armada_text = $this->input->post('delivery_armada_text');
        $courier = $this->input->post('delivery_courier');
        $courier_text = $this->input->post('delivery_courier_text');
        $depart_time = $this->input->post('delivery_depart_time');
        $str_warning = '&nbsp;<span class="label label-warning"><i class="fa fa-warning"></i> '
                . 'Paket ini tidak ditemukan di daftar barang'
                . '</span>';

        /** Get package id */
        $package_detail = $this->packages_model->get([
                    'package_id' => $package,
                    'package_status' => 5
                ])->row();

        /** Check wheter the package */
        if (isset($package_detail) && !in_array($package, $this->session->userdata('package_delivery_session'))) {

            // Get all parent of package
            $parent_id = $this->packages_model->get_parents($package);
            $all_child = $this->packages_model->get_all_child($parent_id);
            $input_list = '';
            foreach ($all_child as $key => $item) {
                $input_list .= "<input type='hidden' name='package_id[]' value='{$item->package_id}' />"
                        . "<input type='hidden' name='armada_id[]' value='{$armada}' />"
                        . "<input type='hidden' name='courier_id[]' value='{$courier}' />"
                        . "<input type='hidden' name='depart_time[]' value='{$depart_time}' />";
            }

            /** Return success message */
            $str = "<tr>"
                    . "<td>$package_detail->package_id</td>"
                    . "<td>$package_detail->package_title </td >"
                    . "<td>$armada_text </td >"
                    . "<td>$courier_text </td >"
                    . "<td>$depart_time </td >"
                    . "<td width='10%'>"
                    . "<label class='label label-info' >Paket</label>"
                    . "</td>"
                    . "<td width='90%'  >"
                    . ($package_detail->package_status != 5 ? $str_warning : '')
                    . "</td>"
                    . "<td  class='actions' >"
                    . $input_list
                    . "<div class='btn-group btn-sm pull-right'>"
                    . "<button type='button' class='btn btn-danger btn-delete btn-sm'><i class='fa fa-times'></i></button>"
                    . "</div>"
                    . "</td>"
                    . "</tr>";

            // Insert into session delivery package
            $temp = $this->session->userdata('package_delivery_session');
            $temp [] = $package_detail->package_id;
            $this->session->set_userdata('package_delivery_session', $temp);

            echo $str;
        } else {

            $return = 'error_01';

            if (in_array($package, $this->session->userdata('package_delivery_session'))) {
                $return = 'error_02';
            }

            echo $return;
        }
    }

    public function stores() {

        /** Initialization */
        $armada = $this->input->post('delivery_armada_id');
        $armada_text = $this->input->post('delivery_armada_text');
        $courier = $this->input->post('delivery_courier');
        $courier_text = $this->input->post('delivery_courier_text');
        $depart_time = $this->input->post('delivery_depart_time');
        $order = $this->input->post('order_id');
        $str_list_package = '';
        $tr_list = '';
        $str_warning = '&nbsp;<span class="label label-warning">'
                . '<i class="fa fa-warning"></i> '
                . 'Paket ini tidak ditemukan di daftar barang</span>';

        // Load all resource needed
        $this->load->model('orders_model');

        // Check no resi manual
        $order_id_valid = $this->orders_model->get_order_id($order);

        // Get detail order
        $where_order = ['orders.order_id' => $order_id_valid, 'package_status' => 5];
        $order_detail = $this->orders_model
                ->get_joined($where_order)
                ->row();

        /** Update package status and insert into delivery table when package available */
        if (!empty($order_detail) && !in_array($order_id_valid, $this->session->userdata('order_delivery_session'))) {

            // Get all package in the order
            $where['det_order.order_id'] = $order_id_valid;
            $package = $this->packages_model->get_joined($where)->result();

            // Generate all package as input, total size, and total weight
            $str_input = '';
            if (!empty($package)) {
                foreach ($package as $key => $item) {
                    $str_input .= "<input type='hidden' name='package_id[]' value='{$item->package_id}' />"
                            . "<input type='hidden' name='armada_id[]' value='{$armada}' />"
                            . "<input type='hidden' name='courier_id[]' value='{$courier}' />"
                            . "<input type='hidden' name='depart_time[]' value='{$depart_time}' />";
                }
            }

            /**
             * Break down package available to deliver,
             * then insert into deliveries table and listed it to message purpose
             */
            /** Listing packages that was success to delivering */
            $tr_list .= "<tr>"
                    . "<td>$order_detail->order_id</td>"
                    . "<td>$order_detail->customer_name </td >"
                    . "<td>$armada_text </td >"
                    . "<td>$courier_text </td >"
                    . "<td>$depart_time </td >"
                    . "<td width='10%'>"
                    . "<label class='label label-success' >Resi</label>"
                    . "</td>"
                    . "<td  width='90%' ></td>"
                    . "<td  class='actions' >"
                    . $str_input
                    . "<div class='btn-group btn-sm pull-right'>"
                    . "<button type='button' class='btn btn-danger btn-delete btn-sm'><i class='fa fa-times'></i></button>"
                    . "</div>"
                    . "</td>"
                    . "</tr>";

            // Input order id into session 
            $temp = $this->session->userdata('order_delivery_session');
            $temp[] = $order_detail->order_id;
            $this->session->set_userdata('order_delivery_session', $temp);

            echo $tr_list;
        } else {

            $error_msg = 'error_01';

            if (in_array($order_id_valid, $this->session->userdata('order_delivery_session'))) {
                $error_msg = 'error_02';
            }

            echo $error_msg;
        }
    }

    /**
     *  An ajax function to insert package into delivery list
     * 
     * Insert package list into package delivery list, update the order last location 
     * and update order status from finish order (sign as 5) into delivery status (sign as 7)
     * 
     * @return String 'true' when function is success 
     *      and 'false' when function is not success caused by not data available 
     * 
     */
    public function final_store() {

        /**
         * @var Array Collection of order id that purpose to group package per order id, 
         * then prevent redundant when inserting the last location of order 
         */
        $temp_collection = [];

        /**
         * @var String Result the function proccess, 'true' when function is success 
         * and 'false' when function is not success caused by not data available 
         */
        $result = '';

        /** @var Array List of package that will to insert into delivery table */
        $data_package = $this->input->post('data_package');

        /**
         * @var Array List of armada that will to insert into delivery table as armada 
         * carier assosiate the package 
         */
        $data_armada = $this->input->post('data_armada');

        /**
         * @var Array List of courier that will to insert into delivery table as driver 
         *  assosiate the package 
         */
        $data_courier = $this->input->post('data_courier');

        /**
         * @var Array List of courier that will to insert into delivery table as depart time 
         *  assosiate the package 
         */
        $data_depart_time = $this->input->post('data_depart_time');

        /** Final store process will do when data not empty */
        if (!empty($data_package)) {

            /** Break down array to update data */
            foreach ($data_package as $key => $item) {

                /** Insert package data into delivery table */
                $this->deliveries_model->insert([
                    'delivery_package_id' => $data_package[$key]['value'],
                    'delivery_courier' => $data_courier[$key]['value'],
                    'delivery_armada_id' => $data_armada[$key]['value'],
                    'delivery_depart_time' => strtotime($data_depart_time[$key]['value']),
                    'created_at' => time()
                ]);

                /**  Update package status */
                $this->packages_model->update([
                    'package_id' => $data_package[$key]['value'],
                    'package_status' => 7
                ]);

                /** Update last position */
                if (!in_array($this->packages_model->get_order_id($data_package[$key]['value']), $temp_collection)) {

                    orderLastPosition($this->packages_model->get_order_id($data_package[$key]['value']), 'Barang mulai diantarkan');
                    $temp_collection[] = $this->packages_model->get_order_id($data_package[$key]['value']);

                    // Update delivered update
                    $this->load->model('orders_model');
                    $this->orders_model->update_delivered_date($this->packages_model->get_order_id($data_package[$key]['value']), time());

                    // Insert package management table
                    $this->load->model('package_management_model');
                    $this->package_management_model->update_delivery($this->packages_model->get_order_id($data_package[$key]['value']), $data_armada[$key]['value'], $data_courier[$key]['value']);
                    
                }
            }

            /** Set return value with true when update status is success */
            $result = 'true';
        } else {

            /** Set return value with false when data is empty */
            $result = 'false';
        }

        /** Return the result of process */
        echo $result;
    }
    
     // Reset session loading package 
    public function destroys_all() {

        // Collected data from user form
        $order_session = $this->input->post('order_session');
        $package_session = $this->input->post('package_session');

        // Clear package session
        if ($package_session != '') {
            $data = $this->session->userdata('package_session');
            $key = array_search($package_session, $data);
            unset($data[$key]);
            $this->session->set_userdata('order_delivery_session', $data);
        }

        // Clear order session
        if ($order_session != '') {
            $data = $this->session->userdata('order_session');
            $key = array_search($order_session, $data);
            unset($data[$key]);
            $this->session->set_userdata('order_session', $data);
        }

    }

}
