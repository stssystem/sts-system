<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Unload_session extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function remove_order_row($order_id = '') {
        $temp = $this->session->userdata('unloading_order_session');
        $key = array_search($order_id, $temp);
        unset($temp[$key]);
    }

    public function remove_package_row($package_id = '') {
        $temp = $this->session->userdata('unloading_package_session');
        $key = array_search($order_id, $temp);
        unset($temp[$key]);
    }

    public function remove_all() {
        $this->session->unset_userdata('unloading_order_session');
        $this->session->unset_userdata('unloading_package_session');
    }

}
