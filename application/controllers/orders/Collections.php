<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Collections extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'orders';
    public $controller_path = 'orders/collections';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('orders_model');
            $this->load->model('packages_model');
            $this->load->model('cities_model');
            $this->load->model('districts_model');
            $this->load->model('villages_model');

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "cs";

            $this->data['cities'] = $this->cities_model->get()->result_array();
            $this->data['districts'] = $this->districts_model->get()->result_array();
            $this->data['villages'] = $this->villages_model->get()->result_array();
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        $this->data['title'] = "Pengambilan Paket";
        $this->load->view($this->view_path . '/collections', $this->data);   
    }

    public function search_order()
    {
        $this->data['title'] = "Resi No. 12345";
        $this->load->view($this->view_path . '/search_order_result', $this->data);
    }

    public function detail_order()
    {
        $this->data['title'] = "Resi No. 12345";
        $this->load->view($this->view_path . '/detail_order', $this->data);
    }

}
