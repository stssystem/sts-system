<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Locations_update extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'locations_update';
    public $controller_path = 'orders/locations_update';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_location_orders";

            $this->load->model('locations_update_model');
        } else {
            redirect('program/login');
        }
    }

    public function index() {
        $this->data['title'] = "Update Lokasi";
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function add() {

        /** Collect data from form */
        $armada_id = $this->input->post('armada_id');
        $branch_id = $this->input->post('branch_id');

        /** Update last location in armada and orders */
        $this->locations_update_model->update($armada_id, $branch_id);

        echo "Success";
        
    }

   

}
