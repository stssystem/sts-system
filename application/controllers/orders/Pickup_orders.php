<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pickup_orders extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'pickup_orders';
    public $controller_path = 'orders/pickup_orders';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_pickup_orders";
        } else {
            redirect('program/login');
        }
    }

    public function index($pickup_status = 0) {
        $this->data['title'] = "Daftar Pickup Order";
        if ($this->input->is_ajax_request()) {

            // Load all resource needed
            $this->load->library('datatable/dt_pickup_orders');
            echo $this->dt_pickup_orders->get($this, $pickup_status, $_REQUEST);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function destroy($id = '') {

        // Load all resource needed
        $this->load->model('pickup_orders_model');

        $result = $this->pickup_orders_model->destroy($id);
        set_alert('alert-success', "Order atas nama pengirim <strong>{$result->nama_pengirim}</strong> telah berhasil dihapus");
        redirect($this->controller_path);
    }

    public function edit($no_resi = 0) {

        // Load all resource needed
        $this->load->model('orders_model');

        if (!$this->orders_model->order_isexist($no_resi)) {
            $this->orders_model->close();
            pickup_set($no_resi);
            pickup_unlock();
            redirect('orders/orders');
        } else {
            set_alert('alert-danger', "Nomor Resi <strong>$no_resi</strong> sudah terdapat di daftar order");
            redirect($this->controller_path);
        }
    }

    public function update() {

        // Load all resource needed
        $this->load->model('pickup_orders_model');

        $id = $this->input->post('id');
        $return = $this->pickup_orders_model->update_pickup_status($id, 1);
        set_alert('alert-success', "Order atas nama pengirim <strong>{$return->nama_pengirim}</strong> telah berhasil diperbarui status <storng>Siap Diambil</strong>");
        redirect($this->controller_path);
    }

    public function update2() {

        // Load all resource needed
        $this->load->model('pickup_orders_model');

        $id = $this->input->post('id');
        $return = $this->pickup_orders_model->update_pickup_status($id, 2);
        set_alert('alert-success', "Order atas nama pengirim <strong>{$return->nama_pengirim}</strong> telah berhasil diperbarui status <storng>Masuk Daftar Order</strong>");
        redirect($this->controller_path);
    }

    public function printout() {

        // Load all resource needed
        $this->load->model('pickup_orders_model');

        $branch_based = (!empty(branch())) ? branch()['city_name'] : false;
        $where = ['pickup_status' => 1];
        $this->data['print'] = $this->pickup_orders_model->get_joined($where, ['order' => ['created_at' => 'desc']], $branch_based)->result();
        $this->load->view('pickup_orders/printout', $this->data);
    }
    
    public function reset(){
        $this->session->unset_userdata('dt_pickuporder');
        redirect($this->controller_path);
    }

}
