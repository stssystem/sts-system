<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'orders';
    public $controller_path = 'orders/orders';
    private $order_id;

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('orders_model');
            $this->load->model('packages_model');
            $this->load->model('package_pic_model');
            $this->load->model('det_orders_model');
            $this->load->model('orders_model');
            $this->load->model('cities_model');
            $this->load->model('districts_model');
            $this->load->model('villages_model');
            $this->load->model('costumers_model');
            $this->load->model('det_orders_extra_model');
            $this->load->model('package_types_model');
            $this->load->model('provinces_model');
            $this->load->model('customers_origin_model');
            $this->load->model('sync_package_model');

            $this->load->library('request/request_costumer');
            $this->load->library('request/request_orders');
            $this->load->library('datatable/dt_orders');
            $this->load->library('datatable/dt_det_orders_extra');

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "cs";
            $this->data['provinces'] = $this->provinces_model->get_available()->result_array();
            $this->data['cities'] = $this->cities_model->get_available()->result_array();
            $this->data['districts'] = $this->districts_model->get()->result_array();
            $this->data['villages'] = $this->villages_model->get()->result_array();
            $this->data['package_types'] = $this->package_types_model->get()->result_array();

            if (!isset(temp_orders()['orders']->order_id)) {
                $this->order_id = $this->orders_model->open();
            } else {
                $this->order_id = temp_orders()['orders']->order_id;
            }
        } else {
            redirect('program/login');
        }
    }

    public function index($order_id = 0) {

        $this->data['title'] = "Form Order";
        $this->data['order_id'] = $this->order_id;

        if ($this->input->is_ajax_request()) {
            echo $this->dt_orders->get($this, $order_id);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
        
    }

    public function store() {

        $data = json_decode($this->input->post('data'), true);

        if ($data['det_order_qty'] > 1) {

            /** Do when quantity more than one */
            $package_parent = 0;
            for ($i = 0; $i < $data['det_order_qty']; $i++) {

                /** Package create */
                $package_id = $this->insert_pakages($data);

                /** Set package parent */
                if ($i == 0) {
                    $package_parent = $package_id;
                }

                /** Upload image */
                $this->insert_packages_pic($data, $package_id);

                /** Order detail */
                $this->insert_det_orders($data, $package_id, $package_parent);
            }
        } else {

            /** Do when quantity just one */
            /** Package create */
            $package_id = $this->insert_pakages($data);

            /** Upload image */
            $this->insert_packages_pic($data, $package_id);

            /** Order detail */
            $this->insert_det_orders($data, $package_id, $package_id);
        }
    }

    private function insert_pakages($data = array()) {

        $set_array = array(
            'package_title' => $data['package_title'],
            'package_type' => $data['package_type'],
            'package_origin' => $data['branch_id_origin'],
            'package_destination' => $data['branch_id_destination'],
            'package_status' => 0,
            'package_content' => $data['package_content'],
            'package_width' => $data['package_width'],
            'package_height' => $data['package_height'],
            'package_lenght' => $data['package_lenght'],
            'package_size' => ($data['package_size'] * 1000000) / $data['det_order_qty'],
            'package_weight' => $data['package_weight'],
            'created_at' => time()
        );

        return $this->packages_model->insert($set_array);
    }

    private function insert_packages_pic($data = array(), $package_id) {

        /** Listing image availabe from form */
        if (!empty($data['package_pic'])) {
            foreach ($data['package_pic'] as $key => $value) {

                /** Write base64 image code into file */
                $filename = date('ymdhis') . rand(1000, 9999) . '.jpg';
                $img = str_replace('data:image/jpeg;base64,', '', $value);

                $img = str_replace(' ', '+', $img);
                $img_data = base64_decode($img);
                write_file('./assets/upload/' . $filename, $img_data);

                /** Save image file name into packages_pic table */
                $this->package_pic_model->insert([
                    'package_pic' => $filename,
                    'package_id' => $package_id,
                ]);
            }
        }
    }

    private function insert_det_orders($data = array(), $package_id, $package_parent = 0) {

        $set_array = array(
            'package_id' => $package_id,
            'order_id' => $data['order_id'],
            'det_order_total' => $data['total'],
            'det_order_qty' => $data['det_order_qty'],
            'det_order_sell_total' => round($data['det_order_sell_total']),
            'det_order_package_parent' => $package_parent
        );

        $this->det_orders_model->insert($set_array);
    }

    public function det_orders_extra_store() {

        $data = json_decode($this->input->post('data'), true);

        /** Order detail */
        $set_array = array(
            'order_id' => $data['order_id'],
            'det_order_extra_name' => $data['det_order_extra_name'],
            'det_order_extra_total' => $data['det_order_extra_total']
        );

        $this->det_orders_extra_model->insert($set_array);
    }

    public function update() {

        $data = json_decode($this->input->post('data'), true);
        $parent_id = $this->packages_model->get_parents($data['package_id']);

        /** Remove old data */
        $this->package_pic_model->delete_byparent($parent_id);
        $this->packages_model->delete_byparent($parent_id);
        $this->det_orders_model->delete_byparent($parent_id);

        if ($data['det_order_qty'] > 1) {

            /** Do when quantity more than one */
            $package_parent = 0;
            for ($i = 0; $i < $data['det_order_qty']; $i++) {

                /** Package create */
                $package_id = $this->insert_pakages($data);

                /** Set package parent */
                if ($i == 0) {
                    $package_parent = $package_id;
                }

                /** Upload image */
                $this->insert_packages_pic($data, $package_id);

                /** Order detail */
                $this->insert_det_orders($data, $package_id, $package_parent);
            }
        } else {

            /** Do when quantity just one */
            /** Package create */
            $package_id = $this->insert_pakages($data);

            /** Upload image */
            $this->insert_packages_pic($data, $package_id);

            /** Order detail */
            $this->insert_det_orders($data, $package_id, $package_id);
        }
    }

    public function update_deprecated() {

        $data = json_decode($this->input->post('data'), true);

        /** Package update  */
        $set_array = array(
            'package_id' => $data['package_id'],
            'package_title' => $data['package_title'],
            'package_type' => $data['package_type'],
            'package_status' => 0,
            'package_content' => $data['package_content'],
            'package_width' => $data['package_width'],
            'package_height' => $data['package_height'],
            'package_lenght' => $data['package_lenght'],
            'package_size' => $data['package_size'],
            'package_weight' => $data['package_weight'],
            'package_origin' => $data['package_origin'],
            'package_destination' => $data['package_destination'],
            'updated_at' => time(),
        );

        $package_id = $this->packages_model->update($set_array);

        /** Detail package update */
        $order_detail = [
            'det_order_sell_total' => $data['det_order_sell_total'],
            'det_order_qty' => $data['det_order_qty']
        ];

        $this->det_orders_model->update($order_detail, ['package_id' => $data['package_id']]);
    }

    public function complete_store() {

        /** Generate data array */
        $this->data_array();

        /** Package validation, true where have at least on package */
        $validation_attr = array();
        if ($this->packages_model->isempty($this->input->post('order_id'))) {
            $validation_attr['add_validation'] = array([
                    'field' => 'package_list',
                    'label' => 'Daftar pake tidak boleh kosong',
                    'rules' => 'required'
            ]);
        }

        // Exception duplicate resi manual validation for edit action
        if (isset(temp_orders()['orders']->order_id)) {
            $validation_attr['id'] = temp_orders()['orders']->order_id;
        }

        if (!$this->request_orders->validation($validation_attr)) {
            redirect($this->controller_path, 'refresh');
        } else {

            $result = 0;
            if ($this->input->post('order_manual_id') == '') {
                $no_resi_manual_ = $this->input->post('branch_code') . $this->input->post('order_manual_id');
                $result = $this->check_manual_resi($no_resi_manual_);
            } else {
                $result = NULL;
            }

            if (count($result) == 0 || $this->input->post('order_manual_id') == '') {

                /** Insert customer data into customer table */
                $is_update = $this->input->post('customer_id') == '' ? false : true;
                $customer_id = $this->costumer_insert($is_update);

                /** Insert customer origin */
                if ($this->input->post('save_update_origin')) {
                    $this->insert_customers_origin($customer_id);
                }

                /** Insert order data into orders table */
                $order_id = $this->order_insert($customer_id);

                /** Update package origin and destination rely on the lastest order */
                $this->package_update();

                /** Success redirect */
                set_alert('alert-success', 'Order berhasil dibuat');

                if (pickup_isset()) {
                    pickup_destroy();
                }

                redirect('orders/order/view/' . $order_id, 'refresh');
            } else {

                /** Success redirect */
                set_alert('alert-danger', "Nomor Resi Manual <strong>{$this->input->post('order_manual_id')}</strong> telah dipakai");
                redirect('orders/orders', 'refresh');
            }
        }

        session_write_close();
    }

    public function cancel($order_id = 0) {

        // Delete det_order table
        $result = $this->det_orders_model->delete(['order_id' => $order_id]);

        // Delete package table
        if (!empty($result->result())) {
            foreach ($result->result() as $key_i => $value_i) {
                $package = $this->packages_model->delete(['package_id' => $value_i->package_id])->result();
                $array = $this->package_pic_model->delete(['package_id' => $value_i->package_id])->result();
                if (!empty($array)) {
                    foreach ($array as $key_k => $value_k) {
                        unlink("./assets/upload/" . $value_k->package_pic);
                    }
                }
            }
        }

        // Delete delete orders_extra table
        $this->det_orders_extra_model->delete(['order_id' => $order_id]);

        //  Delete old orders
        $this->orders_model->delete(['order_id' => $order_id]);

        // Clear session
        $this->session->unset_userdata('temp_orders');


        set_alert('alert-success', 'Order telah dibatalkan');

        redirect($this->controller_path);
    }

    public function destroy_package($package_id = 0) {

        $parent_id = $this->packages_model->get_parents($package_id);

        /** Remove old data */
        $this->package_pic_model->delete_byparent($parent_id);
        $this->packages_model->delete_byparent($parent_id);
        $this->det_orders_model->delete_byparent($parent_id);
    }

    public function destroy_package_multiple() {
        $data = json_decode($this->input->post('data'), true);
        if (!empty($data)) {
            foreach ($data as $key => $item) {
                $this->destroy_package($item);
            }
        }
    }

    public function destroy_orderextra_multiple() {
        $data = json_decode($this->input->post('data'), true);
        if (!empty($data)) {
            foreach ($data as $key => $item) {
                $this->destroy_order_extra($item);
            }
        }
    }

    public function destroy_package_pic() {
        $array = $this->package_pic_model->delete(['package_id' => $package_id])->result();
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                unlink("./assets/upload/" . $value->package_pic);
            }
        }
    }

    public function get_orders_extra($order_id = 0) {
        echo $this->dt_det_orders_extra->get($this, $order_id);
    }

    private function data_array() {

        /** Helper input */
        old('old_province_id_origin', $this->input->post('old_province_id_origin'));
        old('old_province_id_destination', $this->input->post('old_province_id_destination'));

        $data_arr['order_id'] = $this->input->post('order_id');

        if (empty(branch())) {
            $data_arr['order_origin'] = $this->input->post('branch_id_origin');
        } else {
            $data_arr['order_origin'] = branch()['branch_id'];
        }

        $data_arr['order_destination'] = $this->input->post('branch_id_destination');

        old('order_origin', $data_arr['order_origin']);
        old('order_destination', $data_arr['order_destination']);

        $data_arr['order_origin_text'] = [
            'province_id_origin' => $this->input->post('province_id_origin'),
            'city_id_origin' => $this->input->post('city_id_origin'),
            'districts_id_origin' => $this->input->post('districts_id_origin'),
            'village_id_origin' => $this->input->post('village_id_origin'),
            'branch_id_origin' => $this->input->post('branch_id_origin'),
            'order_origin_text' => $this->input->post('order_origin_text')
        ];
        old($data_arr['order_origin_text']);

        $data_arr['order_destination_text'] = [
            'province_id_destination' => $this->input->post('province_id_destination'),
            'city_id_destination' => $this->input->post('city_id_destination'),
            'districts_id_destination' => $this->input->post('districts_id_destination'),
            'village_id_destination' => $this->input->post('village_id_destination'),
            'branch_id_destination' => $this->input->post('branch_id_destination'),
            'order_destination_text' => $this->input->post('order_destination_text')
        ];
        old($data_arr['order_destination_text']);

        return $data_arr;
    }

    private function costumer_insert($update = false) {

        $set_array = array(
            'customer_name' => $this->input->post('customer_name'),
            'customer_web_id' => $this->input->post('customer_web_id'),
            'customer_address' => $this->input->post('customer_address'),
            'customer_phone' => $this->input->post('customer_phone'),
            'customer_type' => $this->input->post('customer_type'),
            'customer_city' => $this->input->post('customer_city'),
            'customer_referral' => $this->input->post('customer_referral'),
            'customer_last_transaction' => time(),
            'customer_id_number' => $this->input->post('customer_id_number'),
            'customer_id_type' => $this->input->post('customer_id_type'),
            'customer_id_type_other' => $this->input->post('customer_id_type_other'),
            'customer_birth_date' => strtotime($this->input->post('customer_birth_date')),
            'customer_freq_sending' => $this->input->post('customer_freq_sending'),
            'customer_sending_quota' => $this->input->post('customer_sending_quota'),
            'customer_company_type' => $this->input->post('customer_company_type'),
            'customer_company_type_other' => $this->input->post('customer_company_type_other'),
            'customer_gender' => $this->input->post('customer_gender'),
            'customer_company_website' => $this->input->post('customer_company_website'),
            'customer_company_telp' => $this->input->post('customer_company_telp'),
            'customer_company_email' => $this->input->post('customer_company_email'),
            'customer_company_address' => $this->input->post('customer_company_address'),
            'customer_company_name' => $this->input->post('customer_company_name'),
            'customer_code' => $this->input->post('customer_code'),
            'created_at' => time(),
        );

        if ($update == false) {
            return $this->costumers_model->insert($set_array);
        } else {
            $set_array['customer_id'] = $this->input->post('customer_id');
            $this->costumers_model->update($set_array);
            return $this->input->post('customer_id');
        }
    }

    public function insert_customers_origin($customer_id = 0) {

        $data = [
            'customers_origin_customer_id' => $customer_id,
            'customers_origin_province_id' => $this->input->post('province_id_origin'),
            'customers_origin_city_id' => $this->input->post('city_id_origin'),
            'customers_origin_districts_id' => $this->input->post('districts_id_origin'),
            'customers_origin_villages_id' => $this->input->post('village_id_origin'),
            'customers_origin_branch_id' => $this->input->post('branch_id_origin'),
            'customers_origin_address' => $this->input->post('order_origin_text')
        ];

        $this->customers_origin_model->store($data, 'customers_origin_customer_id');
    }

    private function check_manual_resi($manual_resi = '') {
        $data = $this->orders_model->get(['order_manual_id' => $manual_resi])->result();
        return $data;
    }

    private function order_insert($customer_id = 0) {

        $data_arr = $this->data_array();
        $redate = str_replace('/', '-', $this->input->post('order_date'));

        $set_array = array(
            'order_id' => $this->input->post('order_id'),
            'order_date' => strtotime($redate),
            'order_status' => -3,
            'customer_id' => $customer_id,
            'staff_id' => profile()->user_id,
            'order_manual_id' => $this->input->post('order_manual_id') != '' ? $this->input->post('branch_code') . $this->input->post('order_manual_id') : '',
            'order_id_web' => $this->input->post('order_manual_id') != '' ? substr($this->input->post('order_manual_id'), -7) : '',
            'order_payment_type' => $this->input->post('order_payment_type'),
            'order_notes' => $this->input->post('order_notes'),
            'order_origin' => $data_arr['order_origin'],
            'order_origin_text' => json_encode($data_arr['order_origin_text']),
            'order_destination' => $data_arr['order_destination'],
            'order_destination_text' => json_encode($data_arr['order_destination_text']),
            'order_total' => $this->helper_get_total($this->input->post('order_id'), 'total'),
            'order_sell_total' => $this->input->post('customer_prices_total'),
            'order_use_tax' => $this->input->post('order_use_tax'),
            'destination_name' => $this->input->post('destination_name'),
            'destination_telp' => $this->input->post('destination_telp'),
            'order_type' => $this->input->post('order_type'),
            'orders_due_date' => $this->input->post('long_due_date'),
            'order_branch_code' => $this->input->post('branch_code'),
            'order_manual_resi_pure' => $this->input->post('order_manual_id'),
            'created_at' => time()
        );

        $this->orders_model->update($set_array);

        /** Update order last location */
        $this->load->model('orders_locations_model');
        $this->load->model('branches_model');
        $get_branch = $this->branches_model->get_geobranch($data_arr['order_origin']);
        $this->orders_locations_model->update_last_location($this->input->post('order_id'), $get_branch, $this->input->post('city_id_origin'), $data_arr['order_origin'], 'Barang telah diterima');

        /** Update orders status */
        if ($this->input->post('pickup_order_session') != '') {
            $this->load->model('pickup_orders_model');
            $id = $this->pickup_orders_model->get_id(substr($this->input->post('order_id'), -7));
            $this->pickup_orders_model->update_pickup_status($id, 3);
        }

        destroy_sessorder();

        return $this->input->post('order_id');
    }

    private function package_update() {

        $data_arr = $this->data_array();

        $update = [
            'det_order.order_id' => $data_arr['order_id'],
            'packages.package_origin' => $data_arr['order_origin'],
            'packages.package_destination' => $data_arr['order_destination'],
            'packages.package_status' => 1
        ];

        $this->packages_model->update_joined($update);
    }

    public function helper_get_total($order_id = '', $column = 'total') {
        $array = $this->packages_model->get_joined(['det_order.order_id' => $order_id], ['group_by' => 'det_order.det_order_package_parent'])->result();
        $total = 0;
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                if ($column == 'total') {
                    $total += $value->total;
                } else {
                    $total += $value->sell_total;
                }
            }
        }
        return $total;
    }

    public function customer_insert_single() {
        $this->costumer_insert();
        set_alert('alert-info', 'Data Telah Disimpan');
        redirect($this->controller_path . '/customer');
    }

    public function print_orders($order_id = 0) {
        $data['order'] = $this->orders_model->get(['order_id' => $order_id])->row();
        $data['customer'] = $this->costumers_model->get(['customer_id' => $data['order']->customer_id])->row();
        $data['package'] = $this->packages_model->get_joined(['order_id' => $order_id])->result();
        $data['det_orders_extra'] = $this->det_orders_extra_model->get(['order_id' => $order_id])->result();
        $this->load->view('orders/printout_barcode', $data);
    }

    public function print_order($order_id = 0, $mode = '') {

        // Load all resource needed
        $this->load->helper('order');

        // Get orders detail
        $data['order'] = $this->orders_model->get_joined(['orders.order_id' => $order_id])->row();

        if ($mode == 'new') {

            // Update orders status
            $this->orders_model->update_status($order_id, '0');

            // Update orders payment status
            if ($data['order']->order_payment_type == 1 || $data['order']->order_payment_type == 3 || $data['order']->order_payment_type == 5) {
                
                // Set payment status as 1 when payment type is cash, jatuh tunai, and bdb
                $this->orders_model->update_payment_status($order_id, '1');
                
            }else{
                
                // Set payment status as 0 when payment type is franko and jatuh tempo
                $this->orders_model->update_payment_status($order_id, '0');
                
            }

            // Clear temp orders session
            $this->session->unset_userdata('temp_orders');
        }

        // Load view barcode
        $this->load->view('orders/printout_barcode', $data);
    }

    public function print_barcode($package_id = 0) {
        $this->data['code'] = $package_id;
        $this->data['data'] = $this->packages_model->get_joined(['packages.package_id' => $package_id])->row();
        $this->load->view('orders/barcode', $this->data);
    }

    public function print_multiple_barcode($order_id = 0) {
        $this->data['codes'] = $this->packages_model->get_joined(['det_order.order_id' => $order_id])->result();
        $this->data['data'] = $this->orders_model->get_joined(['orders.order_id' => $order_id])->row();
        $this->load->view('orders/barcode', $this->data);
    }

    public function render($package_id = 0) {

        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');

        $rendererOptions = array();
        $barcodeOptions = array(
            'text' => sprintf("%05d", $package_id),
            'rendererParams' => array('imageType' => 'jpg'),
            'barThickWidth' => 3,
            'font' => 5
        );

        $file = Zend_Barcode::render('code39', 'image', $barcodeOptions, $rendererOptions);
    }

    public function destroy_order_extra($order_extra_id = 0) {
        $this->det_orders_extra_model->delete(['det_order_extra_id' => $order_extra_id]);
    }

    public function get_image_list($package_id = 0) {
        $parent_id = $this->packages_model->get_parents($package_id);

        $images = $this->package_pic_model->get(['package_id' => $parent_id])->result();

        $result = [];
        if (!empty($images)) {
            foreach ($images as $key => $item) {
                $path = './assets/upload/' . $item->package_pic;
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $type = $type == 'jpg' ? 'jpeg' : $type;
                $data = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $result[] = $base64;
            }
        }

        echo json_encode($result);
    }

    public function det_orders_extra_update() {
        $array = json_decode($this->input->post('data'), true);
        $this->det_orders_extra_model->update($array);
    }

    public function restart() {

        /** Delete Order */
        if (pickup_isset()) {
            $this->orders_model->close();
        }

        redirect('orders/orders');
    }

    public function reback($order_id = '') {

        $this->load->model('orders_temp_model');

        $result = $this->orders_temp_model->get_temp($order_id);

        $this->session->set_userdata('temp_orders', $result);

        redirect('orders/orders');
    }

    public function preview() {
        $this->load->view('order/order/preview');
    }

    public function update_price() {

        // Load all resource needed
        $this->load->model('prices_model');

        // Get data from user forms
        $branch_origin = $this->input->post('branch_origin');
        $branch_destination = $this->input->post('branch_destination');
        $order_id = $this->input->post('order_id');

        // Package parent list
        $packages = $this->packages_model->get_single_item($order_id);

        // Update price
        foreach ($packages as $key => $item) {

            // Get new price
            $new_price = $this->prices_model->get_price(
                    $branch_origin, $branch_destination, $item['package_weight'], $item['package_size'], $item['det_order_qty']
            );

            // Price update 
            $this->det_orders_model->update([
                'det_order_total' => $new_price,
                'det_order_sell_total' => $new_price
                    ], [
                'det_order_package_parent' => $item['det_order_package_parent']
            ]);

            // Update origin and destination
            $this->packages_model->update_joined([
                'package_origin' => $branch_origin,
                'package_destination' => $branch_destination
                    ], [
                'det_order_package_parent' => $item['det_order_package_parent']
            ]);
        }

        return true;
    }

    public function restrict_tab() {
        if ($this->uri->uri_string() == 'orders/orders' && !$this->input->is_ajax_request()) {
            if ($this->session->userdata('order-temp') == '') {
                $this->session->set_userdata('order-temp', 'true');
            } else {
                redirect('program');
            }
        }
    }

}
