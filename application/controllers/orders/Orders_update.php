<?php

class Orders_update extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'order';
    public $controller_path = 'orders/order';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('orders_model');
            $this->load->model('packages_model');
            $this->load->model('package_edit_model');
            $this->load->model('package_edit_location_model');
            $this->load->model('package_edit_total_model');
            $this->load->model('package_edit_orders_extra_model');
            $this->load->model('det_orders_extra_model');
            $this->load->model('regionals_model');
            $this->load->model('users_model');
            $this->load->model('package_delete_model');
        } else {
            redirect('program/login');
        }
    }

    public function update_package() {

        $user_staff_id = profile()->user_id;
        $user_type_id = profile()->user_type_id;

        $user_regional = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }

        $packages = [
            'order_id' => $this->input->post('order_id'),
            'package_id' => $this->input->post('package_id'),
            'package_edit_title' => $this->input->post('package_title'),
            'package_edit_type' => $this->input->post('package_type'),
            'package_qty' => $this->input->post('package_qty'),
            'package_edit_width' => $this->input->post('package_width') / 100,
            'package_edit_height' => $this->input->post('package_height') / 100,
            'package_edit_lenght' => $this->input->post('package_lenght') / 100,
            'package_edit_weight' => $this->input->post('package_weight'),
            'package_edit_size' => $this->input->post('package_width') * $this->input->post('package_height') * $this->input->post('package_lenght'),
            'package_edit_content' => $this->input->post('package_content'),
            'user_staff_id' => $user_staff_id,
            'package_edit_date_updated' => time(),
            'branch_id' => (!empty(profile()->branch_id)) ? profile()->branch_id : '',
            'regional_id' => (!empty($user_regional)) ? $user_regional : '',
            'order_status' => '2',
            'package_edit_status' => '2',
            'created_at' => time(),
            'updated_at' => time()
        ];

        $insert = $this->package_edit_model->insert($packages);

        // email Admin Regional
        $data_regional = $this->regionals_model->get_regional_manager($user_regional);
        $email_regional = (!empty($data_regional)) ? $data_regional->user_email : '';

        // email Admin pusat
        $admin_pusat = $this->users_model->get_profile(['user.user_type_id ' => 120])->result();
        foreach ($admin_pusat as $key => $value) {
            $email_pusat[] = $value->user_email;
        }

        // email Super Admin
        $super_admin = $this->users_model->get_profile(['user.user_type_id' => 99])->result();
        foreach ($super_admin as $key => $value) {
            $email_superadmin[] = $value->user_email;
        }

        if ($insert == TRUE) {
            $insert_id = $this->db->insert_id();

            $this->send_mail($email_regional, $email_pusat, $email_superadmin);

            if ($user_type_id == '114') { // 114 = admin cabang
                $alert_type = 'alert-warning';
                $alert = "Dapatkan izin dari Admin Regional terlebih dulu untuk edit paket.";
            } else if ($user_type_id == '124' || $user_type_id == '120' || $user_type_id == '99') {
                //124= Admin Regional, 120 = Admin Pusat , 99 = Super Admin
                return redirect('orders/orders_approval/approve_edit_package/' . $insert_id);
            } else {
                $alert_type = 'alert-danger';
                $alert = "Perubahan data yang Anda lakukan perlu menuggu konfirmasi terlebih dahulu dari admin";
            }
        } else {
            $alert_type = 'alert-danger';
            $alert = "Permintaan perubahan data yang Anda lakukan ditolak !";
        }

        set_alert($alert_type, $alert);
        return redirect('orders/order/view/' . $this->input->post('order_id'), 'refresh');
    }

    public function update_origin() {

        $user_staff_id = profile()->user_id;
        $user_type_id = profile()->user_type_id;

        $user_regional = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }

        $json = [
            'city_id_origin' => $this->input->post('city_id_origin'),
            'districts_id_origin' => $this->input->post('districts_id_origin'),
            'village_id_origin' => $this->input->post('village_id_origin'),
            'order_origin_text' => $this->input->post('order_origin_text'),
        ];

        $data = [
            'order_id' => $this->input->post('order_id'),
            'pckg_edit_loc_type' => '1', //sender / from origin location
            'pckg_edit_loc_origin' => $this->input->post('branch_id_origin'),
            'pckg_edit_loc_origin_text' => json_encode($json),
            'user_staff_id' => $user_staff_id,
            'pckg_edit_loc_date_updated' => time(),
            'city_id' => $this->input->post('city_id_origin'),
            'branch_id' => (!empty(profile()->branch_id)) ? profile()->branch_id : '',
            'regional_id' => (!empty($user_regional)) ? $user_regional : '',
            'order_status' => '2', //need approval
            'created_at' => time(),
            'updated_at' => time(),
        ];

        $insert = $this->package_edit_location_model->insert($data);

        // email Admin Regional
        $data_regional = $this->regionals_model->get_regional_manager($user_regional);
        $email_regional = (!empty($data_regional)) ? $data_regional->user_email : '';

        // email Admin pusat
        $admin_pusat = $this->users_model->get_profile(['user.user_type_id ' => 120])->result();
        foreach ($admin_pusat as $key => $value) {
            $email_pusat[] = $value->user_email;
        }

        // email Super Admin
        $super_admin = $this->users_model->get_profile(['user.user_type_id' => 99])->result();
        foreach ($super_admin as $key => $value) {
            $email_superadmin[] = $value->user_email;
        }

        if ($insert == TRUE) {
            $insert_id = $this->db->insert_id();

            $this->send_mail($email_regional, $email_pusat, $email_superadmin);

            if ($user_type_id == '114') { // 114 = admin cabang
                $alert_type = 'alert-warning';
                $alert = "Dapatkan izin dari Admin Regional terlebih dulu untuk edit paket.";
            } else if ($user_type_id == '124' || $user_type_id == '120' || $user_type_id == '99') {
                //124= Admin Regional, 120 = Admin Pusat , 99 = Super Admin
                return redirect('orders/orders_approval/approve_edit_location/' . $insert_id);
            } else {
                $alert_type = 'alert-danger';
                $alert = "Perubahan data yang Anda lakukan perlu menuggu konfirmasi terlebih dahulu dari admin";
            }
        } else {
            $alert_type = 'alert-danger';
            $alert = "Permintaan perubahan data yang Anda lakukan ditolak !";
        }

        set_alert($alert_type, $alert);
        return redirect('orders/order/view/' . $this->input->post('order_id'), 'refresh');
    }

    public function update_destination() {

        $user_staff_id = profile()->user_id;
        $user_type_id = profile()->user_type_id;

        $user_regional = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }

        $json = [
            'city_id_destination' => $this->input->post('city_id_destination'),
            'districts_id_destination' => $this->input->post('districts_id_destination'),
            'village_id_destination' => $this->input->post('village_id_destination'),
            'order_destination_text' => $this->input->post('order_destination_text')
        ];

        $data = [
            'order_id' => $this->input->post('order_id'),
            'pckg_edit_loc_type' => '2', //receiver / for destination location
            'pckg_edit_loc_destination' => $this->input->post('branch_id_destination'),
            'pckg_edit_loc_destination_text' => json_encode($json),
            'user_staff_id' => $user_staff_id,
            'pckg_edit_loc_date_updated' => time(),
            'city_id' => $this->input->post('city_id_destination'),
            'branch_id' => (!empty(profile()->branch_id)) ? profile()->branch_id : '',
            'regional_id' => (!empty($user_regional)) ? $user_regional : '',
            'order_status' => '2', //need approved
            'created_at' => time(),
            'updated_at' => time(),
        ];

        $insert = $this->package_edit_location_model->insert($data);

        // email Admin Regional
        $data_regional = $this->regionals_model->get_regional_manager($user_regional);
        $email_regional = (!empty($data_regional)) ? $data_regional->user_email : '';

        // email Admin pusat
        $admin_pusat = $this->users_model->get_profile(['user.user_type_id ' => 120])->result();
        foreach ($admin_pusat as $key => $value) {
            $email_pusat[] = $value->user_email;
        }

        // email Super Admin
        $super_admin = $this->users_model->get_profile(['user.user_type_id' => 99])->result();
        foreach ($super_admin as $key => $value) {
            $email_superadmin[] = $value->user_email;
        }

        if ($insert == TRUE) {
            $insert_id = $this->db->insert_id();

            $this->send_mail($email_regional, $email_pusat, $email_superadmin);

            if ($user_type_id == '114') { // 114 = admin cabang
                $alert_type = 'alert-warning';
                $alert = "Dapatkan izin dari Admin Regional terlebih dulu untuk edit paket.";
            } else if ($user_type_id == '124' || $user_type_id == '120' || $user_type_id == '99') {
                //124= Admin Regional, 120 = Admin Pusat , 99 = Super Admin
                return redirect('orders/orders_approval/approve_edit_location/' . $insert_id);
            } else {
                $alert_type = 'alert-danger';
                $alert = "Perubahan data yang Anda lakukan perlu menuggu konfirmasi terlebih dahulu dari admin";
            }
        } else {
            $alert_type = 'alert-danger';
            $alert = "Permintaan perubahan data yang Anda lakukan ditolak !";
        }

        set_alert($alert_type, $alert);
        return redirect('orders/order/view/' . $this->input->post('order_id'), 'refresh');
    }

    public function update_total_order_price() {

        $user_staff_id = profile()->user_id;
        $user_type_id = profile()->user_type_id;

        $user_regional = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }

        $packages = [
            'order_id' => $this->input->post('order_id'),
            'total_order_price' => $this->input->post('det_total_order_price'),
            'user_staff_id' => $user_staff_id,
            'date_updated' => time(),
            'branch_id' => (!empty(profile()->branch_id)) ? profile()->branch_id : '',
            'regional_id' => (!empty($user_regional)) ? $user_regional : '',
            'order_status' => '2',
            'created_at' => time(),
            'updated_at' => time(),
        ];

        $insert = $this->package_edit_total_model->insert($packages);

        // email Admin Regional
        $data_regional = $this->regionals_model->get_regional_manager($user_regional);
        $email_regional = (!empty($data_regional)) ? $data_regional->user_email : '';
    
        // email Admin pusat
        $admin_pusat = $this->users_model->get_profile(['user.user_type_id ' => 120])->result();
        foreach ($admin_pusat as $key => $value) {
            $email_pusat[] = $value->user_email;
        }

        // Email Super Admin
        $super_admin = $this->users_model->get_profile(['user.user_type_id' => 99])->result();
        foreach ($super_admin as $key => $value) {
            $email_superadmin[] = $value->user_email;
        }

        if ($insert == TRUE) {
            $insert_id = $this->db->insert_id();

            $this->send_mail($email_regional, $email_pusat, $email_superadmin);

            if ($user_type_id == '114') { // 114 = admin cabang
                $alert_type = 'alert-warning';
                $alert = "Dapatkan izin dari Admin Regional terlebih dulu untuk edit paket.";
            } else if ($user_type_id == '124' || $user_type_id == '120' || $user_type_id == '99') {
                //124= Admin Regional, 120 = Admin Pusat , 99 = Super Admin
                return redirect('orders/orders_approval/approve_edit_total_price/' . $insert_id);
            } else {
                $alert_type = 'alert-danger';
                $alert = "Perubahan data yang Anda lakukan perlu menuggu konfirmasi terlebih dahulu dari admin";
            }
        } else {
            $alert_type = 'alert-danger';
            $alert = "Permintaan perubahan data yang Anda lakukan ditolak !";
        }

        set_alert($alert_type, $alert);
        return redirect('orders/order/view/' . $this->input->post('order_id'), 'refresh');
    }

    public function update_orders_extra() {

        $user_staff_id = profile()->user_id;
        $user_type_id = profile()->user_type_id;

        $user_regional = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }

        $packages = [
            'order_id' => $this->input->post('order_id'),
            'orders_extra_id' => $this->input->post('det_order_extra_id'),
            'orders_extra_name' => $this->input->post('det_order_extra_name'),
            'orders_extra_total' => $this->input->post('det_order_extra_total'),
            'user_staff_id' => $user_staff_id,
            'date_updated' => time(),
            'branch_id' => (!empty(profile()->branch_id)) ? profile()->branch_id : '',
            'regional_id' => (!empty($user_regional)) ? $user_regional : '',
            'order_status' => '2',
            'created_at' => time(),
            'updated_at' => time(),
        ];

        $insert = $this->package_edit_orders_extra_model->insert($packages);

        // email Admin Regional
        $data_regional = $this->regionals_model->get_regional_manager($user_regional);
        $email_regional = (!empty($data_regional)) ? $data_regional->user_email : '';

        // email Admin pusat
        $admin_pusat = $this->users_model->get_profile(['user.user_type_id ' => 120])->result();
        foreach ($admin_pusat as $key => $value) {
            $email_pusat[] = $value->user_email;
        }

        // email Super Admin
        $super_admin = $this->users_model->get_profile(['user.user_type_id' => 99])->result();
        foreach ($super_admin as $key => $value) {
            $email_superadmin[] = $value->user_email;
        }

        if ($insert == TRUE) {
            $insert_id = $this->db->insert_id();

            $this->send_mail($email_regional, $email_pusat, $email_superadmin);

            if ($user_type_id == '114') { // 114 = admin cabang
                $alert_type = 'alert-warning';
                $alert = "Dapatkan izin dari Admin Regional terlebih dulu untuk edit paket.";
            } else if ($user_type_id == '124' || $user_type_id == '120' || $user_type_id == '99') {
                //124= Admin Regional, 120 = Admin Pusat , 99 = Super Admin
                return redirect('orders/orders_approval/approve_edit_orders_extra/' . $insert_id);
            } else {
                $alert_type = 'alert-danger';
                $alert = "Perubahan data yang Anda lakukan perlu menuggu konfirmasi terlebih dahulu dari admin";
            }
        } else {
            $alert_type = 'alert-danger';
            $alert = "Permintaan perubahan data yang Anda lakukan ditolak !";
        }

        set_alert($alert_type, $alert);
        return redirect('orders/order/view/' . $this->input->post('order_id'), 'refresh');
    }

    public function send_mail($email_regional, $email_pusat = array(), $email_superadmin = array()) {

        $user_type_id = profile()->user_type_id;

        // Load all resource needed
        $this->config->load('email');
        $this->load->library('email');

        if ($user_type_id == '114') { // 114 = admin cabang
            $to_email = $email_regional;
            $email = array_merge($email_pusat, $email_superadmin);
            $email = implode(', ', $email);
            $cc_email = $email;
        } else if ($user_type_id == '124') { //124= Admin Regional
            $email_pusat = implode(', ', $email_pusat);
            $to_email = $email_pusat;
            $email_superadmin = implode(', ', $email_superadmin);
            $cc_email = $email_superadmin;
        } elseif ($user_type_id == '120' || $user_type_id == '99') { //120 = Admin Pusat , 99 = Super Admin
            $email_superadmin = implode(', ', $email_superadmin);
            $to_email = $email_superadmin;
            $cc_email = '';
        }

        $this->email->initialize($this->config->item('email'));

        $this->email->from('paketstsindonesia@gmail.com', 'noreply');
        $this->email->to($to_email);
        $this->email->cc($cc_email);
        $this->email->subject('Request Edit Order Approval');
        $this->email->message('Hello user, there is a request for Edit Data Order Approval from user:' . profile()->user_id . ' - ' . profile()->name . ' - ' . profile()->branch_name);
        if ($this->email->send()) {
            echo "Mail Sent!";
            #echo $this->email->print_debugger();
        } else {
            echo "There is error in sending mail!";
            #echo $this->email->print_debugger();
        }
    }

    public function package_destroy($id = '') {
        $order_id = $id;
        $package_id = $this->uri->segment(5);

        $user_staff_id = profile()->user_id;
        $user_type_id = profile()->user_type_id;

        $user_regional = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }

        $packages = [
            'order_id' => $order_id,
            'package_id' => $package_id,
            'user_staff_id' => $user_staff_id,
            'date_updated' => time(),
            'branch_id' => (!empty(profile()->branch_id)) ? profile()->branch_id : '',
            'regional_id' => (!empty($user_regional)) ? $user_regional : '',
            'package_delete_status' => '2', //delete pending
            'created_at' => time(),
            'updated_at' => time()
        ];

        $insert = $this->package_delete_model->insert($packages);

        // email Admin Regional
        $data_regional = $this->regionals_model->get_regional_manager($user_regional);
        $email_regional = (!empty($data_regional)) ? $data_regional->user_email : '';

        // email Admin pusat
        $admin_pusat = $this->users_model->get_profile(['user.user_type_id ' => 120])->result();
        foreach ($admin_pusat as $key => $value) {
            $email_pusat[] = $value->user_email;
        }

        // email Super Admin
        $super_admin = $this->users_model->get_profile(['user.user_type_id' => 99])->result();
        foreach ($super_admin as $key => $value) {
            $email_superadmin[] = $value->user_email;
        }

        if ($insert == TRUE) {
            $insert_id = $this->db->insert_id();

            if ($user_type_id == '114') { // 114 = admin cabang
                $alert_type = 'alert-warning';
                $alert = "Dapatkan izin dari Admin Regional terlebih dulu untuk edit paket.";
            } else if ($user_type_id == '124' || $user_type_id == '120' || $user_type_id == '99') {
                //124= Admin Regional, 120 = Admin Pusat , 99 = Super Admin
                return redirect('orders/orders_approval/approve_package_delete/' . $insert_id);
            } else {
                $alert_type = 'alert-danger';
                $alert = "Perubahan data yang Anda lakukan perlu menuggu konfirmasi terlebih dahulu dari admin";
            }

            $this->send_mail($email_regional, $email_pusat, $email_superadmin);
        } else {
            $alert_type = 'alert-danger';
            $alert = "Permintaan perubahan data yang Anda lakukan ditolak !";
        }

        set_alert($alert_type, $alert);
        return redirect('orders/order/view/' . $order_id, 'refresh');
    }

}
