<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'booking';
    public $controller_path = 'orders/booking';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_booking";

            $this->load->model('orders_model');
            $this->load->library('request/request_cities');
        } else {
            redirect('program/login');
        }
    }

    public function index() {
        if ($this->input->is_ajax_request()) {
            // Load all resource needed
            $this->load->library('datatable/dt_booking');
            echo $this->dt_booking->get($this, $_REQUEST);
        } else {
            $this->data['title'] = "Daftar Booking Resi";
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add() {
        $this->data['title'] = "Buat Booking Resi";
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store() {

        // Load all resource needed
        $this->load->model('orders_model');
        $this->load->library('request/request_booking_order');

        // Check is customer exist
        $customer_id = $this->check_customers();

        // Validation input
        if (!$this->request_booking_order->validation()) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            // Insert order
            $data_order = array(
                'order_id' => $this->orders_model->generate_resi(),
                'order_manual_id' => $this->input->post('branch_code') . $this->input->post('resi_manual'),
                'order_branch_code' => $this->input->post('branch_code'),
                'order_manual_resi_pure' => $this->input->post('resi_manual'),
                'order_status' => -4,
                'staff_id' => profile()->user_id,
                'customer_id' => $customer_id,
                'order_date' => strtotime($this->input->post('order_date')),
                'order_origin' => $this->input->post('branch_origin'),
                'order_destination' => $this->input->post('branch_destination'),
                'created_at' => time()
            );
            $this->orders_model->insert($data_order);

            // Only run when package quantity more than zero
            if ($this->input->post('package_qty') > 0) {

                // Load all resource needed
                $this->load->model('packages_model');
                $this->load->model('det_orders_model');

                // Loops for total package item
                for ($i = 0; $i < $this->input->post('package_qty'); $i++) {

                    // Insert package
                    $data_package = array(
                        'package_title' => '-',
                        'package_origin' => $this->input->post('branch_origin'),
                        'package_destination' => $this->input->post('branch_destination'),
                        'package_status' => -1,
                        'created_at' => time()
                    );
                    $package_id = $this->packages_model->insert($data_package);

                    // Insert det order
                    $data_det_order = array(
                        'order_id' => $data_order['order_id'],
                        'package_id' => $package_id,
                        'det_order_package_parent' => $package_id
                    );
                    $this->det_orders_model->insert($data_det_order);
                }
            }

            // Set alert message
            set_alert('alert-success', 'Booking order dengan nomor resi <strong>' . $data_order['order_id'] . '</strong> berhasil dibuat');

            // Redirect into list booking order
            redirect($this->controller_path);
        }
    }

    public function edit($order_id = '') {

        // Load all resource needed
        $this->load->model('orders_model');
        $this->load->model('booking_model');
        $this->load->model('costumers_model');

        // Get detail order
        $detail = $this->orders_model->get(['order_id' => $order_id])->row_array();
        $detail['package_qty'] = $this->booking_model->count_package($order_id);
        $detail['customer_name'] = $this->costumers_model->get_name($detail['customer_id']);

        // Fill container variable with data
        $this->data['data'] = $detail;
        $this->data['title'] = "Edit Booking Resi";

        // Load page 
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update($order_id = '') {

        // Load all resource needed
        $this->load->model('orders_model');
        $this->load->model('booking_model');
        $this->load->library('request/request_booking_order');

        // Check is customer exist
        $customer_id = $this->check_customers();

        // Validation input
        if (!$this->request_booking_order->validation(['id' => $order_id])) {
            redirect($this->controller_path . '/edit/' . $order_id, 'refresh');
        } else {

            // Collected order data
            $data_order = array(
                'order_id' => $order_id,
                'order_manual_id' => $this->input->post('branch_code') . $this->input->post('resi_manual'),
                'order_branch_code' => $this->input->post('branch_code'),
                'order_manual_resi_pure' => $this->input->post('resi_manual'),
                'order_status' => -4,
                'staff_id' => profile()->user_id,
                'customer_id' => $customer_id,
                'order_date' => strtotime($this->input->post('order_date')),
                'order_origin' => $this->input->post('branch_origin'),
                'order_destination' => $this->input->post('branch_destination'),
                'updated_at' => time()
            );

            // Update order
            $this->orders_model->update($data_order);

            // Get current package quantity
            $package_qty = $this->booking_model->count_package($order_id);

            // Update package 
            $update_package = array(
                'package_origin' => $this->input->post('branch_origin'),
                'package_destination' => $this->input->post('branch_destination'),
                'updated_at' => time()
            );
            $this->booking_model->update_package($order_id, $update_package);

            /*
             * Compare package quantity only run when current package quantity 
             * more bigger than new package quantity
             */
            if ($package_qty != $this->input->post('package_qty')) {

                // Load all resource needed
                $this->load->model('packages_model');
                $this->load->model('det_orders_model');

                if ($package_qty < $this->input->post('package_qty')) {

                    // Get the difference
                    $difference = $this->input->post('package_qty') - $package_qty;

                    // Insert excess
                    for ($i = 0; $i < $difference; $i++) {

                        // Insert package
                        $data_package = array(
                            'package_title' => '-',
                            'package_origin' => $this->input->post('branch_origin'),
                            'package_destination' => $this->input->post('branch_destination'),
                            'package_status' => -1,
                            'created_at' => time()
                        );
                        $package_id = $this->packages_model->insert($data_package);

                        // Insert det order
                        $data_det_order = array(
                            'order_id' => $data_order['order_id'],
                            'package_id' => $package_id,
                            'det_order_package_parent' => $package_id
                        );
                        
                        $this->det_orders_model->insert($data_det_order);
                    }
                } elseif ($package_qty > $this->input->post('package_qty')) {

                    // Get difference
                    $difference = $package_qty - $this->input->post('package_qty');

                    // Delete excess
                    $this->booking_model->delete_package($order_id, $difference);
                }
            }

            // Set alert message
            set_alert('alert-success', 'Booking order dengan nomor resi <strong>' . $data_order['order_id'] . '</strong> berhasil diperbarui');

            // Redirect into list booking order
            redirect($this->controller_path);
        }
    }

    public function destroy($order_id = '') {

        // Load all resource needed
        $this->load->model('orders_model');
        $this->load->model('packages_model');

        // Destroy orders
        $this->orders_model->destroy($order_id);

        // Destroy package and detail order
        $this->packages_model->destroy($order_id);

        // Set alert message
        set_alert('alert-success', 'Booking order dengan nomor resi <strong>' . $order_id . '</strong> telah dihapus');

        // Redirect into list booking order
        redirect($this->controller_path);
    }

    private function check_customers() {

        if ($this->input->post('customer_id') == '') {

            // New Customer
            $this->load->model('costumers_model');
            $data_customer = array(
                'customer_name' => $this->input->post('customer_name')
            );
            return $this->costumers_model->insert($data_customer);
        } else {

            // Existed Customer
            return $this->input->post('customer_id');
        }
    }

    public function create_order($order_id = '') {

        $this->load->model('orders_temp_model');

        $result = $this->orders_temp_model->get_temp($order_id);

        $this->session->set_userdata('temp_orders', $result);

        redirect('orders/orders');
    }

}
