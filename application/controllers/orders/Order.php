<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'order';
    public $controller_path = 'orders/order';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";
        } else {
            redirect('program/login');
        }
    }

    public function index($status = 0) {

        $this->data['title'] = "Daftar Order";

        if ($this->input->is_ajax_request()) {
            $this->load->library('datatable/dt_order');
            echo $this->dt_order->get($this, $status, $_REQUEST);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function get_total($status = 0) {

        if ($this->input->is_ajax_request()) {

            // Load all resource needed
            $this->load->model('order_list_model');
            $profile = profile();

            // Get total paramater preparing
            $attr = array();
            $attr['status'] = $status;

            // When user restricted by branches access
            $attr['branch'] = $profile->branch_name;
            $attr['branch_access'] = $profile->branch_access;

            // When user restricted by regional access
            if ($profile->regional_id != '') {

                // Load all resource needed
                $this->load->model('regionals_model');

                // Get all branches listed name under choiced regional
                $attr['regional'] = $this->regionals_model->get_branches_name($this->profile->regional_id);
            }

            // Get total result from order list model
            $result = $this->order_list_model->get_totalize($attr);
            echo json_encode($result);
            return true;
        }
    }

    public function view($id = 0, $from = '') {

        // Show error when id is empty
        if (empty($id)) {
            $this->load->view('errors/html/error_404');
            echo "<script>alert('id not found')</script>";
            redirect('orders/order/index', 'refresh');
        }

        // Load all resource needed
        $this->load->model('orders_model');
        $this->load->model('cities_model');
        $this->load->model('branches_model');
        $this->load->model('packages_model');
        $this->load->model('det_orders_model');
        $this->load->model('districts_model');
        $this->load->model('villages_model');
        $this->load->model('det_orders_extra_model');

        $this->data['from'] = $from;
        $this->data['cities'] = $this->cities_model->get()->result_array();
        $this->data['title'] = "Resi No " . $id;
        $this->data['data'] = $this->orders_model->get_joined(['orders.order_id' => $id])->row();

        if (!empty($this->data['data'])) {
            $origin_text = json_decode($this->data['data']->order_origin_text, true);
            $destination_text = json_decode($this->data['data']->order_destination_text, true);
        }

        if (!empty($origin_text) || !empty($destination_text)) {
            /** Full address origin */
            if ($origin_text['districts_id_origin'] != '') {
                $this->data['origin_districs'] = $this->districts_model->get(['districts_id' => $origin_text['districts_id_origin']])->row();
            }
            if ($origin_text['village_id_origin'] != '') {
                $this->data['origin_villages'] = $this->villages_model->get(['village_id' => $origin_text['village_id_origin']])->row();
            }

            /** Full address destination */
            if ($destination_text['districts_id_destination'] != '') {
                $this->data['destination_districs'] = $this->districts_model->get(['districts_id' => $destination_text['districts_id_destination']])->row();
            }

            if ($destination_text['village_id_destination'] != '') {
                $this->data['destination_villages'] = $this->villages_model->get(['village_id' => $destination_text['village_id_destination']])->row();
            }
        }

        if (!empty($this->data['data'])) {
            $this->data['origin_branch'] = $this->branches_model->get_joined_address(
                            ['branches.branch_id' => $this->data['data']->order_origin]
                    )->row();

            $this->data['destination_branch'] = $this->branches_model->get_joined_address(
                            ['branches.branch_id' => $this->data['data']->order_destination]
                    )->row();
        }

        $this->data['package'] = $this->packages_model->get_joined(['order_id' => $id], ['group_by' => ['det_order.det_order_package_parent']])->result();

        $this->data['package_total'] = $this->packages_model->get_packages_view(['order_id' => $id])->row();

        $this->data['det_order_extra'] = $this->det_orders_extra_model->get(['order_id' => $id])->result();

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function destroy($id = 0) {

        /** Load all resource needed */
        $this->load->model('orders_model');

        $data = $this->orders_model->delete(['order_id' => $id]);

        set_alert('alert-danger', 'Order dengan kode resi <strong>' . $data->row()->order_id . '</strong> telah dihapus');

        redirect($this->controller_path);
    }

    public function add_package($order_id = null, $update_order = 'false') {

        $this->load->model('packages_model');

        $package_id = $this->packages_model->retrieve_input($order_id);

        $this->det_orders_model->retrieve_input($package_id, $order_id, $update_order);

        /** Update package if true */
        if ($update_order == 'true') {
            $this->orders_model->update_total_price($order_id);
        }

        redirect($this->controller_path . '/view/' . $order_id);
    }

    public function close($order_id = 0) {

        // Load all resource needed
        $this->load->model('orders_model');

        $type_pembayaran = $this->orders_model->get_payment_type($order_id);

        $status = ($type_pembayaran == 1) ? 2 : 3;

        $this->orders_model->update_status($order_id, $status);

        set_alert('alert-success', 'Order dengan nomor resi <strong>' . $order_id . '</strong> telah ditutup');

        redirect($this->controller_path);
    }

    public function paid($order_id = 0) {

        /** Load all resource needed */
        $this->load->model('orders_model');

        $this->orders_model->update_status($order_id, 2);

        set_alert('alert-success', 'Order dengan nomor resi <strong>' . $order_id . '</strong> telah lunas');

        redirect($this->controller_path);
    }

    public function self_take() {

        // Load all resource needed
        $this->load->model('accepted_report_model');
        $this->load->model('orders_model');

        // Collected data from user form
        $data = [
            'type' => 1,
            'order_id' => $this->input->post('order_id'),
            'accepted_name' => $this->input->post('accepted_name'),
            'created_at' => time(),
        ];

        // Insert into accepted report table
        $this->accepted_report_model->insert($data);

        // If paid type is franco, insert information into franco list
        if ($this->orders_model->is_franco($this->input->post('order_id'))) {

            // Load all resource needed
            $this->load->model('franco_model');

            // Insert into franco table
            $this->franco_model->create($this->input->post('order_id'), 1);
        }

        // Update status order from 1 into 2
        $this->orders_model->update_status($this->input->post('order_id'), 2);
        $this->orders_model->update_received_date($this->input->post('order_id'), time());

        // Set success alert to page
        set_alert('alert-success', 'Order dengan nomor resi '
                . '<strong>' . $this->input->post('order_id') . '</strong> '
                . 'telah diambil oleh '
                . '<strong>' . $this->input->post('accepted_name') . '</strong>');

        // Redirect to page list of orders 
        redirect('orders/order');
    }

    public function delivery() {

        /** Load all resource needed */
        $this->load->model('accepted_report_model');
        $this->load->model('orders_model');

        $data = [
            'type' => 2,
            'order_id' => $this->input->post('order_id'),
            'accepted_name' => $this->input->post('accepted_name'),
            'created_at' => time(),
        ];

        $this->accepted_report_model->insert($data);

        // If paid type is franco, insert information into franco list
        if ($this->orders_model->is_franco($this->input->post('order_id'))) {

            // Load all resource needed
            $this->load->model('franco_model');

            // Insert into franco table
            $this->franco_model->create($this->input->post('order_id'), 1);
        }

        $this->orders_model->update_status($this->input->post('order_id'), 2);
        $this->orders_model->update_received_date($this->input->post('order_id'), time());

        set_alert('alert-success', 'Order dengan nomor resi '
                . '<strong>' . $this->input->post('order_id') . '</strong> '
                . 'telah diterima oleh '
                . '<strong>' . $this->input->post('accepted_name') . '</strong>');

        redirect('orders/order');
    }

    public function package_rollback() {

        // Load all resource needed
        $this->load->model('cancel_report_model');
        $this->load->model('packages_model');

        $data = [
            'order_id' => $this->input->post('order_id'),
            'note' => $this->input->post('note'),
            'created_at' => time(),
        ];

        $this->cancel_report_model->insert($data);

        /** Update all package status from 7 as delivery into 5 as finish */
        $this->packages_model->update_joined(
                ['package_status' => 5], ['det_order.order_id' => $this->input->post('order_id')]
        );

        set_alert('alert-success', 'Order dengan nomor resi '
                . '<strong>' . $this->input->post('order_id') . '</strong> '
                . 'telah dibatalkan dari daftar pengantaran ');

        redirect('orders/order');
    }

    public function filter() {

        // Load all resource needed
        $this->load->model('branches_model');

        // Set filter session value
        $data['as_branch'] = $this->input->post('as_branch');
        $data['as_branch_name'] = $this->branches_model->get_branch_name(
                $this->input->post('as_branch')
        );
        $this->session->set_userdata('order_list_filter', $data);

        redirect($this->controller_path);
    }

    public function status_filter_willdrop() {
        $this->session->set_userdata('status_filter_willdrop', $this->input->post('status_filter_willdrop'));
        redirect($this->controller_path);
    }

    public function status_filter_inprocess() {
        $this->session->set_userdata('status_filter_inprocess', $this->input->post('status_filter_inprocess'));
        redirect($this->controller_path);
    }

    public function reset_status_filter() {
        $this->session->unset_userdata('status_filter_willdrop');
        $this->session->unset_userdata('status_filter_inprocess');
        $this->session->unset_userdata('dt_order');
        $this->session->unset_userdata('order_list_filter');
        redirect($this->controller_path);
    }

    public function set_paid($order_id = '') {

        // Load orders model 
        $this->load->model('orders_model');

        $this->orders_model->update_payment_status($order_id, '1');

        set_alert('alert-success', 'No. Resi '.$order_id.' sudah lunas');

        redirect('orders/order/view/' . $order_id);
        
    }

    public function set_unpaid($order_id = '') {

        // Load orders model 
        $this->load->model('orders_model');

        $this->orders_model->update_payment_status($order_id, '0');
        
        set_alert('alert-success', 'No. Resi '.$order_id.' belum lunas');

        redirect('orders/order/view/' . $order_id);
        
    }

}
