<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_approval extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'order/order_approval';
    public $controller_path = 'orders/orders_approval';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "m1";

            $this->load->model('orders_model');
            $this->load->model('cities_model');
            $this->load->model('branches_model');
            $this->load->model('packages_model');
            $this->load->model('det_orders_model');
            $this->load->model('districts_model');
            $this->load->model('villages_model');
            $this->load->model('det_orders_extra_model');
            $this->load->model('det_orders_model');
            $this->load->model('package_edit_model');
            $this->load->model('package_edit_location_model');
            $this->load->model('package_edit_total_model');
            $this->load->model('package_edit_orders_extra_model');
            $this->load->model('package_delete_model');
            $this->load->model('users_model');
            $this->load->model('regionals_model');

            $this->load->library('datatable/dt_order_approval');
            $this->load->library('datatable/dt_order_approval_location');
            $this->load->library('datatable/dt_order_approval_total');
            $this->load->library('datatable/dt_order_approval_orders_extra');
            $this->load->library('datatable/dt_order_approval_package_delete');

            $this->data['cities'] = $this->cities_model->get()->result_array();
        } else {
            return redirect('program/login');
        }
    }

    public function index($status = '') {

        $this->data['title'] = "Daftar Permintaan Edit Order Paket";

        $user_type_id = profile()->user_type_id;

        $user_regional       = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }

        if ($this->input->is_ajax_request()) {
            $status = (empty($status)) ? '2' : $status;
            echo $this->dt_order_approval->get_package($this, $status);
        } else {
            return $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function location_dt($status = '')
    {
        if ($this->input->is_ajax_request()) {
            $status = (empty($status)) ? '2' : $status;
            echo $this->dt_order_approval_location->get_location($this, $status);
        } 
    }

    public function total_dt($status = '')
    {
        if ($this->input->is_ajax_request()) {
            $status = (empty($status)) ? '2' : $status;
            echo $this->dt_order_approval_total->get_total($this, $status);
        } 
    }

    public function orders_extra_dt($status = '')
    {
        if ($this->input->is_ajax_request()) {
            $status = (empty($status)) ? '2' : $status;
            echo $this->dt_order_approval_orders_extra->get_orders_extra($this, $status);
        } 
    }

    public function package_delete_dt($status = '')
    {
        if ($this->input->is_ajax_request()) {
            $status = (empty($status)) ? '2' : $status;
            echo $this->dt_order_approval_package_delete->get_package_delete($this, $status);
        } 
    }

    public function approve_edit_package($id = '') {
        if (!empty($id)) {
            $package_edit_id = $id;
        } else {
            $package_edit_id = $this->input->post('id');
        }

        $user_staff_id  = profile()->user_id;
        $user_type_id   = profile()->user_type_id;

        $data_approve = [
        'package_edit_id'               => $package_edit_id,
        'user_approved_staff_id'        => $user_staff_id,
        'package_edit_date_approved'    => time(),
        'order_status'                  => '1',  //approved
        'package_edit_status'           => '1',  //approved
        'updated_at'                    => time(),
        ];

        $approve = $this->package_edit_model->approve($data_approve);

        $package = $this->package_edit_model->get_package_edit(['package_edit_id' => $package_edit_id])->row();

        $data_package = [
        'package_id'        => $package->package_id,
        'package_title'     => $package->package_edit_title,
        'package_type'      => $package->package_edit_type,
        'package_width'     => $package->package_edit_width,
        'package_height'    => $package->package_edit_height,
        'package_lenght'    => $package->package_edit_lenght,
        'package_weight'    => $package->package_edit_weight,
        'package_size'      => $package->package_edit_width * $package->package_edit_height * $package->package_edit_lenght,
        'package_content'   => $package->package_edit_content,
        ];

        $update_package = $this->packages_model->update($data_package);

        $detail_order = $this->det_orders_model->get(['package_id' => $package->package_id, 'order_id' => $package->order_id])->row();

        $data_package_detail_order = array(
            'det_order_id'  => $detail_order->det_order_id,
            'package_id'    => $package->package_id,
            'order_id'      => $package->order_id,
            'det_order_qty' => $package->package_qty,
            );
        $this->det_orders_model->update($data_package_detail_order);

        $status = 1;
        $this->get_mail($package, $status);

        set_alert('alert-success', 'Permintaan berhasil diperbaharui');

        return redirect('orders/order/view/' . $package->order_id, 'refresh');
    }

    public function approve_edit_location($id = '') {

        if (!empty($id)) {
            $pckg_edit_loc_id = $id;
        } else {
            $pckg_edit_loc_id = $this->input->post('id');
        }

        $user_staff_id  = profile()->user_id;
        $user_type_id   = profile()->user_type_id;

        $location = $this->package_edit_location_model->get_package_edit(['pckg_edit_loc_id' => $pckg_edit_loc_id])->row();

        if (!empty($location)) {
            if ($location->type_edit == '1') { //edit data asal paket

                $data_approve = [
                'pckg_edit_loc_id'              => $pckg_edit_loc_id,
                'user_approved_staff_id'        => $user_staff_id,
                'pckg_edit_loc_date_approved'   => time(),
                'order_status'                  => '1', //approved
                'updated_at'                    => time(),
                ];
                $approve = $this->package_edit_location_model->approve($data_approve);

                $data = [
                'order_id'          => $location->order_id,
                'order_origin'      => $location->pckg_edit_loc_origin,
                'order_origin_text' => $location->pckg_edit_loc_origin_text,
                ];
                $this->orders_model->update($data);

                $packages = [
                'det_order.order_id'    => $location->order_id,
                'package_origin'        => $location->pckg_edit_loc_origin,
                ];
                $this->packages_model->update_joined($packages);

            } else if ($location->type_edit == '2') { //edit data tujuan paket

                $data_approve = [
                'pckg_edit_loc_id'              => $pckg_edit_loc_id,
                'user_approved_staff_id'        => $user_staff_id,
                'pckg_edit_loc_date_approved'   => time(),
                'order_status'                  => '1', //approved
                'updated_at'                    => time(),
                ];
                $approve = $this->package_edit_location_model->approve($data_approve);

                $data = [
                'order_id'                  => $location->order_id,
                'order_destination'         => $location->pckg_edit_loc_destination,
                'order_destination_text'    => $location->pckg_edit_loc_destination_text,
                ];
                $this->orders_model->update($data);

                $packages = [
                'det_order.order_id'    => $location->order_id,
                'package_destination'   => $location->pckg_edit_loc_destination
                ];
                $this->packages_model->update_joined($packages);

            } else {
                set_alert('alert-danger', 'Permintaan ditolak');
                return redirect($this->controller_path, 'refresh');
            }
        }

        $status = 1;
        $this->get_mail($location, $status);

        set_alert('alert-success', 'Permintaan berhasil diperbaharui');
        return redirect('orders/order/view/' . $location->order_id, 'refresh');
    }

    public function approve_edit_total_price($id = '') {

        if (!empty($id)) {
            $pckg_edit_total_id = $id;
        } else {
            $pckg_edit_total_id = $this->input->post('id');
        }

        $user_staff_id  = profile()->user_id;
        $user_type_id   = profile()->user_type_id;

        $total_price = $this->package_edit_total_model->get_package_edit(['pckg_edit_total_id' => $pckg_edit_total_id])->row();

        if (!empty($total_price)) {
            $data_approve = [
            'pckg_edit_total_id'        => $pckg_edit_total_id,
            'user_approved_staff_id'    => $user_staff_id,
            'date_approved'             => time(),
            'order_status'              => '1', //approved
            'updated_at'                => time(),
            ];

            $approve    = $this->package_edit_total_model->approve($data_approve);
            $order      = $this->orders_model->get(['order_id' => $total_price->order_id])->row();

            $order_use_tax      = $order->order_use_tax;
            $order_sell_total   = $total_price->total_order_price;

            if ($order_use_tax == 1) {
                $order_sell_total = $total_price->total_order_price * (100 / 101);
            }

            $tax = ($order_sell_total / 100) * 1;

            $data = [
            'order_id'          => $total_price->order_id,
            'order_sell_total'  => $order_sell_total + $tax,
            ];

            $this->orders_model->update($data);
        } else {
            set_alert('alert-danger', 'Permintaan ditolak');
            return redirect($$this->controller_path, 'refresh');
        }

        $status = 1;
        $this->get_mail($total_price, $status);

        set_alert('alert-success', 'Permintaan berhasil diperbaharui');
        return redirect('orders/order/view/' . $total_price->order_id, 'refresh');
    }

    public function approve_edit_orders_extra($id = '') {

        if (!empty($id)) {
            $pckg_edit_orders_extra_id = $id;
        } else {
            $pckg_edit_orders_extra_id = $this->input->post('id');
        }

        $user_staff_id  = profile()->user_id;
        $user_type_id   = profile()->user_type_id;

        $orders_extra = $this->package_edit_orders_extra_model->get_package_edit(['pckg_edit_orders_extra_id' => $pckg_edit_orders_extra_id])->row();

        if (!empty($orders_extra)) {
            $data_approve = [
            'pckg_edit_orders_extra_id' => $pckg_edit_orders_extra_id,
            'user_approved_staff_id'    => $user_staff_id,
            'date_approved'             => time(),
            'order_status'              => '1', //approved
            'updated_at'                => time(),
            ];

            $approve = $this->package_edit_orders_extra_model->approve($data_approve);

            $data = [
            'det_order_extra_id'    => $orders_extra->orders_extra_id,
            'det_order_extra_name'  => $orders_extra->orders_extra_name,
            'det_order_extra_total' => $orders_extra->orders_extra_total
            ];

            $this->det_orders_extra_model->update($data);

        } else {
            set_alert('alert-danger', 'Permintaan ditolak');
            return redirect($this->controller_path, 'refresh');
        }

        $status = 1;
        $this->get_mail($orders_extra, $status);

        set_alert('alert-success', 'Permintaan berhasil diperbaharui');
        return redirect('orders/order/view/' . $orders_extra->order_id, 'refresh');
    }

    public function approve_package_delete($id='')
    {
        if (!empty($id)) {
            $package_delete_id = $id;
        } else {
            $package_delete_id = $this->input->post('id');
        }

        $user_staff_id  = profile()->user_id;
        $user_type_id   = profile()->user_type_id;

        $package_delete = $this->package_delete_model->get_package_delete(['package_delete_id' => $package_delete_id])->row();

        if (!empty($package_delete)) {
            $data_approve = [
            'package_delete_id'         => $package_delete_id,
            'user_approved_staff_id'    => $user_staff_id,
            'date_approved'             => time(),
            'package_delete_status'     => '1', //approved
            'updated_at'                => time(),
            ];

            $approve    = $this->package_delete_model->approve($data_approve);

            $delete     = $this->packages_model->delete(['package_id' => $package_delete->package_id]);

        } else {
            set_alert('alert-danger', 'Permintaan ditolak');
            return redirect($this->controller_path, 'refresh');
        }

        $status = 1;
        $this->get_mail($package_delete, $status);

        set_alert('alert-success', 'Permintaan hapus data paket berhasil diperbaharui');
        return redirect('orders/order/view/' . $package_delete->order_id, 'refresh');
    }

    public function reject_edit_package($id = '') {

        if (!empty($id)) {
            $package_edit_id = $id;
        } else {
            $package_edit_id = $this->input->post('id');
        }

        $user_staff_id = profile()->user_id;
        $user_type_id = profile()->user_type_id;

        $data_approve = [
        'package_edit_id'               => $package_edit_id,
        'user_approved_staff_id'        => $user_staff_id,
        'package_edit_date_approved'    => time(),
        'order_status'                  => '3',  //rejected
        'package_edit_status'           => '3',  //rejected
        'package_notes'                 => $this->input->post('package_notes'),
        'updated_at'                    => time(),
        ];

        $approve = $this->package_edit_model->approve($data_approve);
        $package = $this->package_edit_model->get_package_edit(['package_edit_id' => $package_edit_id])->row();

        $status = 2;
        $this->get_mail($package, $status);

        set_alert('alert-success', 'Permintaan edit berhasil diperbaharui');

        return redirect($this->controller_path);
    }

    public function reject_edit_location($id = '') {

        if (!empty($id)) {
            $pckg_edit_loc_id = $id;
        } else {
            $pckg_edit_loc_id = $this->input->post('id');
        }

        $user_staff_id  = profile()->user_id;
        $user_type_id   = profile()->user_type_id;
        $type_edit      = $this->input->post('type_edit');

        if (!empty($type_edit)) {
            if ($type_edit == 1) { //edit data asal paket
                $data_approve = [
                'pckg_edit_loc_id'              => $pckg_edit_loc_id,
                'user_approved_staff_id'        => $user_staff_id,
                'pckg_edit_loc_date_approved'   => time(),
                'order_status'                  => '3',  //rejected
                'package_notes'                 => $this->input->post('package_notes'),
                'updated_at'                    => time(),
                ];
            } else if ($type_edit == 2) { //edit data tujuan paket
                $data_approve = [
                'pckg_edit_loc_id'              => $pckg_edit_loc_id,
                'user_approved_staff_id'        => $user_staff_id,
                'pckg_edit_loc_date_approved'   => time(),
                'order_status'                  => '3',  //rejected
                'package_notes'                 => $this->input->post('package_notes'),
                'updated_at'                    => time(),
                ];
            }
        } 
        $approve    = $this->package_edit_location_model->approve($data_approve);
        $location   = $this->package_edit_location_model->get_package_edit(['pckg_edit_loc_id' => $pckg_edit_loc_id])->row();

        $status = 2;
        $this->get_mail($location, $status);

        set_alert('alert-success', 'Permintaan edit berhasil diperbaharui');
        return redirect($this->controller_path);
    }

    public function reject_edit_total($id = '') {

        if (!empty($id)) {
            $pckg_edit_total_id = $id;
        } else {
            $pckg_edit_total_id = $this->input->post('id');
        }

        $user_staff_id = profile()->user_id;
        $user_type_id = profile()->user_type_id;

        $data_approve = [
        'pckg_edit_total_id'        => $pckg_edit_total_id,
        'user_approved_staff_id'    => $user_staff_id,
        'date_approved'             => time(),
        'order_status'              => '3',  //rejected
        'package_notes'             => $this->input->post('package_notes'),
        'updated_at'                => time(),
        ];

        $approve = $this->package_edit_total_model->approve($data_approve);
        $total_price = $this->package_edit_total_model->get_package_edit(['pckg_edit_total_id' => $pckg_edit_total_id])->row();

        $status = 2;
        $this->get_mail($total_price, $status);

        set_alert('alert-success', 'Permintaan edit berhasil diperbaharui');
        return redirect($this->controller_path);
    }

    public function reject_edit_orders_extra($id = '') {

        if (!empty($id)) {
            $pckg_edit_orders_extra_id = $id;
        } else {
            $pckg_edit_orders_extra_id = $this->input->post('id');
        }

        $user_staff_id = profile()->user_id;
        $user_type_id = profile()->user_type_id;

        $data_approve = [
        'pckg_edit_orders_extra_id'     => $pckg_edit_orders_extra_id,
        'user_approved_staff_id'        => $user_staff_id,
        'date_approved'                 => time(),
        'order_status'                  => '3',  //rejected
        'package_notes'                 => $this->input->post('package_notes'),
        'updated_at' => time(),
        ];

        $approve = $this->package_edit_orders_extra_model->approve($data_approve);
        $orders_extra = $this->package_edit_orders_extra_model->get_package_edit(['pckg_edit_orders_extra_id' => $pckg_edit_orders_extra_id])->row();

        $status = 2;
        $this->get_mail($orders_extra, $status);

        set_alert('alert-success', 'Permintaan edit berhasil diperbaharui');
        return redirect($this->controller_path);
    }

    public function reject_package_delete($id = '') {

        if (!empty($id)) {
            $package_delete_id = $id;
        } else {
            $package_delete_id = $this->input->post('id');
        }

        $user_staff_id  = profile()->user_id;
        $user_type_id   = profile()->user_type_id;

        $data_approve = [
        'package_delete_id'         => $package_delete_id,
        'user_approved_staff_id'    => $user_staff_id,
        'date_approved'             => time(),
        'package_delete_status'     => '3',  //rejected
        'package_notes'             => $this->input->post('package_notes'),
        'updated_at'                => time(),
        ];

        $approve        = $this->package_delete_model->approve($data_approve);
        $package_delete = $this->package_delete_model->get_package_delete(['package_delete_id' => $package_delete_id])->row();

        $status = 2;
        $this->get_mail($package_delete, $status);

        set_alert('alert-success', 'Permintaan hapus data paket berhasil diperbaharui');
        return redirect($this->controller_path);
    }

    public function get_mail($data = array(), $status)
    {
        // email Super Admin
        $super_admin    = $this->users_model->get_profile(['user.user_type_id' => 99])->result();
        foreach ($super_admin as $key => $value) {
            $email_superadmin[] = $value->user_email; 
        }

        // email Admin pusat
        $admin_pusat    = $this->users_model->get_profile(['user.user_type_id ' => 120])->result();
        foreach ($admin_pusat as $key => $value) {
            $email_pusat[] = $value->user_email; 
        }

        // email Admin Regional
        $data_regional  = $this->regionals_model->get_regional_manager($data->regional_id);
        $email_regional = (!empty($data_regional)) ? $data_regional->user_email : '' ;

        // email & user_type_id User Branch
        $user_branch            = $this->users_model->get_user(['user_id' => $data->request_user_staff_id])->row();
        $email_user_branch      = (!empty($user_branch)) ? $user_branch->user_email : '' ;

        $this->send_mail($email_superadmin, $email_pusat, $email_regional, $email_user_branch, $status);
    }

    public function send_mail($email_superadmin = array(),$email_pusat = array(),$email_regional,$email_user_branch, $status) {

        // Load all resource needed
        $this->config->load('email');
        $this->load->library('email');
        
        $to_email   = $email_user_branch;
        $email      = array();
        array_push($email, $email_regional);
        $email      = array_merge($email_pusat, $email_superadmin);
        $email      = implode(', ', $email);
        $cc_email   = $email;

        if ($status == 1) {
            $subject = 'Notification Order Approval Accepted';
            $message = 'Hello user, order approval that you requested has been updated by user :'.profile()->user_id.' - '.profile()->name.' - '.profile()->branch_name;
        } else if ($status == 2){
            $subject = 'Notification Order Approval Rejected';
            $message = 'Hello user, order approval that you requested has been rejected by user :'.profile()->user_id.' - '.profile()->name.' - '.profile()->branch_name;
        }
        
        $this->email->initialize($this->config->item('email'));

        $this->email->from('paketstsindonesia@gmail.com', 'noreply');
        $this->email->to($to_email);
        $this->email->cc($cc_email); 
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send()) {
            echo "Mail Sent!";
            #echo $this->email->print_debugger();
        } else {
            echo "There is error in sending mail!";
            #echo $this->email->print_debugger();
        }
    }

}
