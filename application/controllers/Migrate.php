<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* For detailed reference, please read https://ellislab.com/codeigniter/user-guide/libraries/migration.html */

class Migrate extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('migration');
    }

    function index() {
        //$migration_okay=$this->migration->current();  // set as defined in $config['migration_version'], in application/config/migration.php
        $migration_okay = $this->migration->latest();   // set to latest changes as applied in application/migrations
        //$migration_okay=$this->migration->version(2); // set to version N, as available in application/migrations
        if (!$migration_okay)
            show_error($this->migration->error_string());
        else
            echo 'Migrated to latest version';
    }

    function version() {
        $version = $this->uri->segment(3);
        $migration_okay = $this->migration->version($version);
        if (!$migration_okay)
            show_error($this->migration->error_string());
        else
            echo 'Migrated to version: ' . $version;
    }

    //base_data
    public function the_starter($key = '') {

        if ($key == 'b64209hc') {

            $set_array = array(
                'company_name' => 'PT. STS'
            );

            $this->db->set($set_array);
            $this->db->insert('sys_config');

            $set_array = array(
                'username' => 'super_admin',
                'password' => md5('123456'),
                'user_status' => 1,
                'user_email' => 'admin@email.com',
                'created_at' => time(),
                'user_type_id' => 99
            );
            $this->db->set($set_array);
            $this->db->insert('user');

            $set_array = array(
                'user_id' => 1
            );
            $this->db->set($set_array);
            $this->db->insert('userprofile');

            $set_array2 = array(
                'user_type_id' => 99,
                'user_type_name' => 'Super Admin'
            );
            $this->db->set($set_array2);
            $this->db->insert('user_types');
        }
    }

    public function create_view() {

        /** Add columns at regional table */
        if (!$this->db->table_exists('finance_report'))
            $this->db->query('CREATE VIEW finance_report AS '
                    . 'SELECT '
                    . 'orders.order_id AS order_id, '
                    . 'orders.order_status AS order_status, '
                    . 'orders.order_date AS order_date, '
                    . 'orders.order_use_tax AS order_use_tax, '
                    . 'orders.order_sell_total AS order_sell_total, '
                    . 'customers.customer_name AS customer_name, '
                    . 'branches_origin.branch_name AS branch_name_origin, '
                    . 'branches_destination.branch_name AS branch_name_destination, '
                    . 'SUM(det_orders_extra.det_order_extra_total) AS extra_total '
                    . 'FROM orders '
                    . 'LEFT JOIN branches AS branches_origin ON orders.order_origin = branches_origin.branch_id '
                    . 'LEFT JOIN branches AS branches_destination ON orders.order_destination = branches_destination.branch_id '
                    . 'LEFT JOIN customers ON customers.customer_id = orders.customer_id '
                    . 'LEFT JOIN det_orders_extra ON orders.order_id = det_orders_extra.order_id WHERE orders.order_status > -1 AND orders.deleted_at = 0 '
                    . 'GROUP BY orders.order_id ORDER BY orders.order_date DESC');

        /** Add columns at regional table */
        if (!$this->db->table_exists('orders_view'))
            $this->db->query('CREATE VIEW orders_view AS '
                    . 'SELECT '
                    . 'orders.order_id AS order_id, '
                    . 'orders.order_date AS order_date, '
                    . 'orders.order_load_date AS order_load_date, '
                    . 'orders.order_unload_date AS order_unload_date, '
                    . 'orders.order_delivered_date AS order_delivered_date, '
                    . 'orders.order_received_date AS order_received_date, '
                    . 'orders.order_status AS order_status, '
                    . 'customers.customer_name AS customer_name, '
                    . 'branches_origin.branch_name AS branch_name_origin, '
                    . 'branches_destination.branch_name AS branch_name_destination '
                    . 'FROM orders '
                    . 'LEFT JOIN customers ON orders.customer_id = customers.customer_id '
                    . 'LEFT JOIN branches AS branches_origin ON orders.order_origin = branches_origin.branch_id '
                    . 'LEFT JOIN branches AS branches_destination ON orders.order_destination = branches_destination.branch_id '
                    . 'WHERE orders.deleted_at = "0" '
                    . 'ORDER BY orders.order_date DESC ');

        if (!$this->db->table_exists('packages_view_detail'))
            $this->db->query('CREATE VIEW packages_view_detail AS '
                    . 'SELECT '
                    . 'det_order.order_id, '
                    . 'det_order.package_id, '
                    . 'det_order.det_order_total, '
                    . 'det_order.det_order_notes, '
                    . 'det_order.det_order_qty, '
                    . 'det_order.det_order_sell_total, '
                    . 'det_order.det_order_package_parent, '
                    . 'packages.package_weight, '
                    . 'packages.package_weight*det_order.det_order_qty  AS total_package_weight, '
                    . 'packages.package_size, '
                    . 'packages.package_size*det_order.det_order_qty AS total_package_size, ' //dalam CM3
                    . 'packages.package_status '
                    . 'FROM det_order '
                    . 'LEFT JOIN packages ON det_order.package_id = packages.package_id '
                    . 'WHERE packages.deleted_at = "0" '
                    . 'GROUP BY det_order.det_order_package_parent ');

        if (!$this->db->table_exists('packages_view'))
            $this->db->query('CREATE VIEW packages_view AS '
                    . 'SELECT '
                    . 'order_id, '
                    . 'COUNT(package_id) AS total_package, '
                    . 'SUM(det_order_qty) AS total_koli, '
                    . 'SUM(total_package_weight) AS total_weight, '
                    . 'SUM(total_package_size) AS total_size, '
                    . 'package_status AS package_status '
                    . 'FROM packages_view_detail '
                    . 'GROUP BY order_id ');

        // if (!$this->db->table_exists('packages_view'))
        //     $this->db->query('CREATE VIEW packages_view AS '
        //         . 'SELECT '
        //         . 'det_order.order_id, '
        //         . 'COUNT(packages.package_id) AS total_koli, '
        //         . 'SUM(packages.package_weight) AS total_weight, '
        //         . 'SUM(packages.package_size) AS total_size, '
        //         . 'packages.package_status AS package_status '
        //         . 'FROM det_order '
        //         . 'LEFT JOIN packages ON det_order.package_id = packages.package_id '
        //         . 'WHERE packages.deleted_at = "0" '
        //         . 'GROUP BY det_order.order_id, det_order.det_order_package_parent ');

        if (!$this->db->table_exists('customer_transaction'))
            $this->db->query('CREATE VIEW customer_transaction AS '
                    . 'SELECT  '
                    . 'customer_id AS customer_id,  '
                    . 'SUM(order_sell_total) AS total_transaction,  '
                    . 'order_date AS last_transaction '
                    . 'FROM orders '
                    . 'GROUP BY customer_id '
                    . 'ORDER BY order_date DESC');

        if (!$this->db->table_exists('customers_transaction_report'))
            $this->db->query(''
                    . 'CREATE VIEW '
                    . 'customers_transaction_report AS '
                    . 'SELECT '
                    . 'customers.customer_id AS customer_id, '
                    . 'customers.customer_name AS customer_name, '
                    . 'cities.city_name AS city_name, '
                    . 'branches.branch_name AS branch_name, '
                    . 'customers.customer_referral AS customer_referral, '
                    . 'customer_transaction.total_transaction AS total_transaction, '
                    . 'customer_transaction.last_transaction AS last_transaction, '
                    . 'customers.branch_created as customer_branch_id, '
                    . 'branches.branch_regional_id as customer_regional_id '
                    . 'FROM customers '
                    . 'LEFT JOIN cities ON customers.customer_city = cities.city_id '
                    . 'LEFT JOIN branches ON branches.branch_id = customers.branch_created '
                    . 'LEFT JOIN customer_transaction ON customer_transaction.customer_id = customers.customer_id'
                    . '');

        if (!$this->db->table_exists('customers_report_detail'))
            $this->db->query(''
                    . 'CREATE VIEW customers_report_detail AS '
                    . 'SELECT '
                    . 'orders.order_date AS order_date, '
                    . 'orders.order_id AS order_id, '
                    . 'orders.customer_id AS customer_id, '
                    . 'orders.order_sell_total AS order_sell_total, '
                    . 'branch_origin.branch_name AS branch_origin_name, '
                    . 'branch_destination.branch_name AS branch_destination_name, '
                    . 'packages_view.total_koli AS total_koli, '
                    . 'packages_view.total_weight AS total_weight, '
                    . 'packages_view.total_size AS total_size '
                    . 'FROM orders '
                    . 'JOIN branches AS branch_origin ON orders.order_origin = branch_origin.branch_id '
                    . 'JOIN branches AS branch_destination ON orders.order_destination = branch_destination.branch_id '
                    . 'JOIN packages_view ON orders.order_id = packages_view.order_id'
                    . '');

        if (!$this->db->table_exists('order_payment_report'))
            $this->db->query(''
                    . 'CREATE VIEW order_payment_report AS '
                    . 'SELECT '
                    . 'orders.order_date AS order_date, '
                    . 'orders.order_id AS order_id, '
                    . 'customers.customer_name AS customer_name, '
                    . 'orders.order_payment_type AS order_payment_type, '
                    . 'orders.orders_due_date AS orders_due_date, '
                    . 'branch_origin.branch_name AS branch_origin_name, '
                    . 'branch_destination.branch_name AS branch_destination_name '
                    . 'FROM orders '
                    . 'JOIN branches AS branch_origin ON orders.order_origin = branch_origin.branch_id '
                    . 'JOIN branches AS branch_destination ON orders.order_destination = branch_destination.branch_id '
                    . 'JOIN customers ON orders.customer_id = customers.customer_id '
                    . 'WHERE orders.deleted_at = "0" '
                    . 'ORDER BY orders.order_date DESC ');

        if (!$this->db->table_exists('sale_comission_report_up'))
            $this->db->query(''
                    . 'CREATE VIEW sale_comission_report_up AS '
                    . 'SELECT '
                    . 'orders.order_origin AS branch_id, '
                    . 'count(orders.order_id) AS total_order, '
                    . 'sum(orders.order_sell_total) AS total_omset '
                    . 'FROM orders '
                    . 'LEFT JOIN package_management ON orders.order_id = package_management.resi_number '
                    . 'WHERE orders.deleted_at = "0" '
                    . 'AND package_management.type_do = "load" '
                    . 'GROUP BY orders.order_origin'
                    . '');

        if (!$this->db->table_exists('sale_comission_report_down'))
            $this->db->query(''
                    . 'CREATE VIEW sale_comission_report_down AS '
                    . 'SELECT '
                    . 'orders.order_origin AS branch_id, '
                    . 'count(orders.order_id) AS total_order, '
                    . 'sum(orders.order_sell_total) AS total_omset '
                    . 'FROM orders '
                    . 'LEFT JOIN package_management ON orders.order_id = package_management.resi_number '
                    . 'WHERE orders.deleted_at = "0" '
                    . 'AND package_management.type_do = "unload" '
                    . 'GROUP BY orders.order_origin ');

        if (!$this->db->table_exists('sale_comission_report_up_detail'))
            $this->db->query(''
                    . 'CREATE VIEW sale_comission_report_up_detail AS '
                    . 'SELECT '
                    . 'orders.order_origin AS branch_id, '
                    . 'orders.order_id AS order_id, '
                    . 'orders.order_date AS order_date, '
                    . 'orders.order_status AS order_status, '
                    . 'orders.order_sell_total AS omset, '
                    . 'customers.customer_name AS customer_name, '
                    . 'branches_origin.branch_name AS branch_name_origin, '
                    . 'branches_destination.branch_name AS branch_name_destination '
                    . 'FROM orders '
                    . 'LEFT JOIN package_management ON orders.order_id = package_management.resi_number '
                    . 'LEFT JOIN customers ON orders.customer_id = customers.customer_id '
                    . 'LEFT JOIN branches AS branches_origin ON orders.order_origin = branches_origin.branch_id '
                    . 'LEFT JOIN branches AS branches_destination ON orders.order_destination = branches_destination.branch_id '
                    . 'WHERE orders.deleted_at = "0" '
                    . 'AND package_management.type_do = "load" '
                    . 'ORDER BY orders.order_date DESC ');

        if (!$this->db->table_exists('sale_comission_report_down_detail'))
            $this->db->query(''
                    . 'CREATE VIEW sale_comission_report_down_detail AS '
                    . 'SELECT '
                    . 'orders.order_origin AS branch_id, '
                    . 'orders.order_id AS order_id, '
                    . 'orders.order_date AS order_date, '
                    . 'orders.order_status AS order_status, '
                    . 'orders.order_sell_total AS omset, '
                    . 'customers.customer_name AS customer_name, '
                    . 'branches_origin.branch_name AS branch_name_origin, '
                    . 'branches_destination.branch_name AS branch_name_destination '
                    . 'FROM orders '
                    . 'LEFT JOIN package_management ON orders.order_id = package_management.resi_number '
                    . 'LEFT JOIN customers ON orders.customer_id = customers.customer_id '
                    . 'LEFT JOIN branches AS branches_origin ON orders.order_origin = branches_origin.branch_id '
                    . 'LEFT JOIN branches AS branches_destination ON orders.order_destination = branches_destination.branch_id '
                    . 'WHERE orders.deleted_at = "0" '
                    . 'AND package_management.type_do = "unload" '
                    . 'ORDER BY orders.order_date DESC ');

        if (!$this->db->table_exists('orders_view_location'))
            $this->db->query(''
                    . 'CREATE VIEW orders_view_location AS '
                    . 'SELECT '
                    . 'orders.order_id AS order_id, '
                    . 'orders.order_origin AS branch_origin_id, '
                    . 'branches_origin.branch_name AS branch_origin_name, '
                    . 'orders.order_destination AS branch_destination_id, '
                    . 'branches_destination.branch_name AS branch_destination_name, '
                    . 'orders.order_date AS order_date, '
                    . 'orders.customer_id AS customer_id, '
                    . 'orders.order_status AS order_status, '
                    . 'orders_locations.orders_locations_city_id AS city_id, '
                    . 'cities.city_name AS city_name, '
                    . 'packages_view.total_weight AS total_weight, '
                    . 'packages_view.total_size AS total_size '
                    . 'FROM orders '
                    . 'LEFT JOIN orders_locations ON orders_locations.orders_locations_order_id = orders.order_id '
                    . 'LEFT JOIN packages_view ON packages_view.order_id = orders.order_id '
                    . 'LEFT JOIN customers ON orders.customer_id = customers.customer_id '
                    . 'LEFT JOIN branches AS branches_origin ON orders.order_origin = branches_origin.branch_id '
                    . 'LEFT JOIN branches AS branches_destination ON orders.order_destination = branches_destination.branch_id '
                    . 'LEFT JOIN cities ON cities.city_id = orders_locations.orders_locations_city_id '
                    . 'WHERE orders.deleted_at = "0" '
                    . 'ORDER BY orders.order_date DESC ');

        if (!$this->db->table_exists('orders_view_location_total'))
            $this->db->query(''
                    . 'CREATE VIEW orders_view_location_total AS '
                    . 'SELECT '
                    . 'orders_view_location.city_id AS city_id, '
                    . 'SUM(orders_view_location.total_weight) AS total_weight, '
                    . 'SUM(orders_view_location.total_size) AS total_size '
                    . 'FROM orders_view_location '
                    // . 'WHERE orders_view_location.city_id is not null'
                    . 'GROUP BY orders_view_location.city_id '
                    . 'ORDER BY orders_view_location.city_id ASC ');

        if (!$this->db->table_exists('report_logistic_trips_city_start'))
            $this->db->query(''
                    . 'CREATE VIEW report_logistic_trips_city_start AS '
                    . 'SELECT '
                    . 'det_trips.det_trip_id AS det_trip_id, '
                    . 'det_trips.trip_id AS trip_id, '
                    . 'det_trips.det_trip_city_start AS city_start '
                    . 'FROM det_trips '
                    . 'GROUP BY det_trips.trip_id ');

        if (!$this->db->table_exists('report_logistic_trips_city_stop'))
            $this->db->query(''
                    . 'CREATE VIEW report_logistic_trips_city_stop AS '
                    . 'SELECT '
                    . 'det_trips.det_trip_id AS det_trip_id, '
                    . 'det_trips.trip_id AS trip_id, '
                    . 'det_trips.det_trip_city_stop AS city_stop '
                    . 'FROM det_trips '
                    . 'WHERE det_trips.det_trip_id IN (SELECT max(det_trips.det_trip_id) FROM det_trips GROUP BY det_trips.trip_id)'
                    . 'GROUP BY det_trips.trip_id ');

        if (!$this->db->table_exists('report_logistic_trips_city_detail'))
            $this->db->query(''
                    . 'CREATE VIEW report_logistic_trips_city_detail AS '
                    . 'SELECT '
                    . 'a.trip_id AS trip_id, '
                    . 'a.city_start AS city_start, '
                    . 'b.city_stop AS city_stop '
                    . 'FROM report_logistic_trips_city_start a '
                    . 'LEFT JOIN report_logistic_trips_city_stop b ON a.trip_id = b.trip_id ');

        if (!$this->db->table_exists('report_logistic_armadas'))
            $this->db->query(''
                    . 'CREATE VIEW report_logistic_armadas AS '
                    . 'SELECT '
                    . 'b.trip_id AS trip_id, '
                    . 'a.armada_id AS armada_id, '
                    . 'a.armada_name AS armada_name, '
                    . 'a.armada_status AS armada_status, '
                    . 'c.city_start AS start_id, '
                    . 'branch_start.branch_name AS start_name, '
                    . 'c.city_stop AS stop_id, '
                    . 'branch_stop.branch_name AS stop_name, '
                    . 'b.trip_start_date AS trip_start_date, '
                    . 'b.trip_end_date AS trip_end_date '
                    . 'FROM armadas a '
                    . 'LEFT JOIN trips b ON a.armada_id = b.armada_id '
                    . 'LEFT JOIN report_logistic_trips_city_detail c ON b.trip_id = c.trip_id '
                    . 'LEFT JOIN branches AS branch_start ON branch_start.branch_id = c.city_start '
                    . 'LEFT JOIN branches AS branch_stop ON branch_stop.branch_id = c.city_stop '
                    . 'WHERE a.armada_status = 1 ');

        if (!$this->db->table_exists('report_logistic_packages'))
            $this->db->query(''
                    . 'CREATE VIEW report_logistic_packages AS '
                    . 'SELECT '
                    . 'a.armadatrip_armada_id AS armadatrip_armada_id, '
                    . 'a.armadatrip_package_id AS package_id, '
                    . 'c.order_id AS order_id '
                    . 'FROM armadatrips a '
                    . 'LEFT JOIN det_order b ON a.armadatrip_package_id = b.package_id '
                    . 'LEFT JOIN orders c ON b.order_id = c.order_id ');

        // if (!$this->db->table_exists('report_logistic_orders'))
        //     $this->db->query(''
        //         . 'CREATE VIEW report_logistic_orders AS '
        //         . 'SELECT '
        //         . 'armadatrip_armada_id AS armadatrip_armada_id, '
        //         . 'DISTINCT(order_id) AS order_id '
        //         . 'FROM report_logistic_packages ');

        if (!$this->db->table_exists('sale_comission_branch_origin'))
            $this->db->query(''
                    . 'CREATE VIEW sale_comission_branch_origin AS '
                    . 'SELECT '
                    . 'orders.order_origin AS branch_id, '
                    . 'count(orders.order_id) AS total_order, '
                    . 'sum(orders.order_sell_total) AS total_omset '
                    . 'FROM orders '
                    . 'WHERE orders.order_status > "0" '
                    . 'AND orders.deleted_at = "0" '
                    . 'GROUP BY orders.order_origin ');

        if (!$this->db->table_exists('sale_comission_branch_origin_detail'))
            $this->db->query(''
                    . 'CREATE VIEW sale_comission_branch_origin_detail AS '
                    . 'SELECT '
                    . 'orders.order_origin AS branch_id, '
                    . 'orders.order_id AS order_id, '
                    . 'orders.order_date AS order_date, '
                    . 'orders.order_status AS order_status, '
                    . 'orders.order_sell_total AS omset, '
                    . 'customers.customer_name AS customer_name, '
                    . 'branches_origin.branch_name AS branch_name_origin, '
                    . 'branches_destination.branch_name AS branch_name_destination '
                    . 'FROM orders '
                    . 'LEFT JOIN customers ON orders.customer_id = customers.customer_id '
                    . 'LEFT JOIN branches AS branches_origin ON orders.order_origin = branches_origin.branch_id '
                    . 'LEFT JOIN branches AS branches_destination ON orders.order_destination = branches_destination.branch_id '
                    . 'WHERE orders.order_status > "0" '
                    . 'AND orders.deleted_at = "0" '
                    . 'ORDER BY orders.order_date DESC ');

        if (!$this->db->table_exists('sale_comission_branch_destination'))
            $this->db->query(''
                    . 'CREATE VIEW sale_comission_branch_destination AS '
                    . 'SELECT '
                    . 'orders.order_destination AS branch_id, '
                    . 'count(orders.order_id) AS total_order, '
                    . 'sum(orders.order_sell_total) AS total_omset '
                    . 'FROM orders '
                    . 'WHERE orders.order_status > "0" '
                    . 'AND orders.deleted_at = "0" '
                    . 'GROUP BY orders.order_destination ');

        if (!$this->db->table_exists('sale_comission_branch_destination_detail'))
            $this->db->query(''
                    . 'CREATE VIEW sale_comission_branch_destination_detail AS '
                    . 'SELECT '
                    . 'orders.order_destination AS branch_id, '
                    . 'orders.order_id AS order_id, '
                    . 'orders.order_date AS order_date, '
                    . 'orders.order_status AS order_status, '
                    . 'orders.order_sell_total AS omset, '
                    . 'customers.customer_name AS customer_name, '
                    . 'branches_origin.branch_name AS branch_name_origin, '
                    . 'branches_destination.branch_name AS branch_name_destination '
                    . 'FROM orders '
                    . 'LEFT JOIN customers ON orders.customer_id = customers.customer_id '
                    . 'LEFT JOIN branches AS branches_origin ON orders.order_origin = branches_origin.branch_id '
                    . 'LEFT JOIN branches AS branches_destination ON orders.order_destination = branches_destination.branch_id '
                    . 'WHERE orders.order_status > "0" '
                    . 'AND orders.deleted_at = "0" '
                    . 'ORDER BY orders.order_date DESC ');

        if (!$this->db->table_exists('report_logistic_deliveries_1'))
            $this->db->query(''
                    . 'CREATE VIEW report_logistic_deliveries_1 AS '
                    . 'SELECT '
                    . 'orders.order_origin AS branch_id_origin, '
                    . 'branches.branch_name AS branch_name_origin, '
                    . 'count(orders.order_id) AS total_order '
                    . 'FROM orders '
                    . 'LEFT JOIN branches ON branches.branch_id = orders.order_origin '
                    . 'LEFT JOIN packages_view ON packages_view.order_id = orders.order_id '
                    . 'WHERE orders.order_status = "1" '
                    . 'AND packages_view.package_status = "7" '
                    . 'GROUP BY orders.order_origin ');

        if (!$this->db->table_exists('report_logistic_deliveries_1_detail'))
            $this->db->query(''
                    . 'CREATE VIEW report_logistic_deliveries_1_detail AS '
                    . 'SELECT '
                    . 'orders.order_origin AS branch_id_origin, '
                    . 'branches.branch_name AS branch_name_origin, '
                    . 'orders.order_id '
                    . 'FROM orders '
                    . 'LEFT JOIN branches ON branches.branch_id = orders.order_origin '
                    . 'LEFT JOIN packages_view ON packages_view.order_id = orders.order_id '
                    . 'WHERE orders.order_status = "1" '
                    . 'AND packages_view.package_status = "7" ');

        if (!$this->db->table_exists('report_logistic_deliveries_2'))
            $this->db->query(''
                    . 'CREATE VIEW report_logistic_deliveries_2 AS '
                    . 'SELECT '
                    . 'orders.order_origin as branch_id, '
                    . 'branch_origin.branch_name as branch_origin_name, '
                    . 'count(orders.order_id) AS total_order '
                    . 'FROM accepted_report '
                    . 'LEFT JOIN orders ON orders.order_id = accepted_report.order_id '
                    . 'LEFT JOIN branches AS branch_origin ON orders.order_origin = branch_origin.branch_id '
                    . 'GROUP BY orders.order_origin ');

        if (!$this->db->table_exists('report_logistic_deliveries_2_detail'))
            $this->db->query(''
                    . 'CREATE VIEW report_logistic_deliveries_2_detail AS '
                    . 'SELECT '
                    . 'orders.order_origin as branch_id, '
                    . 'branch_origin.branch_name as branch_origin_name, '
                    . 'accepted_report.order_id, '
                    . 'accepted_report.type, '
                    . 'accepted_report.accepted_name, '
                    . 'accepted_report.created_at '
                    . 'FROM accepted_report '
                    . 'LEFT JOIN orders ON orders.order_id = accepted_report.order_id '
                    . 'LEFT JOIN branches AS branch_origin ON orders.order_origin = branch_origin.branch_id '
                    . 'GROUP BY orders.order_origin ');

        if (!$this->db->table_exists('report_logistic_deliveries_3'))
            $this->db->query(''
                    . 'CREATE VIEW report_logistic_deliveries_3 AS '
                    . 'SELECT '
                    . 'orders.order_origin as branch_id, '
                    . 'branch_origin.branch_name as branch_origin_name, '
                    . 'count(orders.order_id) AS total_order '
                    . 'FROM cancel_report '
                    . 'LEFT JOIN orders ON orders.order_id = cancel_report.order_id '
                    . 'LEFT JOIN branches AS branch_origin ON orders.order_origin = branch_origin.branch_id '
                    . 'GROUP BY orders.order_origin ');

        if (!$this->db->table_exists('report_logistic_deliveries_3_detail'))
            $this->db->query(''
                    . 'CREATE VIEW report_logistic_deliveries_3_detail AS '
                    . 'SELECT '
                    . 'orders.order_origin as branch_id, '
                    . 'branch_origin.branch_name as branch_origin_name, '
                    . 'cancel_report.order_id, '
                    . 'cancel_report.note, '
                    . 'cancel_report.created_at '
                    . 'FROM cancel_report '
                    . 'LEFT JOIN orders ON orders.order_id = cancel_report.order_id '
                    . 'LEFT JOIN branches AS branch_origin ON orders.order_origin = branch_origin.branch_id '
                    . 'GROUP BY orders.order_origin ');

        if (!$this->db->table_exists('report_logistic_cities'))
            $this->db->query('CREATE VIEW report_logistic_cities AS '
                    . 'SELECT '
                    . 'orders_locations.orders_locations_order_id AS order_id, '
                    . 'orders_locations.orders_locations_branch_id AS branch_id, '
                    . 'orders_locations.orders_locations_city_id AS city_id '
                    . 'FROM orders_locations '
                    . 'WHERE orders_locations.orders_locations_city_id != 0 ');

        if (!$this->db->table_exists('report_logistic_cities_detail'))
            $this->db->query('CREATE VIEW report_logistic_cities_detail AS '
                    . 'SELECT '
                    . 'report_logistic_cities.order_id, '
                    . 'orders.order_origin AS branch_origin_id, '
                    . 'branches_origin.branch_name AS branch_origin_name, '
                    . 'orders.order_destination AS branch_destination_id, '
                    . 'branches_destination.branch_name AS branch_destination_name, '
                    . 'orders.order_date AS order_date, '
                    . 'orders.customer_id AS customer_id, '
                    . 'customers.customer_name AS customer_name, '
                    . 'orders.order_status AS order_status, '
                    . 'report_logistic_cities.city_id, '
                    . 'cities.city_name AS city_name, '
                    . 'packages_view.total_weight AS total_weight, '
                    . 'packages_view.total_size AS total_size '
                    . 'FROM report_logistic_cities '
                    . 'LEFT JOIN orders ON report_logistic_cities.order_id = orders.order_id '
                    . 'LEFT JOIN packages_view ON packages_view.order_id = orders.order_id '
                    . 'LEFT JOIN customers ON orders.customer_id = customers.customer_id '
                    . 'LEFT JOIN branches AS branches_origin ON orders.order_origin = branches_origin.branch_id '
                    . 'LEFT JOIN branches AS branches_destination ON orders.order_destination = branches_destination.branch_id '
                    . 'LEFT JOIN cities ON cities.city_id = report_logistic_cities.city_id '
                    . 'WHERE orders.deleted_at = "0" '
                    . 'ORDER BY orders.order_date DESC ');

        if (!$this->db->table_exists('report_logistic_cities_total'))
            $this->db->query('CREATE VIEW report_logistic_cities_total AS '
                    . 'SELECT '
                    . 'city_id, '
                    . 'city_name, '
                    . 'COUNT(order_id) AS total_order, '
                    . 'SUM(total_weight) AS total_weight, '
                    . 'SUM(total_size) AS total_size '
                    . 'FROM report_logistic_cities_detail '
                    . 'GROUP BY city_id '
                    . 'ORDER BY city_name ASC ');

        echo "Table View Berhasil Dibuat";
    }

    public function drop_view() {

        if ($this->db->table_exists('finance_report'))
            $this->db->query('DROP VIEW finance_report');

        if ($this->db->table_exists('orders_view'))
            $this->db->query('DROP VIEW orders_view');

        if ($this->db->table_exists('packages_view_detail'))
            $this->db->query('DROP VIEW packages_view_detail');

        if ($this->db->table_exists('packages_view'))
            $this->db->query('DROP VIEW packages_view');

        if ($this->db->table_exists('customer_transaction'))
            $this->db->query('DROP VIEW customer_transaction');

        if ($this->db->table_exists('customers_transaction_report'))
            $this->db->query('DROP VIEW customers_transaction_report');

        if ($this->db->table_exists('customers_report_detail'))
            $this->db->query('DROP VIEW customers_report_detail');

        if ($this->db->table_exists('order_payment_report'))
            $this->db->query('DROP VIEW order_payment_report');

        if ($this->db->table_exists('sale_comission_report_up'))
            $this->db->query('DROP VIEW sale_comission_report_up');

        if ($this->db->table_exists('sale_comission_report_down'))
            $this->db->query('DROP VIEW sale_comission_report_down');

        if ($this->db->table_exists('sale_comission_report_up_detail'))
            $this->db->query('DROP VIEW sale_comission_report_up_detail');

        if ($this->db->table_exists('sale_comission_report_down_detail'))
            $this->db->query('DROP VIEW sale_comission_report_down_detail');

        if ($this->db->table_exists('orders_view_location'))
            $this->db->query('DROP VIEW orders_view_location');

        if ($this->db->table_exists('orders_view_location_total'))
            $this->db->query('DROP VIEW orders_view_location_total');

        if ($this->db->table_exists('report_logistic_trips_city_start'))
            $this->db->query('DROP VIEW report_logistic_trips_city_start');

        if ($this->db->table_exists('report_logistic_trips_city_stop'))
            $this->db->query('DROP VIEW report_logistic_trips_city_stop');

        if ($this->db->table_exists('report_logistic_trips_city_detail'))
            $this->db->query('DROP VIEW report_logistic_trips_city_detail');

        if ($this->db->table_exists('report_logistic_armadas'))
            $this->db->query('DROP VIEW report_logistic_armadas');

        if ($this->db->table_exists('report_logistic_packages'))
            $this->db->query('DROP VIEW report_logistic_packages');

        if (!$this->db->table_exists('sale_comission_branch_origin'))
            $this->db->query('DROP VIEW sale_comission_branch_origin');

        if (!$this->db->table_exists('sale_comission_branch_origin_detail'))
            $this->db->query('DROP VIEW sale_comission_branch_origin_detail');

        if (!$this->db->table_exists('sale_comission_branch_destination'))
            $this->db->query('DROP VIEW sale_comission_branch_destination');

        if (!$this->db->table_exists('sale_comission_branch_destination_detail'))
            $this->db->query('DROP VIEW sale_comission_branch_destination_detail');

        if (!$this->db->table_exists('report_logistic_deliveries_1'))
            $this->db->query('DROP VIEW report_logistic_deliveries_1');

        if (!$this->db->table_exists('report_logistic_deliveries_1_detail'))
            $this->db->query('DROP VIEW report_logistic_deliveries_1_detail');

        if (!$this->db->table_exists('report_logistic_deliveries_2'))
            $this->db->query('DROP VIEW report_logistic_deliveries_2');

        if (!$this->db->table_exists('report_logistic_deliveries_2_detail'))
            $this->db->query('DROP VIEW report_logistic_deliveries_2_detail');

        if (!$this->db->table_exists('report_logistic_deliveries_3'))
            $this->db->query('DROP VIEW report_logistic_deliveries_3');

        if (!$this->db->table_exists('report_logistic_deliveries_3_detail'))
            $this->db->query('DROP VIEW report_logistic_deliveries_3_detail');

        if (!$this->db->table_exists('report_logistic_cities'))
            $this->db->query('DROP VIEW report_logistic_cities');

        if (!$this->db->table_exists('report_logistic_cities_detail'))
            $this->db->query('DROP VIEW report_logistic_cities_detail');

        if (!$this->db->table_exists('report_logistic_cities_total'))
            $this->db->query('DROP VIEW report_logistic_cities_total');

        echo "Table View Berhasil Dihapus";
    }

}

/** CREATE VIEW finance_report AS SELECT orders.order_id AS order_id, orders.order_status AS order_status, orders.order_date AS order_date, orders.order_use_tax AS order_use_tax, orders.order_sell_total AS order_sell_total, customers.customer_name AS customer_name, branches_origin.branch_name AS branch_name_origin, branches_destination.branch_name AS branch_name_destination, SUM(det_orders_extra.det_order_extra_total) AS extra_total FROM orders LEFT JOIN branches AS branches_origin ON orders.order_origin = branches_origin.branch_id LEFT JOIN branches AS branches_destination ON orders.order_destination = branches_destination.branch_id LEFT JOIN customers ON customers.customer_id = orders.customer_id LEFT JOIN det_orders_extra ON orders.order_id = det_orders_extra.order_id WHERE orders.order_status > -1 AND orders.deleted_at = 0 GROUP BY orders.order_id ORDER BY orders.order_date DESC */