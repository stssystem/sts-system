<?php

Class Lacak extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('orders_model');
    }

    public function get($resi_number = 0) {
        $result = $this->orders_model->get_join_package($resi_number)->row();
        echo json_encode($result);
    }

}
