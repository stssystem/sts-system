<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order_helper extends CI_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->model('customers_price_model');

        if (!$this->session->userdata('logged')) {
            redirect('program/login');
        }
    }

    public function price($mode = 'retail') {
        
        $data = json_decode($this->input->post('price_params'), true);

        /** Customer price parameter */
        $customer_id = $data['customer_id'];
        $customer_price = $data['customer_price'];

        $origin = $data['branch_id_origin'];
        $destination = $data['branch_id_destination'];

        $width = $data['package_width'];
        $height = $data['package_height'];
        $lenght = $data['package_lenght'];
        $size = $data['package_size'];
        $qty = $data['det_order_qty'];

        /** main parameter */
        $weight = ceil(($data['package_weight'] * $data['det_order_qty']));
        $volume = ($data['package_size'] * $data['det_order_qty']);
        $volume = ceil($volume / 400);
        
        $final_weight = ($weight > $volume) ? $weight : $volume;

        $this->load->model('prices_model');

        $result = [];
        if ($origin != '' && $destination != '') {
            $where = [
                'branch_id_origin' => $origin,
                'branch_id_destination' => $destination
            ];
            $result = $this->prices_model->get_price_list($where)->row();
        }

        $minimum = isset($result->price_minimum) ? (int) $result->price_minimum : 0;

        if (
                $customer_id != '' &&
                $customer_price != 0 &&
                $this->customers_price_model->get_price($customer_id, $origin, $destination) != null
        ) {
            $price_kg_retail = $this->customers_price_model->get_price($customer_id, $origin, $destination);
        } else {
            $price_kg_retail = isset($result->price_kg_retail) ? (int) $result->price_kg_retail : 0;
        }

        if ($final_weight <= 5) {
            echo $minimum;
        } else {
            echo ($minimum + (($final_weight - 5) * $price_kg_retail));
        }
    }

}
