<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends CI_Controller {

    public function __construct() {

        parent::__construct();

        if (!$this->session->userdata('logged')) {
            redirect('program/login');
        }

        if (!$this->input->is_ajax_request()) {
            show_404();
        }
    }

    public function cities($name = 'city_id_origin') {

        $this->load->model('cities_model');        

        $cities = $this->cities_model->get()->result_array();

        /** Additional class */
        $class = '';
        if ($name == 'city_id_origin') {
            $class = 'origin_geobranch';
            $label = 'Kota *';
            $required = true;
        } else if ($name == 'city_id_destination') {
            $class = 'destination_geobranch';
            $label = 'Kota *';
            $required = true;
        } else if ($name == 'customer_city_id'){
            $class = 'customer_geobranch';
            $label = 'Kota*';
            $required = true;
        } else if ($name == 'pic_city_id'){
            $class = 'pic_geobranch';
            $label = 'Kota*';
            $required = true;
        }

        $params = [
            'name' => $name,
            'label' => $label,
            'max' => 100,
            'required' => $required,
            'value' => $cities,
            'keys' => 'city_id',
            'values' => 'city_name',
            'class' => 'select2 col-md-12 select-districts select-price-params ' . $class
        ];

        echo $this->form->select_h($params);
    }

    public function districts($name = 'districts_id', $disabled = '') {

        $this->load->model('districts_model');

        $city_id = $this->input->post('city_id');

        $districts = $this->districts_model->get(['city_id' => $city_id])->result_array();

        /** Additional class */
        $class = '';
        if ($name == 'districts_id_origin') {
            $class = 'origin_geobranch';
        } else if ($name == 'districts_id_destination') {
            $class = 'destination_geobranch';
        } else if ($name == 'customer_districts_id') {
            $class = 'customer_geobranch';
        } else if ($name == 'pic_districts_id') {
            $class = 'pic_geobranch';
        }

        $params = [
            'name' => $name,
            'label' => 'Kecamatan',
            'max' => 100,
            'required' => false,
            'value' => $districts,
            'keys' => 'districts_id',
            'values' => 'districts_name',
            'class' => 'select2 col-md-12 select-districts select-price-params ' . $class,
            'disabled' => $disabled
        ];

        if ($disabled == 'true') {
            $params['disabled'] = true;
        }

        echo $this->form->select_h($params);
    }

    public function villages($name = 'village_id') {

        $this->load->model('villages_model');
        $districts_id = $this->input->post('districts_id');
        $villages = $this->villages_model->get(['district_id' => $districts_id])->result_array();

        /** Additional class */
        $class = '';
        if ($name == 'village_id_origin') {
            $class = 'origin_geobranch';
        } else if ($name == 'village_id_destination') {
            $class = 'destination_geobranch';
        } else if ($name == 'customer_village_id') {
            $class = 'customer_geobranch';
        } else if ($name == 'pic_village_id') {
            $class = 'pic_geobranch';
        } 

        echo $this->form->select_h(
                [
                    'name' => $name,
                    'label' => 'Kelurahan',
                    'max' => 100,
                    'required' => false,
                    'value' => $villages,
                    'keys' => 'village_id',
                    'values' => 'village_name',
                    'class' => 'select2 col-md-12 select-price-params ' . $class
                ]
        );
    }

    public function branches($name = 'branch_id', $disabled = '') {

        $this->load->model('branches_model');
        $city_id = $this->input->post('city_id');

        /** Branches params */
        $where = [];

        /** Additional class */
        $class = '';
        $pal_name = '';
        if ($name == 'branch_id_origin') {
            $class = 'origin_geobranch';
            $pal_name = 'origin_branch_code';
            $where = [
               # 'branch_city' => $city_id
            ];
        } else if ($name == 'branch_id_destination') {
            $class = 'destination_geobranch';
            $pal_name = 'destination_branch_code';
            $where = [
                #'branch_city' => $city_id
                #'branch_type' => '0' # Deactive, Until condition stable 
            ];
        }

        $branches = $this->branches_model->get_joined($where)->result_array();

        if (empty($branches)) {
            $this->load->model('cities_model');
            
             # Deactive, Until condition stable 
            // if ($name == 'branch_id_destination')
            //     $where_r['branch_type'] = 0;
            $branches = $this->branches_model->get_joined()->result_array();
        }

        $params = [
            'name' => $name,
            'label' => 'Cabang / Agen *',
            'max' => 100,
            'required' => true,
            'value' => $branches,
            'keys' => 'branch_id',
            'values' => 'branch_name',
            'class' => 'select2 col-md-12 select-price-params ' . $class,
            'use_default' => false,
            'pal_name' => $pal_name
        ];

        if ($disabled == 'true') {
            $params['disabled'] = true;
        }

        echo $this->form->cabang_h($params);
    }

    /** Function list for price list */
    public function prices_branch($name = '') {

        $this->load->model('branches_model');

        $city_id = $this->input->post('city_id');
        $selected = $this->input->post('selected');

        $branches = $this->branches_model->get(['branch_city' => $city_id])->result_array();

        echo $this->form->select(
                [
                    'name' => $name,
                    'label' => 'Cabang',
                    'max' => 100,
                    'required' => true,
                    'value' => $branches,
                    'keys' => 'branch_id',
                    'values' => 'branch_name',
                    'class' => 'select2',
                    'selected' => $selected
                ]
        );
    }

    public function get_geobranch($branch_id = 0) {

        $this->load->model('branches_model');

        if ($branch_id != 0) {
            $branches = $this->branches_model->get([
                        'branch_id' => $branch_id
                    ])->row();

            echo $branches->branch_geo;
        } else {
            echo '';
        }
    }

    public function get_branches() {
        $this->load->model('branches_model');
        echo json_encode($this->branches_model->get_joined()->result_array());
    }

    public function get_destination_branches() {
        $this->load->model('branches_model');
        echo json_encode($this->branches_model->get_joined()->result_array());
    }

    public function get_detail_branch($id = 0) {
        $this->load->model('branches_model');
        echo json_encode($this->branches_model->get_joined(['branches.branch_id' => $id])->row());
    }

    public function get_province_id() {
        $city_id = $this->input->post('city_id');
        $this->load->model('cities_model');
        echo $this->cities_model->get(['city_id' => $city_id])->row()->province_id;
    }

}
