<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_helper extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('logged')) {
            redirect('program/login');
        }

        $this->load->model('costumers_model');
    }

    public function get() {

        if ($this->input->is_ajax_request()) {

            $array = $this->costumers_model->get()->result();

            $data = [];
            if (!empty($array)) {
                foreach ($array as $key => $value) {

                    $data[$key]['customer_name'] = $value->customer_name;
                    $data[$key]['customer_code'] = $value->customer_code;
                    $data[$key]['code'] = $value->customer_name;
                    $data[$key]['customer_id'] = $value->customer_id;
                    $data[$key]['customer_birth_date'] = mdate('%d/%m/%Y', $value->customer_birth_date);
                }
            }

            echo json_encode($data);
        }
    }
    
    

    public function get_detail($customer_id = 0) {
        if ($this->input->is_ajax_request()) {
            $result = $this->costumers_model->get_joined(['customer_id' => $customer_id])->row();
            echo json_encode($result);
        }
    }

    public function get_all() {

        if ($this->input->is_ajax_request()) {

            $array = $this->costumers_model->get()->result();

            $data = [];
            if (!empty($array)) {
                foreach ($array as $key => $value) {
                    $data[$key]['customer_name'] = $value->customer_name .' - '. $value->customer_code;
                    $data[$key]['customer_code'] = $value->customer_code;
                    $data[$key]['code'] = $value->customer_name;
                    $data[$key]['customer_id'] = $value->customer_id;
                    $data[$key]['customer_birth_date'] = mdate('%d/%m/%Y', $value->customer_birth_date);
                }
            }

            echo json_encode($data);
            
        }
    }
    
    public function get_like() {

         if ($this->input->is_ajax_request()) {

            $params = $this->input->get('phrase');
            
            $array = $this->costumers_model->get_name_code([], ['like' => $params])->result();

            $data = [];
            if (!empty($array)) {
                foreach ($array as $key => $value) {
                    $data[$key]['customer_name'] = $value->customer_name;
                    $data[$key]['customer_code'] = $value->customer_code;
                    $data[$key]['code'] = $value->customer_name;
                    $data[$key]['customer_id'] = $value->customer_id;
                    $data[$key]['customer_birth_date'] = mdate('%d/%m/%Y', $value->customer_birth_date);
                }
            }

            echo json_encode($data);
         }
    }

}
