<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Franco extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'franco';
    public $controller_path = 'resi/franco';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "franco";
        } else {
            redirect('program/login');
        }
    }

    public function page_delivery() {
        $this->data['title'] = "Kirim Resi";
        $this->session->set_userdata('franco_session', array());
        $this->load->view('franco/delivery', $this->data);
    }

    public function proces_delivery() {

        // Load all resource needed
        $this->load->model('orders_model');
        $this->load->model('armadas_model');
        $this->load->model('franco_model');
        $this->load->library('request/request_franco_delivery');

        // Get order id by manual resi number 
        $noresi_sytem = $this->orders_model->get_order_id($this->input->post('order_id'));
        $noresi_manual = $this->input->post('order_id');
        if ($noresi_sytem === NULL) {
            $noresi_sytem = $this->input->post('order_id');
        }

        // Get all data from order         
        $orders = $this->orders_model->get_joined(['orders.order_id' => $noresi_sytem])->row_array();

        // Collecting data from user form
        $data = array(
            'order_id' => $noresi_sytem,
            'manual_resi' => $noresi_manual,
            'armada_id' => $this->input->post('armada'),
            'nopol' => $this->input->post('nopol'),
            'vendor_name' => $this->input->post('vendor_name'),
            'origin' => $orders['order_origin'],
            'destination' => $orders['order_destination'],
            'status' => 2,
            'costumer_id' => $orders['customer_id'],
            'created_at' => time()
        );

        // Get armada value
        if ($this->input->post('armada') == 'vendor') {
            $armada = $this->input->post('vendor_name') . ' - ' . $this->input->post('nopol');
        } else {
            $armada = $this->armadas_model->get_name($this->input->post('armada'));
        }

        // Insert data into franco table
        if ($this->request_franco_delivery->validation() &&
                !empty($orders) &&
                $this->franco_model->check_order_exist($noresi_sytem, 2) == null &&
                !in_array($noresi_sytem, $this->session->userdata('franco_session'))) {

            // Set check exist session order id
            $check_session = $this->session->userdata('franco_session');
            $check_session[] = $noresi_sytem;
            $this->session->set_userdata('franco_session', $check_session);

            $html = "<tr>"
                    . "<td>" . $orders['order_id'] . "</td>"
                    . "<td>" . $orders['customer_name'] . "</td>"
                    . "<td>" . $orders['branch_name_origin'] . '-' . $orders['branch_name_destination'] . "</td>"
                    . "<td>" . $armada . "</td>"
                    . "<td></td>"
                    . "<td class='td-action'>"
                    . "<input name='resi' type='hidden' value='" . json_encode($data) . "'  >"
                    . "<div class='btn-group pull-right'>"
                    . "<button type='button' class='btn btn-danger btn-sm btn-delete'><i class='fa fa-times'></i></button>"
                    . "</div>"
                    . "</td>"
                    . "</tr>";

            echo $html;
        } else {

            $result = '';

            if (!$this->request_franco_delivery->validation()) {
                $result = 1;
            } elseif (empty($orders)) {
                $result = 2;
            } elseif ($this->franco_model->check_order_exist($this->input->post('order_id'), 2) != null ||
                    in_array($this->input->post('order_id'), $this->session->userdata('franco_session'))) {
                $result = 3;
            }

            echo $result;
        }
    }

    public function record_delivery() {

        // Load all resource needed 
        $this->load->model('franco_model');
        $this->load->model('franco_detail_model');

        // Get data from user form
        $data = $this->input->post('data');

        // Insert into franco model
        if (!empty($data)) {
            foreach ($data as $key => $item) {

                // Convert data from json into array
                $array = json_decode($item['value'], true);

                // Check franco wheter exist or not
                $franco_id = $this->franco_model->check_order_exist($array['order_id']);

                if ($franco_id == null) {

                    // Collected data franco into array
                    $data_franco = array(
                        'order_id' => $array['order_id'],
                        'manual_resi' => $array['manual_resi'],
                        'origin' => $array['origin'],
                        'destination' => $array['destination'],
                        'status' => $array['status'],
                        'costumer_id' => $array['costumer_id'],
                        'created_at' => $array['created_at']
                    );

                    // Insert into franco and get last franco id 
                    $franco_id = $this->franco_model->insert($data_franco);
                } else {

                    // Collected data franco into array
                    $data_franco = array(
                        'franco_id' => $franco_id,
                        'status' => $array['status'],
                        'updated_at' => $array['created_at']
                    );

                    // Update data in franco table
                    $this->franco_model->update($franco_id, $data_franco);
                }

                // Check franco detail wheter exist or not 
                $franco_detail_id = $this->franco_detail_model->check_franco_exist($franco_id, 2);
                if ($franco_detail_id == null) {
                    
                     // Collected data franco detail into array
                    $data_franco_detail = array(
                        'franco_id' => $franco_id,
                        'armada_id' => $array['armada_id'],
                        'nopol' => $array['nopol'],
                        'vendor_name' => $array['vendor_name'],
                        'last_branch' => isset(branch()['branch_id']) ? branch()['branch_id'] : '',
                        'last_city' => isset(branch()['city_id']) ? branch()['city_id'] : '',
                        'last_geo' => isset(branch()['branch_geo']) ? branch()['branch_geo'] : '',
                        'note' => 'Pengantaran Resi',
                        'status' => $array['status'],
                        'created_at' => $array['created_at']
                    );

                    // Insert into franco detail
                    $this->franco_detail_model->insert($data_franco_detail);
                    
                } else {

                    // Collected data franco detail into array
                    $data_franco_detail = array(
                        'franco_id' => $franco_id,
                        'armada_id' => $array['armada_id'],
                        'nopol' => $array['nopol'],
                        'vendor_name' => $array['vendor_name'],
                        'last_branch' => isset(branch()['branch_id']) ? branch()['branch_id'] : '',
                        'last_city' => isset(branch()['city_id']) ? branch()['city_id'] : '',
                        'last_geo' => isset(branch()['branch_geo']) ? branch()['branch_geo'] : '',
                        'note' => 'Pengantaran Resi',
                        'status' => $array['status'],
                        'updated_at' => $array['created_at']
                    );
                    
                    // Update franco detail table
                    $this->franco_detail_model->update($franco_detail_id, $data_franco_detail);
                    
                }
                
            }
            
            // Flush all franco session 
            $this->session->set_userdata('franco_session', array());
            
            echo 'true';
            
        } else {
            
            echo 'false';
            
        }
        
    }

    public function cancel_row($order_id = 0) {

        $key = array_search($order_id, $this->session->userdata('franco_session'));

        if ($key !== false) {
            $data = $this->session->userdata('franco_session');
            unset($data[$key]);
            $this->session->set_userdata('franco_session', $data);
        }
    }

    public function cancel_row_all() {
        $this->session->set_userdata('franco_session', array());
    }

}
