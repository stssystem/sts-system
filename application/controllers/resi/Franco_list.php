<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Franco_list extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'franco_list';
    public $controller_path = 'resi/franco_list';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "franco";
        } else {

            redirect('program/login');
        }
    }

    public function index($status = 0) {

        $this->data['title'] = "Daftar Resi Franco";
        if ($this->input->is_ajax_request()) {

            // Load all resource needed
            $this->load->library('datatable/dt_franco_list');
            echo $this->dt_franco_list->get($this, $status, $_REQUEST);
            
        } else {

            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function self_take() {

        // Variable initiate
        $order_id = $this->input->post('order_id');

        // Load all resource needed
        $this->load->model('franco_model');
        $this->load->model('orders_model');

        // Get data order
        $orders = $this->orders_model->get_joined(
                        ['orders.order_id' => $order_id]
                )->row();

        // Check franco wheter exist or not
        $franco_id = $this->franco_model->check_order_exist($order_id);

        if ($franco_id == null) {

            // Collected data franco into array
            $data_franco = array(
                'order_id' => $order_id,
                'manual_resi' => $orders->order_manual_id,
                'costumer_id' => $orders->customer_id,
                'origin' => $orders->order_origin,
                'destination' => $orders->order_destination,
                'status' => 5,
                'note' => $this->input->post('receiver_name'),
                'created_at' => time()
            );

            // Insert into franco and get last franco id 
            $franco_id = $this->franco_model->insert($data_franco);
        } else {

            // Collected data franco into array
            $data_franco = array(
                'franco_id' => $franco_id,
                'status' => 5,
                'note' => $this->input->post('receiver_name'),
                'updated_at' => time()
            );

            // Update data in franco table
            $this->franco_model->update($franco_id, $data_franco);
        }

        set_alert('alert-success', 'Order dengan nomor resi '
                . '<strong>' . $order_id . ''
                . '</strong> diambil oleh '
                . $this->input->post('receiver_name'));

        redirect($this->controller_path);
    }

    public function filter() {

        // Load all resource needed
        $this->load->model('branches_model');

        // Set filter session value
        $data['as_branch'] = $this->input->post('as_branch');
        $data['as_branch_name'] = $this->branches_model->get_branch_name(
                $this->input->post('as_branch')
        );
        $this->session->set_userdata('franco_list_filter', $data);

        redirect($this->controller_path);
    }

    public function reset_status_filter() {
        $this->session->unset_userdata('franco_list_filter');
        $this->session->unset_userdata('dt_franco_list');
        redirect($this->controller_path);
    }

}
