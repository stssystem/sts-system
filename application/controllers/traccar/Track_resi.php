<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Track_resi extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'track_resi';
    public $controller_path = 'traccar/track_resi';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_traccar";
            $this->data['path'] = $this->controller_path;
        } else {
            redirect('program/login');
        }
    }

    public function index() {
        $this->data['title'] = "Track Resi";
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function search()
    {
        $this->load->model('orders_model');
        $this->load->model('orders_locations_model');
        $this->data['title'] = "Lacak Resi";

        $id = $this->input->post('order_id');

        if (empty($id)) {
            set_alert('alert-warning', 'Masukan nomor resi terlebih dahulu.');
            $this->load->view($this->view_path . '/index', $this->data);
        }

        $this->data['search'] = $this->orders_model->search($id)->row();

        if ($this->data['search'] == null) {
            $this->data['detail'] = array();
            set_alert('alert-danger', 'Nomor resi <strong>' . $this->input->post('order_id') . '</strong> yang anda masukan tidak tersedia. Masukan nomor resi lain.');
        } else {
            $this->data['last_date'] = $this->orders_locations_model->get_last($id)->created_at;
            $this->data['detail'] = $this->orders_locations_model->get_order(['orders_locations_order_id' => $this->data['search']->order_id])->result();
            set_alert('alert-success', 'Nomor resi <strong>' . $this->input->post('order_id') . '</strong> yang anda masukan telah terlacak.');
        }

        $this->load->view($this->view_path . '/index', $this->data);

    }


}
