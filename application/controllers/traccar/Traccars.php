<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Traccars extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'traccars';
    public $controller_path = 'traccar/traccars';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('devices_model');
            $this->load->model('armadas_model');
            
            $this->data['path'] = $this->controller_path;
            $this->data['armadas'] = $this->armadas_model->get()->result_array();
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_traccar";
        } else {
            redirect('program/login');
        }
    }

    public function index() {
        $this->data['title'] = "Traccar";
        $this->data['data'] = json_encode($this->devices_model->get_armada_lastposition());
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update() {
        
        /** Load all resource needed */
        $this->load->model('orders_model');
        
        /** Get data from server */
        $result = $this->devices_model->get_armada_lastposition();
        
        /** Update last location armada */ 
        $this->armadas_model->update_all_lastlocation($result);
        
        /** Update last location order */
//        $this->orders_model->update_last_location($result);
        
        echo json_encode($result);
        
    }

    public function get_cities() {

        echo json_encode($this->det_routes_model->get_cities(['route_id' => $this->input->get('route_id')]));
    }

}
