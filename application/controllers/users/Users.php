<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    private $controller_path;
    private $view_path;
    private $data = [];

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->load->model('users_model');
            $this->load->model('user_types_model');
            $this->load->model('user_profile_model');
        } else {
            redirect('program/login');
        }

        /** Set the url of controller */
        $this->controller_path = 'users/users';

        /** Set the path of view */
        $this->view_path = 'user_include';

        /** Set url path */
        $this->data['path'] = $this->controller_path;

        /** Dafault Data */
        $this->data['user_type_id'] = $this->user_types_model->get()->result_array();
        $this->data['title'] = "Pengaturan User";
        $this->data['title2'] = "";
        $this->data['active_menu'] = "p_user";

        /** Request validation  */
        $this->load->library('request/request_users');
        $this->load->library('request/request_users_profile');

        /** Datatables */
        $this->load->library('datatable/dt_users');
    }

    public function index() {
        if ($this->input->is_ajax_request()) {
            // echo $this->dt_users->get($this);
            echo $this->dt_users->get($this, $_REQUEST);
        } else {
            $this->load->view($this->view_path . '/user_management', $this->data);
        }
    }

    public function add() {
        $this->data['title'] = "Tambah User";
        $this->load->view('user_include/' . __FUNCTION__, $this->data);
    }

    public function store() {

        if (!$this->request_users->validation()) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            $where_array = array(
                'username' => $this->input->post('username'),
            );

            $query = $this->users_model->get_user($where_array);

            if ($query->num_rows() == 0) {

                /** User create */
                $users_array = array(
                    'username' => $this->input->post('username'),
                    'user_email' => $this->input->post('user_email'),
                    'user_type_id' => $this->input->post('user_type_id'),
                    'password' => md5($this->input->post('password')),
                    'user_status' => $this->input->post('user_status'),
                    'created_at' => time(),
                );

                $users_id = $this->users_model->insert_user($users_array);

                /** User Profile Create */
                $userprofile_array = array(
                    'user_id' => $users_id,
                    'userprofile_fullname' => $this->input->post('userprofile_fullname'),
                    'created_at' => time()
                );

                /** Check user branch access */
                $this->load->model('user_types_model');
                if ($this->user_types_model->is_branch_access($this->input->post('user_type_id'))) {
                    $this->load->model('user_profile_model');
                    $this->user_profile_model->open_branch_access($users_id);
                }

                $this->user_profile_model->insert($userprofile_array);

                redirect($this->controller_path);
            } else {
                set_alert('alert-danger', 'Usename <strong>' . $this->input->post('username') . '</strong> sudah terpakai');
                redirect($this->controller_path . '/add');
            }
        }
    }

    public function edit($users_id = 0) {
        $this->data['title'] = "Edit User";
        $this->data['user'] = $this->users_model->get_profile(['user.user_id' => $users_id])->row();
        $this->load->view('user_include/' . __FUNCTION__, $this->data);
    }

    public function update() {

        if (!$this->request_users->validation(['except' => ['password']])) {
            redirect($this->controller_path . '/edit/' . $this->input->post('user_id'), 'refresh');
        } else {

            $where_array = array(
                'username' => $this->input->post('username'),
            );

            $query = $this->users_model->get_user($where_array);

            if ($query->num_rows() > 0 && $query->row()->user_id != $this->input->post('user_id')) {

                set_alert('alert-danger', 'Usename <strong>' . $this->input->post('username') . '</strong> sudah terpakai');
                redirect('users/users/edit/' . $this->input->post('user_id'));
            } else {

                $users_array = array(
                    'user_id' => $this->input->post('user_id'),
                    'username' => $this->input->post('username'),
                    'user_email' => $this->input->post('user_email'),
                    'user_type_id' => $this->input->post('user_type_id'),
                    'user_status' => $this->input->post('user_status'),
                    'created_at' => time(),
                    'updated_at' => time()
                );

                if ($this->input->post('password') != '') {
                    $users_array['password'] = md5($this->input->post('password'));
                }

                $this->users_model->update($users_array);

                /** Check user branch access */
                $this->load->model('user_types_model');
                if ($this->user_types_model->is_branch_access($this->input->post('user_type_id'))) {
                    $this->load->model('user_profile_model');
                    $this->user_profile_model->open_branch_access($this->input->post('user_id'));
                }

                redirect($this->controller_path);
            }
        }
    }

    public function destroy($users_id = null) {
        $this->users_model->delete(['user_id' => $users_id]);
        set_alert('alert-danger', 'Data berhasil dihapus');
        redirect($this->controller_path);
    }

    public function view($user_id = null) {
        $this->data['title'] = "Edit Tipe User";
        $this->data['data'] = $this->users_model->get_profile($user_id)->row();
        $this->load->view($this->view_path . '/view', $this->data);
    }

    public function update_branch_user() {

        $branch_id = $this->input->post('branch_id');
        $array = json_decode($this->input->post('user_list'), true);

        if (!empty($array[0])) {
            foreach ($array[0] as $key => $value) {
                $data = [
                    'user.user_id' => $value,
                    'userprofile.branch_id' => $branch_id
                ];
                $this->users_model->update_joined($data);
            }
        }

        $data['users_available'] = $this->users_model->get_profile(['userprofile.branch_id' => $branch_id])->result();
        $this->load->view('branches/user_staf_list', $data);
    }

    public function delete_staft($id = 0, $branch_id = 0) {

        $data = [
            'user.user_id' => $id,
            'userprofile.branch_id' => NULL
        ];

        $this->users_model->update_joined($data);
    }

    public function get() {
        echo $this->dt_users->get($this);
    }

    public function add_regional_staff() {

        /** Get data from user request */
        $regional_id = $this->input->post('regional_id');
        $staff = json_decode($this->input->post('users'), true)[0];

        /** Load all resource needed */
        $this->load->model('users_model');

        /** Add regional staff */
        $this->users_model->add_regional_staff($staff, $regional_id);

        $result['regional_staff'] = $this->users_model->get_regional_staff($regional_id)->result();
        $this->load->view('regionals/user_staf_list', $result);
    }

    public function get_regional_staff($regional_id) {

        /** Load all resource needed */
        $this->load->model('users_model');

        /** Get regional staff */
        $result = $this->users_model->get_regional_staff($regional_id);

        /** Send resullt */
        echo json_encode($result);
    }

    public function delete_regional_staff($users_id) {
        $this->users_model->delete_regional_staff($users_id);
        return true;
    }

    public function get_options() {

        $this->load->helper('users');

        echo $this->form->select([
            'name' => 'users',
            'label' => '',
            'value' => get_users_list(),
            'keys' => 'user_id',
            'values' => 'name',
            'multiple' => true,
            'class' => 'select2',
            'display_default' => false
        ]);
    }

}
