<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_types extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'user_type';
    public $controller_path = 'users/user_types';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('user_types_model');
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_user_type";
            $this->id = $this->input->post('user_type_id');

            $this->load->library('request/request_user_types');
            $this->load->library('datatable/datatables');
            $this->load->library('datatable/dt_user_types');
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        $this->data['title'] = "Pengaturan Tipe User";

        if ($this->input->is_ajax_request()) {
            echo $this->dt_user_types->get($this);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add() {
        $this->data['title'] = "Tambah User Type";
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store() {

        if (!$this->request_user_types->validation()) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            /** User create */
            $users_array = array(
                'user_type_name' => $this->input->post('user_type_name'),
                'user_type_access' => json_encode($this->input->post('user_type_access')),
                'branch_access' => $this->input->post('branch_access'),
                'created_at' => time(),
            );

            $user_type_id = $this->user_types_model->insert($users_array);

            set_alert('alert-success', 'Tipe User <strong>' . $this->input->post('user_type_name') . '</strong> berhasil dibuat');

            redirect($this->controller_path, 'refresh');
        }
    }

    public function edit($user_type_id = 0) {

        $this->data['title'] = "Edit Tipe User";
        $this->data['data'] = $this->user_types_model->get(['user_type_id' => $user_type_id])->row();

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update() {

        if (!$this->request_user_types->validation()) {
            redirect($this->controller_path . '/edit/' . $this->id, 'refresh');
        } else {

            $where_array = array(
                'user_type_id' => $this->id,
                'user_type_name' => $this->input->post('user_type_name'),
                'user_type_access' => json_encode($this->input->post('user_type_access')),
                'branch_access' => $this->input->post('branch_access'),
                'updated_at' => time()
            );

            $this->user_types_model->update($where_array);

            set_alert('alert-success', 'Tipe User <strong>' . $this->input->post('user_type_name') . '</strong> berhasil diperbarui');

            redirect($this->controller_path);
        }
    }

    public function destroy($user_type_id = null) {
        $this->user_types_model->delete(['user_type_id' => $user_type_id]);
        set_alert('alert-danger', 'Data berhasil dihapus');
        redirect($this->controller_path);
    }

}
