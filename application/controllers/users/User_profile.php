<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_profile extends CI_Controller {

    private $controller_path = 'users/user_profile';
    private $view_path = 'user_profile';
    private $data = [];

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->load->model('user_types_model');
            $this->load->model('user_profile_model');
            $this->load->model('users_model');
            $this->data['path'] = $this->controller_path;
        } else {
            redirect('program/login');
        }
    }

    public function edit($user_id = 0) {

        $this->data['title'] = "Edit Tipe User";
        $this->data['title2'] = '';
        $this->data['user_type_id'] = $this->user_types_model->get()->result();
        $this->data['data'] = $this->users_model->get_profile($user_id)->row();

        $this->load->view($this->view_path . '/edit', $this->data);
    }

    public function update() {

        // User input validation
        $config = array(
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|max_length[30]'
            ], [
                'field' => 'user_email',
                'label' => 'Email',
                'rules' => 'required|max_length[100]'
            ], [
                'field' => 'user_type_id',
                'label' => 'Tipe User',
                'rules' => 'required'
            ], [
                'field' => 'userprofile_fullname',
                'label' => 'Nama Lengkap',
                'rules' => 'required|max_length[100]'
            ], [
                'field' => 'userprofile_address',
                'label' => 'Alamat',
                'rules' => 'required'
            ]
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            set_alert('alert-danger', validation_errors());
            redirect($this->controller_path . '/edit/'. $this->input->post('user_id'), 'refresh');
        } else {

            /** User Update */
            $where_array = array(
                'user_id' => $this->input->post('user_id'),
                'username' => $this->input->post('username'),
                'user_email' => $this->input->post('user_email'),
                'user_type_id' => $this->input->post('user_type_id'),
                'updated_at' => time()
            );

            /** Update Password  */
            if (
                    $this->input->post('old_password') != '' &&
                    $this->input->post('new_password') != '' &&
                    $this->input->post('confirm_password') != ''
            ) {

                $user_password = $this->users_model->get_user(['user_id' => $this->input->post('user_id')])->row();

                if ($user_password->password == md5($this->input->post('old_password'))) {
                    if ($this->input->post('new_password') === $this->input->post('confirm_password')) {
                        $where_array['password'] = md5($this->input->post('new_password'));
                    } else {
                        set_alert('alert-danger', 'Password baru tidak sama dengan konfirmasi password baru');
                        redirect($this->controller_path . '/edit/' . $this->input->post('user_id'));
                    }
                } else {
                    set_alert('alert-danger', 'Password lama Anda salah');
                    redirect($this->controller_path . '/edit/' . $this->input->post('user_id'));
                }
            }

            $this->users_model->update($where_array);

            /** User Profile Update */
            $userprofile = $this->user_profile_model->get(['userprofile_id' => $this->input->post('userprofile_id')])->row();
            
            $where_array = array(
                'userprofile_id' => $this->input->post('userprofile_id'),
                'userprofile_fullname' => $this->input->post('userprofile_fullname'),
                'userprofile_address' => $this->input->post('userprofile_address'),
                'updated_at' => time()
            );

            /** Upload Photo Profile  */
            if ($_FILES['userfile']['name'] != '') {

                /* Deleting old file */
                if ($userprofile->userprofile_photo != '') {
                    unlink("./assets/upload/" . $userprofile->userprofile_photo);
                }
                $where_array['userprofile_photo'] = $this->save_photo_profile($this->input->post('user_id'));
            }

            /* If image delted */
            if ($this->input->post('delete_photo')) {
                if ($userprofile->userprofile_photo != '') {
                    unlink("./assets/upload/" . $userprofile->userprofile_photo);
                }
                $where_array['userprofile_photo'] = '';
            }

            $this->user_profile_model->update($where_array);

            set_alert('alert-success', 'Data berhasil diperbarui');

            redirect($this->controller_path . '/edit/' . $this->input->post('user_id'));
        }
    }

    public function save_photo_profile($id = '') {

        $config['upload_path'] = './assets/upload/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '5000';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect($this->controller_path . '/edit/' . $id, 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }
    }

}
