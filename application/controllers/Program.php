<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller {

    private $controller_path = 'program';
    private $view_path = 'global_include';
    private $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('sys_config_model');
        $this->data['title2'] = '';
        $this->data['path'] = $this->controller_path;
        $this->load->library('request/request_sys_config');
    }

    public function index() {


        if ($this->session->userdata('logged')) {
            $data['title'] = "Dashboard";
            $data['title2'] = "Control Panel";
            $data['active_menu'] = "dashboard";
            $this->load->view('dashboard', $data);
        } else {

            redirect('program/login');
        }
    }

    public function login() {
        $this->load->view('global_include/login');
    }

    public function logout() {
        $this->session->sess_destroy();
        blank_sessorder();
        redirect('program');
    }

    function process_login() {

        $where_array = array(
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password'))
        );

        $query = $this->users_model->get_user($where_array);

        if ($query->num_rows() == 1) {

            $this->session->set_userdata('logged', true);
            $this->session->set_userdata('user_meta', $query->row());

            $set_array = array(
                'user_id' => $query->row()->user_id,
                'last_login' => time()
            );

            $this->users_model->update($set_array);
        }

        redirect('program');
    }

    public function edit($id = null) {

        $this->data['title'] = 'Setting Konfigurasi Sistem';
        $this->data['data'] = $this->sys_config_model->get()->row();

        $this->load->view('system_config/edit', $this->data);
    }

    public function update($id = null) {

        if ($this->request_sys_config->validation()) {

            $where_array = array(
                'company_address' => $this->input->post('company_address'),
                'company_name' => $this->input->post('company_name'),
                'online_url' => $this->input->post('online_url'),
                'traccar_url' => $this->input->post('traccar_url'),
                'traccar_db' => $this->input->post('traccar_db'),
                'traccar_db_username' => $this->input->post('traccar_db_username'),
                'traccar_db_password' => $this->input->post('traccar_db_password'),
                'web_url' => $this->input->post('web_url'),
                'web_db' => $this->input->post('web_db'),
                'web_db_username' => $this->input->post('web_db_username'),
                'web_db_password' => $this->input->post('web_db_password'),
                'license_key' => $this->input->post('license_key'),
            );

            $sys_config = $this->sys_config_model->get()->row();

            /** Upload Photo Profile  */
            if ($_FILES['userfile']['name'] != '') {

                /* Deleting old file */
                if ($sys_config->main_logo != '') {
                    unlink("./assets/upload/" . $sys_config->main_logo);
                }
                $where_array['main_logo'] = $this->save_photo_profile();
            }

            /* If image delted */
            if ($this->input->post('delete_photo')) {
                if ($sys_config->main_logo != '') {
                    unlink("./assets/upload/" . $sys_config->main_logo);
                }
                $where_array['main_logo'] = '';
            }

            $this->sys_config_model->update($where_array);
        }

        redirect('program/edit');
    }

    public function save_photo_profile($id = '') {

        $config['upload_path'] = './assets/upload/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '5000';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect($this->controller_path . '/edit', 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }
    }

    /* all for forum */

    public function forum() {
        $this->data['title'] = "Forum Diskusi";
        $this->db->select('messages.*');
        $this->db->select('user.username');
        $this->db->join('user', 'user.user_id=messages.user_id');
        $this->db->where('messages.message_parent', 0);
        $this->db->where('messages.message_status', 0);
        $this->data['query'] = $this->db->get('messages');
        $this->load->view('global_include/forum_index', $this->data);
    }

    public function forum_add_new($message_parent = 0) {
        $this->data['title'] = "Buat Diskusi Baru";
        $this->data['message_parent'] = $message_parent;
        $this->load->view('global_include/forum_add_new', $this->data);
    }

    public function forum_save() {
        if ($this->input->post()) {
            $set_array = array(
                'message_title' => $this->input->post('message_title'),
                'message_content' => $this->input->post('message_content'),
                'message_parent' => $this->input->post('message_parent'),
                'user_id' => $this->session->userdata('user_meta')->user_id,
                'created_at' => time()
            );
            $this->db->set($set_array);
            $this->db->insert('messages');
            if ($this->input->post('message_parent') == 0)
                redirect('program/forum');
            else
                redirect('program/forum_view/' . $this->input->post('message_parent'));
        }
    }

    public function forum_view($message_id = 0) {
        $this->db->select('messages.*');
        $this->db->select('user.username');
        $this->db->join('user', 'user.user_id=messages.user_id');
        $this->db->where('messages.message_id', $message_id);
        $query = $this->db->get('messages');
        $this->data['row'] = $query->row();
        $this->data['title'] = "Detail Diskusi";

        $this->db->select('messages.*');
        $this->db->select('user.username');
        $this->db->join('user', 'user.user_id=messages.user_id');
        $this->db->where('messages.message_parent', $message_id);
        $this->data['query'] = $this->db->get('messages');
        $this->load->view('global_include/forum_view', $this->data);
    }

    public function forum_close($message_id) {
        $this->db->where('message_id', $message_id);
        $this->db->delete('messages');
        redirect('program/forum');
    }

    /* end of forum */


    /* all pickup order */

    public function pick_up_order() {
        $db = db_connect('web');
        $this->data['query'] = $db->get('wp_ss_pick_up_order');
        $this->data['title'] = "Daftar Order Dari Website";
        $this->load->view('global_include/pick_up_order', $this->data);
    }

    // Autoclose
    public function autoclose() {
        $this->data['title'] = "Akses Halaman Pengiriman Baru";
        $this->load->view('autoclose', $this->data);
    }

    // Remove url temporary session
    public function remove_url_session() {

        // Load all resource needed 
        $this->load->helper('urlstore');
        $tab = $this->input->post('current_url');

        $url_arr_temp = $this->session->userdata('url-arr-temp');
        if (is_array($url_arr_temp)) {
            if (key_exists($tab, $url_arr_temp) && $url_arr_temp[$tab] == 'orders/orders') {
                  url_remove('orders/orders');
            }
        }

//        $current_url = str_replace(site_url() . '/', '', $this->input->post('current_url'));
//        url_remove($current_url);
    }

    // Switch page 
    public function switch_page() {
        $this->load->helper('urlstore');
        $tab = $this->input->post('tab');
        set_last_tab($tab);
    }

    // Save url array session
    public function url_arr_session() {

        // Load all resource needed 
        $this->load->helper('urlstore');

        $url = $this->input->post('url');
        $tab = $this->input->post('tab');

        urlarr_store($url, $tab);
    }

}
