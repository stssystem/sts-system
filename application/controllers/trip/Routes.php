<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Routes extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'routes';
    public $controller_path = 'trip/routes';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('routes_model');
            $this->load->model('det_routes_model');
            $this->load->model('branches_model');

            $this->load->library('request/request_routes');
            $this->load->library('datatable/dt_routes');

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "trip-manage";
            $this->data['branches'] = $this->branches_model->get_joined_cities()->result_array();

            $this->id = $this->input->post('route_id');
            
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        $this->data['title'] = "Daftar Rute";

        if ($this->input->is_ajax_request()) {
            echo $this->dt_routes->get($this, $_REQUEST);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add() {
        $this->data['title'] = "Tambah Rute";
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store() {

        if (!$this->request_routes->validation()) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            /** route create */
            $set_array = array(
                'route_name' => $this->input->post('route_name'),
                'route_status' => $this->input->post('route_status'),
                'created_at' => time(),
            );

            $this->id = $this->routes_model->insert($set_array);

            /** Route detail create */
            $det_route = $this->input->post('det_route');
            $det_route = json_decode($det_route, true);

            $det_route_array = [];
            $i = count($det_route);
            if ($i >= 2) {

                if (!empty($det_route)) {
                    foreach ($det_route as $key => $value) {
                        if ($key <= ($i - 2)) {
                            $det_route_array[$key]['route_id'] = $this->id;
                            $det_route_array[$key]['route_city_start'] = $value['city_id'];
                            $det_route_array[$key]['route_city_end'] = $det_route[$key + 1]['city_id'];
                        }
                    }
                }

                $this->det_routes_model->insert($det_route_array, true);
            }

            set_alert('alert-success', 'Data rute <strong>' . $this->input->post('route_name') . '</strong> berhasil ditambahkan');

            redirect($this->controller_path, 'refresh');
        }
    }

    public function edit($id = 0) {

        $this->data['title'] = "Edit Rute";
        $this->data['data'] = $this->routes_model->get(['route_id' => $id])->row();
        $this->data['det_route'] = $this->det_routes_model->get_cities(['route_id' => $id]);
        $this->data['det_route'] = json_encode($this->data['det_route']);

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update() {

        if (!$this->request_routes->validation()) {
            redirect($this->controller_path . '/edit/' . $this->id, 'refresh');
        } else {

            $where_array = array(
                'route_id' => $this->id,
                'route_name' => $this->input->post('route_name'),
                'route_status' => $this->input->post('route_status'),
                'updated_at' => time()
            );

            $this->routes_model->update($where_array);

            /** Route detail create */
            $this->det_routes_model->delete(['route_id' => $this->id], true);

            $det_route = $this->input->post('det_route');
            $det_route = json_decode($det_route, true);

            $det_route_array = [];
            $i = count($det_route);
            if ($i >= 2) {

                if (!empty($det_route)) {
                    foreach ($det_route as $key => $value) {
                        if ($key <= ($i - 2)) {
                            $det_route_array[$key]['route_id'] = $this->id;
                            $det_route_array[$key]['route_city_start'] = $value['city_id'];
                            $det_route_array[$key]['route_city_end'] = $det_route[$key + 1]['city_id'];
                        }
                    }
                }

                $this->det_routes_model->insert($det_route_array, true);
            }

            set_alert('alert-success', 'Data rute <strong>' . $this->input->post('route_name') . '</strong> telah diperbarui');

            redirect($this->controller_path);
        }
    }

    public function destroy($id = null) {

        $data = $this->routes_model->delete(['route_id' => $id]);
        $this->det_routes_model->delete(['route_id' => $id], true);

        set_alert('alert-danger', 'Data rute <strong>' . $data->route_name . '</strong> telah dihapus');

        redirect($this->controller_path);
        
    }
    
    public function reset(){
        $this->session->unset_userdata('dt_routes');
        redirect($this->controller_path);
    }

}
