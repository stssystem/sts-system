<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Armadas extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'armadas';
    public $controller_path = 'trip/armadas';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->id = $this->input->post('armada_id');
            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "trip-manage";
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        // Switch respon based on request type (ajax or not)
        if ($this->input->is_ajax_request()) {

            // Load all resource needed
            $this->load->library('datatable/dt_armadas');

            // Echo json datatable format 
            echo $this->dt_armadas->get($this, $_REQUEST);
            
        } else {

            // Set data for view page
            $this->data['title'] = "Daftar Nama Kota";

            // Load view
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add() {

        // Load all resource needed
        $this->load->model('users_model');

        // Fill data that shown on page
        $this->data['users'] = $this->users_model->get_profile()->result_array();
        $this->data['title'] = "Tambah Data Armada";

        // Load view
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store() {

        // Load all resource needed
        $this->load->model('armadas_model');
        $this->load->library('request/request_armadas');

        // Validation input from user form
        if (!$this->request_armadas->validation()) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            // Fill data from user form
            $set_array = array(
                'traccar_device_id' => $this->input->post('traccar_device_id'),
                'armada_name' => $this->input->post('armada_name'),
                'armada_v_width' => $this->input->post('armada_v_width'),
                'armada_v_long' => $this->input->post('armada_v_long'),
                'armada_v_height' => $this->input->post('armada_v_height'),
                'armada_v_m3' => $this->input->post('armada_v_m3'),
                'armada_license_plate' => $this->input->post('armada_license_plate'),
                'armada_reg_date' => strtotime($this->input->post('armada_reg_date')),
                'armada_custom_code' => $this->input->post('armada_custom_code'),
                'armada_status' => $this->input->post('armada_status'),
                'armada_tonase' => $this->input->post('armada_tonase'),
                'armada_kir_number' => strtotime($this->input->post('armada_kir_number')),
                'armada_stnk_number' => strtotime($this->input->post('armada_stnk_number')),
                'armada_driver' => $this->input->post('armada_driver'),
                'created_at' => time(),
            );

            // Upload Photo Profile  
            if ($_FILES['userfile']['name'] != '') {
                $set_array['armada_photo'] = $this->save_photo_profile();
            }

            // Insert armada data into armada table
            $this->armadas_model->insert($set_array);

            // Set success alert message
            set_alert('alert-success', 'Data armada <strong>' . $this->input->post('armada_name') . '</strong> berhasil ditambahkan');

            // Redirect into armada list
            redirect($this->controller_path, 'refresh');
        }
    }

    public function edit($id = 0) {

        // Load all resource needed
        $this->load->model('armadas_model');
        $this->load->model('users_model');

        // Fill data that shown on page
        $this->data['title'] = "Edit Data Armada";
        $this->data['users'] = $this->users_model->get_profile()->result_array();
        $this->data['data'] = $this->armadas_model->get(['armada_id' => $id])->row();

        // Load view
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update() {
        
        // Load all resource needed
        $this->load->model('armadas_model');
        $this->load->library('request/request_armadas');

        // Validation input from user form
        if (!$this->request_armadas->validation()) {
            redirect($this->controller_path . '/edit/' . $this->id, 'refresh');
        } else {

            // Fill data from user form
            $where_array = array(
                'armada_id' => $this->id,
                'traccar_device_id' => $this->input->post('traccar_device_id'),
                'armada_name' => $this->input->post('armada_name'),
                'armada_v_width' => $this->input->post('armada_v_width'),
                'armada_v_long' => $this->input->post('armada_v_long'),
                'armada_v_height' => $this->input->post('armada_v_height'),
                'armada_v_m3' => $this->input->post('armada_v_m3'),
                'armada_license_plate' => $this->input->post('armada_license_plate'),
                'armada_reg_date' => strtotime($this->input->post('armada_reg_date')),
                'armada_custom_code' => $this->input->post('armada_custom_code'),
                'armada_status' => $this->input->post('armada_status'),
                'armada_tonase' => $this->input->post('armada_tonase'),
                'armada_kir_number' => strtotime($this->input->post('armada_kir_number')),
                'armada_stnk_number' => strtotime($this->input->post('armada_stnk_number')),
                'armada_driver' => $this->input->post('armada_driver'),
                'updated_at' => time()
            );

            // Get armada info
            $armada = $this->armadas_model->get(['armada_id' => $this->id])->row();

            // Upload Photo Profile 
            if ($_FILES['userfile']['name'] != '') {

                // Deleting old file
                if ($armada->armada_photo != '') {
                    unlink("./assets/upload/" . $armada->armada_photo);
                }
                $where_array['armada_photo'] = $this->save_photo_profile();
            }

            // If image delted
            if ($this->input->post('delete_photo')) {
                if ($armada->armada_photo != '') {
                    unlink("./assets/upload/" . $armada->armada_photo);
                }
                $where_array['armada_photo'] = '';
            }

            // Update armada table
            $this->armadas_model->update($where_array);

            // Set success alert message
            set_alert('alert-success', 'Data armada <strong>' . $this->input->post('armada_name') . '</strong> telah diperbarui');

            // Redirect into armada list
            redirect($this->controller_path);
        }
    }

    public function destroy($id = null) {

        // Load all resource needed
        $this->load->model('armadas_model');

        // Delete from armada table
        $data = $this->armadas_model->delete(['armada_id' => $id]);
        
         // Set success alert message
        set_alert('alert-danger', 'Data armada <strong>' . $data->armada_name . '</strong> telah dihapus');
        
        // Redirect into armada list
        redirect($this->controller_path);
        
    }

    public function view($id = '') {
        
         // Load all resource needed
        $this->load->model('armadas_model');

        // Fill data that shown on page
        $this->data['title'] = "Detail Data Armada";
        $this->data['data'] = $this->armadas_model->get_joined(['armada_id' => $id])->row();

        // Load view
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        
    }
    
    public function reset(){        
        $this->session->unset_userdata('dt_armadas');
        redirect($this->controller_path);
    }

    public function save_photo_profile($id = '') {

        $config['upload_path'] = './assets/upload/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '5000';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            if ($id == '') {
                redirect($this->controller_path . '/add', 'refresh');
            } else {
                redirect($this->controller_path . '/edit/' . $id, 'refresh');
            }
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }
    }

}
