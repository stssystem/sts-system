<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Trips extends CI_Controller {

    private $data = [];
    private $id;
    private $view_path = 'trips';
    public $controller_path = 'trip/trips';

    public function __construct() {

        parent::__construct();

        if ($this->session->userdata('logged')) {

            $this->load->model('trips_model');
            $this->load->model('det_trips_model');
            $this->load->model('armadas_model');
            $this->load->model('users_model');
            $this->load->model('routes_model');
            $this->load->model('branches_model');
            $this->load->model('det_routes_model');

            $this->load->library('request/request_trips');
            $this->load->library('datatable/dt_trips');

            $this->data['path'] = $this->controller_path;
            $this->data['title2'] = "";
            $this->data['active_menu'] = "p_trips";
            $this->data['users'] = $this->users_model->get_profile()->result_array();
            $this->data['armadas'] = $this->armadas_model->get()->result_array();
            $this->data['routes'] = $this->routes_model->get()->result_array();
            $this->data['branches'] = $this->branches_model->get_joined_cities()->result_array();

            $this->id = $this->input->post('trip_id');
        } else {
            redirect('program/login');
        }
    }

    public function index() {

        $this->data['title'] = "Daftar Jadwal Perjalanan";

        if ($this->input->is_ajax_request()) {
            echo $this->dt_trips->get($this, $_REQUEST);
        } else {
            $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
        }
    }

    public function add() {

        // Add charter option in routes
        $temp['-1'] = ['route_id' => -1, 'route_name' => 'Charter'];
        foreach($this->data['routes'] as $key => $item){
            $temp[] = ['route_id' => $item['route_id'], 'route_name' =>  $item['route_name']];
        }
        $this->data['routes'] = $temp;
        
        $this->data['title'] = "Tambah Jadwal Perjalanan";
        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function store() {

        if (!$this->request_trips->validation()) {
            redirect($this->controller_path . '/add', 'refresh');
        } else {

            /** route create */
            $set_array = array(
                'route_id' => $this->input->post('route_id'),
                'armada_id' => $this->input->post('armada_id'),
                'user_id' => $this->input->post('user_id'),
                'trip_codriver' => $this->input->post('trip_codriver'),
                'trip_start_date' => strtotime($this->input->post('trip_start_date')),
                'trip_end_date' => strtotime($this->input->post('trip_end_date')),
                'created_at' => time(),
            );

            $this->id = $this->trips_model->insert($set_array);

            /** Trips detail create */
            $det_route = $this->input->post('det_trips');
            $det_route = json_decode($det_route, true);

            $det_route_array = [];
            $i = count($det_route);
            if ($i >= 2) {

                if (!empty($det_route)) {
                    foreach ($det_route as $key => $value) {
                        if ($key <= ($i - 2)) {
                            $det_route_array[$key]['trip_id'] = $this->id;
                            $det_route_array[$key]['det_trip_city_start'] = $value['city_id'];
                            $det_route_array[$key]['det_trip_city_stop'] = $det_route[$key + 1]['city_id'];
                        }
                    }
                }

                $this->det_trips_model->insert($det_route_array, true);
            }

            /** Insert detail trips */
            set_alert('alert-success', 'Data perjalanan berhasil ditambahkan');

            redirect($this->controller_path, 'refresh');
        }
    }

    public function edit($id = 0) {
        
          // Add charter option in routes
        $temp['-1'] = ['route_id' => -1, 'route_name' => 'Charter'];
        foreach($this->data['routes'] as $key => $item){
            $temp[] = ['route_id' => $item['route_id'], 'route_name' =>  $item['route_name']];
        }
        $this->data['routes'] = $temp;

        $this->data['title'] = "Edit Jadwal Perjalanan";
        $this->data['data'] = $this->trips_model->get(['trip_id' => $id])->row();
        $this->data['det_trips'] = json_encode($this->det_trips_model->get_cities(['trip_id' => $id]));

        $this->load->view($this->view_path . '/' . __FUNCTION__, $this->data);
    }

    public function update() {

        if (!$this->request_trips->validation()) {
            redirect($this->controller_path . '/edit/' . $this->id, 'refresh');
        } else {

            $where_array = array(
                'trip_id' => $this->id,
                'route_id' => $this->input->post('route_id'),
                'armada_id' => $this->input->post('armada_id'),
                'user_id' => $this->input->post('user_id'),
                'trip_start_date' => strtotime($this->input->post('trip_start_date')),
                'trip_end_date' => strtotime($this->input->post('trip_end_date')),
                'trip_codriver' => $this->input->post('trip_codriver'),
                'updated_at' => time()
            );

            $this->trips_model->update($where_array);

            /** Create trip detail */
            $this->det_trips_model->delete(['trip_id' => $this->id], true);

            /** Trips detail create */
            $det_route = $this->input->post('det_trips');
            $det_route = json_decode($det_route, true);

            $det_route_array = [];
            $i = count($det_route);
            if ($i >= 2) {

                if (!empty($det_route)) {
                    foreach ($det_route as $key => $value) {
                        if ($key <= ($i - 2)) {
                            $det_route_array[$key]['trip_id'] = $this->id;
                            $det_route_array[$key]['det_trip_city_start'] = $value['city_id'];
                            $det_route_array[$key]['det_trip_city_stop'] = $det_route[$key + 1]['city_id'];
                        }
                    }
                }

                $this->det_trips_model->insert($det_route_array, true);
            }

            set_alert('alert-success', 'Data perjalanan telah diperbarui');

            redirect($this->controller_path);
        }
    }

    public function destroy($id = null) {

        $data = $this->trips_model->delete(['trip_id' => $id]);
        $this->det_trips_model->delete(['trip_id' => $id], true);

        set_alert('alert-danger', 'Data perjalanan telah dihapus');

        redirect($this->controller_path);
    }

    public function get_cities() {

        echo json_encode($this->det_routes_model->get_cities(['route_id' => $this->input->get('route_id')]));
    }

}
