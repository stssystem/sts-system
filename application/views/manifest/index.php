<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-md-12">
        <a href="<?php echo site_url('manifests/manifest/printout'); ?>" class="btn btn-info print-manifest">
            <i class="fa fa-print"></i> Print Manifest
        </a>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-sm-12">
        <div class="row">

            <?php if (!empty($data)): ?>
                <?php foreach ($data as $key => $value) : ?>

                    <div class="col-sm-4">
                        <div class="box">
                            <div class="box-header">


                            </div>
                            <div class="box-body">
                                <table class="table">
                                    <tr><td>Total Paket</td><td><?php echo $value->total_package; ?></td></tr>
                                    <tr><td>Armada</td><td><?php echo $value->armada_name; ?></td></tr>
                                    <tr><td>Asal</td><td><?php echo $value->origin_city_name; ?></td></tr>
                                    <tr><td>Tujuan</td><td><?php echo $value->destination_city_name; ?></td></tr>
                                    <tr><td>Tanggal</td><td><?php echo mdate('%d %F %Y', $value->manifest_date); ?></td></tr>
                                    <tr><td>Driver</td><td><?php echo $value->driver; ?></td></tr>
                                </table>
                            </div>
                            <div class="box-footer">
                                <a href="<?php echo site_url($path . '/view/' . $value->manifest_id); ?>" class="btn btn-sm btn-primary">Lihat Detail</a>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
            <?php endif; ?>

        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>

<script type="text/javascript">
    $(document).ready(function () {

        $('.print-manifest').click(function (e) {
            e.preventDefault();
            targets = $(this).attr('href');
            $("<iframe>")                             // create a new iframe element
                    .hide()                               // make it invisible
                    .attr("src", targets) // point the iframe to the page you want to print
                    .appendTo("body");
        });
        
    });
</script>