<?php $this->load->view('header'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Daftar Paket</h3>
            </div>
            <div class="box-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Resi</th>
                            <th>Paket</th>
                            <th>Berat</th>
                            <th>Volume</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($data)): ?>
                            <?php foreach ($data as $key => $value) : ?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td><?php echo $value->package_id; ?></td>
                                    <td><?php echo $value->package_title; ?></td>
                                    <td><?php echo $value->package_weight; ?> kg</td>
                                    <td><?php echo $value->package_size; ?> m3</td>
                                    <td><?php echo package_status($value->package_status); ?> </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <a href="<?php echo site_url($path); ?>" class="btn btn-info">Kembali Ke Daftar Manifest</a> 
                <a href="#" class="btn btn-danger">Proses Manifest</a> <br/>
                <span class="small">Pastikan Sebelum Menekan tombol ini, proses loading fisik telah selesai dilaksanakan</span>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>