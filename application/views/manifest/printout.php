<style type="text/css">
    @page {
        size: A4;
        margin:0;
    }

    @media print {
        html, body {
            width: 210mm;
            height: 297mm;

            margin-top: 20px;
            margin-bottom: 20px;
            margin-left: 20px;
            margin-right: 20px;

        }
        /* ... the rest of the rules ... */
    }

    .pagebreak { page-break-before: always; } 

    .logo{
        height:60px;
        width:60px;
    }

    .address{
        font-size: 12px;
    }

    .noted{
        font-size: 13px;
    }


    .body-div{
        width: 195mm;
        margin-top: 20px;
    }

    .aligns-bottom{
        vertical-align: bottom;
    }

    .aligns-right{
        text-align: left;
    }


</style>

<html>
    <body>

        <?php if (!empty($data)): ?>
            <?php foreach ($data as $key => $value) : ?>

                <div class="body-div">
                    <table>
                        <tr>
                            <th>
                                <div class="logo">
                                    <img src="<?php echo base_url('assets/public/sts-logo.png'); ?>" class="logo">
                                </div>
                            </th>

                            <td class="address" width="60%" >

                                <div>
                                    PT. SANTOSO TEGUH SAKTI
                                </div>
                                <div>
                                    JL. SOEKARNO HATTA 21 MAGELANG
                                </div>
                                <div>
                                    TELP. 0293-364040
                                </div>
                            </td>

                            <td class="noted aligns-bottom" width="40%" >
                                Tanggal : <?php echo mdate('%d %F %Y', $value['manifest']->manifest_date); ?>

                        </tr>

                    </table>
                    <hr/>

                    <table width="100%" >
                        <tr>
                            <td width="60%">
                                <table>
                                    <tr class="noted">
                                        <td >Total Paket</td>
                                        <td>:</td>   
                                        <td><?php echo $value['manifest']->total_package; ?></td>
                                    </tr>
                                    <tr class="noted">
                                        <td width="20%">Armada</td>
                                        <td>:</td>
                                        <td><?php echo $value['manifest']->armada_name; ?></td>
                                    </tr>
                                    <tr class="noted" >
                                        <td>Driver</td>
                                        <td>:</td>
                                        <td><?php echo $value['manifest']->driver; ?></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="40%">
                                <table>
                                    <tr class="noted" >
                                        <td width="20%" >Asal</td>
                                        <td>:</td>
                                        <td><?php echo $value['manifest']->origin_city_name; ?></td>
                                    </tr>
                                    <tr class="noted">
                                        <td>Tujuan</td>
                                        <td>:</td>
                                        <td><?php echo $value['manifest']->destination_city_name; ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <h4>Daftar Paket</h4>
                    <table border="1" width="100%">
                        <tr>
                            <th width="15%">No Resi</th>
                            <th width="45%" >Nama Paket</th>
                            <th width="20%">Dimensi Paket</th>
                            <th width="20%">Berat Paket</th>
                        </tr>

                        <?php if (!empty($value['manifest_detail'])): ?>
                            <?php foreach ($value['manifest_detail'] as $k => $v) : ?>
                                <tr>
                                    <td><?php echo $v->order_id; ?></td>
                                    <td><?php echo $v->package_title; ?></td>
                                    <td><?php echo $v->package_size; ?> m3</td>
                                    <td><?php echo $v->package_weight; ?> Kg</td>                            
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </table>


                </div>

                <div class="pagebreak"> </div>

            <?php endforeach; ?>
        <?php endif; ?>

    </body>
</html>

<script type="text/javascript">
    window.print();
</script>

