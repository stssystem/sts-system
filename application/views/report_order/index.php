<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open($path . '/filter'); ?>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Tanggal Awal</label>
                            <input type="text" name="date_start_filter" class="form-control"
                                   value="<?php echo ($this->session->userdata('filter')['date_start']) ? $this->session->userdata('filter')['date_start'] : date('01/m/Y'); ?>"
                                   />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Tanggal Akhir</label>
                            <input type="text" name="date_end_filter" class="form-control"
                                   value="<?php echo ($this->session->userdata('filter')['date_end']) ? $this->session->userdata('filter')['date_end'] : date('t/m/Y'); ?>"
                                   />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Asal</label>
                            <select name="origin_filter" class="form-control select2">
                                <option value="0" >-- Pilih Cabang --</option>
                                <?php if (!empty($cities)): ?>
                                    <?php foreach ($cities as $key => $item): ?>
                                        <?php if ($this->session->userdata('filter')['branch_origin'] && $this->session->userdata('filter')['branch_origin'] == $item['branch_id']): ?>
                                            <option value="<?php echo $item['branch_id']; ?>" selected="selected"><?php echo $item['branch_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo $item['branch_id']; ?>"><?php echo $item['branch_name']; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Tujuan</label>
                            <select name="destination_filter" class="form-control select2">
                                <option value="0">-- Pilih Cabang --</option>
                                <?php if (!empty($cities)): ?>
                                    <?php foreach ($cities as $key => $item): ?>
                                        <?php if ($this->session->userdata('filter')['branch_destination'] && $this->session->userdata('filter')['branch_destination'] == $item['branch_id']): ?>
                                            <option value="<?php echo $item['branch_id']; ?>" selected="selected"><?php echo $item['branch_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo $item['branch_id']; ?>"><?php echo $item['branch_name']; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="btn-group">
                            <button id="filter" class="btn btn-filter btn-info"><i class="fa fa-filter"></i> Filter</button>
                            <a href="<?php echo site_url($path . '/clear'); ?>" class="btn btn-warning"><i class="fa fa-eraser"></i> Reset</a>
                        </div>
                        <a href="<?php echo site_url($path . '/download'); ?>" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Download</a>
                        <hr />
                    </div>
                    <?php echo form_close(); ?>

                    <div class="col-md-12">
                        <?php get_alert(); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 table-responsive ">
                        <table class="table data-tables">
                            <thead>
                                <tr class="txt-sm">
                                    <th width="10%" >#ID</th>
                                    <th width="20%" >Nama</th>
                                    <th width="15%" >Asal</th>
                                    <th width="15%" >Tujuan</th>
                                    <th width="7%" >Total</th>
                                    <th width="7%" >Total Ekstra</th>
                                    <th width="7%" >Jumlah</th>
                                    <th width="11%" >Status</th>
                                    <th width="8%" >Action</th>
                                </tr>
                            </thead>
                            <tbody class="txt-sm">

                            </tbody>
                            <tfoot>
                                <tr  style="background-color: #dcdcdc;">
                                    <th colspan="4"  rowspan="2"  style="vertical-align: middle;border-right:solid 1px #ffffff;" >Total</th>
                                    <th class="ptotal-sell" style="border-right: solid 1px #ffffff;" ></th>
                                    <th class="ptotal-extra"  style="border-right: solid 1px #ffffff;" ></th>
                                    <th class="ptotal-sell-extra" ></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr style="background-color: #dcdcdc;">
                                    <th class="total-sell" style="border-right: solid 1px #ffffff;"></th>
                                    <th class="total-extra" style="border-right: solid 1px #ffffff;" ></th>
                                    <th class="total-sell-extra"></th>
                                    <th colspan="2"></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>


<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Numeral -->
<script src="<?php echo base_url(); ?>assets/plugins/numeral/numeral.min.js"></script>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    /** Delete report */
    $('body').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus order dengan nomor resi <strong>" + data['order_id'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);

        /** Set url */
        var url = "<?php echo site_url('report/report_order/destroy'); ?>/" + data['order_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** New order list */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        target = target.replace("#", "");
        data_tables(target);
    });

    $('input[name="date_start_filter"]').datepicker({format: 'dd/mm/yyyy'});
    $('input[name="date_end_filter"]').datepicker({format: 'dd/mm/yyyy'});

    $(".select2").select2();
    $(".select2").select2();

    /** Data tables set up */
    data_tables();
    function data_tables() {

        var url = '<?php echo site_url($path) ?>';

        var tables = $('.data-tables').DataTable({
            ordering: true,
            serverSide: true,
            order: [
                [
<?php dtarray_mlt('dt_report_order', 'order', 'columns', 0); ?>,
                    "<?php dtarray_mlt('dt_report_order', 'order', 'mode', 'desc'); ?>"
                ]
            ],
            displayStart: <?php dtarray('dt_report_order', 'start', 0); ?>,
            pageLength: <?php dtarray('dt_report_order', 'lenght', 10); ?>,
            search: {
                search: "<?php dtarray('dt_report_order', 'search', ''); ?>"
            },
            ajax: {
                url: url,
                type: 'POST'
            },
            columns: [
                {data: 'order_id'},
                {data: 'customer_name'},
                {data: 'order_origin'},
                {data: 'order_destination'},
                {data: 'order_sell_total'},
                {data: 'extra_total'},
                {data: 'total'},
                {data: 'status'},
                {data: 'action'}
            ], footerCallback: function (row, data, start, end, display) {

                // Total 
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/\./g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                var total_sell = api
                        .column(4)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                var total_extra = api
                        .column(5)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                var total_sell_extra = api
                        .column(6)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Update footer
                $('.ptotal-sell').html(total_sell.format());
                $('.ptotal-extra').html(total_extra.format());
                $('.ptotal-sell-extra').html(total_sell_extra.format());

                // Get total from all page and update the footer 
                $.get("<?php echo site_url($path . '/get_sum'); ?>", function (data) {
                    var result = JSON.parse(data);
                    $('.total-sell').html(result['total_sell']);
                    $('.total-extra').html(result['total_extra']);
                    $('.total-sell-extra').html(result['total_sell_extra']);
                });



            }
        });

    }

    Number.prototype.format = function () {
        return this.toString().split(/(?=(?:\d{3})+(?:\.|$))/g).join(".");
    };

</script>