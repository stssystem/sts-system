<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/extensions/FixedHeader/css/dataTables.fixedHeader.min.css">

<style type="text/css">

    .fixed-column{
        background-color: #E4E4E4;
    }

    thead tr th{
        background-color: #E4E4E4;
    }

    tfoot tr th{
        background-color: #E4E4E4;
    }
</style>
<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">

    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <br/>
                    <div class="form-group col-md-12">
                        <div class="col-md-8">
                            <select class="form-control" name="year">
                                <?php $year = $this->uri->segment(4); ?>
                                <?php for ($y = 2014; $y <= date('Y'); $y++): ?>
                                    <option class="<?php echo $y; ?>" <?php echo (($year != '' && $y == $year) || ($year == '' && $y == date('Y'))) ? 'selected="selected"' : ''; ?> ><?php echo $y; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <a href="<?php echo site_url($path) ?>" class="btn btn-info btn-flat btn-year">
                                <i class="fa fa-plus"></i> Tampilkan
                            </a>
<!--                            <a href="<?php echo site_url($path) ?>" class="btn btn-danger btn-flat">
                                <i class="fa fa-file-pdf-o"></i> Export PDF
                            </a>
                            <a href="<?php echo site_url($path) ?>" class="btn btn-success btn-flat">
                                <i class="fa fa-file-excel-o"></i> Export Excel
                            </a>-->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">

            </div>
            <div class="box-body">
                <?php get_alert(); ?>

                <table class="table data-tables">
                    <thead>
                        <tr>
                            <th style="width:160px;" >Cabang</th>
                            <th style="width:160px;" >Januari</th>
                            <th style="width:160px;" >Februari</th>
                            <th style="width:160px;" >Maret</th>
                            <th style="width:160px;" >April</th>
                            <th style="width:160px;" >Mei</th>
                            <th style="width:160px;" >Juni</th>
                            <th style="width:160px;" >Juli</th>
                            <th style="width:160px;" >Agustus</th>
                            <th style="width:160px;" >September</th>
                            <th style="width:160px;" >Oktober</th>
                            <th style="width:160px;" >November</th>
                            <th style="width:160px;" >Desember</th>
                            <th style="width:160px;" >Total</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>	

                    <tfoot>
                        <tr>
                            <th style="width:160px;" >Jumlah</th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                            <th style="width:140px;" ></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/numeral/numeral.min.js"></script>

<script type="text/javascript" >

    /** Data tables set up */
    var tables = $('.data-tables').DataTable({
        scrollX: true,
        scrollCollapse: true,
        fixedColumns: {
            leftColumns: 1,
            footer: true
        },
        ajax: {
            url: '<?php echo site_url($path) ?>/index/' +  $('select[name="year"]').val(),
            dataSrc: ''
        },
        columns: [
            {data: 'branch_name'},
            {data: '1'},
            {data: '2'},
            {data: '3'},
            {data: '4'},
            {data: '5'},
            {data: '6'},
            {data: '7'},
            {data: '8'},
            {data: '9'},
            {data: '10'},
            {data: '11'},
            {data: '12'},
            {data: 'total'}
        ],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                        i.replace(/[\$.]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
            };

            for (k = 1; k <= 13; k++) {

                /** Total  */
                var total = api
                        .column(k)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                /** Format total */
                total = numeral(total).format('0,0');

                /** Update footer value */
                $(api.column(k).footer()).html(total);

            }

        },
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td:eq(0)', nRow).addClass("fixed-column");
        }
    });

    $('.btn-year').click(function (event) {
        event.preventDefault();
        window.location.href = $(this).attr('href') + '/index/' + $('select[name="year"]').val();
    });

</script>