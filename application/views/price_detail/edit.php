<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open($path . '/update/' . $data->price_id . '/' . $origin . '/' . $destination, ['class' => 'form-edit']); ?>

                <div class="row">
                    <div class="col-md-6">
                        <h4>Kota dan Cabang Asal</h4>
                        <?php
                        echo $this->form->select(
                                [
                                    'name' => 'branch_origin',
                                    'label' => 'Pilih Cabang Asal',
                                    'required' => true,
                                    'value' => get_branches_list($origin),
                                    'keys' => 'branch_id',
                                    'values' => 'branch_name',
                                    'class' => 'select2',
                                    'disabled' => true,
                                    'selected' => $data->price_branch_origin_id
                                ]
                        );
                        ?>

                    </div>
                    <div class="col-md-6">
                        <h4>Kota dan Cabang Tujuan</h4>
                        <?php
                        echo $this->form->select(
                                [
                                    'name' => 'branch_destination',
                                    'label' => 'Pilih Cabang Tujuan',
                                    'required' => true,
                                    'value' => get_branches_list($destination),
                                    'keys' => 'branch_id',
                                    'values' => 'branch_name',
                                    'class' => 'select2',
                                    'disabled' => true,
                                    'selected' => $data->price_branch_destination_id
                                ]
                        );
                        ?>

                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <h4>Detail Harga</h4>
                        <?php
                        echo $this->form->text(
                                [
                                    'name' => 'price_minimum',
                                    'label' => 'Harga Minimal',
                                    'required' => true,
                                    'type' => 'number',
                                    'value' => $data->price_minimum,
                                    'class' => '',
                                    'disabled' => false
                                ]
                        );
                        ?>
                    </div>

                    <div class="col-md-6">
                        <?php
                        echo $this->form->text(
                                [
                                    'name' => 'price_kg_retail',
                                    'label' => 'Harga / Kg Retail',
                                    'required' => true,
                                    'type' => 'number',
                                    'value' => $data->price_kg_retail,
                                    'class' => '',
                                    'disabled' => false
                                ]
                        );
                        ?>

                        <?php
                        echo $this->form->text(
                                [
                                    'name' => 'price_volume_retail',
                                    'label' => 'Harga / M3 Retail',
                                    'required' => true,
                                    'type' => 'number',
                                    'value' => $data->price_volume_retail,
                                    'class' => '',
                                    'disabled' => false
                                ]
                        );
                        ?>

                    </div>

                    <div class="col-md-6">
                        <?php
                        echo $this->form->text(
                                [
                                    'name' => 'price_kg_partai',
                                    'label' => 'Harga / Kg Partai',
                                    'required' => true,
                                    'type' => 'number',
                                    'value' => $data->price_kg_partai,
                                    'class' => '',
                                    'disabled' => false
                                ]
                        );
                        ?>

                        <?php
                        echo $this->form->text(
                                [
                                    'name' => 'price_volume_partai',
                                    'label' => 'Harga / M3 Partai',
                                    'required' => true,
                                    'type' => 'number',
                                    'value' => $data->price_volume_partai,
                                    'class' => '',
                                    'disabled' => false
                                ]
                        );
                        ?>
                    </div>

                </div>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url($path . "/index/$origin/$destination"); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <button type="submit" class="btn-info btn btn-save">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<?php $this->load->view('prices/style'); ?>

<script type="text/javascript">
    get_branch('price_branch_origin_id', $('select[name="city_origin"] option:selected').val(), '<?php echo $data->price_branch_origin_id; ?>');
    get_branch('price_branch_destination_id', $('select[name="city_destination"] option:selected').val(), '<?php echo $data->price_branch_destination_id; ?>');
</script>
