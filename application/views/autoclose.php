
<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body table-responsive ">
                <div class="alert alert-danger" role="alert">
                    <strong>Peringatan!</strong> Anda tidak dapat membuka halaman pengiriman baru lebih dari satu 
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">
    
    alert('Anda tidak dapat membuka halaman pengiriman baru lebih dari satu');
    window.close();

</script>