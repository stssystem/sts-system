<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open($path . '/update/'. $data->customer_price_id .'/'. $customer_id . '/' . $origin . '/' . $destination, ['class' => 'form-edit']); ?>

                <div class="row">
                    <div class="col-md-6">
                        <?php
                        echo $this->form->select(
                                [
                                    'name' => 'branch_origin',
                                    'label' => 'Cabang Asal',
                                    'required' => true,
                                    'value' => get_branches_list($origin),
                                    'keys' => 'branch_id',
                                    'values' => 'branch_name',
                                    'class' => 'select2',
                                    'display_default' => true,
                                    'selected' => $data->customer_branch_id_origin,
                                    'disabled' => true
                                ]
                        );
                        ?>

                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->form->select(
                                [
                                    'name' => 'branch_destination',
                                    'label' => 'Cabang Tujuan',
                                    'required' => true,
                                    'value' => get_branches_list($destination),
                                    'keys' => 'branch_id',
                                    'values' => 'branch_name',
                                    'class' => 'select2',
                                    'display_default' => true,
                                    'selected' => $data->customer_branch_id_destination,
                                    'disabled' => true
                                ]
                        );
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php
                        echo $this->form->text(
                                [
                                    'name' => 'customer_price',
                                    'label' => 'Harga Langganan',
                                    'required' => true,
                                    'type' => 'number',
                                    'value' => $data->customer_price,
                                    'class' => '',
                                    'disabled' => false
                                ]
                        );
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url($path . '/index/' . $customer_id."/$origin/$destination"); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <button type="submit" class="btn-info btn btn-save">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<?php $this->load->view('prices/style'); ?>
 