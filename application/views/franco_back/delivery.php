<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<style type="text/css">
    .unloadtype-class{
        margin-top: -23px;
    }

    .txt-sm{
        font-size: 14px;
    }

</style>
<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Pilih Armada</h3>
            </div>
            <div class="box-body">

                <div class="row">

                    <div class="col-md-4" >
                        <div class="form-group armadas-select">
                            <select class="form-control select2" name="armada">
                                <option>-- Pilih Armada --</option>
                                <option value="vendor" >VENDOR</option>
                                <?php if (get_armada_norestict()): ?>
                                    <?php foreach (get_armada_norestict() as $key => $item): ?>
                                        <option value="<?php echo $key; ?>"><?php echo $item; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <input class="form-control" name="vendor_name" placeholder="Nama Vendor" />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <input class="form-control" name="nopol" placeholder="Nomor Polisi" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Scan Resi &nbsp;<span class="loading-proces"></span></h4>
                        <input type="text" class="form-control input-lg is-correct-armada" name="order_id" id="order_id">
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="error-alert">

                        </div>
                        <form class="form-loading">
                            <table class="table table-bordered table-striped txt-sm">
                                <thead>
                                    <tr>
                                        <th colspan="6">
                                            Daftar Paket Siap Diangkut
                                        </th>
                                    </tr>
                                    <tr>
                                        <th width="10%" >No Resi</th>
                                        <th width="20%" >Nama Pengirim</th>
                                        <th width="30%" >Asal - Tujuan</th>
                                        <th width="25%" >Armada / Vendor</th>
                                        <th width="25%" >Keterangan</th>
                                        <th width="10%" ></th>
                                    </tr>
                                </thead>
                                <tbody id="load_result" >

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>

            </div>
            <div class="box-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-danger btn-flat btn-delete-all"><i class="fa fa-times"></i> Hapus Semua</button>
                    <button type="button" class="btn btn-info btn-flat btn-delivery"><i class="fa fa-truck"></i> Terima Resi</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        // Vendor switch
        $('input[name="vendor_name"]').attr('disabled', 'true');
        $('input[name="nopol"]').attr('disabled', 'true');

        $('select[name="armada"]').change(function () {
            if ($(this).find('option:selected').val() == 'vendor') {
                $('input[name="vendor_name"]').removeAttr('disabled');
                $('input[name="nopol"]').removeAttr('disabled');
            } else {
                $('input[name="vendor_name"]').val('');
                $('input[name="nopol"]').val('');
                $('input[name="vendor_name"]').attr('disabled', 'true');
                $('input[name="nopol"]').attr('disabled', 'true');
            }
        });

        // Select2
        $(".select2").select2();

        // Enter
        $('input[name="order_id"]').keypress(function (event) {
            if (event.which == 13) {

                // Get data from input
                var armada = $('select[name="armada"] option:selected').val();
                var vendor_name = $('input[name="vendor_name"]').val();
                var nopol = $('input[name="nopol"]').val();
                var order_id = $('input[name="order_id"]').val();

                // Show the spinner 
                $('.loading-proces').append('<span class="fa fa-spinner fa-spin"></span>');

                // Send into server by post
                $.post("<?php echo site_url($path . '/proces_delivery'); ?>",
                        {armada: armada, vendor_name: vendor_name, nopol: nopol, order_id: order_id}, function (data) {

                    $('.loading-proces').empty();
                    $('.error-alert').empty();

                    if (data === '1' || data === '2' || data === '3') {

                        if (data === '1') {
                            $('.error-alert').append('<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Peringatan!</strong> Pastikan semua form terisi</div>');
                        }

                        if (data === '2') {
                            $('.error-alert').append('<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Peringatan!</strong> Resi tidak ditemukan</div>');
                        }

                        if (data === '3') {
                            $('.error-alert').append('<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Peringatan!</strong> Nomor resi sudah dalam proses pengantaran</div>');
                        }

                    } else {
                        $('#load_result').append(data);
                        $("input[name='order_id']").val('');
                        $("input[name='order_id']").focus();
                    }

                });

            }
        });

        // Delete row 
        $('body').on('click', '.btn-delete', function () {

            // Collected data from user form
            var input = $(this).parent().parent().parent().find('input[name="resi"]').val();
            var html = $(this).parent().parent().parent();
            input = JSON.parse(input);

            // Send data into server by get
            $.get("<?php echo site_url($path . '/cancel_row'); ?>/" + input['order_id'], function (data) {
                $(html).remove();
            });

        });

        // Delivery
        $('.btn-delivery').click(function () {

            // Get data from user form
            var data = $('.form-loading').serializeArray();

            // Show the spinner 
            $(this).html('<span class="fa fa-spinner fa-spin"></span> Sedang Proses');
            $(this).attr('disabled', true);

            // Send into server by post
            $.post("<?php echo site_url($path . '/record_delivery'); ?>", {data: data}, function (data) {

                $('.btn-delivery').removeAttr('disabled');
                $('.btn-delivery').html('<i class="fa fa-truck"></i> Antar Resi');

                if (data === 'true') {
                    $('.td-action').html('<span class="label label-success"><i class="fa fa-check"></i> Berhasil</span>')
                }

            });

        });

        /** Remove all package */
        $('.btn-delete-all').click(function () {
         
            // Send data into server by get
            $.get("<?php echo site_url($path . '/cancel_row_all'); ?>", function (data) {
                   $("#load_result").empty();
            });

        });

    });
</script>