<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open($path . '/store/' . $customer_id, ['class' => 'form-add']); ?>

                <div class="row">
                    <div class="col-md-6">
                        <?php
                        echo $this->form->select(
                                [
                                    'name' => 'city_origin',
                                    'label' => 'Kota Asal',
                                    'required' => true,
                                    'value' => $cities,
                                    'keys' => 'city_id',
                                    'values' => 'city_name',
                                    'class' => 'select2',
                                    'display_default' => true
                                ]
                        );
                        ?>

                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->form->select(
                                [
                                    'name' => 'city_destination',
                                    'label' => 'Kota Tujuan',
                                    'required' => true,
                                    'value' => $cities,
                                    'keys' => 'city_id',
                                    'values' => 'city_name',
                                    'class' => 'select2',
                                    'display_default' => true
                                ]
                        );
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php
                        echo $this->form->text(
                                [
                                    'name' => 'customer_price',
                                    'label' => 'Harga Langganan',
                                    'required' => true,
                                    'type' => 'number',
                                    'value' => '',
                                    'class' => '',
                                ]
                        );
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url($path . '/index/' . $customer_id); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <button type="submit" class="btn-info btn btn-save">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>
<?php $this->load->view('customers_price/style'); ?>



