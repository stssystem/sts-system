<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8">
                        <h4><?php echo $title; ?></h4>    
                    </div>
                    <div class="col-md-4">
                        <div class="box-tools pull-right">
                            <div class="btn-group">
                                <a href="<?php echo site_url('customers/customers') ?>" class="btn btn-default">
                                    <i class="fa fa-arrow-left"></i> Kembali
                                </a>
                                <a href="<?php echo site_url($path . '/add/' . $customer_id) ?>" class="btn btn-success">
                                    <i class="fa fa-plus"></i> Tambah Harga Langganan
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <table class="table data-tables">
                    <thead>
                        <tr>
                            <th width="5%" >ID</th>
                            <th width="23%" >Asal</th>
                            <th width="23%" >Tujuan</th>
                            <th width="15%" >Harga</th>
                            <th width="20%" >Dibuat</th>
                            <th width="14%" >Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>	
                </table>
            </div>
        </div>
    </div>
</div>



<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    /** Data tables set up */
    $('.data-tables').DataTable({
        ajax: {
            url: '<?php echo site_url($path . '/index/' . $customer_id) ?>',
            dataSrc: ''
        },
        columns: [
            {data: 'customer_price_id'},
            {data: 'customer_origin'},
            {data: 'customer_destination'},
            {data: 'customer_price'},
            {data: 'created_at'},
            {data: 'action'}
        ]
    }
    );

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus harga langganan untuk jalur <strong>" + data['customer_origin'] + ' - ' + data['customer_destination'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);

        /** Set url */
        var url = "<?php echo site_url($path . '/destroy/' . $customer_id) ?>/" + data['city_id_origin'] + '/' + data['city_id_destination'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    })

</script>