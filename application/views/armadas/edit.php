<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open_multipart($path . '/update', ['class' => 'form-edit']); ?>

                <input type="hidden" name="armada_id" value="<?php echo $data->armada_id ?>" />

                <div class="row">
                    <div class="col-md-9">

                        <div class="row">

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'armada_name',
                                        'label' => 'Nama Armada',
                                        'max' => 100,
                                        'required' => true,
                                        'type' => 'text',
                                        'value' => $data->armada_name,
                                        'class' => 'col-md-6',
                                    ]
                            );
                            ?>

                            <?php
                            echo $this->form->select(
                                    [
                                        'name' => 'armada_driver',
                                        'label' => 'Supir Armada',
                                        'value' => $users,
                                        'required' => true,
                                        'class' => 'col-md-6 select2',
                                        'keys' => 'uid',
                                        'values' => 'userprofile_fullname',
                                        'selected' => $data->armada_driver
                                    ]
                            );
                            ?>

                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <h4>Dimensi Armada</h4>
                            </div>

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'armada_v_width',
                                        'label' => 'Lebar',
                                        'required' => true,
                                        'class' => 'col-md-3 grep-volume',
                                        'type' => 'number',
                                        'value' => $data->armada_v_width
                                    ]
                            );

                            echo $this->form->text(
                                    [
                                        'name' => 'armada_v_long',
                                        'label' => 'Panjang',
                                        'required' => true,
                                        'class' => 'col-md-3 grep-volume',
                                        'type' => 'number',
                                        'value' => $data->armada_v_long
                                    ]
                            );

                            echo $this->form->text(
                                    [
                                        'name' => 'armada_v_height',
                                        'label' => 'Tinggi',
                                        'required' => true,
                                        'class' => 'col-md-3 grep-volume',
                                        'type' => 'number',
                                        'value' => $data->armada_v_height
                                    ]
                            );

                            echo $this->form->text(
                                    [
                                        'name' => 'armada_v_m3',
                                        'label' => 'Volume',
                                        'required' => true,
                                        'class' => 'col-md-3 grep-volume',
                                        'type' => 'number',
                                        'value' => $data->armada_v_m3
                                    ]
                            );
                            ?>
                        </div>

                        <div class="row">
                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'armada_tonase',
                                        'label' => 'Tonase',
                                        'required' => true,
                                        'class' => 'col-md-12',
                                        'type' => 'number',
                                        'value' => $data->armada_tonase
                                    ]
                            );
                            ?>
                        </div>

                        <div class="row">
                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'armada_license_plate',
                                        'label' => 'Plat Nomor',
                                        'required' => true,
                                        'class' => 'col-md-6',
                                        'type' => 'text',
                                        'max' => 20,
                                        'value' => $data->armada_license_plate
                                    ]
                            );

                            echo $this->form->text(
                                    [
                                        'name' => 'armada_reg_date',
                                        'label' => 'Tanggal Registrasi',
                                        'required' => true,
                                        'class' => 'col-md-6',
                                        'type' => 'text',
                                        'value' => mdate('%d/%m/%Y', $data->armada_reg_date)
                                    ]
                            );
                            ?>
                        </div>

                        <div class="row">
                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'armada_kir_number',
                                        'label' => 'Tanggal KIR',
                                        'required' => true,
                                        'class' => 'col-md-6',
                                        'type' => 'text',
                                        'max' => 100,
                                        'value' => mdate('%d/%m/%Y', $data->armada_kir_number)
                                    ]
                            );

                            echo $this->form->text(
                                    [
                                        'name' => 'armada_stnk_number',
                                        'label' => 'Tanggal STNK',
                                        'required' => true,
                                        'class' => 'col-md-6',
                                        'type' => 'text',
                                        'max' => 100,
                                        'value' => mdate('%d/%m/%Y', $data->armada_stnk_number)
                                    ]
                            );
                            ?>
                        </div>

                        <div class="row">
                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'armada_custom_code',
                                        'label' => 'Kode Armada',
                                        'required' => true,
                                        'class' => 'col-md-6',
                                        'type' => 'text',
                                        'max' => 100,
                                        'value' => $data->armada_custom_code
                                    ]
                            );

                            echo $this->form->text(
                                    [
                                        'name' => 'traccar_device_id',
                                        'label' => 'ID Device',
                                        'required' => true,
                                        'class' => 'col-md-6',
                                        'type' => 'text',
                                        'max' => 100,
                                        'value' => $data->traccar_device_id
                                    ]
                            );
                            ?>
                        </div>

                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12" style="height: 100%;">
                                <label>Foto Armada</label>
                                <a href="#" class="thumbnail" id="photo_profile">
                                    <img src="<?php echo image($data->armada_photo); ?>" style="width: 100%;">
                                </a>
                                <input type="file" name="userfile" id="photo_upload" style="display: none;" />
                                <input type="checkbox" name ="delete_photo" /> Hapus Foto
                            </div>
                        </div>

                        <?php
                        echo $this->form->text(
                                [
                                    'name' => 'armada_status',
                                    'label' => 'Status',
                                    'required' => true,
                                    'type' => 'radio',
                                    'class' => '',
                                    'value' => ['1' => 'Aktif', '2' => 'Tidak Aktif'],
                                    'checked' => [$data->armada_status]
                                ]
                        );
                        ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url($path); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <a type="button" class="btn-info btn btn-save" data-toggle="modal" data-target="#modal-save-alert">
                            <i class="fa fa-save"></i>
                            Simpan
                        </a>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">

    /** Select 2 */
    $(".select2").find('select').select2();

    $('.btn-save').click(function () {

        var armada_name = $('.form-edit').find('input[name="armada_name"]').val();

        if (armada_name == '') {

            /** Set message */
            var text = "Nama armada harus diisi";

        } else {

            /** Set message */
            var text = "Apakah Anda akan menyimpan nama armada <strong>" + armada_name + "</strong>?";

        }

        /** Set modal */
        $('#modal-save-alert').find('.modal-body p').html(text);

    });

    $('.alert-btn-save').click(function () {
        $('.form-edit').submit();
    });

    $("a#photo_profile").click(function () {
        $("input#photo_upload").trigger("click");
    });

    $("input#photo_upload").change(function () {

        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) {
            return;
        }

        if (/^image/.test(files[0].type)) {

            var reader = new FileReader();
            reader.readAsDataURL(files[0]);

            reader.onloadend = function () {

                $('a#photo_profile > img').remove();
                var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                $('a#photo_profile').append(html);

            }

        }

    });

    function grep_volume() {

        var width = $('input[name="armada_v_width"]').val();
        var long = $('input[name="armada_v_long"]').val();
        var height = $('input[name="armada_v_height"]').val();
        var volume = width * long * height;
        $('input[name="armada_v_m3"]').val(volume);

    }

    $('.grep-volume').on('keyup', function () {
        grep_volume();
    });

    $('input[name="armada_reg_date"]').datepicker({ format: 'dd/mm/yyyy' });
    $('input[name="armada_kir_number"]').datepicker({ format: 'dd/mm/yyyy' });
    $('input[name="armada_stnk_number"]').datepicker({ format: 'dd/mm/yyyy' });

</script>