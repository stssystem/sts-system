<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <div class="row">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="row" >
                                    <div class="col-md-12" style="height: 100%;">
                                        <a href="#" class="thumbnail" id="photo_profile">
                                            <img src="<?php echo image($data->armada_photo); ?>" style="width: 100%;">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <table class="table table-striped">
                                    <tr>
                                        <th width="20%" >Nama Armada</th>
                                        <td width="20%" >
                                            <?php echo $data->armada_name; ?> 
                                            &nbsp;&nbsp;
                                            <?php echo user_status($data->armada_status); ?>
                                        </td>
                                        <th width="20%" >Supir</th>
                                        <td width="40%" >
                                            <?php echo $data->userprofile_fullname; ?>
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-striped">
                                    <tr>
                                        <th colspan="4" class="bg-gray">
                                            <i class="fa fa-truck"></i>
                                            Dimensi
                                        </th>
                                    </tr>
                                    <tr>
                                        <th width="20%">Lebar</th>
                                        <td width="20%"><?php echo $data->armada_v_width; ?></td>
                                        <th width="20%">Panjang</th>
                                        <td width="40%"><?php echo $data->armada_v_long; ?></td>
                                    </tr>
                                    <tr>
                                        <th width="20%" >Tinggi</th>
                                        <td width="20%" ><?php echo $data->armada_v_height; ?></td>
                                        <th width="20%" >Volume</th>
                                        <td width="40%" ><?php echo $data->armada_v_m3; ?></td>
                                    </tr>
                                    <tr>
                                        <th width="20%">Tonase</th>
                                        <td width="20%"><?php echo $data->armada_tonase; ?></td>
                                        <td colspan="2"></td>
                                    </tr>
                                </table>

                                <table class="table table-striped">
                                    <tr>
                                        <th colspan="4" class="bg-gray">
                                            <i class="fa fa-pencil-square"></i>
                                            Registrasi
                                        </th>
                                    </tr>
                                    <tr>
                                        <th width="20%" >Plat Nomor</th>
                                        <td width="20%" ><?php echo $data->armada_license_plate; ?></td>
                                        <th width="20%" >Tanggal Registrasi</th>
                                        <td width="40%" ><?php echo mdate('%d %M %Y', $data->armada_reg_date); ?></td>
                                    </tr>
                                    <tr>
                                        <th width="20%" >Nomor KIR</th>
                                        <td width="20%" ><?php echo $data->armada_kir_number; ?></td>
                                        <th width="20%" >Nomor STNK</th>
                                        <td width="40%" ><?php echo $data->armada_stnk_number; ?></td>
                                    </tr>
                                </table>

                                <table class="table table-striped">
                                    <tr>
                                        <th colspan="4" class="bg-gray">
                                            <i class="fa fa-desktop"></i>
                                            Device
                                        </th>
                                    </tr>
                                    <tr>
                                        <th width="20%" >Track Car Device ID</th>
                                        <td width="20%" ><?php echo $data->traccar_device_id; ?></td>
                                        <th width="20%" >Kode Armada</th>
                                        <td width="40%" ><?php echo $data->armada_custom_code; ?></td>
                                    </tr>
                                </table>

                            </div>
                        </div>

                        <div class="form-group">
                            <?php if (is_access(12)): ?>
                                <div class="btn-group">
                                    <a href="<?php echo site_url($path); ?>"  class="btn-info btn" >
                                        <i class="fa fa-arrow-left"></i>
                                        Kembali
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

    $('.btn-save').click(function () {

        var user_type_name = $('.form-edit').find('input[name="user_type_name"]').val();

        if (user_type_name == '') {

            /** Set message */
            var text = "User Tipe harus diisi";

        } else {

            /** Set message */
            var text = "Apakah Anda akan memperbarui data profil?";

        }

        /** Set modal */
        $('#modal-save-alert').find('.modal-body p').html(text);

    });

    $('.alert-btn-save').click(function () {
        $('.form-edit').submit();
    });

    /** Icheck */
//    $('input[name="delete_photo"]').iCheck({
//        checkboxClass: 'icheckbox_square-blue',
//        increaseArea: '20%' // optional
//    });


</script>