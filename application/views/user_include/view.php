<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4>Edit User Profil</h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open_multipart($path . '/update', ['class' => 'form-edit']); ?>

                <input type="hidden" name ="user_id" value="<?php echo $data->user_id ?>" />
                <input type="hidden" name ="userprofile_id" value="<?php echo $data->userprofile_id ?>" />

                <div class="row">
                    <div class="col-md-8">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="row" >
                                    <div class="col-md-12" style="height: 100%;">
                                        <a href="#" class="thumbnail" id="photo_profile">
                                            <img src="<?php echo image_profile(); ?>" style="width: 100%;">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <table class="table table-striped">
                                    <tr>
                                        <th width="30%" >Nama Lengkap</th>
                                        <td width="70%" ><?php echo $data->userprofile_fullname; ?></td>
                                    </tr>
                                    <tr>
                                        <th width="30%" >Email</th>
                                        <td width="70%" ><?php echo $data->user_email; ?></td>
                                    </tr>
                                    <tr>
                                        <th width="30%" >Username</th>
                                        <td width="70%" ><?php echo $data->username; ?></td>
                                    </tr>
                                    <tr>
                                        <th width="30%" >Alamat</th>
                                        <td width="70%" ><?php echo $data->userprofile_address; ?></td>
                                    </tr>
                                    <tr>
                                        <th width="30%" >Tipe User</th>
                                        <td width="70%" ><?php echo $data->user_type_name; ?></td>
                                    </tr>
                                     <tr>
                                        <th width="30%" >Status</th>
                                        <td width="70%" ><?php echo user_status($data->user_status); ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="btn-group">
                                <a href="<?php echo site_url($path); ?>"  class="btn-info btn" >
                                    <i class="fa fa-arrow-left"></i>
                                    Kembali
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

    $('.btn-save').click(function () {

        var user_type_name = $('.form-edit').find('input[name="user_type_name"]').val();

        if (user_type_name == '') {

            /** Set message */
            var text = "User Tipe harus diisi";

        } else {

            /** Set message */
            var text = "Apakah Anda akan memperbarui data profil?";

        }

        /** Set modal */
        $('#modal-save-alert').find('.modal-body p').html(text);

    });

    $('.alert-btn-save').click(function () {
        $('.form-edit').submit();
    });

    /** Icheck */
//    $('input[name="delete_photo"]').iCheck({
//        checkboxClass: 'icheckbox_square-blue',
//        increaseArea: '20%' // optional
//    });


</script>