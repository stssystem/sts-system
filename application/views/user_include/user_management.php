<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8">
                        <h4><?php echo $title; ?></h4>
                    </div>
                    <div class="col-md-4">
                        <a  class="btn btn-success pull-right" href="<?php echo site_url('users/users/add') ?>" ><i class="fa fa-plus"></i> Tambah User</a>        
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive ">
                <table class="table data-tables">
                    <thead>
                        <tr>
                            <th width="5%" >ID</th>
                            <th width="20%" >Nama</th>
                            <th width="15%" >Username</th>
                            <th width="15%" >Cabang</th>
                            <th width="10%" >Status</th>
                            <th width="10%" >Terakhir Login</th>
                            <th width="10%" >Dibuat</th>
                            <th width="15%" >Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>


<script type="text/javascript" >

    $('.data-tables').DataTable({
        serverSide: true,
        ordering: true,
        order: [
            [
                <?php dtorder_column('dt_users', 0); ?>, 
                "<?php dtorder_mode('dt_users', 'asc'); ?>"
            ]
        ],
        displayStart : <?php dtarray('dt_users','start', 0); ?>,
        pageLength : <?php dtarray('dt_users', 'lenght', 10); ?>,
        search: {
           search: "<?php dtarray('dt_users', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>',
            type: 'POST'
        },
        columns: [
            {data: 'user_id'},
            {data: 'userprofile_fullname'},
            {data: 'username'},
            {data: 'branch_name'},
            {data: 'user_status'},
            {data: 'last_login'},
            {data: 'created_at'},
            {data: 'action'}
        ], 
        columnDefs: [
            { targets: [ 7 ], orderable: false},
        ],
    }
    );

    /** Data tables set up */
    // $('.data-tables').DataTable({
    //     ajax: {
    //         url: '<?php echo site_url($path) ?>',
    //         dataSrc: ''
    //     },
    //     columns: [
    //         {data: 'user_id'},
    //         {data: 'userprofile_fullname'},
    //         {data: 'username'},
    //         {data: 'branch_name'},
    //         {data: 'user_status'},
    //         {data: 'last_login'},
    //         {data: 'created_at'},
    //         {data: 'action'}
    //     ]
    // });

    /** Delete */
    $('body').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus pengguna <strong>" + data['userprofile_fullname'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);

        /** Set url */
        var url = "<?php echo site_url('users/users/destroy'); ?>/" + data['user_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });

</script>