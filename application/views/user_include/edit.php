<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open('users/users/update', ['class' => 'form-edit']); ?>

                <input class="form-control" type="hidden" name="user_id"  value="<?php echo $user->user_id; ?>"/>

                <?php
                echo $this->form->text(
                        [
                            'name' => 'userprofile_fullname',
                            'label' => 'Nama Lengkap',
                            'max' => 100,
                            'required' => true,
                            'value' => $user->userprofile_fullname
                        ]
                );
                ?>

                <?php
                echo $this->form->text(
                        [
                            'name' => 'username',
                            'label' => 'Username',
                            'max' => 30,
                            'required' => true,
                            'value' => $user->username
                        ]
                );
                ?>

                <?php
                echo $this->form->text(
                        [
                            'name' => 'password',
                            'label' => 'Password Baru',
                            'max' => 50,
                            'required' => false,
                            'value' => '',
                            'type' => 'password'
                        ]
                );
                ?>

                <?php
                echo $this->form->text(
                        [
                            'name' => 'user_email',
                            'label' => 'Email',
                            'max' => 25,
                            'required' => true,
                            'value' => $user->user_email,
                            'type' => 'email'
                        ]
                );
                ?>

                <?php
                echo $this->form->select([
                    'name' => 'user_type_id',
                    'label' => 'Tipe User',
                    'value' => $user_type_id,
                    'keys' => 'user_type_id',
                    'values' => 'user_type_name',
                    'class' => 'select2',
                    'selected' => $user->user_type_id
                ]);
                ?>

                <?php
                echo $this->form->text(
                        [
                            'name' => 'user_status',
                            'label' => 'Status',
                            'required' => true,
                            'type' => 'radio',
                            'class' => '',
                            'value' => ['1' => 'Aktif', '0' => 'Tidak Aktif'],
                            'checked' => [$user->user_status]
                        ]
                );
                ?>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url('users/users'); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <button type="submit" class="btn-info btn btn-save" >
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">

    $('.alert-btn-save').click(function () {
        $('.form-edit').submit();
    });
    
    $('select[name="user_type_id"]').select2();

</script>