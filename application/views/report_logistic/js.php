<script type="text/javascript">
	
	$('a.logistic_report_tab').on('shown.bs.tab', function () {
        var value = $(this).attr('data-value');
        $('.th-action').attr('style', 'width:75px;');
    });

    $(function() { 
        // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTab');
        if (lastTab) {
            $('[href="' + lastTab + '"]').tab('show');
        }
    }); 

    var column_1 = [
    {data: 'city_name'},
    {data: 'total_weight'},
    {data: 'total_size'},
    ];

    var column_2 = [
    {data: 'armada_id'},
    {data: 'armada_name'},
    {data: 'armada_status'},
    {data: 'start_name'},
    {data: 'stop_name'},
    {data: 'schedule'},
    {data: 'action'},
    ];

    var column_3 = [
    {data: 'branch_name'},
    {data: 'total'},
    ];

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

        var target = $(e.target).attr("href");
        target = target.replace("#", "");

        var status;
        var columns;

        switch (target) {

            case 'tab_cities':
            status  = 1;
            columns = column_1;
            break;

            case 'tab_armadas':
            status  = 2;
            columns = column_2;
            break;
            
            case 'tab_deliveries_1':
            status  = 3;
            columns = column_3;
            break;

            case 'tab_deliveries_2':
            status  = 4;
            columns = column_3;
            break;

            case 'tab_deliveries_3':
            status  = 5;
            columns = column_3;
            break;

            case 'tab_pickup_orders':
            status  = 6;
            columns = column_3;
            break;

        }

        data_tables(target, columns, status);

    });

    /** Datatable default setup */
    data_tables('tab_cities', column_1, 1);

    function data_tables(id_table, columns, status) {

        $('.' + id_table).dataTable().fnDestroy();

        $('.' + id_table).DataTable({
            ordering: true,
            ajax: {
                url: '<?php echo site_url($path) ?>/index/' + status,
                type: 'POST',
                dataSrc: '',
            },
            columns: columns,
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
     
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
     
                // Total over all pages
                total = api
                    .column( 1 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
     
                // Total over this page
                pageTotal = api
                    .column( 1, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
     
                // Update footer
                $( api.column( 1 ).footer() ).html(
                    pageTotal +' ( '+ total +' total)'
                );
            }
        });
    }

    $('input[name="date_start_filter"]').datepicker({ format: 'dd/mm/yyyy' });
    $('input[name="date_end_filter"]').datepicker({ format: 'dd/mm/yyyy' });

    $(".select2").select2();

</script>