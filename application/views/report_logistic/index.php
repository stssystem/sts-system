<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a class="logistic_report_tab" href="#tab_cities" data-value="1" data-toggle="tab" aria-expanded="false"> Laporan Muatan Per Kota</a>
                </li>
                <li class="">
                    <a class="logistic_report_tab" href="#tab_armadas" data-value="3" data-toggle="tab" aria-expanded="false"> Laporan Muatan Per Armada</a>
                </li>
                <li class="">
                    <a class="logistic_report_tab" href="#tab_deliveries" data-value="1" data-toggle="tab" aria-expanded="false"> Laporan Muatan Pengantaran</a>
                </li>
                <li class="">
                    <a class="logistic_report_tab" href="#tab_pickup_orders" data-value="3" data-toggle="tab" aria-expanded="false"> Laporan Muatan Pick Up</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_cities">
                    <?php $this->load->view('report_logistic/tab_cities'); ?>
                </div>
                <div class="tab-pane" id="tab_armadas">
                    <?php $this->load->view('report_logistic/tab_armadas'); ?>
                </div>
                <div class="tab-pane" id="tab_deliveries">
                    <?php $this->load->view('report_logistic/tab_deliveries'); ?>
                </div>
                <div class="tab-pane" id="tab_pickup_orders">
                    <?php $this->load->view('report_logistic/tab_pickup_orders'); ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php $this->load->view('footer'); ?>


<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<?php $this->load->view('report_logistic/js'); ?>
