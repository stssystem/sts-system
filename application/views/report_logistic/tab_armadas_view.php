<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
             <div class="box-header" >
            <div class="row">
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <tr>
                                <th width="30%">Kode Armada</th>
                                <th width="5%" >:</th>
                                <td width="65%" ><?php echo (!empty($data_armadas->armada_id)) ? $data_armadas->armada_id :  '<span class="label label-danger">Armada Tidak Terdaftar</span>'?></td>
                            </tr>
                            <tr>
                                <th>Nama Armada</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_armadas->armada_name)) ? $data_armadas->armada_name :  '<span class="label label-danger">Armada Tidak Terdaftar</span>'?></td>
                            </tr>
                            <tr>
                                <th>Lebar Armada</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_armadas->armada_v_width)) ? $data_armadas->armada_v_width :  '<span class="label label-danger">Data Tidak Tersedia</span>'?></td>
                            </tr>
                            <tr>
                                <th>Panjang Armada</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_armadas->armada_v_long)) ? $data_armadas->armada_v_long :  '<span class="label label-danger">Data Tidak Tersedia</span>'?></td>
                            </tr>
                            <tr>
                                <th>Tinggi Armada</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_armadas->armada_v_height)) ? $data_armadas->armada_v_height :  '<span class="label label-danger">Data Tidak Tersedia</span>'?></td>
                            </tr>
                            <tr>
                                <th>Plat Armada</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_armadas->armada_license_plate)) ? $data_armadas->armada_license_plate :  '<span class="label label-danger">Data Tidak Tersedia</span>'?></td>
                            </tr>
                            <tr>
                                <th>Nama Driver</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_armadas->userprofile_fullname)) ? $data_armadas->userprofile_fullname :  '<span class="label label-danger">Data Tidak Tersedia</span>'?></td>
                            </tr>
                            <tr>
                                <th>Status Armada</th>
                                <th>:</th>
                                <td>
                                    <?php if (!empty($data_armadas->armada_status)) {
                                        echo ($data_armadas->armada_status) ? '<span class="label label-success">Aktif</span>' : '<span class="label label-danger">Tidak Aktif</span>' ;
                                    } else {
                                        '<span class="label label-danger">Armada Tidak Terdaftar</span>';
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- <div class="col-md-6">
                        <table class="table table-striped">
                            <tr>
                                <th width="30%">Total Transaksi</th>
                                <th width="5%" >:</th>
                                <td width="65%" ><?php echo (!empty($value_branches->total_order)) ? $value_branches->total_order :  '<span class="label label-danger">Transaksi Tidak Tersedia</span>'?></td>
                            </tr>
                            <tr>
                                <th>Total Omset Armada</th>
                                <th>:</th>
                                <td><?php echo (!empty($value_branches->total_omset)) ? '<span class="label label-success">Rp. '.number_format($value_branches->total_omset).'</span>' : '<span class="label label-danger">Tidak Ada Omset Armada</span>' ; ?></td>
                            </tr>
                            <tr>
                                <th>Total Komisi Armada</th>
                                <th>:</th>
                                <td><?php echo (!empty($value_branches->total_omset)) ? '<span class="label label-success">Rp. '.number_format((20/100)*$value_branches->total_omset).'</span>' : '<span class="label label-danger">Tidak Ada Komisi Armada</span>' ; ?></td>
                            </tr>
                        </table>
                    </div> -->
                </div>


                <hr/>

            </div> 

            <?php $this->load->view('report_logistic/filter_search/search_armadas'); ?> 

            <div class="box-body table-responsive ">
            <?php echo get_alert(); ?>

                <table class="table table-gray table-hover logistic_armadas_view">
                    <thead>
                        <tr class="txt-sm">
                            <th width="20%">Order ID</th>
                            <th width="20%">Armada</th>
                            <th width="20%">Pengirim</th>
                            <th width="10%">Total Berat</th>
                            <th width="10%">Total Volume</th>
                            <th width="20%">Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if (!empty($order)) {
                            foreach ($order as $key => $value) {
                                ?>
                                <tr class="txt-sm">
                                    <th width="20%"><?php echo $value->order_id; ?></th>
                                    <th width="20%"><?php echo $value->branch_name_origin; ?></th>
                                    <th width="20%"><?php echo $value->customer_name; ?></th>
                                    <th width="10%"><?php echo round($value->total_weight, 2); ?></th>
                                    <th width="10%"><?php echo round($value->total_size/1000000, 2); ?></th>
                                    <th width="20%">
                                        <a href="<?php echo base_url('orders/order/view/'.$value->order_id)?>" class="btn btn-info btn-sm"><i class="fa fa-search"></i></a>
                                    </th>
                                </tr>
                                <?php
                            }
                        } else {
                        }

                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">
    $('input[name="date_start"]').datepicker({ format: 'dd/mm/yyyy' });
    $('input[name="date_end"]').datepicker({ format: 'dd/mm/yyyy' });
</script>





