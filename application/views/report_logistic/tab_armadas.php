<div class="row">
    <div class="col-sm-12 table-responsive ">

        <?php echo get_alert(); ?>

        <table class="table table-bordered table-gray table-hover tab_armadas">
            <thead>
                <tr class="txt-sm">
                    <th width="5%">ID</th>
                    <th width="20%">Nama Kendaraan/Armada</th>
                    <th width="10%">Status Kendaraan</th>
                    <th width="20%">Asal</th>
                    <th width="20%">Tujuan</th>
                    <th width="20%">Jadwal</th>
                    <th width="5%">Detail</th>
                </tr>
            </thead>
        </table>

    </div>
</div>

