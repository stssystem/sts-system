
<div class="row">
  <div class="col-sm-12">

    <?php echo get_alert(); ?>

    <div class="nav-tabs-custom">

      <ul class="nav nav-tabs">
        <li class=""><a class="comission_tab" href="#tab_deliveries_1" data-value="1" data-toggle="tab" aria-expanded="false"> Paket Dalam Proses Pengantaran</a></li>
        <li class=""><a class="comission_tab" href="#tab_deliveries_2" data-value="1" data-toggle="tab" aria-expanded="false"> Paket Diterima</a></li>
        <li class=""><a class="comission_tab" href="#tab_deliveries_3" data-value="2" data-toggle="tab" aria-expanded="false"> Paket Kembali ke Agen</a></li>
      </ul>

      <div class="tab-content">

        <!-- tab 1 - paket asal -->
        <div class="tab-pane table-responsive " id="tab_deliveries_1">
         <table class="table table-bordered table-gray table-hover tab_deliveries_1">
            <thead>
                <tr class="txt-sm">
                    <th width="20%">Nama Cabang</th>
                    <th width="20%">Total Paket</th>
                </tr>
            </thead>
            <!-- <tfoot>
                <tr>
                    <td>Total</td>
                    <td></td>
                </tr>
            </tfoot> -->
        </table>

        </div>

        <!-- tab 2 - paket naik -->
        <div class="tab-pane table-responsive" id="tab_deliveries_2">
          <table class="table table-bordered table-gray table-hover tab_deliveries_2">
            <thead>
              <tr class="txt-sm">
                <th width="20%">Nama Cabang</th>
                <th width="20%">Total Paket</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>

        <!-- tab 3 - paket turun  -->
        <div class="tab-pane table-responsive" id="tab_deliveries_3">
         <table class="table table-bordered table-gray table-hover tab_deliveries_3">
           <thead>
             <tr class="txt-sm">
                <th width="20%">Nama Cabang</th>
                <th width="20%">Total Paket</th>
             </tr>
           </thead>
           <tbody>

           </tbody>
         </table>
       </div>

    </div>
    <!-- /.tab-content -->
  </div>

</div>
</div>

