<?php echo form_open($path . '/filter/' . $asal); ?>


<div class="row">

    <?php
        echo $this->form->text(
        [
        'name' => 'date_start',
        'label' => 'Tanggal Mulai',
        'max' => 100,
        'type' => 'text',
        'class' => 'col-md-3',
        'display_default' => false,
        'value' => isset($this->session->userdata('report_pickup_orders')['date_start']) && $this->session->userdata('report_pickup_orders')['date_start'] != '' ? $this->session->userdata('report_pickup_orders')['date_start'] : ''
        ]
        );
    ?>

    <?php
        echo $this->form->text(
        [
        'name' => 'date_end',
        'label' => 'Tanggal Akhir',
        'max' => 100,
        'type' => 'text',
        'class' => 'col-md-3',
        'display_default' => false,
        'value' => isset($this->session->userdata('report_pickup_orders')['date_end']) && $this->session->userdata('report_pickup_orders')['date_end'] != '' ? $this->session->userdata('report_pickup_orders')['date_end'] : ''
        ]
        );
    ?>

</div>

<div class="row">
    <div class="col-md-6">
        
    <div class="col-md-1">
    <a href="<?php echo site_url('report/report_logistic'); ?>" class="btn btn-info">
            <i class="fa fa-arrow-left"></i> Kembali
        </a>
    </div>

    <div class="col-md-5 pull-right">
        <div class="btn-group pull-right" >
            <button type="submit" class="btn btn-info">
                <i class="fa fa-filter"></i> Filter
            </button>
            <a href="<?php echo site_url('report/report_logistic_pickup_orders/clear/'.$asal); ?>" class="btn btn-warning">
                <i class="fa fa-refresh"></i> Reset
            </a>
        </div>
    </div>

    </div>
</div>
<?php echo form_close(); ?>