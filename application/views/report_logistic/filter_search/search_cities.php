<?php echo form_open($path . '/filter/' . $city_id); ?>

<div class="row">

    <?php
        echo $this->form->text(
        [
        'name' => 'date_start',
        'label' => 'Tanggal Mulai',
        'max' => 100,
        'type' => 'text',
        'class' => 'col-md-3',
        'display_default' => false,
        'value' => isset($this->session->userdata('cities')['date_start']) && $this->session->userdata('cities')['date_start'] != '' ? $this->session->userdata('cities')['date_start'] : ''
        ]
        );
    ?>

    <?php
        echo $this->form->text(
        [
        'name' => 'date_end',
        'label' => 'Tanggal Akhir',
        'max' => 100,
        'type' => 'text',
        'class' => 'col-md-3',
        'display_default' => false,
        'value' => isset($this->session->userdata('cities')['date_end']) && $this->session->userdata('cities')['date_end'] != '' ? $this->session->userdata('cities')['date_end'] : ''
        ]
        );
    ?>

    <?php
        echo $this->form->text(
        [
        'name' => 'total_weight',
        'label' => 'Jumlah Berat (kg)',
        'max' => 100000,
        'type' => 'number',
        'class' => 'col-md-3',
        'display_default' => false,
        'value' => isset($this->session->userdata('cities')['total_weiht']) && $this->session->userdata('cities')['total_weight'] != '' ? $this->session->userdata('cities')['total_weight'] : ''
        ]
        );
    ?>

    <!-- <?php
        echo $this->form->text(
        [
        'name' => 'total_size',
        'label' => 'Jumlah Volume ( m&sup3 )',
        'max' => 100,
        'type' => 'number',
        'class' => 'col-md-3',
        'display_default' => false,
        'value' => isset($this->session->userdata('cities')['total_size']) && $this->session->userdata('cities')['total_size'] != '' ? $this->session->userdata('cities')['total_size'] : ''
        ]
        );
    ?> -->

</div>

<div class="row">
        <div class="col-md-9">
        <div class="col-md-2">
        <a href="<?php echo site_url('report/report_logistic'); ?>" class="btn btn-info">
                <i class="fa fa-arrow-left"></i> Kembali
            </a>
        </div>

        <div class="col-md-7 pull-right">
            <div class="btn-group pull-right" >
                <button type="submit" class="btn btn-info">
                    <i class="fa fa-filter"></i> Filter
                </button>
                <a href="<?php echo site_url('report/report_logistic_cities/clear/'.$city_id); ?>" class="btn btn-warning">
                    <i class="fa fa-refresh"></i> Reset
                </a>
            </div>
        </div>
        </div>
</div>
<?php echo form_close(); ?>