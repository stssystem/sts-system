<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header" >

                <div class="row">
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <tr>
                                <th width="30%">Nama Kota</th>
                                <th width="5%" >:</th>
                                <td width="65%" ><?php echo (!empty($value_cities->city_name)) ? $value_cities->city_name :  '<span class="label label-danger">Cabang Tidak Terdaftar</span>'?></td>
                            </tr>
                            <tr>
                                <th width="30%">Total Order</th>
                                <th width="5%" >:</th>
                                <td width="65%" ><?php echo (!empty($value_cities->total_order)) ? $value_cities->total_order :  '<span class="label label-danger">Transaksi Tidak Tersedia</span>'?></td>
                            </tr>
                            <tr>
                                <th width="30%">Total Berat</th>
                                <th width="5%" >:</th>
                                <td width="65%" ><?php echo (!empty($value_cities->total_weight)) ? '<span class="label label-success">'.round($value_cities->total_weight).' kg</span>' :  '<span class="label label-danger">Transaksi Tidak Tersedia</span>'?></td>
                            </tr>
                            <tr>
                                <th width="30%">Total Volume</th>
                                <th width="5%" >:</th>
                                <td width="65%" ><?php echo (!empty($value_cities->total_size)) ? '<span class="label label-success">'.round($value_cities->total_size / 1000000, 2).' m &sup3</span>' :  '<span class="label label-danger">Transaksi Tidak Tersedia</span>'?></td>
                            </tr>
                        </table>
                    </div>
                </div>


                <hr/>

            <?php $this->load->view('report_logistic/filter_search/search_cities'); ?> 

            </div>
            <div class="box-body table-responsive">
                <?php echo get_alert(); ?>

                <table class="table data-tables table-gray table-hover txt-sm">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="20%">Tanggal Order</th>
                            <th width="20%">Order ID</th>
                            <th width="20%">Cabang</th>
                            <th width="20%">Pengirim</th>
                            <th width="10%">Total Berat</th>
                            <th width="10%">Total Volume</th>
                            <th width="20%">Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    /** Data tables set up */
    $('.data-tables').DataTable({
        serverSide: true,
        ajax: {
            url: '<?php echo site_url($path . "/index/" . $city_id); ?>',
            type: 'POST',
            dataSrc: 'data',
        },
        columns: [
        {data: 'no'},
        {data: 'order_date'},
        {data: 'order_id'},
        {data: 'branch_origin'},
        {data: 'customer_name'},
        {data: 'total_weight'},
        {data: 'total_size'},
        {data: 'action'}
        ], 
        columnDefs: [
        { targets: [ 0 ], orderable: false},    
        { targets: [ 7 ], orderable: false},
        ],
    }
    );

    $('input[name="date_start"]').datepicker({ format: 'dd/mm/yyyy' });
    $('input[name="date_end"]').datepicker({ format: 'dd/mm/yyyy' });

    // $(".select2").select2();

</script>
