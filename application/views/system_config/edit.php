

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open_multipart($path . '/update', ['class' => 'form-edit']); ?>

                <div class="row">
                    <div class="col-md-12">

                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#company" aria-controls="home" role="tab" data-toggle="tab">Info Perusahaan</a></li>
                                <li role="presentation"><a href="#trackcar" aria-controls="profile" role="tab" data-toggle="tab">Track Car</a></li>
                                <li role="presentation"><a href="#website" aria-controls="profile" role="tab" data-toggle="tab">Website</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">

                                <br/>

                                <div role="tabpanel" class="tab-pane active" id="company">

                                    <div class="row">
                                        <div class="col-md-9">

                                            <div class="row">

                                                <?php
                                                echo $this->form->text(
                                                        [
                                                            'name' => 'company_name',
                                                            'label' => 'Nama Perusahaan',
                                                            'max' => 100,
                                                            'required' => true,
                                                            'class' => 'col-md-6',
                                                            'value' => $data->company_name
                                                        ]
                                                );
                                                ?>

                                                <?php
                                                echo $this->form->text(
                                                        [
                                                            'name' => 'online_url',
                                                            'label' => 'Online URL',
                                                            'max' => 150,
                                                            'required' => true,
                                                            'class' => 'col-md-6',
                                                            'value' => $data->online_url
                                                        ]
                                                );
                                                ?>

                                            </div>


                                            <?php
                                            echo $this->form->textarea(
                                                    [
                                                        'name' => 'company_address',
                                                        'label' => 'Alamat',
                                                        'required' => true,
                                                        'class' => '',
                                                        'rows' => 4,
                                                        'value' => $data->company_address
                                                    ]
                                            );
                                            ?>

                                            <?php
                                            echo $this->form->text(
                                                    [
                                                        'name' => 'license_key',
                                                        'label' => 'Nomor Lisensi',
                                                        'max' => 100,
                                                        'required' => true,
                                                        'value' => $data->license_key
                                                    ]
                                            );
                                            ?>

                                        </div>

                                        <div class="col-md-3">
                                            <div class="row">
                                                <div class="col-md-12" style="height: 100%;">
                                                    <label>Logo Perusahaan</label>
                                                    <a href="#" class="thumbnail" id="photo_profile">
                                                        <img src="<?php echo image($data->main_logo); ?>" style="width: 100%;">
                                                    </a>
                                                    <input type="file" name="userfile" id="photo_upload" style="display: none;" />
                                                    <input type="checkbox" name ="delete_photo" /> Hapus Foto
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane" id="trackcar">

                                    <?php
                                    echo $this->form->text(
                                            [
                                                'name' => 'traccar_url',
                                                'label' => 'Alamat URL',
                                                'max' => 150,
                                                'required' => true,
                                                'value' => $data->traccar_url
                                            ]
                                    );

                                    echo $this->form->text(
                                            [
                                                'name' => 'traccar_db',
                                                'label' => 'Database',
                                                'max' => 100,
                                                'required' => true,
                                                'value' => $data->traccar_db
                                            ]
                                    );
                                    ?>


                                    <div class="row">

                                        <?php
                                        echo $this->form->text(
                                                [
                                                    'name' => 'traccar_db_username',
                                                    'label' => 'Username',
                                                    'max' => 100,
                                                    'required' => true,
                                                    'class' => 'col-md-6',
                                                    'value' => $data->traccar_db_username
                                                ]
                                        );

                                        echo $this->form->text(
                                                [
                                                    'name' => 'traccar_db_password',
                                                    'label' => 'Password',
                                                    'max' => 100,
                                                    'required' => true,
                                                    'class' => 'col-md-6',
                                                    'type' => 'password',
                                                    'value' => $data->traccar_db_password
                                                ]
                                        );
                                        ?>

                                    </div>

                                </div>

                                <div role="tabpanel" class="tab-pane" id="website">
                                    <?php
                                    echo $this->form->text(
                                            [
                                                'name' => 'web_url',
                                                'label' => 'Alamat URL',
                                                'max' => 150,
                                                'required' => true,
                                                'value' => $data->web_url,
                                                'class' => '',
                                                'type' => 'text'
                                            ]
                                    );

                                    echo $this->form->text(
                                            [
                                                'name' => 'web_db',
                                                'label' => 'Database',
                                                'max' => 100,
                                                'required' => true,
                                                'value' => $data->web_db,
                                                'class' => '',
                                                'type' => 'text'
                                            ]
                                    );
                                    ?>

                                    <div class="row">

                                        <?php
                                        echo $this->form->text(
                                                [
                                                    'name' => 'web_db_username',
                                                    'label' => 'Username',
                                                    'max' => 100,
                                                    'required' => true,
                                                    'class' => 'col-md-6',
                                                    'value' => $data->web_db_username
                                                ]
                                        );

                                        echo $this->form->text(
                                                [
                                                    'name' => 'web_db_password',
                                                    'label' => 'Password',
                                                    'max' => 100,
                                                    'required' => true,
                                                    'class' => 'col-md-6',
                                                    'type' => 'password',
                                                    'value' => $data->web_db_password
                                                ]
                                        );
                                        ?>

                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="btn-group">
                                <a type="button" class="btn-info btn btn-save" data-toggle="modal" data-target="#modal-save-alert">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

    $('.btn-save').click(function () {

        var user_type_name = $('.form-edit').find('input[name="user_type_name"]').val();

        if (user_type_name == '') {

            /** Set message */
            var text = "User Tipe harus diisi";

        } else {

            /** Set message */
            var text = "Apakah Anda akan memperbarui konfigurasi sistem?";

        }

        /** Set modal */
        $('#modal-save-alert').find('.modal-body p').html(text);

    });

    $('.alert-btn-save').click(function () {
        $('.form-edit').submit();
    });

    $("a#photo_profile").click(function () {
        $("input#photo_upload").trigger("click");
    });

    $("input#photo_upload").change(function () {

        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) {
            return;
        }

        if (/^image/.test(files[0].type)) {

            var reader = new FileReader();
            reader.readAsDataURL(files[0]);

            reader.onloadend = function () {

                $('a#photo_profile > img').remove();
                var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                $('a#photo_profile').append(html);

            }

        }

    });

    /** Icheck */
//    $('input[name="delete_photo"]').iCheck({
//        checkboxClass: 'icheckbox_square-blue',
//        increaseArea: '20%' // optional
//    });


</script>