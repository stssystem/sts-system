<style type="text/css">
    .footer-background{
        background-color: #bdbdbd;
    }
</style>

<div class="tab-content">

    <div class="tab-pane active table-responsive" id="loading">        
        <div class="row">
            <div class="col-md-12">
                <table class="table data-tables loading">
                    <thead>
                        <tr class="txt-sm">
                            <th width="15%" >No Resi</th>
                            <th width="18%" >Tanggal Resi</th>
                            <th width="18%" >Tanggal Loading</th>
                            <th width="15%" >No. Kendaraan</th>
                            <th width="8%">Koli</th>
                            <th width="8%">Total Berat <br/>( Kg )</th>
                            <th width="8%">Volume <br/>( M<sup>3</sup> )</th>
                            <th width="5%" >Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="txt-sm">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="tab-pane table-responsive" id="unloading">

        <div class="row">
            <div class="col-md-12">
                <table class="table data-tables unloading">
                    <thead>
                        <tr class="txt-sm">
                            <th width="15%" >No Resi</th>
                            <th width="18%" >Tanggal Resi</th>
                            <th width="18%" >Tanggal Unloading</th>
                            <th width="15%" >No. Kendaraan</th>
                            <th width="8%">Koli</th>
                            <th width="8%">Total Berat <br/>( Kg )</th>
                            <th width="8%">Volume <br/>( M<sup>3</sup> )</th>
                            <th width="5%" >Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="txt-sm">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="tab-pane table-responsive" id="transit">
        <div class="row">
            <div class="col-md-12">
                <table class="table data-tables transit">
                    <thead>
                        <tr class="txt-sm">
                            <th width="15%" >No Resi</th>
                            <th width="18%" >Tanggal Resi</th>
                            <th width="18%" >Tanggal Transit</th>
                            <th width="15%" >No. Kendaraan</th>
                            <th width="8%">Koli</th>
                            <th width="8%">Total Berat <br/>( Kg )</th>
                            <th width="8%">Volume <br/>( M<sup>3</sup> )</th>
                            <th width="5%" >Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="txt-sm">
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="tab-pane table-responsive" id="delivery">
        <div class="row">
            <div class="col-md-12">
                <table class="table data-tables delivery">
                    <thead>
                        <tr class="txt-sm">
                            <th width="15%" >No Resi</th>
                            <th width="18%" >Tanggal Resi</th>
                            <th width="18%" >Tanggal Antar</th>
                            <th width="15%" >No. Kendaraan</th>
                            <th width="8%">Koli</th>
                            <th width="8%">Total Berat <br/>( Kg )</th>
                            <th width="8%">Volume <br/>( M<sup>3</sup> )</th>
                            <th width="5%" >Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="txt-sm">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>