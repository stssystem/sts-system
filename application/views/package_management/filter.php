<?php echo form_open($path . '/filter'); ?>
<div class="col-sm-4">
    <div class="form-group">
        <label>Tanggal Awal</label>
        <input type="text" name="date_start_filter" class="form-control"
               value="<?php echo ($this->session->userdata('package-management-filter')['date_start']) ? $this->session->userdata('package-management-filter')['date_start'] : date('01/m/Y'); ?>"
               />
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <label>Tanggal Akhir</label>
        <input type="text" name="date_end_filter" class="form-control"
               value="<?php echo ($this->session->userdata('package-management-filter')['date_end']) ? $this->session->userdata('package-management-filter')['date_end'] : date('t/m/Y'); ?>"
               />
    </div>
</div>

<?php $disabled = 'disabled="disabled;"'; ?>
<?php if (profile()->user_type_id == 99 || profile()->branch_access != '' || profile()->regional_id != ''): ?>
    <?php $disabled = ''; ?>
<?php endif; ?>
<div class="col-sm-4">
    <div class="form-group">
        <label>Lihat Sebagai Cabang</label>

        <?php
        if (profile()->branch_access == '' && profile()->regional_id != '') {
            $cities = get_branches_list(null, profile()->regional_id);
        } else {
            $cities = get_branches_list(null);
        }
        ?>
        
        <select name="branches" class="form-control select2" <?php echo $disabled; ?> >
            <option value="0" >-- Pilih Cabang --</option>
            <?php if (!empty($cities)): ?>
                <?php foreach ($cities as $key => $item): ?>
                    <?php if (($this->session->userdata('package-management-filter')['branches'] && $this->session->userdata('package-management-filter')['branches'] == $item['branch_id']) || (profile()->branch_id != '' && profile()->branch_id == $item['branch_id'])): ?>
                        <option value="<?php echo $item['branch_id']; ?>" selected="selected"><?php echo $item['branch_name']; ?></option>
                    <?php else: ?>
                        <option value="<?php echo $item['branch_id']; ?>"><?php echo $item['branch_name']; ?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div>
</div>

<div class="col-sm-12">
    <div class="btn-group">
        <button id="filter" class="btn btn-filter btn-info"><i class="fa fa-filter"></i> Filter</button>
        <a href="<?php echo site_url($path . '/clear'); ?>" class="btn btn-warning"><i class="fa fa-eraser"></i> Reset</a>
    </div>
    <a href="<?php echo site_url($path . '/donwload'); ?>" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Download</a>
    <hr />
</div>
<?php echo form_close(); ?>

<div class="col-md-12">
    <?php get_alert(); ?>
</div>