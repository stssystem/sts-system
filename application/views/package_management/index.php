<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<style type="text/css">
    .txt-sm{        
    }

    .txt-mn{
        font-size: 10px;
    }
</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="box">
    <div class="box-header">

    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <?php $this->load->view('package_management/filter'); ?>
                </div>

                <div class="row">

                    <div class="col-sm-12">
                        <?php get_alert(); ?>
                    </div>

                    <div class="col-sm-12">

                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#loading" data-toggle="tab" aria-expanded="false">
                                        Paket Loading
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#unloading" data-toggle="tab" aria-expanded="false">
                                        Paket Unloading
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#transit" data-toggle="tab" aria-expanded="false">
                                        Paket Transit
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#delivery" data-toggle="tab" aria-expanded="false">
                                        Paket Diantar
                                    </a>
                                </li>
                            </ul>

                            <?php $this->load->view('package_management/tab-content'); ?>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
<!-- Numeral -->
<script src="<?php echo base_url(); ?>assets/plugins/numeral/numeral.min.js"></script>
<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<?php $this->load->view('package_management/index-js'); ?>