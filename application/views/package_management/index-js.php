<script type="text/javascript" >

    var column_default = [
        {data: 'resi_number'},
        {data: 'resi_date'},
        {data: 'do_date'},
        {data: 'armada_plate_number'},
        {data: 'koli'},
        {data: 'weight'},
        {data: 'volume'},
        {data: 'action'}
    ];

    // Set default tab
    if ($.session.get('tab-package-management') != '') {
        var href_ = '#' + $.session.get('tab-package-management');
        shown_tab(column_default);
        $('a[href="' + href_ + '"]').trigger('click');
    }

    /** New order list */
    function shown_tab(column_default) {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

            /** Variable initiation */
            var target = $(e.target).attr("href");           
            var status;
            var column = column_default;

            target = target.replace("#", "");

            $.session.set('tab-package-management', target);

            switch (target) {
                case 'loading':
                    status = 0;
                    column = column_default;
                    break;
                case 'unloading':
                    status = 1;
                    column = column_default;
                    break;
                case 'transit':
                    status = 2;
                    column = column_default;
                    break;
                case 'delivery':
                    status = 3;
                    column = column_default;
                    break;
            }

            data_tables(status, target, column);

        });
    }

    /** Datatable default setup */
    data_tables(0, 'loading', column_default);

    function data_tables(status, id_table, column) {
        
        var url = '<?php echo site_url($path) ?>/index/' + status;
      
        $('.' + id_table).dataTable().fnDestroy();

        $('.' + id_table).DataTable({
            serverSide: true,
            ordering: true,
            order: [
                [
                    <?php dtorder_column('dt_package_management', 0); ?>,
                    "<?php dtorder_mode('dt_package_management', 'desc'); ?>"
                ]
            ],
            displayStart: <?php dtarray('dt_package_management', 'start', 0); ?>,
            pageLength: <?php dtarray('dt_package_management', 'lenght', 10); ?>,
            search: {
                search: "<?php dtarray('dt_package_management', 'search', ''); ?>"
            },
            ajax: {
                url: url,
                type: 'POST',
            },
            columns: column
        });
    }

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus data order dengan nomor resi <strong>" + data['order_id'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);

        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['order_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });

    /** Close order */
    $('body').on('click', '.btn-closed-final', function () {
        var data_edit = $(this).parent().parent().find('input[name="edit-value"]').val();
        data_edit = JSON.parse(data_edit);
        /** Set url */
        var url = "<?php echo site_url($path . '/paid'); ?>/" + data_edit['order_id'];
        $('#closed_final').find('.modal-footer a.a-closed').attr('href', url);
    });

    /** Self take modal */
    $('body').on('click', '.btn-self-take', function () {
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);
        $('#modal-self-take').find('input[name="order_id"]').val(data['order_id']);
        $('#modal-self-take').modal('show');
    });

    /** Delivery modal */
    $('body').on('click', '.btn-delivery', function () {
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);
        $('#modal-delivery').find('input[name="order_id"]').val(data['order_id']);
        $('#modal-delivery').modal('show');
    });

    /** Package rollback modal */
    $('body').on('click', '.btn-package-rollback', function () {
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);
        $('#modal-package-rollback').find('input[name="order_id"]').val(data['order_id']);
        $('#modal-package-rollback').modal('show');
    });

    // Set Select2
    $('select[name="as_branch"]').select2();
    $('select[name="branches"]').select2();

    // Clear Session
    $('.btn-clear').click(function () {
        $.session.remove('tab-order-list');
    });
    
    // Date plugin
    $('input[name="date_start_filter"]').datepicker({format: 'dd/mm/yyyy'});
    $('input[name="date_end_filter"]').datepicker({format: 'dd/mm/yyyy'});

</script>