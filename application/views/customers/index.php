<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8">
                        <h4><?php echo $title; ?></h4>    
                    </div>
                    <div class="col-md-4">
                        <div class="box-tools pull-right">
                            <a href="<?php echo site_url($path . '/add') ?>" class="btn btn-success btn-flat">
                                <i class="fa fa-plus"></i> Tambah Data Pelanggan
                            </a>
                            <a href="<?php echo site_url($path . '/reset') ?>" class="btn btn-warning  btn-flat">
                                <i class="fa fa-refresh"></i> Reset
                            </a>
                        </div>
                    </div>
                </div>
                <br> <hr>
                <?php $this->load->view('customers/search_filter'); ?>
            </div>
            <div class="box-body table-responsive ">
                <?php get_alert(); ?>

                <table class="table data-tables">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th width="5%">ID Pelanggan</th>
                            <th width="20%">Nama</th>
                            <th width="15%">Kota</th>
                            <th width="25%">Alamat</th>
                            <th width="10%">Telepon</th>
                            <th width="15%">Nama PIC</th>
                            <th width="15%">Tanggal Lahir PIC</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>	
                </table>
            </div>
        </div>
    </div>
</div>



<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Easy Autocomplete -->
<script src="<?php echo base_url(); ?>assets/plugins/easy-autocomplete/jquery.easy-autocomplete.min.js"></script>

<?php $this->load->view('customers/index-js'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    /** Data tables set up */
    $('.data-tables').DataTable({
        serverSide: true,
        ordering: true,
        order: [
            [
                <?php dtorder_column('dt_customers', 0); ?>, 
                "<?php dtorder_mode('dt_customers', 'asc'); ?>"
            ]
        ],
        displayStart : <?php dtarray('dt_customers','start', 0); ?>,
        pageLength : <?php dtarray('dt_customers', 'lenght', 10); ?>,
        search: {
           search: "<?php dtarray('dt_customers', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>',
            type: 'POST'
        },
        columns: [
            {data: 'no'},    
            {data: 'customer_id'},
            {data: 'customer_name'},
            {data: 'customer_city'},
            {data: 'customer_address'},
            {data: 'customer_phone'},
            {data: 'pic_name'},
            {data: 'pic_birth_date'},
            {data: 'action'}
        ], 
        columnDefs: [
            { targets: [ 0 ], orderable: false},
            { targets: [ 8 ], orderable: false},
        ],
    }
    );

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);
        
        alert(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus pelanggan <strong>" + data['customer_name'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);
        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['customer_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    })

     // $(".select2").find('select').select2();



</script>