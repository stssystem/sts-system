    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/easy-autocomplete/easy-autocomplete.css">

    <?php $this->load->view('header'); ?>
    <?php $this->load->view('component/modal/alert-save'); ?>
    <div class="row">

        <div class="col-sm-12">
            <div class="box">

            <?php echo form_open($path . '/store', 'class="form-horizontal"'); ?>
            <!-- Province -->
            <input type="hidden" name="old_customer_province_id" value="" />
            <!-- Cities -->
            <input type="hidden" name="old_customer_city_id" value="" />
            <!-- Districts -->
            <input type="hidden" name="old_customer_districts_id" value="" />
            <!-- Villages  -->
            <input type="hidden" name="old_customer_village_id" value="" />

                <div class="box-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4><?php echo $title; ?></h4>
                        </div>
                        <div class="col-sm-6">
                            <?php $user_branch_id = (!empty($user_branch_id)) ? $user_branch_id : 'null' ; ?>
                            <?php $user_branch_name = (!empty($user_branch_name)) ? $user_branch_name : 'null' ; ?>
                            <div  class="form-group user_branch"> 
                                <label class="control-label col-sm-3" for="inputSuccess1">Cabang/Agen</label>
                                <div class="col-sm-3">
                                    <input class="form-control" type="text" name="user_branch_id" value="<?php echo $user_branch_id; ?>">
                                </div>
                                 <div class="col-sm-5">
                                    <input class="form-control" type="text" name="user_branch_name" value="<?php echo $user_branch_name; ?>">
                                </div>
                            </div>
                            <br>
                            <div  class="form-group select2 user_branch_new"> 
                                <label class="control-label col-sm-3" for="inputSuccess1">Cabang/Agen</label>
                                 <div class="col-sm-9">
                                    <select class="form-control" name="branch_created_id">
                                        <option value=" ">-- Pilih Cabang/Agen --</option>
                                        <?php 
                                        if (!empty($list_branches)) {
                                            foreach ($list_branches as $key => $value) {
                                                ?>
                                                <option value="<?php echo $value->branch_id; ?>">
                                                <?php echo $value->branch_code.' -'.$value->branch_name; ?>
                                                </option>
                                                <?php 
                                            }
                                        } 
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">

                    <div class="row">
                        <div class="col-sm-6">
                            
                            <h4 class="box-title">No. Referral</h4>
                            <div class="row">
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_referral',
                                        'label' => 'Bila Ada',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12',
                                        'attribute' => ['style' => 'border-radius:0px;'],
                                        'value'     => '',
                                        ]
                                    );
                                ?>
                            </div>

                            <h4 class="box-title">Kode Pelanggan</h4>
                            <div class="row">
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_code',
                                        'label' => 'Kode Pelanggan',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12'
                                        ]
                                    );
                                ?>
                            </div>

                            <h4 class="box-title">Info Pelanggan</h4>
                            <div class="row">

                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_name',
                                        'label' => 'Nama*',
                                        'max' => 100,
                                        'required' => true,
                                        'class' => 'col-sm-12'
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_phone',
                                        'label' => 'Nomor Telepon/HP',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->textarea_h(
                                        [
                                        'name' => 'customer_address',                                
                                        'label' => 'Alamat',                               
                                        'max' => 100,
                                        'required' => false,
                                        'value' => '',
                                        'rows' => 3,
                                        'class' => 'col-sm-12'
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'customer_province_id',
                                        'label'     => 'Provinsi',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => $provinces,
                                        'keys'      => 'province_id',
                                        'values'    => 'province_name',
                                        'class'     => 'select2 col-md-12 province customer_geobranch'
                                        ]
                                    );
                                ?>  

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'customer_city_id',
                                        'label' => 'Kota*',
                                        'max' => 100,
                                        'required' => true,
                                        'class' => 'select2 col-md-12 city select-price-params customer_geobranch',
                                        'value' => $cities,
                                        'keys' => 'city_id',
                                        'values' => 'city_name',
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'customer_districts_id',
                                        'label'     => 'Kecamatan',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => [],
                                        'keys'      => 'districts_id',
                                        'values'    => 'districts_name',
                                        'class'     => 'select2 col-md-12 district select-price-params customer_geobranch'
                                        ]
                                    );
                                ?>  

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'customer_village_id',
                                        'label'     => 'Kelurahan',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => [],
                                        'keys'      => 'village_id',
                                        'values'    => 'village_name',
                                        'class'     => 'select2 col-md-12 village select-price-params origin_geobranch'
                                        ]
                                    );
                                ?> 
                            </div>

                            <h4 class="box-title">Info Lain</h4>
                            <div class="row">

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'customer_id_type',
                                        'label' => 'Identitas',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'customer_id_type col-md-12',
                                        'value' => [
                                            ['id' => 1, 'item' => 'KTP'],
                                            ['id' => 2, 'item' => 'SIM'],
                                            ['id' => 3, 'item' => 'NPWP'],
                                            ['id' => 4, 'item' => 'Lainnya']
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item'
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_id_type_other',
                                        'label' => 'Identitas (Lain)',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_id_number',
                                        'label' => 'Nomor Identitas',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12',
                                        'value' => ''
                                        ]
                                    );
                                ?>      

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'customer_type',
                                        'label' => 'Tipe Customer',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => [
                                            ['id' => 1, 'item' => 'Personal'],
                                            ['id' => 2, 'item' => 'Perusahaan'],
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item'
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'customer_company_type',
                                        'label'     => 'Jenis Usaha',
                                        'max'       => 100,
                                        'required'  => false,
                                        'class'     => 'select2 customer_company_type col-md-12',
                                        'value'     => [
                                            ['id' => 1, 'item' => 'Manufaktur'],
                                            ['id' => 2, 'item' => 'Jasa'],
                                            ['id' => 3, 'item' => 'Ritel'],
                                            ['id' => 4, 'item' => 'Makanan'],
                                            ['id' => 5, 'item' => 'Minuman'],
                                            ['id' => 6, 'item' => 'Fashion'],
                                            ['id' => 7, 'item' => 'Konveksi'],
                                            ['id' => 8, 'item' => 'Hasil Tani'],
                                            ['id' => 9, 'item' => 'Mebel'],
                                            ['id' => 10, 'item' => 'Obat'],
                                            ['id' => 11, 'item' => 'Lainnya'],
                                        ],
                                        'keys'      => 'id',
                                        'values'    => 'item'
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name'      => 'customer_company_type_other',
                                        'label'     => 'Jenis Usaha Lainnya',
                                        'max'       => 100,
                                        'required'  => false,
                                        'class'     => 'col-md-12',
                                        'value'     => ''
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'customer_company_type_of_business',
                                        'label'     => 'Tipe Usaha',
                                        'max'       => 100,
                                        'required'  => false,
                                        'class'     => 'select2 col-md-12',
                                        'value'     => [
                                            ['id' => 1, 'item' => 'Personal'],
                                            ['id' => 2, 'item' => 'Perorangan'],
                                            ['id' => 3, 'item' => 'UD'],
                                            ['id' => 4, 'item' => 'CV'],
                                            ['id' => 5, 'item' => 'PT'],
                                            ['id' => 6, 'item' => 'Asing'],
                                        ]
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_company_email',
                                        'label' => 'Email',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name'      => 'customer_web_id',
                                        'label'     => 'ID Web',
                                        'max'       => 100,
                                        'required'  => false,
                                        'class'     => 'col-md-12',
                                        'value'     => ''
                                        ]
                                    );
                                ?>
                            </div>

                            <h4 class="box-title">Informasi Pengiriman</h4>
                            <div class="row">
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'customer_freq_sending',
                                        'label' => 'Frekuensi Pengiriman Paket',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'select2 col-md-12',
                                        'value' => [
                                        ['id' => 1, 'value' => 'Harian'],
                                        ['id' => 2, 'value' => 'MIngguan'],
                                        ['id' => 3, 'value' => 'Bulanan'],
                                        ],
                                        'keys' => 'id',
                                        'values' => 'value',
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_sending_quota',
                                        'label' => 'Jumlah Pengiriman Paket',
                                        'max' => 100,
                                        'required' => false,
                                        'value' => '',
                                        'class' => 'col-md-12'
                                        ]
                                    );
                                ?>
                            </div> 
                                 
                        </div>

                        <div class="col-sm-6">                          

                            <h4>Informasi PIC</h4>
                            <div class="row">
                                <div  class="form-group"> 
                                    <label class="control-label col-sm-3" for="inputSuccess1">Nama Kontak/Pemilik*</label>
                                    <div class="col-sm-4">
                                        <input 
                                        class="form-control" 
                                        type="text" 
                                        name="pic_first_name" 
                                        placeholder="Nama Depan" 
                                        required="true">
                                        <span class="help-block"></span>
                                    </div>
                                     <div class="col-sm-4">
                                        <input 
                                        class="form-control" 
                                        type="text" 
                                        name="pic_last_name" 
                                        placeholder="Nama Belakang" 
                                        required="true">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'pic_identity_type',
                                        'label' => 'Identitas',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'pic_identity_type col-md-12',
                                        'value' => [
                                            ['id' => 1, 'item' => 'KTP'],
                                            ['id' => 2, 'item' => 'SIM'],
                                            ['id' => 3, 'item' => 'NPWP'],
                                            ['id' => 4, 'item' => 'Lainnya']
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item'
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'pic_identity_type_other',
                                        'label' => 'Identitas (Lain)',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'pic_identity_number',
                                        'label' => 'Nomor Identitas',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12',
                                        'value' => ''
                                        ]
                                    );
                                ?>                          
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'pic_birth_date',
                                        'label' => 'Tanggal Lahir',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12',
                                        'value' => ''
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'pic_gender',
                                        'label' => 'Jenis Kelamin',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => [
                                            ['id' => 1, 'item' => 'Laki-Laki'],
                                            ['id' => 2, 'item' => 'Perempuan']
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item'
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'pic_religion',
                                        'label'     => 'Agama',
                                        'max'       => 100,
                                        'required'  => false,
                                        'class'     => 'select2 pic_religion col-md-12',
                                        'value'     => [
                                            ['id' => 1, 'item' => 'Islam'],
                                            ['id' => 2, 'item' => 'Kristen'],
                                            ['id' => 3, 'item' => 'Katolik'],
                                            ['id' => 4, 'item' => 'Budha'],
                                            ['id' => 5, 'item' => 'Hindu'],
                                            ['id' => 6, 'item' => 'Lainnya'],
                                        ],
                                        'keys'      => 'id',
                                        'values'    => 'item'
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'pic_phone',
                                        'label' => 'Nomor Telepon/HP',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'pic_email',
                                        'label' => 'Email',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                        ]
                                    );
                                ?>
                            </div>

                            <h4 class="box-title">Alamat PIC</h4>
                            <div class="row">

                                <?php
                                    echo $this->form->textarea_h(
                                        [
                                        'name' => 'pic_address',                                
                                        'label' => 'Alamat',                               
                                        'max' => 100,
                                        'required' => false,
                                        'value' => '',
                                        'rows' => 3,
                                        'class' => 'col-sm-12'
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'pic_province_id',
                                        'label'     => 'Provinsi',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => $provinces,
                                        'keys'      => 'province_id',
                                        'values'    => 'province_name',
                                        'class'     => 'select2 col-md-12 province pic_geobranch'
                                        ]
                                    );
                                ?>  

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'pic_city_id',
                                        'label' => 'Kota*',
                                        'max' => 100,
                                        'required' => true,
                                        'class' => 'select2 col-md-12 city select-price-params pic_geobranch',
                                        'value' => $cities,
                                        'keys' => 'city_id',
                                        'values' => 'city_name',
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'pic_districts_id',
                                        'label'     => 'Kecamatan',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => [],
                                        'keys'      => 'districts_id',
                                        'values'    => 'districts_name',
                                        'class'     => 'select2 col-md-12 district select-price-params pic_geobranch'
                                        ]
                                    );
                                ?>  
                                
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'pic_village_id',
                                        'label'     => 'Kelurahan',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => [],
                                        'keys'      => 'village_id',
                                        'values'    => 'village_name',
                                        'class'     => 'select2 col-md-12 village select-price-params pic_geobranch'
                                        ]
                                    );
                                ?> 
                            </div> 

                        </div>

                    </div>
                    
                    <hr />
                    <div class="btn-group pull-right">
                        <a href="<?php echo site_url($path); ?>" class="btn btn-flat btn-default">
                            <i class="fa fa-arrow-left"></i> Kembali 
                        </a>
                        <button type="submit" class="btn btn-primary btn-flat ">
                            <i class="fa fa-save"></i> Simpan
                        </button>  
                    </div>

                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>

        <?php $this->load->view('footer'); ?>

        <!-- Select 2 -->
        <script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

        <!-- Easy Autocomplete -->
        <script src="<?php echo base_url(); ?>assets/plugins/easy-autocomplete/jquery.easy-autocomplete.min.js"></script>

       <?php $this->load->view('customers/js'); ?>
       <?php $this->load->view('customers/js_2'); ?>

        <script type="text/javascript">

            function validate() {
                // if (document.myForm.name.value == "") {
                //     alert("Enter a name");
                //     document.myForm.name.focus();
                //     return false;
                // }
                if (!/^[a-zA-Z]*$/g.test(document.myForm.name.value)) {
                    alert("Invalid characters");
                    document.myForm.name.focus();
                    return false;
                }
            }

            $('.select2').find('select').select2();

            var user_branch_id = $('input[name="user_branch_id').val();
            var user_branch_name = $('input[name="user_branch_name').val();
            if (user_branch_id == 'null') {
                $('.user_branch').hide();
                $('.user_branch_new').show(); 
            } else {
                $('.user_branch').show();
                $('.user_branch_new').hide(); 
                $('input[name="user_branch_id"]').prop('readOnly', true); 
                $('input[name="user_branch_name"]').prop('readOnly', true);  
            }

            $('input[name="customer_company_type_other').prop('readOnly', true);    
            $('.customer_company_type').change(function() {
                var customer_company_type = $('select[name="customer_company_type').val();
                if (customer_company_type == '' || customer_company_type != '11') {
                    $('input[name="customer_company_type_other"]').prop('readOnly', true);    
                } else {
                    $('input[name="customer_company_type_other"]').prop('readOnly', false);   
                }
            });

            $('input[name="customer_id_type_other').prop('readOnly', true);
            $('input[name="customer_id_number').prop('readOnly', true);
            $('.customer_id_type').change(function() {
                var customer_id_type = $('select[name="customer_id_type').val();
                if (customer_id_type == ''){
                    $('input[name="customer_id_type_other').prop('readOnly', true);
                    $('input[name="customer_id_number').prop('readOnly', true); 
                } else if (customer_id_type != '4') {
                    $('input[name="customer_id_type_other"]').prop('readOnly', true);
                    $('input[name="customer_id_number').prop('readOnly', false);     
                } else {
                    $('input[name="customer_id_type_other"]').prop('readOnly', false);
                    $('input[name="customer_id_number').prop('readOnly', false);    
                }
            });

            $('input[name="pic_identity_type_other').prop('readOnly', true);
            $('input[name="pic_identity_number').prop('readOnly', true);
            $('.pic_identity_type').change(function() {
                var pic_identity_type = $('select[name="pic_identity_type').val();
                if (pic_identity_type == ''){
                    $('input[name="pic_identity_type_other').prop('readOnly', true);
                    $('input[name="pic_identity_number').prop('readOnly', true); 
                } else if (pic_identity_type != '4') {
                    $('input[name="pic_identity_type_other"]').prop('readOnly', true);
                    $('input[name="pic_identity_number').prop('readOnly', false);     
                } else {
                    $('input[name="pic_identity_type_other"]').prop('readOnly', false);
                    $('input[name="pic_identity_number').prop('readOnly', false);    
                }
            });

            $('input[name="pic_birth_date"]').datepicker({ format: 'dd/mm/yyyy' });

            $('.alert-btn-save').click(function () {
                $('.form-add').submit();
            });

            var optionsx = {
                url: "<?php echo site_url('request/customer_helper/get'); ?>",
                getValue: "customer_name",
                theme: "square",
                template: {
                    type: "description",
                    fields: {
                        description: "customer_code"
                    }
                },
                list: {
                    match: {
                        enabled: true
                    },
                    onChooseEvent: function () {
                        var customer_code = $("input[name='customer_referral']").getSelectedItemData().customer_code;
                        $("input[name='customer_referral']").val(customer_code);
                    }
                }
            };

            $("input[name='customer_referral']").easyAutocomplete(optionsx);

        </script>

