<div class="row">
    <?php echo form_open($path . '/filter_customers'); ?>

    <div class="col-md-12">
        <div class="col-sm-3">
            <div class="form-group">
                <label>Tanggal Awal</label>
                <input type="text" name="date_start" class="form-control" placeholder="Masukan Tanggal Awal"
                value="<?php echo ($this->session->userdata('customers')['date_start']) ? $this->session->userdata('customers')['date_start'] : '' ?>"
                />
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group">
                <label>Tanggal Akhir</label>
                <input type="text" name="date_end" class="form-control" placeholder="Masukan Tanggal Akhir"
                value="<?php echo ($this->session->userdata('customers')['date_end']) ? $this->session->userdata('customers')['date_end'] : '' ?>"
                />
            </div>
        </div>
        
        <?php
            echo $this->form->text(
                [
                'name' => 'customer_name',
                'label' => 'Nama',
                'max' => 100,
                'type' => 'text',
                'class' => 'col-sm-3 customer_name',
                'display_default' => false,
                'value' => isset($this->session->userdata('customers')['customer_name']) && $this->session->userdata('customers')['customer_name'] != '' ? $this->session->userdata('customers')['customer_name'] : ''
                ]
            );
        ?>
        <div class="form-group col-md-3">
            <label class="control-label" for="inputSuccess1">Kota</label>
            <select name="customer_city" class="form-control"> 

                <option value=" ">-- Pilih Kota --</option>

                <?php if (!empty($cities)): ?>
                    <?php foreach ($cities as $key => $item) : ?>
                        <option value="<?php echo $item['city_id']; ?>" >
                                <?php echo $item['city_name']; ?>
                        </option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>

        </div>

    </div>
    
    <div class="col-md-12">
        <div class="col-sm-3">
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" name="customer_address" class="form-control" placeholder="Masukan Alamat.."
                value="<?php echo ($this->session->userdata('customers')['customer_address']) ? $this->session->userdata('customers')['customer_address'] : '' ?>"
                />
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>Agama</label>
                <select name="pic_religion" class="form-control">
                    <option value="0" >-- Pilih Agama --</option>
                    <option value="1" >Islam</option>
                    <option value="2" >Kristen</option>
                    <option value="3" >Katholik</option>
                    <option value="4" >Hindu</option>
                    <option value="5" >Budha</option>
                    <option value="6" >Lainnya</option>
                </select>
            </div>
        </div>
        <?php
            echo $this->form->select(
                [
                'name'      => 'customer_company_type',
                'label'     => 'Jenis Usaha',
                'max'       => 100,
                'required'  => false,
                'class'     => 'col-md-3',
                'value'     => [
                    ['id' => 0, 'item' => '-- Pilih Jenis Usaha --'],
                    ['id' => 1, 'item' => 'Manufaktur'],
                    ['id' => 2, 'item' => 'Jasa'],
                    ['id' => 3, 'item' => 'Ritel'],
                    ['id' => 4, 'item' => 'Makanan'],
                    ['id' => 5, 'item' => 'Minuman'],
                    ['id' => 6, 'item' => 'Fashion'],
                    ['id' => 7, 'item' => 'Konveksi'],
                    ['id' => 8, 'item' => 'Hasil Tani'],
                    ['id' => 9, 'item' => 'Mebel'],
                    ['id' => 10, 'item' => 'Obat'],
                    ['id' => 11, 'item' => 'Lainnya'],
                ],
                'keys'      => 'id',
                'values'    => 'item'
                ]
            );
        ?>
        <?php
            echo $this->form->select(
                [
                'name'      => 'customer_company_type_of_business',
                'label'     => 'Tipe Usaha',
                'max'       => 100,
                'required'  => false,
                'class'     => 'col-md-3',
                'value'     => [
                    ['id' => 0, 'item' => '-- Pilih Tipe Usaha --'],
                    ['id' => 1, 'item' => 'Personal'],
                    ['id' => 2, 'item' => 'Perorangan'],
                    ['id' => 3, 'item' => 'UD'],
                    ['id' => 4, 'item' => 'CV'],
                    ['id' => 5, 'item' => 'PT'],
                    ['id' => 6, 'item' => 'Asing'],
                ]
                ]
            );
        ?>
    </div>

    <div class="col-sm-12">
        <div class="col-sm-3">
            <div class="btn-group">
                <button id="filter" class="btn btn-filter btn-info"><i class="fa fa-filter"></i> Filter</button>
                <a href="<?php echo site_url($path . '/clear_customers'); ?>" class="btn btn-warning"><i class="fa fa-eraser"></i> Reset</a>
            </div>
        </div>
        <a href="<?php echo site_url(''); ?>" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Download</a>
        <br />
        <hr />
    </div>

    <?php echo form_close(); ?>
</div>