<script type="text/javascript">

    $('input[name="date_start"]').datepicker({ format: 'dd/mm/yyyy' });
    $('input[name="date_end"]').datepicker({ format: 'dd/mm/yyyy' });

    autocomplete();

    function autocomplete() {
        /** Autocomplete */
        var options = {
            url: function(phrase) {
                return "<?php echo site_url('request/customer_helper/get_like?phrase='); ?>" + phrase;
            },
            getValue: "customer_name",
            theme: "square",
            requestDelay: 250,
            template: {
                type: "description",
                fields: {
                    description: "customer_code"
                }
            },
            list: {
                match: {
                    enabled: true
                },
                onChooseEvent: function () {
                    var customer_id = $("input[name='customer_name']").getSelectedItemData().customer_id;
                    $.post('<?php echo site_url('request/customer_helper/get_detail'); ?>/' + customer_id,
                            function (data) {

                                var result = JSON.parse(data);

                                if (customer_id != '') {

                                    $('input[name="customer_id"]').val(customer_id);

                                }

                            }
                    );

                }
            }
        };

        $("input[name='customer_name']").easyAutocomplete(options);

        $('input[name="customer_name"]').on('keyup', function (event) {
            if (event.which != 13) {
                $('input[name="customer_id"]').val('');
            }
        });

    }
    
</script>