<script type="text/javascript">

    /** Default province */
    var old_pic_province_id = $('input[name="old_pic_province_id"]').val();
    if (old_pic_province_id != '') {
        get_origin_province(old_pic_province_id);
    } else {
        get_origin_province();
    }

    /** Default city origin event */
    $('select[name="pic_city_id"]').on('change', function () {
        get_origin_districts($(this).val());
        $.post('<?php echo site_url('request/locations/get_province_id'); ?>', {city_id: $(this).val()}, function (data) {
            $('select[name="pic_province_id"]').val(data);
            $('select[name="pic_province_id"]').select2();
        });

    });

    function get_origin_province(selected) {

        /**Get origin cities */
        $('select[name="pic_province_id"]').on('change', function () {
            get_origin_cities($(this).val());
            var prov = $('select[name="pic_province_id"]').val();
            var province_id = $('select[name="pic_province_id"]').val();
            $('input[name="old_pic_province_id"]').val($(this).val());
        });

        // console.log(province_id);
        // console.log(prov);


        /** Repopulate */
        if (typeof (selected) !== 'undefined') {
            $('select[name="pic_province_id"]').val(selected);

            /** Repopulate origin cities */
            var old_pic_city_id = $('input[name="old_pic_city_id"]').val();
            if (old_pic_city_id != '') {
                get_origin_cities(selected, old_pic_city_id);
            } else {
                get_origin_cities(selected, branch_json['city_id']);
            }

        } else {
            $('select[name="pic_districts_id"]').val(" ");
            $('select[name="pic_village_id"]').val(" ");
        }

    }

    function get_origin_cities(province_id, selected, callback) {

        var url = '<?php echo site_url('request/locations/cities/pic_city_id'); ?>';

        $.post(url, {province_id: province_id}, function (data) {

            var form = $('select[name="pic_city_id"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="pic_city_id"]').select2();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="pic_city_id"]').val(selected);
            }

            /** Select city */
            $('select[name="pic_city_id"]').on('change', function () {
                get_origin_districts($(this).val());
            });

            // bind_change_event();

        });

        /** Closing with callback function */
        if (callback && typeof (callback) === "function") {
            callback(arguments[1]);
        }

    }

    /** Get origin districts */
    function get_origin_districts(city_id, selected) {

        var url = '<?php echo site_url('request/locations/districts/pic_districts_id'); ?>';

        $.post(url,{city_id: city_id},
        function (data) {
            var form = $('select[name="pic_districts_id"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="pic_districts_id"]').select2();
            // bind_change_event();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="pic_districts_id"]').val(selected);
                /** Select village */
                get_origin_village(selected, $('input[name="old_pic_village_id"]').val());
            }

            /** Select village */
            $('body').on('change', 'select[name="pic_districts_id"]', function () {
                get_origin_village($(this).val(), $('input[name="old_pic_village_id"]').val());
            });

        });

    }

    /** Get origin village */
    function get_origin_village(districts_id, selected) {
        $.post('<?php echo site_url('request/locations/villages/pic_village_id'); ?>',
                {districts_id: districts_id},
        function (data) {
            var form = $('select[name="pic_village_id"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="pic_village_id"]').select2();
            // bind_change_event();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="pic_village_id"]').val(selected);
            }

        }
        );
    }

</script>