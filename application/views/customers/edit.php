    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/easy-autocomplete/easy-autocomplete.css">

    <?php $this->load->view('header'); ?>
    <?php $this->load->view('component/modal/alert-save'); ?>
    <div class="row">

        <div class="col-sm-12">
            <div class="box">

           <?php echo form_open($path . '/update', 'class="form-horizontal"'); ?>
                <input type="hidden" name ="customer_id" value="<?php echo $data->customer_id; ?>" />

                <div class="box-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4><?php echo $title; ?></h4>
                        </div>
                        <div class="col-sm-6">
                            <div  class="form-group user_branch"> 
                                <label class="control-label col-sm-3" for="inputSuccess1">Cabang/Agen</label>
                                <div class="col-sm-2">
                                    <input class="form-control" type="text" name="user_branch_id" value="<?php echo $data->branch_created; ?>">
                                </div>
                                 <div class="col-sm-5">
                                    <input class="form-control" type="text" name="user_branch_name" value="<?php echo $data->branch_name?>">
                                </div>
                                <div class="col-sm-2">
                                    <a href="#" class="form-control user_branch_edit btn btn-info"><i class="fa fa-pencil"></i> edit</a>
                                </div>
                            </div>
                            <br>
                            <div  class="form-group select2 user_branch_new"> 
                                <label class="control-label col-sm-3" for="inputSuccess1">Cabang/Agen</label>
                                 <div class="col-sm-7">
                                    <select class="form-control" name="branch_created_id">
                                        <option value=" ">-- Pilih Cabang/Agen --</option>
                                        <?php 
                                        if (!empty($list_branches)) {
                                            foreach ($list_branches as $key => $value) {
                                                ?>
                                                <option value="<?php echo $value->branch_id; ?>">
                                                <?php echo $value->branch_code.' -'.$value->branch_name; ?>
                                                </option>
                                                <?php 
                                            }
                                        } 
                                        ?>
                                    </select>
                                </div>
                                 <div class="col-sm-2">
                                    <a href="#" class="form-control user_branch_cancel btn btn-warning">cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">

                    <div class="row">
                        <div class="col-sm-6">

                            <h4 class="box-title">No. Referral</h4>
                            <div class="row">
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_referral',
                                        'label' => 'Bila Ada',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12',
                                        'attribute' => ['style' => 'border-radius:0px;'],
                                        'value' => $data->customer_referral,
                                        ]
                                    );
                                ?>
                            </div>

                            <h4 class="box-title">Kode Pelanggan</h4>
                            <div class="row">
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_code',
                                        'label' => 'Kode Pelanggan',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12',
                                        'value' => $data->customer_code
                                        ]
                                    );
                                ?>
                            </div>

                            <h4 class="box-title">Info Pelanggan</h4>
                            <div class="row">
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_name',
                                        'label' => 'Nama*',
                                        'max' => 100,
                                        'required' => true,
                                        'class' => 'col-sm-12',
                                        'value' => $data->customer_name
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_phone',
                                        'label' => 'Nomor Telepon/HP',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => $data->customer_phone
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->textarea_h(
                                        [
                                        'name'      => 'customer_address',
                                        'label'     => 'Alamat',
                                        'max'       => 100,
                                        'required'  => false,
                                        'class'     => 'col-md-12',
                                        'value' => $data->customer_address,
                                        'rows'      => 5
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'customer_province_id',
                                        'label'     => 'Provinsi',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => $provinces,
                                        'keys'      => 'province_id',
                                        'values'    => 'province_name',
                                        'class'     => 'select2 col-md-12 customer_geobranch',
                                        'selected' => $data->customer_province_id,
                                        ]
                                    );
                                ?>  
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'customer_city_id',
                                        'label'     => 'Kota*',
                                        'max'       => 100,
                                        'required'  => true,
                                        'value'     => $cities,
                                        'keys'      => 'city_id',
                                        'values'    => 'city_name',
                                        'class'     => 'select2  col-md-12 select-price-params customer_geobranch',
                                        'selected' => $data->customer_city
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'customer_districts_id',
                                        'label'     => 'Kecamatan',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => [],
                                        'keys'      => 'districts_id',
                                        'values'    => 'districts_name',
                                        'class'     => 'select2 col-md-12 select-price-params customer_geobranch',
                                        'selected' => $data->customer_districts_id
                                        ]
                                    );
                                ?>  
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'customer_village_id',
                                        'label'     => 'Kelurahan',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => [],
                                        'keys'      => 'village_id',
                                        'values'    => 'village_name',
                                        'class'     => 'select2 col-md-12 select-price-params customer_geobranch',
                                        'selected' => $data->customer_village_id
                                        ]
                                    );
                                ?> 
                            </div>

                            <h4 class="box-title">Info Lain</h4>
                            <div class="row">
                                
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'customer_id_type',
                                        'label' => 'Identitas',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'customer_id_type col-md-12',
                                        'value' => [
                                            ['id' => 1, 'item' => 'KTP'],
                                            ['id' => 2, 'item' => 'SIM'],
                                            ['id' => 3, 'item' => 'NPWP'],
                                            ['id' => 4, 'item' => 'Lainnya']
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item',
                                        'selected' => $data->customer_id_type
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_id_type_other',
                                        'label' => 'Identitas (Lain)',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => $data->customer_id_type_other
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_id_number',
                                        'label' => 'Nomor Identitas',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12',
                                        'value' => $data->customer_id_number
                                        ]
                                    );
                                ?>                          
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'customer_type',
                                        'label' => 'Tipe Customer',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => [
                                            ['id' => 1, 'item' => 'Personal'],
                                            ['id' => 2, 'item' => 'Perusahaan'],
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item',
                                        'selected' => $data->customer_type
                                        ]
                                    );
                                ?>
                                

                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'customer_company_type',
                                        'label'     => 'Jenis Usaha',
                                        'max'       => 100,
                                        'required'  => false,
                                        'class'     => 'select2 customer_company_type col-md-12',
                                        'value'     => [
                                            ['id' => 1, 'item' => 'Manufaktur'],
                                            ['id' => 2, 'item' => 'Jasa'],
                                            ['id' => 3, 'item' => 'Ritel'],
                                            ['id' => 4, 'item' => 'Makanan'],
                                            ['id' => 5, 'item' => 'Minuman'],
                                            ['id' => 6, 'item' => 'Fashion'],
                                            ['id' => 7, 'item' => 'Konveksi'],
                                            ['id' => 8, 'item' => 'Hasil Tani'],
                                            ['id' => 9, 'item' => 'Mebel'],
                                            ['id' => 10, 'item' => 'Obat'],
                                            ['id' => 11, 'item' => 'Lainnya'],
                                        ],
                                        'keys'      => 'id',
                                        'values'    => 'item',
                                        'selected' => $data->customer_company_type
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name'      => 'customer_company_type_other',
                                        'label'     => 'Jenis Usaha Lainnya',
                                        'max'       => 100,
                                        'required'  => false,
                                        'class'     => 'col-md-12',
                                        'value' => $data->customer_company_type_other
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'customer_company_type_of_business',
                                        'label'     => 'Tipe Usaha',
                                        'max'       => 100,
                                        'required'  => false,
                                        'class'     => 'select2 col-md-12',
                                        'value'     => [
                                            ['id' => 1, 'item' => 'Personal'],
                                            ['id' => 2, 'item' => 'Perorangan'],
                                            ['id' => 3, 'item' => 'UD'],
                                            ['id' => 4, 'item' => 'CV'],
                                            ['id' => 5, 'item' => 'PT'],
                                            ['id' => 6, 'item' => 'Asing'],
                                        ],
                                        'selected' => $data->customer_company_type_of_business
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_company_email',
                                        'label' => 'Email',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => $data->customer_company_email
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name'      => 'customer_web_id',
                                        'label'     => 'ID Web',
                                        'max'       => 100,
                                        'required'  => false,
                                        'class'     => 'col-md-12',
                                        'value' => $data->customer_web_id
                                        ]
                                    );
                                ?>
                            </div>

                            <h4 class="box-title">Informasi Pengiriman</h4>
                            <div class="row">
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'customer_freq_sending',
                                        'label' => 'Frekuensi Pengiriman Paket',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'select2 col-md-12',
                                        'value' => [
                                        ['id' => 1, 'value' => 'Harian'],
                                        ['id' => 2, 'value' => 'MIngguan'],
                                        ['id' => 3, 'value' => 'Bulanan'],
                                        ],
                                        'keys' => 'id',
                                        'values' => 'value',
                                        'selected' => $data->customer_freq_sending
                                        ]
                                    );
                                ?>

                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'customer_sending_quota',
                                        'label' => 'Jumlah Pengiriman Paket',
                                        'max' => 100,
                                        'required' => false,
                                        'value' => $data->customer_sending_quota,
                                        'class' => 'col-md-12'
                                        ]
                                    );
                                ?>
                            </div> 
                                 
                        </div>

                        <div class="col-sm-6">                           

                            <h4 class="box-title">Informasi PIC</h4>
                            <div class="row">
                                <div  class="form-group"> 
                                    <label class="control-label col-sm-3" for="inputSuccess1">Nama Kontak/Pemilik*</label>
                                    <div class="col-sm-4">
                                        <input 
                                        class="form-control" 
                                        type="text" 
                                        name="pic_first_name" 
                                        placeholder="Nama Depan" 
                                        required="true"
                                        value="<?php echo $data->pic_first_name; ?>">
                                        <span class="help-block"></span>
                                    </div>
                                     <div class="col-sm-4">
                                        <input 
                                        class="form-control" 
                                        type="text" 
                                        name="pic_last_name" 
                                        placeholder="Nama Belakang"
                                        required="true"
                                        value="<?php echo $data->pic_last_name; ?>">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'pic_identity_type',
                                        'label' => 'Identitas',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'pic_identity_type col-md-12',
                                        'value' => [
                                            ['id' => 1, 'item' => 'KTP'],
                                            ['id' => 2, 'item' => 'SIM'],
                                            ['id' => 3, 'item' => 'NPWP'],
                                            ['id' => 4, 'item' => 'Lainnya']
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item',
                                        'selected' => $data->pic_identity_type
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'pic_identity_type_other',
                                        'label' => 'Identitas (Lain)',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => $data->pic_identity_type_other
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'pic_identity_number',
                                        'label' => 'Nomor Identitas',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12',
                                        'value' => $data->pic_identity_number
                                        ]
                                    );
                                ?>                          
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'pic_birth_date',
                                        'label' => 'Tanggal Lahir',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12',
                                        'value' => (!empty($data->pic_birth_date)) ? mdate('%d/%m/%Y', $data->pic_birth_date) : ' - ',
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'pic_gender',
                                        'label' => 'Jenis Kelamin',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => [
                                            ['id' => 0, 'item' => '-- Pilih Jenis Kelamin --'],
                                            ['id' => 1, 'item' => 'Laki-Laki'],
                                            ['id' => 2, 'item' => 'Perempuan']
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item',
                                        'selected' => $data->pic_gender
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name' => 'pic_religion',
                                        'label' => 'Agama',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'pic_religion col-md-12',
                                        'value' => [
                                            ['id' => 0, 'item' => '-- Pilih Agama --'],
                                            ['id' => 1, 'item' => 'Islam'],
                                            ['id' => 2, 'item' => 'Kristen'],
                                            ['id' => 3, 'item' => 'Katolik'],
                                            ['id' => 4, 'item' => 'Hindu'],
                                            ['id' => 4, 'item' => 'Budha'],
                                            ['id' => 4, 'item' => 'Lainnya']
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item',
                                        'selected' => $data->pic_religion
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'pic_phone',
                                        'label' => 'Nomor Telepon/HP',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => $data->pic_phone
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->text_h(
                                        [
                                        'name' => 'pic_email',
                                        'label' => 'Email',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => $data->pic_email
                                        ]
                                    );
                                ?>
                            </div>

                            <h4 class="box-title">Alamat PIC</h4>
                            <div class="row">
                                <?php
                                    echo $this->form->textarea_h(
                                        [
                                        'name'      => 'pic_address',
                                        'label'     => 'Alamat',
                                        'max'       => 100,
                                        'required'  => false,
                                        'class'     => 'col-md-12',
                                        'value' => $data->pic_address,
                                        'rows'      => 5
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'pic_province_id',
                                        'label'     => 'Provinsi',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => $provinces,
                                        'keys'      => 'province_id',
                                        'values'    => 'province_name',
                                        'class'     => 'select2 col-md-12 pic_geobranch',
                                        'selected' => $data->pic_province_id,
                                        ]
                                    );
                                ?>  
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'pic_city_id',
                                        'label'     => 'Kota*',
                                        'max'       => 100,
                                        'required'  => true,
                                        'value'     => $cities,
                                        'keys'      => 'city_id',
                                        'values'    => 'city_name',
                                        'class'     => 'select2  col-md-12 select-price-params pic_geobranch',
                                        'selected'  => $data->pic_city_id
                                        ]
                                    );
                                ?>
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'pic_districts_id',
                                        'label'     => 'Kecamatan',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => [],
                                        'keys'      => 'districts_id',
                                        'values'    => 'districts_name',
                                        'class'     => 'select2 col-md-12 select-price-params pic_geobranch',
                                        'selected' => $data->pic_districts_id
                                        ]
                                    );
                                ?>  
                                <?php
                                    echo $this->form->select_h(
                                        [
                                        'name'      => 'pic_village_id',
                                        'label'     => 'Kelurahan',
                                        'max'       => 100,
                                        'required'  => false,
                                        'value'     => [],
                                        'keys'      => 'village_id',
                                        'values'    => 'village_name',
                                        'class'     => 'select2 col-md-12 select-price-params pic_geobranch',
                                        'selected' => $data->pic_village_id
                                        ]
                                    );
                                ?> 
                            </div> 

                        </div>

                    </div>
                    
                    <hr />
                    <div class="btn-group pull-right">
                        <a href="<?php echo site_url($path); ?>" class="btn btn-flat btn-default">
                            <i class="fa fa-arrow-left"></i> Kembali 
                        </a>
                        <button type="submit" class="btn btn-primary btn-flat ">
                            <i class="fa fa-save"></i> Simpan
                        </button>  
                    </div>

                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>

        <?php $this->load->view('footer'); ?>

        <!-- Select 2 -->
        <script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

        <!-- Easy Autocomplete -->
        <script src="<?php echo base_url(); ?>assets/plugins/easy-autocomplete/jquery.easy-autocomplete.min.js"></script>

        <?php $this->load->view('customers/js'); ?>
        <?php $this->load->view('customers/js_2'); ?>

        <script type="text/javascript">

            $('input[name="pic_birth_date"]').datepicker({ format: 'dd/mm/yyyy' });

            $('.alert-btn-save').click(function () {
                $('.form-add').submit();
            });

            var optionsx = {
                url: "<?php echo site_url('request/customer_helper/get'); ?>",
                getValue: "customer_name",
                theme: "square",
                template: {
                    type: "description",
                    fields: {
                        description: "customer_code"
                    }
                },
                list: {
                    match: {
                        enabled: true
                    },
                    onChooseEvent: function () {
                        var customer_code = $("input[name='customer_referral']").getSelectedItemData().customer_code;
                        $("input[name='customer_referral']").val(customer_code);
                    }
                }
            };

            $("input[name='customer_referral']").easyAutocomplete(optionsx);

            function validate() {
                if (!/^[a-zA-Z]*$/g.test(document.myForm.name.value)) {
                    alert("Invalid characters");
                    document.myForm.name.focus();
                    return false;
                }
            }

            $('.select2').find('select').select2();

            var user_branch_id = $('input[name="user_branch_id"]').val();
            $('input[name="user_branch_id"]').prop('readOnly', true); 
            $('input[name="user_branch_name"]').prop('readOnly', true);  
            $('.user_branch_new').hide(); 
            $('.user_branch_edit').click(function () {
                $('.user_branch_new').show();
                $('.user_branch').hide();
                $('input[name="user_branch_id"]').val("");
            });
            $('.user_branch_cancel').click(function () {
                $('.user_branch_new').hide();
                $('.user_branch').show();
                $('input[name="user_branch_id"]').val(user_branch_id);
            });

            $('.customer_company_type').change(function() {
                var customer_company_type = $('select[name="customer_company_type').val();
                if (customer_company_type == '' || customer_company_type != '11') {
                    $('input[name="customer_company_type_other"]').prop('readOnly', true);    
                } else {
                    $('input[name="customer_company_type_other"]').prop('readOnly', false);   
                }
            });

            $('input[name="customer_company_type_other').prop('readOnly', true);    
            $('.customer_company_type').change(function() {
                var customer_company_type = $('select[name="customer_company_type').val();
                if (customer_company_type == '' || customer_company_type != '11') {
                    $('input[name="customer_company_type_other"]').prop('readOnly', true);    
                } else {
                    $('input[name="customer_company_type_other"]').prop('readOnly', false);   
                }
            });

            $('input[name="customer_id_type_other').prop('readOnly', true);
            $('input[name="customer_id_number').prop('readOnly', true);
            $('.customer_id_type').change(function() {
                var customer_id_type = $('select[name="customer_id_type').val();
                if (customer_id_type == ''){
                    $('input[name="customer_id_type_other').prop('readOnly', true);
                    $('input[name="customer_id_number').prop('readOnly', true); 
                } else if (customer_id_type != '4') {
                    $('input[name="customer_id_type_other"]').prop('readOnly', true);
                    $('input[name="customer_id_number').prop('readOnly', false);     
                } else {
                    $('input[name="customer_id_type_other"]').prop('readOnly', false);
                    $('input[name="customer_id_number').prop('readOnly', false);    
                }
            });

            $('input[name="pic_identity_type_other').prop('readOnly', true);
            $('input[name="pic_identity_number').prop('readOnly', true);
            $('.pic_identity_type').change(function() {
                var pic_identity_type = $('select[name="pic_identity_type').val();
                if (pic_identity_type == ''){
                    $('input[name="pic_identity_type_other').prop('readOnly', true);
                    $('input[name="pic_identity_number').prop('readOnly', true); 
                } else if (pic_identity_type != '4') {
                    $('input[name="pic_identity_type_other"]').prop('readOnly', true);
                    $('input[name="pic_identity_number').prop('readOnly', false);     
                } else {
                    $('input[name="pic_identity_type_other"]').prop('readOnly', false);
                    $('input[name="pic_identity_number').prop('readOnly', false);    
                }
            });

            

        </script>

