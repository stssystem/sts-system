

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4>Edit User Profil</h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open_multipart($path . '/update', ['class' => 'form-edit']); ?>

                <input type="hidden" name ="user_id" value="<?php echo $data->user_id ?>" />
                <input type="hidden" name ="userprofile_id" value="<?php echo $data->userprofile_id ?>" />

                <div class="row">
                    <div class="col-md-9">

                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#user-profile" aria-controls="home" role="tab" data-toggle="tab">User Profil</a></li>
                                <li role="presentation"><a href="#password" aria-controls="profile" role="tab" data-toggle="tab">Password</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">

                                <br/>

                                <div role="tabpanel" class="tab-pane active" id="user-profile">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" type="text" value="<?php echo $data->userprofile_fullname ?>"  name="userprofile_fullname" max="30" required="required"/>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Username</label>
                                            <input class="form-control" type="text" value="<?php echo $data->username ?>"  name="username" max="30" required="required"/>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>Email</label>
                                            <input class="form-control" type="text" value="<?php echo $data->user_email ?>"  name="user_email" max="30" required="required"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Address</label>
                                        <textarea name="userprofile_address" class="form-control"><?php echo $data->userprofile_address ?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label>Tipe User</label>
                                        <select name="user_type_id" class="form-control"  >
                                            <option>-- Pilih TIpe User --</option>
                                            <?php foreach ($user_type_id as $key => $item): ?>
                                                <option value="<?php echo $item->user_type_id; ?>" <?php echo ($data->user_type_id == $item->user_type_id ) ? 'selected="selected"' : '' ?> >
                                                    <?php echo $item->user_type_name ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane" id="password">

                                    <div class="form-group">
                                        <label>Password Lama</label>
                                        <input class="form-control" type="password" value=""  name="old_password" max="30" required="required"/>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Password Baru</label>
                                            <input class="form-control" type="password" value=""  name="new_password" max="30" required="required"/>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>Konfirmasi Password Baru</label>
                                            <input class="form-control" type="password" value=""  name="confirm_password" max="30" required="required"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="btn-group">
                                <a type="button" class="btn-info btn btn-save" data-toggle="modal" data-target="#modal-save-alert">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="row" style="margin-top: 85px;">
                            <div class="col-md-12" style="height: 100%;">
                                <a href="#" class="thumbnail" id="photo_profile">
                                    <img src="<?php echo image_profile(); ?>" style="width: 100%;">
                                </a>
                                <input type="file" name="userfile" id="photo_upload" style="display: none;" />
                                <input type="checkbox" name ="delete_photo" /> Hapus Foto
                            </div>
                        </div>
                    </div>

                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

    $('.btn-save').click(function () {

        var user_type_name = $('.form-edit').find('input[name="user_type_name"]').val();

        if (user_type_name == '') {

            /** Set message */
            var text = "User Tipe harus diisi";

        } else {

            /** Set message */
            var text = "Apakah Anda akan memperbarui data profil?";

        }

        /** Set modal */
        $('#modal-save-alert').find('.modal-body p').html(text);

    });

    $('.alert-btn-save').click(function () {
        $('.form-edit').submit();
    });

    $("a#photo_profile").click(function () {
        $("input#photo_upload").trigger("click");
    });

    $("input#photo_upload").change(function () {

        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) {
            return;
        }

        if (/^image/.test(files[0].type)) {

            var reader = new FileReader();
            reader.readAsDataURL(files[0]);

            reader.onloadend = function () {

                $('a#photo_profile > img').remove();
                var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                $('a#photo_profile').append(html);

            }

        }

    });

    /** Icheck */
//    $('input[name="delete_photo"]').iCheck({
//        checkboxClass: 'icheckbox_square-blue',
//        increaseArea: '20%' // optional
//    });


</script>