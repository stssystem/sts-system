<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }

    .tbody-tax-report tr td:nth-child(2){
        text-align: right;
    }

    .tbody-tax-report tr td:nth-child(3){
        text-align: right;
    }

    .tbody-tax-report tr td:nth-child(4){
        text-align: right;
    }
    
    .th-total{
        text-align: right;
    }
    
    .footer-background{
        background-color: #bdbdbd;
    }

</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open($path . '/filter'); ?>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Tanggal Awal</label>
                            <input type="text" name="date_start_filter" class="form-control"
                                   value="<?php echo ($this->session->userdata('tax_report_filter')['date_start']) ? $this->session->userdata('tax_report_filter')['date_start'] : date('01/m/Y'); ?>"
                                   />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Tanggal Akhir</label>
                            <input type="text" name="date_end_filter" class="form-control"
                                   value="<?php echo ($this->session->userdata('tax_report_filter')['date_end']) ? $this->session->userdata('tax_report_filter')['date_end'] : date('t/m/Y'); ?>"
                                   />
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top: 24px;">
                        <div class="btn-group">
                            <button id="filter" class="btn btn-filter btn-info"><i class="fa fa-filter"></i> Filter</button>
                            <a href="<?php echo site_url($path . '/clear'); ?>" class="btn btn-warning"><i class="fa fa-eraser"></i> Reset</a>
                        </div>
                        <a href="<?php echo site_url($path . '/donwload'); ?>" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Download</a>
                    </div>

                    <?php echo form_close(); ?>

                    <div class="col-md-12">
                        <?php get_alert(); ?>
                    </div>

                </div>

                <hr />

                <div class="row">
                    <div class="col-sm-12 table-responsive ">
                        <table class="table data-tables">
                            <thead>
                                <tr >
                                    <th width="30%" >Cabang</th>
                                    <th width="20%" >DPP</th>
                                    <th width="20%" >PPN</th>
                                    <th width="25%" >Total</th>
                                    <th width="5%" >Action</th>
                                </tr>
                            </thead>
                            <tbody class="tbody-tax-report" >

                            </tbody>
                            <tfoot class="footer-background">
                                <tr>
                                    <th>Total</th>
                                    <th class="th-total total-dpp" ></th>
                                    <th class="th-total total-ppn" ></th>
                                    <th class="th-total total" ></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>


<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Numeral -->
<script src="<?php echo base_url(); ?>assets/plugins/numeral/numeral.min.js"></script>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >    

    $('input[name="date_start_filter"]').datepicker({format: 'dd/mm/yyyy'});
    $('input[name="date_end_filter"]').datepicker({format: 'dd/mm/yyyy'});

    $(".select2").select2();
    $(".select2").select2();

    /** Data tables set up */
    data_tables();
    function data_tables() {

        var url = '<?php echo site_url($path) ?>';

        var tables = $('.data-tables').DataTable({            
            serverSide: true,
            order: [
                [
                    <?php dtorder_column('dt_report_tax', 0); ?>,
                    "<?php dtorder_mode('dt_report_tax', 'asc'); ?>"
                ]
            ],
            displayStart: <?php dtarray('dt_report_tax', 'start', 0); ?>,
            pageLength: <?php dtarray('dt_report_tax', 'lenght', 10); ?>,
            search: {
                search: "<?php dtarray('dt_report_tax', 'search', ''); ?>"
            },
            ajax: {
                url: url,
                type: 'POST'
            },
            columns: [
                {data: 'branch'},
                {data: 'dpp'},
                {data: 'ppn'},
                {data: 'total'},
                {data: 'action'}
            ], footerCallback: function (row, data, start, end, display) {

                // Get total from all page and update the footer
                $.get("<?php echo site_url($path . '/get_total'); ?>", function (data) {
                    var result = JSON.parse(data);
                    $('.total-dpp').html(result['dpp']);
                    $('.total-ppn').html(result['ppn']);
                    $('.total').html(result['total']);
                });

            }
        });

    }

</script>