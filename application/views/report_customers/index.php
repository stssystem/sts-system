<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header" style="height: 90px;">
                <?php echo form_open($path . '/filter'); ?>

                <div class="row">

                    <?php
                    echo $this->form->select(
                            [
                                'name' => 'cities',
                                'label' => 'Kota Asal',
                                'value' => get_cities_list(),
                                'keys' => 'city_id',
                                'values' => 'city_name',
                                'class' => 'select2 col-md-3',
                                'display_default' => true,
                                'selected' => isset($this->session->userdata('customers_report_filter')['city_id']) ? $this->session->userdata('customers_report_filter')['city_id'] : ''
                            ]
                    );
                    ?>

                    <?php
                    echo $this->form->select(
                            [
                                'name' => 'branches',
                                'label' => 'Cabang Asal',
                                'value' => get_branches_list(),
                                'keys' => 'branch_id',
                                'values' => 'branch_name',
                                'class' => 'select2 col-md-3',
                                'display_default' => true,
                                'selected' => isset($this->session->userdata('customers_report_filter')['branch_id']) ? $this->session->userdata('customers_report_filter')['branch_id'] : ''
                            ]
                    );
                    ?>

                    <?php
                    echo $this->form->text(
                            [
                                'name' => 'start_date',
                                'label' => 'Tanggal Mulai',
                                'max' => 100,
                                'type' => 'text',
                                'class' => 'col-md-3',
                                'display_default' => false,
                                'value' => ($this->session->userdata('customers_report_filter')['date_start']) ? $this->session->userdata('customers_report_filter')['date_start'] : date('01/m/Y')
                            ]
                    );
                    ?>

                    <?php
                    echo $this->form->text(
                            [
                                'name' => 'end_date',
                                'label' => 'Tanggal Akhir',
                                'max' => 100,
                                'type' => 'text',
                                'class' => 'col-md-3',
                                'display_default' => false,
                                'value' => ($this->session->userdata('customers_report_filter')['date_end']) ? $this->session->userdata('customers_report_filter')['date_end'] : date('t/m/Y')
                            ]
                    );
                    ?>

                </div>

                <div class="row">

                    <div class="col-md-12">
                        <div class="btn-group" >
                            <button type="submit" class="btn btn-info">
                                <i class="fa fa-filter"></i> Filter
                            </button>
                            <a href="<?php echo site_url($path . '/clear'); ?>" class="btn btn-warning">
                                <i class="fa fa-refresh"></i> Reset
                            </a>
                        </div>

                        <div class="btn-group pull-right" >
                            <a href="<?php echo site_url($path . "/download") ?>" class="btn btn-success" >
                                <i class="fa fa-file-excel-o"></i> Download
                            </a>
                        </div>

                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
           
            <div class="box-body">
                <div class="box-body table-responsive ">
                    <?php get_alert(); ?>
                     <hr/>
                    <div class="row">
                        <table class="table data-tables txt-sm">
                            <thead>
                                <tr>
                                    <th width="5%">ID</th>
                                    <th width="20%">Nama</th>
                                    <th width="20%">Kota Asal</th>
                                    <th width="15%">Cabang Asal</th>
                                    <th width="10%">No Referal</th>
                                    <th width="15%">Total Transaksi</th>
                                    <th width="10%">Transaksi Terakhir</th>
                                    <th width="5%">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot style="background-color: #d1d1d1;" >
                                <tr>
                                    <th colspan="5"  style="font-size: 16px;" >Total</th>
                                    <th colspan="3" style="font-size: 16px;" ></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Numeral -->
<script src="<?php echo base_url(); ?>assets/plugins/numeral/numeral.min.js"></script>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    $('select[name="branches"]').select2();
    $('select[name="cities"]').select2();
    $('input[name="start_date"]').datepicker({format: 'dd/mm/yyyy'});
    $('input[name="end_date"]').datepicker({format: 'dd/mm/yyyy'});

    /** Data tables set up */
    $('.data-tables').DataTable({
        serverSide: true,
        order: [
            [
                <?php dtorder_column('dt_report_customers', 0); ?>,
                "<?php dtorder_mode('dt_report_customers', 'asc'); ?>"
            ]
        ],
        displayStart: <?php dtarray('dt_report_customers', 'start', 0); ?>,
        pageLength: <?php dtarray('dt_report_customers', 'lenght', 10); ?>,
        search: {
            search: "<?php dtarray('dt_report_customers', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>',
            type: 'POST'
        },
        columns: [
            {data: 'customer_id'},
            {data: 'customer_name'},
            {data: 'city_name'},
            {data: 'branch_name'},
            {data: 'customer_referral'},
            {data: 'total_transaction'},
            {data: 'last_transaction'},
            {data: 'action'}
        ], footerCallback: function (row, data, start, end, display) {

            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                        i.replace(/[\$.]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                    .column(5)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

            // Update footer
            $(api.column(5).footer()).html(
                    'Rp ' + numeral(total).format('0,0')
                    );

        }
    }
    );

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus pelanggan <strong>" + data['customer_name'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);
        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['customer_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });

</script>