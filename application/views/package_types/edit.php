<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open($path . '/update', ['class' => 'form-edit']); ?>

                <input type="hidden" name="package_type_id" value="<?php echo $data->package_type_id ?>" />

                <?php
                echo $this->form->text(
                        [
                            'name' => 'package_type_name',
                            'label' => 'Nama Tipe Paket',
                            'max' => 100,
                            'required' => true,
                            'value' => $data->package_type_name
                        ]
                );
                ?>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url($path); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <button type="submit" class="btn-info btn btn-save" >
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>