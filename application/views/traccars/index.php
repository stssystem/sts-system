<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4><?php echo $title; ?></h4>
                    </div>
                    <div class="col-md-12">
                    </div>
                </div>
            </div>
            <div class="box-body">
                <?php echo form_open(''); ?>
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                        echo $this->form->select([
                            'name' => 'armada_id',
                            'label' => '',
                            'value' => $armadas,
                            'keys' => 'armada_id',
                            'values' => 'armada_name',
                            'class' => 'select2',
                            'required' => true
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <a type="button" class="btn-info btn btn-save" data-toggle="modal" data-target="#modal-save-alert">
                            <i class="fa fa-search"></i>
                            Filter
                        </a>
                    </div>

                </div> 
                <?php form_close(); ?>
                <hr />
                <div id="maps" style="width: 100%; height: 700px;">

                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name ="latlang" value="-7.477001,110.220713"/>
<input type="hidden" name ="zoom"  value="5" />


<?php $this->load->view('footer'); ?>

<!-- Map api -->
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0rbsGWnTpB_XUFNahoj710PZNvFvrqXI&v=3&callback=initMap" async defer></script>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>


<script type="text/javascript">

    $(".select2").find('select').select2();

    var map;
    var marker = [];
    var i;
    var locations = 0;
    locations = <?php echo $data; ?>;

    var latlang = $('input[name="latlang"]').val();
    var res = latlang.split(",");

    var latitude = res[0];
    var longitude = res[1];
    var zoom = parseInt($('input[name="zoom"]').val());

    function initMap() {

        var myLatlng = new google.maps.LatLng(latitude, longitude);

        var myOptions = {
            zoom: zoom,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById("maps"), myOptions);
        var infowindow = new google.maps.InfoWindow(
                {
                    size: new google.maps.Size(200, 200)
                });


        for (i = 0; i < locations.length; i++) {

            var info_ = '<table style="width:200px; font-size:11px;">'
                    + '<tr>'
                    + '<th colspan="2" style="font-weight:bold;">' + locations[i][0] + '</th>'
                    + '</tr>'
                    + '<tr>'
                    + '<td style="wdith:70px;">Perjalanan </td>'
                    + '<td>' + locations[i][4] + '</td>'
                    + '</tr>'
                    + '<tr>'
                    + '<td style="wdith:70px;">Sopir </td>'
                    + '<td>' + locations[i]['driver'] + '</td>'
                    + '</tr>'
                    + '<tr>'
                    + '<td style="wdith:70px;">Volume </td>'
                    + '<td>' + locations[i]['volume'] + ' m<sup>3</sup></td>'
                    + '</tr>'
                    + '<tr>'
                    + '<td>Tonase </td>'
                    + '<td>' + locations[i]['tonase'] + '</td>'
                    + '</tr>'
                    + '<tr>'
                    + '<td>Plat Nomor </td>'
                    + '<td>' + locations[i]['license_plate'] + '</td>'
                    + '</tr>'
                    + '</table>';

            marker[i] = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon: '<?php echo base_url('assets/public/marker.png') ?>',
                title: locations[i][0],
                info: info_
            });

            new google.maps.event.addListener(marker[i], 'click', function () {
                infowindow.setContent(this.info);
                infowindow.open(map, this);
            });

        }

    }

    setInterval(function () {

        $.get('<?php echo site_url($path . '/update'); ?>', function (data) {
            locations = JSON.parse(data);
        });

        for (i = 0; i < locations.length; i++) {

            position = new google.maps.LatLng(locations[i][1], locations[i][2]);
            marker[i].setPosition(position);

        }

    }, 90000);

</script>
