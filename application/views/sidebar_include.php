<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->

        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo image_profile(); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo profile()->userprofile_fullname; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li id="dashboard"><a href="<?php echo site_url('program'); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            <?php if (profile()->branch_id != '' || profile()->regional_id != '' || profile()->branch_access == 1 || profile()->user_type_id == 99): ?>

                <?php if (is_access_parent([1, 2, 3, 4, 5, 41])): ?>
                    <li class="header">OPERASIONAL</li>
                    <?php if (is_access_parent([1, 2, 3, 4, 5, 41])): ?>
                        <li id="cs" class="treeview">
                            <a href=""><i class="fa fa-desktop"></i> <span>Admin</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <?php if (is_access(1)): ?>
                                    <li><a href="<?php echo site_url('orders/orders/restart'); ?>"><i class="fa fa-file"></i> Pengiriman Baru</a></li>
                                <?php endif; ?>
                                <?php if (is_access(41)): ?>
                                    <li><a href="<?php echo site_url('orders/booking'); ?>"><i class="fa fa-sticky-note"></i> Booking Order</a></li>
                                <?php endif; ?>
                                <?php if (is_access(5)): ?>
                                    <li><a href="<?php echo site_url('orders/order'); ?>"><i class="fa fa-bar-chart"></i> Daftar Order</a></li>
                                <?php endif; ?>
                                <?php if (is_access(3)): ?>
                                    <li><a href="<?php echo site_url('orders/pickup_orders'); ?>"><i class="fa fa-truck"></i> Pick Up Order</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (is_access_parent([7, 27, 30, 36, 50])): ?>
                    <li id="paket-ops" class="treeview">
                        <a href="#">
                            <i class="fa fa-archive"></i> 
                            <span>Kelola Paket</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                             <?php if (is_access(50)): ?>
                                <li><a href="<?php echo site_url('packages/package_management'); ?>"><i class="fa fa-file-o"></i> Daftar Kelola Paket</a></li>
                            <?php endif; ?>
                            <?php if (is_access(27)): ?>
                                <li><a href="<?php echo site_url('packages/packages/loading_barang'); ?>"><i class="fa fa-archive"></i> Loading</a></li>
                            <?php endif; ?>
                            <?php if (is_access(7)): ?>
                                <li><a href="<?php echo site_url('packages/packages/unloading_barang'); ?>"><i class="fa fa-download"></i> Unloading</a></li>
                            <?php endif; ?>
                            <?php if (is_access(30)): ?>
                                <li><a href="<?php echo site_url('packages/deliveries'); ?>"><i class="fa fa-truck"></i> Pengantaran</a></li>
                            <?php endif; ?>
                            <?php if (is_access(36)): ?>
                                <li><a href="<?php echo site_url('orders/locations_update'); ?>"><i class="fa fa-thumb-tack"></i> Update Lokasi</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>

                <?php if (is_access_parent([42, 43, 44, 45])): ?>
                    <li id="paket-ops" class="treeview">
                        <a href="#">
                            <i class="fa  fa-sticky-note"></i>
                            <span>Kelola Resi</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <?php if (is_access(42)): ?>
                                <li>
                                    <a href="<?php echo site_url('resi/franco_list'); ?>">
                                        <i class="fa fa-bars"></i>
                                        Daftar Resi Franco
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (is_access(43)): ?>
                                <li>
                                    <a href="<?php echo site_url('resi/franco/page_delivery'); ?>">
                                        <i class="fa fa-plane"></i> 
                                        Kirim Resi
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (is_access(44)): ?>
                                <li>
                                    <a href="<?php echo site_url('resi/franco_back/page_delivery'); ?>">
                                        <i class="fa fa-truck"></i>
                                        Terima Resi
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (is_access(45)): ?>
                                <li>
                                    <a href="<?php echo site_url('resi/franco_delivery/page_delivery'); ?>">
                                        <i class="fa fa-bus"></i> 
                                        Antar Resi
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>

                <?php if (is_access_parent([31, 32, 33, 34, 35, 49])): ?>
                    <li id="trip-manage" class="treeview">
                        <a href="#">
                            <i class="fa  fa-file-text"></i>
                            <span>Laporan</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>

                        <ul class="treeview-menu">

                            <?php if (is_access(31)): ?>
                                <li><a href="<?php echo site_url('report/report_order'); ?>"><i class="fa fa-money"></i> Laporan Keuangan</a></li>
                            <?php endif; ?>

                            <?php if (is_access(49)): ?>
                                <li><a href="<?php echo site_url('report/report_customers'); ?>"><i class="fa fa-users"></i> Laporan Customer</a></li>
                            <?php endif; ?>

                            <?php if (is_access(32)): ?>
                                <li ><a href="<?php echo site_url('report/report_tax'); ?>"><i class="fa fa-file-o"></i> Laporan Pajak</a></li>
                            <?php endif; ?>

                            <?php if (is_access(33)): ?>
                                <li><a href="<?php echo site_url('report/report_sale'); ?>"><i class="fa fa-cubes"></i> Laporan Penjualan</a></li>
                            <?php endif; ?>

                            <?php if (is_access(34)): ?>
                                <li><a href="<?php echo site_url('report/report_logistic'); ?>"><i class="fa fa-thumbs-o-up"></i> Laporan Logistik</a></li>
                            <?php endif; ?>

                            <?php if (is_access(35)): ?>
                                <li style="display: none;" ><a href="<?php echo site_url('report/report_trip_plans'); ?>"><i class="fa fa-send-o"></i> Perjalanan</a></li>
                            <?php endif; ?>

                            <?php if (is_access(46)): ?>
                                <li ><a href="<?php echo site_url('report/report_gps'); ?>"><i class="fa fa-road"></i> Laporan GPS</a></li>
                            <?php endif; ?>

                        </ul>
                    </li>
                <?php endif; ?>

                <?php if (is_access_parent([8, 9])): ?>

                    <li class="header">DATA</li>

                    <?php if (is_access_parent([8, 9])): ?>
                        <li id="datamenu" class="treeview">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Customer Data</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <?php if (is_access(8)): ?>
                                    <li><a href="<?php echo site_url('customers/customers'); ?>"><i class="fa fa-user"></i>Semua Customer</a></li>
                                <?php endif; ?>
                                <?php if (is_access(37)): ?>
                                    <li><a href="<?php echo site_url('customers/customers_web'); ?>"><i class="fa fa-users"></i> Customer Web</a></li>
                                <?php endif; ?>
                                <?php if (is_access(9)): ?>
                                    <li><a href="<?php echo site_url('customers/customers/add'); ?>"><i class="fa fa-users"></i> Input Customer</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                <?php endif; ?>

                <?php if (is_access_parent([10, 11, 12, 13, 14, 15, 17, 47])): ?>

                    <li class="header">MANEJEMEN OPERASIONAL</li>

                    <?php if (is_access_parent([10, 11])): ?>
                        <li id="manifesting" class="treeview">
                            <a href="#">
                                <i class="fa fa-file-text-o"></i>
                                <span>Manifesting</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <?php if (is_access(10)): ?>
                                    <li><a href="<?php echo site_url('manifests/manifests'); ?>"><i class="fa fa-file"></i> Buat Daftar Paket</a></li>
                                <?php endif; ?>
                                <?php if (is_access(11)): ?>
                                    <li><a href="<?php echo site_url('manifests/manifests/pre_manifest_list'); ?>"><i class="fa fa-file-o"></i> Daftar Paket</a></li>
                                <?php endif; ?>
                                <?php if (is_access(6)): ?>
                                    <li><a href="<?php echo site_url('manifests/manifest'); ?>"><i class="fa fa-upload"></i> Loading List</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (is_access_parent([12, 13, 14, 15])): ?>
                        <li id="trip-manage" class="treeview">
                        
                            <a href="#">
                                <i class="fa fa-location-arrow"></i>
                                <span>Trip Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>

                            <ul class="treeview-menu">

                                <?php if (is_access(12)): ?>
                                    <li><a href="<?php echo site_url('trip/armadas'); ?>"><i class="fa fa-truck"></i> Kelola Kendaraan</a></li>
                                <?php endif; ?>

                                <?php if (is_access(13)): ?>
                                    <li><a href="<?php echo site_url('trip/routes'); ?>"><i class="fa fa-thumb-tack"></i> Kelola Rute</a></li>
                                <?php endif; ?>

                                <?php if (is_access(14)): ?>
                                    <li><a href="<?php echo site_url('trip/trips'); ?>"><i class="fa fa-compass"></i> Jadwal Perjalanan</a></li>
                                <?php endif; ?>

                                <?php if (is_access(15)): ?>
                                    <li style="display: none;"><a href="#"><i class="fa fa-globe"></i> Trip Report</a></li>
                                <?php endif; ?>

                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (is_access(17)): ?>
                        <li><a href="<?php echo site_url('traccar/traccars'); ?>"><i class="fa fa-map-marker"></i> <span>Traccar</span></a></li>
                    <?php endif; ?>

                    <?php if (is_access(47)): ?>
                        <li><a href="<?php echo site_url('traccar/track_resi'); ?>"><i class="fa fa-search"></i> <span>Lacak Resi</span></a></li>
                    <?php endif; ?>

                <?php endif; ?>


                <?php if (is_access_parent([18, 19, 20, 21, 22, 23, 24, 25, 26, 16])): ?>

                    <li class="header">MANAJEMEN SISTEM</li>
                    <?php if (is_access_parent([18, 19])): ?>
                        <li id="p_user" class="treeview">
                            <a href=""><i class="fa fa-user"></i> <span>Pengaturan User</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <?php if (is_access(18)): ?>
                                    <li><a href="<?php echo site_url('users/users'); ?>"><i class="fa fa-users"></i> Semua User</a></li>
                                <?php endif; ?>
                                <?php if (is_access(19)): ?>
                                    <li><a href="<?php echo site_url('users/users/add'); ?>"><i class="fa fa-user-plus"></i> Tambah Baru</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (is_access_parent([20, 21])): ?>
                        <li id="p_user_type" class="treeview">
                            <a href=""><i class="fa fa-users"></i> <span>Pengaturan Tipe User</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <?php if (is_access(20)): ?>
                                    <li><a href="<?php echo site_url('users/user_types'); ?>"><i class="fa fa-users"></i> Semua Tipe User</a></li>
                                <?php endif; ?>
                                <?php if (is_access(21)): ?>
                                    <li><a href="<?php echo site_url('users/user_types/add'); ?>"><i class="fa fa-user-plus"></i> Tambah Baru</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (is_access_parent([28, 29])): ?>
                        <li id="p_user_type" class="treeview">
                            <a href=""><i class="fa fa-recycle"></i> <span>Pengaturan Tipe Paket</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <?php if (is_access(28)): ?>
                                    <li><a href="<?php echo site_url('packages/package_types'); ?>"><i class="fa fa-recycle"></i> Semua Tipe Paket</a></li>
                                <?php endif; ?>
                                <?php if (is_access(29)): ?>
                                    <li><a href="<?php echo site_url('packages/package_types/add'); ?>"><i class="fa fa-plus-square"></i> Tambah Tipe Paket</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (is_access_parent([22, 23])): ?>
                        <li id="p_city" class="treeview">
                            <a href=""><i class="fa fa-map-marker"></i> <span>Pengaturan Kota</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <?php if (is_access(22)): ?>
                                    <li><a href="<?php echo site_url('cities/cities'); ?>"><i class="fa fa-map-marker"></i> Semua Kota</a></li>
                                <?php endif; ?>
                                <?php if (is_access(23)): ?>
                                    <li><a href="<?php echo site_url('cities/cities/add'); ?>"><i class="fa fa-plus"></i> Tambah Kota</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (is_access_parent([24, 25])): ?>
                        <li id="p_branch" class="treeview">
                            <a href=""><i class="fa fa-building"></i> <span>Pengaturan Cabang</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <?php if (is_access(24)): ?>
                                    <li><a href="<?php echo site_url('branch/branch'); ?>"><i class="fa fa-building"></i> Semua Cabang</a></li>
                                <?php endif; ?>
                                <?php if (is_access(25)): ?>
                                    <li><a href="<?php echo site_url('branch/branch/add'); ?>"><i class="fa fa-plus"></i> Tambah Cabang</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (is_access_parent([38, 39])): ?>
                        <li id="p_branch" class="treeview">
                            <a href=""><i class="fa fa-map"></i> <span>Pengaturan Regional</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <?php if (is_access(38)): ?>
                                    <li><a href="<?php echo site_url('branch/regionals'); ?>"><i class="fa fa-map"></i> Semua Regional</a></li>
                                <?php endif; ?>
                                <?php if (is_access(39)): ?>
                                    <li><a href="<?php echo site_url('branch/regionals/add'); ?>"><i class="fa fa-plus"></i> Tambah Regional</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (is_access(16)): ?>
                        <li id="p_price">
                            <a href="<?php echo site_url('prices/prices'); ?>">
                                <i class="fa fa-money"></i> <span>Kelola Harga</span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if (is_access(40)): ?>
                        <li id="p_price">
                            <a href="<?php echo site_url('orders/orders_approval'); ?>">
                                <i class="fa fa-check"></i> <span>Approval Edit Detail Order</span>
                                <?php if (get_notif_orders_approval()) {
                                    ?>
                                    <span class="pull-right badge bg-red">
                                        <?php echo get_notif_orders_approval(); ?>
                                    </span> 
                                    <?php 
                                }; ?>     
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if (is_access(26)): ?>
                        <li>
                            <a href="<?php echo site_url('program/edit'); ?>"><i class="fa fa-gear"></i> <span>Pengaturan Sistem</span></a>
                        </li>
                    <?php endif; ?>

                <?php endif; ?>
                <li class="header">SUPPORT</li>
                <li><a href="<?php echo site_url('program/forum'); ?>"><i class="fa fa-whatsapp"></i> <span>Forum Diskusi</span></a></li>
            <?php endif; ?>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>