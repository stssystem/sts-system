<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<style type="text/css">
    .unloadtype-class{
        margin-top: -23px;
    }
</style>
<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Pilih Armada</h3>
            </div>
            <div class="box-body">

                <div class="row">

                    <div class="col-md-12">
                        <div class="alert alert-success last-locaions" role="alert"> 
                            <strong><i class="fa fa-check"></i> Sukes!</strong> Informasi lokasi saat ini telah diperbarui 
                        </div>
                    </div>

                    <div class="col-md-9" >

                        <div class="row">
                            <div class="form-group armadas-select col-md-6">
                                <?php
                                echo form_dropdown('armada_id', get_armada(), '', 'class="form-control select2"');
                                ?>
                            </div>    
                            <div class="form-group col-md-6">
                                <?php
                                echo form_dropdown('branch_id', get_branch(), '', 'class="form-control select2"');
                                ?>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3">
                        <button  type="button" class="btn-update-location btn btn-info">Check In</button>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        /** Checkbox styling */
        $('input[name="unloading_type"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            increaseArea: '30%'
        });

        /** Select 2 */
        $(".select2").select2();

        $('.last-locaions').hide();

        /** Post update location */
        $('.btn-update-location ').click(function () {

            var armada_id = $('select[name="armada_id"]').val();
            var branch_id = $('select[name="branch_id"]').val();

            $.post('<?php echo site_url('orders/locations_update/add'); ?>',
                    {armada_id: armada_id, branch_id: branch_id}, function (data) {
                $('.last-locaions').show();

                setTimeout(function () {
                    $('.last-locaions').fadeOut('slow');
                }, 3000);

            });

        });

    });
</script>