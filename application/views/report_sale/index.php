<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a class="sale_report_tab" href="#tab_comission" data-value="1" data-toggle="tab" aria-expanded="false"> Laporan Komisi Agen</a></li>
                <li class=""><a class="sale_report_tab" href="#tab_payment" data-value="3" data-toggle="tab" aria-expanded="false"> Laporan Status Pembayaran</a></li>
            </ul>
            <div class="tab-content">
                
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="tab_comission">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->load->view('report_sale/tab_comission/filter'); ?>
                        </div>
                    </div>
                    <?php $this->load->view('report_sale/tab_comission'); ?>
                </div>
                
                <div class="tab-pane" id="tab_payment">
                    <?php $this->load->view('report_sale/tab_payment'); ?>
                </div>
                
            </div>
            <!-- /.tab-content -->
        </div>

    </div>
</div>

<?php $this->load->view('footer'); ?>


<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<?php $this->load->view('report_sale/js'); ?>
