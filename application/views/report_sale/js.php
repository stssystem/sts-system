<script type="text/javascript">

    $('a.sale_report_tab').on('shown.bs.tab', function () {
        var value = $(this).attr('data-value');
        $('.th-action').attr('style', 'width:75px;');
    });

    $('a.comission_tab').on('shown.bs.tab', function (e) {

        var value = $(this).attr('data-value');

        $('.th-action').attr('style', 'width:75px;');

        var href = "<?php echo site_url($path . '/download_comission'); ?>/" + value;

        $('.comission-download').attr('href', href);

    });

    $('.panel-comission').find();

    $(function () {
        // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTab');
        if (lastTab) {
            $('[href="' + lastTab + '"]').tab('show');
        }
    });

    var column_1 = [
        {data: 'branch_id'},
        {data: 'branch_name'},
        {data: 'total_order'},
        {data: 'total_omset'},
        {data: 'comission'},
        {data: 'action'}
    ];

    var column_2 = [
        {data: 'order_date'},
        {data: 'order_id'},
        {data: 'customer_name'},
        {data: 'branch_origin_name'},
        {data: 'branch_destination_name'},
        {data: 'order_payment_type'},
        {data: 'status'},
        {data: 'orders_due_date'},
        {data: 'action'}
    ];

    var columnDefs_1 = [
        {targets: [5], orderable: false},
    ];

    var columnDefs_2 = [
        {targets: [8], orderable: false},
    ];

    // Commision Report to Package Load
    $('.comission_up').DataTable({
        serverSide: true,
        order: [
            [
<?php dtorder_column('dt_package_load', 0); ?>,
                "<?php dtorder_mode('dt_package_load', 'asc'); ?>"
            ]
        ],
        displayStart: <?php dtarray('dt_package_load', 'start', 0); ?>,
        pageLength: <?php dtarray('dt_package_load', 'lenght', 10); ?>,
        search: {
            search: "<?php dtarray('dt_package_load', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>/index/1',
            type: 'POST'
        },
        columns: column_1,
        columnDefs: columnDefs_1,
        footerCallback: function (row, data, start, end, display) {

            // Get total from all page and update the footer
            $.get("<?php echo site_url($path . '/get_total_comission/2'); ?>", function (data) {

                // Variable initialization
                var result = JSON.parse(data);
                var parent_ = '.comission-package-load';

                // Set on web page
                $(parent_).find('.total-order').html(result['total_order']);
                $(parent_).find('.total-omset').html(result['total_omset']);
                $(parent_).find('.agent-comission').html(result['comission_agent']);

            });

        }
    });

    // Commision Report to Package Drop
    $('.comission_down').DataTable({
        serverSide: true,
        order: [
            [
                <?php dtorder_column('dt_package_drop', 0); ?>,
                "<?php dtorder_mode('dt_package_drop', 'asc'); ?>"
            ]
        ],
        displayStart: <?php dtarray('dt_package_drop', 'start', 0); ?>,
        pageLength: <?php dtarray('dt_package_drop', 'lenght', 10); ?>,
        search: {
            search: "<?php dtarray('dt_package_drop', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>/index/2',
            type: 'POST'
        },
        columns: column_1,
        columnDefs: columnDefs_1,
        footerCallback: function (row, data, start, end, display) {

            // Get total from all page and update the footer
            $.get("<?php echo site_url($path . '/get_total_comission/3'); ?>", function (data) {

                // Variable initialization
                var result = JSON.parse(data);
                var parent_ = '.comission-package-drop';

                // Set on web page
                $(parent_).find('.total-order').html(result['total_order']);
                $(parent_).find('.total-omset').html(result['total_omset']);
                $(parent_).find('.agent-comission').html(result['comission_agent']);

            });

        }
    });

    // Commision report for branch origin
    $('.branch_origin').DataTable({
        serverSide: true,
        order: [
            [
                <?php dtorder_column('dt_cms_branch_origin', 0); ?>,
                "<?php dtorder_mode('dt_cms_branch_origin', 'asc'); ?>"
            ]
        ],
        displayStart: <?php dtarray('dt_cms_branch_origin', 'start', 0); ?>,
        pageLength: <?php dtarray('dt_cms_branch_origin', 'lenght', 10); ?>,
        search: {
            search: "<?php dtarray('dt_cms_branch_origin', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>/index/4',
            type: 'POST'
        },
        columns: column_1,
        columnDefs: columnDefs_1,
        footerCallback: function (row, data, start, end, display) {

            // Get total from all page and update the footer
            $.get("<?php echo site_url($path . '/get_total_comission/1'); ?>", function (data) {

                // Variable initialization
                var result = JSON.parse(data);
                var parent_ = '.comission-branch-origin';

                // Set on web page
                $(parent_).find('.total-order').html(result['total_order']);
                $(parent_).find('.total-omset').html(result['total_omset']);
                $(parent_).find('.agent-comission').html(result['comission_agent']);

            });

        }
    });

    // Comission report for branch destination
    $('.branch_destination').DataTable({
        serverSide: true,
        order: [
            [
<?php dtorder_column('dt_cms_branch_destination', 0); ?>,
                "<?php dtorder_mode('dt_cms_branch_destination', 'asc'); ?>"
            ]
        ],
        displayStart: <?php dtarray('dt_cms_branch_destination', 'start', 0); ?>,
        pageLength: <?php dtarray('dt_cms_branch_destination', 'lenght', 10); ?>,
        search: {
            search: "<?php dtarray('dt_cms_branch_destination', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>/index/5',
            type: 'POST'
        },
        columns: column_1,
        columnDefs: columnDefs_1,
        footerCallback: function (row, data, start, end, display) {

            // Get total from all page and update the footer
            $.get("<?php echo site_url($path . '/get_total_comission/4'); ?>", function (data) {

                // Variable initialization
                var result = JSON.parse(data);
                var parent_ = '.comission-branch-destination';

                // Set on web page
                $(parent_).find('.total-order').html(result['total_order']);
                $(parent_).find('.total-omset').html(result['total_omset']);
                $(parent_).find('.agent-comission').html(result['comission_agent']);

            });

        }
    });

    // Payment report data
    $('.tab_payment').DataTable({
        serverSide: true,
        ordering: true,
        order: [
            [
                <?php dtorder_column('dt_report_order_payment', 0); ?>,
                "<?php dtorder_mode('dt_report_order_payment', 'asc'); ?>"
            ]
        ],
        displayStart: <?php dtarray('dt_report_order_payment', 'start', 0); ?>,
        pageLength: <?php dtarray('dt_report_order_payment', 'lenght', 10); ?>,
        search: {
            search: "<?php dtarray('dt_report_order_payment', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>/index/3',
            type: 'POST'
        },
        columns: column_2,
        columnDefs: columnDefs_2,
    });

    $('input[name="date_start"]').datepicker({format: 'dd/mm/yyyy'});
    $('input[name="date_end"]').datepicker({format: 'dd/mm/yyyy'});
    $('input[name="due_date"]').datepicker({format: 'dd/mm/yyyy'});

    $(".select2").select2();

</script>