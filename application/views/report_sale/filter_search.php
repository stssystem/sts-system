<div class="row">
    <?php echo form_open($path . '/filter'); ?>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Tanggal Awal</label>
            <input type="text" name="date_start_filter" class="form-control"
            value="<?php echo ($this->session->userdata('filter')['date_start']) ? $this->session->userdata('filter')['date_start'] : '' ?>"
            />
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Tanggal Akhir</label>
            <input type="text" name="date_end_filter" class="form-control"
            value="<?php echo ($this->session->userdata('filter')['date_end']) ? $this->session->userdata('filter')['date_end'] : '' ?>"
            />
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Asal</label>
            <!-- <select name="origin_filter" class="form-control select2"> -->
            <select name="origin_filter" class="form-control">
                <option value="0" >-- Pilih Cabang --</option>
                <?php if (!empty($cities)): ?>
                    <?php foreach ($cities as $key => $item): ?>
                        <?php if ($this->session->userdata('filter')['branch_origin'] && $this->session->userdata('filter')['branch_origin'] == $item['branch_id']): ?>
                            <option value="<?php echo $item['branch_id']; ?>" selected="selected"><?php echo $item['branch_name']; ?></option>
                        <?php else: ?>
                            <option value="<?php echo $item['branch_id']; ?>"><?php echo $item['branch_name']; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Tujuan</label>
            <!-- <select name="destination_filter" class="form-control select2"> -->
            <select name="destination_filter" class="form-control">
                <option value="0">-- Pilih Cabang --</option>
                <?php if (!empty($cities)): ?>
                    <?php foreach ($cities as $key => $item): ?>
                        <?php if ($this->session->userdata('filter')['branch_destination'] && $this->session->userdata('filter')['branch_destination'] == $item['branch_id']): ?>
                            <option value="<?php echo $item['branch_id']; ?>" selected="selected"><?php echo $item['branch_name']; ?></option>
                        <?php else: ?>
                            <option value="<?php echo $item['branch_id']; ?>"><?php echo $item['branch_name']; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="btn-group">
            <button id="filter" class="btn btn-filter btn-info"><i class="fa fa-filter"></i> Filter</button>
            <a href="<?php echo site_url($path . '/clear'); ?>" class="btn btn-warning"><i class="fa fa-eraser"></i> Reset</a>
        </div>
        <a href="<?php echo site_url($path . '/donwload'); ?>" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Download</a>
        <hr />
    </div>
    <?php echo form_close(); ?>
</div>