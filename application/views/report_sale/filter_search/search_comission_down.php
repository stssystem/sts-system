<?php echo form_open($path . '/filter/' . $branch_id); ?>


<div class="row">

    <?php
    echo $this->form->select(
        [
        'name' => 'origin_filter',
        'label' => 'Asal',
        'value' => get_branches_list(),
        'keys' => 'branch_id',
        'values' => 'branch_name',
        'class' => 'select2 col-md-3',
        'display_default' => true,
        'selected' => isset($this->session->userdata('comission_down')['branch_origin']) ? $this->session->userdata('comission_down')['branch_origin'] : ''
        ]
        );
        ?>

        <?php
        echo $this->form->select(
            [
            'name' => 'destination_filter',
            'label' => 'Tujuan',
            'value' => get_branches_list(),
            'keys' => 'branch_id',
            'values' => 'branch_name',
            'class' => 'select2 col-md-3',
            'display_default' => true,
            'selected' => isset($this->session->userdata('comission_down')['branch_destination_id']) ? $this->session->userdata('comission_down')['branch_destination_id'] : ''
            ]
            );
            ?>

            <?php
            echo $this->form->text(
                [
                'name' => 'date_start',
                'label' => 'Tanggal Mulai',
                'max' => 100,
                'type' => 'text',
                'class' => 'col-md-3',
                'display_default' => false,
                'value' => isset($this->session->userdata('comission_down')['date_start']) && $this->session->userdata('comission_down')['date_start'] != '' ? $this->session->userdata('comission_down')['date_start'] : ''
                ]
                );
                ?>

                <?php
                echo $this->form->text(
                    [
                    'name' => 'date_end',
                    'label' => 'Tanggal Akhir',
                    'max' => 100,
                    'type' => 'text',
                    'class' => 'col-md-3',
                    'display_default' => false,
                    'value' => isset($this->session->userdata('comission_down')['date_end']) && $this->session->userdata('comission_down')['date_end'] != '' ? $this->session->userdata('comission_down')['date_end'] : ''
                    ]
                    );
                    ?>

                </div>

                <div class="row">

                    <div class="col-md-3">
                    <a href="<?php echo site_url('report/report_sale'); ?>" class="btn btn-info">
                            <i class="fa fa-arrow-left"></i> Kembali
                        </a>
                    </div>

                    <div class="col-md-3 pull-right">
                        <div class="btn-grodown pull-right" >
                            <button type="submit" class="btn btn-info">
                                <i class="fa fa-filter"></i> Filter
                            </button>
                            <a href="<?php echo site_url($path . '/clear/' . $branch_id); ?>" class="btn btn-warning">
                                <i class="fa fa-refresh"></i> Reset
                            </a>
                        </div>
                    </div>

                </div>
                <?php echo form_close(); ?>