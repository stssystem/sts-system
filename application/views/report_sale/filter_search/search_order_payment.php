<div class="row">
    <?php echo form_open($path . '/filter_order_payment'); ?>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Tanggal Awal</label>
            <input type="text" name="date_start" class="form-control" placeholder="Masukan Tanggal Awal"
                   value="<?php echo ($this->session->userdata('report_filter_order_payment')['date_start']) ? $this->session->userdata('report_filter_order_payment')['date_start'] : '' ?>"
                   />
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Tanggal Akhir</label>
            <input type="text" name="date_end" class="form-control" placeholder="Masukan Tanggal Akhir"
                   value="<?php echo ($this->session->userdata('report_filter_order_payment')['date_end']) ? $this->session->userdata('report_filter_order_payment')['date_end'] : '' ?>"
                   />
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Cabang Asal</label><br/>
            <select name="origin_filter" class="form-control select2" style="width: 100%;">
                <option value="0" >-- Pilih Cabang --</option>
                <?php if (!empty($cities)): ?>
                    <?php foreach ($cities as $key => $item): ?>
                        <?php if ($this->session->userdata('report_filter_order_payment')['branch_origin'] && $this->session->userdata('report_filter_order_payment')['branch_origin'] == $item['branch_id']): ?>
                            <option value="<?php echo $item['branch_id']; ?>" selected="selected"><?php echo $item['branch_name']; ?></option>
                        <?php else: ?>
                            <option value="<?php echo $item['branch_id']; ?>"><?php echo $item['branch_name']; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Cabang Tujuan</label>
            <select name="destination_filter" class="form-control select2" style="width: 100%;" >
                <option value="0">-- Pilih Cabang --</option>
                <?php if (!empty($cities)): ?>
                    <?php foreach ($cities as $key => $item): ?>
                        <?php if ($this->session->userdata('report_filter_order_payment')['branch_destination'] && $this->session->userdata('report_filter_order_payment')['branch_destination'] == $item['branch_id']): ?>
                            <option value="<?php echo $item['branch_id']; ?>" selected="selected"><?php echo $item['branch_name']; ?></option>
                        <?php else: ?>
                            <option value="<?php echo $item['branch_id']; ?>"><?php echo $item['branch_name']; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Sistem Pembayaran</label>
            <?php ?>

            <select name="type_filter" class="form-control">
                <option value="0" >-- Pilih Pembayaran --</option>
                <option value="1" <?php if ($this->session->userdata('report_filter_order_payment')['type_filter'] && $this->session->userdata('report_filter_order_payment')['type_filter'] == 1) echo "selected='selected'"; ?> >Cash / Tunai</option>
                <option value="2" <?php if ($this->session->userdata('report_filter_order_payment')['type_filter'] && $this->session->userdata('report_filter_order_payment')['type_filter'] == 2) echo "selected='selected'"; ?> >Franko</option>
                <option value="3" <?php if ($this->session->userdata('report_filter_order_payment')['type_filter'] && $this->session->userdata('report_filter_order_payment')['type_filter'] == 3) echo "selected='selected'"; ?> >Tunai Tempo</option>
                <option value="4" <?php if ($this->session->userdata('report_filter_order_payment')['type_filter'] && $this->session->userdata('report_filter_order_payment')['type_filter'] == 4) echo "selected='selected'"; ?> >Tagih Tempo</option>
                <option value="5" <?php if ($this->session->userdata('report_filter_order_payment')['type_filter'] && $this->session->userdata('report_filter_order_payment')['type_filter'] == 5) echo "selected='selected'"; ?> >BDB</option>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Status</label>
            <select name="status_filter" class="form-control">
                <option value="0">-- Pilih Status --</option>
                <option value="1" <?php if ($this->session->userdata('report_filter_order_payment')['status_filter'] && $this->session->userdata('report_filter_order_payment')['status_filter'] == 1) echo "selected='selected'"; ?> >Lunas</option>
                <option value="2" <?php if ($this->session->userdata('report_filter_order_payment')['status_filter'] && $this->session->userdata('report_filter_order_payment')['status_filter'] == 2) echo "selected='selected'"; ?> >Belum Lunas</option>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Tanggal Jatuh Tempo</label>
            <input type="text" name="due_date" class="form-control" placeholder="Masukan Tanggal Jatuh Tempo"
                   value="<?php echo ($this->session->userdata('report_filter_order_payment')['due_date']) ? $this->session->userdata('report_filter_order_payment')['due_date'] : '' ?>"
                   />
        </div>
    </div>
    <div class="col-sm-3">
        <br />
        <div class="btn-group" style="margin-top: 5px;">
            <button id="filter" class="btn btn-filter btn-info"><i class="fa fa-filter"></i> Filter</button>
            <a href="<?php echo site_url($path . '/clear_order_payment'); ?>" class="btn btn-warning"><i class="fa fa-eraser"></i> Reset</a>
            <a href="<?php echo site_url($path . '/download_payment'); ?>" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Excel</a>
        </div>
    </div>
    <div class="col-sm-12">
        <hr />
        <br />
    </div>
    <?php echo form_close(); ?>
</div>