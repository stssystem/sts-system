<?php echo form_open($path . '/filter/' . $branch_id); ?>


<div class="row">

    <?php
    echo $this->form->select(
            [
                'name' => 'destination_filter',
                'label' => 'Tujuan',
                'value' => get_branches_list(),
                'keys' => 'branch_id',
                'values' => 'branch_name',
                'class' => 'select2 col-md-6',
                'display_default' => true,
                'selected' => isset($this->session->userdata('filter_report_sale_branch_origin')['branch_destination']) ? $this->session->userdata('filter_report_sale_branch_origin')['branch_destination'] : ''
            ]
    );
    ?>

    <?php
    echo $this->form->text(
            [
                'name' => 'date_start',
                'label' => 'Tanggal Mulai',
                'max' => 100,
                'type' => 'text',
                'class' => 'col-md-3',
                'display_default' => false,
                'value' => isset($this->session->userdata('filter_report_sale_branch_origin')['date_start']) && $this->session->userdata('filter_report_sale_branch_origin')['date_start'] != '' ? $this->session->userdata('filter_report_sale_branch_origin')['date_start'] :  date('01/m/Y')
            ]
    );
    ?>

    <?php
    echo $this->form->text(
            [
                'name' => 'date_end',
                'label' => 'Tanggal Akhir',
                'max' => 100,
                'type' => 'text',
                'class' => 'col-md-3',
                'display_default' => false,
                'value' => isset($this->session->userdata('filter_report_sale_branch_origin')['date_end']) && $this->session->userdata('filter_report_sale_branch_origin')['date_end'] != '' ? $this->session->userdata('filter_report_sale_branch_origin')['date_end'] : date('t/m/Y')
            ]
    );
    ?>

</div>

<div class="row">

    <div class="col-md-9">
        <a href="<?php echo site_url('report/report_sale'); ?>" class="btn btn-info">
            <i class="fa fa-arrow-left"></i> Kembali
        </a>
    </div>

    <div class="col-md-3">
        <div class="btn-group pull-right" >
            <button type="submit" class="btn btn-info">
                <i class="fa fa-filter"></i> Filter
            </button>
            <a href="<?php echo site_url($path . '/clear/' . $branch_id); ?>" class="btn btn-warning">
                <i class="fa fa-refresh"></i> Reset
            </a>
            <a href="<?php echo site_url($path . '/download/' . $branch_id); ?>" class="btn btn-success pull-right">
            <i class="fa fa-file-excel-o"></i> Download
        </a>
        </div>
    </div>
    
</div>
<?php echo form_close(); ?>