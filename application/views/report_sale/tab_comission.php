<div class="row">
    <div class="col-sm-12">

        <?php echo get_alert(); ?>

        <div class="nav-tabs-custom">

            <ul class="nav nav-tabs">
                <li class="active"><a class="comission_tab" href="#branch_origin" data-value="1" data-toggle="tab" aria-expanded="false"> Cabang / Agen Asal</a></li>
<!--                <li class=""><a class="comission_tab" href="#comission_up" data-value="2" data-toggle="tab" aria-expanded="false"> Paket Naik</a></li>
                <li class=""><a class="comission_tab" href="#comission_down" data-value="3" data-toggle="tab" aria-expanded="false"> Paket Turun</a></li>-->
                <li class=""><a class="comission_tab" href="#branch_destination" data-value="4" data-toggle="tab" aria-expanded="false"> Cabang / Agen Tujuan</a></li>
            </ul>

            <div class="tab-content panel-comission">

                <!-- tab 1 - paket asal -->
                <?php $this->load->view('report_sale/tab_comission/branch_origin'); ?>

                <!-- tab 2 - paket naik -->
                <?php #$this->load->view('report_sale/tab_comission/package_load'); ?>

                <!-- tab 3 - paket turun  -->
                <?php #$this->load->view('report_sale/tab_comission/package_drop'); ?>

                <!-- tab 4 - paket tujuan -->
                <?php $this->load->view('report_sale/tab_comission/branch_destination'); ?>

            </div>
            <!-- /.tab-content -->
        </div>

    </div>
</div>