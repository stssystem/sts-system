<div class="row">
    <div class="col-sm-12 table-responsive ">

        <?php $this->load->view('report_sale/filter_search/search_order_payment') ?>

        <?php echo get_alert(); ?>

        <table class="table table-gray table-hover tab_payment">
            <thead>
                <tr class="txt-sm">
                    <th width="10%">Tanggal</th>
                    <th width="8%">No Resi</th>
                    <th width="10%">Pengirim</th>
                    <th width="15%">Cabang/Agen Asal</th>
                    <th width="15%">Cabang/Agen Tujuan</th>
                    <th width="7%">Sistem Pembayaran</th>
                    <th width="5%">Status</th>
                    <th width="10%">Jatuh Tempo</th>
                    <th width="3%">Aksi</th>
                </tr>
            </thead>
        </table>

    </div>
</div> 

