<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/detailtables/detailTables.bootstrap.css"> -->
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
       <div class="nav-tabs-custom">

           <div class="col-md-12">
               <?php $this->load->view('report_sale/filter_search') ?>
           </div>

           <div class="tab-content">
            <!-- <table class="table table-bordered table-gray table-hover detail_orders"> -->
            <table class="table table-bordered table-gray table-hover">
                <thead>
                    <tr class="txt-sm">
                        <th width="10%">Tanggal Order</th>
                        <th width="10%"><strong>No Resi</strong></th>
                        <th width="15%">Pengirim</th>
                        <th width="15%">Agen Asal</th>
                        <th width="15%">Agen Tujuan</th>
                        <th width="15%">Omset Agen</th>
                        <th width="10%">Komisi Agen</th>
                        <th>Detail</th>
                        <!-- <th width="5%">Detail</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    if (!empty($detail)) {
                        foreach ($detail as $key => $value) {
                            ?>  
                            <tr>
                                <td><?php echo mdate('%d %M %Y %H:%i:%s', $value->order_date); ?></td>
                                <td><?php echo $value->order_id; ?></td>
                                <td><?php echo $value->customer_name; ?></td>
                                <td><?php echo $value->branch_name_origin; ?></td>
                                <td><?php echo $value->branch_name_destination; ?></td>
                                <td><?php echo (!empty($value->omset)) ? 'Rp. '.number_format($value->omset) : '' ; ?></td>
                                <td><?php echo 'Rp. '.number_format((20/100)*$value->omset) ?></td>
                                <td>
                                    <a href="<?php echo base_url('orders/order/view/'.$value->order_id); ?>"class="btn btn-info btn-sm">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                                <!-- <td>detail</td> -->
                            </tr>
                            <?php

                            $omset[]        = (!empty($value->omset)) ? $value->omset : '' ;
                            $comission[]    = (!empty($value->omset)) ? (20/100)*$value->omset : '';
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="7">
                                Data detail komisi agen tidak ditemukan
                            </td>
                        </tr>
                        <?php 
                    }
                    ?>
                </tbody>
                <tfoot>
                 <tr>
                     <td colspan="5" style="text-align: right;"><strong>Total :</strong></td>
                     <td><strong><?php echo (!empty($omset)) ? 'Rp. '.number_format(array_sum($omset)) : 'tidak ada omset'; ?></strong></td>
                     <td><strong><?php echo (!empty($comission)) ? 'Rp. '.number_format(array_sum($comission)) : 'tidak ada komisi' ?></strong></td>
                 </tr>                                   
             </tfoot>
         </table>

     </div>
 </div>
 <a href="<?php echo site_url($path); ?>" class="btn btn-primary" style="float: right;">
     Back
 </a>
</div>
</div>

<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript">
    $('input[name="date_start_filter"]').datepicker({ format: 'dd/mm/yyyy' });
    $('input[name="date_end_filter"]').datepicker({ format: 'dd/mm/yyyy' });

    $(".select2").select2();
</script>


