<div class="tab-pane table-responsive " id="branch_destination" style="margin-left: 6px;" >
    <div class="row" style="margin-left: 6px;" >
        <table class="table table-bordered table-gray table-hover branch_destination">
            <thead>
                <tr class="txt-sm">
                    <th width="3%"></th>
                    <th width="30%">Agen / Cabang</th>
                    <th width="10%">Total Order</th>
                    <th width="20%">Total Omset</th>
                    <th width="20%">Komisi Agen</th>
                    <th width="5%">Detail Komisi</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
                <tr class="comission-branch-destination" style="background-color: #bdbdbd;">
                    <th colspan="2">Total</th>
                    <th class="total-order"></th>
                    <th class="total-omset"></th>
                    <th class="agent-comission"></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>