<div class="row">
    <?php echo form_open($path . '/filter'); ?>
    <div class="col-sm-12" >
        <div class="btn-group">
            <a href="<?php echo site_url($path . '/clear_report_comission'); ?>" class="btn btn-warning"><i class="fa fa-eraser"></i> Reset</a>
            <a href="<?php echo site_url($path . '/download_comission/1'); ?>" class="btn btn-success pull-right comission-download"><i class="fa fa-file-excel-o"></i> Download</a>
        </div>
        <hr />
    </div>
    <?php echo form_close(); ?>

    <div class="col-md-12">
        <?php get_alert(); ?>
    </div>
</div>