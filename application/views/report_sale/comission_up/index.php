<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header" >

                <div class="row">
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <tr>
                                <th width="30%">Kode Cabang</th>
                                <th width="5%" >:</th>
                                <td width="65%" ><?php echo (!empty($data_branches->branch_code)) ? $data_branches->branch_code : '<span class="label label-danger">Cabang Tidak Terdaftar</span>' ?></td>
                            </tr>
                            <tr>
                                <th>Nama Cabang</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_branches->branch_name)) ? $data_branches->branch_name : '<span class="label label-danger">Cabang Tidak Terdaftar</span>' ?></td>
                            </tr>
                            <tr>
                                <th>Alamat Cabang</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_branches->branch_address)) ? $data_branches->branch_address : '<span class="label label-danger">Data Tidak Tersedia</span>' ?></td>
                            </tr>
                            <tr>
                                <th>Kota</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_branches->city_name)) ? $data_branches->city_name : '<span class="label label-danger">Data Tidak Tersedia</span>' ?></td>
                            </tr>
                            <tr>
                                <th>Regional</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_branches->regional)) ? $data_branches->regional : '<span class="label label-danger">Data Tidak Tersedia</span>' ?></td>
                            </tr>
                            <tr>
                                <th>Telepon/HP Cabang</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_branches->branch_phone)) ? $data_branches->branch_phone : '<span class="label label-danger">Data Tidak Tersedia</span>' ?></td>
                            </tr>
                            <tr>
                                <th>Email Cabang</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_branches->branch_email)) ? $data_branches->branch_email : '<span class="label label-danger">Data Tidak Tersedia</span>' ?></td>
                            </tr>
                            <tr>
                                <th>Cabang Induk</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_branches->branch_parent)) ? $data_branches->branch_parent : '<span class="label label-danger">Data Tidak Tersedia</span>' ?></td>
                            </tr>
                            <tr>
                                <th>Manager Cabang</th>
                                <th>:</th>
                                <td><?php echo (!empty($data_branches->userprofile_fullname)) ? $data_branches->userprofile_fullname : '<span class="label label-danger">Data Tidak Tersedia</span>' ?></td>
                            </tr>
                            <tr>
                                <th>Status Cabang</th>
                                <th>:</th>
                                <td>
                                    <?php
                                    if (!empty($data_branches->branch_status)) {
                                        echo ($data_branches->branch_status) ? '<span class="label label-success">Aktif</span>' : '<span class="label label-danger">Tidak Aktif</span>';
                                    } else {
                                        '<span class="label label-danger">Cabang Tidak Terdaftar</span>';
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <tr>
                                <th width="30%">Total Transaksi</th>
                                <th width="5%" >:</th>
                                <td width="65%" ><?php echo (!empty($value_branches->total_order)) ? $value_branches->total_order : '<span class="label label-danger">Transaksi Tidak Tersedia</span>' ?></td>
                            </tr>
                            <tr>
                                <th>Total Omset Cabang</th>
                                <th>:</th>
                                <td><?php echo (!empty($value_branches->total_omset)) ? '<span class="label label-success">Rp. ' . number_format($value_branches->total_omset) . '</span>' : '<span class="label label-danger">Tidak Ada Omset Cabang</span>'; ?></td>
                            </tr>
                            <tr>
                                <th>Total Komisi Cabang</th>
                                <th>:</th>
                                <td><?php echo (!empty($value_branches->total_omset)) ? '<span class="label label-success">Rp. ' . number_format((20 / 100) * $value_branches->total_omset) . '</span>' : '<span class="label label-danger">Tidak Ada Komisi Cabang</span>'; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>


                <hr/>

<?php $this->load->view('report_sale/filter_search/search_comission_up'); ?>

            </div>
            <div class="box-body table-responsive ">
<?php echo get_alert(); ?>

                <table class="table data-tables txt-sm">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="15%">Tanggal Transaksi</th>
                            <th width="15%">No Resi</th>
                            <th width="20%">Pengirim</th>
                            <th width="10%">Agen Asal</th>
                            <th width="10%">Agen Tujuan</th>
                            <th width="10%">Omset</th>
                            <th width="15%">Komisi</th>
                            <th width="5%">Detail Resi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot class="sale-report-detail-package-up" style="background-color: #bdbdbd;">
                    <th colspan="6">Total</th>
                    <th class="total-omset"></th>
                    <th class="total-comission"></th>
                    <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    /** Data tables set up */
    $('.data-tables').DataTable({
        serverSide: true,
        ordering: true,
        order: [
            [
<?php dtorder_column('dt_report_sale_comission_up_detail', 0); ?>,
                "<?php dtorder_mode('dt_report_sale_comission_up_detail', 'asc'); ?>"
            ]
        ],
        displayStart: <?php dtarray('dt_report_sale_comission_up_detail', 'start', 0); ?>,
        pageLength: <?php dtarray('dt_report_sale_comission_up_detail', 'lenght', 10); ?>,
        search: {
            search: "<?php dtarray('dt_report_sale_comission_up_detail', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path . "/index/" . $branch_id); ?>',
            type: 'POST',
            dataSrc: 'data',
        },
        columns: [
            {data: 'no'},
            {data: 'order_date'},
            {data: 'order_id'},
            {data: 'customer_name'},
            {data: 'branch_origin'},
            {data: 'branch_destination'},
            {data: 'omset'},
            {data: 'comission'},
            {data: 'action'}
        ],
        columnDefs: [
            {targets: [0], orderable: false},
            {targets: [8], orderable: false},
        ],  footerCallback: function (row, data, start, end, display) {

//            // Get total from all page and update the footer
//            $.get("<?php echo site_url($path . '/get_sum/' . $branch_id); ?>", function (data) {
//
////                console.log(data);
////                // Variable initialization
////                var result = JSON.parse(data);
////                var parent_ = '.sale-report-detail-package-up';
////
////              
////
////                // Set on web page
////                $(parent_).find('.total-omset').html(result['total_omset']);
////                $(parent_).find('.total-comission').html(result['comission_agent']);
//
//            });

        }
    }
    );

     // Form input styling
    $('input[name="date_start"]').datepicker({format: 'dd/mm/yyyy'});
    $('input[name="date_end"]').datepicker({format: 'dd/mm/yyyy'});
    $("select[name='destination_filter']").select2();

</script>
