<div class="modal fade" tabindex="-1" role="dialog" id="modal-update-alert">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open($path . '/update'); ?>
            <input name="id" type="hidden" />
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi Memperbarui Status : Siap Diambil</h4>
            </div>
            <div class="modal-body">
                <p>
                    Apakah Anda yakin memperbarui status order ini menjadi <strong>Siap Diambil</strong>?
                </p>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-ban"></i>
                        Batal
                    </button>
                    <button type="submit" class="btn btn-info alert-btn-save">
                        <i class="fa fa-check"></i>
                        Konfirmasi
                    </button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
