<style type="text/css">
    input[type=view] {
        border: 0px;
        border-radius: 0;

        -webkit-appearance: none;
    }
    textarea[type=view] {
        border: 0px;
        border-radius: 0;

        -webkit-appearance: none;
    }
</style>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-detail-alert" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><strong>Detail Paket</strong></h4>
    </div>

    <div class="modal-body">
        <div class="container-fluid bd-example-row">

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label  class="col-md-3 control-label">No Resi :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="no_resi" readonly="true" />
                        </div>
                    </div>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-md-6">
                    <h5><strong>Data Pengirim</strong></h5>
                    <table class="table">
                        <tr>
                            <td width="20%">
                                <label>Nama</label>
                            </td>
                            <td width="5%">:</td>
                            <td width="75%">
                                <input name="nama_pengirim" type="view" readonly="true"></input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Alamat</label>
                            </td>
                            <td>:</td>
                            <td>
                                <textarea name="alamat_pengirim" type="view" readonly="true"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>No Telp/HP</label>
                            </td>
                            <td>:</td>
                            <td>
                                <input name="telp_pengirim" type="view" readonly="true"></input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Email</label>
                            </td>
                            <td>:</td>
                            <td>
                                <input name="email_pengirim" type="view" readonly="true"></input>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="col-md-6">
                    <h5><strong>Data Penerima</strong></h5>
                    <table class="table">
                        <tr>
                            <td width="20%">
                                <label>Nama</label>
                            </td>
                            <td width="5%">:</td>
                            <td width="75%">
                                <input name="nama_penerima" type="view" readonly="true"></input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Alamat</label>
                            </td>
                            <td>:</td>
                            <td>
                                <textarea name="alamat_penerima" type="view" readonly="true"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>No Telp/HP</label>
                            </td>
                            <td>:</td>
                            <td>
                                <input name="telp_penerima" type="view" readonly="true"></input>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Email</label>
                            </td>
                            <td>:</td>
                            <td>
                                <input name="email_penerima" type="view" readonly="true"></input>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="row">
                    <div class="col-md-11">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label  class="col-md-3 control-label">Asal Paket :</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="kabupaten_pengirim" readonly="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-md-3 control-label">Tujuan Paket :</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="kabupaten_penerima" readonly="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-md-3 control-label">Jumlah Paket :</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="jumlah_paket" readonly="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-md-3 control-label">Total Harga Kalkulator :</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="total_harga" readonly="true" />
                                </div>
                            </div> 
                            <div class="form-group">
                                <label  class="col-md-3 control-label">Waktu Pengambilan :</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="waktu_pengambilan" readonly="true" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
    </div>
</div>
</div>
</div>

