<style type="text/css">
    @page {
        size: A5;
        margin:0;
    }

    @media print {
        html, body {
            width: 210mm;
            height: 297mm;

            margin-top: 20px;
            margin-bottom: 20px;
            margin-left: 20px;
            margin-right: 20px;

        }
        /* ... the rest of the rules ... */
    }

    .logo{
        height:60px;
        width:60px;
    }

    .address{
        font-size: 12px;
    }

    .noted{
        font-size: 13px;
    }


    .body-div{
        width: 195mm;
    }

    .aligns-bottom{
        vertical-align: bottom;
    }

    .aligns-right{
        text-align: left;
    }


</style>

<html>
    <body>
        <div class="body-div">
            <table>
                <tr>
                    <th>
                <div class="logo">
                    <img src="<?php echo base_url('assets/public/sts-logo.png'); ?>" class="logo">
                </div>
                </th>

                <td class="address" width="60%" >

                    <div>
                        PT. SANTOSO TEGUH SAKTI
                    </div>
                    <div>
                        JL. SOEKARNO HATTA 21 MAGELANG
                    </div>
                    <div>
                        TELP. 0293-364040
                    </div>
                </td>

                <td class="noted aligns-bottom" width="40%" >
                    Tanggal : <?php echo date('d F Y'); ?>

                    </tr>

            </table>
            <hr/>
            
            <br/>

            <table border="1" style="width: 195mm;">
                <thead>
                    <tr>
                        <th width="10%" >Kode</th>
                        <th width="15%" >Pengirim</th>
                        <th width="20%" >Alamat Pengirim</th>
                        <th width="20%" >Asal Paket</th>
                        <th width="20%" >Tujuan</th>
                        <th width="5%" >Jumlah Paket</th>
                        <th width="5%" >Total Harga Kalkulator</th>
                        <th width="5%" >Waktu Pengambilan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($print)): ?>
                        <?php foreach ($print as $key => $value): ?>

                            <?php
                            /** Sender tag format */
                            $html_sender = "<strong>$value->nama_pengirim</strong><br/>"
                                    . "$value->perusahaan <br/>"
                                    . "$value->email_pengirim<br/>"
                                    . "$value->telp_pengirim<br/>";

                            /** Sender address */
                            $html_address = "$value->alamat_pengirim, <br/>"
                                    . "$value->kelurahan_pengirim, <br/>"
                                    . "$value->kecamatan_pengirim, <br/>"
                                    . "$value->kabupaten_pengirim";
                            ?>
                            <tr>
                                <td><?php echo $value->no_resi; ?></td>
                                <td><?php echo $html_sender; ?></td>
                                <td><?php echo $html_address; ?></td>
                                <td><?php echo $value->asal; ?></td>
                                <td><?php echo $value->tujuan; ?></td>
                                <td><?php echo $value->jumlah_paket; ?></td>
                                <td><?php echo number_format($value->total_harga, 0, ',', '.'); ?></td>
                                <td><?php echo $value->waktu_pengambilan_tanggal . ' ' . $value->waktu_pengambilan_jam ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
            <br/>
        </div>
    </body>
</html>

<script type="text/javascript">
    window.print();
</script>

