<script type="text/javascript">

    $('a.pickup_tab').on('shown.bs.tab', function (e) {

        var value = $(this).attr('data-value');
        $('.th-action').attr('style', 'width:75px;');
        $('.new-table').find('.th-action').attr('style', 'width:75px;');

        /** Variable initiation */
        var target = $(e.target).attr("href");
        target = target.replace("#", "");
        $.session.set('tab-pickuporder', target);

    });

    // Initialization
    datatable();

    // Set default tab 
    if ($.session.get('tab-pickuporder') != '') {
        var href_ = '#' + $.session.get('tab-pickuporder');
        datatable();
        $('a[href="' + href_ + '"]').trigger('click');
    }

    function datatable() {

        $('.new-table').dataTable().fnDestroy();
        $('.pickup-table').dataTable().fnDestroy();
        $('.finish-table').dataTable().fnDestroy();

        $('.new-table').DataTable({
            ordering: true,
            serverSide: true,
            order: [
                [
                    <?php dtorder_column('dt_pickuporder', 0); ?>,
                    "<?php dtorder_mode('dt_pickuporder', 'desc'); ?>"
                ]
            ],
            displayStart: <?php dtarray('dt_pickuporder', 'start', 0); ?>,
            pageLength: <?php dtarray('dt_pickuporder', 'lenght', 10); ?>,
            search: {
                search: "<?php dtarray('dt_pickuporder', 'search', ''); ?>"
            },
            ajax: {
                url: '<?php echo site_url($path) ?>/index/0',
                type: 'POST'
            },
            columns: [
                {data: 'id'},
                {data: 'sender'},
                {data: 'sender_address'},
                {data: 'origin'},
                {data: 'destination'},
                {data: 'quantity'},
                {data: 'total'},
                {data: 'pickup_date'},
                {data: 'action'}
            ]
        });

        $('.pickup-table').DataTable({
            ordering: true,
            serverSide: true,
            order: [
                [
<?php dtorder_column('dt_pickuporder', 0); ?>,
                    "<?php dtorder_mode('dt_pickuporder', 'desc'); ?>"
                ]
            ],
            displayStart: <?php dtarray('dt_pickuporder', 'start', 0); ?>,
            pageLength: <?php dtarray('dt_pickuporder', 'lenght', 10); ?>,
            search: {
                search: "<?php dtarray('dt_pickuporder', 'search', ''); ?>"
            },
            ajax: {
                url: '<?php echo site_url($path) ?>/index/1',
                type: 'POST'
            },
            columns: [
                {data: 'id'},
                {data: 'sender'},
                {data: 'sender_address'},
                {data: 'origin'},
                {data: 'destination'},
                {data: 'quantity'},
                {data: 'total'},
                {data: 'pickup_date'},
                {data: 'action'}
            ]
        });

        $('.finish-table').DataTable({
            ordering: true,
            serverSide: true,
            order: [
                [
<?php dtorder_column('dt_pickuporder', 0); ?>,
                    "<?php dtorder_mode('dt_pickuporder', 'desc'); ?>"
                ]
            ],
            displayStart: <?php dtarray('dt_pickuporder', 'start', 0); ?>,
            pageLength: <?php dtarray('dt_pickuporder', 'lenght', 10); ?>,
            search: {
                search: "<?php dtarray('dt_pickuporder', 'search', ''); ?>"
            },
            ajax: {
                url: '<?php echo site_url($path) ?>/index/2',
                type: 'POST'
            },
            columns: [
                {data: 'id'},
                {data: 'sender'},
                {data: 'sender_address'},
                {data: 'origin'},
                {data: 'destination'},
                {data: 'quantity'},
                {data: 'total'},
                {data: 'pickup_date'},
                {data: 'action'}
            ]
        });

    }

    /** Delete */
    $('.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus order dari <strong>" + data['nama_pengirim'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);

        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });

    /** Approve order */
    $('body').on('click', '.btn-confirm', function () {
        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);
        $('#modal-update-alert').find('input[name="id"]').val(data['id']);
    });

    /** Approve 2 order */
    $('body').on('click', '.btn-confirm', function () {
        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);
        $('#modal-update2-alert').find('input[name="id"]').val(data['id']);
    });

    var data = 0;
    $('body').on('click', '.btn-views', function () {
        /** Get data */
        data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);
        // console.log(data);
    });

    $('#modal-detail-alert').on('shown.bs.modal', function (e) {
        $('input[name="no_resi"]').val(data['no_resi']);
        $('input[name="nama_pengirim"]').val(data['nama_pengirim']);
        $('textarea[name="alamat_pengirim"]').val(data['alamat_pengirim']);
        $('input[name="telp_pengirim"]').val(data['telp_pengirim']);
        $('input[name="email_pengirim"]').val(data['email_pengirim']);
        $('input[name="nama_penerima"]').val(data['nama_penerima']);
        $('textarea[name="alamat_penerima"]').val(data['alamat_penerima']);
        $('input[name="telp_penerima"]').val(data['telp_penerima']);
        $('input[name="email_penerima"]').val(data['email']);
        $('input[name="kabupaten_pengirim"]').val(data['kabupaten_pengirim']);
        $('input[name="kabupaten_penerima"]').val(data['kabupaten_penerima']);
        $('input[name="jumlah_paket"]').val(data['jumlah_paket']);
        $('input[name="total_harga"]').val('Rp. ' + data['total_harga']);
        $('input[name="waktu_pengambilan"]').val(data['waktu_pengambilan_tanggal'] + ' - ' + data['waktu_pengambilan_jam']);
        $('input[name="metode_pembayaran"]').val(data['metode_pembayaran']);
        $('input[name="pickup_status"]').val(data['pickup_status']);
        $('input[name="status"]').val(data['status']);
    });

    /** Print out */
    $('.btn-print').click(function (e) {
        e.preventDefault();
        targets = $(this).attr('href');
        $("<iframe>")                             // create a new iframe element
                .hide()                               // make it invisible
                .attr("src", targets) // point the iframe to the page you want to print
                .appendTo("body");
    });

    setTimeout(function () {
        $('.alert-dismissable').fadeOut('slow');
    }, 3000);

    $('.btn-clear').click(function () {
        $.session.remove('tab-pickuporder');
    });


</script>