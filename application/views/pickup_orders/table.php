<table class="table table-striped data-tables txt-sm <?php echo $tbclass; ?>">
    <thead>
        <tr>
            <th width="5%" >Kode</th>
            <th width="20%" >Pengirim</th>
            <th width="15%" >Alamat Pengirim</th>
            <th width="15%" >Asal Paket</th>
            <th width="15%" >Tujuan</th>
            <th width="5%" >Jumlah Paket</th>
            <th width="5%" >Total Harga Kalkulator</th>
            <th width="5%" >Waktu Pengambilan</th>            
            <th width="15%" class="th-action">Aksi</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>