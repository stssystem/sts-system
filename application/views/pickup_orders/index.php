<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>
<?php $this->load->view('pickup_orders/alert-update'); ?>
<?php $this->load->view('pickup_orders/alert-update2'); ?>
<?php $this->load->view('pickup_orders/alert-detail'); ?>

<div class="row">
    <div class="col-sm-12">

        <?php echo get_alert(); ?>

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a class="pickup_tab" href="#tab_new" data-value="1" data-toggle="tab" aria-expanded="false"> Baru Diterima</a></li>
                <li class=""><a class="pickup_tab" href="#tab_pickup" data-value="2" data-toggle="tab" aria-expanded="false"> Siap Diambil</a></li>
                <li class=""><a class="pickup_tab" href="#tab_finish" data-value="3" data-toggle="tab" aria-expanded="false"> Telah Diambil</a></li>
                <li class="pull-right">
                    <div >
                        <a href="<?php echo site_url($path) ?>/printout" class="btn btn-danger btn-print">
                            <i class="fa fa-file-pdf-o"></i> Cetak Order Siap Ambil
                        </a>
                        <a href="<?php echo site_url($path) ?>/reset" class="btn btn-warning btn-clear">
                            <i class="fa fa-refresh"></i> Reset
                        </a>
                    </div>
                </li>
            </ul>
            <div class="tab-content">
                <!-- /.tab-pane -->
                <div class="tab-pane table-responsive active" id="tab_new">
                    <?php $this->load->view('pickup_orders/table', ['tbclass' => 'new-table']); ?>
                </div>
                <div class="tab-pane table-responsive " id="tab_pickup">
                    <?php $this->load->view('pickup_orders/table', ['tbclass' => 'pickup-table']); ?>
                </div>
                <div class="tab-pane table-responsive " id="tab_finish">
                    <?php $this->load->view('pickup_orders/table', ['tbclass' => 'finish-table']); ?>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>

    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
<?php $this->load->view('pickup_orders/js'); ?>