<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header" >

                <div class="row">
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <tr>
                                <th width="30%">Nomor ID</th>
                                <th width="5%" >:</th>
                                <td width="65%" ><?php echo isset($data_customer->customer_id) ? $data_customer->customer_id : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>Nomor Referal</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->customer_referral) ? $data_customer->customer_referral : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>Nama Kontak</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->customer_name) ? $data_customer->customer_name : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>Tanggal Lahir</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->customer_birth_date) ? mdate('%d %M %Y', $data_customer->customer_birth_date) : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->customer_address) ? $data_customer->customer_address : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>Telepon</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->customer_phone) ? $data_customer->customer_phone : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>Nama Usaha</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->customer_company_name) ? $data_customer->customer_company_name : ' - '; ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <tr>
                                <th width="30%">Kota</th>
                                <th width="5%" >:</th>
                                <td width="65%" ><?php echo isset($data_customer->city_name) ? $data_customer->city_name : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>Cabang Pendaftar</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->branch_name) ? $data_customer->branch_name : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>Nama Usaha</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->customer_company_name) ? $data_customer->customer_company_name : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>Alamat Usaha</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->customer_company_address) ? $data_customer->customer_company_address : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>No Telepon Usaha</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->customer_company_telp) ? $data_customer->customer_company_telp : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->customer_company_email) ? $data_customer->customer_company_email : ' - '; ?></td>
                            </tr>
                            <tr>
                                <th>Website</th>
                                <th>:</th>
                                <td><?php echo isset($data_customer->customer_company_website) ? $data_customer->customer_company_website : ' - '; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <hr/>
                <?php echo form_open($path . '/filter/' . $customers_id); ?>


                <div class="row">

                    <?php
                    echo $this->form->select(
                            [
                                'name' => 'branch_origin',
                                'label' => 'Asal',
                                'value' => get_branches_list(),
                                'keys' => 'branch_id',
                                'values' => 'branch_name',
                                'class' => 'select2 col-md-3',
                                'display_default' => true,
                                'selected' => isset($this->session->userdata('customers_report_detail_filter')['branch_origin_id']) ? $this->session->userdata('customers_report_detail_filter')['branch_origin_id'] : ''
                            ]
                    );
                    ?>

                    <?php
                    echo $this->form->select(
                            [
                                'name' => 'branch_destination',
                                'label' => 'Tujuan',
                                'value' => get_branches_list(),
                                'keys' => 'branch_id',
                                'values' => 'branch_name',
                                'class' => 'select2 col-md-3',
                                'display_default' => true,
                                'selected' => isset($this->session->userdata('customers_report_detail_filter')['branch_destination_id']) ? $this->session->userdata('customers_report_detail_filter')['branch_destination_id'] : ''
                            ]
                    );
                    ?>

                    <?php
                    echo $this->form->text(
                            [
                                'name' => 'start_date',
                                'label' => 'Tanggal Mulai',
                                'max' => 100,
                                'type' => 'text',
                                'class' => 'col-md-3',
                                'display_default' => false,
                                'value' => isset($this->session->userdata('customers_report_detail_filter')['date_start']) && $this->session->userdata('customers_report_detail_filter')['date_start'] != '' ? mdate('%d/%m/%Y', $this->session->userdata('customers_report_detail_filter')['date_start']) : date('01-m-Y')
                            ]
                    );
                    ?>

                    <?php
                    echo $this->form->text(
                            [
                                'name' => 'end_date',
                                'label' => 'Tanggal Akhir',
                                'max' => 100,
                                'type' => 'text',
                                'class' => 'col-md-3',
                                'display_default' => false,
                                'value' => isset($this->session->userdata('customers_report_detail_filter')['date_end']) && $this->session->userdata('customers_report_detail_filter')['date_end'] != '' ? mdate('%d/%m/%Y', $this->session->userdata('customers_report_detail_filter')['date_end']) : date('t-m-Y')
                            ]
                    );
                    ?>

                </div>
                <div class="row">

                    <div class="col-md-4">
                        <div class="btn-group">
                            <a href="<?php echo site_url('report/report_customers'); ?>" class="btn btn-info">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                            <a href="<?php echo site_url($path.'/download/'.$customers_id); ?>" class="btn btn-success">
                                <i class="fa fa-file-excel-o"></i> Download
                            </a>
                        </div>
                    </div>

                    <div class="col-md-3 pull-right">
                        <div class="btn-group pull-right" >
                            <button type="submit" class="btn btn-info">
                                <i class="fa fa-filter"></i> Filter
                            </button>
                            <a href="<?php echo site_url($path . '/clear/' . $customers_id); ?>" class="btn btn-warning">
                                <i class="fa fa-refresh"></i> Reset
                            </a>
                        </div>
                    </div>

                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="box-body">
                <?php get_alert(); ?>

                <table class="table data-tables txt-sm">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="15%">Tanggal Transaksi</th>
                            <th width="15%">No Resi</th>
                            <th width="20%">Asal - Tujuan</th>
                            <th width="10%">Jumlah Koli / Paket</th>
                            <th width="10%">Total Berat <br/>( Kg )</th>
                            <th width="10%">Total Volume <br/>( M<sup>3</sup> )</th>
                            <th width="15%">Total Transaksi</th>
                            <!--<th width="5%">Action</th>-->
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot style="background-color: #d1d1d1;" >
                        <tr>
                            <th colspan="7"  style="font-size: 16px;" >Total</th>
                            <th style="font-size: 16px;" ></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Numeral -->
<script src="<?php echo base_url(); ?>assets/plugins/numeral/numeral.min.js"></script>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    $('select[name="branch_origin"]').select2();
    $('select[name="branch_destination"]').select2();
    $('input[name="start_date"]').datepicker({format: 'dd/mm/yyyy'});
    $('input[name="end_date"]').datepicker({format: 'dd/mm/yyyy'});

    /** Data tables set up */
    $('.data-tables').DataTable({
        serverSide: true,
        ajax: {
            url: '<?php echo site_url($path . "/index/" . $customers_id); ?>',
            type: 'POST'
        },
        columns: [
            {data: 'no'},
            {data: 'order_date'},
            {data: 'order_id'},
            {data: 'branch_name'},
            {data: 'total_koli'},
            {data: 'total_weight'},
            {data: 'total_size'},
            {data: 'order_sell_total'}
        ], footerCallback: function (row, data, start, end, display) {

            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                        i.replace(/[\$.]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                    .column(7)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

            // Update footer
            $(api.column(7).footer()).html(
                    'Rp ' + numeral(total).format('0,0')
                    );

        }
    }
    );

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus pelanggan <strong>" + data['customer_name'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);
        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['customer_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });

</script>