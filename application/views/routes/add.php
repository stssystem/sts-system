<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4 class="cek"><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open_multipart($path . '/store', ['class' => 'form-add']); ?>
                <input type="hidden" name="det_route" class='det-route' value="<?php echo old_input('det_route'); ?>" />

                <div class="row">
                    <div class="col-md-6">

                        <?php
                        echo $this->form->text(
                                [
                                    'name' => 'route_name',
                                    'label' => 'Nama Rute',
                                    'max' => 100,
                                    'required' => true,
                                    'type' => 'text'
                                ]
                        );
                        ?>

                        <?php
                        echo $this->form->text(
                                [
                                    'name' => 'route_status',
                                    'label' => 'Status',
                                    'required' => true,
                                    'type' => 'radio',
                                    'class' => '',
                                    'value' => ['1' => 'Aktif', '0' => 'Tidak Aktif'],
                                    'checked' => ['1']
                                ]
                        );
                        ?>

                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <?php
                            echo $this->form->select([
                                'name' => 'route_det',
                                'label' => 'Cabang',
                                'value' => $branches,
                                'keys' => 'branch_id',
                                'values' => 'branch_name',
                                'class' => 'select2 col-md-8'
                            ]);
                            ?>
                            <button 
                                type="button" 
                                class="btn-info btn col-md-2 btn-flat btn-add-city" 
                                style="margin-top: 25px;"
                                > 
                                <i class="fa fa-plus"></i>
                                Tambah
                            </button>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-cities">
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url($path); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <a type="button" class="btn-info btn btn-save" data-toggle="modal" data-target="#modal-save-alert">
                            <i class="fa fa-save"></i>
                            Simpan
                        </a>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">

    $('.btn-save').click(function () {

        var route_name = $('.form-add').find('input[name="route_name"]').val();

        if (route_name == '') {

            /** Set message */
            var text = "Kolom nama rute wajib diisi";

        } else {

            /** Set message */
            var text = "Apakah Anda akan menyimpan rute <strong>" + route_name + "</strong>?";

        }

        /** Set modal */
        $('#modal-save-alert').find('.modal-body p').html(text);

    });

    $('.alert-btn-save').click(function () {
        $('.form-add').submit();
    });

    /** Select 2 */
    $(".select2").find('select').select2();

    /** City rute */
    var tempData = [];
    var result_data = '';

    /** Re-populate city */
    if ($('.det-route').val() != '') {
        var det_routes = $('.det-route').val();
        det_routes = JSON.parse(det_routes);

        tempData = [];
        $.each(det_routes, function (index, value) {
            var text = $('select[name="route_det"]').find('option[value="' + value['city_id'] + '"]').text();

            var html = '<tr><td width="80%">' + text + '</td>';
            html += '<td width="20%">';
            html += '<input type="hidden" class="city_id_class" name ="city_id" value="' + value['city_id'] + '" />';
            html += '<input type="hidden" name ="order" />';
            html += '<button class="btn btn-danger pull-right btn-city-delete">';
            html += '<i class="fa fa-trash"></i>';
            html += '</button></td></tr>';

            $('.table-cities tbody').append(html);

            var person = {city_id: value['city_id']};
            tempData.push(person);

        });

    }

    /** Add city */
    $('.btn-add-city').click(function () {

        var city_id = $('select[name="route_det"] option:selected').val();
        var city = $('select[name="route_det"] option:selected').text();

        if (city_id !== '') {

            var html = '<tr><td width="80%">' + city + '</td>';
            html += '<td width="20%">';
            html += '<input type="hidden" class="city_id_class" name ="city_id" value="' + city_id + '" />';
            html += '<input type="hidden" name ="order" />';
            html += '<button class="btn btn-danger pull-right btn-city-delete">';
            html += '<i class="fa fa-trash"></i>';
            html += '</button></td></tr>';

            $('.table-cities tbody').append(html);

            var person = {city_id: city_id};
            tempData.push(person);
            result_data = JSON.stringify(tempData);
            $('.det-route').attr('value', result_data);
        }

    });

    /** Delete cities */
    $('.table-cities').on('click', '.btn-city-delete', function () {

        $(this).parent().parent().remove();

        tempData = [];
        $('.city_id_class').each(function () {
            var person = {city_id: $(this).val()};
            tempData.push(person);
        });
        result_data = JSON.stringify(tempData);
        $('.det-route').attr('value', result_data);

    });

    /** Sort event */
    $(".table-cities tbody").sortable();
    $(".table-cities tbody").disableSelection();
    $(".table-cities tbody").on("sortstop", function (event, ui) {
        tempData = [];
        $('.city_id_class').each(function () {
            var person = {city_id: $(this).val()};
            tempData.push(person);
        });
        result_data = JSON.stringify(tempData);
        $('.det-route').attr('value', result_data);

    });

    $('.cek').click(function () {
        console.log(result_data);
    });

</script>