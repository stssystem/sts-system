<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4><?php echo $title; ?></h4>
                    </div>
                    <div class="col-md-6">
                        <div class="box-tools pull-right">
                            <a href="<?php echo site_url($path . '/add') ?>" class="btn btn-success">
                                <i class="fa fa-plus"></i> 
                                Tambah Rute
                            </a>
                            <a href="<?php echo site_url($path . '/reset') ?>" class="btn btn-warning" >
                                <i class="fa fa-refresh"></i> 
                                Reset
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive ">
                <?php get_alert(); ?>

                <table class="table data-tables">
                    <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th width="50%">Rute Name</th>
                            <th width="15%">Status</th>
                            <th width="20%">Dibuat</th>
                            <th width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>	
                </table>
            </div>
        </div>
    </div>
</div>



<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    /** Data tables set up */
    $('.data-tables').DataTable({
        serverSide: true,
        ordering: true,
          order: [
            [
                <?php dtorder_column('dt_routes', 0); ?>, 
                "<?php dtorder_mode('dt_routes', 'asc'); ?>"
            ]
        ],
        displayStart : <?php dtarray('dt_routes','start', 0); ?>,
        pageLength : <?php dtarray('dt_routes', 'lenght', 10); ?>,
        search: {
           search: "<?php dtarray('dt_routes', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>',
            type: 'POST'
        },
        columns: [
            {data: 'route_id'},
            {data: 'route_name'},
            {data: 'route_status'},
            {data: 'created_at'},
            {data: 'action'}
        ]
    }
    );

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus data rute <strong>" + data['route_name'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);
        
        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['route_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    })



</script>