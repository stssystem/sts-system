<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/easy-autocomplete/easy-autocomplete.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jn-input-mask/css/jasny-bootstrap.css">

<style type="text/css">

    .img-delete:hover{
        cursor:pointer;
    }

    .text-sm{
        font-size: 13px;
    }

    .text-sm-table tr td{
        font-size: 13px;
    }

    .package-actions tr{
        cursor: pointer;
    }

    .orderextra-actions tr{
        cursor: pointer;
    }

    .rows{
        background-color: #B1D1EC;
    }

    .hide-click{
        display: none;
    }

</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>
<?php $this->load->view('component/modal/alert-package-validation'); ?>
<?php $this->load->view('orders/modal/package_size'); ?>
<?php $this->load->view('orders/modal/package_note'); ?>

<div class="row" >

    <?php echo form_open($path . '/complete_store', 'novalidate class="form-add"'); ?>
    <!-- anchor trigger  -->
    <a class="package-validation-trigger" data-toggle="modal" data-target="#modal-package-validation"></a>
    <a class="extra-order-validation-trigger" data-toggle="modal" data-target="#modal-extra-order-validation"></a>

    <!-- Load hidden input that contain basic information -->
    <?php $this->load->view('orders/component/hidden_input', ['order_id' => $order_id]); ?>

    <div class="col-sm-12">

        <?php $this->load->view('orders/component/required_alert'); ?>

        <!-- Load hidden input that contain basic information -->
        <?php $this->load->view('orders/component/general'); ?>

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_customer" data-toggle="tab" aria-expanded="false"><span class="lead">1</span> Data Customer</a></li>
                <li class=""><a href="#tab_pengiriman" data-toggle="tab" aria-expanded="false"><span class="lead">2</span> Data Pengiriman</a></li>
                <li class=""><a href="#tab_paket" data-toggle="tab" aria-expanded="false"><span class="lead">3</span> Data Paket</a></li>
                <li class=""><a href="#tab_extra" data-toggle="tab" aria-expanded="false"><span class="lead">4</span> Data Tambahan / Extra</a></li>
                <li class="pull-right">
                    <h4>
                        <label class="label label-warning">
                            <i class="fa fa-calendar"></i>
                            <?php echo date('d F Y'); ?>
                        </label>
                    </h4>
                </li>
            </ul>
            <div class="tab-content">
                <!-- /.tab-pane -->
                <?php $this->load->view('orders/component/tab/customers'); ?>
                <?php $this->load->view('orders/component/tab/delivery', ['order_id' => $order_id]); ?>
                <?php $this->load->view('orders/component/tab/packages', ['package_types' => $package_types]); ?>
                <?php $this->load->view('orders/component/tab/orders_extra'); ?>
            </div>
            <!-- /.tab-content -->
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<?php $this->load->view('footer'); ?>
<?php $this->load->view('orders/js'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('input[name="det_order_qty"]').blur(function () {
            sum_package_weight();
        });
    });
</script>

