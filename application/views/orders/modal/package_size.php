<div class="modal fade" tabindex="-1" role="dialog" id="modal-package-size">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Masukkan Dimensi Paket</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label>Panjang</label>
                        <input type="number" name="package_lenght" class="form-control input-price-params"  min="0"/>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Lebar</label>
                        <input type="number" name="package_width" class="form-control input-price-params" min="0" />
                    </div>
                    <div class="form-group col-md-4">
                        <label>Tinggi</label>
                        <input type="number" name="package_height" class="form-control input-price-params"  min="0" />
                    </div>
                </div>
                <div class="row">
                    <?php
                    echo $this->form->text(
                            [
                                'name' => 'unit',
                                'label' => 'Satuan',
                                'max' => 100,
                                'required' => false,
                                'value' => [2 => 'Cm ( Sentimeter )', 1 => 'M ( Meter )'],
                                'rows' => 3,
                                'class' => 'col-md-12',
                                'type' => 'radio',
                                'checked' => [2]
                            ]
                    );
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-ban"></i>
                        Batal
                    </button>
                    <button class="btn btn-success btn-enter-volume" data-dismiss="modal" >
                        <i class="fa fa-check"></i>
                        Masukkan
                    </button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
