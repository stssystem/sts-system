<?php $this->load->view('header');?>
	<div class="row">
		<div class="col-sm-3">
			
			<div class="box box-primary">
				
		        <div class="box-body box-profile">
	              
	              	<h3 class="profile-username text-center">Info Order</h3>

	              

		             <ul class="list-group list-group-unbordered">
		                <li class="list-group-item">
		                	<b>No Resi</b> <a class="pull-right">R 12345</a>
		                </li>

		                <li class="list-group-item">
		                	<b>No Resi Manual</b> <a class="pull-right">-</a>
		                </li>
		                
		                <li class="list-group-item">
		                  	<b>Tanggal</b> <a class="pull-right">21 Januari 2016</a>
		                </li>
						
						<li class="list-group-item">
							<b>Status</b> <a class="pull-right">Baru Diterima</a>
						</li>

						<li class="list-group-item">
							<b>Metode Pembayaran</b> <a class="pull-right">Tunai</a>
						</li>

						<li class="list-group-item">
							<b>Dibuat Oleh</b> <a class="pull-right">Agung</a>
						</li>
						<p>Catatan</p>
		            </ul>
		           
		            
				
	             
            	</div>
			</div>

			<div class="box box-danger">
				
				<div class="box-body">
					
					<div class="row">
						<div class="col-sm-12">
							<a href="#" target="_blank" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali Ke Daftar Order</a>
							<a href="#" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i> Cetak Resi</a>
						
	         			</div>
         			</div>
				</div>
			</div>
		</div>
		<div class="col-sm-9">
			<div class="row">
				<div class="col-sm-6">
					<div class="box box-primary">
				
				        <div class="box-body box-profile">
			              
			              	<h3 class="profile-username text-center">Pengirim (Asal)</h3>

			              

				             <ul class="list-group list-group-unbordered">
				                <li class="list-group-item">
				                	<b>Kota</b> <a class="pull-right">Bandung</a>
				                </li>
				                
				                <li class="list-group-item">
				                  <b>Kecamatan</b> <a class="pull-right">Soreang</a>
				                </li>
								
								<li class="list-group-item">
									<b>Desa/Kelurahan</b> <a class="pull-right">Beji</a>
								</li>

								<li class="list-group-item">
									<b>Dari Cabang</b> <a class="pull-right">Bandung 1</a>
								</li>

				            </ul>
				           
				            <p>Jl. Duku Nomor 5, Kelurahan Bajik, Sorengan</p>
						
			             
		            	</div>
					</div>
				</div>
				<div class="col-sm-6">

					<div class="box box-success">
				        <div class="box-body box-profile">
			              
			              	<h3 class="profile-username text-center">Penerima (Tujuan)</h3>

			              

				             <ul class="list-group list-group-unbordered">
				                <li class="list-group-item">
				                	<b>Kota</b> <a class="pull-right">Semarang</a>
				                </li>
				                
				                <li class="list-group-item">
				                  <b>Kecamatan</b> <a class="pull-right">Gunung Pati</a>
				                </li>
								
								<li class="list-group-item">
									<b>Desa/Kelurahan</b> <a class="pull-right">Sukorejo</a>
								</li>

								<li class="list-group-item">
									<b>Ke Cabang</b> <a class="pull-right">Semarang</a>
								</li>

				            </ul>
				           
				            <p>Jl. Dewi Sartika Timur V Perum A5</p>
						
			             
		            	</div>
					</div>
				</div>
			</div>
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Paket</h3>
				</div>
				<div class="box-body">
					

					<table class="table">
						<thead>
							<tr><th>No.</th><th>Deskripsi</th><th>PxLxT</th><th>Volume</th><th>Berat</th><th class="text-right">PriceList</th><th class="text-right">Biaya</th><th></th></tr>
						</thead>
						<tbody>
							<tr><td>1</td><td>Kacang Garam</td><td>1x2x3 cm</td><td>22cm3</td><td>20kg</td><td class="text-right">80.000</td><td class="text-right">90.000</td><td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-print"></i></a></td></tr>
							<tr><td>2</td><td>Handphone Samsung</td><td>1x2x3 cm</td><td>22cm3</td><td>20kg</td><td class="text-right">90.000</td><td class="text-right">100.000</td><td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-print"></i></a></td></tr>
							<tr><td class="text-right" colspan="5">Total</td><td colspan="2" class="text-right"><strong>190.000</strong></td><td></td></tr>
							<tr><td class="text-right" colspan="5">Pajak</td><td colspan="2" class="text-right"><strong>19.000</strong></td><td></td></tr>
							<tr><td class="text-right" colspan="5">Total Akhir</td><td colspan="2" class="text-right"><strong>209.000</strong></td><td></td></tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Extra</h3>
				</div>
				<div class="box-body">
					<table class="table">
						<thead>
							<tr><th>No.</th><th>Nama Extra</th><th class="text-right">Total</th></tr>
						</thead>
						<tbody>
							<tr><td>1</td><td>Antar Dirumah</td><td class="text-right">100.000</td></tr>
							<tr><td colspan="2">Total</td><td class="text-right"><strong>100.000</strong></td></tr>
						</tbody>
					</table>
				</div>
			</div>

			
		</div>
	</div>

<?php $this->load->view('footer');?>