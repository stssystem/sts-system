<div class="box" style="margin-bottom: 0px;">
    <div class="box-body" style="padding-bottom: 0px;">

        <?php echo get_alert(); ?>

        <div class="row">

            <div class="col-md-4">
                <label>No Resi Manual **</label>
                <div class="row">
                    <div class="col-md-3" >
                        <input
                            style="border-radius: 0px; width: 70px;"
                            type="text" 
                            name="branch_code" 
                            class="form-control" 
                            <?php if (branch() != ''): ?>
                                readonly="readonly"
                            <?php endif; ?>
                            <?php if (!isset(temp_orders()['orders']->order_branch_code)): ?>
                                value="<?php echo branch() != '' ? branch()['branch_code'] : '000'; ?>" 
                            <?php else: ?>
                                value='<?php echo temp_orders()['orders']->order_branch_code; ?>'
                            <?php endif; ?> />  
                    </div>
                    <div class="col-md-9" >
                        <input 
                            type="text"
                            name="order_manual_id" 
                            class="form-control" 
                            <?php if (!isset(temp_orders()['orders']->order_manual_resi_pure)): ?>
                                value="<?php echo pickup_isset() ? substr($order_id, -10) : ''; ?>"
                            <?php else: ?>
                                value='<?php echo temp_orders()['orders']->order_manual_resi_pure; ?>'
                            <?php endif; ?>
                            placeholder="No Resi Manual" />
                    </div>
                </div>
                <span id="helpBlock2" style="font-size: 12px;" class="help-block">**) Hanya jika diperlukan</span>
            </div>

            <div class="col-md-2">
                <?php
                echo $this->form->select(
                        [
                            'name' => 'order_type',
                            'label' => 'Jenis Order',
                            'value' => [
                                ['id' => 1, 'item' => 'Ritel'],
                                ['id' => 2, 'item' => 'Partai'],
                                ['id' => 3, 'item' => 'Partai Borong']
                            ],
                            'keys' => 'id',
                            'values' => 'item',
                            'class' => '',
                            'required' => true,
                            'selected' => isset(temp_orders()['orders']->order_type) ? temp_orders()['orders']->order_type : ''
                        ]
                );
                ?>
            </div>

            <?php
            echo $this->form->text(
                    [
                        'name' => 'order_date',
                        'label' => 'Tanggal Order',
                        'max' => 100,
                        'required' => false,
                        'class' => 'col-sm-2',
                        'value' => isset(temp_orders()['orders']->order_date) ? mdate('%d/%m/%Y', temp_orders()['orders']->order_date) : mdate('%d/%m/%Y'),
                        'attribute' => ['readonly' => true],
                        'type' => 'text'
                    ]
            );
            ?>

            <?php
            echo $this->form->select(
                    [
                        'name' => 'order_payment_type',
                        'label' => 'Metode Pembayaran',
                        'value' => [
                            ['id' => 1, 'item' => 'Cash / Tunai'],
                            ['id' => 2, 'item' => 'Franko'],
                            ['id' => 3, 'item' => 'Tagih Tunai'],
                            ['id' => 4, 'item' => 'Tagih Tempo'],
                            ['id' => 5, 'item' => 'BDB']
                        ],
                        'keys' => 'id',
                        'values' => 'item',
                        'class' => 'col-sm-2',
                        'required' => true,
                        'selected' => isset(temp_orders()['orders']->order_payment_type) ? temp_orders()['orders']->order_payment_type : ''
                    ]
            );
            ?>

            <?php
            echo $this->form->text(
                    [
                        'name' => 'long_due_date',
                        'label' => 'Lama Masa Jatuh Tempo',
                        'max' => 100,
                        'required' => false,
                        'class' => 'col-sm-2',
                        'value' => isset(temp_orders()['orders']->orders_due_date) ? temp_orders()['orders']->orders_due_date : '',
                        'attribute' => ['min' => 0],
                        'type' => 'number'
                    ]
            );
            ?> 

            <!-- <?php
            echo $this->form->text(
                    [
                        'name' => 'order_due_date',
                        'label' => 'Tanggal Jatuh Tempo',
                        'max' => 100,
                        'required' => false,
                        'class' => 'col-sm-2',
                        'attribute' => [],
                        'type' => 'text',
                        'value' => isset(temp_orders()['orders']->order_due_date) ? temp_orders()['orders']->order_due_date : ''
                    ]
            );
            ?> -->


        </div>

    </div>
</div>
