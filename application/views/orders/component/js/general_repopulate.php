<script type="text/javascript">
    var pickups = $('input[name="pickup_order_session"]').val();
    
    if(pickups != ''){
        
        pickups = JSON.parse(pickups);

        /** General information */
        $('select[name="order_payment_type"]').val(3);
        $('input[name="order_notes"]').val('Paket pickup order dari websites');    
        
        /** Customer inforation */
        $('input[name="customer_name"]').val(pickups['nama_pengirim']);
        if (pickups['perusahaan'] === '') {
            $('select[name="customer_type"]').val(1);
        } else {
            $('select[name="customer_type"]').val(2);
        }
        
        $('input[name="customer_phone"]').val(pickups['telp_pengirim']);
        $('input[name="customer_code"]').val(pickups['user_id']);
        $('textarea[name="customer_address"]').val(pickups['alamat_pengirim']);
        $('select[name="customer_city"]').find('option:contains(' + pickups['kabupaten_pengirim'] + ')').prop('selected', true);
        $('select[name="customer_city"]').select2();

        /** Origin */
        $('select[name="city_id_origin"]').val(pickups['kbp_asal_id']).trigger('change');
        $('textarea[name="order_origin_text"]').val(pickups['alamat_pengirim']);
        
        /** Destination */
        $('select[name="city_id_destination"]').val(pickups['kbp_tujuan_id']).trigger('change');
        
        $('textarea[name="order_destination_text"]').val(pickups['alamat_penerima']);
        $('input[name="destination_name"]').val(pickups['nama_penerima']);
        $('input[name="destination_telp"]').val(pickups['telp_penerima']);
        
    }
    
</script>