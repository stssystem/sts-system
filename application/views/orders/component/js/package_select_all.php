<script type="text/javascript">
    $('body').on('click', '.package-actions tr', function () {
        $(this).toggleClass("rows");
    });

    $('.package-select-all').click(function () {
        if (check_rows_selected($(".package-actions tr"))) {
            $(".package-actions tr").removeClass('rows');
        } else {
            $(".package-actions tr").addClass('rows');
        }
    });

    function check_rows_selected(obj) {
        var r = false;
        $(obj).each(function () {
            if ($(this).hasClass('rows')) {
                r = true;
            }
        });
        return r;
    }

    /** Delete Package */
    $('body').on('click', '.package-delete-all', function () {

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus data ini?";
        $('#modal-delete-alert').find('.modal-body p').html(text);

        /** Alter class */
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '#');
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('data-dismiss', 'modal');
        $('#modal-delete-alert').find('.modal-footer a.a-delete').addClass('destroy-all-package');

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });

    $('body').on('click', '.destroy-all-package', function () {
        var package_list = Array();
        $('.rows').each(function () {
            var temp = $(this).find('input[name="edit-value"]').val();
            temp = JSON.parse(temp);
            package_list.push(temp['package_parent']);
        });

        $.post('<?php echo site_url('orders/orders/destroy_package_multiple'); ?>',
                {data: JSON.stringify(package_list)}, function (data) {
            data_tables();
        });

        $('#modal-delete-alert').find('.modal-footer a.a-delete').removeClass('destroy-all-package');

    });
</script>