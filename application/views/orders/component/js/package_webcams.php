<script type="text/javascript">
    
    /** Image upload */
    $("a#photo_profile").click(function () {
        $("input#photo_upload").trigger("click");
    });

    $("input#photo_upload").change(function () {

        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) {
            return;
        }

        for (i = 0; files.length; i++) {

            if (/^image/.test(files[i].type)) {

                var reader = new FileReader();
                reader.readAsDataURL(files[i]);

                reader.onloadend = function () {

                    var img = '<div class="col-md-3 img-list"><a class="thumbnail" id="photo_profile">'
                            + '<img src="' + this.result + '" style="width: 100%; height:150px;"><i class="fa fa-times img-delete"></i>'
                            + '<input name="image[]" class="input-image" type="hidden" value="' + this.result + '" ></a>'
                            + '</div>';

                    $('.image-frame').prepend(img);

                }
            }
        }

    });

    $('body').on('click', '.img-delete', function () {
        $(this).parent().parent().remove();
    });

    $('a#take-photo').on('shown.bs.tab', function (e) {

        setTimeout(function () {
            Webcam.attach('#photoshot');
        }, 500);

    });

    $('#photo_profile').click(function () {

        Webcam.snap(function (data_uri) {

            var img = '<div class="col-md-6 img-list"><a class="thumbnail" id="photo_profile">'
                    + '<img src="' + data_uri + '" style="width: 100%; height:150px;"><i class="fa fa-times img-delete"></i>'
                    + '<input name="image[]" class="input-image" type="hidden" value="' + data_uri + '" ></a>'
                    + '</div>';

            $('.image-frame').prepend(img);

        });

    });
    
</script>