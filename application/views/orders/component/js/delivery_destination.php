<script type="text/javascript" >

    destination_branches_autocomplete();

    /**Get destination privinces */
    /** Default province */
    var old_province_id_destination = $('input[name="old_province_id_destination"]').val();
    if (old_province_id_destination != '') {
        get_destination_province(old_province_id_destination);
    } else {
        get_destination_province();
    }

    /** Default city destination */
    $('select[name="city_id_destination"]').on('change', function () {
        get_destination_districts($(this).val());
        get_destination_branch($(this).val());
        geo_destination_graps();
        clear_destination();

        $.post('<?php echo site_url('request/locations/get_province_id'); ?>', {city_id: $(this).val()}, function (data) {
            $('select[name="province_id_destination"]').val(data);
            $('select[name="province_id_destination"]').select2();
        });

    });

    /** Event */
    $('body').on('change', 'select[name="branch_id_destination"]', function () {
        update_price();
    });

    function get_destination_province(selected) {

        /**Get destination province */
        $('select[name="province_id_destination"]').change(function () {
            get_destination_cities($(this).val());
            $('input[name="old_province_id_destination"]').val($(this).val());
        });

        /** Repopulate */
        if (typeof (selected) !== 'undefined') {

            $('select[name="province_id_destination"]').val(selected);

            /** Repopulate destination cities */
            var old_city_id_destination = $('input[name="old_city_id_destination"]').val();
            get_destination_cities(selected, old_city_id_destination);

        }

    }

    function get_destination_cities(province_id, selected, callback) {

        var url = '<?php echo site_url('request/locations/cities/city_id_destination'); ?>';

        $.post(url, {province_id: province_id}, function (data) {

            var form = $('select[name="city_id_destination"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="city_id_destination"]').select2();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="city_id_destination"]').val(selected);
            }

            /** Select city */
            $('select[name="city_id_destination"]').on('change', function () {
                get_destination_districts($(this).val());
                get_destination_branch($(this).val());
                geo_destination_graps();
                clear_destination();
            });

            geo_destination_graps();
            bind_change_event();

        });

        /** Closing with callback function */
        if (callback && typeof (callback) === "function") {
            callback(arguments[1]);
        }

    }

    /** Repopulate destination */
    var old_city_id_destination = $('select[name="city_id_destination"]').val();
    if (old_city_id_destination != '') {
        get_destination_districts(old_city_id_destination, $('input[name="old_districts_id_destination"]').val());
        get_destination_branch(old_city_id_destination, $('input[name="old_order_destination"]').val());
    }

    /** Select  */
    $('select[name="city_id_destination"]').on('change', function () {
        get_destination_districts($(this).val());
        get_destination_branch($(this).val());
    });

    function get_destination_districts(city_id, selected) {
        $.post('<?php echo site_url('request/locations/districts/districts_id_destination'); ?>',
                {city_id: city_id},
        function (data) {
            var form = $('select[name="districts_id_destination"]').parent().parent();
            $(form).replaceWith(data);

            $('select[name="districts_id_destination"]').select2();
            bind_change_event();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="districts_id_destination"]').val(selected);
                /** Select village */
                get_destination_village(selected, $('input[name="old_village_id_destination"]').val());
            }

            /** Select district */
            $('body').on('change', 'select[name="districts_id_destination"]', function () {
                get_destination_village($(this).val());
            });

        });

    }

    function get_destination_village(districts_id, selected) {

        $.post('<?php echo site_url('request/locations/villages/village_id_destination'); ?>',
                {districts_id: districts_id},
        function (data) {
            var form = $('select[name="village_id_destination"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="village_id_destination"]').select2();
            bind_change_event();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="village_id_destination"]').val(selected);
            }

        });

    }

    function get_destination_branch(city_id, selected, callback) {

        $.post('<?php echo site_url('request/locations/branches/branch_id_destination'); ?>',
                {city_id: city_id},
        function (data) {

            /** Set up select form */
            /** 
             * Disable until condition stable
             var form = $('select[name="branch_id_destination"]').parent().parent();
             $(form).replaceWith(data);
             $('select[name="branch_id_destination"]').select2();
             */

            /** Event */
            $('body').on('change', 'select[name="branch_id_destination"]', function () {
                var str = $(this).find('option:selected').attr('data-branch-code');
                $('input[name="destination_branch_code"]').val(str.trim());
                
                // Update price in total 
                update_price();
                
            });

            /** Price setting */
            bind_change_event();
            destination_branches_autocomplete();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="branch_id_destination"]').val(selected);
            }

        });

        /** Closing with callback function */
        if (callback && typeof (callback) === "function") {
            callback(arguments[1]);
        }

    }

    /** Branch autocomplete */

    function destination_branches_autocomplete() {

        var destination_branches = {
            url: "<?php echo site_url('request/locations/get_destination_branches'); ?>",
            getValue: "branch_name_code",
            theme: "square",
            list: {
                match: {
                    enabled: true
                },
                onChooseEvent: function () {
                    var branch_id = $("input[name='destination_branch_code']").getSelectedItemData().branch_id;
                    $.post('<?php echo site_url('request/locations/get_detail_branch'); ?>/' + branch_id,
                            function (data) {
                                var result = JSON.parse(data);
//                                $('select[name="province_id_destination"]').val(result['province_id']).trigger("change");
//                                get_destination_cities(result['province_id'], result['city_id'], set_destination_cities(result['city_id']));
                                get_destination_branch(result['city_id'], 'false', set_destination_branch(result['branch_id']));
                            }
                    );
                },
                onLoadEvent: function () {
                    $('.easy-autocomplete-container').attr('style', 'width:200px;');
                }

            }
        };

        $("input[name='destination_branch_code']").easyAutocomplete(destination_branches);
    }

    function set_destination_cities(city_id) {
        $('select[name="city_id_destination"]').val(city_id).trigger('change');
        setTimeout(function () {
            $('select[name="city_id_destination"]').select2();
        }, 1000);
    }

    function set_destination_branch(branch_id) {
        setTimeout(function () {
            $('select[name="branch_id_destination"]').val(branch_id).trigger('change');
            grap_price_params();
        }, 2000);
    }



</script>