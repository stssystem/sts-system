<script type="text/javascript">

    var order_extra = {};
    function form_submit_order_extra() {

        /** Collect data from form */
        order_extra['order_id'] = $('input[name="order_id"]').val();
        order_extra['det_order_extra_name'] = $('input[name="det_order_extra_name"]').val();
        order_extra['det_order_extra_total'] = $('input[name="det_order_extra_total"]').val();

        /** Validation */
        var validation = true;
        $('.package-error').remove();
        $('.has-error').removeClass('has-error');
        global_message = "<ul>";

        if (order_extra['det_order_extra_name'] == '' || order_extra['det_order_extra_name'] == 0 || typeof order_extra['det_order_extra_name'] == 'undefined') {
            validation = set_message($('input[name="det_order_extra_name"]'), 'Nama biaya ekstra belum diisi', true);
        }
        if (order_extra['det_order_extra_total'] == '' || order_extra['det_order_extra_total'] == 0 || typeof order_extra['det_order_extra_total'] == 'undefined') {
            validation = set_message($('input[name="det_order_extra_total"]'), 'Total biaya ekstra belum diisi', true);
        }

        if (validation == false) {
            $(".extra-order-validation-trigger").trigger("click");
            $('#modal-extra-order-validation').on('shown.bs.modal', function (e) {
                $(this).find('.modal-body p').empty();
                $(this).find('.modal-body p').append(global_message + '</ul>');
            });
        } else {

            var result = JSON.stringify(order_extra);

            $.post('<?php echo site_url('orders/orders/det_orders_extra_store'); ?>',
                    {data: result},
            function (data) {
                order_extra_data_table();
            }
            );

            $('input[name="det_order_extra_name"]').val('');
            $('input[name="det_order_extra_total"]').val('');

        }

    }

    /** Delete extra order */
    $('body').on('click', '.btn-delete-order-extra', function () {
        delete_extra_order($(this));
    });

    /** Save new extra order */
    $('.btn-order-extra').click(function () {
        form_submit_order_extra();
    });

    /** Update orders extra  */
    $('body').on('click', '.btn-edit-order-extra', function () {
        edit_orders_extra($(this));
    });

    $('body').on('click', '.btn-order-extra-update', function () {

        var rorder_extra = {};
        rorder_extra['det_order_extra_id'] = $('input[name="det_order_extra_id"]').val();
        rorder_extra['det_order_extra_name'] = $('input[name="det_order_extra_name"]').val();
        rorder_extra['det_order_extra_total'] = $('input[name="det_order_extra_total"]').val();

        $.post('<?php echo site_url('orders/orders/det_orders_extra_update'); ?>',
                {data: JSON.stringify(rorder_extra)}, function (data) {

            var button = '<button style="margin-top: 24px;" type="button" class="btn btn-flat btn-info btn-order-extra btn-block"><i class="fa fa-pencil"></i> Tambahkan</button>';
            $('.btn-order-extra-update').replaceWith(button);

            $('body').on('click', '.btn-order-extra', function () {
                form_submit_order_extra();
            });

            reset_orders_extra();

            order_extra_data_table();

        });

    });

    function edit_orders_extra(tag) {
        var data = $(tag).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);
        $('input[name="det_order_extra_id"]').val(data['det_order_extra_id']);
        $('input[name="det_order_extra_name"]').val(data['det_order_extra_name']);
        $('input[name="det_order_extra_total"]').val(data['det_order_extra_total']);

        var html = '<button '
                + 'style="margin-top: 24px;" type="button" '
                + 'class="btn btn-flat btn-success btn-order-extra-update btn-block">'
                + '<i class="fa fa-refresh"></i> Update</button>';

        $('.btn-order-extra').replaceWith(html);
    }

    function reset_orders_extra() {
        $('input[name="det_order_extra_id"]').val('');
        $('input[name="det_order_extra_name"]').val('');
        $('input[name="det_order_extra_total"]').val('');
    }
    
</script>