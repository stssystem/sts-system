<script type="text/javascript">
    
    var def_dpt = $('select[name="order_payment_type"]').val();
    
    if (def_dpt == 2 || def_dpt == 4) {
        due_date(false);
    } else {
        due_date(true);
    }

    $('select[name="order_payment_type"]').change(function () {
        var val = $(this).val();
        if (val == 2 || val == 4) {
            due_date(false);
        } else {
            due_date(true);
        }
    });

    $('input[name="order_due_date"]').datepicker({ format: 'dd/mm/yyyy' });
    $('input[name="order_date"]').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-31d', 
        endDate: '0',
        todayBtn: "linked",
        autoclose: true,
        todayHighlight: true
    });

    function due_date(status) {
        $('input[name="long_due_date"]').prop('disabled', status);
        $('input[name="order_due_date"]').prop('disabled', status);
        if (status == true) {
            $('input[name="long_due_date"]').val('');
            $('input[name="order_due_date"]').val('');
        }

    }

    $('input[name="long_due_date"]').on('keyup', function () {
        var day = moment().add($(this).val(), 'days');
        day = moment(day).format('MM/DD/YYYY');
        $('input[name="order_due_date"]').val(day);
    });

    $('input[name="order_due_date"]').on('change', function () {
        var start = moment();
        var stop = moment($(this).val());
        var result = stop.diff(start, 'days');
        $('input[name="long_due_date"]').val(result);
    });

    setTimeout(function () {
        $('.alert-general').fadeOut('slow');
    }, 10000);

    /** Branch autocomplete */
    var branches_code = {
        url: "<?php echo site_url('request/locations/get_branches'); ?>",
        getValue: "branch_name_code",
        theme: "square",
        list: {
            match: {
                enabled: true
            },
            onChooseEvent: function () {
                var branch_code = $("input[name='branch_code']").getSelectedItemData().branch_code;
                $('input[name="branch_code"]').val(branch_code);
            },
            onLoadEvent: function () {
                $('.easy-autocomplete-container').attr('style', 'width:200px;');
            }
        }
    };

    $('input[name="branch_code"]').easyAutocomplete(branches_code);

    $('input[name="order_manual_id"]').trigger('focus');

</script>