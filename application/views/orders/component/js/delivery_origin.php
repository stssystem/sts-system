<script type="text/javascript" >

    orgin_branches_autocomplete();

    /** Auto select when user branch available */
    var branch_now = $('input[name="branch_now"]').val();
    var branch_json = 0;
    if (branch_now != 'null') {
        branch_json = JSON.parse(branch_now);
    }

    if (branch_now != 'null') {
        if ($('input[name="old_city_id_origin"]').val() == '') {
            $('select[name="city_id_origin"]').val(branch_json['city_id']);
            get_origin_districts(branch_json['city_id']);
        } else {
            $('select[name="city_id_origin"]').val($('input[name="old_city_id_origin"]').val());
            get_origin_districts($('input[name="old_city_id_origin"]').val(), $('input[name="old_districts_id_origin"]').val());
        }
        get_origin_branch(branch_json['city_id'], 'true', branch_json['branch_id']);
        $('select[name="branch_id_origin"]').attr('disabled');
    }


    /** Repopulate origin */
    var old_city_id_origin = $('input[name="old_city_id_origin"]').val();
    if (old_city_id_origin != '') {
        get_origin_districts(old_city_id_origin, $('input[name="old_districts_id_origin"]').val());
        if (branch_now == 'null') {
            get_origin_branch(old_city_id_origin, 'false', $('input[name="old_order_origin"]').val());
        } else {
            get_origin_branch(branch_json['city_id'], 'true', $('input[name="old_order_origin"]').val());
        }
    }

    /** Default province */
    if (branch_now !== 'null') {
        var old_province_id_origin = $('input[name="old_province_id_origin"]').val();
        if (old_province_id_origin != '') {
            get_origin_province(old_province_id_origin);
        } else {
            get_origin_province(branch_json['province_id']);
        }
    } else {
        var old_province_id_origin = $('input[name="old_province_id_origin"]').val();
        if (old_province_id_origin != '') {
            get_origin_province(old_province_id_origin);
        } else {
            get_origin_province();
        }
    }

    /** Event */
    $('body').on('change', 'select[name="branch_id_origin"]', function () {
        update_price();
    });

    /** Default city origin event */
    $('select[name="city_id_origin"]').on('change', function () {
        get_origin_districts($(this).val());
        if (branch_now == 'null') {
            get_origin_branch($(this).val(), 'false');
        }
        geo_graps();
        clear_origin();

        $.post('<?php echo site_url('request/locations/get_province_id'); ?>', {city_id: $(this).val()}, function (data) {
            $('select[name="province_id_origin"]').val(data);
            $('select[name="province_id_origin"]').select2();
        });

    });

    function get_origin_province(selected) {

        /**Get origin cities */
        $('select[name="province_id_origin"]').change(function () {
            get_origin_cities($(this).val());
            $('input[name="old_province_id_origin"]').val($(this).val());
        });

        /** Repopulate */
        if (typeof (selected) !== 'undefined') {
            $('select[name="province_id_origin"]').val(selected);
            /** Repopulate origin cities */
            var old_city_id_origin = $('input[name="old_city_id_origin"]').val();
            if (old_city_id_origin != '') {
                get_origin_cities(selected, old_city_id_origin);
            } else {
                get_origin_cities(selected, branch_json['city_id']);
            }

        }

    }

    function get_origin_cities(province_id, selected, callback) {

        var url = '<?php echo site_url('request/locations/cities/city_id_origin'); ?>';

        $.post(url, {province_id: province_id}, function (data) {

            var form = $('select[name="city_id_origin"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="city_id_origin"]').select2();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="city_id_origin"]').val(selected);
            }

            /** Select city */
            $('select[name="city_id_origin"]').on('change', function () {
                get_origin_districts($(this).val());
                if (branch_now == 'null') {
                    get_origin_branch($(this).val(), 'false');
                }
                geo_graps();
                clear_origin();
            });

            geo_graps();
            bind_change_event();

        });

        /** Closing with callback function */
        if (callback && typeof (callback) === "function") {
            callback(arguments[1]);
        }

    }

    /** Get origin districts */
    function get_origin_districts(city_id, selected) {

        var url = '<?php echo site_url('request/locations/districts/districts_id_origin'); ?>';

        $.post(url,
                {city_id: city_id},
        function (data) {
            var form = $('select[name="districts_id_origin"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="districts_id_origin"]').select2();
            geo_graps();
            bind_change_event();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="districts_id_origin"]').val(selected);
                /** Select village */
                get_origin_village(selected, $('input[name="old_village_id_origin"]').val());
            }

            /** Select village */
            $('body').on('change', 'select[name="districts_id_origin"]', function () {
                get_origin_village($(this).val(), $('input[name="old_village_id_origin"]').val());
            });

        });

    }

    /** Get origin village */
    function get_origin_village(districts_id, selected) {
        $.post('<?php echo site_url('request/locations/villages/village_id_origin'); ?>',
                {districts_id: districts_id},
        function (data) {
            var form = $('select[name="village_id_origin"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="village_id_origin"]').select2();
            bind_change_event();
            geo_graps();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="village_id_origin"]').val(selected);
            }

        }
        );
    }

    /** Get origin branch */
    function get_origin_branch(city_id, disabled, selected, callback) {

        var url = '';
        if (disabled == 'true') {
            url = '<?php echo site_url('request/locations/branches/branch_id_origin'); ?>/true';
        } else {
            url = '<?php echo site_url('request/locations/branches/branch_id_origin'); ?>';
        }

        $.post(url,
                {city_id: city_id},
        function (data) {

            /** Set up select form */
            /** Disable until condition stable
             var form = $('select[name="branch_id_origin"]').parent().parent();
             $(form).replaceWith(data);
             $('select[name="branch_id_origin"]').select2();
             */

            /** Event */
            $('body').on('change', 'select[name="branch_id_origin"]', function () {

                // get branch-code
                var str = $(this).find('option:selected').attr('data-branch-code');
                $('input[name="origin_branch_code"]').val(str.trim());

                update_price();

            });

            /** Price setting */
            bind_change_event();
            geo_graps();
            orgin_branches_autocomplete();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="branch_id_origin"]').val(selected);
            }

        });

        /** Closing with callback function */
        if (callback && typeof (callback) === "function") {
            callback();
        }

    }

    /** Branch autocomplete */
    function orgin_branches_autocomplete() {
        var origin_branches = {
            url: "<?php echo site_url('request/locations/get_branches'); ?>",
            getValue: "branch_name_code",
            theme: "square",
            list: {
                match: {
                    enabled: true
                },
                onChooseEvent: function () {
                    var branch_id = $("input[name='origin_branch_code']").getSelectedItemData().branch_id;
                    $.post('<?php echo site_url('request/locations/get_detail_branch'); ?>/' + branch_id,
                            function (data) {
                                var result = JSON.parse(data);

                                /** This function for temporary is deprecated until politic stable */
                                $('select[name="province_id_origin"]').val(result['province_id']).trigger("change");
                                get_origin_cities(result['province_id'], result['city_id'], set_origin_cities(result['city_id']));

                                get_origin_branch(result['city_id'], 'false', set_origin_branch(result['branch_id']));

                            });
                },
                onLoadEvent: function () {
                    $('.easy-autocomplete-container').attr('style', 'width:200px;');
                }
            }
        };

        $("input[name='origin_branch_code']").easyAutocomplete(origin_branches);
    }

    function set_origin_cities(city_id) {
        $('select[name="city_id_origin"]').val(city_id).trigger('change');
        setTimeout(function () {
            $('select[name="city_id_origin"]').select2();
        }, 1000);
    }

    function set_origin_branch(branch_id) {
        setTimeout(function () {
            $('select[name="branch_id_origin"]').val(branch_id).trigger('change');
            grap_price_params();
        }, 2000);
    }

    /** Checkbox styling */
    $('input[name="save_update_origin"]').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        increaseArea: '30%'
    });


</script>