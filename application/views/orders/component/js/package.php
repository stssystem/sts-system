<script type="text/javascript">

    /** Insert package */
    $('.btn-submit').click(function () {
        form_submit();
    });

    /** Edit package  */
    $('.data-tables').on('click', '.btn-edit', function () {

        var data = JSON.parse($(this).parent().parent().find('input').val());

        var button = '<button type="button" class="btn btn-success btn-sm btn-flat btn-submit btn-action btn-edit-package"  data-toggle="tooltip" data-placement="top" title="Perbaru Data" >'
                + '<i class="fa fa-refresh"></i></button>';

        $('.btn-action').replaceWith(button);

        /** Get image list from server */
        get_image_list(data['package_id']);

        $('input[name="package_id"]').val(data['package_id']);
        $('input[name="package_title"]').val(data['package_title']);
        $('textarea[name="package_content"]').val(data['package_content']);
        $('input[name="package_width"]').val(data['package_width']);
        $('input[name="package_height"]').val(data['package_height']);
        $('input[name="package_lenght"]').val(data['package_lenght']);
        $('input[name="package_weight"]').val(data['package_weight']);
        $('select[name="package_type"]').val(data['package_type']);
        $('input[name="det_order_qty"]').val(data['qty']);
        var package_size = math.eval('(' + data['package_size'] + ' * ' + data['qty'] + ') / 1000000');
        $('input[name="package_size"]').val(package_size);

        var ods = $(".order-sell-total").attr("name");
        if (ods == 'det_order_sell_total_temp') {
            $('input[name="det_order_sell_total_temp"]').attr('name', 'det_order_sell_total');
        }

        setTimeout(function () {

            var total_sell = math.eval('(' + data['sell_total'] + ' * ' + data['qty'] + ')');

            $('input[name="det_order_sell_total"]').val(total_sell);
            $('input[name="det_order_sell_total"]').attr('name', 'det_order_sell_total_temp');
            $('input[name="det_order_sell_total_temp"]').attr('value', total_sell);

            var total = math.eval('(' + data['total'] + ' * ' + data['qty'] + ')');
            $('.total-price').text(numeral(total).format('0,0'));

        }, 1000);

        grap_price_params();

    });

    $('body').on('click', '.btn-edit-package', function () {
        form_update();
    });

    /** Delete Package */
    $('body').on('click', '.btn-delete', function () {

        var data = $(this).parent().parent().find('input').val();
        var data = JSON.parse(data);

        $.get('<?php echo site_url('orders/orders/destroy_package'); ?>/' + data['package_id'],
                {data: null},
        function (data) {
            data_tables();
        }
        );

    });

    $('select[name="customer_price"]').change(function () {
        grap_price_params();
    });

</script>