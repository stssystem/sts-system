<script type="text/javascript">

    $('input[name="customer_birth_date"]').datepicker({ format: 'dd/mm/yyyy' });

    $('a[href="#tab_customer"]').on('shown.bs.tab', function (e) {
        autocomplete();
    });

    autocomplete();

    function autocomplete() {
        /** Autocomplete */
        var options = {
            url: function(phrase) {
                return "<?php echo site_url('request/customer_helper/get_like?phrase='); ?>" + phrase;
            },
            getValue: "customer_name",
            theme: "square",
            requestDelay: 250,
            template: {
                type: "description",
                fields: {
                    description: "customer_code"
                }
            },
            list: {
                match: {
                    enabled: true
                },
                onChooseEvent: function () {
                    var customer_id = $("input[name='customer_name']").getSelectedItemData().customer_id;
                    var customer_birth_date = $("input[name='customer_name']").getSelectedItemData().customer_birth_date;
                    $.post('<?php echo site_url('request/customer_helper/get_detail'); ?>/' + customer_id,
                            function (data) {

                                var result = JSON.parse(data);

                                if (customer_id != '') {

                                    $('input[name="customer_id"]').val(customer_id);

                                    $('input[name="customer_web_id"]').val(result['customer_web_id']);
                                    $('input[name="customer_phone"]').val(result['customer_phone']);
                                    $('input[name="customer_id_number"]').val(result['customer_id_number']);
                                    $('input[name="customer_id_type_other"]').val(result['customer_id_type_other']);

                                    $('input[name="customer_birth_date"]').val(customer_birth_date);

                                    $('input[name="customer_sending_quota"]').val(result['customer_sending_quota']);
                                    $('input[name="customer_company_type_other"]').val(result['customer_company_type_other']);
                                    $('input[name="customer_referral"]').val(result['customer_referral']);
                                    $('input[name="customer_company_telp"]').val(result['customer_company_telp']);
                                    $('input[name="customer_company_email"]').val(result['customer_company_email']);
                                    $('input[name="customer_company_name"]').val(result['customer_company_name']);

                                    $('textarea[name="customer_address"]').val(result['customer_address']);
                                    $('textarea[name="customer_company_address"]').val(result['customer_company_address']);

                                    $('select[name="customer_type"]').val(result['customer_type']);
                                    $('select[name="customer_city"]').val(result['customer_city']);
                                    $('select[name="customer_id_type"]').val(result['customer_id_type']);
                                    $('select[name="customer_freq_sending"]').val(result['customer_freq_sending']);
                                    $('select[name="customer_company_type"]').val(result['customer_company_type']);
                                    $('select[name="customer_gender"]').val(result['customer_gender']);
                                    $('input[name="customer_code"]').val(result['customer_code']);

                                    /** Delivery default */
                                    get_origin_province(result['customers_origin_province_id']);
                                    get_origin_cities(result['customers_origin_province_id'], result['customers_origin_city_id']);                                    
                                    
                                    // Set Session
                                    $.session.set('customers_origin_province', result['customers_origin_province_id']);
                                    $.session.set('customers_origin_city', result['customers_origin_city_id']);
                                    
                                    $('textarea[name="order_origin_text"]').val(result['customers_origin_address']);
                                    $('input[name="origin_branch_code"]').val(result['branch_name'] + ' - ' + result['branch_code']);

                                    setTimeout(function () {
                                        get_origin_districts(result['customers_origin_city_id'], result['customers_origin_districts_id']);
                                        var branch_now_ = $('input[name="branch_now"]').val();
                                        if (branch_now_ == 'null') {
                                            get_origin_branch(result['customers_origin_city_id'], 'false', result['customers_origin_branch_id']);
                                        }
                                        setTimeout(function () {
                                            get_origin_village(result['customers_origin_districts_id'], result['customers_origin_villages_id']);
                                        }, 500);

                                    }, 500);

                                    $(".select2").find('select').select2();

                                }

                            }
                    );

                }
            }
        };

        $("input[name='customer_name']").easyAutocomplete(options);

        $('input[name="customer_name"]').on('keyup', function (event) {
            if (event.which != 13) {
                $('input[name="customer_id"]').val('');
            }
        });

    }
    
    /** Autocomplete for cusomer referal number */
    var optionsx = {        
        url: function(phrase) {
            return "<?php echo site_url('request/customer_helper/get_like?phrase='); ?>" + phrase;
        },
        getValue: "customer_name",
        theme: "square",
        requestDelay: 250,
        template: {
            type: "description",
            fields: {
                description: "customer_code"
            }
        },
        list: {
            maxNumberOfElements: 7,
            match: {
                enabled: true
            },
            onChooseEvent: function () {
                var customer_code = $("input[name='customer_referral']").getSelectedItemData().customer_code;
                $("input[name='customer_referral']").val(customer_code);
            }
        }
    };
    
    $("input[name='customer_referral']").easyAutocomplete(optionsx);


    /** Autocomplete for  customer code */
    var autocomplete_datacostomer = {
        url: function(phrase) {
            return "<?php echo site_url('request/customer_helper/get_like?phrase='); ?>" + phrase;
        },
        getValue: "customer_name",
        theme: "square",
        requestDelay: 250,
        list: {
            match: {
                enabled: true
            },
            onChooseEvent: function () {
                var customer_code = $("input[name='customer_code']").getSelectedItemData().customer_code;
                $("input[name='customer_code']").val(customer_code);

                var customer_id = $("input[name='customer_code']").getSelectedItemData().customer_id;
                var customer_birth_date = $("input[name='customer_code']").getSelectedItemData().customer_birth_date;

                $.post('<?php echo site_url('request/customer_helper/get_detail'); ?>/' + customer_id,
                        function (data) {

                            var result = JSON.parse(data);

                            if (customer_id != '') {

                                $('input[name="customer_id"]').val(customer_id);

                                $('input[name="customer_web_id"]').val(result['customer_web_id']);
                                $('input[name="customer_phone"]').val(result['customer_phone']);
                                $('input[name="customer_id_number"]').val(result['customer_id_number']);
                                $('input[name="customer_id_type_other"]').val(result['customer_id_type_other']);
                                $('input[name="customer_name"]').val(result['customer_name']);

                                $('input[name="customer_birth_date"]').val(customer_birth_date);

                                $('input[name="customer_sending_quota"]').val(result['customer_sending_quota']);
                                $('input[name="customer_company_type_other"]').val(result['customer_company_type_other']);
                                $('input[name="customer_referral"]').val(result['customer_referral']);
                                $('input[name="customer_company_telp"]').val(result['customer_company_telp']);
                                $('input[name="customer_company_email"]').val(result['customer_company_email']);
                                $('input[name="customer_company_name"]').val(result['customer_company_name']);

                                $('textarea[name="customer_address"]').val(result['customer_address']);
                                $('textarea[name="customer_company_address"]').val(result['customer_company_address']);

                                $('select[name="customer_type"]').val(result['customer_type']);
                                $('select[name="customer_city"]').val(result['customer_city']);
                                $('select[name="customer_id_type"]').val(result['customer_id_type']);
                                $('select[name="customer_freq_sending"]').val(result['customer_freq_sending']);
                                $('select[name="customer_company_type"]').val(result['customer_company_type']);
                                $('select[name="customer_gender"]').val(result['customer_gender']);
                                $('input[name="customer_code"]').val(result['customer_code']);

                                /** Delivery default */
                                get_origin_province(result['customers_origin_province_id']);
                                get_origin_cities(result['customers_origin_province_id'], result['customers_origin_city_id']);
                                $('textarea[name="order_origin_text"]').val(result['customers_origin_address']);
                                $('input[name="origin_branch_code"]').val(result['branch_name'] + ' - ' + result['branch_code']);

                                setTimeout(function () {
                                    get_origin_districts(result['customers_origin_city_id'], result['customers_origin_districts_id']);
                                    var branch_now_ = $('input[name="branch_now"]').val();
                                    if (branch_now_ == 'null') {
                                        get_origin_branch(result['customers_origin_city_id'], 'false', result['customers_origin_branch_id']);
                                    }
                                    setTimeout(function () {
                                        get_origin_village(result['customers_origin_districts_id'], result['customers_origin_villages_id']);
                                    }, 500);
                                }, 500);
                                $(".select2").find('select').select2();
                            }
                        }
                );
            }
        }
    };

    $("input[name='customer_code']").easyAutocomplete(autocomplete_datacostomer);

</script>