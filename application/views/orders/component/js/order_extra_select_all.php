<script type="text/javascript">
    $('body').on('click', '.orderextra-actions tr', function () {
        $(this).toggleClass("rows");
    });

    $('.orderextra-select-all').click(function () {
        if (check_rows_selected($(".orderextra-actions tr"))) {
            $(".orderextra-actions tr").removeClass('rows');
        } else {
            $(".orderextra-actions tr").addClass('rows');
        }
    });

    function check_rows_selected(obj) {
        var r = false;
        $(obj).each(function () {
            if ($(this).hasClass('rows')) {
                r = true;
            }
        });
        return r;
    }

    /** Delete Package */
    $('body').on('click', '.orderextra-delete-all', function () {

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus data ini?";
        $('#modal-delete-alert').find('.modal-body p').html(text);

        /** Alter class */
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '#');
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('data-dismiss', 'modal');
        $('#modal-delete-alert').find('.modal-footer a.a-delete').addClass('destroy-all-orderextra');

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });

    $('body').on('click', '.destroy-all-orderextra', function () {
        var orderextra_list = Array();
        $('.rows').each(function () {
            var temp = $(this).find('input[name="edit-value"]').val();
            temp = JSON.parse(temp);
            orderextra_list.push(temp['det_order_extra_id']);
        });


        $.post('<?php echo site_url('orders/orders/destroy_orderextra_multiple'); ?>',
                {data: JSON.stringify(orderextra_list)}, function (data) {
            order_extra_data_table();
        });

        $('#modal-delete-alert').find('.modal-footer a.a-delete').removeClass('destroy-all-orderextra');

    });
</script>