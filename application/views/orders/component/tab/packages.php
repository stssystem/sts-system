<style type="text/css">
    .unit-text{
        font-size: 10px;
    }
</style>

<div class="tab-pane" id="tab_paket">
    <div class="row">

        <div class="col-md-12">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#detail-paket" aria-controls="home" role="tab" data-toggle="tab" id="detail-package-form">Detail Paket</a></li>
                <li role="presentation"><a href="#galeri" id="take-photo" aria-controls="profile" role="tab" data-toggle="tab">Foto Paket</a></li>
                <li class="pull-right">
                    <select class="form-control" name="customer_price">
                        <option value="0">Gunakan Harga Normal</option>
                        <option value="1">Gunakan Harga Langganan</option>
                    </select>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" style="margin-top: 20px;">
                <div role="tabpanel" class="tab-pane active" id="detail-paket">

                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-sm" width="10%">Nama Paket</th>
                                <th class="text-sm" width="10%">Jenis Paket</th>
                                <th class="text-sm" width="9%">Berat / Paket <span class="unit-text">(Kg)</span></th>
                                <th class="text-sm" width="9%">Jumlah Koli</th>
                                <th class="text-sm" width="9%">Berat Total <span class="unit-text">(Kg)</span></th>
                                <th class="text-sm" width="7%">Panjang <span class="unit-text">(Cm)</span></th>
                                <th class="text-sm" width="7%">Lebar <span class="unit-text">(Cm)</span></th>
                                <th class="text-sm" width="7%">Tinggi <span class="unit-text">(Cm)</span></th>
                                <th class="text-sm" width="7%" >Volume Total <span class="unit-volume">(m<sup>3</sup>)</span></th>
                                <th class="text-sm" width="7%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <td>
                                    <input type="text" name="package_title" value="" class="form-control">
                                </td>
                                <td>

                                    <?php
                                    echo $this->form->select(
                                        [
                                        'name' => 'package_type',
                                        'label' => '',
                                        'max' => 100,
                                        'required' => false,
                                        'value' => get_package_type_list(),
                                        'keys' => 'package_type_id',
                                        'values' => 'package_type_name',
                                        'class' => ''
                                        ]
                                        );
                                        ?>
                                    </td>
                                    <td>
                                        <input type="number" name="package_weight" min="0" class="form-control input-price-params input-xs">
                                    </td>

                                    <td>
                                        <input type="number" name="det_order_qty"  min="1" class="form-control input-price-params" value="1">
                                    </td>
                                    
                                    <td>
                                        <input type="number" name="package_weight_total"  min="0" class="form-control input-price-params">
                                    </td>
                                    
                                    <td>
                                        <input type="number" name="tpackage_lenght"  min="0" class="form-control total-package-size input-price-params">
                                    </td>
                                    <td>
                                        <input type="number" name="tpackage_width"  min="0" class="form-control total-package-size input-price-params">
                                    </td>
                                    <td>
                                        <input type="number" name="tpackage_height"  min="0" class="form-control total-package-size input-price-params">
                                    </td>
                                    <td>
                                        <input type="number" name="package_size"  min="0" class="form-control input-price-params" >
                                        <input min="0" type="hidden" class="form-control pull-left order-sell-total" name="det_order_sell_total" />
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-sm btn-warning btn-flat" data-toggle="modal" data-target="#modal-package-note" data-toggle="tooltip" data-placement="top" title="Tambahkan Catatan" >
                                                <i class="fa fa-file-text-o"></i>
                                            </button>
                                            <button type="button" class="btn btn-info btn-sm btn-flat btn-submit btn-action btn-add-package" data-toggle="tooltip" data-placement="top" title="Simpan" >
                                                <i class="fa fa-save"></i>
                                            </button>
                                        </div>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="galeri">

                        <div  class="row">

                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div  class="thumbnail" id="photo_profile">
                                        <div id="photoshot" style="width: 100%; height: 350px; background-color: white;" >
                                        </div>
                                    </div>
                                    <input type="file" name="userfile" multiple id="photo_upload" style="display: none;" />
                                </div>
                            </div>

                            <div class="col-md-6 image-frame">

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12" >
                <table class="table data-tables" style="margin-top:-10px;" >
                    <thead>
                        <tr>
                            <th class="text-sm"  >ID</th>
                            <th class="text-sm"  >Nama Paket</th>
                            <th class="text-sm"  >Jenis Paket</th>
                            <th class="text-sm"  >Berat / Paket (Kg)</th>
                            <th class="text-sm"  >Jumlah Koli</th>
                            <th class="text-sm"  >Total Berat (Kg)</th>
                            <th class="text-sm"  >Total Volume (m<sup>3</sup>)</th>
                            <th class="text-sm" width="10%"  >Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="text-sm-table package-actions">

                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" >
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-flat btn-default package-select-all"><i class="fa fa-check"></i>  Pilih Semua</button>
                                    <button type="button" class="btn btn-sm btn-flat btn-danger package-delete-all" data-toggle="modal" data-target="#modal-delete-alert" ><i class="fa fa-trash"></i>  Hapus Masal</button>
                                </div>
                            </td>
                            <td align="left" style="font-weight:bold;">
                                Keterangan Order
                            </td>
                            <td colspan="5">
                                <?php
                                echo $this->form->text(
                                    [
                                    'name' => 'order_notes',
                                    'label' => '',
                                    'max' => 200,
                                    'required' => false,
                                    'class' => 'col-md-12 row',
                                    'value' => ''
                                    ]
                                    );
                                    ?>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="col-md-12">
                    <!-- small box -->
                    <div class="small-box bg-green" >
                        <div class="inner">
                            <div class="row">
                                <div class="col-md-4">
                                    <h5 ><strong>Total Biaya (PriceList)</strong></h5>
                                    <h4 ><b>Rp. <span class="total-all">0</span>,-</b></h4>        
                                </div>
                                <div class="col-md-4">
                                    <h5 ><strong>Total Biaya Pelanggan</strong></h5>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-1"><h4><b>Rp. </b></h4></label>
                                            <div class="col-md-8">
                                                <input class="total-all-customer form-control" name="customer_prices_total" />
                                            </div>
                                        </div>
                                    </div>        
                                </div>
                                <div class="col-md-4">
                                    <h5 ><strong>Termasuk Pajak?</strong></h5>
                                    <h4><input type="checkbox" name="order_use_tax" checked="checked" value="1"/> Ya</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <a href="#" class="btn btn-block btn-lg btn-primary btn-next-step3">Lanjutkan Ke Step 4</a>
                </div>
            </div>
        </div>