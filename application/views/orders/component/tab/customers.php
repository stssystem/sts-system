<div class="tab-pane active" id="tab_customer">

    <div class="row">
        <div class="col-md-6">
            <h4 class="box-title">No. Referral</h4>
            <div class="row">
                <?php
                echo $this->form->text([
                    'name' => 'customer_referral',
                    'label' => 'Bila Ada',
                    'max' => 100,
                    'required' => false,
                    'class' => 'col-sm-12',
                    'value' => isset(temp_orders()['customers']->customer_referral) ? temp_orders()['customers']->customer_referral : '',
                    'type' => 'text',
                    'attribute' => ['style' => "border-radius:0px;"]
                ]);
                ?>
            </div>
        </div>
        <div class="col-md-6">
            <h4 class="box-title">Kode Pelanggan</h4>
            <div class="row">
                <?php
                echo $this->form->text(
                        [
                            'name' => 'customer_code',
                            'label' => 'Kode Pelanggan',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-sm-12',
                            'value' => isset(temp_orders()['customers']->customer_code) ? temp_orders()['customers']->customer_code : '',
                        ]
                );
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-horizontal col-sm-6">
            <h4>Informasi Umum</h4>
            <div class="row">
                <?php
                echo $this->form->text_h(
                        [
                            'name' => 'customer_name',
                            'label' => 'Nama *',
                            'max' => 100,
                            'required' => true,
                            'class' => 'col-sm-12',
                            'attribute' => ['style' => 'border-radius:0px;'],
                            'value' => isset(temp_orders()['customers']->customer_name) ? temp_orders()['customers']->customer_name : '',
                        ]
                );
                ?>

                <?php
                echo $this->form->text_h(
                        [
                            'name' => 'customer_id_number',
                            'label' => 'Nomor Pengenal',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-sm-12 hidden onoff',
                            'value' => '',
                        ]
                );
                ?>

                <?php
                echo $this->form->select_h(
                        [
                            'name' => 'customer_id_type',
                            'label' => 'Tipe Pengenal',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-md-12 select2 hidden onoff',
                            'value' => [
                                ['id' => 1, 'item' => 'KTP'],
                                ['id' => 2, 'item' => 'SIM'],
                                ['id' => 3, 'item' => 'NPWP'],
                                ['id' => 3, 'item' => 'Lainnya']
                            ],
                            'keys' => 'id',
                            'values' => 'item',
                            'selected' => ''
                        ]
                );
                ?>

                <?php
                echo $this->form->text_h(
                        [
                            'name' => 'customer_id_type_other',
                            'label' => 'Tipe Pengenal (Lain)',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-md-12 hidden onoff',
                            'value' => ''
                        ]
                );
                ?>
            </div>
            <div class="row">
                <?php
                echo $this->form->text_h(
                        [
                            'name' => 'customer_birth_date',
                            'label' => 'Tanggal Lahir',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-sm-12 hidden onoff',
                            'value' => ''
                        ]
                );
                ?>

                <?php
                echo $this->form->select_h(
                        [
                            'name' => 'customer_gender',
                            'label' => 'Jenis Kelamin',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-md-12 hidden onoff',
                            'value' => [
                                ['id' => 1, 'item' => 'Laki-Laki'],
                                ['id' => 2, 'item' => 'Perempuan']
                            ],
                            'keys' => 'id',
                            'values' => 'item'
                        ]
                );
                ?>

                <?php
                echo $this->form->select_h(
                        [
                            'name' => 'customer_type',
                            'label' => 'Tipe Customer',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-md-12',
                            'value' => [
                                ['id' => 1, 'item' => 'Personal'],
                                ['id' => 2, 'item' => 'Perusahaan'],
                            ],
                            'keys' => 'id',
                            'values' => 'item',
                            'selected' => isset(temp_orders()['customers']->customer_type) ? temp_orders()['customers']->customer_type : ''
                        ]
                );
                ?>

                <?php
                echo $this->form->text_h(
                        [
                            'name' => 'customer_phone',
                            'label' => 'No. Telp *',
                            'max' => 100,
                            'required' => true,
                            'class' => 'col-md-12',
                            'value' => isset(temp_orders()['customers']->customer_phone) ? temp_orders()['customers']->customer_phone : ''
                        ]
                );
                ?>

            </div>
        </div>

        <div class="col-sm-6 form-horizontal">
            <h4 class="box-title">Informasi Alamat</h4>
            <div class="row">

                <div class="col-md-12">


                    <div class="row">
                        <?php
                        echo $this->form->select_h(
                                [
                                    'name' => 'customer_city',
                                    'label' => 'Kota',
                                    'max' => 100,
                                    'required' => false,
                                    'class' => 'select2 col-md-12',
                                    'value' => $cities,
                                    'keys' => 'city_id',
                                    'values' => 'city_name',
                                    'selected' => isset(temp_orders()['customers']->customer_city) ? temp_orders()['customers']->customer_city : ''
                                ]
                        );
                        ?>
                    </div>
                    <div class="row">
                        <?php
                        echo $this->form->textarea_h(
                                [
                                    'name' => 'customer_address',
                                    'label' => 'Alamat',
                                    'max' => 100,
                                    'required' => false,
                                    'value' => isset(temp_orders()['customers']->customer_address) ? temp_orders()['customers']->customer_address : '',
                                    'rows' => 3,
                                    'class' => 'col-sm-12'
                                ]
                        );
                        ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <hr />
                    <h4 class="box-title hidden onoff">Informasi Pengiriman</h4>
                    <div class="row">
                        <?php
                        echo $this->form->select_h(
                                [
                                    'name' => 'customer_freq_sending',
                                    'label' => 'Frekuensi Pengiriman Paket',
                                    'max' => 100,
                                    'required' => false,
                                    'class' => 'select2 col-md-12 hidden onoff',
                                    'value' => [
                                        ['id' => 1, 'value' => 'Harian'],
                                        ['id' => 2, 'value' => 'MIngguan'],
                                        ['id' => 3, 'value' => 'Bulanan'],
                                    ],
                                    'keys' => 'id',
                                    'values' => 'value',
                                ]
                        );
                        ?>

                        <?php
                        echo $this->form->text_h(
                                [
                                    'name' => 'customer_sending_quota',
                                    'label' => 'Jumlah Pengiriman Paket',
                                    'max' => 100,
                                    'required' => false,
                                    'value' => '',
                                    'class' => 'col-md-12 hidden onoff'
                                ]
                        );
                        ?>

                    </div>

                </div>

            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6 form-horizontal">
            <hr />
            <h4 class="box-title hidden onoff">Informasi Perusahaan</h4>
            <div class="row">
                <?php
                echo $this->form->text_h(
                        [
                            'name' => 'customer_company_name',
                            'label' => 'Nama Perusahaan',
                            'max' => 100,
                            'required' => false,
                            'value' => '',
                            'rows' => 3,
                            'class' => 'col-md-12 hidden onoff'
                        ]
                );
                ?>


                <?php
                echo $this->form->select_h(
                        [
                            'name' => 'customer_company_type',
                            'label' => 'Jenis Perusahaan',
                            'max' => 100,
                            'required' => false,
                            'class' => 'select2 col-md-12 hidden onoff',
                            'value' => [
                                ['id' => 1, 'item' => 'Manufaktur'],
                                ['id' => 2, 'item' => 'Jasa'],
                                ['id' => 3, 'item' => 'Ritel'],
                                ['id' => 4, 'item' => 'Lainnya'],
                            ],
                            'keys' => 'id',
                            'values' => 'item'
                        ]
                );
                ?>
                <?php
                echo $this->form->text_h(
                        [
                            'name' => 'customer_company_type_other',
                            'label' => 'Jenis Perusahaan Lainnya',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-md-12 hidden onoff',
                            'value' => ''
                        ]
                );
                ?>
            </div>

            <div class="row">
                <?php
                echo $this->form->text_h(
                        [
                            'name' => 'customer_web_id',
                            'label' => 'ID Web',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-md-12 hidden onoff',
                            'value' => ''
                        ]
                );
                ?>
            </div>




            <div class="row">

                <?php
                echo $this->form->text_h(
                        [
                            'name' => 'customer_company_telp',
                            'label' => 'Nomor Telepon Perusahaan',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-md-12 hidden onoff',
                            'value' => ''
                        ]
                );
                ?>

                <?php
                echo $this->form->text_h(
                        [
                            'name' => 'customer_company_email',
                            'label' => 'Alamat Email Perusahaan',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-md-12 hidden onoff',
                            'value' => ''
                        ]
                );
                ?>

            </div>

            <div class="row">
                <?php
                echo $this->form->textarea_h(
                        [
                            'name' => 'customer_company_address',
                            'label' => 'Alamat Perusahaan',
                            'max' => 100,
                            'required' => false,
                            'class' => 'col-md-12 hidden onoff',
                            'value' => '',
                            'rows' => 5
                        ]
                );
                ?>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="#" class="btn btn-block btn-lg btn-primary btn-next-step1">Lanjutkan Ke Step 2</a>
        </div>
    </div>

</div>