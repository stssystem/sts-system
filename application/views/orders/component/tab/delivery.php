<div class="tab-pane" id="tab_pengiriman">
    <div class="row form-order">

        <input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
        <input type="hidden" name ="total" />
        <input type="hidden" name ="package_id" />
        <input type="hidden" name ="customer_id" value="<?php echo isset(temp_orders()['customers']->customer_id) ? temp_orders()['customers']->customer_id : ''; ?>" /> 

        <div class="col-md-12 form-horizontal">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">

                        <div class="col-md-12">
                            <h4>
                                Asal Pengiriman
                            </h4>
                            <hr/>
                        </div>

                        <?php
                        echo $this->form->select_h(
                                [
                                    'name' => 'province_id_origin',
                                    'label' => 'Provinsi',
                                    'max' => 100,
                                    'required' => true,
                                    'value' => $provinces,
                                    'keys' => 'province_id',
                                    'values' => 'province_name',
                                    'class' => 'select2 col-md-12 origin_geobranch',
                                    'selected' => isset(temp_delivery('order_origin_text')['province_id_origin']) ? temp_delivery('order_origin_text')['province_id_origin'] : ''
                                ]
                        );
                        ?>  

                        <?php
                        echo $this->form->select_h(
                                [
                                    'name' => 'city_id_origin',
                                    'label' => 'Kota *',
                                    'max' => 100,
                                    'required' => true,
                                    'value' => $cities,
                                    'keys' => 'city_id',
                                    'values' => 'city_name',
                                    'class' => 'select2  col-md-12 select-price-params origin_geobranch',
                                    'selected' => isset(temp_delivery('order_origin_text')['city_id_origin']) ? temp_delivery('order_origin_text')['city_id_origin'] : ''
                                ]
                        );
                        ?>  


                        <?php
                        echo $this->form->select_h(
                                [
                                    'name' => 'districts_id_origin',
                                    'label' => 'Kecamatan',
                                    'max' => 100,
                                    'required' => false,
                                    'value' => [],
                                    'keys' => 'districts_id',
                                    'values' => 'districts_name',
                                    'class' => 'select2 col-md-12 select-price-params origin_geobranch',
                                    'selected' => isset(temp_delivery('order_origin_text')['districts_id_origin']) ? temp_delivery('order_origin_text')['districts_id_origin'] : ''
                                ]
                        );
                        ?>  

                        <?php
                        echo $this->form->select_h(
                                [
                                    'name' => 'village_id_origin',
                                    'label' => 'Kelurahan',
                                    'max' => 100,
                                    'required' => false,
                                    'value' => [],
                                    'keys' => 'village_id',
                                    'values' => 'village_name',
                                    'class' => 'select2 col-md-12 select-price-params origin_geobranch',
                                    'selected' => isset(temp_delivery('order_origin_text')['village_id_origin']) ? temp_delivery('order_origin_text')['village_id_origin'] : ''
                                ]
                        );
                        ?>  

                        <?php
                        echo $this->form->cabang_h(
                                [
                                    'name' => 'branch_id_origin',
                                    'label' => 'Cabang / Agen *',
                                    'max' => 100,
                                    'required' => true,
                                    'value' => get_restricted_branches(),
                                    'keys' => 'branch_id',
                                    'values' => 'branch_name',
                                    'class' => 'select2 col-md-12 select-price-params origin_geobranch',
                                    'pal_name' => 'origin_branch_code',
                                    'selected' => (temp_delivery('order_origin')) ? temp_delivery('order_origin') : ''
                                ]
                        );
                        ?>  

                        <?php
                        $this->form->textarea_h(
                                [
                                    'name' => 'order_origin_text',
                                    'label' => 'Detail Pengirim',
                                    'max' => 100,
                                    'value' => isset(temp_delivery('order_origin_text')['order_origin_text']) ? temp_delivery('order_origin_text')['order_origin_text'] : '',
                                    'required' => false,
                                    'class' => 'select2 col-md-12 select-price-params origin_geobranch'
                                ]
                        );
                        ?>

                        <div class="form-group col-sm-12">
                            <label class="col-md-3 control-label" style="margin-top: -10px;">
                                Simpan Perubahan?
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="checkbox" value="1" name="save_update_origin" />
                                        &nbsp;&nbsp;
                                        <strong>Ya *</strong>    
                                    </div>
                                    <div class="col-md-9">
                                        <i>*) Data asal pengiriman pada kostumer ini akan di diperbarui jika Anda pilih 'Ya'</i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br/><br/><br/>

                        <div class="form-group col-md-12" style="margin-top: 20px;" >
                            <div class="col-md-3"></div>
                            <div class="col-md-9 has-warning" style="margin-top:-25px;" >
                                <strong class="help-block">Jarak cabang dengan lokasi pengirim : <span class="origin-distance">-</span></strong>    
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">

                        <div class="col-md-12">
                            <h4>
                                Tujuan Pengiriman
                            </h4>
                            <hr/>
                        </div>

                        <?php
                        echo $this->form->select_h(
                                [
                                    'name' => 'province_id_destination',
                                    'label' => 'Provinsi',
                                    'max' => 100,
                                    'required' => true,
                                    'value' => $provinces,
                                    'keys' => 'province_id',
                                    'values' => 'province_name',
                                    'class' => 'select2 col-md-12 destination_geobranch',
                                    'selected' => isset(temp_delivery('order_destination_text')['province_id_destination']) ? temp_delivery('order_destination_text')['province_id_destination'] : ''
                                ]
                        );
                        ?>  

                        <?php
                        echo $this->form->select_h(
                                [
                                    'name' => 'city_id_destination',
                                    'label' => 'Kota *',
                                    'max' => 100,
                                    'required' => true,
                                    'value' => $cities,
                                    'keys' => 'city_id',
                                    'values' => 'city_name',
                                    'class' => 'select2  col-md-12 select-price-params destination_geobranch',
                                    'selected' => isset(temp_delivery('order_destination_text')['city_id_destination']) ? temp_delivery('order_destination_text')['city_id_destination'] : ''
                                ]
                        );
                        ?>  

                        <?php
                        echo $this->form->select_h(
                                [
                                    'name' => 'districts_id_destination',
                                    'label' => 'Kecamatan',
                                    'max' => 100,
                                    'required' => false,
                                    'value' => [],
                                    'keys' => 'districts_id',
                                    'values' => 'districts_name',
                                    'class' => 'select2 col-md-12 select-price-params destination_geobranch',
                                    'selected' => isset(temp_delivery('order_destination_text')['districts_id_destination']) ? temp_delivery('order_destination_text')['districts_id_destination'] : ''
                                ]
                        );
                        ?>

                        <?php
                        echo $this->form->select_h(
                                [
                                    'name' => 'village_id_destination',
                                    'label' => 'Kelurahan',
                                    'max' => 100,
                                    'required' => false,
                                    'value' => [],
                                    'keys' => 'village_id',
                                    'values' => 'village_name',
                                    'class' => 'select2 col-md-12 select-price-params destination_geobranch',
                                    'selected' => isset(temp_delivery('order_destination_text')['village_id_destination']) ? temp_delivery('order_destination_text')['village_id_destination'] : ''
                                ]
                        );
                        ?>  

                        <?php
                        echo $this->form->cabang_h(
                                [
                                    'name' => 'branch_id_destination',
                                    'label' => 'Cabang / Agen*',
                                    'max' => 100,
                                    'required' => true,
                                    'value' => get_freely_branches(),
                                    'keys' => 'branch_id',
                                    'values' => 'branch_name',
                                    'class' => 'select2 col-md-12 select-price-params destination_geobranch',
                                    'pal_name' => 'destination_branch_code',
                                    'selected' => (temp_delivery('order_destination')) ? temp_delivery('order_destination') : ''
                                ]
                        );
                        ?>

                        <?php
                        echo $this->form->text_h(
                                [
                                    'name' => 'destination_name',
                                    'label' => 'Nama Tujuan',
                                    'max' => 100,
                                    'required' => false,
                                    'value' => isset(temp_orders()['orders']->destination_name) ? temp_orders()['orders']->destination_name : '',
                                    'class' => 'col-md-12'
                                ]
                        );
                        ?>

                        <?php
                        echo $this->form->text_h(
                                [
                                    'name' => 'destination_telp',
                                    'label' => 'Nomor Telp. Tujuan',
                                    'max' => 100,
                                    'required' => false,
                                    'value' => isset(temp_orders()['orders']->destination_telp) ? temp_orders()['orders']->destination_telp : '',
                                    'class' => 'col-md-12'
                                ]
                        );
                        ?>


                        <?php
                        $this->form->textarea_h(
                                [
                                    'name' => 'order_destination_text',
                                    'label' => 'Alamat Detail Tujuan',
                                    'max' => 100,
                                    'required' => false,
                                    'value' => isset(temp_delivery('order_destination_text')['order_destination_text']) ? temp_delivery('order_destination_text')['order_destination_text'] : '',
                                    'class' => 'select2 col-md-12 select-price-params destination_geobranch'
                                ]
                        );
                        ?>

                        <div class="form-group has-warning" >
                            <div class="col-md-3"></div>
                            <div class="col-md-9" style="margin-top:-25px;" >
                                <strong class="help-block">Jarak cabang dengan lokasi penerima : <span class="destination-distance">-</span></strong>    
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <hr />

                            <a href="#" class="btn btn-block btn-lg btn-primary btn-next-step2">Lanjutkan Ke Step 3</a>
                        </div>

                    </div>
                </div>
            </div>


        </div>

    </div>

</div>