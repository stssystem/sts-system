<div class="tab-pane" id="tab_extra">
    <div class="row">
        <input type="hidden" name="det_order_extra_id" />
        <?php
        echo $this->form->text(
                [
                    'name' => 'det_order_extra_name',
                    'label' => 'Nama Item',
                    'max' => 100,
                    'required' => false,
                    'class' => 'col-md-5',
                    'value' => '',
                    'type' => 'text'
                ]
        );
        ?>
        <?php
        echo $this->form->text(
                [
                    'name' => 'det_order_extra_total',
                    'label' => 'Biaya',
                    'max' => 999999999,
                    'required' => false,
                    'class' => 'col-md-5',
                    'value' => '',
                    'type' => 'number'
                ]
        );
        ?>
        <div class="col-md-2">
            <button style="margin-top: 24px;" type="button" class="btn btn-flat btn-info btn-order-extra btn-block">
                <i class="fa fa-pencil"></i>
                Tambahkan
            </button>
        </div>
    </div>
    
    <table class="table data-order-extra">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nama Biaya Ekstra</th>
                <th>Jumlah Biaya Extra</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody class="orderextra-actions">

        </tbody>
        <tfoot>
            <tr>
                <td colspan="2">
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-flat btn-default orderextra-select-all"><i class="fa fa-check"></i>  Pilih Semua</button>
                        <button type="button" class="btn btn-sm btn-flat btn-danger orderextra-delete-all" data-toggle="modal" data-target="#modal-delete-alert" ><i class="fa fa-trash"></i>  Hapus Masal</button>
                    </div>
                    <strong class="pull-right">Total</strong>
                </td>
                <th colspan="2">
                    Rp. <span class="total-order-extra"></span>
                </th>
            </tr>
        </tfoot>
    </table>

    <div class="row">
        <div class="col-md-6">
            <div class="btn-group">
                <a href="#" class="btn btn-danger btn-flat btn-cancel" data-toggle="modal" data-target="#modal-delete-alert">
                    <i class="fa fa-times-circle"></i>
                    Batal
                </a>
                <button class="btn btn-info btn-flat btn-preview" style="display: none" >
                    <i class="fa fa-search"></i>
                    Preview
                </button>
                <button type="submit" class="btn btn-primary btn-flat">
                    <i class="fa fa-print"></i>
                    Lihat Hasil
                </button>
            </div>
        </div>
    </div>

</div>