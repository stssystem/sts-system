<div class="alert alert-warning alert-dismissible alert-general" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-warning"></i> Peringatan!</strong> Kolom-kolom dengan tanda bintang (*) wajib diisi.
</div>

<?php if (error('package_list')): ?>
    <div class="alert alert-danger alert-dismissible alert-general" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong><i class="fa fa-warning"></i> Peringatan!</strong> Data paket Anda masih kosong
    </div>
<?php endif; ?>

<?php if (error('order_manual_temp')): ?>
    <div class="alert alert-danger alert-dismissible alert-general" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong><i class="fa fa-warning"></i> Peringatan!</strong> Nomor Manual <strong><?php echo old_input('order_manual_id'); ?></strong> Sudah Terpakai
    </div>
<?php endif; ?>

<?php if (error('order_manual_id')): ?>
    <div class="alert alert-danger alert-dismissible alert-general" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong><i class="fa fa-warning"></i> Peringatan!</strong> Nomor resi manual harus di isi
    </div>
<?php endif; ?>