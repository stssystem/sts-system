<input type="hidden" name="branch_now" value='<?php echo json_encode(branch()); ?>' />
<!-- Old input  -->
<!-- Province -->
<input type="hidden" name="old_province_id_origin" value="<?php echo old('old_province_id_origin'); ?>" />
<input type="hidden" name="old_province_id_destination" value="<?php echo old('old_province_id_destination'); ?>" />
<!-- Cities -->
<input type="hidden" name="old_city_id_origin" value="<?php echo old('city_id_origin'); ?>" />
<input type="hidden" name="old_city_id_destination" value="<?php echo old('city_id_destination'); ?>" />
<!-- Districts -->
<input type="hidden" name="old_districts_id_origin" value="<?php echo old('districts_id_origin'); ?>" />
<input type="hidden" name="old_districts_id_destination" value="<?php echo old('districts_id_destination'); ?>" />
<!-- Villages  -->
<input type="hidden" name="old_village_id_origin" value="<?php echo old('village_id_origin'); ?>" />
<input type="hidden" name="old_village_id_destination" value="<?php echo old('village_id_destination'); ?>" />
<!-- Branches -->
<input type="hidden" name="old_order_origin" value="<?php echo old('order_origin'); ?>" />
<input type="hidden" name="old_order_destination" value="<?php echo old('order_destination'); ?>" />

<!-- Size -->
<input type="hidden"  name="package_width_s" />
<input type="hidden"  name="package_height_s" />
<input type="hidden"  name="package_lenght_s" />

<!-- Branch geo location  -->
<input type="hidden"  name="origin_geobranch" />
<input type="hidden"  name="origin_geocustomer" />
<input type="hidden"  name="destination_geobranch" />
<input type="hidden"  name="destination_geocustomer" />
<input type="hidden"  name="mode" />
<input type="hidden"  name="pickup_order_session" value='<?php echo pickup_isset()? pickup_order_get() : ''; ?>' />
<input type="hidden" name="order_id" value="<?php echo $order_id; ?>"/>