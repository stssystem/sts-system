<style type="text/css">
    @page {
        size: A5;
        margin:0;
    }

    @media print {
        html, body {
            width: 118mm;
            height: 140mm;

            margin-top: 20px;
            margin-bottom: 20px;
            margin-left: 20px;
            margin-right: 20px;

        }
        /* ... the rest of the rules ... */
    }

    table {
        margin: 0 auto;
    }


</style>

<html>
    <body style="text-align: center;">
        <table style="width:100%;">
            <tr>
                <td style="font-size: 24px; text-align: center;">
                    <?php echo $order->branch_name_origin . ' - ' . $order->branch_name_destination; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img width="100%" src="<?php echo base_url('index.php/orders/orders/render/' . $order->order_id); ?>"/>
                </td>
            </tr>
        </table>
    </body>
</html>

<script type="text/javascript">
    window.print();
</script>

