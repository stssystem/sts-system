<style type="text/css">
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 210mm;
            height: 297mm;
        }
        /* ... the rest of the rules ... */
    }

    .logo{
        height:60px;
        width:60px;
    }
</style>

<html>
    <body>
        <table>
            <tr>
                <td>
                    <div class="logo">
                        <img src="<?php echo base_url('assets/public/sts-logo.png'); ?>" class="logo">
                    </div>
                </td>
                <td>
                    <div>
                        PT. SANTOSO TEGUH SAKTI
                    </div>
                    <div>
                        IZIN USAHA NO 558/SIPJT/DIRJEN/2000
                    </div>
                    <div>
                        JL. SOEKARNO HATTA 21 MAGELANG
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>