<?php $this->load->view('header');?>
	
	<div class="row">
		<div class="col-sm-6">
			<?php echo form_open('orders/collections/search_order'); ?>
			<div class="box">
		        <div class="box-header with-border">
		        	<h3 class="box-title">Pengambilan Paket Onsite</h3>

		         
		        </div>
		        <div class="box-body">
		          
					<div class="row">
						<div class="col-sm-12">
							<p>Masukkan Data Pencarian</p>
							
								<div class="row">
									<div class="col-sm-6">
						                <?php
						                echo $this->form->text(
					                        [
					                            'name' => 'order_id',
					                            'label' => 'Nomor Resi',
					                            'max' => 100,
					                            'required' => false
					                        ]
						                );
						                ?>
						            </div>
						            <div class="col-sm-6">
										<?php 
											echo $this->form->text(
												[
													'name' => 'customer_phone',
													'label' => 'Nama Customer',
													'max' => 100,
													'required' => false
												]
											)
										?>
						            </div>
						            <div class="col-sm-6">
										<?php 
											echo $this->form->text(
												[
													'name' => 'customer_phone',
													'label' => 'Telephone',
													'max' => 100,
													'required' => false
												]
											);
										?>
						            </div>
						            
						        </div>
							
						</div>

					</div>

		        
		        <!-- /.box-body -->
			        <div class="box-footer">
						<div class="col-sm-12">
							<button class="btn pull-right btn-primary"><i class="fa fa-search"></i> Cari</button>
			            </div>
			        </div>
		      	</div>
		    </div>
		   	<?php echo form_close();?>
		</div>
	</div>

<?php $this->load->view('footer');?>