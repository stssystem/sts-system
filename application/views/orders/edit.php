<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open($path . '/update', ['class' => 'form-edit']); ?>

                <input type="hidden" name="city_id" value="<?php echo $data->city_id ?>" />

                <?php
                echo $this->form->text(
                        [
                            'name' => 'city_name',
                            'label' => 'Nama Kota',
                            'max' => 100,
                            'required' => true,
                            'value' => $data->city_name
                        ]
                );
                ?>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url($path); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <a type="button" class="btn-info btn btn-save" data-toggle="modal" data-target="#modal-save-alert">
                            <i class="fa fa-save"></i>
                            Simpan
                        </a>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

    $('.btn-save').click(function () {

        var city_name = $('.form-edit').find('input[name="city_name"]').val();

        if (city_name == '') {

            /** Set message */
            var text = "Nama kota harus diisi";

        } else {

            /** Set message */
            var text = "Apakah Anda akan menyimpan nama kota <strong>" + city_name + "</strong>?";

        }

        /** Set modal */
        $('#modal-save-alert').find('.modal-body p').html(text);

    });

    $('.alert-btn-save').click(function () {
        $('.form-edit').submit();
    });

</script>