<!--Data Table-->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Numeral -->
<script src="<?php echo base_url(); ?>assets/plugins/numeral/numeral.min.js"></script>

<!-- WebCam -->
<script src="<?php echo base_url(); ?>assets/plugins/webcam/webcam.min.js"></script>

<!-- Easy Autocomplete -->
<script src="<?php echo base_url(); ?>assets/plugins/easy-autocomplete/jquery.easy-autocomplete.min.js"></script>

<!-- Easy Autocomplete -->
<script src="<?php echo base_url(); ?>assets/plugins/momentjs/moment.min.js"></script>

<!-- Math.JS -->
<script src="<?php echo base_url(); ?>assets/plugins/mathjs/math.js"></script>

<!-- Jquery Session -->
<script src="<?php echo base_url(); ?>assets/plugins/jsession/jsession.js"></script>

<!-- Input Mask -->
<script src="<?php echo base_url(); ?>assets/plugins/jn-input-mask/js/jasny-bootstrap.js"></script>

<script src="<?php echo base_url(); ?>assets/js/application/orders/global_variable.js"></script>
<script src="<?php echo base_url(); ?>assets/js/application/orders/collect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/application/orders/validation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/application/orders/packages/totalize.js"></script>
<script src="<?php echo base_url(); ?>assets/js/application/orders/packages/request.js"></script>
<script src="<?php echo base_url(); ?>assets/js/application/orders/delivery/maps.js"></script>

<!-- Map api -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0rbsGWnTpB_XUFNahoj710PZNvFvrqXI&v=3&callback=initialize" async defer></script>

<script type="text/javascript" >

    /** URL pass */
    var url_order_helper_price = '<?php echo site_url('request/order_helper/price'); ?>';
    var url_orders_store = '<?php echo site_url('orders/orders/store'); ?>';
    var url_destroy_order_extra = '<?php echo site_url('orders/orders/destroy_order_extra'); ?>';
    var url_geo_branch = '<?php echo site_url('request/locations/get_geobranch'); ?>';
    var url_image_list = '<?php echo site_url('orders/orders/get_image_list'); ?>';
    var url_update_package = '<?php echo site_url('orders/orders/update'); ?>';

    geo_graps();
    geo_destination_graps();
    bind_change_event();
    data_tables();
    order_extra_data_table();

    /** Select 2 */
    $(".select2").find('select').select2();

    /** Reactivate Select 2 */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(".select2").find('select').select2();
    });

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus kota <strong>" + data['city_name'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);
        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['city_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });


    function bind_change_event() {
        $('body').on('change', '.select-price-params', function () {
            setTimeout(function () {
                grap_price_params();
            }, 1000);
        });
    }

    /** Datatable for package */
    function data_tables() {

        $(".data-tables").dataTable().fnDestroy();

        $('.data-tables').DataTable({
            "paging": false,
            "searching": false,
            "ordering": false,
            "info": false,
            ajax: {
                url: '<?php echo site_url($path . '/index/' . $order_id); ?>',
                dataSrc: ''
            },
            columns: [
                {data: 'package_id'},
                {data: 'package_title'},
                {data: 'package_type'},
                {data: 'package_weight'},
                {data: 'qty'},
                {data: 'package_weight_total'},
                {data: 'total_volume'},
                {data: 'action'}
            ],
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

                // Total Berat
                var weight = api
                        .column(3)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // // Total Koli
                var qty = api
                        .column(4)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Total -> Total Berat
                var weight_total = api
                        .column(5)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Total Volume
                var volume = api
                        .column(6)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Update footer
                $(api.column(2).footer()).html('Total : ');
                $(api.column(3).footer()).html(weight + ' kg');
                $(api.column(4).footer()).html(qty + ' koli');
                $(api.column(5).footer()).html(weight_total + ' kg');
                $(api.column(6).footer()).html(volume.toFixed(2) + ' m3');
            }
        });

        setTimeout(function () {
            var total_orders = 0;
            $('input[name="total_orders"]').each(function () {
                total_orders += parseInt($(this).val());
            });

            var sell_total_order = 0;
            $('input[name="sell_total_orders"]').each(function () {
                sell_total_order += parseInt($(this).val());
            });

            $('.total_orders').empty();
            $('.total_orders').text(numeral(total_orders).format('0,0'));

            $('.total-all').empty();
            $('.total-all').text(numeral(total_orders).format('0,0'));

            $('.total-all-customer').empty();
            $('.total-all-customer').val(sell_total_order);

            $('.sell_total_orders').empty();
            $('.sell_total_orders').text(numeral(sell_total_order).format('0,0'));

        }, 3000);

    }

    /** Datatable for extra order */
    function order_extra_data_table() {

        total_extra_order_();

        $(".data-order-extra").dataTable().fnDestroy();

        $('.data-order-extra').DataTable({
            "paging": false,
            "searching": false,
            "ordering": false,
            "info": false,
            ajax: {
                url: '<?php echo site_url($path . '/get_orders_extra/' . $order_id) ?>',
                dataSrc: ''
            },
            columns: [
                {data: 'det_order_extra_id'},
                {data: 'det_order_extra_name'},
                {data: 'det_order_extra_total'},
                {data: 'action'}
            ]
        });


    }

    $('.input-price-params').on('keyup change', function () {
        grap_price_params();
    });

    /** Cancel confirmation */
    $('body').on('click', '.btn-cancel', function () {

        /** Set modal */
        var text = "Apakah Anda yakin akan membatalkan order?";
        $('#modal-delete-alert').find('.modal-body p').html(text);

        /** Set url */
        var url = "<?php echo site_url($path . '/cancel/' . $order_id) ?>";
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    // Next Step 1
    $('.btn-next-step1').click(function () {
        $('a[href="#tab_pengiriman"]').trigger('click');
    });

    $('.btn-next-step2').click(function () {
        $('a[href="#tab_paket"]').trigger('click');
    });

    $('.btn-next-step3').click(function () {
        $('a[href="#tab_extra"]').trigger('click');
    });

    $('#show_advanced').click(function () {
        $('.onoff').removeClass('hidden');
    });

    /** Reset auto complete width */
    setTimeout(function () {
        $('.eac-square').removeAttr('style');
    }, 1000);

    $('a[href="#tab_extra"]').on('shown.bs.tab', function (e) {
        total_extra_order_();
    });

    /** Total order extra */
    $('.hide-click').click(function (event) {
        total_extra_order_();
    });

    function total_extra_order_() {

        setInterval(function () {

            /** Total Order Extra */
            var total_order_exra = $('input[name="total_order_extra"]').last().val();
            $('.total-order-extra').empty();
            $('.total-order-extra').text(numeral(total_order_exra).format('0,0'));

        }, 1000);

    }
    
     // Create server event
    function update_price() {
        $.post("<?php echo site_url('orders/orders/update_price'); ?>", {
            branch_origin: $('select[name="branch_id_origin"] option:selected').val(),
            branch_destination: $('select[name="branch_id_destination"] option:selected').val(),
            order_id : $('input[name="order_id"]').val()
        }).done(function () {
            data_tables();            
        });
    }

    // Preview button
    $('.btn-preview').click(function () {

    });

</script>

<?php $this->load->view('orders/component/js/delivery_origin'); ?>
<?php $this->load->view('orders/component/js/delivery_destination'); ?>
<?php $this->load->view('orders/component/js/package_webcams'); ?>
<?php $this->load->view('orders/component/js/customers'); ?>
<?php $this->load->view('orders/component/js/order_extra'); ?>
<?php $this->load->view('orders/component/js/package'); ?>
<?php $this->load->view('orders/component/js/package_select_all'); ?>
<?php $this->load->view('orders/component/js/order_extra_select_all'); ?>
<?php $this->load->view('orders/component/js/general'); ?>
<?php $this->load->view('orders/component/js/general_repopulate'); ?>

<script type="text/javascript">

    // Insert default address of customers 
    // Next Step 1
    $('a[href="#tab_pengiriman"]').click(function () {

        var content = $('textarea[name="customer_address"]').val();
        var city_selected = $('select[name="customer_city"] option:selected').val();

        if (($.session.get('customers_origin_province') == '' && $.session.get('customers_origin_city') == '') || $.session.get('customers_origin_city') != city_selected) {

            if ($("select[name='city_id_origin']").val() == '') {

                $("select[name='city_id_origin']").val(city_selected);

                get_origin_districts(city_selected);
                get_origin_village();

                $.post('<?php echo site_url('request/locations/get_province_id'); ?>', {city_id: city_selected}, function (data) {
                    $('select[name="province_id_origin"]').val(data);
                    $('select[name="province_id_origin"]').select2();
                });

            }

            if ($("textarea[name='order_origin_text']").val() == '') {
                $('textarea[name="order_origin_text"]').val(content);
            }

        }

    });

</script>
