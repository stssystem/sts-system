<?php $this->load->view('header');?>
	<div class="row">
		<div class="col-sm-3">
			
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Pengirim</h3>
				</div>
		        <div class="box-body box-profile">
	              
	              	<h3 class="profile-username text-center">Nama Pengirimnya</h3>

	              

		             <ul class="list-group list-group-unbordered">
		                <li class="list-group-item">
		                	<b>Kota</b> <a class="pull-right">Bandung</a>
		                </li>
		                
		                <li class="list-group-item">
		                  <b>Kecamatan</b> <a class="pull-right">Soreang</a>
		                </li>
						
						<li class="list-group-item">
							<b>Desa/Kelurahan</b> <a class="pull-right">Beji</a>
						</li>

		            </ul>
		           
		            <p>Jl. Duku Nomor 5, Kelurahan Bajik, Sorengan</p>
				
	             
            	</div>
			</div>

			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Tujuan</h3>
				</div>
		        <div class="box-body box-profile">
	              
	              	<h3 class="profile-username text-center">Nama Penerima Seharusnya</h3>

	              

		             <ul class="list-group list-group-unbordered">
		                <li class="list-group-item">
		                	<b>Kota</b> <a class="pull-right">Semarang</a>
		                </li>
		                
		                <li class="list-group-item">
		                  <b>Kecamatan</b> <a class="pull-right">Gunung Pati</a>
		                </li>
						
						<li class="list-group-item">
							<b>Desa/Kelurahan</b> <a class="pull-right">Sukorejo</a>
						</li>

		            </ul>
		           
		            <p>Jl. Dewi Sartika Timur V Perum A5</p>
				
	             
            	</div>
			</div>
		</div>
		<div class="col-sm-9">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Paket</h3>
				</div>
				<div class="box-body">
					

					<table class="table">
						<thead>
							<tr><th>No.</th><th>Deskripsi</th><th>PxLxT</th><th>Berat</th><th class="text-right">Biaya</th></tr>
						</thead>
						<tbody>
							<tr><td>1</td><td>Kacang Garam</td><td>1x2x3 cm</td><td>20kg</td><td class="text-right">80.000</td></tr>
							<tr><td>2</td><td>Handphone Samsung</td><td>1x2x3 cm</td><td>20kg</td><td class="text-right">90.000</td></tr>
							<tr><td class="text-right" colspan="4">Total</td><td class="text-right"><strong>170.000</strong></td></tr>
						</tbody>
					</table>
				</div>
			</div>


			<div class="box box-danger">
				<div class="box-header">
					<h3 class="box-title">Identitas Penerima Final</h3>
				</div>
				<div class="box-body">
					<div class="row">
						
						<div class="col-sm-6">
							<?php 
								echo $this->form->text(
					                        [
					                            'name' => 'recipent_name',
					                            'label' => 'Nama Penerima',
					                            'max' => 100,
					                            'required' => true
					                        ]
						                );
							?>

						</div>
						<div class="col-sm-6">
							<?php 
								echo $this->form->text(
					                        [
					                            'name' => 'recipent_phone',
					                            'label' => 'Nomor Telepon',
					                            'max' => 100,
					                            'required' => true
					                        ]
						                );
							?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<a href="#" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Selesai dan Cetak</a>
							<a href="#" target="_blank" class="btn btn-primary"><i class="fa fa-close"></i> Selesai Tanpa Cetak</a>
						
	         			</div>
         			</div>
				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('footer');?>