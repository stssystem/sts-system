<style type="text/css">
    @page {
        size: A5;
        margin:0;
    }

    @media print {
        html, body {
            width: 210mm;
            height: 297mm;

            margin-top: 20px;
            margin-bottom: 20px;
            margin-left: 20px;
            margin-right: 20px;

        }
        /* ... the rest of the rules ... */
    }

    .logo{
        height:60px;
        width:60px;
    }

    .address{
        font-size: 12px;
    }

    .noted{
        font-size: 13px;
    }


    .body-div{
        width: 195mm;
    }

    .aligns-bottom{
        vertical-align: bottom;
    }

    .aligns-right{
        text-align: left;
    }


</style>

<html>
    <body>
        <div class="body-div">
            <table>
                <tr>
                    <th>
                <div class="logo">
                    <img src="<?php echo base_url('assets/public/sts-logo.png'); ?>" class="logo">
                </div>
                </th>

                <td class="address" width="60%" >
                    <div>
                        PT. SANTOSO TEGUH SAKTI
                    </div>
                    <div>
                        JL. SOEKARNO HATTA 21 MAGELANG
                    </div>
                    <div>
                        TELP. 0293-364040
                    </div>
                </td>

                <td class="noted aligns-bottom" width="40%" >
                    Tanggal : <?php echo mdate('%d %F %Y', $order->order_date); ?>
                </td>

                </tr>

            </table>
            <hr/>

            <table width="100%" >
                <tr>
                    <td width="60%">
                        <table>
                            <tr class="noted">
                                <td colspan="3" >Identitas Pengirim</td>
                            </tr>
                            <tr class="noted">
                                <td width="20%">Nama</td>
                                <td>:</td>
                                <td><?php echo $customer->customer_name; ?></td>
                            </tr>
                            <tr class="noted" >
                                <td>Alamat </td>
                                <td>:</td>
                                <td><?php echo $customer->customer_address; ?></td>
                            </tr>
                        </table>
                    </td>
                    <td width="40%">
                        <table>
                            <tr class="noted">
                                <td colspan="3">Identitas Tujuan</td>
                            </tr>
                            <tr class="noted" >
                                <td width="20%" >Nama</td>
                                <td>:</td>
                                <td><?php echo $order->destination_name; ?></td>
                            </tr>
                            <tr class="noted">
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?php echo json_decode($order->order_destination_text, true)['order_destination_text']; ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <h4>Biaya Paket</h4>
            <table border="1" width="100%">
                <tr>
                    <th width="20%" >Nama Paket</th>
                    <th width="15%">Jenis Paket</th>
                    <th width="15%">Dimensi Paket</th>
                    <th width="15%">Berat Paket</th>
                    <th width="15%">Jumlah Paket</th>
                    <th width="20%" >Total</th>
                </tr>

                <?php $total = 0; ?>
                <?php if (!empty($package)): ?>
                    <?php foreach ($package as $key => $value) : ?>
                        <?php if ($value->package_parent == $value->package_id): ?>
                            <tr>
                                <td><?php echo $value->package_title; ?></td>
                                <td><?php echo package_type($value->package_type); ?></td>
                                <td>
                                    <?php echo $value->package_size * $value->qty; ?> cm3
                                </td>
                                <td>
                                    <?php echo $value->package_weight * $value->qty; ?> kg
                                </td>
                                <td>
                                    <?php echo $value->qty; ?> Item
                                </td>
                                <td>
                                    Rp. <?php echo number_format($value->total * $value->qty); ?>
                                    <?php $total += ($value->total * $value->qty); ?>
                                </td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>

                <tr>
                    <th colspan="5">Total</th>
                    <th >Rp. <?php echo number_format($total); ?></th>
                </tr>

            </table>

            <?php $total_extra = 0; ?>
            <?php if (!empty($det_orders_extra)): ?>
                <h4>Biaya Tambahan</h4>
                <table border="1" width="100%">
                    <tr>
                        <th width="70%" >Nama Item</th>
                        <th width="30%">Biaya</th>
                    </tr>
                    <?php if (!empty($det_orders_extra)): ?>
                        <?php foreach ($det_orders_extra as $key => $value) : ?>
                            <tr>
                                <td><?php echo $value->det_order_extra_name; ?></td>
                                <td>
                                    Rp. <?php echo number_format($value->det_order_extra_total); ?>
                                    <?php $total_extra += $value->det_order_extra_total; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>

                    <tr>
                        <th >Total</th>
                        <th >Rp. <?php echo number_format($total_extra); ?></th>
                    </tr>

                </table>

            <?php endif; ?>

            <br/>

            <hr/>
            <table >
                <tr>
                    <td class="aligns-right" colspan="3"><h4>Total Biaya </h4></td>
                </tr>
                <tr>
                    <th class="aligns-right" width="40%" >Biaya Paket </th>
                    <th>:</th>
                    <th width="40%" class="aligns-right" >Rp. <?php echo number_format($total); ?></th>
                </tr>
                <tr>
                    <th class="aligns-right" >Biaya Tambahan </th>
                    <th>:</th>
                    <th class="aligns-right" >Rp. <?php echo number_format($total_extra); ?></th>
                </tr>
                <tr>
                    <th class="aligns-right" >Total Biaya </th>
                    <th>:</th>
                    <th class="aligns-right" >Rp. <?php echo number_format($total + $total_extra); ?></th>
                </tr>
            </table>

        </div>
    </body>
</html>

<script type="text/javascript">
    window.print();

</script>

