<?php $this->load->view('header');?>
	
	<div class="row">
		<div class="col-sm-12">
			<div class="box">
				<div class="box-header">
					<h4 class="box-title">Daftar Order</h4>
				</div>
				<div class="box-body">
					<table class="table">
						<thead>
							<tr><th>#ID</th><th>Nama</th><th>Asal</th><th>Tujuan</th><th>Tanggal Masuk</th><th>Total</th><th>Detail</th></tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('footer');?>