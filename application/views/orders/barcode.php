<style type="text/css">
    @page {
        size: A5;
        margin:0;
    }

    @media print {
        html, body {
            width: 118mm;
            height: 140mm;

            margin-top: 20px;
            margin-bottom: 20px;
            margin-left: 20px;
            margin-right: 20px;

        }
        /* ... the rest of the rules ... */
    }


</style>

<html>
    <body>
        <div class="body-div">
            <table style="width:100%;" >
                <?php if (isset($codes)): ?>
                    <?php if (!empty($codes)): ?>
                        <?php foreach ($codes as $key => $value) : ?>
                            <tr >
                                <td style="font-size: 10px; text-align: center;">
                                    <?php echo $data->branch_name_origin . ' - ' . $data->branch_name_destination; ?>
                                </td>
                            </tr>
                            <tr >
                                <td style="text-align: center;">
                                    <img width="100%" src="<?php echo base_url('index.php/orders/orders/render/' . $value->package_id); ?>"/>
                                </td>
                            </tr>
                            <tr><td></td></tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php else: ?>
                    <tr>
                        <td style="font-size: 10px; text-align: center;">
                            <?php echo $data->origin_branch_name . ' - ' . $data->destination_branch_name; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <img width="100%" src="<?php echo base_url('index.php/orders/orders/render/' . $code); ?>"/>
                        </td>
                    </tr>
                <?php endif; ?>

            </table>
        </div>
    </body>
</html>

<script type="text/javascript">
    window.print();
</script>

