<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
    
    .txt-mn{
        font-size: 10px;
    }
</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>
<?php $this->load->view('order/modal/closed_final'); ?>
<?php $this->load->view('order/modal/self_take'); ?>
<?php $this->load->view('order/modal/delivery'); ?>
<?php $this->load->view('order/modal/package_rollback'); ?>

<div class="row">

    <div class="col-sm-12">
        <?php get_alert(); ?>
    </div>

    <div class="col-sm-12">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#must-be-processed" data-toggle="tab" aria-expanded="false">
                        Harus Di Proses
                    </a>
                </li>
                <li class="">
                    <a href="#be-received" data-toggle="tab" aria-expanded="false">
                        Akan Diterima
                    </a>
                </li>
                <li class="">
                    <a href="#pack-drops" data-toggle="tab" aria-expanded="false">
                        Paket Turun
                    </a>
                </li>
                <li class="">
                    <a href="#delivery" data-toggle="tab" aria-expanded="false">
                        Pengantaran
                    </a>
                </li>
                <li class="">
                    <a href="#in-the-process" data-toggle="tab" aria-expanded="false">
                        Dalam Proses
                    </a>
                </li>
                <li class="" >
                    <a href="#finish" data-toggle="tab" aria-expanded="false">
                        Selesai
                    </a>
                </li>
                <li class="" style="display: none;" >
                    <a href="#not-pay" data-toggle="tab" aria-expanded="false">
                        Belum Bayar
                    </a>
                </li>
                <li class="pull-right">

                    <?php if (profile()->user_type_id == 99 || profile()->branch_access != '' || profile()->regional_id != ''): ?>

                        <?php echo form_open($path . '/filter'); ?>

                        <select name="as_branch" class="select2 form-control">
                            <option>-- Lihat Sebagai Cabang --</option>

                            <?php
                            if (profile()->branch_access == '' && profile()->regional_id != '') {
                                $branches_list = get_branches_list(null, profile()->regional_id);
                            } else {
                                $branches_list = get_branches_list(null);
                            }
                            ?>

                            <?php if (!empty($branches_list)): ?>
                                <?php foreach ($branches_list as $key => $item): ?>
                                    <option 
                                        value="<?php echo $item['branch_id']; ?>"
                                        <?php echo (isset($this->session->userdata('order_list_filter')['as_branch']) && $this->session->userdata('order_list_filter')['as_branch'] == $item['branch_id']) ? 'selected="selected"' : '' ?>
                                        >
                                            <?php echo $item['branch_name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>

                        <button class="btn btn-info btn-flat" type="submit">
                            <i class="fa fa-search"></i>
                            Lihat
                        </button>

                        <a href="<?php echo site_url($path . '/reset_status_filter'); ?>" class="btn btn-warning btn-flat btn-clear" >
                            <i class="fa fa-refresh"></i>
                            Reset
                        </a>

                        <?php echo form_close(); ?>

                    <?php endif; ?>

                </li>
            </ul>

            <?php $this->load->view('order/tab-content'); ?>

        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>


<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
<!-- Numeral -->
<script src="<?php echo base_url(); ?>assets/plugins/numeral/numeral.min.js"></script>
<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<?php $this->load->view('order/index-js'); ?>
