<div class="row">
    <?php echo form_open($path . '/status_filter_inprocess'); ?>
    <div class="col-md-2">
        <select class="form-control" name="status_filter_inprocess">
            <option value="">-- Semua Status --</option>
            <option value="3" <?php echo $this->session->userdata('status_filter_inprocess') == 3 ? 'selected="selected"' : ''; ?> >Belum Naik</option>
            <option value="4" <?php echo $this->session->userdata('status_filter_inprocess') == 4 ? 'selected="selected"' : ''; ?> >Sudah Naik</option>
            <option value="5" <?php echo $this->session->userdata('status_filter_inprocess') == 5 ? 'selected="selected"' : ''; ?> >Sudah Diturunkan</option>
            <option value="6" <?php echo $this->session->userdata('status_filter_inprocess') == 6 ? 'selected="selected"' : ''; ?> >Barang Transit</option>
            <option value="7" <?php echo $this->session->userdata('status_filter_inprocess') == 7 ? 'selected="selected"' : ''; ?> >Pengantaran</option>
        </select>
    </div>
    <button class="btn btn-info btn-flat col-md-1" >
        <i class="fa fa-filter"></i>
        Filter
    </button>
    
    <?php echo form_close(); ?>
</div>
<hr/>