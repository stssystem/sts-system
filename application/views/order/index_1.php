<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>
<?php $this->load->view('order/modal/closed_final'); ?>

<div class="row">

    <div class="col-sm-12">
        <?php get_alert(); ?>
    </div>

    <div class="col-sm-12">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#0" data-toggle="tab" aria-expanded="false">
                        Harus Di Proses
                    </a>
                </li>
                <li class="">
                    <a href="#4" data-toggle="tab" aria-expanded="false">
                        Akan Diterima
                    </a>
                </li>
                <li class="">
                    <a href="#5" data-toggle="tab" aria-expanded="false">
                        Paket Turun
                    </a>
                </li>
                <li class="">
                    <a href="#6" data-toggle="tab" aria-expanded="false">
                        Pengantaran
                    </a>
                </li>
                <li class="">
                    <a href="#1" data-toggle="tab" aria-expanded="false">
                        Dalam Proses
                    </a>
                </li>
                <li class="">
                    <a href="#2" data-toggle="tab" aria-expanded="false">
                        Selesai
                    </a>
                </li>
                <li class="">
                    <a href="#3" data-toggle="tab" aria-expanded="false">
                        Belum Bayar
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab">
                    <table class="table data-tables">
                        <thead>
                            <tr class="txt-sm">
                                <th width="10%" >#ID</th>
                                <th width="13%" >Nama</th>
                                <th width="20%" >Asal</th>
                                <th width="20%" >Tujuan</th>
                                <th width="8%" >Tanggal Masuk</th>
                                <th width="8%" >Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="txt-sm">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>


<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    /** New order list */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        target = target.replace("#", "");
        if (target == 4) {
            $('.th-temp').remove();
        } else {
            var html = '<th width="7%" class="th-temp" >Total</th>'
                    + '<th width="7%" class="th-temp" >Total Ekstra</th>'
                    + '<th width="7%" class="th-temp" >Jumlah</th>';
            $('.pre-temp').after(html);
        }
        data_tables(target);
    });

    /** Data tables set up */
    data_tables(0);

    function data_tables(status) {

        var url = '<?php echo site_url($path) ?>/index/' + status;

        $(".data-tables").dataTable().fnDestroy();

        $('.data-tables').DataTable({
            serverSide: true,
            ordering: false,
            ajax: {
                url: url,
                type: 'POST'
            },
            columns: [
                {data: 'order_id'},
                {data: 'customer_name'},
                {data: 'order_origin'},
                {data: 'order_destination'},
                {data: 'created_at'},
                {data: 'action'}
            ]
        });
    }

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus data order dengan nomor resi <strong>" + data['order_id'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);

        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['order_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });

    /** Close order */
    $('body').on('click', '.btn-closed-final', function () {
        var data_edit = $(this).parent().parent().find('input[name="edit-value"]').val();
        data_edit = JSON.parse(data_edit);
        /** Set url */
        var url = "<?php echo site_url($path . '/paid'); ?>/" + data_edit['order_id'];
        $('#closed_final').find('.modal-footer a.a-closed').attr('href', url);
    });


</script>