<style type="text/css">
    .footer-background{
        background-color: #bdbdbd;
    }
</style>

<div class="tab-content">

    <div class="tab-pane active table-responsive" id="must-be-processed">
   
        <div class="row">
            <div class="col-md-12">
                <table class="table data-tables must-be-processed">
                    <thead>
                        <tr class="txt-sm">
                            <th width="8%" >Tanggal Resi</th>
                            <th width="8%" >#ID</th>
                            <th width="19%" >Nama</th>
                            <th width="23%" >Asal - Tujuan</th>
                            <th width="8%">Jumlah Koli</th>
                            <th width="8%">Total Berat <br/>( Kg )</th>
                            <th width="8%">Volume <br/>( M<sup>3</sup> )</th>
                            <th width="8%">Usia Resi <br/>( Hari )</th>
                            <th width="10%" >Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="txt-sm">

                    </tbody>
                    <tfoot class="footer-background">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th colspan="2"></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th colspan="3" class="total-package" ></th>
                            <th colspan="2" class="total-weight" ></th>
                            <th colspan="4" class="total-size" ></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="tab-pane table-responsive" id="be-received">

        <?php $this->load->view('order/filter_will_drop'); ?>

        <div class="row">
            <div class="col-md-12">
                <table class="table data-tables be-received">
                    <thead>
                        <tr class="txt-sm">
                            <th width="8%" >Tanggal Resi</th>
                            <th width="8%" >Tanggal Naik</th>
                            <th width="8%" >#ID</th>
                            <th width="14%" >Nama</th>
                            <th width="20%" >Asal - Tujuan</th>
                            <th width="7%">Jumlah Koli</th>
                            <th width="7%">Total Berat <br/>( Kg ) </th>
                            <th width="7%">Volume <br/>( M<sup>3</sup> )</th>
                            <th width="7%">Usia Resi <br/>( Hari )</th>
                            <th width="6%">Status</th>
                            <th width="8%" >Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="txt-sm">

                    </tbody>
                    <tfoot class="footer-background" >
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th colspan="4" ></th>
                        </tr>
                        <tr>
                            <th colspan="4" class="total-package" ></th>
                            <th colspan="2" class="total-weight" ></th>
                            <th colspan="5" class="total-size" ></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="tab-pane table-responsive" id="pack-drops">

      
        <div class="row">
            <div class="col-md-12">
                <table class="table data-tables pack-drops">
                    <thead>
                        <tr class="txt-sm">
                            <th width="8%" >Tanggal Resi</th>
                            <th width="8%" >Tanggal Naik</th>
                            <th width="8%" >Tanggal Turun</th>
                            <th width="8%" >#ID</th>
                            <th width="15%" >Nama</th>
                            <th width="20%" >Asal - Tujuan</th>
                            <th width="5%">Jumlah Koli</th>
                            <th width="5%">Total Berat <br/>( Kg )</th>
                            <th width="5%">Volume <br/>( M<sup>3</sup> )</th>
                            <th width="5%">Usia Resi <br/>( Hari )</th>
                            <th width="14%" >Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="txt-sm">

                    </tbody>
                    <tfoot class="footer-background" >
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th colspan="2" ></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th colspan="5" class="total-package" ></th>
                            <th colspan="2" class="total-weight" ></th>
                            <th colspan="4" class="total-size" ></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="tab-pane table-responsive" id="delivery">
        <div class="row">
            <div class="col-md-12">
                <table class="table data-tables delivery">
                    <thead>
                        <tr class="txt-sm">
                            <th width="8%" >Tanggal Resi</th>
                            <th width="8%" >Tanggal Antar</th>
                            <th width="8%" >#ID</th>
                            <th width="13%" >Nama</th>
                            <th width="15%" >Asal - Tujuan</th>
                            <th width="8%">Jumlah Koli</th>
                            <th width="8%">Total Berat <br/>( Kg )</th>
                            <th width="8%">Volume <br/>( M<sup>3</sup> )</th>
                            <th width="8%">Usia Resi <br/>( Hari )</th>
                            <th width="20%" >Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="txt-sm">

                    </tbody>
                    <tfoot class="footer-background" >
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th colspan="2" ></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th colspan="4" class="total-package" ></th>
                            <th colspan="2" class="total-weight" ></th>
                            <th colspan="4" class="total-size" ></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="tab-pane table-responsive" id="in-the-process">
        <?php $this->load->view('order/filter_in_process'); ?>
        <div class="row">
            <div class="col-md-12">
                <table class="table data-tables in-the-process">
                    <thead>
                        <tr class="txt-sm">
                            <th width="8%" >Tanggal Resi</th>
                            <th width="8%" >#ID</th>
                            <th width="14%" >Nama</th>
                            <th width="20%" >Asal - Tujuan</th>
                            <th width="7%">Jumlah Koli</th>
                            <th width="7%">Total Berat <br/>( Kg ) </th>
                            <th width="7%">Volume <br/>( M<sup>3</sup> )</th>
                            <th width="7%">Usia Resi <br/>( Hari )</th>
                            <th width="6%">Status</th>
                            <th width="8%" >Tanggal Update</th>
                            <th width="8%" >Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="txt-sm">

                    </tbody>
                    <tfoot class="footer-background" >
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th colspan="4" ></th>
                        </tr>
                        <tr>
                            <th colspan="3" class="total-package" ></th>
                            <th colspan="3" class="total-weight" ></th>
                            <th colspan="5" class="total-size" ></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="tab-pane table-responsive" id="finish">
        <table class="table data-tables finish">
            <thead>
                <tr class="txt-sm">
                    <th width="8%" >Tanggal Resi</th>
                    <th width="8%" >Tanggal Terima</th>
                    <th width="8%" >#ID</th>
                    <th width="15%" >Nama</th>
                    <th width="20%" >Asal - Tujuan</th>
                    <th width="8%">Jumlah Koli</th>
                    <th width="8%">Total Berat <br/>( Kg )</th>
                    <th width="8%">Volume <br/>( M<sup>3</sup> )</th>
                    <th width="8%">Usia Resi <br/>( Hari )</th>
                    <th width="8%" >Aksi</th>
                </tr>
            </thead>
            <tbody class="txt-sm">

            </tbody>
            <tfoot class="footer-background" >
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th colspan="2" ></th>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="4" class="total-package" ></th>
                    <th colspan="2" class="total-weight" ></th>
                    <th colspan="4" class="total-size" ></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="tab-pane table-responsive" id="not-pay">
        <table class="table data-tables not-pay">
            <thead>
                <tr class="txt-sm">
                    <th width="8%" >Tanggal Resi</th>
                    <th width="8%" >#ID</th>
                    <th width="18%" >Nama</th>
                    <th width="25%" >Asal - Tujuan</th>
                    <th width="8%">Jumlah Koli</th>
                    <th width="8%">Total Berat <br/>( Kg )</th>
                    <th width="8%">Volume <br/>( M<sup>3</sup> )</th>
                    <th width="8%">Usia Resi <br/>( Hari )</th>
                    <th width="8%" >Aksi</th>
                </tr>
            </thead>
            <tbody class="txt-sm">

            </tbody>
            <tfoot class="footer-background" >
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th colspan="2" ></th>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="3" class="total-package" ></th>
                    <th colspan="2" class="total-weight" ></th>
                    <th colspan="4" class="total-size" ></th>
                </tr>
            </tfoot>
        </table>
    </div>

</div>