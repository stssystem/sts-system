<div class="modal fade"  role="dialog" id="package">
    <div class="modal-dialog">

        <div class="bootbox-body row">

            <?php echo form_open('orders/orders_update/update_package', 'class=""form-horizontal col-md-12"'); ?>
            <input type="hidden" name="order_id" value="<?php echo $data->order_id; ?>" />
            <input type="hidden" name="package_id" value="" />

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Informasi Order</h4>
                </div>

                <div class="modal-body row">

                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_title',
                                        'label' => 'Nama Paket',
                                        'max' => 100,
                                        'required' => true,
                                        'value' => '',
                                        'class' => 'col-md-6'
                                    ]
                            );
                            ?>  

                            <?php
                            echo $this->form->select(
                                    [
                                        'name' => 'package_type',
                                        'label' => 'Jenis Paket',
                                        'max' => 100,
                                        'required' => true,
                                        'class' => 'col-md-6',
                                        'value' => get_package_type_list(),
                                        'keys' => 'package_type_id',
                                        'values' => 'package_type_name'
                                    ]
                            );
                            ?>  



                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_qty',
                                        'label' => 'Jumlah Koli',
                                        'max' => 99999,
                                        'required' => true,
                                        'value' => '',
                                        'class' => 'col-md-3',
                                        'type' => 'number'
                                    ]
                            );
                            ?>

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_lenght',
                                        'label' => 'Panjang Paket (cm)',
                                        'max' => 99999,
                                        'required' => true,
                                        'value' => '',
                                        'class' => 'col-md-3',
                                        'type' => 'number'
                                    ]
                            );
                            ?>  

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_width',
                                        'label' => 'Lebar Paket (cm)',
                                        'max' => 99999,
                                        'required' => true,
                                        'value' => '',
                                        'class' => 'col-md-3',
                                        'type' => 'number'
                                    ]
                            );
                            ?>  

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_height',
                                        'label' => 'Tinggi Paket (cm)',
                                        'max' => 99999,
                                        'required' => true,
                                        'value' => '',
                                        'class' => 'col-md-3',
                                        'type' => 'number'
                                    ]
                            );
                            ?>  

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_weight',
                                        'label' => 'Berat Paket (kg)',
                                        'max' => 99999,
                                        'required' => true,
                                        'value' => '',
                                        'class' => 'col-md-3',
                                        'type' => 'number'
                                    ]
                            );
                            ?>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            echo $this->form->textarea(
                                    [
                                        'name' => 'package_content',
                                        'label' => 'Isi Paket',
                                        'max' => 100,
                                        'required' => true,
                                        'value' => '',
                                        'class' => 'col-md-12',
                                    ]
                            );
                            ?> 
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-ban"></i>
                            Batal
                        </button>
                        <button type="submit" class="btn btn-info">
                            <i class="fa fa-trash"></i>
                            Simpan
                        </button>
                    </div>
                </div>

            </div><!-- /.modal-content -->
            <?php echo form_close(); ?>
        </div>

    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->