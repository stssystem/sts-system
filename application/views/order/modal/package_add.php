<div class="modal fade"  role="dialog" id="package_add">
    <div class="modal-dialog" style="width: 800px;">

        <div class="bootbox-body row">

            <?php echo form_open('orders/order/add_package/' . $data->order_id, 'class="col-md-12 form-package-add"'); ?>
            <input type="hidden" name="order_id" value="<?php echo $data->order_id; ?>" />
            <input type="hidden" name="package_id" value="" />

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Data Paket</h4>
                </div>

                <div class="modal-body row">

                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_title',
                                        'label' => 'Nama Paket',
                                        'max' => 100,
                                        'required' => true,
                                        'value' => '',
                                        'class' => 'col-md-6',
                                        'type' => 'text'
                                    ]
                            );
                            ?>  

                            <?php
                            echo $this->form->select(
                                    [
                                        'name' => 'package_type',
                                        'label' => 'Jenis Paket',
                                        'max' => 100,
                                        'required' => true,
                                        'value' => get_package_type_list(),
                                        'keys' => 'package_type_id',
                                        'values' => 'package_type_name',
                                        'class' => 'col-md-3',
                                    ]
                            );
                            ?>  

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_qty',
                                        'label' => 'Jumlah Paket / Qty',
                                        'max' => 99999,
                                        'required' => true,
                                        'value' => '',
                                        'class' => 'col-md-3',
                                        'type' => 'number'
                                    ]
                            );
                            ?>  

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_lenght',
                                        'label' => 'Panjang Paket (Cm)',
                                        'max' => 99999,
                                        'required' => false,
                                        'value' => '',
                                        'class' => 'col-md-3',
                                        'type' => 'number',
                                        'attribute' => ['step' => '0.001']
                                    ]
                            );
                            ?>  

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_width',
                                        'label' => 'Lebar Paket (Cm)',
                                        'max' => 99999,
                                        'required' => false,
                                        'value' => '',
                                        'class' => 'col-md-3',
                                        'type' => 'number',
                                        'attribute' => ['step' => '0.001']
                                    ]
                            );
                            ?>  

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_height',
                                        'label' => 'Tinggi Paket (Cm)',
                                        'max' => 99999,
                                        'required' => false,
                                        'value' => '',
                                        'class' => 'col-md-3',
                                        'type' => 'number',
                                        'attribute' => ['step' => '0.001']
                                    ]
                            );
                            ?>  

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'package_weight',
                                        'label' => 'Berat Paket (Kg)',
                                        'max' => 99999,
                                        'required' => true,
                                        'value' => '',
                                        'class' => 'col-md-3',
                                        'type' => 'number',
                                        'attribute' => ['step' => '0.001']
                                    ]
                            );
                            ?>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            echo $this->form->textarea(
                                    [
                                        'name' => 'package_content',
                                        'label' => 'Isi Paket',
                                        'max' => 100,
                                        'required' => false,
                                        'value' => '',
                                        'class' => 'col-md-12',
                                        'attribute' => []
                                    ]
                            );
                            ?> 
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-ban"></i>
                            Batal
                        </button>
                        <button type="submit" class="btn btn-info">
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                        <?php if (profile()->user_type_id == 99): ?>
                            <button type="button" class="btn btn-warning btn-save-update">
                                <i class="fa fa-money"></i>
                                Simpan & Update Total Harga
                            </button>
                        <?php endif; ?>
                    </div>
                </div>

            </div><!-- /.modal-content -->
            <?php echo form_close(); ?>
        </div>

    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->