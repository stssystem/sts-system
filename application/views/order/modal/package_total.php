<div class="modal fade"  role="dialog" id="total_order_price">
    <div class="modal-dialog">

        <div class="bootbox-body row">

            <?php echo form_open('orders/orders_update/update_total_order_price', 'class=""form-horizontal col-md-12"'); ?>
            <input type="hidden" name="order_id" value="<?php echo $data->order_id; ?>" />

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Informasi Order</h4>
                </div>

                <div class="modal-body row">

                    <div class="row">
                        <div class="col-md-12">

                            <?php
                            echo $this->form->text(
                                    [
                                        'name'      => 'det_total_order_price',
                                        'label'     => 'Total Biaya',
                                        'max'       => 100,
                                        'required'  => true,
                                        'value'     => '',
                                        'class'     => 'col-md-12',
                                        'type'      => 'text'
                                    ]
                            );
                            ?>  

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-ban"></i>
                            Batal
                        </button>
                        <button type="submit" class="btn btn-info">
                            <i class="fa fa-trash"></i>
                            Simpan
                        </button>
                    </div>
                </div>

            </div><!-- /.modal-content -->
            <?php echo form_close(); ?>
        </div>

    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->