<div class="modal fade"  role="dialog" id="origin">
    <div class="modal-dialog">

        <div class="bootbox-body row">

            <?php echo form_open('orders/orders_update/update_origin', 'class=""form-horizontal col-md-12"'); ?>
            <input type="hidden" name="order_id" value="<?php echo $data->order_id; ?>" />
            
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Informasi Order</h4>
                </div>

                <div class="modal-body row">
                    <?php
                    echo $this->form->select_h(
                            [
                                'name' => 'city_id_origin',
                                'label' => 'Kota *',
                                'max' => 100,
                                'required' => true,
                                'value' => $cities,
                                'keys' => 'city_id',
                                'values' => 'city_name',
                                'class' => 'select2  col-md-12 select-price-params'
                            ]
                    );
                    ?>  


                    <?php
                    echo $this->form->select_h(
                            [
                                'name' => 'districts_id_origin',
                                'label' => 'Kecamatan',
                                'max' => 100,
                                'required' => false,
                                'value' => [],
                                'keys' => 'districts_id',
                                'values' => 'districts_name',
                                'class' => 'select2 col-md-12 select-price-params'
                            ]
                    );
                    ?>  

                    <?php
                    echo $this->form->select_h(
                            [
                                'name' => 'village_id_origin',
                                'label' => 'Kelurahan',
                                'max' => 100,
                                'required' => false,
                                'value' => [],
                                'keys' => 'village_id',
                                'values' => 'village_name',
                                'class' => 'select2 col-md-12 select-price-params'
                            ]
                    );
                    ?>  

                    <?php
                    echo $this->form->select_h(
                            [
                                'name' => 'branch_id_origin',
                                'label' => 'Cabang *',
                                'max' => 100,
                                'required' => true,
                                'value' => [],
                                'keys' => 'branch_id',
                                'values' => 'branch_name',
                                'class' => 'select2 col-md-12 select-price-params'
                            ]
                    );
                    ?>  
                    <?php
                    $this->form->textarea_h(
                            [
                                'name' => 'order_origin_text',
                                'label' => 'Detail Pengirim',
                                'max' => 100,
                                'value' => '',
                                'required' => false,
                                'class' => 'select2 col-md-12 select-price-params',
                                'rows' => 4
                            ]
                    );
                    ?>
                </div>

                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-ban"></i>
                            Batal
                        </button>
                        <button type="submit" class="btn btn-info">
                            <i class="fa fa-trash"></i>
                            Simpan
                        </button>
                    </div>
                </div>

            </div><!-- /.modal-content -->
            <?php echo form_close(); ?>
        </div>

    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->