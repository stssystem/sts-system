<div class="modal fade"  role="dialog" id="modal-package-rollback">
    <div class="modal-dialog">

        <div class="bootbox-body row">

            <?php echo form_open('orders/order/package_rollback'); ?>
            <input type="hidden" name="order_id" value="" />

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Informasi Pembatalan Pengiriman Paket</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea name="note" class="form-control" rows="5" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-ban"></i>
                            Batal
                        </button>
                        <button type="submit" class="btn btn-warning a-closed">
                            <i class="fa fa-book"></i>
                            Simpan
                        </button>
                    </div>
                </div>

            </div><!-- /.modal-content -->

            <?php echo form_close(); ?>
        </div>

    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->