<div class="modal fade"  role="dialog" id="modal-self-take">
    <div class="modal-dialog">

        <div class="bootbox-body row">

            <?php echo form_open('orders/order/self_take'); ?>
            <input type="hidden" name="order_id" value="" />

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Informasi Pengambilan Paket</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Pengambil</label>
                                <input type="text" name="accepted_name" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-ban"></i>
                            Batal
                        </button>
                        <button type="submit" class="btn btn-warning a-closed">
                            <i class="fa fa-book"></i>
                            Simpan
                        </button>
                    </div>
                </div>

            </div><!-- /.modal-content -->

            <?php echo form_close(); ?>
        </div>

    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->