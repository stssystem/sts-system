<div class="modal fade"  role="dialog" id="closed">
    <div class="modal-dialog">

        <div class="bootbox-body row">

            <input type="hidden" name="order_id" value="<?php echo $data->order_id; ?>" />

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Informasi Order</h4>
                </div>

                <div class="modal-body">
                    <p>Apakah Anda yakin akan menutup order ini?</p>
                </div>

                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-ban"></i>
                            Batal
                        </button>
                        <a class="btn btn-warning a-closed">
                            <i class="fa fa-book"></i>
                            Tutup
                        </a>
                    </div>
                </div>

            </div><!-- /.modal-content -->
        </div>

    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->