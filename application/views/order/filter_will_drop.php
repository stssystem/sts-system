<div class="row">
    <?php echo form_open($path . '/status_filter_willdrop'); ?>
    <div class="col-md-2">
        <select class="form-control" name="status_filter_willdrop">
            <option value="">-- Semua Status --</option>
            <option value="3" <?php echo $this->session->userdata('status_filter_willdrop') == 3 ? 'selected="selected"' : ''; ?> >Belum Naik</option>
            <option value="4" <?php echo $this->session->userdata('status_filter_willdrop') == 4 ? 'selected="selected"' : ''; ?> >Sudah Naik</option>
        </select>
    </div>
    <button class="btn btn-info btn-flat col-md-1" >
        <i class="fa fa-filter"></i>
        Filter
    </button>
    
    <?php echo form_close(); ?>
</div>
<hr/>