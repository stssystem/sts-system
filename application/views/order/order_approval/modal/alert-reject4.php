<div class="modal fade" tabindex="-1" role="dialog" id="modal-orders-extra-reject-alert">
    <div class="modal-dialog">
        <div class="modal-content">
        <?php echo form_open($path . '/reject_edit_orders_extra'); ?>
            <input name="id"  type="hidden" />
            <input name="type_edit"  type="hidden" />
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi Data Order</h4>
            </div>
            <div class="modal-body">
                <p>
                    Apakah Anda menyetujui untuk <strong>menolak</strong> memperbaharui data order
                    <strong><input name="order_id"  type="view" readonly="true"></strong>?
                </p>

                <div class="form-group">
                    <label for="package_notes">Alasan Order Ditolak :</label>
                    <textarea class="form-control" rows="5" id="package_notes" name="package_notes"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-ban"></i>
                        Batal
                    </button>
                    <button type="submit" class="btn btn-info alert-btn-save">
                        <i class="fa fa-check"></i>
                        Konfirmasi
                    </button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
