<style type="text/css">
    input[type=view] {
        border: 0px;
        border-radius: 0;

        -webkit-appearance: none;
    }
    textarea[type=view] {
        border: 0px;
        border-radius: 0;

        -webkit-appearance: none;
    }
</style>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-location-view">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi Memperbarui Data Order</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                        <div class="col-md-11">
                            <h5><strong>Data Permintaan Edit Lokasi</strong></h5>
                            <table class="table">
                                <tr>
                                    <td width="20%">
                                        <label>Tipe Edit</label>
                                    </td>
                                    <td width="5%">:</td>
                                    <td width="75%">
                                        <textarea name="type_edit" type="view" readonly="true"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <label>Edit Cabang</label>
                                    </td>
                                    <td width="5%">:</td>
                                    <td width="75%">
                                        <input name="branch_edit" type="view" readonly="true"></input>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <label>Edit Kota</label>
                                    </td>
                                    <td width="5%">:</td>
                                    <td width="75%">
                                        <input name="city_edit" type="view" readonly="true"></input>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <label>Edit Kecamatan</label>
                                    </td>
                                    <td width="5%">:</td>
                                    <td width="75%">
                                        <input name="districts_edit" type="view" readonly="true"></input>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <label>Edit Kelurahan</label>
                                    </td>
                                    <td width="5%">:</td>
                                    <td width="75%">
                                        <input name="village_edit" type="view" readonly="true"></input>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Asal Paket :</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="branch_origin_name" readonly="true" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Cabang Tujuan Paket:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="branch_destination_name" readonly="true" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Nama Pengirim Paket :</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="customer_name" readonly="true" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Staff Pemohon :</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="request_staff_name" readonly="true" />
                                        </div>
                                    </div> 
                                    <hr>
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Tipe Staff :</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="request_user_type_name" readonly="true" />
                                        </div>
                                    </div> 
                                    <hr>
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Cabang Staff :</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="request_branch_staff" readonly="true" />
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

        </div>
    </div><
</div>
