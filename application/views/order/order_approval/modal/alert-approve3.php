<div class="modal fade" tabindex="-1" role="dialog" id="modal-total-approve-alert">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open($path . '/approve_edit_total_price'); ?>
            <input name="id"  type="hidden" />
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi Memperbarui Data Order</h4>
            </div>
            <div class="modal-body">
                <p>
                    Apakah Anda menyetujui untuk memperbarui data total harga order 
                    <strong><input name="order_id"  type="view" readonly="true"></strong>?
                </p>

                <div class="container-fluid bd-example-row">

                    <div class="row">
                        <div class="col-md-11">
                            <h5><strong>Data Permintaan Edit Total Harga</strong></h5>
                            <table class="table">
                                <tr>
                                    <td width="20%">
                                        <label>Total Edit Harga</label>
                                    </td>
                                    <td width="5%">:</td>
                                    <td width="75%">
                                        <input name="total_order_price" type="view" readonly="true"></input>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Asal Paket :</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="branch_origin_name" readonly="true" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Cabang Tujuan Paket:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="branch_destination_name" readonly="true" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Nama Pengirim Paket :</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="customer_name" readonly="true" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Staff Pemohon :</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="request_staff_name" readonly="true" />
                                        </div>
                                    </div> 
                                    <hr>
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Tipe Staff :</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="request_user_type_name" readonly="true" />
                                        </div>
                                    </div> 
                                    <hr>
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Cabang Staff :</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="request_branch_staff" readonly="true" />
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-ban"></i>
                        Batal
                    </button>
                    <button type="submit" class="btn btn-info alert-btn-save">
                        <i class="fa fa-check"></i>
                        Konfirmasi
                    </button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
