<script type="text/javascript">

    $(function() { 
        // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTab');
        if (lastTab) {
            $('[href="' + lastTab + '"]').tab('show');
        }
    }); 

    $('a.order_approval_tab').on('shown.bs.tab', function () {
        var value = $(this).attr('data-value');
        $('.th-action').attr('style', 'width:75px;');
    });

    $('a.package_edit_tab').on('shown.bs.tab', function () {
        var value = $(this).attr('data-value');
        $('.th-action').attr('style', 'width:75px;');
    });

    $('a.package_location_tab').on('shown.bs.tab', function () {
        var value = $(this).attr('data-value');
        $('.th-action').attr('style', 'width:75px;');
    });

    $('a.package_total_tab').on('shown.bs.tab', function () {
        var value = $(this).attr('data-value');
        $('.th-action').attr('style', 'width:75px;');
    });

    $('a.package_orders_extra_tab').on('shown.bs.tab', function () {
        var value = $(this).attr('data-value');
        $('.th-action').attr('style', 'width:75px;');
    });

    $('a.package_delete_tab').on('shown.bs.tab', function () {
        var value = $(this).attr('data-value');
        $('.th-action').attr('style', 'width:75px;');
    });

    /** Approve order */

    $('body').on('click', '.btn-confirm', function () {
        var data = $(this).parent().parent().find('input[name="package-edit-data"]').val();
        data = JSON.parse(data);
        $('#modal-package-approve-alert').on('shown.bs.modal', function (e) {
            $('input[name="id"]').val(data['package_edit_id']);
            $('input[name="order_id"]').val(data['order_id']);
            $('input[name="package_id"]').val(data['package_id']);
            $('input[name="package_edit_title"]').val(data['package_edit_title']);
            $('input[name="package_edit_type"]').val(data['package_edit_type']);
            $('input[name="package_edit_qty"]').val(data['package_qty']);
            $('input[name="package_edit_lenght"]').val(data['package_edit_lenght']);
            $('input[name="package_edit_width"]').val(data['package_edit_width']);
            $('input[name="package_edit_height"]').val(data['package_edit_height']);
            $('input[name="package_edit_weight"]').val(data['package_edit_weight']);
            $('input[name="package_edit_content"]').val(data['package_edit_content']);
            $('input[name="branch_origin_name"]').val(data['branch_origin_name']);
            $('input[name="branch_destination_name"]').val(data['branch_destination_name']);
            $('input[name="customer_name"]').val(data['customer_name']);
            $('input[name="request_staff_name"]').val(data['request_staff_name']);
            $('input[name="request_user_type_name"]').val(data['request_user_type_name']);
            $('input[name="request_branch_staff"]').val(data['request_user_branch_name']);
        });
    });

    $('body').on('click', '.btn-confirm_2', function () {
        var data = $(this).parent().parent().find('input[name="location-edit-data"]').val();
        data = JSON.parse(data);

        var location = data['pckg_edit_loc_origin_text'];
        location = JSON.parse(location);

        $('#modal-location-approve-alert').on('shown.bs.modal', function (e) {
            $('input[name="id"]').val(data['pckg_edit_loc_id']);
            $('input[name="order_id"]').val(data['order_id']);
            $('input[name="branch_origin_name"]').val(data['branch_origin_name']);
            $('input[name="branch_destination_name"]').val(data['branch_destination_name']);
            $('input[name="customer_name"]').val(data['customer_name']);
            $('input[name="request_staff_name"]').val(data['request_staff_name']);
            $('input[name="request_user_type_name"]').val(data['request_user_type_name']);
            $('input[name="request_branch_staff"]').val(data['request_user_branch_name']);
            
            if (data['type_edit'] == 1) {
                $('textarea[name="type_edit"]').val('Edit Data Cabang/Agen Asal');
            } else {
                $('textarea[name="type_edit"]').val('Edit Data Cabang/Agen Tujuan');
            }  

            if (data['pckg_edit_loc_origin'] != 0) {
                $('input[name="branch_edit"]').val(data['pckg_edit_loc_origin']);
            } else if (data['pckg_edit_loc_destination'] != 0){
                $('input[name="branch_edit"]').val(data['pckg_edit_loc_destination']);
            }  
            
            if (location['pckg_edit_city_origin'] != 0) {
                $('input[name="city_edit"]').val(location['city_id_origin']);
            } else if (location['pckg_edit_city_destination'] != 0){
                $('input[name="city_edit"]').val(location['city_id_destination']);
            }  

            if (location['pckg_edit_districts_origin'] != 0) {
                $('input[name="districts_edit"]').val(location['districts_id_origin']);
            } else if (location['pckg_edit_districts_destination'] != 0){
                $('input[name="districts_edit"]').val(location['districts_id_destination']);
            }  

            if (location['pckg_edit_village_origin'] != 0) {
                $('input[name="village_edit"]').val(location['village_id_origin']);
            } else if (location['pckg_edit_village_destination'] != 0){
                $('input[name="village_edit"]').val(location['village_id_destination']);
            }    
            
        });
    });

    $('body').on('click', '.btn-confirm_3', function () {
        var data = $(this).parent().parent().find('input[name="total-edit-data"]').val();
        data = JSON.parse(data);
        $('#modal-total-approve-alert').on('shown.bs.modal', function (e) {
            $('input[name="id"]').val(data['pckg_edit_total_id']);
            $('input[name="order_id"]').val(data['order_id']);
            $('input[name="branch_origin_name"]').val(data['branch_origin_name']);
            $('input[name="branch_destination_name"]').val(data['branch_destination_name']);
            $('input[name="customer_name"]').val(data['customer_name']);
            $('input[name="request_staff_name"]').val(data['request_staff_name']);
            $('input[name="request_user_type_name"]').val(data['request_user_type_name']);
            $('input[name="request_branch_staff"]').val(data['request_user_branch_name']);
            $('input[name="total_order_price"]').val(data['total_order_price']);
        });
    });

    $('body').on('click', '.btn-confirm_4', function () {
        var data = $(this).parent().parent().find('input[name="orders-extra-edit-data"]').val();
        data = JSON.parse(data);
        $('#modal-orders-extra-approve-alert').on('shown.bs.modal', function (e) {
            $('input[name="id"]').val(data['pckg_edit_orders_extra_id']);
            $('input[name="order_id"]').val(data['order_id']);
            $('input[name="branch_origin_name"]').val(data['branch_origin_name']);
            $('input[name="branch_destination_name"]').val(data['branch_destination_name']);
            $('input[name="customer_name"]').val(data['customer_name']);
            $('input[name="request_staff_name"]').val(data['request_staff_name']);
            $('input[name="request_user_type_name"]').val(data['request_user_type_name']);
            $('input[name="request_branch_staff"]').val(data['request_user_branch_name']);
            $('input[name="orders_extra_id"]').val(data['orders_extra_id']);
            $('input[name="orders_extra_name"]').val(data['orders_extra_name']);
            $('input[name="orders_extra_total"]').val(data['orders_extra_total']);
        });
    });

    $('body').on('click', '.btn-confirm_5', function () {
        var data = $(this).parent().parent().find('input[name="package-delete-data"]').val();
        data = JSON.parse(data);
        $('#modal-package-delete-approve-alert').on('shown.bs.modal', function (e) {
            $('input[name="id"]').val(data['package_delete_id']);
            $('input[name="order_id"]').val(data['order_id']);
            $('input[name="package_id"]').val(data['package_id']);
            $('input[name="package_name"]').val(data['package_name']);
            $('input[name="branch_origin_name"]').val(data['branch_origin_name']);
            $('input[name="branch_destination_name"]').val(data['branch_destination_name']);
            $('input[name="request_staff_name"]').val(data['request_staff_name']);
            $('input[name="request_user_type_name"]').val(data['request_user_type_name']);
            $('input[name="request_branch_staff"]').val(data['request_user_branch_name']);
        });
    });

    /** Reject order */
    
    $('body').on('click', '.btn-reject', function () {
        var data = $(this).parent().parent().find('input[name="package-edit-data"]').val();
        data = JSON.parse(data);
        $('#modal-package-reject-alert').find('input[name="id"]').val(data['package_edit_id']);
        $('#modal-package-reject-alert').find('input[name="order_id"]').val(data['order_id']);
    });

    $('body').on('click', '.btn-reject_2', function () {
        var data = $(this).parent().parent().find('input[name="location-edit-data"]').val();
        data = JSON.parse(data);
        $('#modal-location-reject-alert').find('input[name="id"]').val(data['pckg_edit_loc_id']);
        $('#modal-location-reject-alert').find('input[name="order_id"]').val(data['order_id']);
        $('#modal-location-reject-alert').find('input[name="type_edit"]').val(data['type_edit']);
    });

    $('body').on('click', '.btn-reject_3', function () {
        var data = $(this).parent().parent().find('input[name="total-edit-data"]').val();
        data = JSON.parse(data);
        $('#modal-total-reject-alert').find('input[name="id"]').val(data['pckg_edit_total_id']);
        $('#modal-total-reject-alert').find('input[name="order_id"]').val(data['order_id']);
    });

    $('body').on('click', '.btn-reject_4', function () {
        var data = $(this).parent().parent().find('input[name="orders-extra-edit-data"]').val();
        data = JSON.parse(data);
        $('#modal-orders-extra-reject-alert').find('input[name="id"]').val(data['pckg_edit_orders_extra_id']);
    });

    $('body').on('click', '.btn-reject_5', function () {
        var data = $(this).parent().parent().find('input[name="package-delete-data"]').val();
        data = JSON.parse(data);
        $('#modal-package-delete-reject-alert').find('input[name="id"]').val(data['package_delete_id']);
        $('#modal-package-delete-reject-alert').find('input[name="package_id"]').val(data['package_id']);
    });


    /** View order */

    $('body').on('click', '.btn-view', function () {
        var data = $(this).parent().parent().find('input[name="package-edit-data"]').val();
        data = JSON.parse(data);
        $('#modal-package-view').on('shown.bs.modal', function (e) {
            $('input[name="id"]').val(data['package_edit_id']);
            $('input[name="order_id"]').val(data['order_id']);
            $('input[name="package_id"]').val(data['package_id']);
            $('input[name="package_edit_title"]').val(data['package_edit_title']);
            $('input[name="package_edit_type"]').val(data['package_edit_type']);
            $('input[name="package_edit_qty"]').val(data['package_qty']);
            $('input[name="package_edit_lenght"]').val(data['package_edit_lenght']);
            $('input[name="package_edit_width"]').val(data['package_edit_width']);
            $('input[name="package_edit_height"]').val(data['package_edit_height']);
            $('input[name="package_edit_weight"]').val(data['package_edit_weight']);
            $('input[name="package_edit_content"]').val(data['package_edit_content']);
            $('input[name="branch_origin_name"]').val(data['branch_origin_name']);
            $('input[name="branch_destination_name"]').val(data['branch_destination_name']);
            $('input[name="customer_name"]').val(data['customer_name']);
            $('input[name="request_staff_name"]').val(data['request_staff_name']);
            $('input[name="request_user_type_name"]').val(data['request_user_type_name']);
            $('input[name="request_branch_staff"]').val(data['request_user_branch_name']);
        });
    });

    $('body').on('click', '.btn-view_2', function () {
        var data = $(this).parent().parent().find('input[name="location-edit-data"]').val();
        data = JSON.parse(data);

        var location = data['pckg_edit_loc_origin_text'];
        location = JSON.parse(location);

        $('#modal-location-view').on('shown.bs.modal', function (e) {
            $('input[name="id"]').val(data['pckg_edit_loc_id']);
            $('input[name="order_id"]').val(data['order_id']);
            $('input[name="branch_origin_name"]').val(data['branch_origin_name']);
            $('input[name="branch_destination_name"]').val(data['branch_destination_name']);
            $('input[name="customer_name"]').val(data['customer_name']);
            $('input[name="request_staff_name"]').val(data['request_staff_name']);
            $('input[name="request_user_type_name"]').val(data['request_user_type_name']);
            $('input[name="request_branch_staff"]').val(data['request_user_branch_name']);
            
            if (data['type_edit'] == 1) {
                $('textarea[name="type_edit"]').val('Edit Data Cabang/Agen Asal');
            } else {
                $('textarea[name="type_edit"]').val('Edit Data Cabang/Agen Tujuan');
            }  

            if (data['pckg_edit_loc_origin'] != 0) {
                $('input[name="branch_edit"]').val(data['pckg_edit_loc_origin']);
            } else if (data['pckg_edit_loc_destination'] != 0){
                $('input[name="branch_edit"]').val(data['pckg_edit_loc_destination']);
            }  
            
            if (location['pckg_edit_city_origin'] != 0) {
                $('input[name="city_edit"]').val(location['city_id_origin']);
            } else if (location['pckg_edit_city_destination'] != 0){
                $('input[name="city_edit"]').val(location['city_id_destination']);
            }  

            if (location['pckg_edit_districts_origin'] != 0) {
                $('input[name="districts_edit"]').val(location['districts_id_origin']);
            } else if (location['pckg_edit_districts_destination'] != 0){
                $('input[name="districts_edit"]').val(location['districts_id_destination']);
            }  

            if (location['pckg_edit_village_origin'] != 0) {
                $('input[name="village_edit"]').val(location['village_id_origin']);
            } else if (location['pckg_edit_village_destination'] != 0){
                $('input[name="village_edit"]').val(location['village_id_destination']);
            }    
        });
    });

    $('body').on('click', '.btn-view_3', function () {
        var data = $(this).parent().parent().find('input[name="total-edit-data"]').val();
        data = JSON.parse(data);
        $('#modal-total-view').on('shown.bs.modal', function (e) {
            $('input[name="id"]').val(data['pckg_edit_total_id']);
            $('input[name="order_id"]').val(data['order_id']);
            $('input[name="branch_origin_name"]').val(data['branch_origin_name']);
            $('input[name="branch_destination_name"]').val(data['branch_destination_name']);
            $('input[name="customer_name"]').val(data['customer_name']);
            $('input[name="request_staff_name"]').val(data['request_staff_name']);
            $('input[name="request_user_type_name"]').val(data['request_user_type_name']);
            $('input[name="request_branch_staff"]').val(data['request_user_branch_name']);
            $('input[name="total_order_price"]').val(data['total_order_price']);
        });
    });

    $('body').on('click', '.btn-view_4', function () {
        var data = $(this).parent().parent().find('input[name="orders-extra-edit-data"]').val();
        data = JSON.parse(data);
        $('#modal-orders-extra-view').on('shown.bs.modal', function (e) {
            $('input[name="id"]').val(data['pckg_edit_orders_extra_id']);
            $('input[name="order_id"]').val(data['order_id']);
            $('input[name="branch_origin_name"]').val(data['branch_origin_name']);
            $('input[name="branch_destination_name"]').val(data['branch_destination_name']);
            $('input[name="customer_name"]').val(data['customer_name']);
            $('input[name="request_staff_name"]').val(data['request_staff_name']);
            $('input[name="request_user_type_name"]').val(data['request_user_type_name']);
            $('input[name="request_branch_staff"]').val(data['request_user_branch_name']);
            $('input[name="orders_extra_id"]').val(data['orders_extra_id']);
            $('input[name="orders_extra_name"]').val(data['orders_extra_name']);
            $('input[name="orders_extra_total"]').val(data['orders_extra_total']);
        });
    });

    $('body').on('click', '.btn-view_5', function () {
        var data = $(this).parent().parent().find('input[name="package-delete-data"]').val();
        data = JSON.parse(data);
        $('#modal-package-delete-view').on('shown.bs.modal', function (e) {
            $('input[name="id"]').val(data['package_delete_id']);
            $('input[name="order_id"]').val(data['order_id']);
            $('input[name="package_id"]').val(data['package_id']);
            $('input[name="package_name"]').val(data['package_name']);
            $('input[name="branch_origin_name"]').val(data['branch_origin_name']);
            $('input[name="branch_destination_name"]').val(data['branch_destination_name']);
            $('input[name="request_staff_name"]').val(data['request_staff_name']);
            $('input[name="request_user_type_name"]').val(data['request_user_type_name']);
            $('input[name="request_branch_staff"]').val(data['request_user_branch_name']);
        });
    });


    setTimeout(function () {
        $('.alert-dismissable').fadeOut('slow');
    }, 3000);


</script>