<?php $this->load->view('order/order_approval/modal/alert-approve3'); ?>
<?php $this->load->view('order/order_approval/modal/alert-reject3'); ?>
<?php $this->load->view('order/order_approval/modal/view_3'); ?>

<div class="row">
    <div class="col-sm-12">

        <?php echo get_alert(); ?>

        <div class="nav-tabs-custom">

            <ul class="nav nav-tabs">

                <li class="">
                    <a class="package_total_tab" href="#tab_total_not_approved_dt" data-value="1" data-toggle="tab" aria-expanded="false"> 
                        Butuh Konfirmasi
                        <?php if (get_notif_3()) {
                            ?>
                            <span class="pull-right badge bg-red">
                                <?php echo get_notif_3(); ?>
                            </span>    
                            <?php 
                        }; ?>     
                    </a>
                </li>
                
                <li class="">
                    <a class="package_total_tab" href="#tab_total_approved_dt" data-value="2" data-toggle="tab" aria-expanded="false"> 
                        Sudah Diperbaharui
                    </a>
                </li>

                <li class="">
                    <a class="package_total_tab" href="#tab_total_rejected_dt" data-value="2" data-toggle="tab" aria-expanded="false"> 
                        Permintaan Edit Ditolak
                    </a>
                </li>

            </ul>

            <div class="tab-content">

                <!-- tab 1 -->
                <div class="tab-pane table-responsive " id="tab_total_not_approved_dt">
                    <table class="table data-tables tab_total_not_approved_dt table-hover">
                        <thead>
                            <tr class="txt-sm">
                                <th width="10%">Tanggal Permintaan</th>
                                <th width="10%">Order ID</th>
                                <th width="15%">Nama Pengirim Paket</th> 
                                <th width="15%">Total Permintan Edit</th>
                                <th width="10%">Staff Pemohon</th>
                                <th width="10%">Tipe Staff</th>
                                <th width="10%">Cabang</th>
                                <th width="5%">Status</th>
                                <th width="5%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="txt-sm">

                        </tbody>
                    </table>
                </div>

                <!-- tab 2 -->
                <div class="tab-pane table-responsive " id="tab_total_approved_dt">
                    <table class="table data-tables tab_total_approved_dt table-gray table-hover">
                        <thead>
                            <tr class="txt-sm">
                                <th width="10%">Tanggal Permintaan</th>
                                <th width="10%">Order ID</th>
                                <th width="15%">Nama Pengirim Paket</th>
                                <th width="20%">Total Permintaan Edit</th>
                                <th width="20%">Staff Pemohon</th>
                                <th width="15%">Staff Penerima</th>
                                <th width="5%">Status</th>
                            </tr>
                        </thead>
                        <tbody class="txt-sm">

                        </tbody>
                    </table>
                </div>

                <!-- tab 3 -->
                <div class="tab-pane table-responsive " id="tab_total_rejected_dt">
                    <table class="table data-tables tab_total_rejected_dt table-gray table-hover">
                        <thead>
                            <tr class="txt-sm">
                                <th width="10%">Tanggal Permintaan</th>
                                <th width="10%">Order ID</th>
                                <th width="15%">Nama Pengirim Paket</th>
                                <th width="20%">Total Permintaan Edit</th>
                                <th width="10%">Staff Pemohon</th>
                                <th width="10%">Tipe Staff</th>
                                <th width="5%">Status</th>
                                <th width="10%">Alasan Ditolak</th>
                                <th width="5%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="txt-sm">

                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /.tab-content -->
        </div>

    </div>
</div>