<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">

        <div class="nav-tabs-custom">

            <ul class="nav nav-tabs">
                <li class="active">
                    <a class="order_approval_tab" href="#tab_package" data-value="1" data-toggle="tab" aria-expanded="false"> 
                        Edit Data Paket  
                        <?php if (get_notif_1()) {
                            ?>
                            <span class="pull-right badge bg-red">
                                <?php echo get_notif_1(); ?>
                            </span>    
                            <?php 
                        }; ?>    
                    </a>
                </li>
                <li class="">
                    <a class="order_approval_tab" href="#tab_location" data-value="3" data-toggle="tab" aria-expanded="false">
                        Edit Asal / Tujuan Pengiriman
                        <?php if (get_notif_2()) {
                            ?>
                            <span class="pull-right badge bg-red">
                                <?php echo get_notif_2(); ?>
                            </span>    
                            <?php 
                        }; ?>    
                    </a>
                </li>
                <li class="">
                    <a class="order_approval_tab" href="#tab_total" data-value="2" data-toggle="tab" aria-expanded="false"> 
                        Edit Total Harga
                        <?php if (get_notif_3()) {
                            ?>
                            <span class="pull-right badge bg-red">
                                <?php echo get_notif_3(); ?>
                            </span>    
                            <?php 
                        }; ?>     
                    </a>
                </li>
                <li class="">
                    <a class="order_approval_tab" href="#tab_orders_extra" data-value="2" data-toggle="tab" aria-expanded="false"> 
                        Edit Extra Order
                        <?php if (get_notif_4()) {
                            ?>
                            <span class="pull-right badge bg-red">
                                <?php echo get_notif_4(); ?>
                            </span>    
                            <?php 
                        }; ?>         
                    </a>
                </li>
                <li class="">
                    <a class="order_approval_tab" href="#tab_package_delete" data-value="2" data-toggle="tab" aria-expanded="false"> 
                        Permintaan Hapus Paket
                        <?php if (get_notif_5()) {
                            ?>
                            <span class="pull-right badge bg-red">
                                <?php echo get_notif_5(); ?>
                            </span>    
                            <?php 
                        }; ?>         
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab_package">
                    <?php $this->load->view('order/order_approval/tab_package'); ?>
                </div>
                <div class="tab-pane" id="tab_location">
                    <?php $this->load->view('order/order_approval/tab_location'); ?>
                </div>
                <div class="tab-pane" id="tab_total">
                    <?php $this->load->view('order/order_approval/tab_total'); ?>
                </div>
                <div class="tab-pane" id="tab_orders_extra">
                    <?php $this->load->view('order/order_approval/tab_orders_extra'); ?>
                </div>
                <div class="tab-pane" id="tab_package_delete">
                    <?php $this->load->view('order/order_approval/tab_package_delete'); ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php $this->load->view('footer'); ?>


<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<?php $this->load->view('order/order_approval/js'); ?>
<?php $this->load->view('order/order_approval/js-tab'); ?>
