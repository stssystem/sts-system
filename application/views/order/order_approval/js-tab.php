<script type="text/javascript" >

    var column_1 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'location'},
        {data: 'request_staff_name'},
        {data: 'request_user_type_name'},
        {data: 'branch'},
        {data: 'status'},
        {data: 'action'}
    ];

    var column_2 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'location'},
        {data: 'request_staff'},
        {data: 'approved_staff'},
        {data: 'status'},
    ];

    var column_3 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'location'},
        {data: 'request_staff_name'},
        {data: 'request_user_type_name'},
        {data: 'status'},
        {data: 'notes'},
        {data: 'action'}
    ];

    var column_4 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'type'},
        {data: 'request_staff'},
        {data: 'request_user_type_name'},
        {data: 'branch'},
        {data: 'status'},
        {data: 'action'}
    ];

    var column_5 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'type'},
        {data: 'request_staff'},
        {data: 'approved_staff'},
        {data: 'status'},
    ];

    var column_6 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'type'},
        {data: 'request_staff_name'},
        {data: 'request_user_type_name'},
        {data: 'status'},
        {data: 'notes'},
        {data: 'action'}
    ];

    var column_7 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'total_order_price'},
        {data: 'request_staff_name'},
        {data: 'request_user_type_name'},
        {data: 'branch'},
        {data: 'status'},
        {data: 'action'}
    ];

    var column_8 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'total_order_price'},
        {data: 'request_staff'},
        {data: 'approved_staff'},
        {data: 'status'},
    ];

    var column_9 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'total_order_price'},
        {data: 'request_staff_name'},
        {data: 'request_user_type_name'},
        {data: 'status'},
        {data: 'notes'},
        {data: 'action'}
    ];

    var column_10 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'orders_extra_id'},
        {data: 'orders_extra_name'},
        {data: 'request_staff_name'},
        {data: 'request_user_type_name'},
        {data: 'branch'},
        {data: 'status'},
        {data: 'action'}
    ];

    var column_11 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'orders_extra_id'},
        {data: 'orders_extra_name'},
        {data: 'request_staff'},
        {data: 'approved_staff'},
        {data: 'status'},
    ];

    var column_12 = [
        {data: 'date'},
        {data: 'id'},
        {data: 'customer_name'},
        {data: 'orders_extra_id'},
        {data: 'orders_extra_name'},
        {data: 'request_staff_name'},
        {data: 'request_user_type_name'},
        {data: 'status'},
        {data: 'notes'},
        {data: 'action'}
    ];

    var column_13 = [
        {data: 'date'},
        {data: 'order_id'},
        {data: 'package_id'},
        {data: 'package_name'},
        {data: 'request_staff_name'},
        {data: 'request_user_type_name'},
        {data: 'branch'},
        {data: 'status'},
        {data: 'action'}
    ];

    var column_14 = [
        {data: 'date'},
        {data: 'order_id'},
        {data: 'package_id'},
        {data: 'package_name'},
        {data: 'request_staff'},
        {data: 'approved_staff'},
        {data: 'status'},
    ];

    var column_15 = [
        {data: 'date'},
        {data: 'order_id'},
        {data: 'package_id'},
        {data: 'package_name'},
        {data: 'request_staff_name'},
        {data: 'request_user_type_name'},
        {data: 'status'},
        {data: 'notes'},
        {data: 'action'}
    ];

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

        var target = $(e.target).attr("href");
        var status;
        var url;
        var column;

        target = target.replace("#", "");

        switch (target) {

            //tab edit package

            case 'tab_package_not_approved_dt':
                status = 2; //not approved
                url = '<?php echo site_url($path) ?>/index/' + status;
                column = column_1;
                break;

            case 'tab_package_approved_dt':
                status = 1; //approved
                url = '<?php echo site_url($path) ?>/index/' + status;
                column = column_2;
                break;

            case 'tab_package_rejected_dt':
                status = 3; //rejected
                url = '<?php echo site_url($path) ?>/index/' + status;
                column = column_3;
                break;

                // tab edit location 

            case 'tab_location_not_approved_dt':
                status = 2; //not approved
                url = '<?php echo site_url() ?>/orders/orders_approval/location_dt/' + status;
                column = column_4;
                break;

            case 'tab_location_approved_dt':
                status = 1; //approved
                url = '<?php echo site_url() ?>/orders/orders_approval/location_dt/' + status;
                column = column_5;
                break;

            case 'tab_location_rejected_dt':
                status = 3; //rejected
                url = '<?php echo site_url() ?>/orders/orders_approval/location_dt/' + status;
                column = column_6;
                break;

                //tab edit total harga

            case 'tab_total_not_approved_dt':
                status = 2; //not approved
                url = '<?php echo site_url() ?>/orders/orders_approval/total_dt/' + status;
                column = column_7;
                break;

            case 'tab_total_approved_dt':
                status = 1; //approved
                url = '<?php echo site_url() ?>/orders/orders_approval/total_dt/' + status;
                column = column_8;
                break;

            case 'tab_total_rejected_dt':
                status = 3; //rejected
                url = '<?php echo site_url() ?>/orders/orders_approval/total_dt/' + status;
                column = column_9;
                break;

                // tab edit order extra

            case 'tab_orders_extra_not_approved_dt':
                status = 2; //not approved
                url = '<?php echo site_url() ?>/orders/orders_approval/orders_extra_dt/' + status;
                column = column_10;
                break;

            case 'tab_orders_extra_approved_dt':
                status = 1; //approved
                url = '<?php echo site_url() ?>/orders/orders_approval/orders_extra_dt/' + status;
                column = column_11;
                break;

            case 'tab_orders_extra_rejected_dt':
                status = 3; //rejected
                url = '<?php echo site_url() ?>/orders/orders_approval/orders_extra_dt/' + status;
                column = column_12;
                break;

                //tab delete package

            case 'tab_package_delete_not_approved_dt':
                status = 2; //not approved
                url = '<?php echo site_url() ?>/orders/orders_approval/package_delete_dt/' + status;
                column = column_13;
                break;

            case 'tab_package_delete_approved_dt':
                status = 1; //approved
                url = '<?php echo site_url() ?>/orders/orders_approval/package_delete_dt/' + status;
                column = column_14;
                break;

            case 'tab_package_delete_rejected_dt':
                status = 3; //rejected
                url = '<?php echo site_url() ?>/orders/orders_approval/package_delete_dt/' + status;
                column = column_15;

        }

        data_tables(status, target, url, column);

    });

    data_tables(2, 'tab_package_not_approved_dt', '<?php echo site_url($path) ?>/index/2', column_1);

    function data_tables(status, id_table, url, column) {
        $('.' + id_table).dataTable().fnDestroy();

        $('.' + id_table).DataTable({
            ordering: true,
            ajax: {
                url: url,
                type: 'POST',
                dataSrc: '',
            },
            columns: column
        });
    }


</script>