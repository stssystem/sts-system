<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<?php $this->load->view('header'); ?>

<?php $this->load->view('order/modal/destination'); ?>
<?php $this->load->view('order/modal/origin'); ?>
<?php $this->load->view('order/modal/package'); ?>
<?php $this->load->view('order/modal/package_add'); ?>
<?php $this->load->view('order/modal/package_total'); ?>
<?php $this->load->view('order/modal/orders_extra'); ?>
<?php $this->load->view('order/modal/closed'); ?>

<div class="row">

    <div class="col-sm-12">
        <?php get_alert(); ?>
    </div>

    <div class="col-sm-3">

        <div class="box box-primary">

            <div class="box-body box-profile">
                <div class="row">
                    <h3 class="profile-username text-center">Info Order </h3>
                </div>
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>No Resi</b> <a class="pull-right"> <?php echo $data->order_id; ?></a>
                    </li>
                    <li class="list-group-item">
                        <b>No Resi Manual</b> <a class="pull-right"><?php echo $data->order_manual_id; ?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Tanggal</b> <a class="pull-right"><?php echo mdate('%d %F %Y', $data->order_date); ?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Status</b> <a class="pull-right"> <?php echo order_status($data->order_status); ?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Metode Pembayaran</b> <a class="pull-right"><?php echo payment_methode($data->order_payment_type); ?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Jenis Order</b> <a class="pull-right"><?php echo order_type($data->order_type); ?></a>
                    </li>

                    <?php if (in_array($data->order_payment_type, [2, 4])): ?>
                        <li class="list-group-item">
                            <b>Tanggal Jatuh Tempo</b> <a class="pull-right"><?php echo mdate('%d %F %Y', $data->orders_due_date); ?></a>
                        </li>
                    <?php endif; ?>

                    <li class="list-group-item">
                        <b>Dibuat Oleh</b> <a class="pull-right"><?php echo $data->userprofile_fullname; ?></a>
                    </li>

                    <li class="list-group-item">
                        <b>Catatan : </b>
                        <?php echo $data->order_notes; ?>
                    </li>

                </ul>

            </div>
        </div>

        <div class="box box-danger">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
                            <?php $urlx = explode('/', $_SERVER['HTTP_REFERER']); ?>
                            <?php $xpath = (end($urlx) == 'order') ? $path : 'report/report_order'; ?>
                            <?php $title = (end($urlx) == 'order') ? 'Kembali Ke Daftar Order' : 'Kembali Ke Laporan'; ?>
                        <?php else: ?>
                            <?php $xpath = $path; ?>
                            <?php $title = 'Kembali Ke Daftar Order'; ?>
                        <?php endif; ?>
                        
                        <a href="<?php echo site_url($xpath); ?>"  class="btn btn-default btn-block"><i class="fa fa-arrow-left"></i> <?php echo $title; ?></a>
                        <a href="<?php echo site_url('orders/orders/print_order/' . $data->order_id); ?>" target="_blank" class="btn btn-primary  btn-block" id="print_order_all"><i class="fa fa-print"></i> Cetak Resi</a>
                        <?php if ($data->order_status < 2): ?>
                            <a href="" data-toggle="modal" data-target="#closed" class="btn btn-warning btn-block btn-closed">
                                <i class="fa fa-book"></i> Tutup Order
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-6">
                <div class="box box-primary">

                    <div class="box-body box-profile">

                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <h3 class="profile-username text-center">Pengirim (Asal) </h3>
                            </div>
                            <div class="col-md-2">
                                <input type="hidden" name ="edit-origin-id" value='<?php echo $origin_branch->branch_id; ?>' />
                                <input type="hidden" name ="edit-origin-text" value='<?php echo $data->order_origin_text; ?>' />
                                <button type="button" data-toggle="modal" data-target="#origin" class="btn btn-info btn-sm btn-flat pull-right btn-edit-origin">
                                    <i class="fa fa-edit"></i>
                                </button>
                            </div>
                        </div>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Kota</b> 
                                <a class="pull-right">
                                    <?php echo $origin_branch->city_name; ?>
                                </a>
                            </li>

                            <li class="list-group-item">
                                <b>Kecamatan</b> 
                                <a class="pull-right">
                                    <?php echo isset($origin_districs->districts_name) ? $origin_districs->districts_name : ''; ?>
                                </a>
                            </li>

                            <li class="list-group-item">
                                <b>Desa/Kelurahan</b>
                                <a class="pull-right">
                                    <?php echo isset($origin_villages->village_name) ? $origin_villages->village_name : ''; ?>
                                </a>
                            </li>

                            <li class="list-group-item">
                                <b>Dari Cabang</b> 
                                <a class="pull-right">
                                    <?php echo $origin_branch->branch_name; ?>
                                </a>
                            </li>

                        </ul>
                        <p>
                            <?php
                            echo trim(
                                    json_decode($data->order_origin_text, true)['order_origin_text'] . ', '
                                    . (isset($origin_villages->village_name) ? $origin_villages->village_name : '' ) . ', '
                                    . (isset($origin_districs->districts_name) ? $origin_districs->districts_name : '') . ', ', ', '
                            );
                            ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">

                <div class="box box-success">
                    <div class="box-body box-profile">

                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <h3 class="profile-username text-center">Penerima ( Tujuan )</h3>
                            </div>
                            <div class="col-md-2">
                                <input type="hidden" name ="edit-destination-id" value='<?php echo $destination_branch->branch_id; ?>' />
                                <input type="hidden" name ="edit-destination-text" value='<?php echo $data->order_destination_text; ?>' />
                                <button type="button" data-toggle="modal" data-target="#destination" class="btn btn-info btn-sm btn-flat pull-right btn-edit-destination">
                                    <i class="fa fa-edit"></i>
                                </button>
                            </div>
                        </div>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Kota</b> 
                                <a class="pull-right"> 
                                    <?php echo $destination_branch->city_name; ?>
                                </a>
                            </li>

                            <li class="list-group-item">
                                <b>Kecamatan</b> 
                                <a class="pull-right">
                                    <?php echo isset($destination_districs->districts_name) ? $destination_districs->districts_name : ''; ?>
                                </a>
                            </li>

                            <li class="list-group-item">
                                <b>Desa/Kelurahan</b> 
                                <a class="pull-right">
                                    <?php echo isset($destination_villages->village_name) ? $destination_villages->village_name : ''; ?>
                                </a>
                            </li>

                            <li class="list-group-item">
                                <b>Dari Cabang</b> 
                                <a class="pull-right">
                                    <?php echo $destination_branch->branch_name; ?>
                                </a>
                            </li>
                        </ul>
                        <p>

                            <?php
                            echo trim(
                                    json_decode($data->order_destination_text, true)['order_destination_text'] . ', '
                                    . (isset($destination_villages->village_name) ? $destination_villages->village_name : '' ) . ', '
                                    . (isset($destination_districs->districts_name) ? $destination_districs->districts_name : '') . ', ', ', '
                            );
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-warning">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="box-title">Detail Paket</h3>
                    </div>
                    <div class="col-md-6">
                        <div class="btn-group pull-right">
                            <a href="<?php echo site_url('orders/orders/print_multiple_barcode/' . $data->order_id); ?>" class="btn btn-warning print_barcode">
                                <i class="fa fa-print"></i> Cetak Semua
                            </a>
                            <a href="#" data-toggle="modal" data-target="#package_add" class="btn btn-info pull-right">
                                <i class="fa fa-plus"></i> Tambah Paket
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">

                <?php $total_weight = 0; ?>
                <?php $total_volume = 0; ?>

                <table class="table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Deskripsi</th>
                            <th>Jumlah Koli</th>
                            <th>Total Berat</th>
                            <th>Total Volume</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        <?php $total_sell = 0; ?>
                        <?php if (!empty($package)): ?>
                            <?php foreach ($package as $key => $value) : ?>

                                <tr>
                                    <td><?php echo $value->package_id; ?></td>
                                    <td><?php echo $value->package_title; ?></td>
                                    <td><?php echo $value->qty; ?></td>
                                    <td>
                                        <?php echo $value->package_weight * $value->qty; ?> Kg
                                        <?php $total_weight += ($value->package_weight * $value->qty); ?>
                                    </td>
                                    <td>
                                        <?php echo round(($value->package_size * $value->qty) / 1000000, 3); ?> M<sup>3</sup>
                                        <?php $total_volume += round(($value->package_size * $value->qty) / 1000000, 3); ?>
                                    </td>
                                    <td class="text-right" style="display: none;">
                                        <?php echo number_format($value->sell_total); ?>
                                        <?php $total_sell += $value->sell_total; ?>
                                    </td>
                                    <td>
                                        <div class="pull-right">
                                            <input type="hidden" name="edit-package" value='<?php echo json_encode($value); ?>' />
                                            <a href="<?php echo site_url('orders/orders/print_barcode/' . $value->package_id); ?>" class="btn btn-sm btn-warning print_barcode">
                                                <i class="fa fa-print"></i>
                                            </a>
                                            <button type="button" data-toggle="modal" data-target="#package" class="btn btn-sm btn-info btn-edit-package">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <tr>
                            <td colspan="2" text-align="center"><strong>Total Jumlah :</strong></td>
                            <td><?php echo $sum_qty; ?></td>
                            <td><?php echo $total_weight; ?> Kg</td>
                            <td><?php echo $total_volume; ?> M<sup>3</sup></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-right" colspan="4">Total </td>
                            <td class="text-right">
                                <strong>
                                    <?php
                                    $the_total = $data->order_sell_total;
                                    if ($data->order_use_tax == 1)
                                        $the_total = $the_total * (100 / 101);

                                    echo number_format($the_total);
                                    ?>
                                </strong>
                            </td>
                            <td class="text-right">
                                <input type="hidden" name="edit-total-order-price" value='<?php echo json_encode($the_total); ?>' />

                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-right" colspan="4">Pajak</td>
                            <td class="text-right">
                                <strong>
                                    <?php $result = 0; ?>
                                    <?php if ($data->order_use_tax == 1): ?>
                                        <?php
                                        $result = $data->order_sell_total - ($data->order_sell_total * (100 / 101));
                                        ?>
                                        <?php echo number_format($result); ?>
                                    <?php else: ?>
                                        <?php
                                        $result = $data->order_sell_total * (1 / 100);
                                        echo number_format($result);
                                        ?>
                                    <?php endif; ?>
                                </strong>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-right" colspan="3">Total Akhir</td>
                            <td class="text-right">
                                <strong>
                                    <?php echo number_format($result + $the_total); ?>
                                </strong>

                            </td>
                            <td></td>
                            <td>
                                <input type="hidden" name="edit-total-order-price" value='<?php echo json_encode($the_total); ?>' />
                                <button type="button" data-toggle="modal" data-target="#total_order_price" class="btn btn-sm btn-info btn-edit-total-order-price pull-left">
                                    <i class="fa fa-edit"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Extra</h3>
            </div>
            <div class="box-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th width="5%" >No.</th>
                            <th width="80%" >Nama Extra</th>
                            <th width="10%"  class="text-right">Total</th>
                            <th width="5%" ></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php $total_extra = 0; ?>
                        <?php if (!empty($det_order_extra)): ?>
                            <?php foreach ($det_order_extra as $key => $value) : ?>

                                <tr>
                                    <td>1</td>
                                    <td>
                                        <?php echo $value->det_order_extra_name; ?>
                                    </td>
                                    <td class="text-right">
                                        <?php echo number_format($value->det_order_extra_total); ?>
                                        <?php $total_extra += $value->det_order_extra_total; ?>
                                    </td>
                                    <td>
                                        <input type="hidden" name="edit-orders-extra" value='<?php echo json_encode($value); ?>' />
                                        <button type="button" data-toggle="modal" data-target="#orders-extra" class="btn btn-sm btn-info btn-edit-orders-extra">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </td>
                                </tr>

                            <?php endforeach; ?>
                        <?php endif; ?>
                        <tr>
                            <td colspan="2">Total</td>
                            <td class="text-right">
                                <strong><?php echo number_format($total_extra); ?></strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<?php $this->load->view('footer'); ?>
<?php $this->load->view('order/js'); ?>
