<script type="text/javascript" >

    var column_default = [
        {data: 'created_at'},
        {data: 'order_id'},
        {data: 'customer_name'},
        {data: 'district'},
        {data: 'qty'},
        {data: 'total_weight'},
        {data: 'volume'},
        {data: 'resi_ages'},
        {data: 'action'}
    ];

    // Set default tab
    if ($.session.get('tab-order-list') != '') {
        var href_ = '#' + $.session.get('tab-order-list');
        shown_tab(column_default);
        $('a[href="' + href_ + '"]').trigger('click');
    }

    /** New order list */
    function shown_tab(column_default) {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

            /** Variable initiation */
            var target = $(e.target).attr("href");
            var status;
            var column = column_default;

            target = target.replace("#", "");

            $.session.set('tab-order-list', target);

            switch (target) {
                case 'must-be-processed':
                    status = 0;
                    column = column_default;
                    break;
                case 'be-received':
                    status = 1;
                    column = [
                        {data: 'created_at'},
                        {data: 'order_load_date'},
                        {data: 'order_id'},
                        {data: 'customer_name'},
                        {data: 'district'},
                        {data: 'qty'},
                        {data: 'total_weight'},
                        {data: 'volume'},
                        {data: 'resi_ages'},
                        {data: 'status'},
                        {data: 'action'}
                    ];
                    break;
                case 'pack-drops':
                    status = 2;
                    column = [
                        {data: 'created_at'},
                        {data: 'order_load_date'},
                        {data: 'order_unload_date'},
                        {data: 'order_id'},
                        {data: 'customer_name'},
                        {data: 'district'},
                        {data: 'qty'},
                        {data: 'total_weight'},
                        {data: 'volume'},
                        {data: 'resi_ages'},
                        {data: 'action'}
                    ];
                    break;
                case 'delivery':
                    status = 3;
                    column = [
                        {data: 'created_at'},
                        {data: 'order_delivered_date'},
                        {data: 'order_id'},
                        {data: 'customer_name'},
                        {data: 'district'},
                        {data: 'qty'},
                        {data: 'total_weight'},
                        {data: 'volume'},
                        {data: 'resi_ages'},
                        {data: 'action'}
                    ];
                    break;
                case 'in-the-process':
                    status = 4;
                    column = [
                        {data: 'created_at'},
                        {data: 'order_id'},
                        {data: 'customer_name'},
                        {data: 'district'},
                        {data: 'qty'},
                        {data: 'total_weight'},
                        {data: 'volume'},
                        {data: 'resi_ages'},
                        {data: 'origin_status'},
                        {data: 'order_update_date'},
                        {data: 'action'}
                    ];
                    break;
                case 'finish':
                    status = 5;
                    column = [
                        {data: 'created_at'},
                        {data: 'order_received_date'},
                        {data: 'order_id'},
                        {data: 'customer_name'},
                        {data: 'district'},
                        {data: 'qty'},
                        {data: 'total_weight'},
                        {data: 'volume'},
                        {data: 'resi_ages'},
                        {data: 'action'}
                    ];
                    break;
                case 'not-pay':
                    status = 6;
                    column = column_default;
                    break;
            }

            data_tables(status, target, column);

        });
    }

    /** Datatable default setup */
    data_tables(0, 'must-be-processed', column_default);

    function data_tables(status, id_table, column) {

        var url = '<?php echo site_url($path) ?>/index/' + status;

        $('.' + id_table).dataTable().fnDestroy();

        $('.' + id_table).DataTable({
            serverSide: true,
            ordering: true,
            order: [
                [
                    <?php dtorder_column('dt_order', 0); ?>,
                    "<?php dtorder_mode('dt_order', 'desc'); ?>"
                ]
            ],
            displayStart: <?php dtarray('dt_order', 'start', 0); ?>,
            pageLength: <?php dtarray('dt_order', 'lenght', 10); ?>,
            search: {
                search: "<?php dtarray('dt_order', 'search', ''); ?>"
            },
            ajax: {
                url: url,
                type: 'POST',
            },
            columns: column,
            footerCallback: function (row, data, start, end, display) {

                var koli = 4;
                var weight = 5;
                var vol = 6;

                if (status == 1 || status == 5 || status == 3) {
                    koli = 5;
                    weight = 6;
                    vol = 7;
                }

                if (status == 2) {
                    koli = 6;
                    weight = 7;
                    vol = 8;
                }

                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

                // // Total Koli
                var qty = api
                        .column(koli)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Total -> Total Berat
                var weight_total = api
                        .column(weight)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Total Volume
                var volume = api
                        .column(vol)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Update footer
                $(api.column(0).footer()).html('Total : ');
                $(api.column(koli).footer()).html(Number((qty).toFixed(3)) + ' Koli');
                $(api.column(weight).footer()).html(Number((weight_total).toFixed(3)) + ' Kg');
                $(api.column(vol).footer()).html(Number((volume).toFixed(3)) + ' m<sup>3<sup>');

                // Get total from all page and update the footer 
                $.get("<?php echo site_url($path . '/get_total'); ?>/" + status, function (data) {
                    var result = JSON.parse(data);
                    $('.total-package').html('Jumlah Total Koli : ' + result['total_koli'] + ' Koli');
                    $('.total-weight').html('Jumlah Total Berat : ' + result['total_weight'] + 'Kg');
                    $('.total-size').html('Jumlah Total Volume : ' + result['total_size'] + ' m<sup>3</sup>');
                });

            }
        });
    }

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus data order dengan nomor resi <strong>" + data['order_id'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);

        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['order_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });

    /** Close order */
    $('body').on('click', '.btn-closed-final', function () {
        var data_edit = $(this).parent().parent().find('input[name="edit-value"]').val();
        data_edit = JSON.parse(data_edit);
        /** Set url */
        var url = "<?php echo site_url($path . '/paid'); ?>/" + data_edit['order_id'];
        $('#closed_final').find('.modal-footer a.a-closed').attr('href', url);
    });

    /** Self take modal */
    $('body').on('click', '.btn-self-take', function () {
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);
        $('#modal-self-take').find('input[name="order_id"]').val(data['order_id']);
        $('#modal-self-take').modal('show');
    });

    /** Delivery modal */
    $('body').on('click', '.btn-delivery', function () {
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);
        $('#modal-delivery').find('input[name="order_id"]').val(data['order_id']);
        $('#modal-delivery').modal('show');
    });


    /** Package rollback modal */
    $('body').on('click', '.btn-package-rollback', function () {
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);
        $('#modal-package-rollback').find('input[name="order_id"]').val(data['order_id']);
        $('#modal-package-rollback').modal('show');
    });

    // Set Select2
    $('select[name="as_branch"]').select2();

    // Clear Session
    $('.btn-clear').click(function () {
        $.session.remove('tab-order-list');
    });

</script>