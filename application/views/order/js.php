<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#print_order_all').click(function (e) {

            <?php if ($order_status == -3): ?>

                    setTimeout(function () {
                        location.reload(true);
                    }, 2000);

            <?php endif; ?>

            e.preventDefault();
            var targets = $(this).attr('href');
            $("<iframe>")                             // create a new iframe element
                    .hide()                               // make it invisible
                    .attr("src", targets) // point the iframe to the page you want to print
                    .appendTo("body");
        });

        $('.print_barcode').click(function (e) {
            e.preventDefault();
            targets = $(this).attr('href');
            $("<iframe>")                             // create a new iframe element
                    .hide()                               // make it invisible
                    .attr("src", targets) // point the iframe to the page you want to print
                    .appendTo("body");
        });

    });

    /** Origin Edit */
    var edit_origin_ids = 0;
    var edit_origin_text = 0;

    $('.btn-edit-origin').click(function () {
        edit_origin_ids = $(this).parent().find('input[name="edit-origin-id"]').val();
        edit_origin_text = $(this).parent().find('input[name="edit-origin-text"]').val();
        edit_origin_text = JSON.parse(edit_origin_text);
    });

    $('#origin').on('shown.bs.modal', function (e) {

        /** Select city */
        $('select[name="city_id_origin"]').val(edit_origin_text['city_id_origin']);
        $('select[name="city_id_origin"]').on('change', function () {
            set_origin_districts($(this).val());
            set_origin_branch($(this).val());
        });

        set_origin_districts(edit_origin_text['city_id_origin']);
        set_origin_village(edit_origin_text['districts_id_origin']);
        set_origin_branch(edit_origin_text['city_id_origin']);
        $('textarea[name="order_origin_text"]').val(edit_origin_text['order_origin_text']);

        /** Select 2 */
        $(".select2").find('select').select2();

    });

    function set_origin_districts(city_id) {
        $.post('<?php echo site_url('request/locations/districts/districts_id_origin'); ?>',
                {city_id: city_id},
        function (data) {
            var form = $('select[name="districts_id_origin"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="districts_id_origin"]').val(edit_origin_text['districts_id_origin']);
            $(".select2").find('select').select2();
            /** Select district */
            $('body').on('change', 'select[name="districts_id_origin"]', function () {
                /** Get village list from server  */
                set_origin_village($(this).val());
            });
        });
    }

    function set_origin_village(districts_id) {
        /** Get village list from server  */
        $.post('<?php echo site_url('request/locations/villages/village_id_origin'); ?>',
                {districts_id: districts_id},
        function (data) {
            var form = $('select[name="village_id_origin"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="village_id_origin"]').val(edit_origin_text['village_id_origin']);
            $(".select2").find('select').select2();
        }
        );
    }

    function set_origin_branch(city_id) {
        /** Get branch list from server  */
        $.post('<?php echo site_url('request/locations/branches/branch_id_origin'); ?>',
                {city_id: city_id},
        function (data) {
            var form = $('select[name="branch_id_origin"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="branch_id_origin"]').val(edit_origin_ids);
            $(".select2").find('select').select2();
        }
        );
    }

    /** Destination Edit */
    var edit_destination_ids = 0;
    var edit_destination_text = 0;

    $('.btn-edit-destination').click(function () {
        edit_destination_ids = $(this).parent().find('input[name="edit-destination-id"]').val();
        edit_destination_text = $(this).parent().find('input[name="edit-destination-text"]').val();
        edit_destination_text = JSON.parse(edit_destination_text);
    });

    $('#destination').on('shown.bs.modal', function (e) {

        /** Select city */
        $('select[name="city_id_destination"]').val(edit_destination_text['city_id_destination']);
        $('select[name="city_id_destination"]').on('change', function () {
            set_destination_districts($(this).val());
            set_destination_branch($(this).val());
        });

        set_destination_districts(edit_destination_text['city_id_destination']);
        set_destination_village(edit_destination_text['districts_id_destination']);
        set_destination_branch(edit_destination_text['city_id_destination']);
        $('textarea[name="order_destination_text"]').val(edit_origin_text['order_destination_text']);

        /** Select 2 */
        $(".select2").find('select').select2();

    });

    function set_destination_districts(city_id) {
        $.post('<?php echo site_url('request/locations/districts/districts_id_destination'); ?>',
                {city_id: city_id},
        function (data) {
            var form = $('select[name="districts_id_destination"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="districts_id_destination"]').val(edit_destination_text['districts_id_destination']);
            $(".select2").find('select').select2();
            /** Select district */
            $('body').on('change', 'select[name="districts_id_destination"]', function () {
                /** Get village list from server  */
                set_destination_village($(this).val());
            });
        }
        );
    }

    function set_destination_village(districts_id) {
        /** Get village list from server  */
        $.post('<?php echo site_url('request/locations/villages/village_id_destination'); ?>',
                {districts_id: districts_id},
        function (data) {
            var form = $('select[name="village_id_destination"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="village_id_destination"]').val(edit_destination_text['village_id_destination']);
            $(".select2").find('select').select2();
        }
        );
    }

    function set_destination_branch(city_id) {
        /** Get branch list from server  */
        $.post('<?php echo site_url('request/locations/branches/branch_id_destination'); ?>',
                {city_id: city_id},
        function (data) {
            var form = $('select[name="branch_id_destination"]').parent().parent();
            $(form).replaceWith(data);
            $('select[name="branch_id_destination"]').val(edit_destination_ids);
            $(".select2").find('select').select2();
        }
        );
    }

    /** Pakcage Edit */
    var package = 0;
    $('.btn-edit-package').click(function () {
        package = $(this).parent().find('input[name="edit-package"]').val();
        package = JSON.parse(package);
        console.log(package);
    });

    $('#package').on('shown.bs.modal', function (e) {
        $('input[name="package_id"]').val(package['package_id']);
        $('input[name="package_title"]').val(package['package_title']);
        $('input[name="package_width"]').val(package['package_width'] * 100);
        $('input[name="package_qty"]').val(package['qty']);
        $('input[name="package_lenght"]').val(package['package_lenght'] * 100);
        $('input[name="package_height"]').val(package['package_height'] * 100);
        $('input[name="package_weight"]').val(package['package_weight']);
        $('select[name="package_type"]').val(package['package_type']);
        $('textarea[name="package_content"]').val(package['package_content']);
    });

    /** Edit total order */
    var total_order = 0;
    $('.btn-edit-total-order-price').click(function () {
        total_order_price = $(this).parent().find('input[name="edit-total-order-price"]').val();
        total_order_price = JSON.parse(total_order_price);
        console.log(total_order_price);
    });

    $('#total_order_price').on('shown.bs.modal', function (e) {
        $('input[name="det_total_order_price"]').val(total_order_price.toFixed(0));
    });

    /** Edit orders extra  */
    var orders_extra = 0;
    $('.btn-edit-orders-extra').click(function () {
        orders_extra = $(this).parent().find('input[name="edit-orders-extra"]').val();
        orders_extra = JSON.parse(orders_extra);
    });

    $('#orders-extra').on('shown.bs.modal', function (e) {
        $('input[name="det_order_extra_id"]').val(orders_extra['det_order_extra_id']);
        $('input[name="det_order_extra_name"]').val(orders_extra['det_order_extra_name']);
        $('input[name="det_order_extra_total"]').val(orders_extra['det_order_extra_total']);
    });

    /** Close order */
    $('body').on('click', '.btn-closed', function () {

        /** Set url */
        var url = "<?php echo site_url($path . '/close/' . $data->order_id) ?>"
        $('#closed').find('.modal-footer a.a-closed').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#closed').find('.modal-footer a.a-closed').attr('href', '');
    });

    $('.btn-save-update').click(function () {
        var url = $('.form-package-add').attr('action');
        url += '/true';
        $('.form-package-add').attr('action', url);
        $('.form-package-add').submit();
    });

    /** Delete Package*/
    $('table.package-detail').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-package"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus data order dengan nomor resi <strong>" + data['package_id'] + " - " + data['package_title'] + "</strong>?";
        $('#modal-package-delete-alert').find('.modal-body p').html(text);

        /** Set url */
        var url = "<?php echo site_url('orders/orders_update/package_destroy') ?>/" + data['order_id'] + '/' + data['package_id'];
        $('#modal-package-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-package-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-package-delete-alert').find('.modal-body p').empty();
        $('#modal-package-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });

</script>