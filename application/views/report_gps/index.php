<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4><?php echo $title; ?></h4>    
                    </div>
                    <div class="col-md-6">
                        <div class="btn-group pull-right">
                            <a href="<?php echo site_url($path.'/clear'); ?>" class="btn btn-info">
                                <i class="fa fa-refresh"></i>
                                Reset
                            </a>
                            <a href="<?php echo site_url($path.'/download'); ?>" class="btn btn-success">
                                <i class="fa fa-file-excel-o"></i>
                                Download
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive ">
                <?php get_alert(); ?>

                <table class="table data-tables">
                    <thead>
                        <tr>
                            <th width="10%">ID</th>
                            <th width="30%">Nama Armada</th>
                            <th width="15%">Plat Nomor</th>
                            <th width="15%">Nomor Device</th>
                            <th width="25%">Lokasi Terakhir</th>
                            <th width="5%">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>	
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    /** Data tables set up */
    $('.data-tables').DataTable({
        serverSide: true,
        ordering: true,
        order: [
            [
                <?php dtorder_column('dt_report_gps', 0); ?>,
                "<?php dtorder_mode('dt_report_gps', 'asc'); ?>"
            ]
        ],
        displayStart: <?php dtarray('dt_report_gps', 'start', 0); ?>,
        pageLength: <?php dtarray('dt_report_gps', 'lenght', 10); ?>,
        search: {
            search: "<?php dtarray('dt_report_gps', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>',
            type: 'POST'
        },
        columns: [
            {data: 'armada_id'},
            {data: 'armada_name'},
            {data: 'armada_license_plate'},
            {data: 'traccar_device_id'},
            {data: 'armada_last_positin'},
            {data: 'action'}
        ]
    }
    );

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus kota <strong>" + data['city_name'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);
        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['city_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    })



</script>