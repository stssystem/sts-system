<div  class="form-group <?php echo error($name) ? 'has-error ' : ''; ?><?php echo isset($class) ? $class : ''; ?>" 
<?php if (isset($out_attribute)): ?>
    <?php foreach ($out_attribute as $k => $v) : ?>
        <?php echo "$k='$v'"; ?>
    <?php endforeach; ?>
<?php endif; ?>
      >
    <label class="control-label" for="inputSuccess1"><?php echo isset($label) ? $label : ''; ?></label>

    <?php if ((isset($type) && isset($value) && is_array($value)) && ($type == 'radio' || $type == 'checkbox' )): ?>

        <br/>
        <?php if (!empty($value)): ?>
            <?php foreach ($value as $key => $item) : ?>

                <input 
                    type="<?php echo isset($type) ? $type : ''; ?>" 
                    name="<?php echo isset($name) ? $name : ''; ?>" 
                    value ='<?php echo $key; ?>'
                    
                    <?php echo (isset($checked) && in_array($key, $checked)) || (old_input($name) != '' && old_input($name) == $key) ? 'checked="checked"' : ''; ?>
                    <?php echo ($required) ? 'required="required"' : ''; ?>
                    /> <?php echo $item; ?> &nbsp;&nbsp;

            <?php endforeach; ?>
        <?php endif; ?>


    <?php else: ?>

        <input 
            type="<?php echo isset($type) ? $type : ''; ?>" 
            class="form-control" 
            name="<?php echo isset($name) ? $name : ''; ?>" 
            max='<?php echo isset($max) ? $max : ''; ?>'
            maxlength ='<?php echo isset($max) ? $max : ''; ?>'
            placeholder="Masukkan <?php echo isset($label) ? $label : ''; ?> ..."
            value ='<?php echo (!isset($value) || old_input($name)) ? old_input($name) : $value; ?>'
            <?php echo isset($disabled) && $disabled == true ? 'disabled' : ''; ?> 
            <?php if (isset($attribute) && !empty($attribute)): ?>
                <?php foreach ($attribute as $key => $value) : ?>
                    <?php echo "$key='$value'"; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php echo isset($required) && $required == true ? 'required="required"' : ''; ?>
            />

    <?php endif; ?>

    <span class="help-block"><?php echo error($name); ?></span>
</div>

<?php $attribute = []; ?>
