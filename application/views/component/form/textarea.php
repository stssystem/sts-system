<div class="form-group <?php echo error($name) ? 'has-error' : ''; ?> <?php echo isset($class) ? $class : ''; ?>">
    <label class="control-label" for="inputSuccess1"><?php echo isset($label) ? $label : ''; ?></label>
    <textarea
        class="form-control" 
        name="<?php echo isset($name) ? $name : ''; ?>" 
        max='<?php echo isset($max) ? $max : ''; ?>'
        rows='<?php echo isset($rows) ? $rows : ''; ?>'
        placeholder="Masukkan <?php echo isset($label) ? $label : ''; ?> ..."
        <?php echo isset($required) && $required == true  ? 'required="required"' : ''; ?>
        ><?php echo (!isset($value) || old_input($name)) ? old_input($name) : $value; ?></textarea>
    <span class="help-block"><?php echo error($name); ?></span>
</div>