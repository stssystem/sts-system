<div class="form-group <?php echo error($name) ? 'has-error' : ''; ?> <?php echo isset($class) ? $class : ''; ?>">

    <?php if (isset($label) && $label != ''): ?>
        <label class="control-label col-sm-3" for="inputSuccess1"><?php echo isset($label) ? $label : ''; ?></label>
    <?php endif; ?>

    <div class="col-sm-9">
        <select 
            name="<?php echo isset($name) ? $name : ''; ?>"  
            <?php echo isset($required) && $required == true ? 'required="required"' : ''; ?> 
            <?php echo isset($multiple) && $multiple == true ? 'multiple="multiple"' : ''; ?> 
            class="form-control"
            <?php echo isset($disabled) && $disabled == true ? 'disabled' : ''; ?> 
            > 
                <?php
                $display_default = true;
                if (isset($use_default))
                    $display_default = $use_default;
                if ($display_default == true) :
                    ?>
                <option value="">-- Pilih <?php echo $label; ?> --</option>
            <?php endif; ?>
            <?php if (!empty($value)): ?>
                <?php foreach ($value as $key => $item) : ?>
                    <?php $key__ = isset($keys) && $keys != null ? $item[$keys] : $key; ?>
                    <option 
                        value="<?php echo $key__; ?>" 
                        <?php echo ((isset($selected) && ($selected == $key__)) || (old_input($name) != '' && old_input($name) == $key__ )) ? 'selected="selected"' : ''; ?>
                        >
                            <?php echo isset($values) && $values != '' ? $item[$values] : $item; ?>
                    </option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
        
        <span class="help-block"><?php echo error($name); ?></span>
        
    </div>

</div>

<?php unset($values); ?>
