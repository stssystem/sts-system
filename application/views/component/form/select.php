<div class="form-group <?php echo error($name) ? 'has-error' : ''; ?> <?php echo isset($class) ? $class : ''; ?>">
    <?php if (isset($label) && $label != ''): ?>
        <label class="control-label" for="inputSuccess1"><?php echo isset($label) ? $label : ''; ?></label>
    <?php endif; ?>

    <select 
        name="<?php echo isset($name) ? $name : ''; ?>"  
        <?php echo isset($required) && $required == true ? 'required="required"' : ''; ?> 
        <?php echo isset($multiple) && $multiple == true ? 'multiple="multiple"' : ''; ?> 
        class="form-control"
        <?php echo isset($disabled) && $disabled == true ? 'disabled' : ''; ?> 
        > 

        <?php if (isset($display_default) && $display_default == true) : ?>
            <option value="">-- Pilih <?php echo $label; ?> --</option>
        <?php endif; ?>

        <?php if (!empty($value)): ?>
            <?php foreach ($value as $key => $item) : ?>
                <?php $key__ = isset($keys) && $keys != null ? $item[$keys] : $key; ?>
                <option
                    value="<?php echo $key__; ?>" 
                    <?php if (isset($selected) && !is_array($selected)): ?>
                        <?php echo ((isset($selected) && ($selected == $key__)) || (old_input($name) != '' && old_input($name) == $key__ )) ? 'selected="selected"' : ''; ?>
                    <?php elseif (isset($selected) && is_array($selected)): ?>
                        <?php echo (isset($selected) && in_array($key__, $selected)) ? 'selected="selected"' : ''; ?>
                    <?php endif; ?>
                    data-item="<?php echo (isset($data_id) && ($data_id != '' || $data_id != NULL ))?$item[$data_id]:''; ?>"
                    >
                        <?php echo isset($values) && $values != '' ? $item[$values] : $item; ?>
                </option>
            <?php endforeach; ?>
        <?php endif; ?>
    </select>

    <span class="help-block"><?php echo error($name); ?></span>
</div>

<?php unset($values); ?>
