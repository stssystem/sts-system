<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete-alert">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Peringatan Hapus Data</h4>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-ban"></i>
                        Batal
                    </button>
                    <a href="" class="btn btn-danger a-delete">
                        <i class="fa fa-trash"></i>
                        Hapus
                    </a>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
