<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/package-list'); ?>
<?php $origin_branch = []; ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">
                Daftar Paket Berikut di Generate Pada <?php echo $last_generate; ?>
            </div>
            <div class="box-footer">
                <a href="#" class="btn btn-primary">Buat Pre-Manifest Otomatis</a>
            </div>
        </div>
    </div>

    <hr />


    <?php foreach ($data as $rows): ?>

        <?php if (!in_array($rows->origin_branch_id, $origin_branch)): ?>
            <div class="col-sm-3">
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-yellow" style="height: 120px;">
                        <div class="widget-user-image">
                            <img class="img-circle" src="<?php echo image($rows->photo_manager); ?>" alt="User Avatar">
                        </div>
                        <!-- /.widget-user-image -->
                        <h4 class="widget-user-username" style="font-size: 20px;"><?php echo $rows->origin_branch_name; ?></h4>
                        <h5 class="widget-user-desc"><?php echo $rows->branch_manager; ?></h5>
                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked" style="overflow-x: auto; height: 200px;">
                            <?php $totals = 0; ?>
                            <?php foreach ($data as $rows_i): ?>
                                <?php if ($rows_i->origin_branch_id == $rows->origin_branch_id): ?>
                                    <li>
                                        <a href="<?php echo site_url($path.'/show_package_per_pre/'.$rows->origin_branch_id.'/'.$rows_i->destination_branch_id); ?>">
                                            <?php echo $rows_i->destination_branch_name; ?>
                                            <span class="pull-right badge bg-blue">
                                                <?php echo $rows_i->total; ?>
                                                <?php $totals += $rows_i->total; ?>
                                            </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                         
                        </ul>
                        <ul class="nav nav-stacked">
                               <li>
                                <a href="<?php echo site_url($path.'/show_package_per_pre/'.$rows->origin_branch_id); ?>" >
                                    <strong>Total Paket</strong>
                                    <span class="pull-right badge bg-red">
                                        <?php echo $totals; ?>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <?php $origin_branch[] = $rows->origin_branch_id; ?>
        <?php endif; ?>

    <?php endforeach; ?>


</div>

<?php $this->load->view('footer'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.view-detail-package').click(function () {
            $('#modal-package-manifest').modal('show');
            $('#modal-package-manifest .modal-title').html($(this).data('branch_name'));
            pre_manifest_id = $(this).data('pre_manifest_id');
            $('#modal-package-manifest .modal-body').load('<?php echo site_url('manifests/manifests/show_package_per_pre'); ?>/' + pre_manifest_id);
        })
    })
</script>