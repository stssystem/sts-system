<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<style type="text/css">
    .unloadtype-class{
        margin-top: -23px;
    }

    .txt-sm{
        font-size: 14px;
    }

</style>
<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Pilih Armada</h3>
            </div>
            <div class="box-body">

                <div class="row">

                    <div class="col-md-12">
                        <div class="alert alert-danger armada-correction-false armada-alert" role="alert"> 
                            <strong><i class="fa fa-warning"></i> Peringatan!</strong> Armada yang dipilih tidak melalui kota tujuan paket 
                        </div>

                        <div class="alert alert-danger armada-selection armada-alert" role="alert"> 
                            <strong><i class="fa fa-warning"></i> Peringatan!</strong> Belum ada armada yang dipilih
                        </div>

                        <div class="alert alert-success armada-correction-true armada-alert" role="alert"> 
                            <strong><i class="fa fa-check"></i> Tersedia!</strong> Armada yang dipilih melalui kota tujuan paket 
                        </div>
                    </div>

                    <div class="col-md-12" >

                        <div class="form-group armadas-select">
                            <?php
                            echo form_dropdown('armada_id', get_armada_norestict(), '', 'class="form-control select2"');
                            ?>
                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h4>Scan Per Paket <span class="loading-process"></span></h4>
                        <input type="text" class="form-control input-lg is-correct-armada" name="package_id" id="package_id">
                    </div>
                    <div class="col-md-6">
                        <h4>Scan Per Resi <span class="loading-process-resi"></span></h4>
                        <input type="text" class="form-control input-lg is-correct-armada" name="order_id" id="order_id">
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="error-alert">

                        </div>
                        <form class="form-loading">
                            <table class="table table-bordered table-striped txt-sm">
                                <thead>
                                    <tr>
                                        <th colspan="10">
                                            Daftar Paket Siap Diangkut
                                        </th>
                                    </tr>
                                    <tr>
                                        <th width="10%" >ID Paket / Order</th>
                                        <th width="20%" >Nama Paket / Pengirim</th>
                                        <th colspan="2" width="35%" style="text-align:center">Keterangan</th>
                                        <th width="13%" >Asal-Tujuan</th>  
                                        <th width="10%" >Jumlah Koli</th>  
                                        <th width="10%" >Jumlah Volume</th>
                                        <th width="10%" >Jumlah Berat</th>
                                        <th width="5%" ></th>
                                    </tr>
                                </thead>
                                <tbody id="load_result" >

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>

            </div>
            <div class="box-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-danger btn-flat btn-delete-all"><i class="fa fa-times"></i> Hapus Semua</button>
                    <button type="button" class="btn btn-info btn-flat btn-loading"><i class="fa fa-truck"></i> Angkut</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        /** Checkbox styling */
        $('input[name="unloading_type"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            increaseArea: '30%'
        });

        /** Select 2 */
        $(".select2").select2();

        /** Loaded by package id  */
        $('#package_id').keypress(function (event) {
            if (event.which == 13) {

                if ($('select[name="armada_id"]').val() != 0) {

                    p_id = $(this).val();
                    unloading_type = $('input[name="unloading_type"]').prop('checked');
                    trip_id = $('select[name="armada_id"]').val();

                    $('.loading-process').append('<span class="fa fa-spinner fa-spin"></span>');

                    $.post('<?php echo site_url('packages/packages/process_loadup'); ?>',
                            {package_id: p_id, unloading_type: unloading_type, trip_id: trip_id}, function (data) {

                        if (data === 'false' || data === 'null') {

                            if (data === 'false') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Paket dengan kode <strong>' + p_id + '</strong> tidak ditemukan'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            if (data === 'null') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Paket dengan kode <strong>' + p_id + '</strong> sudah masuk ke dalam daftar angkut barang'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            $(".error-alert").html(str);

                            setTimeout(function () {
                                $('.not-found').fadeOut('slow');
                            }, 2000);

                        } else {

                            $("#load_result").append(data);
                            $('#package_id').val('');

                        }

                        $('.armada-correction-false').hide();
                        $('.armada-correction-true').hide();
                        $('input[name="package_id"]').focus();

                        $('.loading-process').empty();

                    });

                    $('.armada-selection').hide();

                } else {
                    $('.armada-selection').show();
                }

            }
        });

        /** Loading by resi number */
        $('input[name="order_id"]').keypress(function (event) {
            if (event.which == 13) {
                if ($('select[name="armada_id"]').val() != 0) {
                    order_id = $(this).val();
                    unloading_type = $('input[name="unloading_type"]').prop('checked');
                    trip_id = $('select[name="armada_id"]').val();

                    $('.loading-process-resi').append('<span class="fa fa-spinner fa-spin"></span>');

                    $.post('<?php echo site_url('packages/packages/process_resi_number'); ?>',
                            {order_id: order_id, unloading_type: unloading_type, trip_id: trip_id}, function (data) {

                        if (data === 'false' || data === 'null') {

                            if (data === 'false') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Nomor Resi <strong>' + order_id + '</strong> tidak ditemukan'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            if (data === 'null') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Nomor Resi <strong>' + order_id + '</strong> sudah masuk ke dalam daftar angkut barang'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            $(".error-alert").html(str);

                            setTimeout(function () {
                                $('.not-found').fadeOut('slow');
                            }, 2000);

                        } else {
                            $("#load_result").append(data);
                            $('input[name="order_id"]').val('');
                        }

                        $('.armada-correction-false').hide();
                        $('.armada-correction-true').hide();
                        $('input[name="order_id"]').focus();
                        $('.armada-selection').hide();

                        $('.loading-process-resi').empty();

                    });
                } else {
                    $('.armada-selection').show();
                }
            }
        });

    });

    /** final process loading */
    $('.btn-loading').click(function () {
        var data = $('form[class="form-loading"]').serializeArray();
        var trip = $('select[name="armada_id"]').val();
        var unloading_type = $('input[name="unloading_type"]').prop('checked');
        $('.btn-loading').empty();
        $('.btn-loading').append('<span class="fa fa-spinner fa-spin"></span> Dalam Proses');
        $('.btn-loading').addClass('disabled');
        $.post('<?php echo site_url('packages/packages/final_loading_proccess'); ?>',
                {data: data, trip: trip, unloading_type: unloading_type}, function (data) {
            if (data === 'true') {
                $('td.actions').empty();
                $('td.actions').append('<span class="label label-success pull-right"><i class="fa fa-check green"></i> Berhasil</span>');
            } else {
                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Tidak ada data yang dimasukkan'
                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                        + '</div>';

                $(".error-alert").html(str);

                setTimeout(function () {
                    $('.not-found').fadeOut('slow');
                }, 2000);

            }

            $('.btn-loading').empty();
            $('.btn-loading').append('<span class="fa fa-truck"></span> Angkut');
            $('.btn-loading').removeClass('disabled');
            $('.armada-alert').hide();

        });
    });

    $('input[name="package_id"]').on('keyup', function (event) {
        if (event.which != 13) {
            $('input[name="order_id"]').val('');
            is_correct_armada();
        }
    });

    $('input[name="order_id"]').on('keyup', function (event) {
        if (event.which != 13) {
            $('input[name="package_id"]').val('');
            is_correct_armada();
        }
    });

    $('select[name="armada_id"]').change(function () {
        is_correct_armada();
        $('.armada-selection').hide();
    });

    $('.armada-correction-false').hide();
    $('.armada-correction-true').hide();
    $('.armada-selection').hide();

    /** Remove package */
    $('body').on('click', '.btn-delete', function () {

        var html = $(this).parent().parent().parent();

        var order_session = $(this).parent().parent().find('input[name="order_session"]').val();
        var package_session = $(this).parent().parent().find('input[name="package_session"]').val();

        $.post('<?php echo site_url($path . '/loading_unset'); ?>', {order_session: order_session, package_session: package_session}, function (data) {
            $(html).remove();
        });
    });

    /** Remove all package */
    $('.btn-delete-all').click(function () {
        $.get('<?php echo site_url($path . '/loading_unset_all'); ?>', function (data) {
            $("#load_result").empty();
        });
    });

    function is_correct_armada() {

        var trip_id = $('select[name="armada_id"]').val();
        var package_id = $('input[name="package_id"]').val();
        var order_id = $('input[name="order_id"]').val();

        if ((package_id != '' || order_id != '') && trip_id != 0) {
            $.post('<?php echo site_url('packages/packages/is_correct_armada'); ?>',
                    {trip_id: trip_id, package_id: package_id, order_id: order_id}, function (data) {
                if (data == 'true') {
                    $('.armada-correction-false').hide();
                    $('.armada-correction-true').show();
                } else {
                    $('.armada-correction-false').show();
                    $('.armada-correction-true').hide();
                }
            });
        }

    }
</script>