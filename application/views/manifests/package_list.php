<?php $this->load->view('header'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">

            <?php echo form_open($path . '/manifest'); ?>
            <input type="hidden" name ="origin" value="<?php echo $origin; ?>" />
            <?php if (isset($destination)): ?>
                <input type="hidden" name ="destination" value="<?php echo $destination; ?>" />
            <?php endif; ?>

            <div class="box-header">

                <div class="row">
                    <div class="box-title col-md-8">
                        <h4 >Berikut adalah daftar paket : <?php echo $rute; ?> </h4>
                    </div>
                    <div class="box-tools col-md-4 ">
                        <div class="btn-group pull-right">
                            <a href="<?php echo site_url($path . '/pre_manifest_list'); ?>" class="btn btn-info btn-flat">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">

                <?php echo get_alert(); ?>

                <table class="table">
                    <thead>
                        <tr><th>No.</th><th>Resi</th><th>Nama Paket</th><th>Asal</th><th>Tujuan</th><th>Volume</th><th>Berat</th><th>Pilih Armada</th></tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($data)): ?>
                            <?php foreach ($data as $key => $value) : ?>
                                <tr>
                                    <td>

                                        <input type="hidden" name ="package_id[]" value="<?php echo $value->package_id; ?>" />
                                        <input type="hidden" name ="pre_manifest_id[]" value="<?php echo $value->pre_manifest_id; ?>" />
                                        <input type="hidden" name ="origin_branch_id[]" value="<?php echo $value->origin_branch_id; ?>" />
                                        <input type="hidden" name ="destination_branch_id[]" value="<?php echo $value->destination_branch_id; ?>" />
                                        <input type="hidden" name ="package_size[]" value="<?php echo $value->package_size; ?>" />
                                        <input type="hidden" name ="package_weight[]" value="<?php echo $value->package_weight; ?>" />

                                        <?php echo $key + 1; ?>

                                    </td>
                                    <td><?php echo $value->order_id; ?></td>
                                    <td><?php echo $value->package_title; ?></td>
                                    <td><?php echo $value->city_name_origin.' - '. $value->origin_branch_name; ?></td>
                                    <td><?php echo $value->city_name_destination.' - '. $value->destination_branch_name; ?></td>
                                    <td><?php echo $value->package_size; ?> m3</td>
                                    <td><?php echo $value->package_weight; ?> kg</td>
                                    <td>
                                        <?php
                                        echo form_dropdown('armada_id[]', get_armadas($value->origin_branch_id, $value->destination_branch_id), '', 'class="form-control"');
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <button type="submit" id="" class="btn btn-primary">Proses Manifest</button>
            </div>

            <?php echo form_close(); ?>

        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>