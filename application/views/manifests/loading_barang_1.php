<?php $this->load->view('header');?>

	<div class="row">
		<div class="col-sm-12">
			<div class="box">
				<div class="box-body">
					<input type="text" class="form-control input-lg" name="package_id" id="package_id">
				</div>
			</div>

			<div class="box">
				<div class="box-body" id="load_result">

				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('footer');?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#package_id').keypress(function(event)
		{
			if ( event.which == 13 ) {
				p_id=$(this).val();
				$.post('<?php echo site_url('packages/packages/process_loadup');?>',
				{
					package_id:p_id
				},
				function(data)
				{
					$("#load_result").html(data);
					$('#package_id').val('');
				}
				);     
			}
		});
	})
</script>