<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<style type="text/css">
    .unloadtype-class{
        margin-top: -23px;
    }

    .txt-sm{
        font-size: 14px;
    }

</style>

<?php $this->load->view('header'); ?>

<div class="row">

    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Pilih Armada</h3>
            </div>
            <div class="box-body">

                <div class="row">

                    <div class="col-md-12">
                        <div class="alert alert-danger armada-selection" role="alert"> 
                            <strong><i class="fa fa-warning"></i> Peringatan!</strong> Belum ada armada yang dipilih
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group armadas-select">
                            <?php
                            echo form_dropdown('armada_id', get_armada_norestict(), '', 'class="form-control select2"');

                            ?>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h4>Scan Per Paket <span class="unloading-process"></span></h4>
                        <input type="text" class="form-control input-lg is-correct-armada" name="package_id" id="package_id">
                    </div>
                    <div class="col-md-6">
                        <h4>Scan Per Resi <span class="unloading-process-resi"></span></h4>
                        <input type="text" class="form-control input-lg is-correct-armada" name="order_id" id="order_id">
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="error-alert">

                        </div>
                        <form class="form-unloading">
                            <table class="table table-bordered table-striped txt-sm">
                                <thead>
                                    <tr>
                                        <th colspan="10">
                                            Daftar Paket Siap Diturunkan
                                        </th>
                                    </tr>
                                    <tr>
                                        <th width="10%" >ID Paket / Order</th>
                                        <th width="20%" >Nama Paket / Pengirim</th>
                                        <th colspan="2" width="35%" style="text-align:center">Keterangan</th>
                                        <th width="10%" >Asal-Tujuan</th>  
                                        <th width="10%" >Jumlah Koli</th>  
                                        <th width="10%" >Jumlah Volume</th>
                                        <th width="10%" >Jumlah Berat</th>
                                        <th width="3%" >Transit</th>
                                        <th width="5%" ></th>
                                    </tr>
                                </thead>
                                <tbody id="load_result" >

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-danger btn-flat btn-delete-all"><i class="fa fa-times"></i> Hapus Semua</button>
                    <button type="button" class="btn btn-info btn-flat btn-unloading"><i class="fa fa-download"></i> Turunkan</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        /** Checkbox styling */
        $('input[name="unloading_type"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            increaseArea: '30%'
        });

        /** Select 2 */
        $(".select2").select2();

        $('.armada-selection').hide();
        $('#package_id').on('keyup', function (event) {
            if (event.which != 13) {
                $('.armada-selection').hide();
            }
        });

        $('input[name="order_id"]').on('keyup', function (event) {
            if (event.which != 13) {
                $('.armada-selection').hide();
            }
        });

        /** Loaded by package id  */
        $('#package_id').keypress(function (event) {
            if (event.which == 13) {

                var p_id = $(this).val();
                var trips_id = $('select[name="armada_id"]').val();
                var unloading_type = $('input[name="unloading_type"]').prop('checked');

                $('.unloading-process').append('<span class="fa fa-spinner fa-spin"></span>');

                if (trips_id != 0) {

                    $.post('<?php echo site_url('packages/packages/unload_process_perpackage'); ?>',
                            {package_id: p_id, trips_id: trips_id, unloading_type: unloading_type}, function (data) {

                        if (data === 'error_00' || data === 'error_01' || data === 'error_02') {

                            if (data === 'error_01') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Paket dengan kode <strong>' + p_id + '</strong> sudah masuk ke dalam daftar unloading'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            if (data === 'error_02') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Paket dengan kode <strong>' + p_id + '</strong> tidak ditemukan di dalam gudang'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            $(".error-alert").html(str);

                            setTimeout(function () {
                                $('.not-found').fadeOut('slow');
                            }, 2000);

                        } else {
                            $("#load_result").append(data);
                            $('#package_id').val('');
                        }

                        $('.unloading-process').empty();

                    });

                } else {

                    $('.armada-selection').show();
                }

                $('input[name="package_id"]').focus();
            }
        });

        /** Loading by resi number */
        $('input[name="order_id"]').keypress(function (event) {
            if (event.which == 13) {

                order_id = $(this).val();
                trips_id = $('select[name="armada_id"]').val();
                unloading_type = $('input[name="unloading_type"]').prop('checked');

                $('.unloading-process-resi').append('<span class="fa fa-spinner fa-spin"></span>');

                if (trips_id != 0) {

                    $.post('<?php echo site_url('packages/packages/unload_process_perorder'); ?>',
                            {order_id: order_id, trips_id: trips_id, unloading_type: unloading_type}, function (data) {


                        if (data === 'false' || data === 'null') {

                            if (data === 'false') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Paket dengan kode <strong>' + order_id + '</strong> tidak ditemukan'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            if (data === 'null') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Paket dengan kode <strong>' + order_id + '</strong> sudah ada dalam daftar unloading'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            $(".error-alert").html(str);

                            setTimeout(function () {
                                $('.not-found').fadeOut('slow');
                            }, 2000);

                        } else {

                            $("#load_result").append(data);
                            $('input[name="order_id"]').val('');
                            $('.armada-correction-false').hide();
                            $('.armada-correction-true').hide();
                            $('.armada-selection').hide();
                            $('input[name="order_id"]').focus();

                        }

                        $('.unloading-process-resi').empty();

                    });

                } else {
                    $('.armada-selection').show();
                }
            }
        });

    });

    /** final process loading */
    $('.btn-unloading').click(function () {

        var data = $('form[class="form-unloading"]').serializeArray();
        var trip = $('select[name="armada_id"]').val();
        var unloading_type = $('input[name="unloading_type"]').prop('checked');

        $('.btn-unloading').empty();
        $('.btn-unloading').append('<span class="fa fa-spinner fa-spin"></span> Dalam Proses');
        $('.btn-unloading').addClass('disabled');

        $.post('<?php echo site_url('packages/packages/final_unloading_process'); ?>',
                {data: data, trip: trip, unloading_type: unloading_type}, function (data) {

            if (data === 'true') {

                $('td.actions').empty();
                $('td.actions').append('<span class="label label-success pull-right"><i class="fa fa-check green"></i> Berhasil</span>');

            } else {

                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Tidak ada data yang dimasukkan'
                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                        + '</div>';

                $(".error-alert").html(str);

                setTimeout(function () {
                    $('.not-found').fadeOut('slow');
                }, 2000);

            }

            $('.btn-unloading').empty();
            $('.btn-unloading').append('<span class="fa fa-download"></span> Turunkan');
            $('.btn-unloading').removeClass('disabled');
            $('.armada-alert').hide();


        });
    });

    /** Remove package */
    $('body').on('click', '.btn-delete', function () {

        // Store list will removed into html variable 
        var html = $(this).parent().parent().parent();

        // Get data value that will deleted
        var order_session = $(this).parent().parent().find('input[name="order_session"]').val();
        var package_session = $(this).parent().parent().find('input[name="package_session"]').val();

        // Send delete action in server side
        $.post('<?php echo site_url($path . '/unloading_unset'); ?>', {order_session: order_session, package_session: package_session}, function (data) {

            // Remove list that appear in browser
            $(html).remove();

        });

    });

    /** Remove all package */
    $('.btn-delete-all').click(function () {
        $.get('<?php echo site_url($path . '/unloading_unset_all'); ?>', function (data) {
            $("#load_result").empty();
        });
    });

    // Transit action
    $('body').on('click', '.is_transit', function () {
        var name = $(this).attr('name');
        var res = name.replace("transit_", "");
        if ($(this).prop("checked") == true) {
            $(this).parent().find('input[name="transit"]').val('{"id":"' + res + '", "value":"true"}');
        } else {
            $(this).parent().find('input[name="transit"]').val('{"id":"' + res + '", "value":"false"}');
        }
    });

</script>