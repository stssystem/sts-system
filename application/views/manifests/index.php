<?php $this->load->view('header'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
            Daftar Paket Terakhir di generate pada <?php echo $last_generate; ?>, pastikan semua paket sudah siap sebelum melakukan pre-manifest otomatis. Daftar Paket Generator akan mengirimkan notifikasi ke semua cabang apabila telah dilakukan generate
        </div>
        <?php echo get_alert(); ?>
        <div class="box">
            <div class="box-header">
                <h4 class="box-title">Berikut adalah daftar paket yang akan dilakukan pengelompokan otomatis : </h4>
                <a href="<?php echo site_url('manifests/manifests/generate_result'); ?>" class="btn btn-primary pull-right">Buat Daftar</a>
            </div>
            <div class="box-body">
                <table class="table">
                    <thead>
                        <tr><th>No.</th><th>Resi</th><th>Nama Paket</th><th>Asal</th><th>Tujuan</th><th>Volume</th><th>Berat</th></tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($data)): ?>
                            <?php foreach ($data as $key => $value) : ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $value->order_id; ?></td>
                                    <td><?php echo $value->package_title; ?></td>
                                    <td><?php echo $value->city_name_origin; ?></td>
                                    <td><?php echo $value->city_name_destination; ?></td>
                                    <td><?php echo $value->package_size; ?> m3</td>
                                    <td><?php echo $value->package_weight; ?> kg</td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>