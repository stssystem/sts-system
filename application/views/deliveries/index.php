<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<style type="text/css">
    .txt-sm{
        font-size: 14px;
    }
</style>

<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div>
            <div class="box-body">

                <div class="row">

                    <div class="col-md-4" >
                        <div class="form-group armadas-select">
                            <label>Armada</label>
                            <?php
                            echo form_dropdown('delivery_armada_id', get_armada_norestict(), '', 'class="form-control select2"');

                            ?>
                            <span class="help-block" style="display: none;" >Armada harus di pilih terlebih dahulu</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group armadas-select">
                            <label>Kurir</label>
                            <?php
                            echo form_dropdown('delivery_courier', get_all_couriers(), '', 'class="form-control select2"');

                            ?>
                            <span class="help-block" style="display: none;">Kurir harus di pilih terlebih dahulu</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Tanggal dan Jam Berangkat</label>
                            <input class="form-control" type="text" name="delivery_depart_time" />
                            <span class="help-block" style="display: none;" >Tanggal dan jam berangkat harus di isi terlebih dahulu</span>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h4>Scan Per Paket <span class="loading-process"></span></h4>
                        <input type="text" class="form-control input-lg is-correct-armada" name="package_id" id="package_id">
                    </div>
                    <div class="col-md-6">
                        <h4>Scan Per No Resi <span class="loading-process"></span></h4>
                        <input type="text" class="form-control input-lg is-correct-armada" name="order_id" id="order_id">
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="error-alert">

                        </div>
                        <form class="form-delivery">
                            <table class="table table-bordered table-striped txt-sm">
                                <thead>
                                    <tr>
                                        <th colspan="8">
                                            Daftar Paket Siap Diantarkan
                                        </th>
                                    </tr>
                                    <tr>
                                        <th width="10%" >ID Paket / Order</th>
                                        <th width="20%" >Nama Paket / Pengirim</th>                                        
                                        <th width="15%" >Armada</th>
                                        <th width="15%" >Supir</th>
                                        <th width="15%" >Waktu Berangkat</th>
                                        <th width="20%" colspan="2">Keterangan</th>
                                        <th width="5%" ></th>
                                    </tr>
                                </thead>
                                <tbody id="load_result" >

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-danger btn-flat btn-delete-all"><i class="fa fa-times"></i> Hapus Semua</button>
                    <button type="button" class="btn btn-info btn-flat btn-delivery"><i class="fa fa-truck"></i> Antar</button>
                </div>
            </div>
        </div>
    </div>

</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<!-- Boostraps Datepicker -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/local/id.js"></script>

<script type="text/javascript">

    /** Delivery depart time  */
    $('input[name="delivery_depart_time"]').datetimepicker({
        locale: 'id'
    });

    /** Select 2 */
    $(".select2").select2();

    $(document).ready(function () {

        /** Loaded by package id  */
        $('#package_id').keypress(function (event) {
            if (event.which == 13) {

                if (has_error()) {

                    /** Preparing value */
                    var package_id = $(this).val();
                    var delivery_armada_id = $('select[name="delivery_armada_id"]').val();
                    var delivery_armada_text = $('select[name="delivery_armada_id"] option:selected').text();
                    var delivery_courier = $('select[name="delivery_courier"]').val();
                    var delivery_courier_text = $('select[name="delivery_courier"] option:selected').text();
                    var delivery_depart_time = $('input[name="delivery_depart_time"]').val();

                    /** Show loading spinner */
                    $('.loading-process').append('<span class="fa fa-spinner fa-spin"></span>');

                    $.post('<?php echo site_url('packages/deliveries/store'); ?>',
                            {
                                delivery_armada_id: delivery_armada_id,
                                delivery_armada_text: delivery_armada_text,
                                delivery_courier: delivery_courier,
                                delivery_courier_text: delivery_courier_text,
                                delivery_depart_time: delivery_depart_time,
                                package_id: $(this).val()
                            },
                    function (data) {

                        if (data === 'error_01' || data === 'error_02') {

                            if (data === 'error_01') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Paket dengan ID <strong>' + package_id + '</strong> tidak ditemukan'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            if (data === 'error_02') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Paket dengan ID <strong>' + package_id + '</strong> sudah masuk ke dalam daftar pengantaran'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            $(".error-alert").html(str);

                            setTimeout(function () {
                                $('.not-found').fadeOut('slow');
                            }, 2000);

                        } else {

                            $("#load_result").append(data);
                            $('#package_id').val('');

                        }

                        /** Hide spinner loading process */
                        $('.loading-process').empty();

                    });
                }

                $('input[name="package_id"]').focus();

            }
        });

        /** Loading by resi number */
        $('input[name="order_id"]').keypress(function (event) {

            if (event.which == 13) {

                /** Preparing value */
                var order_id = $(this).val();
                var delivery_armada_id = $('select[name="delivery_armada_id"]').val();
                var delivery_armada_text = $('select[name="delivery_armada_id"] option:selected').text();
                var delivery_courier = $('select[name="delivery_courier"]').val();
                var delivery_courier_text = $('select[name="delivery_courier"] option:selected').text();
                var delivery_depart_time = $('input[name="delivery_depart_time"]').val();

                if (has_error()) {

                    /** Show loading spinner */
                    $('.loading-process').append('<span class="fa fa-spinner fa-spin"></span>');

                    $.post('<?php echo site_url('packages/deliveries/stores'); ?>',
                            {
                                delivery_armada_id: delivery_armada_id,
                                delivery_armada_text: delivery_armada_text,
                                delivery_courier: delivery_courier,
                                delivery_courier_text: delivery_courier_text,
                                delivery_depart_time: delivery_depart_time,
                                order_id: $(this).val()
                            },
                    function (data) {

                        if (data === 'error_01' || data === 'error_02') {

                            if (data === 'error_01') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Nomor Resi <strong>' + order_id + '</strong> tidak ditemukan'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            if (data === 'error_02') {
                                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Nomor Resi <strong>' + order_id + '</strong> sudah masuk ke dalam daftar pengantaran'
                                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                                        + '</div>';
                            }

                            $(".error-alert").html(str);

                            setTimeout(function () {
                                $('.not-found').fadeOut('slow');
                            }, 2000);

                        } else {
                            $("#load_result").append(data);
                            $('input[name="order_id"]').val('');
                        }

                        /** Hide spinner loading process */
                        $('.loading-process').empty();

                    });

                }

                $('input[name="order_id"]').focus();

            }
        });

    });

    $('.btn-delivery').click(function () {

        /** Get data serializing */
        var data_package = $('input[name="package_id[]"]').serializeArray();
        var data_armada = $('input[name="armada_id[]"]').serializeArray();
        var data_courier = $('input[name="courier_id[]"]').serializeArray();
        var data_depart_time = $('input[name="depart_time[]"]').serializeArray();

        /** Show loading spinner at delivery button */
        $('.btn-delivery').empty();
        $('.btn-delivery').append('<span class="fa fa-spinner fa-spin"></span> Dalam Proses');
        $('.btn-delivery').addClass('disabled');

        $.post('<?php echo site_url('packages/deliveries/final_store'); ?>',
                {data_package: data_package, data_armada: data_armada, data_courier: data_courier, data_depart_time: data_depart_time},
        function (data) {

            if (data === 'true') {

                $('td.actions').empty();
                $('td.actions').append('<span class="label label-success pull-right"><i class="fa fa-check green"></i> Berhasil</span>');

            } else {

                var str = '<div class="alert alert-danger not-found" role="alert" style="display: block;"> '
                        + '<strong><i class="fa fa-warning"></i> Peringatan !</strong> Tidak ada data yang dimasukkan'
                        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                        + '</div>';

                $(".error-alert").html(str);

                setTimeout(function () {
                    $('.not-found').fadeOut('slow');
                }, 2000);

            }

            $('.btn-delivery').empty();
            $('.btn-delivery').append('<span class="fa fa-truck"></span> Antar');
            $('.btn-delivery').removeClass('disabled');

        });



    });

    /** Remove all package */
    $('.btn-delete-all').click(function () {
        $("#load_result").empty();
    });

    /** Remove package */
    $('body').on('click', '.btn-delete', function () {
        
        // Variable initialization
        var html = $(this).parent().parent().parent();
        var order_session = $(this).parent().parent().find('input[name="order_session"]').val();
        var package_session = $(this).parent().parent().find('input[name="package_session"]').val();
        
        alert(order_session);
        alert(package_session);
        
        // Pass to server
//        $.post('<?php echo site_url($path . '/destroy'); ?>', {order_session: order_session, package_session: package_session}, function (data) {
//            $(html).remove();
//        });
        
    });

    function has_error() {

        var delivery_armada_id = $('select[name="delivery_armada_id"]').parent().addClass('has-error');
        var delivery_courier = $('select[name="delivery_courier"]').parent().addClass('has-error');
        var delivery_depart_time = $('input[name="delivery_depart_time"]').parent().addClass('has-error');
        var has_error = true;

        if ($('select[name="delivery_armada_id"]').val() == 0) {
            $(delivery_armada_id).addClass('has-error');
            $(delivery_armada_id).find('.help-block').show();
            var has_error = false;
        } else {
            $(delivery_armada_id).removeClass('has-error');
            $(delivery_armada_id).find('.help-block').hide();
        }

        if ($('select[name="delivery_courier"]').val() == 0) {
            $(delivery_courier).addClass('has-error');
            $(delivery_courier).find('.help-block').show();
            var has_error = false;
        } else {
            $(delivery_courier).removeClass('has-error');
            $(delivery_courier).find('.help-block').hide();
        }

        if ($('input[name="delivery_depart_time"]').val() == '') {
            $(delivery_depart_time).addClass('has-error');
            $(delivery_depart_time).find('.help-block').show();
            var has_error = false;
        } else {
            $(delivery_depart_time).removeClass('has-error');
            $(delivery_depart_time).find('.help-block').hide();
        }

        return has_error;

    }

</script>