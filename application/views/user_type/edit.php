<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open($path . '/update', ['class' => 'form-edit']); ?>

                <input type="hidden" name="user_type_id" value="<?php echo $data->user_type_id ?>" />

                <?php
                echo $this->form->text(
                        [
                            'name' => 'user_type_name',
                            'label' => 'Tipe User',
                            'max' => 30,
                            'required' => true,
                            'value' => $data->user_type_name
                        ]
                );
                ?>

                <div class="row">
                    <?php
                    echo $this->form->select(
                            [
                                'name' => 'user_type_access[]',
                                'label' => 'Akses Menu',
                                'max' => 30,
                                'value' => menu_access(),
                                'keys' => 'id',
                                'values' => 'value',
                                'required' => true,
                                'multiple' => true,
                                'class' => 'select2 col-md-9',
                                'selected' => json_decode($data->user_type_access, true)
                            ]
                    );
                    ?>

                    <?php
                    echo $this->form->text(
                            [
                                'name' => 'branch_access',
                                'label' => 'Akses Cabang',
                                'max' => 30,
                                'required' => true,
                                'type' => 'checkbox',
                                'value' => [1 => 'Tidak Dibatasi'],
                                'class' => 'col-md-3',
                                'checked' => [$data->branch_access]
                            ]
                    );
                    ?>
                </div>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url($path); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <a type="button" class="btn-info btn btn-save" data-toggle="modal" data-target="#modal-save-alert">
                            <i class="fa fa-save"></i>
                            Simpan
                        </a>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">

    /** Select 2 */
    $(".select2").find('select').select2();

    $('.btn-save').click(function () {

        var user_type_name = $('.form-edit').find('input[name="user_type_name"]').val();

        if (user_type_name == '') {

            /** Set message */
            var text = "User Tipe harus diisi";

        } else {

            /** Set message */
            var text = "Apakah Anda akan menyimpan tipe user <strong>" + user_type_name + "</strong>?";

        }

        /** Set modal */
        $('#modal-save-alert').find('.modal-body p').html(text);

    });

    $('.alert-btn-save').click(function () {
        $('.form-edit').submit();
    });

</script>