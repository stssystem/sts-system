<?php $this->load->view('header');?>

<div class="row">
	<div class="col-sm-12">
		<div class="box">
			<div class="box-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Kode</th><th>Pengirim<th>Alamat Pengirim</th><th>Asal Paket</th><th>Tujuan</th><th>Jumlah Paket</th><th>Total Harga Kalkulator</th><th>Waktu Pengambilan</th><th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($query->result() as $rows): ?>
							<tr>
								<td><?php echo $rows->no_resi;?></td>
								<td>
									<strong><?php echo $rows->nama_pengirim;?></strong><br />
									<?php echo $rows->perusahaan;?><br />
									<?php echo $rows->email_pengirim;?><br />
									<?php echo $rows->telp_pengirim;?><br />
								</td>
								
								<td><?php echo $rows->alamat_pengirim;?>, <br /><?php echo $rows->kelurahan_pengirim;?> <?php echo $rows->kecamatan_pengirim;?><br />
									<?php echo $rows->kabupaten_pengirim;?></td>
								<td><?php echo $rows->asal;?></td>
								<td><?php echo $rows->tujuan;?></td>
								<td><?php echo $rows->jumlah_paket;?></td>
								<td><?php echo number_format($rows->total_harga,0,',','.');?></td>
								<td><?php echo $rows->waktu_pengambilan_tanggal.' '.$rows->waktu_pengambilan_jam;?></td>
								<td></td>
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('footer');?>