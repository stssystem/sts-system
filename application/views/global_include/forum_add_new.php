<?php $this->load->view('header');?>

	<div class="row">
		<div class="col-sm-12">
			<div class="box">
				<div class="box-header">Buat Diskusi Baru</div>
				<div class="box-body">
					<?php echo form_open('program/forum_save');?>
						<div class="form-group">
							<label for="message_title">Judul</label>
							<input type="text" name="message_title" id="message_title" class="form-control" />
						</div>
						<div class="form-group">
							<label for="message_content">Isi</label>
							<textarea name="message_content" id="message_content" class="form-control" rows="10"></textarea>
						</div>
						<input type="hidden" name="message_parent" value="<?php echo $message_parent;?>">
						<input type="submit" value="Simpan" class="btn btn-primary">
					<?php echo form_close();?>
				</div>
			</div>

		</div>
	</div>
<?php $this->load->view('footer');?>