<?php $this->load->view('header');?>
<div class="row">
	<div class="col-sm-12">
		<div class="box">
			<table class="table">
				<thead>
					<th>Pengirim</th><th>Pesan</th>
				</thead>
				<tbody>
					<tr>
						<td>
							<strong><?php echo $row->username;?></strong><br />
							<span class="small"><?php echo mdate('%d/%m/%Y %H:%i:%s',$row->created_at);?></span>
						</td>
						<td>
							<strong><?php echo $row->message_title;?></strong><br />
							<p>
								<?php echo $row->message_content;?>
							</p>
						</td>
					</tr>
					<?php foreach($query->result() as $rows): ?>
						<tr>
							<td>
								<strong><?php echo $rows->username;?></strong><br />
								<span class="small"><?php echo mdate('%d/%m/%Y %H:%i:%s',$rows->created_at);?></span>
							</td>
							<td>
								<strong><?php echo $rows->message_title;?></strong><br />
								<p>
									<?php echo $rows->message_content;?>
								</p>
							</td>
						</tr>
					<?php endforeach;?>
					<tr><td colspan="2" class="text-right">
						<a href="<?php echo site_url('program/forum_add_new/'.$row->message_id);?>" class="btn btn-primary">Balas</a>
						<a href="<?php echo site_url('program/forum_close/'.$row->message_id);?>" class="btn btn-success">Tutup Diskusi</a>
					</td></tr>
				</tbody>
			</table>

		</div>
	</div>
</div>

<?php $this->load->view('footer');?>