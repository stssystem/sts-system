<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<?php $this->load->view('header');?>
	<div class="row">
		<div class="col-sm-12">
			<div class="box">
				<div class="box-header">
					<div class="row">
	                    <div class="col-md-12">
	                        <h4><?php echo $title; ?></h4>
	                    </div>
	                    <div class="col-md-12">
	                        <div class="box-tools pull-right">
	                            <a href="<?php echo site_url('program/forum_add_new') ?>" class="btn btn-success">
	                                <i class="fa fa-plus"></i> Buat Diskusi Baru
	                            </a>
	                        </div>
	                    </div>
	                </div>
				</div>
				<div class="box-body">
					<table class="table table-striped" id="ut-table">
						<thead>
							<tr>
								<th>Tanggal</th><th>Judul</th><th>Pengirim</th>
							</tr>

						</thead>
						<tbody>
							<?php foreach($query->result() as $rows): ?>
								<tr>
									<td><?php echo mdate('%d/%m/%Y %H:%i:%s',$rows->created_at);?></td>
									<td><a href="<?php echo site_url('program/forum_view/'.$rows->message_id);?>"><?php echo $rows->message_title;?></a></td>
									<td><?php echo $rows->username;?></td>
								</tr>
							<?php endforeach;?>		
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</div>

<?php $this->load->view('footer');?>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#ut-table').DataTable();
	});
</script>