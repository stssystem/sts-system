<!-- Select 2 -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">

<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8">
                        <h4><?php echo $title; ?></h4>
                    </div>
                    <div class="col-md-4" >
                        <div class="btn-group pull-right" >
                            <a href="<?php echo site_url($path . '/add') ?>" class="btn btn-info">
                                <i class="fa fa-pencil"></i> Booking Order
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive ">

                <?php get_alert(); ?>

                <div class="row">
                    <div class="col-sm-12">
                        <table class="table data-tables txt-sm">
                            <thead>
                                <tr>
                                    <th width="15%">No. Resi</th>
                                    <th width="15%">No. Resi Manual</th>
                                    <th width="15%">Pengirim</th>
                                    <th width="25%">Asal - Tujuan</th>
                                    <th width="7%">Jumlah Paket</th>
                                    <th width="10%">Tgl. Order</th>
                                    <th width="13%">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>	
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript" >
    $("select[name='branch_origin']").select2();
    $("select[name='branch_destination']").select2();

    $('.data-tables').DataTable({
        serverSide: true,
        ordering: true,
        order: [
            [
<?php dtorder_column('dt_booking', 0); ?>,
                "<?php dtorder_mode('dt_booking', 'asc'); ?>"
            ]
        ],
        displayStart: <?php dtarray('dt_booking', 'start', 0); ?>,
        pageLength: <?php dtarray('dt_booking', 'lenght', 10); ?>,
        search: {
            search: "<?php dtarray('dt_booking', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>',
            type: 'POST'
        },
        columns: [
            {data: 'order_id'},
            {data: 'order_manual_id'},
            {data: 'customer'},
            {data: 'origin_destination'},
            {data: 'package_count'},
            {data: 'created_at'},
            {data: 'action'}
        ],
        columnDefs: [
            {targets: [3], orderable: false},
        ],
    });

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus booking order dengan no resi <strong>" + data['order_id'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);
        
        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['order_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

</script>