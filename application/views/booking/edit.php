<!-- Select 2 -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/easy-autocomplete/easy-autocomplete.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<?php
if (profile()->branch_access == '' && profile()->regional_id != '') {
    $cities = get_branches_list(null, profile()->regional_id);
} else {
    $cities = get_branches_list(null);
}
?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8">
                        <h4><?php echo $title; ?></h4>
                    </div>
                    <div class="col-md-4">
                        <div class="pull-right btn-group">
                            <a href="<?php echo site_url($path); ?>" class="btn btn-info">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php if (error('order_manual_temp')): ?>
                    <div class="alert alert-danger alert-dismissible" role="alert"> 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button> 
                        <strong>Peringatan!</strong> Nomor resi manual <?php echo old_input('branch_code') . old_input('resi_manual'); ?> sudah digunakan.
                    </div>
                <?php endif; ?>

                <?php echo form_open($path . '/update/' . $data['order_id']); ?>

                <div class="row">

                    <div class="col-md-6 form-group <?php echo error('branch_origin') ? 'has-error ' : ''; ?>" >
                        <label>Asal</label>
                        <select name="branch_origin" class="form-control select2" >
                            <option value="" >-- Pilih Cabang Asal --</option>
                            <?php if (!empty($cities)): ?>
                                <?php foreach ($cities as $key => $item): ?>
                                    <option
                                        value="<?php echo $item['branch_id']; ?>"
                                        <?php echo (old_input('branch_origin') == $item['branch_id'] || branch()['branch_id'] == $item['branch_id'] || $data['order_origin'] == $item['branch_id'] ) ? 'selected="selected"' : ''; ?> >
                                            <?php echo $item['branch_name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <span class="help-block"><?php echo error('branch_origin'); ?></span>
                    </div>

                    <div class="col-md-6 form-group <?php echo error('branch_destination') ? 'has-error ' : ''; ?>">
                        <label>Tujuan</label>
                        <select name="branch_destination" class="form-control select2" >
                            <option value="" >-- Pilih Cabang Tujuan --</option>
                            <?php if (!empty($cities)): ?>
                                <?php foreach ($cities as $key => $item): ?>
                                    <option
                                        value="<?php echo $item['branch_id']; ?>"
                                        <?php echo (old_input('branch_destination') == $item['branch_id'] || branch()['branch_id'] == $item['branch_id'] || $data['order_destination'] == $item['branch_id'] ) ? 'selected="selected"' : ''; ?> >
                                            <?php echo $item['branch_name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <span class="help-block"><?php echo error('branch_destination'); ?></span>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-2 form-group <?php echo error('branch_code') ? 'has-error ' : ''; ?>" >
                        <label>Kode Cabang</label>                        
                        <input
                            type="text" 
                            style="border-radius: 0;"  
                            name="branch_code" 
                            class="form-control"
                            <?php if (branch()['branch_code'] == ''): ?>
                                value="<?php echo (!old_input('branch_code')) ? $data['order_branch_code'] : old_input('branch_code'); ?>" 
                            <?php else: ?>
                                value="<?php echo branch()['branch_code']; ?>"
                            <?php endif; ?>
                            <?php echo (branch()['branch_code'] != '') ? 'disabled="disabled"' : '' ?>
                            />
                        <span class="help-block"><?php echo error('branch_code'); ?></span>
                    </div>

                    <div class="col-md-4 form-group <?php echo error('resi_manual') ? 'has-error ' : ''; ?>">
                        <label>No Resi Manual</label>
                        <input type="text" name="resi_manual" class="form-control" value="<?php echo (!old_input('resi_manual')) ? $data['order_manual_resi_pure'] : old_input('resi_manual'); ?>"  />
                        <span class="help-block"><?php echo error('resi_manual'); ?></span>
                    </div>

                    <div class="col-md-6 form-group <?php echo error('customer_name') ? 'has-error ' : ''; ?>">
                        <label>Nama Pengirim</label>
                        <input type="text" name="customer_name" class="form-control"  style="border-radius: 0;" value="<?php echo (!old_input('customer_name')) ? $data['customer_name'] : old_input('customer_name'); ?>" />
                        <input type="hidden" name="customer_id" class="form-control" />
                        <span class="help-block"><?php echo error('customer_name'); ?></span>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-3 form-group <?php echo error('order_date') ? 'has-error ' : ''; ?>">
                        <label>Tanggal Order</label>
                        <input 
                            type="text" 
                            name="order_date" 
                            class="form-control" 
                            value="<?php echo (!old_input('order_date')) ? mdate('%d/%m/%Y', $data['order_date']) : old_input('order_date'); ?>" 
                            />
                        <span class="help-block"><?php echo error('order_date'); ?></span>
                    </div>

                    <div class="col-md-3 form-group <?php echo error('package_qty') ? 'has-error ' : ''; ?>">
                        <label>Jumlah Paket</label>
                        <input 
                            type="number" 
                            name="package_qty" 
                            class="form-control" 
                            value="<?php echo (!old_input('package_qty')) ? $data['package_qty'] : old_input('package_qty'); ?>" 
                            />
                        <span class="help-block"><?php echo error('package_qty'); ?></span>
                    </div>

                    <div class="col-md-6 form-group">      
                        <button type="submit" class="btn btn-warning btn-block" style="margin-top: 24px;">
                            <i class="fa fa-key"></i>
                            Booking
                        </button>
                    </div>

                </div>

                <div class="row">

                </div>
                <?php echo form_close(); ?>

            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Easy Autocomplete -->
<script src="<?php echo base_url(); ?>assets/plugins/easy-autocomplete/jquery.easy-autocomplete.min.js"></script>

<script type="text/javascript" >

    $("select[name='branch_origin']").select2();
    $("select[name='branch_destination']").select2();

    $('input[name="order_date"]').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-31d',
        endDate: '0',
        todayBtn: "linked",
        autoclose: true,
        todayHighlight: true
    });

    // Javascript function calling
    autocomplete();
    auto_branch_code();

    function autocomplete() {
        /** Autocomplete */
        var options = {
            url: function (phrase) {
                return "<?php echo site_url('request/customer_helper/get_like?phrase='); ?>" + phrase;
            },
            getValue: "customer_name",
            theme: "square",
            requestDelay: 250,
            template: {
                type: "description",
                fields: {
                    description: "customer_code"
                }
            },
            list: {
                match: {
                    enabled: true
                },
                onChooseEvent: function () {
                    var customer_id = $("input[name='customer_name']").getSelectedItemData().customer_id;
                    var customer_birth_date = $("input[name='customer_name']").getSelectedItemData().customer_birth_date;
                    $.post('<?php echo site_url('request/customer_helper/get_detail'); ?>/' + customer_id,
                            function (data) {
                                var result = JSON.parse(data);
                                if (customer_id != '') {
                                    $('input[name="customer_id"]').val(customer_id);
                                }
                            }
                    );

                }
            }
        };

        $("input[name='customer_name']").easyAutocomplete(options);

        $('input[name="customer_name"]').on('keyup', function (event) {
            if (event.which != 13) {
                $('input[name="customer_id"]').val('');
            }
        });

    }

    /** Branch autocomplete */
    function auto_branch_code() {
        var branch_code = {
            url: "<?php echo site_url('request/locations/get_branches'); ?>",
            getValue: "branch_name_code",
            theme: "square",
            list: {
                match: {
                    enabled: true
                },
                onChooseEvent: function () {
                    var branch_code_ = $("input[name='branch_code']").getSelectedItemData().branch_code;
                    $("input[name='branch_code']").val(branch_code_);
                },
                onLoadEvent: function () {
                    $('.easy-autocomplete-container').attr('style', 'width:200px;');
                }
            }
        };

        $("input[name='branch_code']").easyAutocomplete(branch_code);
    }

</script>