    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

    <style>
    .view-detail {
        padding: 50px;
        display: none;
    }
    </style>

    <?php $this->load->view('header'); ?>
    <?php $this->load->view('component/modal/alert-delete'); ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <div class="row">

                        <?php echo form_open($path . '/search'); ?>
                        <div class="form-group col-md-4">
                            <input class="form-control" type="text" name="order_id" placeholder='Masukkan Nomor Resi' />
                        </div>
                        <div class="form-group col-md-2">
                            <button class="btn btn-info">
                                <i class="fa fa-search" ></i>
                                Lacak
                            </button>
                        </div>
                        <?php echo form_close(); ?>

                    </div>
                    <?php get_alert(); ?>

                    <div class="row">

                        <?php if (!empty($search)) { ?>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <h3><strong>LACAK</strong></h3>
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="col-md-4">
                                <h4><strong>Lacak</strong> <?php echo (!empty($search->order_id)) ? $search->order_id : $search->order_manual_id ; ?></h4>
                            </div>
                            <div class="col-md-4">
                                <h4><strong>Status</strong> : <?php echo order_status($search->order_status); ?> </h4>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-info view"><i class="fa fa-search" ></i> Detail</button>
                            </div>
                        </div>

                        <div class="view-detail col-md-12">

                            <div class="col-md-6">
                                
                                <div class="panel panel-info">
                                    <div class="panel-heading"><strong>Status Terakhir</strong></div>
                                    <div class="panel-body">
                                    <h5><?php echo mdate('%d %M %Y pukul %H:%i', $last_date); ?></h5>
                                    <h5><strong>Kota Asal </strong>: <?php echo $search->city_name_origin; ?></h5>
                                    <h5><strong>Status</strong> : <?php echo order_status($search->order_status); ?> </h5>
                                    </div>
                                </div>

                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5><strong><?php echo $search->city_name_origin.' - '.$search->city_name_destination; ?></strong></h5>
                                    </div>
                                    <div class="panel-body">
                                        <h5><strong>Kota Asal </strong> : <?php echo $search->city_name_origin; ?></h5>
                                        <h5><strong>Kota Tujuan </strong> : <?php echo $search->city_name_destination; ?></h5>
                                        <h5><strong>Cabang Asal </strong> : <?php echo $search->branch_name_origin; ?></h5>
                                        <h5><strong>Cabang Tujuan </strong> : <?php echo $search->branch_name_destination; ?></h5>
                                    </div>
                                </div>

                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h5><strong>Data Detail Order</strong></h5>
                                    </div>
                                    <div class="panel-body">
                                        <h5><strong>Nama Pelanggan </strong> : <?php echo $search->customer_name; ?></h5>
                                        <h5><strong>Tanggal Order </strong> : <?php echo mdate('%d %M %Y', $search->order_date); ?></h5>
                                        <h5><strong>Tanggal Jatuh Tempo </strong> : <?php echo mdate('%d %M %Y', $search->orders_due_date); ?></h5>
                                        <h5><strong>Total </strong> : Rp. <?php echo number_format(round($search->order_total)); ?></h5>
                                        <h5><strong>WEB ID </strong> : <?php echo $search->order_id_web; ?></h5>
                                        <h5><strong>Jenis Pembayaran </strong> : <?php echo payment_methode($search->order_payment_type); ?></h5>
                                        <h5><strong>Nama Tujuan </strong> : <?php echo $search->destination_name; ?></h5>
                                        <h5><strong>Telp Tujuan </strong> : <?php echo $search->destination_telp; ?></h5>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <table class="table table-hover table-bordered table-responsive">
                                    <thead>
                                        <tr class="active">
                                            <th>Tanggal</th>
                                            <th>Lokasi</th>
                                        </tr>
                                    </thead>
                                    <?php 
                                    if (!empty($detail)) {
                                        foreach ($detail as $key => $value) {
                                    ?>
                                    <tbody class="info">
                                        <tr>
                                            <td> <?php echo mdate('%d %M %Y %h:%i', $value->created_at); ?> </td>
                                            <td> 
                                                <p><?php echo $value->city_name; ?></p>
                                                <p><?php echo $value->note; ?></p>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <?php 
                                    }
                                    }
                                    ?>
                                </table>
                            </div>

                        </div>

                        <?php } ?>   

                    </div>

                </div>

               <!--  <div class="box-body">
                    <div id="maps" style="width: 100%; height: 700px;">

                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <input type="hidden" name ="latlang" value="-7.477001,110.220713"/>
    <input type="hidden" name ="zoom"  value="5" />


    <?php $this->load->view('footer'); ?>

    <!-- Map api -->
    <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9UXmy13xdGxWqpY33ZrSCe4kT3iI3nMM&v=3&callback=initMap" async defer></script>

    <!-- Data Table -->
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

    <script>
    $(document).ready(function(){
        $(".view").click(function(){
            $(".view-detail").fadeToggle("slow");
        });
    });
    </script>

    <script type="text/javascript">

        $(".select2").find('select').select2();

        var map;
        var marker = [];
        var i;
        var locations = 0;

        var latlang = $('input[name="latlang"]').val();
        var res = latlang.split(",");

        var latitude = res[0];
        var longitude = res[1];
        var zoom = parseInt($('input[name="zoom"]').val());

        function initMap() {

            var myLatlng = new google.maps.LatLng(latitude, longitude);

            var myOptions = {
                zoom: zoom,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            var map = new google.maps.Map(document.getElementById("maps"), myOptions);
            var infowindow = new google.maps.InfoWindow({ 
                size: new google.maps.Size(200,200)
              });
            

        }


    </script>
