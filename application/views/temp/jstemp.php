<script src="<?php echo base_url(); ?>assets/js/application/orders/global_variable.js"></script>
<script src="<?php echo base_url(); ?>assets/js/application/orders/collect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/application/orders/validation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/application/orders/packages/totalize.js"></script>
<script src="<?php echo base_url(); ?>assets/js/application/orders/packages/request.js"></script>
<script src="<?php echo base_url(); ?>assets/js/application/orders/delivery/maps.js"></script>

<script type="text/javascript" >

    /** URL pass */
    var url_order_helper_price = '<?php echo site_url('request/order_helper/price'); ?>';
    var url_orders_store = '<?php echo site_url('orders/orders/store'); ?>';
    var url_destroy_order_extra = '<?php echo site_url('orders/orders/destroy_order_extra'); ?>';
    var url_geo_branch = '<?php echo site_url('request/locations/get_geobranch'); ?>';

    geo_graps();


    /** Select 2 */
    $(".select2").find('select').select2();

    /** Reactivate Select 2 */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(".select2").find('select').select2();
    });

    /** 
     * Origin Seciton 
     * ---------------------------------------------------------------------------------------------------------------------------------------------
     * */

    /** Auto select when user branch available */
    var branch_now = $('input[name="branch_now"]').val();
    var branch_json = 0;
    if (branch_now != 'null') {
        branch_json = JSON.parse(branch_now);
        if ($('input[name="old_city_id_origin"]').val() == '') {
            $('select[name="city_id_origin"]').val(branch_json['city_id']);
            get_origin_districts(branch_json['city_id']);
        } else {
            $('select[name="city_id_origin"]').val($('input[name="old_city_id_origin"]').val());
            get_origin_districts($('input[name="old_city_id_origin"]').val(), $('input[name="old_districts_id_origin"]').val());
        }
        get_origin_branch(branch_json['city_id'], 'true', branch_json['branch_id']);
        $('select[name="branch_id_origin"]').attr('disabled');
    }

    /** Repopulate origin */
    var old_city_id_origin = $('input[name="old_city_id_origin"]').val();
    if (old_city_id_origin != '') {
        get_origin_districts(old_city_id_origin, $('input[name="old_districts_id_origin"]').val());
        if (branch_now == 'null') {
            get_origin_branch(old_city_id_origin, 'false', $('input[name="old_order_origin"]').val());
        } else {
            get_origin_branch(branch_json['city_id'], 'true', $('input[name="old_order_origin"]').val());
        }
    }

    /** Select city */
    $('select[name="city_id_origin"]').on('change', function () {
        get_origin_districts($(this).val());
        if (branch_now == 'null') {
            get_origin_branch($(this).val(), 'false');
        }
        geo_graps();
    });

    /** Get origin districts */
    function get_origin_districts(city_id, selected) {

        var url = '<?php echo site_url('request/locations/districts/districts_id_origin'); ?>';

        $.post(url,
                {city_id: city_id},
        function (data) {
            var form = $('select[name="districts_id_origin"]').parent().parent();
            $(form).replaceWith(data);
            $(".select2").find('select').select2();
            geo_graps();
            bind_change_event();


            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="districts_id_origin"]').val(selected);
                /** Select village */
                get_origin_village(selected, $('input[name="old_village_id_origin"]').val());
            }

            /** Select village */
            $('body').on('change', 'select[name="districts_id_origin"]', function () {
                get_origin_village($(this).val(), $('input[name="old_village_id_origin"]').val());
            });

        });

    }

    /** Get origin village */
    function get_origin_village(districts_id, selected) {
        $.post('<?php echo site_url('request/locations/villages/village_id_origin'); ?>',
                {districts_id: districts_id},
        function (data) {
            var form = $('select[name="village_id_origin"]').parent().parent();
            $(form).replaceWith(data);
            $(".select2").find('select').select2();
            bind_change_event();
            geo_graps();


            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="village_id_origin"]').val(selected);
            }

        }
        );
    }

    /** Get origin branch */
    function get_origin_branch(city_id, disabled, selected) {

        var url = '';
        if (disabled == 'true') {
            url = '<?php echo site_url('request/locations/branches/branch_id_origin'); ?>/true';
        } else {
            url = '<?php echo site_url('request/locations/branches/branch_id_origin'); ?>';
        }

        $.post(url,
                {city_id: city_id},
        function (data) {
            var form = $('select[name="branch_id_origin"]').parent().parent();
            $(form).replaceWith(data);
            $(".select2").find('select').select2();
            bind_change_event();
            geo_graps();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="branch_id_origin"]').val(selected);
            }

        }
        );

    }

    /** 
     * Destination Seciton 
     * ---------------------------------------------------------------------------------------------------------------------------------------------
     * */

    /** Repopulate origin */
    var old_city_id_destination = $('select[name="city_id_destination"]').val();
    if (old_city_id_destination != '') {
        get_destination_districts(old_city_id_destination, $('input[name="old_districts_id_destination"]').val());
        get_destination_branch(old_city_id_destination, $('input[name="old_order_destination"]').val());
    }

    /** Select  */
    $('select[name="city_id_destination"]').on('change', function () {
        get_destination_districts($(this).val());
        get_destination_branch($(this).val());
    });

    function get_destination_districts(city_id, selected) {
        $.post('<?php echo site_url('request/locations/districts/districts_id_destination'); ?>',
                {city_id: city_id},
        function (data) {
            var form = $('select[name="districts_id_destination"]').parent().parent();
            $(form).replaceWith(data);
            $(".select2").find('select').select2();
            bind_change_event();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="districts_id_destination"]').val(selected);
                /** Select village */
                get_destination_village(selected, $('input[name="old_village_id_destination"]').val());
            }

            /** Select district */
            $('body').on('change', 'select[name="districts_id_destination"]', function () {
                get_destination_village($(this).val());
            });

        });

    }

    function get_destination_village(districts_id, selected) {

        $.post('<?php echo site_url('request/locations/villages/village_id_destination'); ?>',
                {districts_id: districts_id},
        function (data) {
            var form = $('select[name="village_id_destination"]').parent().parent();
            $(form).replaceWith(data);
            $(".select2").find('select').select2();
            bind_change_event();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="village_id_destination"]').val(selected);
            }

        }
        );

    }

    function get_destination_branch(city_id, selected) {

        $.post('<?php echo site_url('request/locations/branches/branch_id_destination'); ?>',
                {city_id: city_id},
        function (data) {
            var form = $('select[name="branch_id_destination"]').parent().parent();
            $(form).replaceWith(data);
            $(".select2").find('select').select2();
            bind_change_event();

            /** Repopulate */
            if (typeof (selected) !== 'undefined') {
                $('select[name="branch_id_destination"]').val(selected);
            }

        }
        );

    }

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus kota <strong>" + data['city_name'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);
        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['city_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    });


    function bind_change_event() {
        $('body').on('change', '.select-price-params', function () {
            setTimeout(function () {
                grap_price_params();
            }, 1000);
        });
    }

    function data_tables() {

        $(".data-tables").dataTable().fnDestroy();

        $('.data-tables').DataTable({
            "bDestroy": true,
            ajax: {
                url: '<?php echo site_url($path . '/index/' . $order_id) ?>',
                dataSrc: ''
            },
            columns: [
                {data: 'package_id'},
                {data: 'package_title'},
                {data: 'package_type'},
                {data: 'total'},
                {data: 'sell_total'},
                {data: 'action'}
            ]
        });

        setTimeout(function () {
            var total_orders = 0;
            $('input[name="total_orders"]').each(function () {
                total_orders += parseInt($(this).val());
            });

            var sell_total_order = 0;
            $('input[name="sell_total_orders"]').each(function () {
                sell_total_order += parseInt($(this).val());
            });

            $('.total_orders').empty();
            $('.total_orders').text(numeral(total_orders).format('0,0'));

            $('.total-all').empty();
            $('.total-all').text(numeral(total_orders).format('0,0'));

            $('.total-all-customer').empty();
            $('.total-all-customer').text(numeral(sell_total_order).format('0,0'));

            $('.sell_total_orders').empty();
            $('.sell_total_orders').text(numeral(sell_total_order).format('0,0'));

            /** Total Order Extra */
            var total_order_exra = 0;
            $('input[name="total_order_extra"]').each(function () {
                total_order_exra += parseInt($(this).val());
            });

            $('.total-order-extra').empty();
            $('.total-order-extra').text(numeral(total_order_exra).format('0,0'));

        }, 1000);

        $(".data-order-extra").dataTable().fnDestroy();

        $('.data-order-extra').DataTable({
            "bDestroy": true,
            ajax: {
                url: '<?php echo site_url($path . '/get_orders_extra/' . $order_id) ?>',
                dataSrc: ''
            },
            columns: [
                {data: 'det_order_extra_id'},
                {data: 'det_order_extra_name'},
                {data: 'det_order_extra_total'},
                {data: 'action'}
            ]
        });

    }

    $('.btn-submit').click(function () {
        form_submit();
    });

    $('.input-price-params').on('keyup', function () {
        grap_price_params();
    });

    /** Cancel confirmation */
    $('body').on('click', '.btn-cancel', function () {

        /** Set modal */
        var text = "Apakah Anda yakin akan membatalkan order?";
        $('#modal-delete-alert').find('.modal-body p').html(text);

        /** Set url */
        var url = "<?php echo site_url($path . '/cancel/' . $order_id) ?>";
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Edit package  */
    $('.data-tables').on('click', '.btn-edit', function () {

        var data = JSON.parse($(this).parent().parent().find('input').val());

        var button = '<button type="button" class="btn btn-warning btn-flat btn-submit btn-action btn-edit-package">'
                + '<i class="fa fa-save"></i> Perbarui Data</button>';

        $('.btn-action').replaceWith(button);

        $('input[name="package_id"]').val(data['package_id']);
        $('input[name="package_title"]').val(data['package_title']);
        $('textarea[name="package_content"]').val(data['package_content']);
        $('input[name="package_width"]').val(data['package_width']);
        $('input[name="package_height"]').val(data['package_height']);
        $('input[name="package_lenght"]').val(data['package_lenght']);
        $('input[name="package_weight"]').val(data['package_weight']);
        $('select[name="package_type"]').val(data['package_type']);

        setTimeout(function () {
            $('input[name="det_order_sell_total"]').val(data['sell_total']);
            $('input[name="det_order_sell_total"]').attr('name', 'det_order_sell_total_temp');
        }, 1000);


        /** Disable package quatity */
        $('input[name="det_order_qty"]').val(1);
        $('input[name="det_order_qty"]').attr('disabled', true);


        grap_price_params();

    });

    $('body').on('click', '.btn-edit-package', function () {

        var button = '<button type="button" class="btn btn-info btn-flat btn-submit btn-action btn-add-package">'
                + '<i class="fa fa-save"></i>Tambahkan dan Simpan</button>';

        $('.btn-action').replaceWith(button);

        form_update();

        $('input[name="package_id"]').val('');
        $('input[name="package_title"]').val('');
        $('textarea[name="package_content"]').val('');
        $('input[name="package_width"]').val('');
        $('input[name="package_height"]').val('');
        $('input[name="package_lenght"]').val('');
        $('input[name="package_weight"]').val('');
        $('select[name="package_type"]').val('');
        $('input[name="package_size"]').val('');
        $('input[name="package_weight_total"]').val('');

        $('input[name="det_order_sell_total"]').val('');

    });

    function form_update() {

        /** Origin and destination */
        params['package_origin'] = $('select[name="branch_id_origin"] option:selected').val();
        params['package_destination'] = $('select[name="branch_id_destination"] option:selected').val();

        params['package_id'] = $('input[name="package_id"]').val();
        params['package_title'] = $('input[name="package_title"]').val();
        params['package_type'] = $('select[name="package_type"] option:selected').val();
        params['package_content'] = $('textarea[name="package_content"]').val();
        params['package_width'] = $('input[name="package_width"]').val();
        params['package_height'] = $('input[name="package_height"]').val();
        params['package_lenght'] = $('input[name="package_lenght"]').val();
        params['package_weight'] = $('input[name="package_weight"]').val();
        params['package_size'] = $('input[name="package_width"]').val() * $('input[name="package_height"]').val() * $('input[name="package_lenght"]').val();
        params['det_order_sell_total'] = $('input[name="det_order_sell_total_temp"]').val();
        $('input[name="det_order_sell_total_temp"]').attr('name', 'det_order_sell_total');

        /** Image handle */
//        var img = {};
//        var index = 0;
//        $('.input-image').each(function () {
////            img[index] = $(this).val();
//            img[index] = index;
//            index++;
//
//        });
        
//        params['package_pic'] = img;

        params['order_id'] = $('input[name="order_id"]').val();
        params['total'] = $('input[name="total"]').val();

        var result = JSON.stringify(params);

        $.post('<?php echo site_url('orders/orders/update'); ?>',
                {data: result}, function (data) {
            data_tables();
        });
    }

    /** Delete Package */
    $('body').on('click', '.btn-delete', function () {

        var data = $(this).parent().parent().find('input').val();
        var data = JSON.parse(data);

        $.get('<?php echo site_url('orders/orders/destroy_package'); ?>/' + data['package_id'],
                {data: null},
        function (data) {
            data_tables();
        }
        );

    });

    bind_change_event();
    data_tables();

    /** Image upload */
    $("a#photo_profile").click(function () {
        $("input#photo_upload").trigger("click");
    });

    $("input#photo_upload").change(function () {

        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) {
            return;
        }

        for (i = 0; files.length; i++) {

            if (/^image/.test(files[i].type)) {

                var reader = new FileReader();
                reader.readAsDataURL(files[i]);

                reader.onloadend = function () {

                    var img = '<div class="col-md-3 img-list"><a class="thumbnail" id="photo_profile">'
                            + '<img src="' + this.result + '" style="width: 100%; height:150px;"><i class="fa fa-times img-delete"></i>'
                            + '<input name="image[]" class="input-image" type="hidden" value="' + this.result + '" ></a>'
                            + '</div>';

                    $('.image-frame').prepend(img);

                }
            }
        }

    });

    $('body').on('click', '.img-delete', function () {
        $(this).parent().parent().remove();
    });

    $('input[name="customer_birth_date"]').datepicker({ format: 'dd/mm/yyyy' });

    $('input[name="unit"]').change(function () {

        if ($(this).val() == 1) {

            /** Change Label */
            $('input[name="package_width"]').parent().parent().find('label').text('Lebar (Cm)');
            $('input[name="package_height"]').parent().parent().find('label').text('Tinggi (Cm)');
            $('input[name="package_lenght"]').parent().parent().find('label').text('Panjang (Cm)');
            $('input[name="package_size"]').parent().parent().find('label').text('Volume Paket (Cm3)');

            /** Change Placeholder */
            $('input[name="package_width"]').attr('placeholder', 'Lebar (Cm)');
            $('input[name="package_height"]').attr('placeholder', 'Tinggi (Cm)');
            $('input[name="package_lenght"]').attr('placeholder', 'Panjang (Cm)');
            $('input[name="package_size"]').attr('placeholder', 'Volume Paket (Cm3)');

            /** Change Value */
            $('input[name="package_width"]').val($('input[name="package_width"]').val() * 100);
            $('input[name="package_height"]').val($('input[name="package_height"]').val() * 100);
            $('input[name="package_lenght"]').val($('input[name="package_lenght"]').val() * 100);

            /** Change package volume */
            $('input[name="package_size"]').val(math.eval($('input[name="package_size"]').val() + ' * 1000000'));


        } else {

            /** Change Label */
            $('input[name="package_width"]').parent().parent().find('label').text('Lebar (Meter)');
            $('input[name="package_height"]').parent().parent().find('label').text('Tinggi (Meter)');
            $('input[name="package_lenght"]').parent().parent().find('label').text('Panjang (Meter)');
            $('input[name="package_size"]').parent().parent().find('label').text('Volume Paket (Meter)');

            /** Change Placeholder */
            $('input[name="package_width"]').attr('placeholder', 'Lebar (Meter)');
            $('input[name="package_height"]').attr('placeholder', 'Tinggi (Meter)');
            $('input[name="package_lenght"]').attr('placeholder', 'Panjang (Meter)');
            $('input[name="package_size"]').attr('placeholder', 'Volume Paket (Meter)');

            /** Change Value */
            $('input[name="package_width"]').val($('input[name="package_width"]').val() / 100);
            $('input[name="package_height"]').val($('input[name="package_height"]').val() / 100);
            $('input[name="package_lenght"]').val($('input[name="package_lenght"]').val() / 100);

            /** Change package volume */
            $('input[name="package_size"]').val(math.eval($('input[name="package_size"]').val() + ' / 1000000'));

        }

        grap_price_params();

    });

    $('a#take-photo').on('shown.bs.tab', function (e) {

        setTimeout(function () {
            Webcam.attach('#photoshot');
        }, 500);

    });

    $('a[href="#tab_customer"]').on('shown.bs.tab', function (e) {
        autocomplete();
    });

    autocomplete();

    function autocomplete() {
        /** Autocomplete */
        var options = {
            url: "<?php echo site_url('request/customer_helper/get'); ?>",
            getValue: "customer_name",
            theme: "square",
            template: {
                type: "description",
                fields: {
                    description: "customer_code"
                }
            },
            list: {
                match: {
                    enabled: true
                },
                onChooseEvent: function () {
                    var customer_id = $("input[name='customer_name']").getSelectedItemData().customer_id;
                    var customer_birth_date = $("input[name='customer_name']").getSelectedItemData().customer_birth_date;
                    $.post('<?php echo site_url('request/customer_helper/get_detail'); ?>/' + customer_id,
                            function (data) {

                                var result = JSON.parse(data);

                                if (customer_id != '') {

                                    $('input[name="customer_id"]').val(customer_id);

                                    $('input[name="customer_web_id"]').val(result['customer_web_id']);
                                    $('input[name="customer_phone"]').val(result['customer_phone']);
                                    $('input[name="customer_id_number"]').val(result['customer_id_number']);
                                    $('input[name="customer_id_type_other"]').val(result['customer_id_type_other']);

                                    $('input[name="customer_birth_date"]').val(customer_birth_date);

                                    $('input[name="customer_sending_quota"]').val(result['customer_sending_quota']);
                                    $('input[name="customer_company_type_other"]').val(result['customer_company_type_other']);
                                    $('input[name="customer_referral"]').val(result['customer_referral']);
                                    $('input[name="customer_company_telp"]').val(result['customer_company_telp']);
                                    $('input[name="customer_company_email"]').val(result['customer_company_email']);
                                    $('input[name="customer_company_name"]').val(result['customer_company_name']);

                                    $('textarea[name="customer_address"]').val(result['customer_address']);
                                    $('textarea[name="customer_company_address"]').val(result['customer_company_address']);

                                    $('select[name="customer_type"]').val(result['customer_type']);
                                    $('select[name="customer_city"]').val(result['customer_city']);
                                    $('select[name="customer_id_type"]').val(result['customer_id_type']);
                                    $('select[name="customer_freq_sending"]').val(result['customer_freq_sending']);
                                    $('select[name="customer_company_type"]').val(result['customer_company_type']);
                                    $('select[name="customer_gender"]').val(result['customer_gender']);
                                    $('input[name="customer_code"]').val(result['customer_code']);

                                    $(".select2").find('select').select2();

                                }

                            }
                    );

                }
            }
        };

        $("input[name='customer_name']").easyAutocomplete(options);

        $('input[name="customer_name"]').on('keyup', function () {
            $('input[name="customer_id"]').val('');
        });

    }

    var optionsx = {
        url: "<?php echo site_url('request/customer_helper/get'); ?>",
        getValue: "customer_name",
        theme: "square",
        template: {
            type: "description",
            fields: {
                description: "customer_code"
            }
        },
        list: {
            match: {
                enabled: true
            },
            onChooseEvent: function () {
                var customer_code = $("input[name='customer_referral']").getSelectedItemData().customer_code;
                $("input[name='customer_referral']").val(customer_code);
            }
        }
    };

    $("input[name='customer_referral']").easyAutocomplete(optionsx);

    $('#photo_profile').click(function () {

        Webcam.snap(function (data_uri) {

            var img = '<div class="col-md-6 img-list"><a class="thumbnail" id="photo_profile">'
                    + '<img src="' + data_uri + '" style="width: 100%; height:150px;"><i class="fa fa-times img-delete"></i>'
                    + '<input name="image[]" class="input-image" type="hidden" value="' + data_uri + '" ></a>'
                    + '</div>';

            $('.image-frame').prepend(img);

        });

    });


    var order_extra = {};
    function form_submit_order_extra() {

        /** Collect data from form */
        order_extra['order_id'] = $('input[name="order_id"]').val();
        order_extra['det_order_extra_name'] = $('input[name="det_order_extra_name"]').val();
        order_extra['det_order_extra_total'] = $('input[name="det_order_extra_total"]').val();

        /** Validation */
        var validation = true;
        $('.package-error').remove();
        $('.has-error').removeClass('has-error');
        global_message = "<ul>";

        if (order_extra['det_order_extra_name'] == '' || order_extra['det_order_extra_name'] == 0 || typeof order_extra['det_order_extra_name'] == 'undefined') {
            validation = set_message($('input[name="det_order_extra_name"]'), 'Nama biaya ekstra belum diisi', true);
        }
        if (order_extra['det_order_extra_total'] == '' || order_extra['det_order_extra_total'] == 0 || typeof order_extra['det_order_extra_total'] == 'undefined') {
            validation = set_message($('input[name="det_order_extra_total"]'), 'Total biaya ekstra belum diisi', true);
        }

        if (validation == false) {
            $(".extra-order-validation-trigger").trigger("click");
            $('#modal-extra-order-validation').on('shown.bs.modal', function (e) {
                $(this).find('.modal-body p').empty();
                $(this).find('.modal-body p').append(global_message + '</ul>');
            });
        } else {

            var result = JSON.stringify(order_extra);

            $.post('<?php echo site_url('orders/orders/det_orders_extra_store'); ?>',
                    {data: result},
            function (data) {
                data_tables();
            }
            );

            $('input[name="det_order_extra_name"]').val('');
            $('input[name="det_order_extra_total"]').val('');

        }

    }

    /** Delete extra order */
    $('body').on('click', '.btn-delete-order-extra', function () {
        delete_extra_order($(this));
    });

    /** Save new extra order */
    $('.btn-order-extra').click(function () {
        form_submit_order_extra();
    });



    $('.btn-next-step1').click(function () {
        $('a[href="#tab_pengiriman"]').trigger('click');
    });

    $('.btn-next-step2').click(function () {
        $('a[href="#tab_paket"]').trigger('click');
    });

    $('.btn-next-step3').click(function () {
        $('a[href="#tab_extra"]').trigger('click');
    });

    $('#show_advanced').click(function () {
        $('.onoff').removeClass('hidden');
    });

</script>