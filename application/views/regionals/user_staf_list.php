<table class="table staf-list">
    <thead>
        <tr>
            <th>#ID</th>
            <th>Username</th>
            <th>Nama</th>
            <th>Poisisi</th>
            <th>Terakhir Login</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($regional_staff)): ?>
            <?php foreach ($regional_staff as $key => $value) : ?>
                <tr>
                    <td><?php echo $value->user_id; ?></td>
                    <td><?php echo $value->username; ?></td>
                    <td><?php echo $value->userprofile_fullname; ?></td>
                    <td><?php echo $value->user_type_name; ?></td>
                    <td><?php echo mdate("%d-%m-%Y %H:%i", $value->last_login); ?></td>
                    <td>
                        <input type="hidden" name ="user_id" value="<?php echo $value->user_id; ?>" />
                        <a class="btn btn-sm btn-danger btn-delete-staft">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>

    </tbody>
</table>