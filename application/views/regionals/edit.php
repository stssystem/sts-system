<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open($path . '/update', ['class' => 'form-edit']); ?>

                <input type="hidden" name="regional_id" value="<?php echo $data->reg_id ?>" />

                <?php
                echo $this->form->text(
                        [
                            'name' => 'regional',
                            'label' => 'Nama Regional',
                            'max' => 100,
                            'required' => true,
                            'value' => $data->regional
                        ]
                );
                ?>

                <?php
                echo $this->form->select([
                    'name' => 'regional_manager',
                    'label' => '',
                    'value' => get_users_all(),
                    'keys' => 'user_id',
                    'values' => 'name',
                    'class' => 'select2',
                    'display_default' => false,
                    'selected' => $data->regional_manager
                ]);
                ?>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url($path); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <button type="submit" class="btn-info btn btn-save" >
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">
    
    /** Select 2 */
    $(".select2").find('select').select2();

    $('.alert-btn-save').click(function () {
        $('.form-edit').submit();
    });

</script>