<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<?php $this->load->view('header'); ?>
<div class="row">
    <div class="col-sm-4">
        <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
                <h3 class="widget-user-username">Cabang <?php echo $data->branch_name; ?></h3>
                <h5 class="widget-user-desc">RM : <?php echo $data->userprofile_fullname; ?></h5>
            </div>
            <div class="widget-user-image">
                <img class="img-circle" src="<?php echo image($data->userprofile_photo, 'noprofile.jpg'); ?>" alt="User Avatar">
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-4 border-right">
                        <div class="description-block">
                            <h5 class="description-header"><?php echo $count_staff; ?></h5>
                            <span class="description-text">Staff</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 border-right">
                        <div class="description-block">
                            <h5 class="description-header"><?php echo $count_package; ?></h5>
                            <span class="description-text">Paket Oustanding</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4">
                        <div class="description-block">
                            <h5 class="description-header"><?php echo $count_order; ?></h5>
                            <span class="description-text">Open Order</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="box">
            <div class="box-header">
                <h4 class="box-title">Tambahkan Staff Cabang </h4>
            </div>
            <div class="box-body">
                <input type="hidden" name="branch_id" value="<?php echo $data->branch_id; ?>" />
                <div class="row">
                    <div class="col-sm-8">
                        <?php
                        echo $this->form->select([
                            'name' => 'branch_user_id',
                            'label' => '',
                            'value' => $users_options,
                            'keys' => 'user_id',
                            'values' => 'name',
                            'multiple' => true,
                            'class' => 'select2',
                            'display_default' => false
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <button class="btn btn-primary btn-block btn-add-user">Tambahkan</button>
                    </div>
                    <div class="col-sm-12">
                        <hr />
                        <table class="table staf-list">
                            <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th>Posisi</th>
                                    <th>Terakhir Login</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($users_available)): ?>
                                    <?php foreach ($users_available as $key => $value) : ?>
                                        <tr>
                                            <td><?php echo $value->user_id; ?></td>
                                            <td><?php echo $value->username; ?></td>
                                            <td><?php echo $value->userprofile_fullname; ?></td>
                                            <td><?php echo $value->user_type_name; ?></td>
                                            <td><?php echo mdate("%d-%m-%Y %H:%i", $value->last_login); ?></td>
                                            <td>
                                                <input type="hidden" name ="user_id" value="<?php echo $value->user_id; ?>" />
                                                <a class="btn btn-sm btn-danger btn-delete-staft">Hapus</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">
    /** Select 2 */
    $(".select2").find('select').select2();

    /** Add user branch  */
    function add_staft() {
        $('.btn-add-user').click(function () {

            var data = {};
            var i = 0;
            var branch_id = $('input[name="branch_id"]').val();

            $('select[name="branch_user_id"]').each(function () {
                data[i] = $(this).val();
            });

            var result = JSON.stringify(data);
            console.log(branch_id);

            $.post('<?php echo site_url('users/users/update_branch_user'); ?>',
                    {branch_id: branch_id, user_list: result},
                    function (data) {
                        $('.staf-list').replaceWith(data);
                        add_staft();
                        delete_staf();
                        replace_select_users();
                    }
            );

            $('select[name="branch_user_id"] option:selected').removeAttr("selected");

        });
    }

    function delete_staf() {
        $('body').on('click', '.btn-delete-staft', function () {

            var id = $(this).parent().find('input').val();
            var tr = $(this).parent().parent();

            $.get('<?php echo site_url('users/users/delete_staft'); ?>/' + id + '/' + $('input[name="branch_id"]').val(),
                    function (data) {
                        $(tr).remove();
                        replace_select_users();
                    }
            );

        });
    }

    function replace_select_users() {
        $.get('<?php echo site_url('branch/branch/get_options'); ?>',
                function (data) {
                    var form = $('select[name="branch_user_id"]').parent();
                    $(form).replaceWith(data);
                    $(".select2").find('select').select2();
                }
        );
    }

    /** Function initialitation */
    add_staft();
    delete_staf();

</script>