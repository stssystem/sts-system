<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4 class="cek"><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php get_alert(); ?>

                <?php echo form_open_multipart($path . '/store', ['class' => 'form-add']); ?>
                <input type="hidden" name="det_route" class='det-route' value="<?php echo old_input('det_route'); ?>" />

                <div class="row">
                    <div class="col-md-6">

                        <div class="row">
                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'branch_name',
                                        'label' => 'Nama Cabang',
                                        'max' => 100,
                                        'required' => true,
                                        'type' => 'text',
                                        'class' => 'col-md-6'
                                    ]
                            );
                            ?>
                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'branch_code',
                                        'label' => 'Kode Cabang (Maksimal 3 Karakter)',
                                        'max' => 3,
                                        'required' => true,
                                        'type' => 'text',
                                        'class' => 'col-md-6'
                                    ]
                            );
                            ?>
                        </div>

                        <?php
                        echo $this->form->textarea(
                                [
                                    'name' => 'branch_address',
                                    'label' => 'Alamat Cabang',
                                    'required' => true,
                                    'rows' => 4,
                                    'class' => ''
                                ]
                        );
                        ?>

                        <div class="row" >

                            <?php
                            echo $this->form->select([
                                'name' => 'branch_city',
                                'label' => 'Kota',
                                'value' => $cities,
                                'keys' => 'city_id',
                                'values' => 'city_name',
                                'class' => 'select2 branch-city-select col-md-6'
                            ]);
                            ?>

                            <?php
                            echo $this->form->select([
                                'name' => 'branch_regional_id',
                                'label' => 'Regional',
                                'value' => get_regional_list(),
                                'keys' => 'reg_id',
                                'values' => 'regional',
                                'class' => 'select2 col-md-6'
                            ]);
                            ?>

                        </div>

                        <div class="row" >
                            <?php
                            echo $this->form->text([
                                'name' => 'branch_phone',
                                'label' => 'No Telp/HP Cabang',
                                'class' => 'col-md-6',
                                'required' => true,
                                'max' => 30,
                                'type' => 'text',
                                'value' => ''
                            ]);
                            ?>

                            <?php
                            echo $this->form->text([
                                'name' => 'branch_email',
                                'label' => 'Email Cabang',
                                'class' => 'col-md-6',
                                'max' => 50,
                                'required' => true,
                                'type' => 'text',
                                'value' => ''
                            ]);
                            ?>
                        </div>

                        <?php
                        echo $this->form->select([
                            'name' => 'branch_parent',
                            'label' => 'Cabang Induk',
                            'value' => $branches,
                            'keys' => 'branch_id',
                            'values' => 'branch_name',
                            'class' => 'select2',
                            'display_default' => true
                        ]);
                        ?>

                        <?php
                        echo $this->form->select([
                            'name' => 'branch_manager',
                            'label' => 'Manager Cabang',
                            'value' => $users,
                            'keys' => 'user_id',
                            'values' => 'userprofile_fullname',
                            'class' => 'select2',
                            'display_default' => false
                        ]);
                        ?>

                        <div class="row">
                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'branch_status',
                                        'label' => 'Status',
                                        'required' => true,
                                        'type' => 'radio',
                                        'class' => 'col-md-4',
                                        'value' => ['1' => 'Aktif', '0' => 'Tidak Aktif'],
                                        'checked' => ['1']
                                    ]
                            );
                            ?>

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'branch_type',
                                        'label' => 'Tipe Cabang',
                                        'required' => true,
                                        'type' => 'radio',
                                        'class' => 'col-md-4',
                                        'value' => ['0' => 'Cabang', '1' => 'Agen'],
                                        'checked' => ['0']
                                    ]
                            );
                            ?>

                            <?php
                            echo $this->form->text(
                                    [
                                        'name' => 'branch_pickup_available',
                                        'label' => 'Pengambilan Paket',
                                        'required' => true,
                                        'type' => 'checkbox',
                                        'class' => 'col-md-4',
                                        'value' => ['1' => 'Menerima'],
                                        'checked' => []
                                    ]
                            );
                            ?>
                        </div>

                        <div class="row">
                            <?php
                            echo $this->form->text([
                                'name' => 'branch_comission',
                                'label' => 'Komisi Cabang / Agen (%)',
                                'class' => 'col-md-12',
                                'max' => 50,
                                'required' => false,
                                'type' => 'number',
                                'value' => ''
                            ]);
                            ?>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-group maps-form">
                                    <label>Lokasi Geografis <?php echo old_input('branch_geo'); ?></label>
                                    <input 
                                        class="form-control" 
                                        type="text" 
                                        name ="branch_geo" 
                                        id="branch_map" 
                                        value="<?php echo old_input('branch_geo') == '' ? '-7.477001,110.220713' : old_input('branch_geo'); ?>" 
                                        readonly="true" />
                                    <span class="help-block map-help-block">Lokasi geografis tidak ditemukan, arahkan marker agar sesuai dengan alamat yang dimaksud</span>
                                </div>  
                                <div id="maps" style="height: 400px; width: 100%;" >
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-cities">
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url($path); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <a type="button" class="btn-info btn btn-save" data-toggle="modal" data-target="#modal-save-alert">
                            <i class="fa fa-save"></i>
                            Simpan
                        </a>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Map api -->
<script src="https://maps.googleapis.com/maps/api/js?v=3&callback=initMap" async defer></script>

<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<script type="text/javascript">

    $('.map-help-block').hide();

    $('.btn-save').click(function () {

        var branch_name = $('.form-add').find('input[name="branch_name"]').val();

        if (branch_name == '') {
            /** Set message */
            var text = "Kolom nama cabang wajib diisi";
        } else {
            /** Set message */
            var text = "Apakah Anda akan menyimpan cabang <strong>" + branch_name + "</strong>?";
        }

        /** Set modal */
        $('#modal-save-alert').find('.modal-body p').html(text);

    }
    );
    $('.alert-btn-save').click(function () {
        $('.form-add').submit();
    });

    /** Select 2 */
    $(".select2").find('select').select2();

    $('select[name="branch_city"]').on("change", function (e) {

        get_geocode();

        setTimeout(function () {
            initMap();
        }, 500);

    });

    $('textarea[name="branch_address"]').focusout(function () {
        get_geocode();
        setTimeout(function () {
            initMap();
        }, 500);
    });

</script>

<script type="text/javascript">
    var map;
    function initMap() {

        var latlang = $('input[name="branch_geo"]').val();
        var res = latlang.split(",");

        var latitude = res[0];
        var longitude = res[1];

        var myLatlng = new google.maps.LatLng(latitude, longitude);
        document.getElementById('branch_map').value = latitude + ',' + longitude;

        var myOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById("maps"), myOptions);

        addMarker(myLatlng, 'Lokasi Cabang', map);

        function addMarker(latlng, title, map) {
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: title,
                draggable: true
            });

            marker.addListener('drag', function (event) {
                document.getElementById('branch_map').value = event.latLng.lat() + ',' + event.latLng.lng();
            });
        }

    }

    function get_geocode() {

        var geocoder = new google.maps.Geocoder();
        var city = $('select[name="branch_city"] option:selected').text();
        city = city.trim();
        var street = $('textarea[name="branch_address"]').val();
        var address = street + ',' + city;

        geocoder.geocode({'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $('input[name="branch_geo"]').val(latitude + ',' + longitude);
                $('.map-help-block').hide();
                $('.maps-form').removeClass('has-error');
            } else {
                $('.map-help-block').show();
                $('.maps-form').addClass('has-error');
            }
        });

    }

</script>