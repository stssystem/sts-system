<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8">
                        <h4><?php echo $title; ?></h4>
                    </div>
                    <div class="col-md-4">
                        <div class="box-tools pull-right">
                            <a href="<?php echo site_url($path . '/add') ?>" class="btn btn-success">
                                <i class="fa fa-plus"></i> Tambah Data Cabang
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive ">
                <?php get_alert(); ?>

                <table class="table data-tables">
                    <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th width="10%">Kode Cabang</th>
                            <th width="20%">Cabang</th>
                            <th width="15%">Regional</th>
                            <th width="10%">Tipe Cabang</th>
                            <th width="10%">Status</th>
                            <th width="15%">Dibuat</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>	
                </table>
            </div>
        </div>
    </div>
</div>



<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    $('.data-tables').DataTable({
        serverSide: true,
        ordering: true,
        order: [
            [
                <?php dtorder_column('dt_branches', 0); ?>, 
                "<?php dtorder_mode('dt_branches', 'asc'); ?>"
            ]
        ],
        displayStart : <?php dtarray('dt_branches','start', 0); ?>,
        pageLength : <?php dtarray('dt_branches', 'lenght', 10); ?>,
        search: {
           search: "<?php dtarray('dt_branches', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path) ?>',
            type: 'POST'
        },
        columns: [
            {data: 'branch_id'},
            {data: 'branch_code'},
            {data: 'branch_name'},
            {data: 'branch_regional'},
            {data: 'branch_type'},
            {data: 'branch_status'},
            {data: 'created_at'},
            {data: 'action'}
        ], 
        columnDefs: [
            { targets: [ 7 ], orderable: false},
        ],
    }
    );

    /** Data tables set up */
    // $('.data-tables').DataTable({
    //     ajax: {
    //         url: '<?php echo site_url($path) ?>',
    //         dataSrc: ''
    //     },
    //     columns: [
    //         {data: 'branch_id'},
    //         {data: 'branch_code'},
    //         {data: 'branch_name'},
    //         {data: 'branch_regional'},
    //         {data: 'branch_type'},
    //         {data: 'branch_status'},
    //         {data: 'created_at'},
    //         {data: 'action'}
    //     ],
    // }
    // );

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus data cabang <strong>" + data['branch_name'] + "</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);
        
        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['branch_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    })



</script>