<fieldset class="scheduler-border" style="display:none" id="show-1">
    <legend class="scheduler-border"> Pilih Kolom yang akan di Export --</legend>
    <div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="1">ID</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="2">Tanggal</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="3">Nama Pengirim</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="4">Asal Paket</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="5">Tujuan Paket</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="6">Total</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="7">Total Ekstra</label>
	</div>
	<!-- <div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="8">Jumlah</label>
	</div> -->
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="8">Status</label>
	</div>
    <p>Note: Jika semua kolom tidak dicentang, maka semua kolom akan di export</p>
</fieldset>

<fieldset class="scheduler-border" style="display:none" id="show-2">
    <legend class="scheduler-border"> Pilih Kolom yang akan di Export --</legend>
    <div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="1">ID Customer</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="2">Nama Customer</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="3">Kota Asal</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="4">Cabang Asal</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="5">No Referal</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="6">Total Transaksi</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="7">Tanggal Transaksi Terakhir</label>
	</div>
    <p>Note: Jika semua kolom tidak dicentang, maka semua kolom akan di export</p>
</fieldset>

<fieldset class="scheduler-border" style="display:none" id="show-3">
    <legend class="scheduler-border"> Pilih Kolom yang akan di Export --</legend>
    <div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="1">ID Cabang</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="2">Nama Cabang</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="3">Total Order</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="4">Total Omset</label>
	</div>
	<!-- <div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="5">Komisi Agen</label>
	</div> -->
    <p>Note: Jika semua kolom tidak dicentang, maka semua kolom akan di export</p>
</fieldset>

<fieldset class="scheduler-border" style="display:none" id="show-4">
    <legend class="scheduler-border"> Pilih Kolom yang akan di Export --</legend>
    <div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="1">Tanggal</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="2">No Resi</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="3">Pengirim</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="4">Cabang/Agen Asal</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="5">Cabang/Agen Tujuan</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="6">Sistem Pembayaran</label>
	</div>
	<!-- <div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="7">Status</label>
	</div> -->
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="7">Jatuh Tempo</label>
	</div>
    <p>Note: Jika semua kolom tidak dicentang, maka semua kolom akan di export</p>
</fieldset>

<fieldset class="scheduler-border" style="display:none" id="show-5">
    <legend class="scheduler-border"> Pilih Kolom yang akan di Export --</legend>
    <div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="1">ID Kota</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="2">Nama Kota</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="3">Total Berat</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="4">Total Volume</label>
	</div>
    <p>Note: Jika semua kolom tidak dicentang, maka semua kolom akan di export</p>
</fieldset>

<fieldset class="scheduler-border" style="display:none" id="show-6">
    <legend class="scheduler-border"> Pilih Kolom yang akan di Export --</legend>
    <div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="1">ID Armada</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="2">Nama Kendaraan / Armada</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="3">Status Kendaraan</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="4">Asal</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="5">Tujuan</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="6">Jadwal Berangkat</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="7">Jadwal Tiba</label>
	</div>
    <p>Note: Jika semua kolom tidak dicentang, maka semua kolom akan di export</p>		
</fieldset>

<fieldset class="scheduler-border" style="display:none" id="show-7">
    <legend class="scheduler-border"> Pilih Kolom yang akan di Export --</legend>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="1">Nama Cabang</label>
	</div>
	<div class="checkbox disabled">
		<label><input type="checkbox" name="column-export[]" value="2" checked="true" disabled>Total Paket</label>
	</div>
    <p>Note: Jika semua kolom tidak dicentang, maka semua kolom akan di export</p>
</fieldset>

<fieldset class="scheduler-border" style="display:none" id="show-8">
    <legend class="scheduler-border"> Pilih Kolom yang akan di Export --</legend>
    <div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="1">ID</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="2">Nama Armada</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="3">Plat Nomor</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="4">Nomor Device</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="column-export[]" value="5">Lokasi Terakhir</label>
	</div>
	<p>Note: Jika semua kolom tidak dicentang, maka semua kolom akan di export</p>
</fieldset>

