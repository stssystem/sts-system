<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }

    fieldset.scheduler-border {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
                box-shadow:  0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
        font-size: 1em !important;
        font-weight: bold !important;
        text-align: left !important;
    }

</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-body">

                <div class="col-md-6">
                        
                    <?php get_alert(); ?>
                    
                    <?php echo form_open('report/report_filter/export'); ?>

                    <div class="form-group">
                        <label>Download dalam format:</label>
                        <select class="form-control" name="type-export" id="type-export">
                            <option value=" ">-- Pilih Format Download --</option>
                            <option value="1">PDF</option>
                            <option value="2">EXCEL</option>
                            <option value="3">CSV</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Pilih Jenis Laporan:</label>
                        <select class="form-control" name="filter-report" id="filter-report">
                            <option value=" ">-- Pilih Jenis Laporan --</option>
                            <option value="1">Laporan Keuangan</option>
                            <option value="2">Laporan Customer</option>
                            <option value="3a">Laporan Penjualan > Laporan Komisi Agen > Cabang Asal</option>
                            <option value="3b">Laporan Penjualan > Laporan Komisi Agen > Paket Naik</option>
                            <option value="3c">Laporan Penjualan > Laporan Komisi Agen > Paket Turun</option>
                            <option value="3d">Laporan Penjualan > Laporan Komisi Agen > Cabang Tujuan</option>
                            <option value="4">Laporan Penjualan > Laporan Status Pembayaran</option>
                            <option value="5">Laporan Logistik > Muatan Per Kota</option>
                            <option value="6">Laporan Logistik > Muatan Per Armada</option>
                            <option value="7a">Laporan Logistik > Muatan Pengantaran > Paket Dalam Proses Pengantaran</option>
                            <option value="7b">Laporan Logistik > Muatan Pengantaran > Paket Diterima</option>
                            <option value="7c">Laporan Logistik > Muatan Pengantaran > Paket Kembali ke Agen</option>
                            <option value="7d">Laporan Logistik > Muatan Pickup Order</option>
                            <option value="8">Laporan GPS</option>
                        </select>
                    </div>

                    <?php $this->load->view('report_filter/filter-report'); ?>

                    <div class="form-group date" style="display:none">
                        <label>Tanggal Awal</label>
                        <input type="text" name="date_start" class="form-control" placeholder="Masukan Tanggal Awal"/>
                    </div>

                    <div class="form-group date" style="display:none">
                        <label>Tanggal Akhir</label>
                        <input type="text" name="date_end" class="form-control" placeholder="Masukan Tanggal Akhir"/>
                    </div>

                    <button class="btn btn-info" style="float: right;"><i class="fa fa-download"></i> Export</button>
                       
                    <?php echo form_close(); ?>

                </div>

            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript" >

    $('input[name="date_start"]').datepicker({ format: 'dd/mm/yyyy' });
    $('input[name="date_end"]').datepicker({ format: 'dd/mm/yyyy' });

    $(document).ready(function(){
    $('#filter-report').on('change', function() {
        if ( this.value == '1'){
            $("#show-1").show();

            $("#show-2").hide();
            $("#show-3").hide();
            $("#show-4").hide();
            $("#show-5").hide();
            $("#show-6").hide();
            $("#show-7").hide();
            $("#show-8").hide();
            $(".date").show();
        } else if ( this.value == '2'){
            $("#show-2").show();

            $("#show-1").hide();
            $("#show-3").hide();
            $("#show-4").hide();
            $("#show-5").hide();
            $("#show-6").hide();
            $("#show-7").hide();
            $("#show-8").hide();
            $(".date").show();
        } else if ( this.value == '3a' || this.value == '3b' || this.value == '3c' || this.value == '3d'){
            $("#show-3").show();

            $("#show-1").hide();
            $("#show-2").hide();
            $("#show-4").hide();
            $("#show-5").hide();
            $("#show-6").hide();
            $("#show-7").hide();
            $("#show-8").hide();
            $(".date").hide();
        } else if ( this.value == '4'){
            $("#show-4").show();

            $("#show-1").hide();
            $("#show-2").hide();
            $("#show-3").hide();
            $("#show-5").hide();
            $("#show-6").hide();
            $("#show-7").hide();
            $("#show-8").hide();
            $(".date").show();
        } else if ( this.value == '5'){
            $("#show-5").show();

            $("#show-1").hide();
            $("#show-2").hide();
            $("#show-3").hide();
            $("#show-4").hide();
            $("#show-6").hide();
            $("#show-7").hide();
            $("#show-8").hide();
            $(".date").hide();
        } else if ( this.value == '6'){
            $("#show-6").show();

            $("#show-1").hide();
            $("#show-2").hide();
            $("#show-3").hide();
            $("#show-4").hide();
            $("#show-5").hide();
            $("#show-7").hide();
            $("#show-8").hide();
            $(".date").hide();
        } else if ( this.value == '7a' || this.value == '7b' || this.value == '7c' || this.value == '7d'){
            $("#show-7").show();

            $("#show-1").hide();
            $("#show-2").hide();
            $("#show-3").hide();
            $("#show-4").hide();
            $("#show-5").hide();
            $("#show-6").hide();
            $("#show-8").hide();
            $(".date").hide();
        } else if ( this.value == '8'){
            $("#show-8").show();

            $("#show-1").hide();
            $("#show-2").hide();
            $("#show-3").hide();
            $("#show-4").hide();
            $("#show-5").hide();
            $("#show-6").hide();
            $("#show-7").hide();
            $(".date").hide();
        } else {
            $("#show-1").hide();
            $("#show-2").hide();
            $("#show-3").hide();
            $("#show-4").hide();
            $("#show-5").hide();
            $("#show-6").hide();
            $("#show-7").hide();
            $("#show-8").hide();
            $(".date").hide();
        }
    });
});
</script>