<script type="text/javascript">
    
    // Finsih button
    $('body').on('click', '.btn-finish', function(){
        var order_id = $(this).parent().parent().find('input').val();
        order_id = JSON.parse(order_id);
        $('#finish-modal').find('input[name="order_id"]').val(order_id['order_id']);
    });

    // Set Select2
    $('select[name="as_branch"]').select2();

    // Default column 
    var column_default = [
        {data: 'franco_id'},
        {data: 'order_id'},
        {data: 'customer_name'},
        {data: 'destination_branch_name'},
        {data: 'origin_branch_name'},
        {data: 'status'},
        {data: 'last_update'},
        {data: 'action'}
    ];

    // Set default tab
    if ($.session.get('tab-franco-list') != '') {
        var href_ = '#' + $.session.get('tab-franco-list');
        shown_tab(column_default);
        $('a[href="' + href_ + '"]').trigger('click');
    }

    // Datatable default setup
    data_tables(1, 'must_proces', column_default);

    // New order list 
    function shown_tab(column_default) {

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

            /** Variable initiation */
            var target = $(e.target).attr("href");
            var status;
            var column = column_default;

            target = target.replace("#", "");

            $.session.set('tab-franco-list', target);

            switch (target) {
                case 'must_proces':
                    status = 1;
                    column = column_default;
                    break;
                case 'will_achieve':
                    status = 2;
                    column = column_default;
                    break;
                case 'achieved':
                    status = 3;
                    column = column_default;
                    break;
                case 'deliver':
                    status = 4;
                    column = column_default;
                    break;
                case 'finish':
                    status = 5;
                    column = column_default;
                    break;

            }

            data_tables(status, target, column);

        });
    }


    // Datatable template
    function data_tables(status, id_table, column) {
        
        var url = '<?php echo site_url($path); ?>/index/' + status;

        $('.class_' + id_table).dataTable().fnDestroy();

        $('.class_' + id_table).DataTable({
            serverSide: true,
            ordering: true,
            order: [
                [
                    <?php dtorder_column('dt_franco_list', 0); ?>, 
                    "<?php dtorder_mode('dt_franco_list', 'asc'); ?>"
                ]
            ],
            displayStart : <?php dtarray('dt_franco_list','start', 0); ?>,
            pageLength : <?php dtarray('dt_franco_list', 'lenght', 10); ?>,
            search: {
            search: "<?php dtarray('dt_franco_list', 'search', ''); ?>"
            },
            ajax: {
                url: url,
                type: 'POST',
            },
            columns: column,
        });

    }


</script>