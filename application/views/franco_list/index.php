<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">

<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<style type="text/css">
    .txt-sm{
        font-size: 13px;
    }
</style>

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>
<?php $this->load->view('franco_list/finish_modal'); ?>

<div class="row">
    <div class="col-sm-12">

        <?php echo get_alert(); ?>

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a class="pickup_tab" href="#must_proces" data-value="1" data-toggle="tab" aria-expanded="false"> Harus Diproses</a>
                </li>
                <li class="">
                    <a class="pickup_tab" href="#will_achieve" data-value="2" data-toggle="tab" aria-expanded="false"> Akan Diterima</a>
                </li>
                <li class="">
                    <a class="pickup_tab" href="#achieved" data-value="3" data-toggle="tab" aria-expanded="false"> Diterima</a>
                </li>
                <li class="">
                    <a class="pickup_tab" href="#deliver" data-value="4" data-toggle="tab" aria-expanded="false"> Dikirim</a>
                </li>
                <li class="">
                    <a class="pickup_tab" href="#finish" data-value="5" data-toggle="tab" aria-expanded="false"> Selesai</a>
                </li>
                <li class="pull-right">

                    <?php echo form_open($path . '/filter'); ?>
                    <?php if (profile()->user_type_id == 99 || profile()->branch_access != '' || profile()->regional_id != ''): ?>

                        <select name="as_branch" class="select2 form-control">
                            <option value="0">-- Lihat Sebagai Cabang --</option>

                            <?php
                            if (profile()->branch_access == '' && profile()->regional_id != '') {
                                $branches_list = get_branches_list(null, profile()->regional_id);
                            } else {
                                $branches_list = get_branches_list(null);
                            }
                            ?>

                            <?php if (!empty($branches_list)): ?>
                                <?php foreach ($branches_list as $key => $item): ?>
                                    <option 
                                        value="<?php echo $item['branch_id']; ?>"
                                        <?php echo (isset($this->session->userdata('franco_list_filter')['as_branch']) && $this->session->userdata('franco_list_filter')['as_branch'] == $item['branch_id']) ? 'selected="selected"' : '' ?>
                                        >
                                            <?php echo $item['branch_name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>

                        <button class="btn btn-info btn-flat" type="submit">
                            <i class="fa fa-search"></i>
                            Lihat
                        </button>

                    <?php endif; ?>

                    <a href="<?php echo site_url($path . '/reset_status_filter'); ?>" class="btn btn-warning btn-flat btn-clear" >
                        <i class="fa fa-refresh"></i>
                        Reset
                    </a>

                    <?php echo form_close(); ?>

                </li>
            </ul>
            <div class="tab-content">
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="must_proces">
                    <?php $this->load->view('franco_list/table', ['tbclass' => 'class_must_proces']); ?>
                </div>
                <div class="tab-pane" id="will_achieve">
                    <?php $this->load->view('franco_list/table', ['tbclass' => 'class_will_achieve']); ?>
                </div>
                <div class="tab-pane" id="achieved">
                    <?php $this->load->view('franco_list/table', ['tbclass' => 'class_achieved']); ?>
                </div>
                <div class="tab-pane" id="deliver">
                    <?php $this->load->view('franco_list/table', ['tbclass' => 'class_deliver']); ?>
                </div>
                <div class="tab-pane" id="finish">
                    <?php $this->load->view('franco_list/table', ['tbclass' => 'class_finish']); ?>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>

    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>
<?php $this->load->view('franco_list/js'); ?>