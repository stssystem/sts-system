<div class="modal fade" tabindex="-1" role="dialog" id="finish-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open($path . '/self_take'); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi Resi Sudah Sampai Kostumer</h4>
                <input type="hidden" name="order_id" />
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Penerima</label>
                    <input class="form-control" name="receiver_name" type="text" />
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-ban"></i>
                        Batal
                    </button>
                    <button type="submit" class="btn btn-info">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
