<div class="box-body table-responsive ">

    <table class="table table-striped data-tables txt-sm <?php echo $tbclass; ?>">
        <thead>
            <tr>
                <th width="5%" >ID</th>
                <th width="13%" >Nomor Resi</th>
                <th width="18%" >Nama Customer</th>
                <th width="17%" >Asal</th>
                <th width="17%" >Tujuan</th>
                <th width="10%" >Status</th>
                <th width="12%" >Pembaruan Terakhir</th>
                <th width="8%" class="th-action">Aksi</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

</div>