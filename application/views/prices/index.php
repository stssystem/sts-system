<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8">
                        <h4><?php echo $title; ?></h4>    
                    </div>
                    <div class="col-md-4">
                        <div class="box-tools pull-right">
                            <a href="<?php echo site_url($path . '/add') ?>" class="btn btn-success">
                                <i class="fa fa-plus"></i> Tambah Data Harga
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive ">
                
                <?php get_alert(); ?>

                <table class="table data-tables">
                    <thead>
                        <tr>
                            <th width="6%" >#ID</th>
                            <th width="15%" >Asal</th>
                            <th width="15%" >Tujuan</th>
                            <th width="10%"  class="text-right">Harga / kg Retail</th>
                            <th width="10%"  class="text-right">Harga / kg Partai</th>
                            <th width="10%"  class="text-right">Harga / m3 Retail</th>
                            <th width="10%"  class="text-right">Harga / m3 Partai</th>
                            <th width="10%"  class="text-right">Harga Minimum </th>
                            <th width="14%" >Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>	
                </table>
            </div>
        </div>
    </div>
</div>



<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >

    /** Data tables set up */
    $('.data-tables').DataTable({
        ajax: {
            url: '<?php echo site_url($path) ?>',
            dataSrc: ''
        },
        columns: [
            {data: 'price_id'},
            {data: 'price_branch_origin_id'},
            {data: 'price_branch_destination_id'},
            {data: 'price_kg_retail'},
            {data: 'price_kg_partai'},
            {data: 'price_volume_retail'},
            {data: 'price_volume_partai'},
            {data: 'price_minimum'},
            {data: 'action'}
        ]
    }
    );

    /** Delete */
    $('table.data-tables').on('click', '.btn-delete', function () {

        /** Get data */
        var data = $(this).parent().parent().find('input[name="edit-value"]').val();
        data = JSON.parse(data);

        /** Set modal */
        var text = "Apakah Anda yakin akan menghapus harga dengan ID <strong>"+data['price_id']+"</strong>?";
        $('#modal-delete-alert').find('.modal-body p').html(text);
        /** Set url */
        var url = "<?php echo site_url($path . '/destroy') ?>/" + data['city_origin_id'] + '/' + data['city_destination_id'];
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', url);

    });

    /** Modal closed */
    $('#modal-delete-alert').on('hidden.bs.modal', function (e) {
        $('#modal-delete-alert').find('.modal-body p').empty();
        $('#modal-delete-alert').find('.modal-footer a.a-delete').attr('href', '');
    })

</script>