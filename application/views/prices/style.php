<!-- Select 2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

<!-- Math.JS -->
<script src="<?php echo base_url(); ?>assets/plugins/mathjs/math.js"></script>

<script type="text/javascript">

    /** Select 2 */
    $(".select2").find('select').select2();

    $('input[name="price_kg_retail"]').on('keyup', function () {

        var price_kg_partai = math.eval('(70 / 100) * ' + $(this).val());
        price_kg_partai = math.ceil(price_kg_partai);
        $('input[name="price_kg_partai"]').val(price_kg_partai);

        var price_volume_retail = math.eval($(this).val() + ' / 0.004');
        price_volume_retail = math.ceil(price_volume_retail);
        $('input[name="price_volume_retail"]').val(price_volume_retail);

        var price_volume_partai = math.eval(price_kg_partai + ' / 0.004');
        price_volume_partai = math.ceil(price_volume_partai);
        $('input[name="price_volume_partai"]').val(price_volume_partai);

    });


</script>