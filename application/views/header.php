<!DOCTYPE html>
<html>
    <?php $this->load->view('head_include'); ?>
    <body class="hold-transition skin-yellow sidebar-mini" >
        
        <div class="wrapper">

            <?php $this->load->view('header_include'); ?>
            <?php $this->load->view('sidebar_include'); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo $title; ?>
                        <small><?php echo $title2; ?></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">