<?php $this->load->view('header');?>
  <div class="row">
    <div class="col-sm-12">
      <div class="box">
        
        <div class="box-body">
          <div class="row">
            <div class="col-sm-4">
              
              <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="<?php echo image_profile(); ?>" alt="User profile picture">

                <h3 class="profile-username text-center"><?php echo profile()->userprofile_fullname;?></h3>

                <p class="text-muted text-center"><?php echo profile()->user_type_name;?></p>

                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <b>Cabang</b> <a class="pull-right">
                      <?php echo profile()->branch_name;?>

                    </a>
                  </li>
                  <li class="list-group-item">
                    <b>Terakhir Login</b> <a class="pull-right"><?php echo mdate('%d/%m/%Y %H:%i:%s',profile()->last_login);?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Status Server</b> <a class="pull-right">Terhubung</a>
                  </li>
                </ul>

               
              </div>

            </div>
            <div class="col-sm-6">
              <h3 class="box-title">Selamat Datang <?php echo profile()->userprofile_fullname;?></h3>
              <p>Ini adalah halaman Dashboard Anda, Selamat Bekerja!!</p>
              <a href="#" class="btn btn-primary btn-sm">Butuh Bantuan ?</a>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        
      </div>
    </div>
  </div>

<?php $this->load->view('footer');?>
<script src="<?php echo base_url();?>assets/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url();?>assets/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/js/demo.js"></script>
