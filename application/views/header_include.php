<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src="<?php echo base_url('assets/img/logo.jpg'); ?>" style="height:50px;"></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="<?php echo base_url('assets/img/logo.jpg'); ?>" style="height:50px;"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <?php if (!empty(get_armada_visited())): ?>
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-truck"></i>
                            <span class="label label-success">
                                <?php echo count(get_armada_visited()); ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu" style="width: 300px;height: 300px;" >
                            <li class="header">Armada yang Akan Datang Hari Ini : <?php echo count(get_armada_visited()); ?> Armada</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <?php foreach (get_armada_visited() as $key => $item): ?>
                                        <li>
                                            <a href="<?php echo site_url('trip/armadas/view/'.$item->armada_id); ?>">
                                                <div class="pull-left">
                                                    <img src="<?php echo image($item->armada_photo); ?>" class="img-circle" alt="User Image">
                                                </div>
                                                <h4>
                                                    <?php echo $item->armada_name; ?> 
                                                    <small><i class="fa fa-user"></i>&nbsp; <?php echo get_armada_driver($item->armada_driver); ?></small>
                                                </h4>
                                                <p>Rute : <?php echo get_armada_beginend($item->trip_id); ?> </p>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo image_profile(); ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo profile()->userprofile_fullname; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo image_profile(); ?>" class="img-circle" alt="User Image">
                            <p>
                                <?php echo profile()->userprofile_fullname; ?> -  <?php echo profile()->user_type_name; ?>
                                <small>Didaftarkan <?php echo mdate('%d %M %Y', profile()->user_created_at); ?></small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo site_url('users/user_profile/edit/' . profile()->user_id); ?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo site_url('program/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
<!--                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>-->
            </ul>
        </div>
    </nav>
</header>