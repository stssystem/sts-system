<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-delete'); ?>

<style type="text/css">
    .txt-sm{

    }
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h5 style="font-weight: bold;">REKAM JEJAK ARMADA</h5>    
                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <a href="<?php echo site_url('report/report_gps'); ?>" class="btn btn-info">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                            <a href="<?php echo site_url($path . '/clear/' . $armada_id); ?>" class="btn btn-info">
                                <i class="fa fa-refresh"></i> Reset
                            </a>
                            <a href="<?php echo site_url($path . '/download/' . $armada_id); ?>" class="btn btn-success">
                                <i class="fa fa-file-excel-o"></i> Download
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <table class="table txt-sm">
                            <tr>
                                <th>Nama Armada</th>
                                <th>:</th>
                                <td><?php echo $armadas->armada_name; ?></td>
                            </tr>
                            <tr>
                                <th>Nomor Polisi</th>
                                <th>:</th>
                                <td><?php echo $armadas->armada_license_plate; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive ">

                <?php get_alert(); ?>

                <table class="table data-tables txt-sm table-bordered" >
                    <thead>
                        <tr>
                            <th widht="10%" >ID</th>
                            <th widht="30%" >Asal Tujuan</th>
                            <th widht="15%" >Pengemudi</th>
                            <th widht="15%" >Kenek</th>
                            <th widht="10%" >Dibuat</th>
                            <th widht="10%" >Waktu Berangkat</th>
                            <th widht="10%" >Waktu Tiba</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>	
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript" >


    /** Data tables set up */
    $('.data-tables').DataTable({
        serverSide: true,
        ordering: true,
        order: [
            [
<?php dtorder_column('dt_report_gps_detail', 0); ?>,
                "<?php dtorder_mode('dt_report_gps_detail', 'asc'); ?>"
            ]
        ],
        displayStart: <?php dtarray('dt_report_gps_detail', 'start', 0); ?>,
        pageLength: <?php dtarray('dt_report_gps_detail', 'lenght', 10); ?>,
        search: {
            search: "<?php dtarray('dt_report_gps_detail', 'search', ''); ?>"
        },
        ajax: {
            url: '<?php echo site_url($path . '/index/' . $armada_id) ?>',
            type: 'POST'
        },
        columns: [
            {data: 'trips_id'},
            {data: 'route_name'},
            {data: 'driver_fullname'},
            {data: 'codriver_fullname'},
            {data: 'created_at'},
            {data: 'trip_start_date'},
            {data: 'trip_end_date'},
        ]
    }
    );


</script>