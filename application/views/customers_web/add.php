<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/easy-autocomplete/easy-autocomplete.css">

<?php $this->load->view('header'); ?>
<?php $this->load->view('component/modal/alert-save'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">

                <?php echo form_open($path . '/store', 'class="form-horizontal"'); ?>
                <div class="row">
                    <div class="col-sm-6">
                        <h4 class="box-title">No. Referral</h4>
                    </div>

                    <div class="col-sm-6">
                        <h4 class="box-title">Kode Pelanggan</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_referral',
                                        'label' => 'Bila Ada',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12',
                                        'attribute' => ['style' => 'border-radius:0px;']
                                    ]
                            );
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_code',
                                        'label' => 'Kode Pelanggan',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12'
                                    ]
                            );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-horizontal col-sm-6">
                        <h4>Informasi Umum</h4>
                        <div class="row">
                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_name',
                                        'label' => 'Nama *',
                                        'max' => 100,
                                        'required' => true,
                                        'class' => 'col-sm-12',
                                        'attribute' => ['style' => 'border-radius:0px;']
                                    ]
                            );
                            ?>

                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_id_number',
                                        'label' => 'Nomor Pengenal *',
                                        'max' => 100,
                                        'required' => true,
                                        'class' => 'col-sm-12'
                                    ]
                            );
                            ?>

                            <?php
                            echo $this->form->select_h(
                                    [
                                        'name' => 'customer_id_type',
                                        'label' => 'Tipe Pengenal *',
                                        'max' => 100,
                                        'required' => true,
                                        'class' => 'col-md-12',
                                        'value' => [
                                            ['id' => 1, 'item' => 'KTP'],
                                            ['id' => 2, 'item' => 'SIM'],
                                            ['id' => 3, 'item' => 'NPWP'],
                                            ['id' => 3, 'item' => 'Lainnya']
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item'
                                    ]
                            );
                            ?>

                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_id_type_other',
                                        'label' => 'Tipe Pengenal (Lain)',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                    ]
                            );
                            ?>


                        </div>

                        <div class="row">



                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_birth_date',
                                        'label' => 'Tanggal Lahir',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-sm-12',
                                        'value' => ''
                                    ]
                            );
                            ?>

                            <?php
                            echo $this->form->select_h(
                                    [
                                        'name' => 'customer_gender',
                                        'label' => 'Jenis Kelamin *',
                                        'max' => 100,
                                        'required' => true,
                                        'class' => 'col-md-12',
                                        'value' => [
                                            ['id' => 1, 'item' => 'Laki-Laki'],
                                            ['id' => 2, 'item' => 'Perempuan']
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item'
                                    ]
                            );
                            ?>

                            <?php
                            echo $this->form->select_h(
                                    [
                                        'name' => 'customer_type',
                                        'label' => 'Tipe Customer',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => [
                                            ['id' => 1, 'item' => 'Personal'],
                                            ['id' => 2, 'item' => 'Perusahaan'],
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item'
                                    ]
                            );
                            ?>

                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_phone',
                                        'label' => 'No. Telp *',
                                        'max' => 100,
                                        'required' => true,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                    ]
                            );
                            ?>

                        </div>
                    </div>

                    <div class="col-sm-6 form-horizontal">
                        <h4 class="box-title">Informasi Alamat</h4>
                        <div class="row">

                            <div class="col-md-12">


                                <div class="row">
                                    <?php
                                    echo $this->form->select_h(
                                            [
                                                'name' => 'customer_city',
                                                'label' => 'Kota',
                                                'max' => 100,
                                                'required' => false,
                                                'class' => 'select2 col-md-12',
                                                'value' => $cities,
                                                'keys' => 'city_id',
                                                'values' => 'city_name',
                                            ]
                                    );
                                    ?>
                                </div>
                                <div class="row">
                                    <?php
                                    echo $this->form->textarea_h(
                                            [
                                                'name' => 'customer_address',
                                                'label' => 'Alamat',
                                                'max' => 100,
                                                'required' => false,
                                                'value' => '',
                                                'rows' => 3,
                                                'class' => 'col-sm-12'
                                            ]
                                    );
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <hr />
                                <h4 class="box-title">Informasi Pengiriman</h4>
                                <div class="row">
                                    <?php
                                    echo $this->form->select_h(
                                            [
                                                'name' => 'customer_freq_sending',
                                                'label' => 'Frekuensi Pengiriman Paket',
                                                'max' => 100,
                                                'required' => false,
                                                'class' => 'select2 col-md-12',
                                                'value' => [
                                                    ['id' => 1, 'value' => 'Harian'],
                                                    ['id' => 2, 'value' => 'MIngguan'],
                                                    ['id' => 3, 'value' => 'Bulanan'],
                                                ],
                                                'keys' => 'id',
                                                'values' => 'value',
                                            ]
                                    );
                                    ?>

                                    <?php
                                    echo $this->form->text_h(
                                            [
                                                'name' => 'customer_sending_quota',
                                                'label' => 'Jumlah Pengiriman Paket',
                                                'max' => 100,
                                                'required' => false,
                                                'value' => '',
                                                'class' => 'col-md-12'
                                            ]
                                    );
                                    ?>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>




                <div class="row">
                    <div class="col-md-6 form-horizontal">
                        <hr />
                        <h4 class="box-title">Informasi Perusahaan</h4>
                        <div class="row">
                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_company_name',
                                        'label' => 'Nama Perusahaan',
                                        'max' => 100,
                                        'required' => false,
                                        'value' => '',
                                        'rows' => 3,
                                        'class' => 'col-md-12'
                                    ]
                            );
                            ?>


                            <?php
                            echo $this->form->select_h(
                                    [
                                        'name' => 'customer_company_type',
                                        'label' => 'Jenis Perusahaan',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'select2 col-md-12',
                                        'value' => [
                                            ['id' => 1, 'item' => 'Manufaktur'],
                                            ['id' => 2, 'item' => 'Jasa'],
                                            ['id' => 3, 'item' => 'Ritel'],
                                            ['id' => 4, 'item' => 'Lainnya'],
                                        ],
                                        'keys' => 'id',
                                        'values' => 'item'
                                    ]
                            );
                            ?>
                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_company_type_other',
                                        'label' => 'Jenis Perusahaan Lainnya',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                    ]
                            );
                            ?>
                        </div>

                        <div class="row">
                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_web_id',
                                        'label' => 'ID Web',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                    ]
                            );
                            ?>
                        </div>




                        <div class="row">

                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_company_telp',
                                        'label' => 'Nomor Telepon Perusahaan',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                    ]
                            );
                            ?>

                            <?php
                            echo $this->form->text_h(
                                    [
                                        'name' => 'customer_company_email',
                                        'label' => 'Alamat Email Perusahaan',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => ''
                                    ]
                            );
                            ?>

                        </div>

                        <div class="row">
                            <?php
                            echo $this->form->textarea_h(
                                    [
                                        'name' => 'customer_company_address',
                                        'label' => 'Alamat Perusahaan',
                                        'max' => 100,
                                        'required' => false,
                                        'class' => 'col-md-12',
                                        'value' => '',
                                        'rows' => 5
                                    ]
                            );
                            ?>
                        </div>
                    </div>

                    <div class="btn-group">
                        <a href="<?php echo site_url($path); ?>" class="btn btn-flat btn-default">
                            <i class="fa fa-arrow-left"></i> Kembali 
                        </a>
                        <button type="submit" class="btn btn-primary btn-flat ">
                            <i class="fa fa-save"></i> Simpan
                        </button>        
                    </div>

                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('footer'); ?>

    <!-- Select 2 -->
    <script src="<?php echo base_url(); ?>assets/plugins/select2/select2.min.js"></script>

    <!-- Easy Autocomplete -->
    <script src="<?php echo base_url(); ?>assets/plugins/easy-autocomplete/jquery.easy-autocomplete.min.js"></script>

    <script type="text/javascript">

        $('.select2').find('select').select2();

        $('input[name="customer_birth_date"]').datepicker({ format: 'dd/mm/yyyy' });

        $('.alert-btn-save').click(function () {
            $('.form-add').submit();
        });

        var optionsx = {
            url: "<?php echo site_url('request/customer_helper/get'); ?>",
            getValue: "customer_name",
            theme: "square",
            template: {
                type: "description",
                fields: {
                    description: "customer_code"
                }
            },
            list: {
                match: {
                    enabled: true
                },
                onChooseEvent: function () {
                    var customer_code = $("input[name='customer_referral']").getSelectedItemData().customer_code;
                    $("input[name='customer_referral']").val(customer_code);
                }
            }
        };

        $("input[name='customer_referral']").easyAutocomplete(optionsx);

    </script>