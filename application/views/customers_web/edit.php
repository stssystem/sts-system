<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="box-body">
                
                <?php get_alert(); ?>

                <?php echo form_open($path . '/update/'.$account['ID'] , ['class' => 'form-add']); ?>
                <input type="hidden" name="id" value="<?php echo $account['ID'] ?>" />

                <div  class='row'>
                    <div  class='col-md-12'>

                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#profile" data-toggle="tab" aria-expanded="false">
                                        Data Diri
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#account" data-toggle="tab" aria-expanded="false">
                                        Password
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile">

                                    <div class="row">

                                        <?php
                                        echo $this->form->text(
                                                [
                                                    'name' => 'first_name',
                                                    'label' => 'Nama Lengkap',
                                                    'max' => 100,
                                                    'required' => true,
                                                    'class' => 'col-md-6',
                                                    'value' => isset($profile['first_name']) ? $profile['first_name'] : ''
                                                ]
                                        );
                                        ?>

                                        <?php
                                        echo $this->form->text(
                                                [
                                                    'name' => 'company',
                                                    'label' => 'Perusahaan',
                                                    'max' => 100,
                                                    'required' => false,
                                                    'class' => 'col-md-6',
                                                    'value' => isset($profile['company']) ? $profile['company'] : ''
                                                ]
                                        );
                                        ?>

                                    </div>

                                    <div class="row">

                                        <?php
                                        echo $this->form->text(
                                                [
                                                    'name' => 'nickname',
                                                    'label' => 'Email',
                                                    'max' => 100,
                                                    'required' => false,
                                                    'class' => 'col-md-6',
                                                    'value' => isset($profile['nickname']) ? $profile['nickname'] : '',
                                                    'disabled' => true
                                                ]
                                        );
                                        ?>

                                        <?php
                                        echo $this->form->text(
                                                [
                                                    'name' => 'phone',
                                                    'label' => 'Nomor Telepon',
                                                    'max' => 15,
                                                    'required' => false,
                                                    'class' => 'col-md-6',
                                                    'value' => isset($profile['phone']) ? $profile['phone'] : '',
                                                    'disabled' => false
                                                ]
                                        );
                                        ?>

                                    </div>

                                    <div  class='row'>
                                        <?php
                                        echo $this->form->textarea(
                                                [
                                                    'name' => 'address',
                                                    'label' => 'Alamat Lengkap',
                                                    'max' => 15,
                                                    'required' => false,
                                                    'class' => 'col-md-12',
                                                    'rows' => 3,
                                                    'value' => isset($profile['address']) ? $profile['address'] : ''
                                                ]
                                        );
                                        ?>
                                    </div>

                                </div>

                                <div class="tab-pane" id="account">
                                    <div class="row">

                                        <?php
                                        echo $this->form->text(
                                                [
                                                    'name' => 'new_password',
                                                    'label' => 'Password Baru',
                                                    'max' => 100,
                                                    'required' => false,
                                                    'class' => 'col-md-6',
                                                    'value' => '',
                                                    'disabled' => false,
                                                    'type' => 'password'
                                                ]
                                        );
                                        ?>

                                        <?php
                                        echo $this->form->text(
                                                [
                                                    'name' => 'confirm_password',
                                                    'label' => 'Konfirmasi Password Baru',
                                                    'max' => 15,
                                                    'required' => false,
                                                    'class' => 'col-md-6',
                                                    'value' => '',
                                                    'disabled' => false,
                                                    'type' => 'password'
                                                ]
                                        );
                                        ?>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <!-- <div  class='col-md-3'>
                        <div  class='row'>
                            <div class="col-md-12" style="height: 100%;">
                                <label>Foto Profile</label>
                                <a href="" class="thumbnail" id="photo_profile">
                                    <img src="<?php echo (isset($profile['custom_avatar']) && $profile['custom_avatar'] != '') ? $profile['custom_avatar'] : image(); ?>" style="width: 100%;">
                                </a>
                                <input type="file" name="userfile" id="photo_upload" style="display: none;" />
                            </div>
                        </div>
                    </div> -->
                </div>

                <div class="form-group">
                    <div class="btn-group">
                        <a href="<?php echo site_url($path); ?>" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                        <button type="submit" class="btn-info btn btn-save" >
                            <i class="fa fa-save"></i>
                            Simpan
                        </button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

    /** File image upload */
    $("a#photo_profile").click(function () {
        $("input#photo_upload").trigger("click");
    });
    $("input#photo_upload").change(function () {

        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) {
            return;
        }
        if (/^image/.test(files[0].type)) {

            var reader = new FileReader();
            reader.readAsDataURL(files[0]);

            reader.onloadend = function () {

                $('a#photo_profile > img').remove();
                var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                $('a#photo_profile').append(html);

            }

        }

    });

</script>