<?php

/** Backend Alert */
/** ------------------------------------------------------------------------------------------------------------------------- */
if (!function_exists('set_alert')) {

    function set_alert($mode = 'alert-info', $message = '', $button = array()) {

        $self = & get_instance();

        if (!empty($button)) {
            $data = array(
                'mode' => $mode,
                'message' => $message,
                'button' => array(
                    'url' => $button['url'],
                )
            );
        } else {
            $data = array(
                'mode' => $mode,
                'message' => $message,
                'button' => ''
            );
        }

        $self->session->set_flashdata('alert', $data);
    }

}

if (!function_exists('get_alert')) {

    function get_alert() {

        $self =& get_instance();

        $data = $self->session->flashdata('alert');

        if (!empty($data)) {
             $self->load->view('component/alert', $data);
        } else {
            return false;
        }
    }

}

/** Frontend Alert */
/** ------------------------------------------------------------------------------------------------------------------------- */

if (!function_exists('set_falert')) {

    function set_falert($mode = 'alert-info', $message = '', $button = array()) {

        $self = & get_instance();

        if (!empty($button)) {
            $data = array(
                'mode' => $mode,
                'message' => $message,
                'button' => array(
                    'url' => $button['url'],
                )
            );
        } else {
            $data = array(
                'mode' => $mode,
                'message' => $message,
                'button' => ''
            );
        }

        $self->session->set_flashdata('falert', $data);
    }

}

if (!function_exists('get_falert')) {

    function get_falert() {
        
        $self = & get_instance();

        $data = $self->session->flashdata('falert');

        if (!empty($data)) {
            return $self->load->view('frontend/component/partial/alert', $data);
        } else {
            return false;
        }
    }

}

if (!function_exists('delete_confirm')) {

    function delete_confirm($data = array()) {

        return $self->load->view('backend/component/alert', $data);
    }

}
?>