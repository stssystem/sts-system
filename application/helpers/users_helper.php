<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('get_users_list')) {

    function get_users_list($regional_id = 0) {

        $self = & get_instance();

        $self->load->model('users_model');

        $result = $self->users_model
                ->get_profile(['userprofile.regional_id' => NULL])
                ->result_array();

        return $result;
        
    }

}

if (!function_exists('get_users_all')) {

    function get_users_all($regional_id = 0) {

        $self = & get_instance();

        $self->load->model('users_model');

        $result = $self->users_model
                ->get_profile()
                ->result_array();

        return $result;
        
    }

}
