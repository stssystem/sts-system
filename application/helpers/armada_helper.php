<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('get_armada_visited')) {

    function get_armada_visited() {
        $CI = & get_instance();
        $CI->load->model('trips_model');
        if (profile()->branch_id != '') {
            return $CI->trips_model->get_armada_visited(profile()->branch_id)->result();
        } else {
            return null;
        }
    }

}

if (!function_exists('get_armada_beginend')) {

    function get_armada_beginend($trip_id = 0) {
        $CI = & get_instance();
        $CI->load->model('trips_model');
        $CI->load->model('branches_model');
        $result = $CI->trips_model->get_startend_branch($trip_id);
        $start = $CI->branches_model->get(['branch_id' => $result['start']])->row();
        $stop = $CI->branches_model->get(['branch_id' => $result['stop']])->row();

        return $start->branch_name . ' - ' . $stop->branch_name;
    }

}

if (!function_exists('get_armada_driver')) {

    function get_armada_driver($user_id = 0) {
        $CI = & get_instance();
        $CI->load->model('users_model');

        $result = $CI->users_model->get_profile(['user.user_id' => $user_id])->row();

        return $result->userprofile_fullname;
    }

}