<?php

if (!function_exists('orderLastPosition')) {
    function orderLastPosition($order_id, $noted) {
        
        $ci = & get_instance();
        $ci->load->model('orders_locations_model');
        
        $geo_branch = branch() != null ? branch()['branch_geo'] : '';
        $city = branch() != null ? branch()['branch_city'] : '';
        $branch = branch() != null ? branch()['branch_id'] : '';
        
        $ci->orders_locations_model->update_last_location($order_id, $geo_branch, $city, $branch, $noted);
        
    }
}