<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('dt_session')) {

    function dt_session($key = '', $value = null) {

        $ci = & get_instance();

        if ($value != null) {
            $ci->session->set_userdata($key, $value);
            return true;
        } else {
            return $ci->session->userdata($key);
        }
    }

}

if (!function_exists('dtarray')) {
    function dtarray($key, $item, $default) {
        echo isset(dt_session($key)[$item]) ? dt_session($key)[$item] : $default;
    }
}

if (!function_exists('dtarray_mlt')) {
    function dtarray_mlt($key, $item, $item, $default) {
        echo isset(dt_session($key)[$item][$item]) ? dt_session($key)[$item][$item] : $default;
    }
}

if (!function_exists('dtorder_column')) {
    function dtorder_column($key, $default) {
        echo isset(dt_session($key)['order']['columns']) ? dt_session($key)['order']['columns'] : $default;
    }
}

if (!function_exists('dtorder_mode')) {
    function dtorder_mode($key, $default) {
        echo isset(dt_session($key)['order']['mode']) ? dt_session($key)['order']['mode'] : $default;
    }
}