<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if(!function_exists('get_regional_list')){
    function get_regional_list(){
        $obj =& get_instance();
        $obj->load->model('regionals_model');
        return $obj->regionals_model->get()->result_array();
    }
}