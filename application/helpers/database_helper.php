<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('database')) {

    function db_connect($switch = 'traccar') {

        $CI = & get_instance();

        $CI->load->model('sys_config_model');
        $result = $CI->sys_config_model->get()->row();

        $config['hostname'] = $result->{$switch . '_url'};
        $config['username'] = $result->{$switch . '_db_username'};
        $config['password'] = $result->{$switch . '_db_password'};
        $config['database'] = $result->{$switch . '_db'};
        $config['dbdriver'] = 'mysqli';
        $config['dbprefix'] = '';
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = '';
        $config['char_set'] = 'utf8';
        $config['dbcollat'] = 'utf8_general_ci';

        return $CI->load->database($config, true);
        
    }

}