<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('download_report')) {

    function download_report($data, $report_name = '') {

        $ci = & get_instance();

        $ci->load->helper('download');
        $ci->load->dbutil();
        $delimiter = ",";
        $newline = "\r\n";
        $new_report = $ci->dbutil->csv_from_result($data, $delimiter, $newline);
        $name = 'laporan_' . $report_name . '_' . date('Y-m-d h-i-s') . '.csv';
        
        return force_download($name, $new_report);
        
    }

}