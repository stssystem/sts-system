<?php

if(!function_exists('session_r')){
    function session_r($key = '', $value = ''){
        $ci =& get_instance();
        
        if($key != '' && $value != ''){
            $ci->session->set_userdata($key, $value);
            return true;
        }
        
        if($key != '' && $value == ''){
            return $ci->session->userdata($key);
        }
        
    }

}

