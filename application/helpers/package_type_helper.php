<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('get_package_type_list')) {
    function get_package_type_list() {
        $ci = & get_instance();
        $ci->load->model('package_types_model');
        return $ci->package_types_model->get()->result_array();
    }
}