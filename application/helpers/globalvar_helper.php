<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$CI = & get_instance();

if (!function_exists('user_status')) {

    function user_status($user_type_id = 0) {
        $return_val = '';
        if ($user_type_id == 1)
            $return_val = "<span class='label label-success'><i class='fa fa-check'></i> Aktif</span>";
        elseif ($user_type_id == 0)
            $return_val = "<span class='label label-danger'><i class='fa fa-times'></i> Tidak Aktif</span>";
        return $return_val;
    }

}

if (!function_exists('get_user_type')) {

    function get_user_type() {

        $CI = & get_instance();

        $CI->load->model('user_types_model');
        $query = $CI->user_types_model->get_user_types();

        $result = [];
        $result[''] = '-- Pilih Tipe User --';
        foreach ($query->result() as $key => $item) {
            $result[$item->user_type_id] = $item->user_type_name;
        }

        return $result;
    }

}

if (!function_exists('profile')) {

    function profile() {

        $CI = & get_instance();

        $CI->load->model('users_model');
        $data = $CI->users_model->get_profile($CI->session->userdata('user_meta')->user_id);

        return $data->row();
    }

}

if (!function_exists('branch')) {

    function branch() {

        /** Get CodeIgniter instance */
        $CI = & get_instance();
        $CI->load->model('branches_model');

        if (profile()->branch_id != '') {
            return $CI->branches_model->get_joined(['branches.branch_id' => profile()->branch_id])->row_array();
        } else {
            return null;
        }
    }

}

if (!function_exists('image_profile')) {

    function image_profile() {
        $filename = profile()->userprofile_photo;
        return $filename == '' ? base_url('assets/public/noprofile.jpg') : base_url('assets/upload/' . $filename);
    }

}

if (!function_exists('image')) {

    function image($filename = '', $default = 'noimage.png') {
        return $filename == '' ? base_url('assets/public/' . $default) : base_url('assets/upload/' . $filename);
    }

}

/** Form error function  */
if (!function_exists('error')) {

    function error($key = '') {

        $CI = & get_instance();
        $result = $CI->session->flashdata('error');        

        if (!empty($result)) {
            return (key_exists($key, $result)) ? $result[$key] : null;
        } else {
            return null;
        }
    }

}

/** Form error function  */
if (!function_exists('old_input')) {

    function old_input($key = '') {
        $CI = & get_instance();
        $result = $CI->session->flashdata('oldinput');
        if (!empty($result)) {
            return (key_exists($key, $result)) ? $result[$key] : null;
        } else {
            return null;
        }
    }

}

if (!function_exists('old')) {

    function old($key = '', $value = '') {

        $CI = & get_instance();

        if ($value != '' || is_array($key)) {
            if (is_array($key)) {
                if (!empty($key)) {
                    foreach ($key as $k => $i) {
                        if ($i != '') {
                            $CI->session->set_userdata($k, $i);
                        }
                    }
                }
            } else {
                $CI->session->set_userdata($key, $value);
            }
        } else {
            $result = $CI->session->userdata($key);
            $CI->session->unset_userdata($key);
            return $result;
        }
    }

}

if (!function_exists('package_type')) {

    function package_type($package_type = 0) {
        $return_val = '';
        if ($package_type == 1)
            $return_val = "<span class='label label-success'>Normal</span>";
        elseif ($package_type == 2)
            $return_val = "<span class='label label-danger'>Liquid</span>";
        elseif ($package_type == 3)
            $return_val = "<span class='label label-info'>Lainnya</span>";

        return $return_val;
    }

}

if (!function_exists('id_type')) {

    function idcards_type($id = '') {

        $idcard = [
            ['id' => 1, 'value' => 'KTP'],
            ['id' => 2, 'value' => "SIM"],
            ['id' => 2, 'value' => "NPWP"],
            ['id' => 2, 'value' => "Lainnya"],
        ];

        if ($id == '') {
            return $idcard;
        } else {
            foreach ($idcard as $key => $value) {
                if ($value['id'] == $id) {
                    return $value['value'];
                }
            }
            return null;
        }
    }

}

if (!function_exists('package_status')) {

    function package_status($status = 0) {
        $return_val = '';
        if ($status == 0)
            $return_val = "Baru Diterima";
        elseif ($status == 1)
            $return_val = "Di Gudang";
        elseif ($status == 2)
            $return_val = "Di Proses";
        elseif ($status == 3)
            $return_val = "Siap Di Muat";
        elseif ($status == 4)
            $return_val = "Sudah Di Angkut";
        elseif ($status == 5)
            $return_val = "Selesai";
        elseif ($status == 6)
            $return_val = "Transit";
        elseif ($status == 7)
            $return_val = "Pengantaran";
        elseif ($status == 8)
            $return_val = "Diserahkan";

        return $return_val;
    }

}

if (!function_exists('order_status')) {

    function order_status($status = 0) {
        $return_val = '';
        if ($status == 0)
            $return_val = "Baru Diterima";
        elseif ($status == 1)
            $return_val = "Dalam Proses";
        elseif ($status == 2)
            $return_val = "Selesai";
        elseif ($status == 3)
            $return_val = "Belum Bayar";
        elseif ($status == 4)
            $return_val = "Akan Sampai";
        if ($status == -3)
            $return_val = "Draft";

        return $return_val;
    }

}

if (!function_exists('payment_methode')) {

    function payment_methode($status = 0) {

        $return_val = '';

        if ($status == 1)
            $return_val = "Cash / Tunai";
        elseif ($status == 2)
            $return_val = "Franko";
        elseif ($status == 3)
            $return_val = "Tunai Tempo";
        elseif ($status == 4)
            $return_val = "Tagih Tempo";

        return $return_val;
    }

}

if (!function_exists('customer_type')) {

    function customer_type($status = 0) {
        $return_val = '';
        if ($status == 1)
            $return_val = "Personal";
        elseif ($status == 2)
            $return_val = "Perusahaan";
        return $return_val;
    }

}

if (!function_exists('menu_access')) {

    function menu_access() {
        $menu = [
            ['id' => 1, 'value' => 'Pengiriman Baru'],
            ['id' => 2, 'value' => 'Pengambilan'],
            ['id' => 3, 'value' => 'Pickup Order'],
            ['id' => 4, 'value' => 'Cek Status'],
            ['id' => 5, 'value' => 'Daftar Order'],
            ['id' => 6, 'value' => 'Loading List'],
            ['id' => 7, 'value' => 'Unloading'],
            ['id' => 8, 'value' => 'Daftar Customer'],
            ['id' => 9, 'value' => 'Tambah Customer'],
            ['id' => 10, 'value' => 'Generate Pre-Manifest'],
            ['id' => 11, 'value' => 'Daftar Pre-Manifest'],
            ['id' => 12, 'value' => 'Kelola Kendaraan'],
            ['id' => 13, 'value' => 'Kelola Rute'],
            ['id' => 14, 'value' => 'Jadwal Perjalanan'],
            ['id' => 15, 'value' => 'Trip Report'],
            ['id' => 16, 'value' => 'Kelola Harga'],
            ['id' => 17, 'value' => 'Traccar'],
            ['id' => 18, 'value' => 'Daftar User'],
            ['id' => 19, 'value' => 'Tambah User'],
            ['id' => 20, 'value' => 'Daftar Tipe User'],
            ['id' => 21, 'value' => 'Tambah Tipe User'],
            ['id' => 22, 'value' => 'Daftar Kota'],
            ['id' => 23, 'value' => 'Tambah Kota'],
            ['id' => 24, 'value' => 'Daftar Cabang'],
            ['id' => 25, 'value' => 'Tambah Cabang'],
            ['id' => 26, 'value' => 'Pengaturan Sistem'],
            ['id' => 27, 'value' => 'Loading Barang'],
            ['id' => 28, 'value' => 'Semua Tipe Paket'],
            ['id' => 29, 'value' => 'Tambah Tipe Paket'],
            ['id' => 30, 'value' => 'Pengantaran'],
            /** Report  */
            ['id' => 31, 'value' => 'Laporan Keuangan'],
            ['id' => 32, 'value' => 'Laporan Pajak'],
            ['id' => 33, 'value' => 'Laporan Penjualan'],
            ['id' => 34, 'value' => 'Laporan Logistik'],
            ['id' => 35, 'value' => 'Laporan Trip Plan'],
            ['id' => 36, 'value' => 'Update Lokasi'],
            ['id' => 37, 'value' => 'Customer Website'],
            ['id' => 38, 'value' => 'Daftar Regional'],
            ['id' => 39, 'value' => 'Tambah Regional'],
            ['id' => 40, 'value' => 'Approval Edit Detail Order'],
            ['id' => 41, 'value' => 'Booking Order'],
            // Kelola Resi
            ['id' => 42, 'value' => 'Daftar Resi Franco'],
            ['id' => 43, 'value' => 'Kirim Resi'],
            ['id' => 44, 'value' => 'Terima Resi'],
            ['id' => 45, 'value' => 'Antar Resi'],
            ['id' => 46, 'value' => 'Laporan GPS'],
            ['id' => 47, 'value' => 'Lacak Resi'],
            ['id' => 49, 'value' => 'Laporan Customer'],
            ['id' => 50, 'value' => 'Daftar Kelola Paket']
        ];

        return $menu;
    }

}


if (!function_exists('is_access')) {

    function is_access($id) {
        $access = json_decode(profile()->user_type_access, true);
        return (!empty($access) && in_array($id, $access)) || profile()->user_type_id == 99 ? true : false;
    }

}

if (!function_exists('is_access_parent')) {

    function is_access_parent($array = array()) {
        $access = false;
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                if (is_access($value) == true) {
                    $access = true;
                }
            }
        }

        return $access;
    }

}

if (!function_exists('get_armadas')) {

    function get_armadas($origin, $destination) {

        $CI = & get_instance();
        $CI->load->model('trips_model');

        /** Armada list  */
        $armada_array = array();
        $qarmada_array[0] = "Pilih Armada";
        $qarmada = $CI->trips_model->get_armada_per_trip($origin, $destination)->result();
        if (!empty($qarmada)) {
            foreach ($qarmada as $rarmada) {
                $qarmada_array[$rarmada->trip_id] = $rarmada->armada_name;
            }
        }

        return $qarmada_array;
    }

}

if (!function_exists('get_all_armadas')) {

    function get_all_armadas() {

        $CI = & get_instance();
        $CI->load->model('trips_model');

        $armada_array = array();
        $qarmada_array[0] = "Pilih Armada";
        $qarmada = $CI->trips_model->get_armada_transited();
        foreach ($qarmada->result() as $rarmada) {
            $qarmada_array[$rarmada->trip_id] = $rarmada->armada_name;
        }

        return $qarmada_array;
    }

}

if (!function_exists('get_armada_norestict')) {

    function get_armada_norestict() {
        $ci = & get_instance();
        $ci->load->model('armadas_model');
        $result = $ci->armadas_model->get()->result();
        $options = [];
        if (!empty($result)) {
            foreach ($result as $key => $item) {
                $options[$item->armada_id] = $item->armada_name;
            }
        }
        return $options;
    }

}

if (!function_exists('get_all_couriers')) {

    function get_all_couriers() {
        $CI = & get_instance();
        $CI->load->model('users_model');

        $result[0] = "Pilih Kurir";
        $users = $CI->users_model->get_profile()->result();
        foreach ($users as $item) {
            $result[$item->user_id] = $item->userprofile_fullname;
        }

        return $result;
    }

}

if (!function_exists('branch_type')) {

    function branch_type($id) {
        return ($id == 0) ? '<span class="label label-warning">Cabang</span>' : '<span class="label label-danger" >Agen</span>';
    }

}

if (!function_exists('get_armada')) {

    function get_armada() {
        $ci = & get_instance();
        $ci->load->model('armadas_model');
        $result = $ci->armadas_model->get()->result();

        $option = [];
        if (!empty($result)) {
            foreach ($result as $key => $item) {
                $option[$item->armada_id] = $item->armada_name;
            }
        }

        return $option;
    }

}


if (!function_exists('order_type')) {

    function order_type($status) {

        $return_val = '';

        if ($status == 1) {
            $return_val = "Ritel";
        } elseif ($status == 2) {
            $return_val = "Partai";
        } elseif ($status == 3) {
            $return_val = "Partai Borong";
        }

        return $return_val;
    }

}


