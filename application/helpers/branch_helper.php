<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('get_branch')) {

    function get_branch() {

        $ci = & get_instance();
        $ci->load->model('branches_model');

        $data = $ci->branches_model->get()->result();
        $option = [];
        if (!empty($data)) {
            foreach ($data as $key => $item) {
                $option[$item->branch_id] = $item->branch_name;
            }
        }
        return $option;
    }

}

if (!function_exists('get_branches_list')) {

    function get_branches_list($city_id = null, $regional_id = '') {

        $obj = & get_instance();
        $obj->load->model('branches_model');

        $where = array();

        if ($city_id != null) {
            $where = array(
                'branch_city' => $city_id
            );
        }

        if ($regional_id != '') {
            $where = array(
                'branch_regional_id' => $regional_id
            );
        }

        return $obj->branches_model->get($where)->result_array();
    }

}

if (!function_exists('get_restricted_branches')) {

    function get_restricted_branches() {

        $obj = & get_instance();
        $obj->load->model('branches_model');

        $where = array();
        if ((profile()->user_type_id != 99 && profile()->branch_access == '')) {

            if (profile()->regional_id) {
                $where = array('branches.branch_regional_id' => profile()->regional_id);
            }

            if (profile()->branch_id && profile()->regional_id == '') {
                $where = array('branches.branch_id' => profile()->branch_id);
            }
            
        }

        return $obj->branches_model
                        ->get($where)
                        ->result_array();
    }

}

if (!function_exists('get_freely_branches')) {

    function get_freely_branches() {

        $obj = & get_instance();
        $obj->load->model('branches_model');

        return $obj->branches_model
                        ->get()
                        ->result_array();
    }

}