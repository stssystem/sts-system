<?php

if (!function_exists('orders_update')) {

    function orders_update($order_id, $status) {
        $ci = & get_instance();
        $ci->load > model('orders_model');
        $ci->orders_model->update([
            'order_id' => $order_id,
            'order_status' => $status
        ]);
    }

}

if (!function_exists('temp_orders')) {

    function temp_orders() {
        $ci = & get_instance();
        return $ci->session->userdata('temp_orders');
    }

}

if (!function_exists('temp_delivery')) {

    function temp_delivery($point = '') {
        return isset(temp_orders()['orders']->$point) ? json_decode(temp_orders()['orders']->$point, true) : false;
    }

}

if (!function_exists('split_address')) {

    function split_address($data, $mode) {

        $data = json_decode($data, true);

        // Remove province key
        if (key_exists("province_id_$mode", $data))
            unset($data["province_id_$mode"]);

        // Remove branch key
        if (key_exists("branch_id_$mode", $data))
            unset($data["branch_id_$mode"]);

        return json_encode($data);
    }

}


if (!function_exists('update_address')) {

    function update_address($order_id) {

        // Create instance
        $ci = & get_instance();

        // Load all reseource needed
        $ci->load->model('orders_model');
        $ci->load->helper('order');

        // Get current address
        $data = $ci->orders_model->get_address_text($order_id);

        // Update address 
        $ci->orders_model->update([
            'order_id' => $order_id,
            'order_origin_text' => split_address($data->order_origin_text, 'origin'),
            'order_destination_text' => split_address($data->order_destination_text, 'destination'),
        ]);

        return true;
    }

}

if (!function_exists('view_session')) {

    function view_session($key, $value = '') {
        
        $ci = & get_instance();

        if ($value == '') {
            return $ci->session->flashdata($key);
        } else {
            $ci->session->set_flashdata($key, $value);
            return true;
        }
        
    }

}