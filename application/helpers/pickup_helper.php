<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('pickup_set')) {

    function pickup_set($pickup_order_id = 0) {
        
        $ci = & get_instance();
        
        $result = '';
        if(branch() != null){
            $result = branch()['branch_code'].$pickup_order_id;
        }else{
            $result = $pickup_order_id;
        }
       
        $ci->session->set_userdata('pickup_order_id', $result);
        
        pickup_up();
        
    }

}

if (!function_exists('pickup_isset')) {

    function pickup_isset($pickup_order_id = 0) {
        $ci = & get_instance();
        return !empty($ci->session->userdata('pickup_order_id')) ? true : false;
    }

}

if (!function_exists('pickup_id_get')) {

    function pickup_id_get() {
        $ci = & get_instance();
        return $ci->session->userdata('pickup_order_id');
    }

}


if (!function_exists('pickup_order_get')) {

    function pickup_order_get($result_type = 'json') {

        $ci = & get_instance();

        $ci->load->model('pickup_orders_model');

        $result = $ci->pickup_orders_model->get_row([
                    'wp_ss_pick_up_order.no_resi' => substr(pickup_id_get(), -7)
                ])->row();

        return $result_type == 'json' ? json_encode($result) : $result;
    }

}

if (!function_exists('pickup_destroy')) {

    function pickup_destroy() {
        $ci = & get_instance();
        $ci->session->unset_userdata('pickup_order_id');
        pickup_down();
    }

}

if (!function_exists('pickup_up')) {

    function pickup_up() {
        $ci = & get_instance();
        $ci->session->set_userdata('pickup-set', true);
    }

}

if (!function_exists('pickup_down')) {

    function pickup_down() {
        $ci = & get_instance();
        $ci->session->unset_userdata('pickup-set');
    }

}

if (!function_exists('pickup_ping')) {

    function pickup_ping() {
        $ci = & get_instance();
        return $ci->session->userdata('pickup-set') ? true : false;
    }

}

if (!function_exists('pickup_unlock')) {

    function pickup_unlock() {
        $ci = & get_instance();
        $ci->session->set_userdata('lock', true);
    }

}

if (!function_exists('pickup_lock')) {

    function pickup_lock() {
        $ci = & get_instance();
        $ci->session->unset_userdata('lock');
    }

}

if (!function_exists('pickup_isunlock')) {

    function pickup_isunlock() {
        $ci = & get_instance();
        return $ci->session->userdata('lock') ? true : false;
    }

}
