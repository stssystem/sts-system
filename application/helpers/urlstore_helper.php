<?php

/**
 * Session name exist : 
 * url-temp, last-tab, last-url, url-arr-temp
 */
if (!function_exists('url_store')) {

    function url_store($url) {
        $ci = & get_instance();
        $url_temp = array();
        $url_temp = $ci->session->userdata('url-temp');
        $url_temp[] = $url;
        $ci->session->set_userdata('url-temp', $url_temp);
    }

}


if (!function_exists('url_remove')) {

    function url_remove($url = '') {
        $ci = & get_instance();
        $url_temp = $ci->session->userdata('url-temp');
        if (is_array($url_temp)) {
            $url_temp = array_diff($url_temp, array($url));
        }
        $ci->session->set_userdata('url-temp', $url_temp);
    }

}

if (!function_exists('url_exist')) {

    function url_exist($url) {
        $ci = & get_instance();
        return is_array($ci->session->userdata('url-temp')) && in_array($url, $ci->session->userdata('url-temp')) ? true : false;
    }

}

/** Last request session */
// Last tab client request
if (!function_exists('set_last_tab')) {

    function set_last_tab($tab) {
        $ci = & get_instance();
        $ci->session->set_userdata('last-tab', $tab);
    }

}

// Last url request
if (!function_exists('set_last_url')) {

    function set_last_url($url) {
        $ci = & get_instance();
        $ci->session->set_userdata('last-url', $url);
    }

}

// Get last tab client request
if (!function_exists('get_last_tab')) {

    function get_last_tab() {
        $ci = & get_instance();
        return $ci->session->userdata('last-tab');
    }

}

// Get last url request
if (!function_exists('get_last_url')) {

    function get_last_url() {
        $ci = & get_instance();
        return $ci->session->userdata('last-url');
    }

}

/** Url array session */
if (!function_exists('urlarr_store')) {

    function urlarr_store($url, $tab) {
        $ci = & get_instance();
        $url_temp = array();
        $url_temp = $ci->session->userdata('url-arr-temp');
        $url_temp[$tab] = str_replace(site_url() . '/', '', $url);
        $ci->session->set_userdata('url-arr-temp', $url_temp);
    }

}