<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if(!function_exists('get_cities_list')){
    function get_cities_list(){
        $obj =& get_instance();
        $obj->load->model('cities_model');
        return $obj->cities_model->get()->result_array();
    }
}