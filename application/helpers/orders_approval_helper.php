<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('get_where')) {

    function get_where() {
        $user_type_id = profile()->user_type_id;

        $user_regional       = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }

        if ($user_type_id == 99 || $user_type_id == 120) { // super admin & Admin Pusat
            $where = ['order_status' => 2];
        } else if ($user_type_id == 124) { // Regional
            $where = [
            'order_status' => 2, 
            'regional_id' => $user_regional
            ];
        } else {
            $where = ['order_status' => 0];
        }

        return $where;
    }
}

if (!function_exists('get_notif_1')) {

    function get_notif_1() {
        $CI = & get_instance();
        $CI->load->model('package_edit_model');
        return $CI->package_edit_model->get_notif(get_where());
    }
}

if (!function_exists('get_notif_2')) {

    function get_notif_2() {
        $CI = & get_instance();
        $CI->load->model('package_edit_location_model');
        return $CI->package_edit_location_model->get_notif(get_where());
    }
}

if (!function_exists('get_notif_3')) {

    function get_notif_3() {
        $CI = & get_instance();
        $CI->load->model('package_edit_total_model');
        return $CI->package_edit_total_model->get_notif(get_where());
    }

}

if (!function_exists('get_notif_4')) {

    function get_notif_4() {
        $CI = & get_instance();
        $CI->load->model('package_edit_orders_extra_model');
        return $CI->package_edit_orders_extra_model->get_notif(get_where());
    }
}

if (!function_exists('get_notif_5')) {

    function get_notif_5() {
        $CI = & get_instance();
        $CI->load->model('package_delete_model');

        $user_type_id = profile()->user_type_id;

        $user_regional       = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }

        if ($user_type_id == 99 || $user_type_id == 120) { // super admin & Admin Pusat
            $where = ['package_delete_status' => 2];
        } else if ($user_type_id == 124) { // Regional
            $where = [
            'package_delete_status' => 2, 
            'regional_id'           => $user_regional
            ];
        } else {
            $where = ['package_delete_status' => 0];
        }

        return $CI->package_delete_model->get_notif($where);
    }
}

if (!function_exists('get_notif_orders_approval')) {

    function get_notif_orders_approval() {
        $sum = get_notif_1() + get_notif_2() + get_notif_3() + get_notif_4() + get_notif_5();
        return $sum;
    }
}

