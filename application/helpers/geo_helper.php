<?php

if (!function_exists('get_geocity')) {

    function get_geocity($latlang = '') {

        $city = '';

        if ($latlang == '') {
            return $city;
        }

        $geocode = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=$latlang&sensor=false");
        $output = json_decode($geocode);

        if ($output->status == 'OK') {
            for ($j = 0; $j < count($output->results[0]->address_components); $j++) {
                if ($output->results[0]->address_components[$j]->types[0] == 'administrative_area_level_2') {
                    $city = $output->results[0]->address_components[$j]->long_name;
                }
            }
        }

        return $city;
    }

}

if (!function_exists('get_geocity_bycurl')) {

    function get_geocity_bycurl($latlang = '') {

        $city = '';

        if ($latlang == '')
            return $city;

        $url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCb1FgTwIwGXNwbd_hKVG2T0MvaM_ZjrZA&v=3&latlng=$latlang&sensor=false";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $contents = curl_exec($ch);
        if (curl_errno($ch)) {
            echo curl_error($ch);
            echo "\n<br />";
            $contents = '';
        } else {
            curl_close($ch);
        }

        if (!is_string($contents) || !strlen($contents)) {
            $contents = '';
        }

        $output = json_decode($contents);

        if ($output->status == 'OK') {
            for ($j = 0; $j < count($output->results[0]->address_components); $j++) {
                if ($output->results[0]->address_components[$j]->types[0] == 'administrative_area_level_2') {
                    $city = $output->results[0]->address_components[$j]->long_name;
                }
            }
        }

        return $city;        
        
    }

}