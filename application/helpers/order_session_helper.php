<?php

/** Backend Alert */
/** ------------------------------------------------------------------------------------------------------------------------- */
if (!function_exists('set_sessorder')) {

    function set_sessorder($order_id = '') {
        $self = & get_instance();
        if ($order_id != '') {
            $self->session->set_userdata('order_lock', $order_id);
        }
    }

}

if (!function_exists('get_sessorder')) {

    function get_sessorder() {
        $self = & get_instance();
        return $self->session->userdata('order_lock');
    }

}

if (!function_exists('destroy_sessorder')) {

    function destroy_sessorder() {
        $self = & get_instance();
        return $self->session->unset_userdata('order_lock');
    }

}

if (!function_exists('has_sessorder')) {

    function has_sessorder() {
        $self = & get_instance();
        $self->load->model('orders_model');

        $data = $self->orders_model->get([
                    'order_id' => get_sessorder(),
                    'order_status' => -1
                ])->result();

        if (count($data) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

}

if (!function_exists('blank_sessorder')) {

    function blank_sessorder() {
        $self = & get_instance();
        $self->load->model('orders_model');
        $self->orders_model->destroy_order_session(get_sessorder());
        destroy_sessorder();
        
    }

}
?>