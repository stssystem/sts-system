<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_tax_detail_model extends CI_Model {

    private $table = 'orders';

    public function __construct() {
        parent::__construct();
    }

    public function get_data($order_origin = '', $attr = array(), $limit = 10, $index = 0, $search = '') {

        $this->select();
        $this->join($order_origin);

        if (key_exists('order', $attr)) {
            $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']);
        } else {
            $this->db->order_by('orders.order_date', 'DESC');
        }

        $this->db->limit($limit, $index);

        return $this->db->get($this->table);
    }

    public function get_download($order_origin = '') {

        $this->db->select('orders.order_date AS `Tanggal Resi`', false);
        $this->db->select('orders.order_id AS `Nomor Resi`', false);
        $this->db->select('customers.customer_name AS `Nama Pelanggan`', false);
        $this->db->select('branches.branch_name AS `Cabang`', false);
        $this->db->select('ROUND(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total - (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))))  ELSE orders.order_sell_total  END) AS DPP');
        $this->db->select('ROUND(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))) ELSE (orders.order_sell_total * (1 / 100))  END) AS PPN');
        $this->db->select('ROUND(CASE orders.order_use_tax WHEN 1 THEN orders.order_sell_total  ELSE orders.order_sell_total + (orders.order_sell_total * (1 / 100)) END) AS Total');

        $this->join($order_origin);
        $this->db->order_by('orders.order_date', 'DESC');

        return $this->db->get($this->table);
    }

    public function get_total($order_origin = '', $search = '') {
        $this->db->select('count(*) AS counter');
        $this->join($order_origin);
        $result = $this->db->get($this->table)->row_array();
        return $result['counter'];
    }

    public function get_sum($order_origin = '') {
        $this->db->select('SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total - (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))))  ELSE orders.order_sell_total  END) AS dpp');
        $this->db->select('SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))) ELSE (orders.order_sell_total * (1 / 100))  END) AS ppn');
        $this->db->select('SUM(CASE orders.order_use_tax WHEN 1 THEN orders.order_sell_total  ELSE orders.order_sell_total + (orders.order_sell_total * (1 / 100)) END) AS total');
        $this->join($order_origin);
        return $this->db->get($this->table)->row_array();
    }

    private function select() {
        $this->db->select('orders.order_date AS order_date', false);
        $this->db->select('orders.order_id AS order_id', false);
        $this->db->select('customers.customer_name AS customer_name', false);
        $this->db->select('branches.branch_name AS branch_name', false);
        $this->db->select('(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total - (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))))  ELSE orders.order_sell_total  END) AS dpp');
        $this->db->select('(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))) ELSE (orders.order_sell_total * (1 / 100))  END) AS ppn');
        $this->db->select('(CASE orders.order_use_tax WHEN 1 THEN orders.order_sell_total  ELSE orders.order_sell_total + (orders.order_sell_total * (1 / 100)) END) AS total');
    }

    private function join($order_origin = '') {
        $this->db->join('branches', 'orders.order_destination = branches.branch_id', 'left');
        $this->db->join('customers', 'orders.customer_id = customers.customer_id');
        $this->db->where('orders.order_origin', $order_origin);
        $this->db->where('orders.order_status >=', '-1');
        $this->default_date();
        $this->session->userdata('tax_report_detail_filter') != '' ? $this->session_filter() : false;
    }

    public function default_date() {
        if (!isset($this->session->userdata('tax_report_detail_filter')['date_start']) && !isset($this->session->userdata('tax_report_detail_filter')['date_end'])) {
            $this->db->where('orders.order_date >=', strtotime(date('01-m-Y')));
            $this->db->where('orders.order_date <=', strtotime(date('t-m-Y')));
        }
    }

    public function session_filter() {

        if (isset($this->session->userdata('tax_report_detail_filter')['date_start']) && isset($this->session->userdata('tax_report_detail_filter')['date_end'])) {
            $start = $this->session->userdata('tax_report_detail_filter')['date_start'];
            $end = $this->session->userdata('tax_report_detail_filter')['date_end'];
            $this->db->where('orders.order_date >=', strtotime($start));
            $this->db->where('orders.order_date <=', strtotime($end));
        }

        if (isset($this->session->userdata('tax_report_detail_filter')['order_origin'])) {
            $order_origin = $this->session->userdata('tax_report_detail_filter')['order_origin'];
            $this->db->where('orders.order_origin', $order_origin);
        }
    }

    private function order($column = '', $mode = '') {

        switch ($column) {
            case 0:
                $this->db->order_by('order_date', $mode);
                break;
            case 1:
                $this->db->order_by('order_id', $mode);
                break;
            case 2:
                $this->db->order_by('customer_name', $mode);
                break;
            case 3:
                $this->db->order_by('branch_name', $mode);
                break;
            case 4:
                $this->db->order_by('dpp', $mode);
                break;
            case 5:
                $this->db->order_by('ppn', $mode);
                break;
            case 6:
                $this->db->order_by('total', $mode);
                break;
        }
    }

}
