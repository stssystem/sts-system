<?php

class Report_logistic_deliveries_1_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '') {

        $this->db->select('*');
        $this->db->join('orders', 'orders.order_id = report_logistic_deliveries_1_detail.order_id');

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('deliveries_1') != '' ? $this->session_filter() : false;

        key_exists('order', $attr) ? $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']) : false;

        $this->sql_base($attr);

        $this->db->limit($limit, $index);
        
        return $this->db->get('report_logistic_deliveries_1_detail');
        
    }

    public function get_total($attr = array(), $search = '') {

        $this->db->select('count(report_logistic_deliveries_1_detail.order_id) AS total');
        $this->db->join('orders', 'orders.order_id = report_logistic_deliveries_1_detail.order_id');

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('deliveries_1') != '' ? $this->session_filter() : false;

        $this->sql_base($attr);
        
        $data = $this->db->get('report_logistic_deliveries_1_detail')->row();

        return $data->total;
        
    }

    private function search($search = '') {

        $str = 'CONCAT('
        . 'report_logistic_deliveries_1_detail.order_id, " ", '
        . 'from_unixtime(orders.created_at, "%d %b %Y %h:%i:%s")'
        . ')';

        $this->db->like($str, $search);
        
    }
    
    private function sql_base($attr = array()){
        if(key_exists('branch_id', $attr)){
            $this->db->where('branch_id_origin', $attr['branch_id']);
        }
    }

    public function session_filter() {

        if ($this->session->userdata('deliveries_1')['date_start'] && $this->session->userdata('deliveries_1')['date_end']) {
            $start  = $this->session->userdata('deliveries_1')['date_start'];
            $end    = $this->session->userdata('deliveries_1')['date_end'];
            $this->db->where('orders.created_at >=', strtotime($start));
            $this->db->where('orders.created_at <=', strtotime($end));
        }

    }

    public function order($column = 0, $mode = 'desc') {

        switch ($column) {
            case 1 :
            $this->db->order_by('report_logistic_deliveries_1_detail.created_at', $mode);
            break;
            case 2 :
            $this->db->order_by('report_logistic_deliveries_1_detail.order_id', $mode);
            break;
        }

    }

}