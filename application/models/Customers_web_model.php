<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_web_model extends CI_Model {

    private $table = 'wp_users';
    private $id = 'ID';
    private $db;

    public function __construct() {
        parent::__construct();
        $this->db = db_connect('web');
    }

    public function get($where_array = array(), $attributes = array(), $branch_based = false) {

        $this->db->where($where_array);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        $branch_based != false ? $this->branch_based($branch_based) : true;

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_joined($where_array = array(), $attributes = array(), $branch_based = false) {

        $this->db->select(''
                . 'wp_users.ID as user_id, '
                . 'usermeta.first_name AS fullname, '
                . 'wp_users.user_nicename AS user_pass, '
                . 'wp_users.user_email AS user_email, '
                . 'wp_users.user_status, '
                . 'wp_users.user_registered AS created_at'
                . '');

        $this->db->where($where_array);

        $this->sql_base();

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        // Datatable order 
        key_exists('dt_order', $attributes) != '' ? $this->order(
                                $attributes['dt_order']['0']['column'], $attributes['dt_order']['0']['dir']
                        ) : false;

        // Search query
        key_exists('search', $attributes) != '' ? $this->search($attributes['search']) : false;

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_total($search = '') {

        $this->db->select('count(*) As count');

        $this->sql_base();

        // Search query
        $search != '' ? $this->search($search) : false;

        $result = $this->db->get($this->table)->row();

        return $result->count;
    }

    private function sql_base() {
        $this->db->join('wp_wsluserscontacts', 'wp_users.ID = wp_wsluserscontacts.user_id', 'left');
        $this->db->join('( '
                . 'SELECT '
                . 'wp_usermeta.user_id AS user_id, '
                . 'wp_usermeta.meta_value AS first_name '
                . 'FROM wp_usermeta '
                . 'WHERE meta_key = "first_name" '
                . ') AS usermeta', 'usermeta.user_id = wp_users.ID', 'left');
    }

    private function search($search = '') {

        $str = 'CONCAT('
                . 'wp_users.ID, " ", '
                . 'usermeta.first_name, " ", '
                . 'wp_users.user_pass, " ", '
                . 'wp_users.user_email, " ", '
                . 'wp_users.user_status, " ", '
                . 'DATE_FORMAT(wp_users.user_registered, "%d %M %Y - %H:%i:%s")'
                . ')';

        $this->db->like($str, $search);
    }

    private function order($column = 0, $mode = 'desc') {

        switch ($column) {
            case 0 :
                $this->db->order_by('wp_users.ID', $mode);
                break;
            case 1 :
                $this->db->order_by('usermeta.first_name', $mode);
                break;
            case 2 :
                $this->db->order_by('wp_users.user_nicename', $mode);
                break;
            case 3 :
                $this->db->order_by('wp_users.user_email', $mode);
                break;
            case 4 :
                $this->db->order_by('wp_users.user_registered', $mode);
                break;
        }
    }

    public function get_row($where_array = array(), $attributes = array()) {

        $str = 'wp_ss_pick_up_order.*, '
                . 'kbp_asal.Nama as kbp_asal_nama, '
                . 'kbp_tujuan.Nama as kbp_tujuan_nama, '
                . 'kbp_asal.IDKabupaten as kbp_asal_id, '
                . 'kbp_tujuan.IDKabupaten as kbp_tujuan_id ';

        $this->db->select($str);

        $this->join_kabupaten();

        $this->db->where($where_array);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        $query = $this->db->get($this->table);

        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function destroy($id = '') {

        $this->db->where($this->id, $id);
        $data = $this->db->get($this->table)->row();

        $this->db->where($this->id, $id);
        $this->db->delete($this->table);

        return $data->display_name;
    }

    public function branch_based($city_name = 0) {
        if (substr($city_name, 0, 4) == "KOTA") {
            $city_name = substr($city_name, 5);
        }
        $this->db->where("( kbp_asal.Nama like '%$city_name%' )");
    }

    public function join_kabupaten() {
        $this->db->join('wp_ss_cargo_kabupaten as c_kbp_asal', 'wp_ss_pick_up_order.asal = c_kbp_asal.Nama');
        $this->db->join('wp_ss_cargo_kabupaten as c_kbp_tujuan', 'wp_ss_pick_up_order.tujuan = c_kbp_tujuan.Nama');
        $this->db->join('wp_ss_kabupaten as kbp_asal', 'c_kbp_asal.IDKabupaten = kbp_asal.IDKabupaten');
        $this->db->join('wp_ss_kabupaten as kbp_tujuan', 'c_kbp_tujuan.IDKabupaten = kbp_tujuan.IDKabupaten');
    }

    public function join_kecamatan() {
        $this->db->join('wp_ss_kecamatan as kcm_asal', 'wp_ss_pick_up_order.kecamatan_asal = kcm_asal.Nama');
        $this->db->join('wp_ss_kecamatan as kcm_tujuan', 'wp_ss_pick_up_order.kecamatan_tujuan = kcm_tujuan.Nama');
        $this->db->join('wp_ss_kelurahan as klh_asal', 'wp_ss_pick_up_order.kelurahan_asal = klh_asal.Nama');
        $this->db->join('wp_ss_kelurahan as klh_tujuan', 'wp_ss_pick_up_order.kelurahan_tujuan = klh_tujuan.Nama');
    }

    public function str_select() {
        $str = 'wp_ss_pick_up_order.*, '
                . 'kbp_asal.Nama as kbp_asal_nama, '
                . 'kbp_tujuan.Nama as kbp_tujuan_nama,'
                . 'kcm_asal.IDKabupaten as kbp_asal_id, '
                . 'kcm_tujuan.IDKabupaten as kbp_tujuan_id, '
                . 'kcm_asal.IDKecamatan as kcm_asal_id, '
                . 'kcm_tujuan.IDKecamatan as kcm_tujuan_id, '
                . 'klh_asal.IDKelurahan as klh_asal_id, '
                . 'klh_tujuan.IDKelurahan as klh_tujuan_id';
        return $str;
    }

    public function get_id($no_resi) {
        return $this->get(['no_resi' => $no_resi])->row()->id;
    }

    public function update_pickup_status($id, $status) {
        $return = $this->get(['id' => $id])->row();
        $this->update([
            'id' => $id,
            'pickup_status' => $status
        ]);
        return $return;
    }

    public function update_password($id = null, $new_pass = '', $confirm_pass = '') {

        if ($new_pass == '' && $confirm_pass == '') {
            return true;
        }

        if ($new_pass == $confirm_pass) {

            $this->update([
                'ID' => $id,
                'user_pass' => md5($new_pass)
            ]);

            return true;
        } else {

            return false;
        }
    }

}
