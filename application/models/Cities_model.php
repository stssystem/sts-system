<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cities_model extends CI_Model {

    private $table = 'cities';
    private $id = 'city_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attr = array(), $attributes = array()) {

        if (!empty($where_array)) {
            $this->db->where($where_array);
        }
        $this->db->where('deleted_at', 0);

        $this->session->userdata('cities') != '' ? $this->session_filter() : false;
        
        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        if (key_exists('order', $attr)) {
            $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']);
        }

        $query = $this->db->get($this->table);
        return $query;
        
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);
        return $this->db->get($this->table)->row();
    }

    public function get_cities_from_city($city_id) {
        $province_id = $this->get_province_id($city_id);
        $this->db->where('province_id', $province_id);
        return $this->db->get($this->table)->result_array();
    }

    public function get_province_id($city_id) {
        $result = $this->get(['city_id' => $city_id])->row();
        return isset($result->province_id) ? $this->get(['city_id' => $city_id])->row() : NULL;
    }

    public function get_available($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);
        $this->db->where('provinces.show', 1);
        $this->db->join('provinces', 'cities.province_id = provinces.province_id');
        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function get_name($id = null) {
        $result = $this->get([
                    'city_id' => $id
                ])->row();
        return isset($result->city_name) ? $result->city_name : '';
    }

    public function get_city_total($where_array = array(), $attributes = array()) {

        if (!empty($where_array)) {
            $this->db->where($where_array);
        }
        $this->db->where('deleted_at', 0);

        $this->session->userdata('cities') != '' ? $this->session_filter() : false;

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    private function concat_like($data = '') {

        $str = 'CONCAT'
                . '(city_id, " ", '
                . 'city_name, " ", '
                . 'from_unixtime(created_at, "%d %b %Y %h:%i:%s")'
                . ')';

        $this->db->like($str, $data);
    }

    private function order($column = 0, $mode = 'desc') {
        switch ($column) {
            case 0 :
                $this->db->order_by('city_id', $mode);
                break;
            case 1 :
                $this->db->order_by('city_name', $mode);
                break;
            case 2 :
                $this->db->order_by('created_at', $mode);
                break;
        }
    }

}
