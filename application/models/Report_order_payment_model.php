<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_order_payment_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attr = array(), $attributes = array(), $column_export = array(), $date_start = '', $date_end = '') {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        }

        // Base sql commonly used
        $this->base_sql();

        if (!empty($date_start) && !empty($date_end)) {
            $this->db->where('order_payment_report.order_date >=', strtotime($date_start));
            $this->db->where('order_payment_report.order_date <=', strtotime($date_end));
        }

        if (!empty($date_start)) {
            $this->db->where('order_payment_report.order_date >=', strtotime($date_start));
        }

        if (!empty($date_end)) {
            $this->db->where('order_payment_report.order_date <=', strtotime($date_end));
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        if (key_exists('order', $attr)) {
            $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']);
        }

        return $this->db->get('order_payment_report');
    }

    public function get_total($where_array = array(), $attributes = array()) {

        $this->session->userdata('report_filter_order_payment') != '' ? $this->session_filter() : false;

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        // Base sql commonly used
        $this->base_sql();

        $this->db->from('order_payment_report');

        return $this->db->count_all_results();
    }

    private function concat_like($data = '') {

        $str = 'CONCAT('
                . 'order_payment_report.order_id, " ", '
                . 'order_payment_report.customer_name, " ", '
                . 'order_payment_report.branch_origin_name, " ", '
                . 'order_payment_report.branch_destination_name, " ", '
                . 'CASE order_payment_report.order_payment_type '
                . 'WHEN 0 THEN "null"  '
                . 'WHEN 1 THEN "Cash / Tunai"  '
                . 'WHEN 2 THEN "Franko"  '
                . 'WHEN 3 THEN "Tunai Tempo"  '
                . 'WHEN 4 THEN "Tagis Tempo"  '
                . 'END , " ", '
                . 'from_unixtime(order_payment_report.order_date, "%d %b %Y %h:%i:%s")'
                . ')';

        $this->db->like($str, $data);
    }

    private function base_sql() {

        // Join to other table
        $this->db->join(''
                . '(SELECT '
                . 'orders.order_payment_status AS order_payment_status, '
                . 'orders.order_id AS temp_order_id '
                . 'FROM orders) AS orders'
                , 'orders.temp_order_id = order_payment_report.order_id');

        // Default date
        $this->default_date();

        // Filter 
        $this->session->userdata('report_filter_order_payment') != '' ? $this->session_filter() : false;
    }

    private function default_date() {
        if ((!$this->session->userdata('report_filter_order_payment')['date_start'] || $this->session->userdata('report_filter_order_payment')['date_start'] == '') && (!$this->session->userdata('report_filter_order_payment')['date_end'] || $this->session->userdata('report_filter_order_payment')['date_end'] == '')) {
            $this->db->where('order_payment_report.order_date >=', strtotime(date('01-m-Y')));
            $this->db->where('order_payment_report.order_date <=', strtotime(date('t-m-Y')));
        }
    }

    public function session_filter() {

        if ($this->session->userdata('report_filter_order_payment')['date_start'] && $this->session->userdata('report_filter_order_payment')['date_end']) {
            $start = $this->session->userdata('report_filter_order_payment')['date_start'];
            $end = $this->session->userdata('report_filter_order_payment')['date_end'];
            $this->db->where('order_payment_report.order_date >=', strtotime(str_replace('/', '-', $start)));
            $this->db->where('order_payment_report.order_date <=', strtotime(str_replace('/', '-', $end)));
        }

        if ($this->session->userdata('report_filter_order_payment')['branch_origin_name'] && $this->session->userdata('report_filter_order_payment')['branch_origin_name'] != '') {
            $origin = $this->session->userdata('report_filter_order_payment')['branch_origin_name'];
            $this->db->where('order_payment_report.branch_origin_name', $origin);
        }

        if ($this->session->userdata('report_filter_order_payment')['branch_destination_name'] && $this->session->userdata('report_filter_order_payment')['branch_destination_name'] != '') {
            $destination = $this->session->userdata('report_filter_order_payment')['branch_destination_name'];
            $this->db->where('order_payment_report.branch_destination_name', $destination);
        }

        if ($this->session->userdata('report_filter_order_payment')['type_filter'] && $this->session->userdata('report_filter_order_payment')['type_filter'] != '') {
            $type = $this->session->userdata('report_filter_order_payment')['type_filter'];
            $this->db->where('order_payment_report.order_payment_type', $type);
        }

        if ($this->session->userdata('report_filter_order_payment')['status_filter'] && $this->session->userdata('report_filter_order_payment')['status_filter'] != '') {
            $status = $this->session->userdata('report_filter_order_payment')['status_filter'];
            if ($status == 1) {
                $this->db->where('order_payment_report.order_payment_type', 1);
            } else {
                $this->db->where('order_payment_report.order_payment_type !=', 1);
            }
        }

        if ($this->session->userdata('report_filter_order_payment')['due_date']) {
            $due_date = $this->session->userdata('report_filter_order_payment')['due_date'];
            $this->db->where('order_payment_report.orders_due_date', strtotime(str_replace('/', '-', $due_date)));
        }
    }

    public function order($column = 0, $mode = 'desc') {

        switch ($column) {
            case 0 :
                $this->db->order_by('order_payment_report.order_date', $mode);
                break;
            case 1 :
                $this->db->order_by('order_payment_report.order_id', $mode);
                break;
            case 2 :
                $this->db->order_by('order_payment_report.customer_name', $mode);
                break;
            case 3 :
                $this->db->order_by('order_payment_report.branch_origin_name', $mode);
                break;
            case 4 :
                $this->db->order_by('order_payment_report.branch_destination_name', $mode);
                break;
            case 5 :
                $this->db->order_by('order_payment_report.order_payment_type', $mode);
                break;
            case 6 :
                $this->db->order_by('order_payment_report.order_payment_type', $mode);
                break;
            case 7:
                $this->db->order_by('(order_payment_report.orders_due_date)', $mode);
                break;
        }
    }

    public function download($column_export = array()) {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {

            $this->db->select(''
                    . 'FROM_UNIXTIME(order_payment_report.order_date, "%d %b %y") AS `Tanggal`, '
                    . 'order_payment_report.order_id AS `Nomor Resi`, '
                    . 'order_payment_report.customer_name AS `Nama Pelanggan`, '
                    . 'order_payment_report.branch_origin_name AS `Cabang Asal`, '
                    . 'order_payment_report.branch_destination_name AS `Cabang Tujuan`, '
                    . '(CASE order_payment_report.order_payment_type '
                    . 'WHEN 1 THEN "Cash / Tunai"  '
                    . 'WHEN 2 THEN "Franko"  '
                    . 'WHEN 3 THEN "Tunai Tempo"  '
                    . 'WHEN 4 THEN "Tagis Tempo"  '
                    . 'END) AS `Sistem Pembayaran`, '
                    . '(CASE orders.order_payment_status '
                    . 'WHEN 0 THEN "Belum Lunas"  '
                    . 'WHEN 1 THEN "Lunas"  '
                    . 'END) AS `Status Pembayaran`, '
                    . 'FROM_UNIXTIME(order_payment_report.orders_due_date, "%d %b %y")  AS `Tanggal Jatuh Tempo`'
                    . '');
        }

        // Base sql commonly used
        $this->base_sql();

        return $this->db->get('order_payment_report');
    }

}
