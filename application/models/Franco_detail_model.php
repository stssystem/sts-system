<?php

class Franco_detail_model extends CI_Model {

    private $table = 'franco_detail';
    private $id = 'franco_detail_id';

    public function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function update($franco_detail_id = 0, $data = array()){
        $this->db->where($this->id, $franco_detail_id);
        $this->db->update($this->table, $data);
        return true;
    }

    public function check_franco_exist($franco_id = 0, $status = 0) {

        $this->db->select('franco_detail_id');

        $this->db->where('franco_id', $franco_id);
        $this->db->where('status', $status);

        $data = $this->db->get($this->table)->row();

        return isset($data->franco_detail_id) ? $data->franco_detail_id : null;
        
    }

}
