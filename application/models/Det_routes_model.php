<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Det_routes_model extends CI_Model {

    private $table = 'det_routes';
    private $id = 'det_route_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array(), $batch = false) {
        if ($batch == false) {
            $this->db->insert($this->table, $set_array);
        } else {
            $this->db->insert_batch($this->table, $set_array);
        }
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array(), $field = false) {

        if ($field == false) {
            $this->db->where($this->id, $set_array[$this->id]);
            $result = $this->db->get($this->table)->row();

            $this->db->where($field, $id);
            $this->db->delete($this->table);
        } else {

            $this->db->where($set_array);
            $this->db->delete($this->table);
            $result=NULL;
        }



        return $result;
    }

    public function get_cities($where_array = array()) {
        $array = $this->get($where_array)->result();

        $result = [];
        $i = 1;
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                if ($key == 0) {
                    $result[0]['city_id'] = $value->route_city_start;
                }
                $result[$key + 1]['city_id'] = $value->route_city_end;
            }
        }

        return $result;
    }


}
