<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Locations_update_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('armadas_model');
        $this->load->model('branches_model');
        $this->load->model('orders_locations_model');
        $this->load->model('armada_trips_model');
    }

    public function update($armada_id = '', $branch_id = '') {

        /** Get geo locations branch by id branch */
        $geo_branch = $this->branches_model->get_geobranch($branch_id);

        /** Update last location in armada   */
        $this->armadas_model->update_last_location($armada_id, $geo_branch);

        /** Get order loaded in the armada */
        $order_loaded = $this->armada_trips_model->get_order_loaded($armada_id);

        /** Update last location in order */
        $this->orders_locations_model->update_last_locations($order_loaded, $geo_branch);

        /** Update last locations in packages */
        $this->armada_trips_model->get_package_loaded($armada_id, $geo_branch);

        return true;
    }

    public function update_last_location($armada_id, $latlang) {
        
        /** Get order loaded in the armada */
        $order_loaded = $this->armada_trips_model->get_order_loaded($armada_id);

        /** Update last location in order */
        $this->orders_locations_model->update_last_locations($order_loaded, $latlang, '', '', 'Dalam Perjalanan');
        
    }

}
