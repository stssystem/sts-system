<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_meta_model extends CI_Model {

    private $table = 'wp_usermeta';
    private $id = 'user_id';
    private $db;

    public function __construct() {
        parent::__construct();
        $this->db = db_connect('web');
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        $query = $this->db->get($this->table);

        return $query;
    }
    
    public function get_profile($id = null) {
        
        $data = $this->get([$this->id => $id])->result();

        $result = [];
        if (!empty($data)) {
            foreach ($data as $key => $item) {
                $result[$item->meta_key] = $item->meta_value;
            }
        }
        
        return $result;
        
    }
    
    public function destroy($id = null) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        return true;
    }
    
    public function update($id = null, $meta_key ='', $meta_value = '') {
        $this->db->where($this->id, $id);
        $this->db->where('meta_key', $meta_key);
        $this->db->update($this->table, ['meta_value' => $meta_value]);
        return true;
    }
    
    public function insert($id = null, $meta_key = '', $meta_value = ''){
        
        $data = [
            'user_id' => $id,
            'meta_key' => $meta_key,
            'meta_value' => $meta_value
        ];
        
        $this->db->insert($this->table, $data);
        
    }
    
    public function is_meta_exist($id = null, $meta_key = ''){
        
        $this->db->where($this->id, $id);
        $this->db->where('meta_key', $meta_key);
        $this->db->from($this->table);
        
        $count = $this->db->count_all_results();
        
        return $count > 0 ? true : false;
        
    }
    
    public function update_profile($id = null, $meta = array()){
        
        if(!empty($meta)){
            
            foreach($meta as $key => $item){
                if($this->is_meta_exist($id, $key)){
                    $this->update($id, $key, $item);
                }else{
                    $this->insert($id, $key, $item);
                }
            }
            
            return true;
            
        }else{
            
            return false;
            
        }
    
        
    }

}
