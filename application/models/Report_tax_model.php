<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_tax_model extends CI_Model {

    private $table = 'orders';

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '') {

        $this->select();
        $this->join();
        $this->db->group_by("order_origin");

        // Do order when order key in attribute parameter not empty
        if (key_exists('order', $attr))
            $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']);

        // Do searching when search paramater is not empty
        if ($search != '')
            $this->search($search);

        $this->db->limit($limit, $index);

        return $this->db->get($this->table);
    }

    public function get_download() {

        $this->db->select('branches.branch_name AS `Cabang`', false);
        $this->db->select("FROM_UNIXTIME(orders.order_date, '%d %m %Y') AS `Tanggal Resi`", false);
        $this->db->select('ROUND(SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total - (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))))  ELSE orders.order_sell_total  END)) AS DPP');
        $this->db->select('ROUND(SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))) ELSE (orders.order_sell_total * (1 / 100))  END)) AS PPN');
        $this->db->select('ROUND(SUM(CASE orders.order_use_tax WHEN 1 THEN orders.order_sell_total  ELSE orders.order_sell_total + (orders.order_sell_total * (1 / 100)) END)) AS Total');

        $this->join();
        $this->db->group_by("order_origin");
        $this->db->order_by('branches.branch_name', 'ASC');

        return $this->db->get($this->table);
    }

    // Still fixing 17 - 09  - 2016
    public function get_total($search = '') {

        $result = $this->db->query("SELECT count(*) AS c FROM ({$this->sql_total($search)}) AS r ")->row_array();

        return $result['c'];
        
    }

    private function sql_total($search = '') {
        $this->db->select('count(branches.branch_id) AS counter');
        $this->join();
        if ($search != '')
            $this->db->like('branches.branch_name', $search);
        $this->db->group_by("order_origin");
        return $this->db->get_compiled_select($this->table);
    }

    public function get_sum() {
        $this->db->select('SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total - (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))))  ELSE orders.order_sell_total  END) AS dpp');
        $this->db->select('SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))) ELSE (orders.order_sell_total * (1 / 100))  END) AS ppn');
        $this->db->select('SUM(CASE orders.order_use_tax WHEN 1 THEN orders.order_sell_total  ELSE orders.order_sell_total + (orders.order_sell_total * (1 / 100)) END) AS total');
        $this->join();
        return $this->db->get($this->table)->row_array();
    }

    private function select() {
        $this->db->select('orders.order_origin AS order_origin', false);
        $this->db->select('branches.branch_id AS branch_id', false);
        $this->db->select('branches.branch_name AS branch', false);
        $this->db->select("FROM_UNIXTIME(orders.order_date, '%d %m %Y') AS order_date", false);
        $this->db->select('SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total - (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))))  ELSE orders.order_sell_total  END) AS dpp');
        $this->db->select('SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))) ELSE (orders.order_sell_total * (1 / 100))  END) AS ppn');
        $this->db->select('SUM(CASE orders.order_use_tax WHEN 1 THEN orders.order_sell_total  ELSE orders.order_sell_total + (orders.order_sell_total * (1 / 100)) END) AS total');
    }

    private function join() {

        $this->db->join('branches', 'orders.order_origin = branches.branch_id', 'left');
        $this->db->where('orders.order_status >=', '-1');
        $this->db->where('branches.branch_name !=', NULL);



        $this->default_date();
        $this->session->userdata('tax_report_filter') != '' ? $this->session_filter() : false;
    }

    public function search($search = '') {
        $this->db->like('CONCAT(branches.branch_name)', $search);
    }

    public function order($column = '', $mode = '') {

        switch ($column) {
            case 0:
                $this->db->order_by('branches.branch_name', $mode);
                break;
            case 1:
                $this->db->order_by('dpp', $mode);
                break;
            case 2:
                $this->db->order_by('ppn', $mode);
                break;
            case 3:
                $this->db->order_by('total', $mode);
                break;
        }
    }

    public function default_date() {
        if (!$this->session->userdata('tax_report_filter')['date_start'] && !$this->session->userdata('tax_report_filter')['date_end']) {
            $this->db->where('orders.order_date >=', strtotime(date('01-m-Y')));
            $this->db->where('orders.order_date <=', strtotime(date('t-m-Y')));
        }
    }

    public function session_filter() {
        if ($this->session->userdata('tax_report_filter')['date_start'] && $this->session->userdata('tax_report_filter')['date_end']) {
            $start = $this->session->userdata('tax_report_filter')['date_start'];
            $end = $this->session->userdata('tax_report_filter')['date_end'];
            $this->db->where('orders.order_date >=', strtotime($start));
            $this->db->where('orders.order_date <=', strtotime($end));
        }
    }

}
