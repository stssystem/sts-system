<?php

class Franco_model extends CI_Model {

    private $table = 'franco';
    private $id = 'franco_id';

    public function __construct() {
        parent::__construct();
    }

    public function insert($data = array()) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($franco_id = 0, $data = array()) {
        $this->db->where($this->id, $franco_id);
        $this->db->update($this->table, $data);
        return true;
    }

    public function create($order_id = 0, $status = 1) {

        // Get all reouserce needed
        $this->load->model('orders_model');

        // Get order detail
        $orders = $this->orders_model->get_joined(
                        ['orders.order_id' => $order_id]
                )->row();
        
        // Insert data for franco 
        $data = array(
            'order_id' => $order_id,
            'manual_resi' => $orders->order_manual_id,
            'costumer_id' => $orders->customer_id,
            'origin' => $orders->order_origin,
            'destination' => $orders->order_destination,
            'status' => $status,
            'created_at' => time()
        );
        
        return $this->insert($data);
        
    }

    public function check_order_exist($order_id = 0, $status = 0) {

        $this->db->select("franco_id");

        $this->db->where('order_id', $order_id);

        if ($status != 0)
            $this->db->where('status', $status);

        $data = $this->db->get($this->table)->row();

        return isset($data->franco_id) ? $data->franco_id : null;
    }

}
