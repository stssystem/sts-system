<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Costumers_model extends CI_Model {

    private $table = 'customers';
    private $id = 'customer_id';
    private $regional = array();
    private $regional_id = 0;

    public function __construct() {
        parent::__construct();

        /** Generate branch of regional */
        $this->load->model('regionals_model', 'rm');
        $branch_id = $this->rm->get_branches_id(profile()->regional_id);
        if (!empty($branch_id)) {
            foreach ($branch_id as $key => $item) {
                $this->regional[] = $item['branch_id'];
            }
        }
        $this->regional_id = profile()->regional_id;
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('customers.deleted_at', 0);
        $this->db->join('branches', 'customers.branch_created = branches.branch_id', 'left');
        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $result = $this->db->get($this->table)->row();

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);

        return $result;
    }

    public function get_name_code($where_array = array(), $attributes = array()) {

        $this->db->select('customers.*, CONCAT(customers.customer_name, " ", customers.customer_code) AS name_code');
        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Do when limit key availabe */
        if (key_exists('like', $attributes)) {
            $this->db->like('CONCAT(customers.customer_name, " ", customers.customer_code)', $attributes['like']);
        }

        $query = $this->db->get($this->table);
        return $query;
        
    }

    public function get_joined($where_array = array(), $attr = array(), $attributes = array()) {

        $this->db->select('customers.created_at as customer_created_at, customers.*');

        $this->sql_joined($where_array);

        $this->session->userdata('customers') != '' ? $this->session_filter() : false;

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        if (key_exists('order', $attr)) {
            $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']);
        }

        // $this->db->limit($limit, $index);

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_total($where_array = array(), $attributes = array()) {

        $this->sql_joined($where_array);

        $this->session->userdata('customers') != '' ? $this->session_filter() : false;

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    public function sql_joined($where_array = array()) {
        $this->db->select('cities.*, customers_origin.*, branches.*');
        $this->db->join('cities', 'customers.customer_city = cities.city_id', 'left');
        $this->db->join('customers_origin', 'customers.customer_id = customers_origin.customers_origin_customer_id', 'left');
        $this->db->join('branches', 'customers_origin.customers_origin_branch_id = branches.branch_id', 'left');

        $this->db->where($where_array);
        $this->db->where('customers.deleted_at', 0);

        /** Previlage */
        if (!empty($this->regional) && $this->regional_id != '') {
            $this->db->where_in('customers_origin.customers_origin_branch_id', $this->regional);
        }
    }

    public function get_name($id = null) {
        $result = $this->get([$this->id => $id])->row();
        return isset($result->customer_name) ? $result->customer_name : '';
    }

    private function concat_like($data = '') {

        $str = 'CONCAT'
                . '(customers.customer_id, " ", '
                . 'customers.customer_name, " ", '
                . 'cities.city_name, " ", '
                . 'customers.customer_address, " ", '
                . 'customers.customer_phone, " " '
                . ')';

        $this->db->like($str, $data);
    }

    private function order($column = 0, $mode = 'desc') {
        switch ($column) {
            case 1 :
                $this->db->order_by('customer_id', $mode);
                break;
            case 2 :
                $this->db->order_by('customer_name', $mode);
                break;
            case 3 :
                $this->db->order_by('customer_city', $mode);
                break;
            case 4 :
                $this->db->order_by('customer_address', $mode);
                break;
            case 5 :
                $this->db->order_by('customer_phone', $mode);
                break;
            case 6 :
                $this->db->order_by('pic_first_name', $mode);
                $this->db->order_by('pic_last_name', $mode);
                break;
            case 7 :
                $this->db->order_by('pic_birth_date', $mode);
                break;

        }
    }

    public function session_filter() {

        if ($this->session->userdata('customers')['date_start'] && $this->session->userdata('customers')['date_end']) {
            $start  = $this->session->userdata('customers')['date_start'];
            $end    = $this->session->userdata('customers')['date_end'];
            $this->db->where('customers.pic_birth_date >=', strtotime($start));
            $this->db->where('customers.pic_birth_date <=', strtotime($end));
        }

        if ($this->session->userdata('customers')['customer_name'] && $this->session->userdata('customers')['customer_name'] != '') {
            $customer_name = $this->session->userdata('customers')['customer_name'];
            $this->db->where('customers.customer_name', $customer_name);
        }

        if ($this->session->userdata('customers')['customer_city'] && $this->session->userdata('customers')['customer_city'] != '') {
            $customer_city = $this->session->userdata('customers')['customer_city'];
            $this->db->where('customers.customer_city', $customer_city);
        }

        if ($this->session->userdata('customers')['customer_address'] && $this->session->userdata('customers')['customer_address'] != '') {
            $customer_address = $this->session->userdata('customers')['customer_address'];
            $this->db->like('customers.customer_address', $customer_address);
        }

        if ($this->session->userdata('customers')['pic_religion'] && $this->session->userdata('customers')['pic_religion'] != '') {
            $pic_religion = $this->session->userdata('customers')['pic_religion'];
            $this->db->where('customers.pic_religion', $pic_religion);
        }

        if ($this->session->userdata('customers')['customer_company_type'] && $this->session->userdata('customers')['customer_company_type'] != '') {
            $customer_company_type = $this->session->userdata('customers')['customer_company_type'];
            $this->db->where('customers.customer_company_type', $customer_company_type);
        }

        if ($this->session->userdata('customers')['customer_company_type_of_business'] && $this->session->userdata('customers')['customer_company_type_of_business'] != '') {
            $customer_company_type_of_business = $this->session->userdata('customers')['customer_company_type_of_business'];
            $this->db->where('customers.customer_company_type_of_business', $customer_company_type_of_business);
        }

    }

}
