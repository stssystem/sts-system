<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_origin_model extends CI_Model {

    private $table = 'customers_origin';
    private $id = 'customers_origin_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
    }

    public function update($set_array = array(), $key = null) {
        
        if ($key == null) {
            $this->db->where($this->id, $set_array[$this->id]);
        } else {
            $this->db->where($key, $set_array[$key]);
        }
        
        $this->db->update($this->table, $set_array);
    }

    public function delete($id = 0) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    public function is_exist($array = []) {
        $result = $this->get($array)->num_rows();
        return $result > 0 ? true : false;
    }

    public function store($array = [], $key) {
        if (!$this->is_exist([$key => $array[$key]])) {
            $this->insert($array);
        } else {
            $this->update($array, $key);
        }
    }

}
