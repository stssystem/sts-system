<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_profile_model extends CI_Model {

    private $table = 'userprofile';
    private $id = 'userprofile_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array()) {
        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);
        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function open_branch_access($user_id) {
        $this->db->where('userprofile.user_id', $user_id);
        $this->db->update($this->table, [
            'userprofile.regional_id' => NULL, 'userprofile.branch_id' => NULL
        ]);
    }

    public function get_driver_name($id = 0) {
        $this->db->select('userprofile_fullname');
        $this->db->where('user_id', $id);
        $data = $this->db->get($this->table)->row();
        return isset($data->userprofile_fullname) ? $data->userprofile_fullname : '';
    }

}
