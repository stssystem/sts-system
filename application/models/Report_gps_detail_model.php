<?php

class Report_gps_detail_model extends CI_Model {

    private $table = 'trips';
    private $id = 'trip_id';

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '') {
        $this->select();
        $this->sql_base($attr, $search);
        $this->db->limit($limit, $index);
        $this->db->order_by('trips.created_at', 'desc');
        return $this->db->get($this->table);
    }

    public function get_total($attr = array(), $search = '') {
        $this->db->select('count(trips.trip_id) AS total');
        $this->sql_base($attr, $search);
        $result = $this->db->get($this->table)->row();
        return $result->total;
    }

    public function download($armada_id = 0, $column = []) {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select('trips.trip_id AS `ID`');
            $this->db->select('routes.route_name AS `Asal Tujuan`');
            $this->db->select('driver_profile.userprofile_fullname AS `Pengemudi`');
            $this->db->select('codriver_profile.userprofile_fullname AS `Kenek`');
            $this->db->select('FROM_UNIXTIME(trips.created_at, "%d %M %Y %H:%i:%s") AS `Di Buat`');
            $this->db->select('FROM_UNIXTIME(trips.trip_start_date , "%d %M %Y %H:%i:%s") AS `Waktu Berangkat`');
            $this->db->select('FROM_UNIXTIME(trips.trip_end_date, "%d %M %Y %H:%i:%s") AS `Waktu Tiba`');
        }

        $this->sql_base(['armada_id' => $armada_id]);

        $this->db->order_by('trips.created_at', 'desc');

        return $this->db->get($this->table);
    }

    private function select() {
        $this->db->select('trips.trip_id AS trips_id');
        $this->db->select('trips.created_at AS created_at');
        $this->db->select('trips.trip_start_date AS trip_start_date');
        $this->db->select('trips.trip_end_date AS trip_end_date');
        $this->db->select('driver_profile.userprofile_fullname AS driver_fullname');
        $this->db->select('codriver_profile.userprofile_fullname AS codriver_fullname');
        $this->db->select('armadas.armada_name AS armada_name');
        $this->db->select('routes.route_name AS route_name');
    }

    // Search
    private function search($search = '') {

        // Concat select
        $str = 'CONCAT('
                . 'trips.trip_id, " ", '
                . 'from_unixtime(trips.created_at, "%d %b %Y"), " ",'
                . 'from_unixtime(trips.trip_start_date, "%d %b %Y"), " ",'
                . 'from_unixtime(trips.trip_end_date, "%d %b %Y"), " ",'
                . 'driver_profile.userprofile_fullname, " ", '
                . 'codriver_profile.userprofile_fullname, " ", '
                . 'armadas.armada_name, " ", '
                . 'routes.route_name '
                . ')';

        $this->db->like($str, $search);
    }

    private function sql_base($attr = [], $search = '') {

        $this->db->join('routes', 'trips.route_id = routes.route_id', 'left');
        $this->db->join('armadas', 'trips.armada_id = armadas.armada_id', 'left');

        // Join to get driver
        $this->db->join('user AS driver', 'trips.user_id = driver.user_id', 'left');
        $this->db->join('userprofile AS driver_profile', 'driver.user_id = driver_profile.user_id', 'left');

        // Join to get driver
        $this->db->join('user AS codriver', 'trips.trip_codriver = codriver.user_id', 'left');
        $this->db->join('userprofile AS codriver_profile', 'codriver.user_id = codriver_profile.user_id', 'left');

        // Where deleted 0, and armada selected
        $this->db->where('trips.deleted_at', 0);
        $this->db->where('trips.armada_id', $attr['armada_id']);

        // Search
        if ($search != '') {
            $this->search($search);
        }
    }

}
