<?php

class Franco_list_model extends CI_Model {

    private $table = 'franco';

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '') {

        $str = ''
                . 'franco.franco_id as franco_id, '
                . 'franco.order_id as order_id, '
                . 'customers.customer_name as customer_name, '
                . 'branch_origin.branch_name as origin_branch_name, '
                . 'branch_destination.branch_name as destination_branch_name, '
                . 'franco.status AS status, '
                . '(CASE franco.status '
                . 'WHEN 1 THEN "Belum Dikirim"  '
                . 'WHEN 2 THEN "Belum Dikirim"  '
                . 'WHEN 3 THEN "Sudah Diterima"  '
                . 'WHEN 4 THEN "Sudah Dikirim"  '
                . 'END) AS status_value, '
                . 'from_unixtime(franco.updated_at, "%d %b %Y %h:%i:%s") AS last_update '
                . '';

        $this->db->select($str);

        $this->sql_base($attr, $search);

        if (key_exists('order', $attr)) {
           $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']);
        }

        $this->db->limit($limit, $index);

        return $this->db->get($this->table);
    }

    public function get_total($attr = array(), $search = '') {

        $this->db->select('count(franco.franco_id) AS total');

        $this->sql_base($attr, $search);

        $data = $this->db->get($this->table)->row();

        return $data->total;
    }

    private function sql_base($attr = array(), $search = '') {

        $this->db->join('customers', 'franco.costumer_id = customers.customer_id', 'left');
        $this->db->join('branches as branch_origin', 'franco.origin = branch_origin.branch_id', 'left');
        $this->db->join('branches as branch_destination', 'franco.destination = branch_destination.branch_id', 'left');

        $search != '' ? $this->search($search) : false;

        $this->status_based($attr['status']);

        // Branch and regional previlage 
        if (!key_exists('branch_access', $attr) || $attr['branch_access'] != 1) {
            if (key_exists('regional', $attr) && !empty($attr['regional'])) {
                $this->regional_based($attr['regional'], $attr['status']);
            } else {
                if (key_exists('branch', $attr) && $attr['branch'] != '') {
                    $this->branch_based($attr['branch'], $attr['status']);
                }
            }
        }

        $this->session->userdata('franco_list_filter') != '' ? $this->session_filter($attr['status']) : false;

    }

    private function search($search = '') {

       $str = 'CONCAT('
               . 'franco.franco_id, " ", '
               . 'franco.order_id, " ", '
               . 'customers.customer_name, " - ", '
               . 'branch_origin.branch_name, " - ", '
               . 'branch_destination.branch_name, " - ", '
               . 'franco.status, " - ", '
               . '(CASE franco.status '
               . 'WHEN 1 THEN "Belum Dikirim"  '
               . 'WHEN 2 THEN "Belum Dikirim"  '
               . 'WHEN 3 THEN "Sudah Diterima"  '
               . 'WHEN 4 THEN "Sudah Dikirim"  '
               . 'END), " - ", '
               . 'from_unixtime(franco.updated_at, "%d %b %Y %h:%i:%s") '
               . ')';

       $this->db->like($str, $search);
       
    }

    private function status_based($status) {

        $this->db->group_start();

        if ($status == 1) {
            $this->db->where("franco.status", 1);
        }

        if ($status == 2) {
            $this->db->where("franco.status", 1);
            $this->db->or_where("franco.status", 2);
        }

        if ($status == 3) {
            $this->db->where("franco.status", 3);
            $this->db->or_where("franco.status", 4);
        }

        if ($status == 4) {
            $this->db->where("franco.status", 2);
            $this->db->or_where("franco.status", 3);
            $this->db->or_where("franco.status", 4);
        }

        if ($status == 5) {
            $this->db->where("franco.status", 5);
        }

        $this->db->group_end();
    }

    private function branch_based($branch_id = '', $status = null) {

        if ($status == 1) {
            $this->db->where("franco.destination", $branch_id);
        }

        if ($status == 2) {
            $this->db->where("franco.origin", $branch_id);
        }

        if ($status == 3) {
            $this->db->where("franco.origin", $branch_id);
        }

        if ($status == 4) {
            $this->db->where("franco.destination", $branch_id);
        }

        if ($status == 5) {
            $this->db->where("franco.origin", $branch_id);
        }
    }

    private function regional_based($regional = array(), $status = null) {

        if ($status == 1) {
            $this->db->where_in("franco.destination", $regional);
        }

        if ($status == 2) {
            $this->db->where_in("franco.origin", $regional);
        }

        if ($status == 3) {
            $this->db->where_in("franco.origin", $regional);
        }

        if ($status == 4) {
            $this->db->where_in("franco.destination", $regional);
        }

        if ($status == 5) {
            $this->db->where_in("franco.origin", $regional);
        }
    }

    private function order($column = 0, $mode = 'desc') {

        switch ($column) {
            case 0 :
                $this->db->order_by('franco.franco_id', $mode);
                break;
            case 1 :
                $this->db->order_by('franco.order_id', $mode);
                break;
            case 2 :
                $this->db->order_by('customers.customer_name', $mode);
                break;
            case 3 :
                $this->db->order_by('branch_origin.branch_name', $mode);
                break;
            case 4 :
                 $this->db->order_by('branch_destination.branch_name', $mode);
                break;
        }
    }

    public function session_filter($status = null) {

        if ($this->session->userdata('franco_list_filter')['as_branch'] && $this->session->userdata('franco_list_filter')['as_branch'] != '0') {

            $branch_id = $this->session->userdata('franco_list_filter')['as_branch'];

            if ($status == 1) {
                $this->db->where("franco.destination", $branch_id);
            }

            if ($status == 2) {
                $this->db->where("franco.origin", $branch_id);
            }

            if ($status == 3) {
                $this->db->where("franco.origin", $branch_id);
            }

            if ($status == 4) {
                $this->db->where("franco.destination", $branch_id);
            }

            if ($status == 5) {
                $this->db->where("franco.origin", $branch_id);
            }
        }
    }


}
