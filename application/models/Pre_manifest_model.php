<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pre_manifest_model extends CI_Model {

    private $table = 'pre_manifest';
    private $id = 'pre_manifest_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->join('branches', 'branches.branch_id = pre_manifest.pre_manifest_origin');
        $this->db->where($where_array);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);

        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array(), $update_by = array()) {

        if (empty($update_by)) {
            $this->db->where($this->id, $set_array[$this->id]);
        } else {
            $this->db->where(key($update_by), $update_by[key($update_by)]);
        }

        $this->db->where('deleted_at', 0);

        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $result = $this->db->get($this->table);

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);

        return $result;
    }

    public function get_joined($where_array = array(), $attributes = array()) {

        $strselect = 'packages.*, pre_manifest.pre_manifest_id, '
                . 'det_order.det_order_total AS total, det_order.order_id AS order_id, '
                . 'origin.branch_id AS origin_branch_id, destination.branch_id  AS destination_branch_id, '
                . 'origin.branch_name as origin_branch_name, '
                . 'destination.branch_name as destination_branch_name, '
                . 'origin.branch_city as origin_branch_city, destination.branch_city AS destination_branch_city, '
                . 'cities_origin.city_name as city_name_origin, cities_destination.city_name  as city_name_destination ';

        $this->db->select($strselect);

        $this->db->where($where_array);
        $this->db->where("{$this->table}.deleted_at", 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $this->db->join('packages', 'pre_manifest.pre_manifest_package_id  = packages.package_id');
        $this->db->join('det_order', 'packages.package_id = det_order.package_id', 'left');
        $this->db->join('branches as origin', 'origin.branch_id = pre_manifest.pre_manifest_origin');
        $this->db->join('branches as destination', 'destination.branch_id = pre_manifest.pre_manifest_destination');
        $this->db->join('cities as cities_origin', 'origin.branch_city = cities_origin.city_id');
        $this->db->join('cities as cities_destination', 'destination.branch_city = cities_destination.city_id');

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_pre_manifest($date = '') {

        $str_select = 'origin_cities.city_id AS origin_city_id, '
                . 'origin_cities.city_name AS origin_city_name, '
                . 'destination_cities.city_id AS destination_city_id, '
                . 'destination_cities.city_name AS destination_city_name, '
                . 'origin_branches.branch_name AS origin_branch_name, '
                . 'destination_branches.branch_name AS destination_branch_name, '
                . 'origin_branches.branch_id AS origin_branch_id, '
                . 'destination_branches.branch_id AS destination_branch_id, '
                . 'userprofile.userprofile_fullname AS branch_manager, '
                . 'userprofile.userprofile_photo AS photo_manager, '
                . 'count(destination_branches.branch_city) AS total';

        $this->db->select($str_select);
        $this->db->join('branches AS origin_branches', 'pre_manifest.pre_manifest_origin = origin_branches.branch_id');
        $this->db->join('branches AS destination_branches', 'pre_manifest.pre_manifest_destination = destination_branches.branch_id');
        $this->db->join('cities AS origin_cities', 'origin_branches.branch_city = origin_cities.city_id');
        $this->db->join('cities AS destination_cities', 'destination_branches.branch_city = destination_cities.city_id');
        $this->db->join('user', 'origin_branches.branch_manager = user.user_id', 'left');
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id');
        $this->db->where('pre_manifest.pre_manifest_status', 1);
        $this->db->group_by(array('origin_branches.branch_id', 'destination_branches.branch_id'));

        return $this->db->get($this->table);
    }

    public function get_last_generate() {
        $this->db->select('pre_manifest_date');
        $this->db->limit(1);
        $this->db->order_by('pre_manifest_date', 'DESC');
        $result = $this->db->get($this->table)->row();
        return !empty($result) ? $result->pre_manifest_date : '';
    }

}
