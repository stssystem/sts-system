<?php

require_once('Orders_model.php');

class Report_orders_model extends Orders_model {

    private $cal_info;
    private $branch_id;
    private $year;
    private $result;
    private $data;

    public function __construct() {
        parent::__construct();
        $this->cal_info = cal_info(0)['months'];
        $this->load->model('branches_model');
    }

    private function build_perbranch() {

        $str = 'city_name, '
                . 'branch_name, '
                . 'order_sell_total, '
                . 'from_unixtime(order_date) as formated_order_date, order_date, '
                . 'orders.order_origin';

        $this->db->select($str);
        $this->db->join('branches', 'orders.order_origin = branches.branch_id');
        $this->db->join('cities', 'branches.branch_city = cities.city_id');
        $this->db->where('orders.order_origin', $this->branch_id);
        $this->db->where("YEAR(from_unixtime(orders.order_date))", $this->year);
        $this->db->where('orders.deleted_at', 0);
        $this->db->order_by('orders.order_date', 'desc');

        $query = $this->db->get($this->table);

        return $query->result();
    }

    private function month_format() {
        $orders = $this->build_perbranch();

        $this->result = [];
        if (!empty($orders)) {
            foreach ($orders as $key => $item) {
                $keys = '' . mdate('%n', $item->order_date);
                if (key_exists($keys, $this->result)) {
                    $this->result[$keys] += $item->order_sell_total;
                } else {
                    $this->result[$keys] = $item->order_sell_total;
                }
            }
        }

        $this->completed();
        $this->totalize();

        return $this->result;
    }

    private function completed() {
        foreach ($this->cal_info as $key => $item) {
            if (!key_exists($key, $this->result)) {
                $this->result[$key] = 0;
            }
        }
    }

    private function totalize() {
        $total = 0;
        foreach ($this->result as $key => $item) {
            $total += $item;
        }
        $this->result['total'] = $total;
    }

    private function branches_plot() {

        $branches = $this->branches_model->get()->result();
        $this->data = [];

        if (!empty($branches)) {
            foreach ($branches as $key => $item) {
                $this->branch_id = $item->branch_id;
                $this->data[$key]['branch'] = $item->branch_name;
                $this->data[$key]['value'] = $this->month_format();
            }
        }

        return $this->data;
    }

    public function get_report($year = 0) {
        $this->year = $year;
        $result = $this->branches_plot();
        return $result;
    }

}
