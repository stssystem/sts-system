<?php

class Report_logistic_cities_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '') {

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('cities') != '' ? $this->session_filter() : false;

        key_exists('order', $attr) ? $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']) : false;

        $this->sql_base($attr);

        $this->db->limit($limit, $index);
        
        return $this->db->get('report_logistic_cities_detail');
        
    }

    public function get_total($attr = array(), $search = '') {

        $this->db->select('count(report_logistic_cities_detail.order_id) AS total');

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('cities') != '' ? $this->session_filter() : false;

        $this->sql_base($attr);
        
        $data = $this->db->get('report_logistic_cities_detail')->row();

        return $data->total;
        
    }

    private function search($search = '') {

        $str = 'CONCAT('
        . 'order_id, " ", '
        . 'branch_origin_name, " ", '
        . 'customer_name, " ", '
        . 'total_weight, " ", '
        . 'total_size, " ", '
        . 'from_unixtime(order_date, "%d %b %Y")'
        . ')';

        $this->db->like($str, $search);
        
    }
    
    private function sql_base($attr = array()){
        if(key_exists('city_id', $attr)){
            $this->db->where('city_id', $attr['city_id']);
        }
    }

    public function session_filter() {

        if ($this->session->userdata('cities')['date_start'] && $this->session->userdata('cities')['date_end']) {
            $start  = $this->session->userdata('cities')['date_start'];
            $end    = $this->session->userdata('cities')['date_end'];
            $this->db->where('order_date >=', strtotime($start));
            $this->db->where('order_date <=', strtotime($end));
        }

        if ($this->session->userdata('cities')['total_weight']) {
            $total_weight = $this->session->userdata('cities')['total_weight'];
            $this->db->where('total_weight', $total_weight);
        }
        
        if ($this->session->userdata('cities')['total_size']) {
            $total_size = $this->session->userdata('cities')['total_size'];
            $this->db->where('total_size', $total_size);
        }

    }

    public function order($column = 0, $mode = 'desc') {

        switch ($column) {
            case 1 :
            $this->db->order_by('order_date', $mode);
            break;
            case 2 :
            $this->db->order_by('order_id', $mode);
            break;
            case 3 :
            $this->db->order_by('branch_origin_name', $mode);
            break;
            case 4 :
            $this->db->order_by('customer_name', $mode);
            break;
            case 5 :
            $this->db->order_by('total_weight', $mode);
            break;
            case 6 :
            $this->db->order_by('total_size', $mode);
            break;
        }

    }

}