<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    protected $table = 'user';
    protected $table_foreign = 'user_types';

    public function get_user($where_array = array()) {
        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);
        $query = $this->db->get($this->table);
        return $query;
    }

    public function update_user($set_array = array(), $where_array = array()) {
        $this->db->set($set_array);
        if (!empty($where_array)) {
            $this->db->where($where_array);
        }
        $this->db->update($this->table);
    }

    public function update($params = array()) {
        $this->db->where('user_id', $params['user_id']);
        $this->db->update($this->table, $params);
    }

    public function insert_user($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function delete($params = array()) {
        $this->db->where('user_id', $params['user_id']);
        $this->db->update($this->table, ['deleted_at' => time()]);
    }

    public function get_profile($where_array = null) {

        $str = 'userprofile.*, user.*, user_types.*, user.created_at as user_created_at, '
                . 'concat(user_type_name, " - " ,userprofile_fullname) AS name, branches.branch_name, branches.*, '
                . 'user.user_id as uid';

        $this->db->select($str);
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id', 'left');
        $this->db->join('user_types', 'user.user_type_id = user_types.user_type_id', 'left');
        $this->db->join('branches', 'userprofile.branch_id = branches.branch_id', 'left');

        $this->db->where('user.deleted_at', 0);
        if ($where_array != null && !is_array($where_array)) {
            $this->db->where('user.user_id', $where_array);
        } else if ($where_array != null && is_array($where_array)) {
            $this->db->where($where_array);
        }

        return $this->db->get($this->table);
    }

    public function get_regional_staff($regional_id = 0) {

        $this->db->select('user.user_id, '
                . 'user.username, '
                . 'userprofile.userprofile_fullname, '
                . 'user_types.user_type_name, '
                . 'user.last_login');

        $this->db->join('userprofile', 'user.user_id = userprofile.user_id', 'left');
        $this->db->join('user_types', 'user.user_type_id = user_types.user_type_id', 'left');

        $this->db->where('userprofile.regional_id', $regional_id);
        $this->db->where('user.deleted_at', 0);

        return $this->db->get($this->table);
    }

    public function update_joined($where_array = array()) {
        $this->db->set($where_array);
        $this->db->where('user.user_id', $where_array['user.user_id']);
        $this->db->update("($this->table JOIN userprofile ON user.user_id = userprofile.user_id)");
    }

    public function add_regional_staff($staff = array(), $regional_id = 0) {
        if (!empty($staff)) {
            foreach ($staff as $key => $item) {
                $this->db->where('user.user_id', $item);
                $this->db->update("($this->table JOIN userprofile ON user.user_id = userprofile.user_id)", ['userprofile.regional_id' => $regional_id]);
            }
        }
    }

    public function delete_regional_staff($users_id) {
        $this->db->where('user.user_id', $users_id);
        $this->db->update("($this->table JOIN userprofile ON user.user_id = userprofile.user_id)", ['userprofile.regional_id' => NULL]);
    }

    public function open_branch_access($user_type = 0, $branch_access = 0) {
        if ($branch_access == 1) {
            $this->db->where('user.user_type_id', $user_type);
            $this->db->update("($this->table JOIN userprofile ON user.user_id = userprofile.user_id)", [
                'userprofile.regional_id' => NULL, 'userprofile.branch_id' => NULL
            ]);
        }
    }

    public function get_user_profile($where_array = array(), $attr = array(), $attributes = array()) {

        $str = 'userprofile.*, user.*, user_types.*, user.created_at as user_created_at, '
                . 'concat(user_type_name, " - " ,userprofile_fullname) AS name, branches.branch_name, branches.*, '
                . 'user.user_id as uid';

        $this->db->select($str);
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id', 'left');
        $this->db->join('user_types', 'user.user_type_id = user_types.user_type_id', 'left');
        $this->db->join('branches', 'userprofile.branch_id = branches.branch_id', 'left');

        $this->db->where('user.deleted_at', 0);

        $this->session->userdata('users') != '' ? $this->session_filter() : false;

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        if (key_exists('order', $attr)) {
            $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']);
        }

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_user_total($where_array = array(), $attributes = array()) {

        $str = 'userprofile.*, user.*, user_types.*, user.created_at as user_created_at, '
                . 'concat(user_type_name, " - " ,userprofile_fullname) AS name, branches.branch_name, branches.*, '
                . 'user.user_id as uid';

        $this->db->select($str);
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id', 'left');
        $this->db->join('branches', 'userprofile.branch_id = branches.branch_id', 'left');
        $this->db->join('user_types', 'user.user_type_id = user_types.user_type_id', 'left');

        $this->db->where('user.deleted_at', 0);

        $this->session->userdata('users') != '' ? $this->session_filter() : false;

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    private function concat_like($data = '') {

        $str = 'CONCAT'
                . '(user.user_id, " ", '
                . 'userprofile_fullname, " ", '
                . 'username, " " '
                // . 'branches.branch_name, " " '
                . ')';

        $this->db->like($str, $data);
    }

    private function order($column = 0, $mode = 'desc') {
        switch ($column) {
            case 1 :
                $this->db->order_by('userprofile.userprofile_fullname', $mode);
                break;
            case 2 :
                $this->db->order_by('user.username', $mode);
                break;
            case 3 :
                $this->db->order_by('branches.branch_name', $mode);
                break;
            case 4 :
                $this->db->order_by('user_status', $mode);
                break;
            case 5 :
                $this->db->order_by('last_login', $mode);
                break;
            case 6 :
                $this->db->order_by('created_at', $mode);
                break;
        }
    }

    // Function to get all fullname of users
    public function get_fullname($user_id = '') {
        $this->db->select('userprofile.userprofile_fullname as fullname');
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id');
        $this->db->where('user.user_id', $user_id);
        $result = $this->db->get($this->table)->row();
        return isset($result->fullname) ? $result->fullname : '';
    } 

}
