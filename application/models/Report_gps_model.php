<?php

class Report_gps_model extends CI_Model {

    private $table = 'armadas';
    private $id = 'armada_id';

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = [], $limit = 10, $index = 0, $search = '', $column_export = array()) {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            // Select only data needed
            $str = ''
                    . 'armadas.armada_id AS armada_id, '
                    . 'armadas.armada_name AS armada_name, '
                    . 'armadas.armada_license_plate AS armada_license_plate, '
                    . 'armadas.traccar_device_id AS traccar_device_id,'
                    . 'armadas.armada_last_positin AS  armada_last_positin '
                    . '';

            $this->db->select($str);
        }

        $this->sql_base($attr, $search);

        if (key_exists('order', $attr)) {
            $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']);
        }

        $this->db->limit($limit, $index);

        return $this->db->get($this->table);
    }

    public function download($column_export = array()) {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            // Select only data needed
            $str = ''
                    . 'armadas.armada_id AS `ID Armada`, '
                    . 'armadas.armada_name AS `Nama Armada`, '
                    . 'armadas.armada_license_plate AS `Plat Nomor`, '
                    . 'armadas.traccar_device_id AS `ID Traccar Device` '
                    . '';

            $this->db->select($str);
            
        }

        $this->sql_base();

        return $this->db->get($this->table);
        
    }

    public function get_total($attr = [], $search = '') {

        $this->db->select('count(armadas.armada_id) AS total');

        $this->sql_base($attr, $search);

        $data = $this->db->get($this->table)->row();

        return $data->total;
    }

    // Internal function list
    private function sql_base($attr = array(), $search = '') {
        
        // Condition query
        $this->db->where('armadas.deleted_at', 0);
        $this->db->where('armadas.armada_status', 1);
        
        // Search when $search variable is filled
        if ($search != '') {
            $this->search($search);
        }
        
    }

    // Search
    private function search($search = '') {

        // Concat select
        $str = 'CONCAT('
                . 'armadas.armada_id, " ", '
                . 'armadas.armada_name, " ", '
                . 'armadas.armada_license_plate, " ", '
                . 'armadas.traccar_device_id'
                . ')';

        $this->db->like($str, $search);
    }

    private function order($column = 0, $mode = 'desc') {
        switch ($column) {
            case 0 :
                $this->db->order_by('armadas.armada_id', $mode);
                break;
            case 1 :
                $this->db->order_by('armadas.armada_name', $mode);
                break;
            case 2 :
                $this->db->order_by('armadas.armada_license_plate', $mode);
                break;
            case 3 :
                $this->db->order_by('armadas.traccar_device_id', $mode);
                break;
            case 4 :
                $this->db->order_by('armadas.armada_last_positin', $mode);
                break;
        }
    }

}
