<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Packages_model extends CI_Model {

    private $table = 'packages';
    private $id = 'package_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        /** Do when select key available */
        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array(), $updated_by = array()) {

        if (empty($updated_by)) {
            $this->db->where($this->id, $set_array[$this->id]);
        } else {
            $this->db->where(key($updated_by), $updated_by[key($updated_by)]);
        }

        $this->db->where('deleted_at', 0);

        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $result = $this->db->get($this->table);

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['packages.deleted_at' => time()]);

        return $result;
    }

    public function destroy($order_id = '') {

        $str = "DELETE det_order, packages "
                . "FROM packages "
                . "JOIN det_order ON det_order.package_id = packages.package_id "
                . "WHERE det_order.order_id = {$order_id}";

        $this->db->query($str);

        return true;
    }

   

    public function get_joined($where_array = array(), $attributes = array()) {

        $strselect = 'packages.*, det_order.det_order_total AS total, '
                . 'det_order.det_order_qty AS qty, '
                . 'det_order.det_order_package_parent AS package_parent, '
                . 'det_order.det_order_sell_total AS sell_total, '
                . 'det_order.order_id AS order_id, '
                . 'origin.branch_id AS origin_branch_id, destination.branch_id  AS destination_branch_id, '
                . 'origin.branch_city as origin_branch_city, destination.branch_city AS destination_branch_city, '
                . 'cities_origin.city_name as city_name_origin, cities_destination.city_name  as city_name_destination,'
                . 'origin.branch_name as origin_branch_name, destination.branch_name as destination_branch_name, '
                . 'package_types.package_type_name as package_type_name';

        $this->db->select($strselect);

        $this->db->where($where_array);
        $this->db->where('packages.deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Do when group by key availabe */
        if (key_exists('group_by', $attributes)) {
            $this->db->group_by($attributes['group_by']);
        }

        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->join('branches as origin', 'packages.package_origin = origin.branch_id', 'left');
        $this->db->join('branches as destination', 'packages.package_destination = destination.branch_id', 'left');
        $this->db->join('cities as cities_origin', 'origin.branch_city = cities_origin.city_id', 'left');
        $this->db->join('cities as cities_destination', 'destination.branch_city = cities_destination.city_id', 'left');
        $this->db->join('package_types', 'packages.package_type = package_types.package_type_id', 'left');

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_joinmanifest($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('packages.deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->join('manifest', 'packages.package_manifest_id = manifest.manifest_id', 'left');

        return $this->db->get($this->table);
    }

    public function get_checkloaded($package_id = '', $status = '') {
        $this->db->select('packages.package_id AS package_id');
        $this->db->where('package_status', $status);
        $this->db->where('package_id', $package_id);
        $result = $this->db->get($this->table)->row();
        return isset($result->package_id) ? true : false;
    }

    public function update_joined($where_array = array(), $update_by = array()) {

        $this->db->set($where_array);

        if (empty($update_by)) {
            $this->db->where('det_order.order_id', $where_array['det_order.order_id']);
        } else {
            $this->db->where($update_by);
        }

        $this->db->update("($this->table JOIN det_order ON packages.package_id = det_order.package_id)");
    }

    public function update_package_manifest($set_array = array(), $where_array = array()) {
        $this->db->set($set_array);
        $this->db->where($where_array);
        $this->db->update($this->table);
    }

    public function delete_byparent($parent_id = 0) {

        $str = 'DELETE packages '
                . 'FROM packages '
                . 'INNER JOIN det_order '
                . 'ON packages.package_id = det_order.package_id '
                . 'WHERE det_order.det_order_package_parent = ' . $parent_id;

        $this->db->query($str);
    }

    public function get_parents($package_id = 0) {
        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->where('packages.package_id', $package_id);
        return $this->db->get($this->table)->row()->det_order_package_parent;
    }

    public function get_parents_list($order_id) {
        $this->db->select('det_order.det_order_package_parent AS parent_id');
        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->where('det_order.order_id', $order_id);
        $this->db->group_by('det_order.det_order_package_parent');
        return $this->db->get($this->table)->result_array();
    }

    public function get_single_item($order_id = '') {
        $this->db->select('packages.*, det_order.det_order_qty AS det_order_qty, det_order.det_order_package_parent AS det_order_package_parent');
        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->where('det_order.order_id', $order_id);
        $this->db->group_by('det_order.det_order_package_parent');
        return $this->db->get($this->table)->result_array();
    }

    public function delete_pickup_foreign() {

        $str = "DELETE packages, det_order FROM packages "
                . "INNER JOIN det_order ON packages.package_id = det_order.package_id "
                . "WHERE det_order.order_id = '" . get_sessorder() . "'";

        $this->db->query($str);
    }

    public function count($order_id = '') {
        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->where('det_order.order_id', $order_id);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function isempty($order_id = '') {
        return $this->count($order_id) <= 0 ? true : false;
    }

    public function get_last_status($order_id = null) {
        $this->db->select('package_status');
        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->where('det_order.order_id', $order_id);
        $this->db->group_by('det_order.order_id');
        $result = $this->db->get($this->table)->row_array();
        return isset($result['package_status']) ? $result['package_status'] : 0;
    }

    public function retrieve_input($order_id = null) {

        /** Load model resource */
        $this->load->model('orders_model');

        $width = $this->input->post('package_width') / 100;
        $length = $this->input->post('package_lenght') / 100;
        $heigth = $this->input->post('package_height') / 100;

        $size = $width * $length * $heigth;

        $set_array = array(
            'package_title' => $this->input->post('package_title'),
            'package_type' => $this->input->post('package_type'),
            'package_origin' => $this->orders_model->get_delivery($order_id)['origin'],
            'package_destination' => $this->orders_model->get_delivery($order_id)['destination'],
            'package_status' => $this->get_last_status($order_id),
            'package_content' => $this->input->post('package_content'),
            'package_width' => $this->input->post('package_width') / 100,
            'package_height' => $this->input->post('package_height') / 100,
            'package_lenght' => $this->input->post('package_lenght') / 100,
            'package_size' => $size,
            'package_weight' => $this->input->post('package_weight'),
            'created_at' => time()
        );

        return $this->insert($set_array);
    }

    public function get_order_id($package_id = 0) {
        $result = $this->get_joined([
                    'packages.package_id' => $package_id
                ])->row_array();
        return isset($result['order_id']) ? $result['order_id'] : null;
    }

    public function count_regional_package($regional_id) {
        $this->db->select('count(*) As total');
        $this->db->join('branches as branch_origin', 'packages.package_origin = branch_origin.branch_id', 'left');
        $this->db->join('regionals', 'branch_origin.branch_regional_id = regionals.regional_id', 'left');
        $this->db->where('regionals.regional_id', $regional_id);
        $count = $this->db->get($this->table)->row();
        return isset($count->total) ? $count->total : '';
    }

    public function get_totals($order_id = 0) {

        $this->db->select(''
                . 'count(*) as total_package, '
                . 'SUM(packages.package_size) As total_package_size, '
                . 'SUM(packages.package_weight) As total_package_weight '
                . '');

        $this->db->join('det_order', 'det_order.package_id = packages.package_id');
        $this->db->where('det_order.order_id', $order_id);

        return $this->db->get($this->table)->row();
    }

    public function get_armada($order_id = '') {

        $this->db->select('armadas.armada_name AS armada_name, '
                . 'armadas.armada_license_plate AS armada_license_plate');

        $this->db->where('det_order.order_id', $order_id);

        $this->db->join('armadatrips', 'armadas.armada_id = armadatrips.armadatrip_armada_id');
        $this->db->join('('
                . 'SELECT '
                . 'det_order.order_id AS order_id, '
                . 'det_order.det_order_package_parent AS package_parent '
                . 'FROM '
                . 'det_order '
                . 'WHERE '
                . 'det_order.package_id = det_order.det_order_package_parent '
                . ') AS det_order', 'armadatrips.armadatrip_package_id = det_order.package_parent');

        $this->db->order_by('armadatrips.created_at', 'desc');
        $this->db->limit(1);

        $result = $this->db->get('armadas')->row();

        if (empty($result)) {
            $result = new stdClass();
            $result->armada_name = '';
            $result->armada_license_plate = '';
        }

        return $result;
    }

    public function get_packages_view($where = array()) {
        if (!empty($where)) {
            $this->db->where($where);
        }

        $query = $this->db->get('packages_view');

        return $query;
    }

    public function get_packages_view_detail($where = array()) {

        $strselect = 'packages.*, packages_view_detail.det_order_total AS total, '
                . 'packages_view_detail.det_order_qty AS qty, '
                . 'packages_view_detail.det_order_package_parent AS package_parent, '
                . 'packages_view_detail.det_order_sell_total AS sell_total, '
                . 'packages_view_detail.order_id AS order_id, '
                . 'packages_view_detail.total_package_size AS total_package_size, '
                . 'packages_view_detail.total_package_weight AS total_package_weight, '
                . 'origin.branch_id AS origin_branch_id, destination.branch_id  AS destination_branch_id, '
                . 'origin.branch_city as origin_branch_city, destination.branch_city AS destination_branch_city, '
                . 'cities_origin.city_name as city_name_origin, cities_destination.city_name  as city_name_destination,'
                . 'origin.branch_name as origin_branch_name, destination.branch_name as destination_branch_name, '
                . 'package_types.package_type_name as package_type_name';

        $this->db->select($strselect);

        $this->db->where($where);
        $this->db->where('packages.deleted_at', 0);

        $this->db->join('packages', 'packages.package_id = packages_view_detail.package_id');
        $this->db->join('branches as origin', 'packages.package_origin = origin.branch_id', 'left');
        $this->db->join('branches as destination', 'packages.package_destination = destination.branch_id', 'left');
        $this->db->join('cities as cities_origin', 'origin.branch_city = cities_origin.city_id', 'left');
        $this->db->join('cities as cities_destination', 'destination.branch_city = cities_destination.city_id', 'left');
        $this->db->join('package_types', 'packages.package_type = package_types.package_type_id', 'left');

        $query = $this->db->get('packages_view_detail');

        return $query;
    }

    public function get_all_child($parent_id = '') {
        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->where('det_order.det_order_package_parent', $parent_id);
        return $this->db->get($this->table)->result();
    }

}
