<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_logistic_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_logistic_cities($where_array = array(), $attributes = array(), $column_export = array())
    {
        $this->db->where('city_name !=', null);

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        }

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('report_logistic_cities_total');
        return $query;
    }

    public function get_logistic_armadas($where_array = array(), $attributes = array(), $column_export = array())
    {
        // $this->db->order_by('orders_view_location_total.total_weight', 'DESC');

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        }

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('report_logistic_armadas');
        return $query;
    }

    public function view_logistic_armadas($where_array = array(), $attributes = array())
    {
        $this->db->select('DISTINCT(report_logistic_packages.order_id) AS order_id, report_logistic_packages.armadatrip_armada_id, orders.order_date, orders_view.*, packages_view.*');
        $this->db->join('orders', 'orders.order_id = report_logistic_packages.order_id', 'left');
        $this->db->join('orders_view', 'orders_view.order_id = orders.order_id', 'left');
        $this->db->join('packages_view', 'packages_view.order_id = orders.order_id', 'left');

        $this->db->where('report_logistic_packages.order_id !=', NULL);

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('report_logistic_packages');
        return $query;
    }

    public function get_logistic_deliveries_1($where_array = array(), $attributes = array(), $column_export = array())
    {
        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        }

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('report_logistic_deliveries_1');
        return $query;
    }

    public function get_logistic_deliveries_2($where_array = array(), $attributes = array(), $column_export = array())
    {   
        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        }

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('report_logistic_deliveries_2');
        return $query;
    }

    public function get_logistic_deliveries_3($where_array = array(), $attributes = array(), $column_export = array())
    {   
        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        }
        
        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('report_logistic_deliveries_3');
        return $query;
    }



}
