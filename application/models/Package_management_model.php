<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package_management_model extends CI_Model {

    private $table = 'package_management';
    private $id = 'package_management_id';
    private $data = array();

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '') {
        $this->select();
        $this->db->limit($limit, $index);
        $this->sql_base($attr);
        return $this->db->get($this->table);
    }

    public function get_total($attr = array(), $search = '') {
        $this->db->select('count(package_management_id) AS c');
        $this->sql_base($attr);
        $result = $this->db->get($this->table)->row_array();
        return $result['c'];
    }

    private function sql_base($attr = array()) {

        $this->db->where('type_do', $attr['type_do']);

        $this->session->userdata('package-management-filter') != '' ? $this->session_filter($attr['type_do']) : false;

        $this->default_date();

        // Branch and regional previlage 
        if (!key_exists('branch_access', $attr) || $attr['branch_access'] != 1) {
            if (key_exists('regional', $attr) && !empty($attr['regional'])) {
                $this->regional_based($attr['regional'], $attr['type_do']);
            } else {
                if (key_exists('branch', $attr) && $attr['branch'] != '') {
                    $this->branch_based($attr['branch'], $attr['type_do']);
                }
            }
        }
    }

    private function select() {
        $this->db->select('package_management_id');
        $this->db->select('resi_number');
        $this->db->select('resi_date');
        $this->db->select('do_date');
        $this->db->select('armada_plate_number');
        $this->db->select('courier');
        $this->db->select('koli');
        $this->db->select('weight');
        $this->db->select('volume');
        $this->db->select('type_do');

        $this->db->select('origin');
        $this->db->select('destination');
        $this->db->select('branch_id_do');
    }

    private function default_date() {
        if (!$this->session->userdata('package-management-filter')['date_start'] && !$this->session->userdata('package-management-filter')['date_end']) {
            $this->db->where('package_management.do_date >=', strtotime(date('01-m-Y')));
            $this->db->where('package_management.do_date <=', strtotime(date('t-m-Y')));
        }
    }

    private function session_filter($status = '') {

        if ($this->session->userdata('package-management-filter')['date_start'] && $this->session->userdata('package-management-filter')['date_end']) {
            $start = $this->session->userdata('package-management-filter')['date_start'];
            $end = $this->session->userdata('package-management-filter')['date_end'];
            $this->db->where('package_management.do_date >=', strtotime($start));
            $this->db->where('package_management.do_date <=', strtotime($end));
        }

        if (isset($this->session->userdata('package-management-filter')['branches']) && $this->session->userdata('package-management-filter')['branches'] != '') {
            $origin = $this->session->userdata('package-management-filter')['branches'];
            $this->branch_based($origin, $status);
        }
        
    }

    private function branch_based($branch_id = '', $status = '') {

        if ($status == 'load') {
            $this->db->where("package_management.origin", $branch_id);
        }

        if ($status == 'unload') {
            $this->db->where("package_management.destination", $branch_id);
        }

        if ($status == 'transit') {
            $this->db->where("package_management.branch_id_do", $branch_id);
        }

        if ($status == 'delivery') {
            $this->db->where("package_management.destination", $branch_id);
        }
    }

    private function regional_based($regional = array(), $status = null) {

        if ($status == 'load') {
            $this->db->where_in("package_management.origin", $regional);
        }

        if ($status == 'unload') {
            $this->db->where_in("package_management.destination", $regional);
        }

        if ($status == 'transit') {
            $this->db->where_in("package_management.branch_id_do", $regional);
        }

        if ($status == 'delivery') {
            $this->db->where_in("package_management.destination", $regional);
        }
    }

    public function update_loading($resi_number = '', $armada_id = '') {

        // Load all resource needed
        $this->load->model('orders_model');
        $this->load->model('armadas_model');

        // Get detail info
        $detail_order = $this->orders_model->get_detail($resi_number);
        $package_info = $this->orders_model->get_package_info($resi_number);

        // Set up data 
        $data = array(
            'resi_number' => $detail_order->order_id,
            'resi_number_manual' => $detail_order->order_manual_id,
            'resi_date' => $detail_order->order_date,
            'do_date' => time(),
            'armada_id' => $armada_id,
            'armada_plate_number' => $this->armadas_model->get_license_plate($armada_id),
            'armada_name' => $this->armadas_model->get_name($armada_id),
            'origin' => $detail_order->order_origin,
            'destination' => $detail_order->order_destination,
            'branch_id_do' => branch()['branch_id'],
            'branch_do' => branch()['branch_name'],
            'city_do' => branch()['city_name'],
            'koli' => $package_info->koli,
            'weight' => $package_info->weight,
            'volume' => $package_info->size,
            'type_do' => 'load',
            'created_at' => time(),
        );

        // Insert data
        $this->db->insert($this->table, $data);

        // Return id
        return $this->db->insert_id();
    }

    public function update_unloading($resi_number = '', $armada_id = '') {

        // Load all resource needed
        $this->load->model('orders_model');
        $this->load->model('armadas_model');

        // Get detail info
        $detail_order = $this->orders_model->get_detail($resi_number);
        $package_info = $this->orders_model->get_package_info($resi_number);

        // Set up data 
        $data = array(
            'resi_number' => $detail_order->order_id,
            'resi_number_manual' => $detail_order->order_manual_id,
            'resi_date' => $detail_order->order_date,
            'do_date' => time(),
            'armada_id' => $armada_id,
            'armada_plate_number' => $this->armadas_model->get_license_plate($armada_id),
            'armada_name' => $this->armadas_model->get_name($armada_id),
            'origin' => $detail_order->order_origin,
            'destination' => $detail_order->order_destination,
            'branch_id_do' => branch()['branch_id'],
            'branch_do' => branch()['branch_name'],
            'city_do' => branch()['city_name'],
            'koli' => $package_info->koli,
            'weight' => $package_info->weight,
            'volume' => $package_info->size,
            'type_do' => 'unload',
            'created_at' => time(),
        );

        // Insert data
        $this->db->insert($this->table, $data);

        // Return id
        return $this->db->insert_id();
    }

    public function update_transit($resi_number = '', $armada_id = '') {

        // Load all resource needed
        $this->load->model('orders_model');
        $this->load->model('armadas_model');

        // Get detail info
        $detail_order = $this->orders_model->get_detail($resi_number);
        $package_info = $this->orders_model->get_package_info($resi_number);

        // Set up data 
        $data = array(
            'resi_number' => $detail_order->order_id,
            'resi_number_manual' => $detail_order->order_manual_id,
            'resi_date' => $detail_order->order_date,
            'do_date' => time(),
            'armada_id' => $armada_id,
            'armada_plate_number' => $this->armadas_model->get_license_plate($armada_id),
            'armada_name' => $this->armadas_model->get_name($armada_id),
            'origin' => $detail_order->order_origin,
            'destination' => $detail_order->order_destination,
            'branch_id_do' => branch()['branch_id'],
            'branch_do' => branch()['branch_name'],
            'city_do' => branch()['city_name'],
            'koli' => $package_info->koli,
            'weight' => $package_info->weight,
            'volume' => $package_info->size,
            'type_do' => 'transit',
            'created_at' => time(),
        );

        // Insert data
        $this->db->insert($this->table, $data);

        // Return id
        return $this->db->insert_id();
    }

    public function update_delivery($resi_number = '', $armada_id = '', $courier_id = '') {

        // Load all resource needed
        $this->load->model('orders_model');
        $this->load->model('armadas_model');
        $this->load->model('users_model');

        // Get detail info
        $detail_order = $this->orders_model->get_detail($resi_number);
        $package_info = $this->orders_model->get_package_info($resi_number);

        // Set up data 
        $data = array(
            'resi_number' => $detail_order->order_id,
            'resi_number_manual' => $detail_order->order_manual_id,
            'resi_date' => $detail_order->order_date,
            'do_date' => time(),
            'armada_id' => $armada_id,
            'armada_plate_number' => $this->armadas_model->get_license_plate($armada_id),
            'armada_name' => $this->armadas_model->get_name($armada_id),
            'courier_id' => $courier_id,
            'courier' => $this->users_model->get_fullname($courier_id),
            'origin' => $detail_order->order_origin,
            'destination' => $detail_order->order_destination,
            'branch_id_do' => branch()['branch_id'],
            'branch_do' => branch()['branch_name'],
            'city_do' => branch()['city_name'],
            'koli' => $package_info->koli,
            'weight' => $package_info->weight,
            'volume' => $package_info->size,
            'type_do' => 'delivery',
            'created_at' => time(),
        );

        // Insert data
        $this->db->insert($this->table, $data);

        // Return id
        return $this->db->insert_id();
    }

}
