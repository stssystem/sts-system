<?php

class Order_list_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '') {

        $str = ''
                . 'orders_view.order_id as order_id, '
                . 'orders_view.order_date as order_date, '
                . 'orders_view.order_load_date AS order_load_date, '
                . 'orders_view.order_unload_date AS order_unload_date, '
                . 'orders_view.order_delivered_date AS order_delivered_date, '
                . 'orders_view.order_received_date AS order_received_date, '
                . 'orders_view.order_status as order_status, '
                . 'orders_view.customer_name as customer_name, '
                . 'orders_view.branch_name_origin as branch_name_origin, '
                . 'orders_view.branch_name_destination as branch_name_destination, '
                . 'packages_view.total_package as total_package, '
                . 'packages_view.total_koli as total_koli, '
                . 'packages_view.total_weight as total_weight, '
                . 'packages_view.total_size as total_size, '
                . 'packages_view.package_status as package_status, '
                . 'CEIL(ABS(UNIX_TIMESTAMP(NOW()) - order_date)/60/60/24) AS resi_age '
                . '';

        $this->db->select($str);

        $this->sql_base($attr, $search);

        if (key_exists('order', $attr)) {
            $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']);
        }

        $this->db->limit($limit, $index);

        return $this->db->get('orders_view');
    }

    public function get_total($attr = array(), $search = '') {

        $this->db->select('count(orders_view.order_id) AS total');

        $this->sql_base($attr, $search);

        $data = $this->db->get('orders_view')->row();

        return $data->total;
    }

    public function get_totalize($attr = array()) {

        $str = ''
                . 'SUM(packages_view.total_koli) AS total_koli, '
                . 'FORMAT(ROUND(SUM(packages_view.total_weight),3),0,"de_DE")  AS total_weight, '
                . 'FORMAT(ROUND(SUM(packages_view.total_size) / 1000000, 3),0,"de_DE") AS total_size'
                . '';

        $this->db->select($str);

        $this->sql_base($attr);

        return $this->db->get('orders_view')->row_array();
    }

    private function sql_base($attr = array(), $search = '') {

        $this->db->join('packages_view', 'orders_view.order_id = packages_view.order_id');

        $search != '' ? $this->concat_like($search) : false;

        $this->status_based($attr['status']);

        // Branch and regional previlage 
        if (!key_exists('branch_access', $attr) || $attr['branch_access'] != 1) {
            if (key_exists('regional', $attr) && !empty($attr['regional'])) {
                $this->regional_based($attr['regional'], $attr['status']);
            } else {
                if (key_exists('branch', $attr) && $attr['branch'] != '') {
                    $this->branch_based($attr['branch'], $attr['status']);
                }
            }
        }

        $this->session->userdata('order_list_filter') != '' ? $this->session_filter($attr['status']) : false;

        if ($attr['status'] == 1) {
            $this->session->userdata('status_filter_willdrop') != '' ? $this->package_status_filter_willdrop($this->session->userdata('status_filter_willdrop')) : false;
        }
        
        if ($attr['status'] == 4) {
            $this->session->userdata('status_filter_inprocess') != '' ? $this->package_status_filter_inprocess($this->session->userdata('status_filter_inprocess')) : false;
        }
        
    }

    private function concat_like($search = '') {

        $str = 'CONCAT('
                . 'orders_view.order_id, " ", '
                . 'orders_view.customer_name, " ", '
                . 'orders_view.branch_name_origin, " - ", '
                . 'orders_view.branch_name_destination, " - ", '
                . 'packages_view.total_koli, " - ", '
                . 'packages_view.total_weight, " - ", '
                . 'packages_view.total_size, " - ", '
                . 'from_unixtime(orders_view.order_date, "%d %b %Y")'
                . ')';

        $this->db->like($str, $search);
    }

    private function status_based($status) {

        if ($status == 0) {
            $this->db->where("orders_view.order_status ", 0);
        }

        if ($status == 1) {
            $this->db->where("(orders_view.order_status  = 0 OR orders_view.order_status  = 1)");
            $this->db->where("package_status <", 5);
        }

        if ($status == 2) {
            $this->db->where("orders_view.order_status", 1);
            $this->db->where("package_status", 5);
        }

        if ($status == 3) {
            $this->db->where("orders_view.order_status", 1);
            $this->db->where("package_status", 7);
        }

        if ($status == 4) {
            $this->db->where("orders_view.order_status", 1);
            $this->db->where("(package_status = 4 OR package_status  = 5 OR package_status  = 6  OR package_status  = 7 )");
        }

        if ($status == 5) {
            $this->db->where("orders_view.order_status", 2);
        }
    }

    private function branch_based($branch_id = '', $status = null) {

        if ($status == null) {
            $this->db->where("orders_view.branch_name_origin", $branch_id);
        }

        if ($status == 0) {
            $this->db->where("orders_view.branch_name_origin", $branch_id);
        }

        if ($status == 1 || $status == 2 || $status == 3) {
            $this->db->where("orders_view.branch_name_destination", $branch_id);
        }

        if ($status == 4) {
            $this->db->where("orders_view.branch_name_origin", $branch_id);
        }

        if ($status == 5) {
            $this->db->where("orders_view.branch_name_destination", $branch_id);
        }
    }

    private function regional_based($regional = array(), $status = null) {

        if ($status == null) {
            $this->db->where_in("orders_view.branch_name_origin", $regional);
        }

        if ($status == 0) {
            $this->db->where_in("orders_view.branch_name_origin", $regional);
        }

        if ($status == 1 || $status == 2 || $status == 3) {
            $this->db->where_in("orders_view.branch_name_destination", $regional);
        }

        if ($status == 4) {
            $this->db->where_in("orders_view.branch_name_origin", $regional);
        }

        if ($status == 5) {
            $this->db->where("orders_view.branch_name_destination", $regional);
        }
    }

    private function order($column = 0, $mode = 'desc') {
        switch ($column) {
            case 0 :
                $this->db->order_by('order_date', $mode);
                break;
            case 1 :
                $this->db->order_by('order_id', $mode);
                break;
            case 2 :
                $this->db->order_by('customer_name', $mode);
                break;
            case 3 :
                $this->db->order_by('branch_name_origin', $mode);
                $this->db->order_by('branch_name_destination', $mode);
                break;
            case 4 :
                $this->db->order_by('total_koli', $mode);
                break;
            case 5 :
                $this->db->order_by('total_weight', $mode);
                break;
            case 6 :
                $this->db->order_by('total_size', $mode);
                break;
            case 7 :
                $this->db->order_by('resi_age', $mode);
                break;
        }
    }

    public function session_filter($status = null) {

        if ($this->session->userdata('order_list_filter')['as_branch_name'] && $this->session->userdata('order_list_filter')['as_branch_name'] != '') {

            $branch_id = $this->session->userdata('order_list_filter')['as_branch_name'];

            if ($status == null) {
                $this->db->where("orders_view.branch_name_origin", $branch_id);
            }

            if ($status == 0) {
                $this->db->where("orders_view.branch_name_origin", $branch_id);
            }

            if ($status == 1 || $status == 2 || $status == 3) {
                $this->db->where("orders_view.branch_name_destination", $branch_id);
            }

            if ($status == 4) {
                $this->db->where("orders_view.branch_name_origin", $branch_id);
            }

            if ($status == 5) {
                $this->db->where("orders_view.branch_name_destination", $branch_id);
            }
        }
    }

    private function package_status_filter_willdrop($status = null) {
        if ($status > 3) {
            $this->db->where("packages_view.package_status ", $status);
        } else {
            $this->db->where("packages_view.package_status <= ", $status);
        }
    }

    private function package_status_filter_inprocess($status = null) {
        if ($status > 3) {
            $this->db->where("packages_view.package_status ", $status);
        } else {
            $this->db->where("packages_view.package_status <= ", $status);
        }
    }

}
