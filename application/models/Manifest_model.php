<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manifest_model extends CI_Model {

    private $table = 'manifest';
    private $id = 'manifest_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);

        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $result = $this->db->get($this->table);

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);

        return $result;
    }

    public function get_joined($where_array = array(), $attributes = array()) {
        
        $str = 'manifest.*, count(packages.package_id) as total_package, '
                . 'armadas.armada_name, origin_cities.city_name as origin_city_name, '
                . 'destination_cities.city_name as destination_city_name, '
                . 'userprofile.userprofile_fullname as driver ';
        
        $this->db->select($str);

        $this->db->join('armadas', 'manifest.manifest_armada_id = armadas.armada_id', 'left');
        $this->db->join('trips', 'manifest.manifest_trip_id = trips.trip_id', 'left');
        $this->db->join('user', 'trips.user_id = user.user_id', 'left');
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id', 'left');
        $this->db->join('branches as origin_branch', 'manifest.manifest_origin = origin_branch.branch_id', 'left');
        $this->db->join('branches as destination_branch', 'manifest.manifest_destination = destination_branch.branch_id', 'left');
        $this->db->join('cities as origin_cities', 'origin_cities.city_id = origin_branch.branch_city', 'left');
        $this->db->join('cities as destination_cities', 'destination_cities.city_id = destination_branch.branch_city', 'left');
        $this->db->join('packages', 'manifest.manifest_id = packages.package_manifest_id');

        $this->db->where($where_array);
        $this->db->where('manifest.deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }
        
        $this->db->group_by('manifest.manifest_id');

        $query = $this->db->get($this->table);

        return $query;
    }
    

}
