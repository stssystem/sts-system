<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Devices_model extends CI_Model {

    private $table = 'devices';
    private $id = 'id';
    private $db;

    public function __construct() {
        parent::__construct();
        $this->db = db_connect('traccar');
        $this->load->model('armadas_model');
        $this->load->model('det_trips_model');
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $result = $this->db->get($this->table)->row();

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);

        return $result;
    }

    public function get_joined($where_array = array(), $attributes = array()) {

        $this->db->join('positions', 'devices.positionId = positions.id');

        $this->db->where($where_array);

        return $this->db->get($this->table);
    }

    public function get_armada_lastposition() {

        // Load all resource needed
        $this->load->model('user_profile_model');

        $array = $this->get_joined()->result_array();
        $result = [];

        if (!empty($array)) {
            foreach ($array as $key => $value) {

                $armadas = $this->armadas_model->get(['traccar_device_id' => $value['uniqueId']])->row_array();
                $driver = $this->user_profile_model->get_driver_name($armadas['armada_driver']);
                $trip = $this->det_trips_model->get_route_name($this->armadas_model->get(['traccar_device_id' => $value['uniqueId']])->row_array()['armada_id']);

                $result[$key] = [
                    $armadas['armada_name'],
                    $value['latitude'],
                    $value['longitude'],
                    $key,
                    ($trip != '') ? $trip : '-',
                    'volume' => ($armadas['armada_v_m3'] != '') ? $armadas['armada_v_m3'] : '-',
                    'tonase' => ($armadas['armada_tonase'] != '') ? $armadas['armada_tonase'] : '-',
                    'license_plate' => ($armadas['armada_license_plate'] != '') ? $armadas['armada_license_plate'] : '-',
                    'driver' => ($driver) ? $driver : '-',
                    'uniqueid' => $value['uniqueId'],
                ];
            }
        }

        return $result;
    }

}
