<?php

class Report_logistic_pickup_orders_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '') {

        $this->db->select('*');
        $this->db->join('orders', 'orders.order_id = accepted_report.order_id');
        $this->db->join('branches AS branch_origin', 'branch_origin.branch_id = order_id.branch_id');
        $search != '' ? $this->search($search) : false;
        $this->session->userdata('pickup_orders') != '' ? $this->session_filter() : false;

        key_exists('order', $attr) ? $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']) : false;

        $this->sql_base($attr);

        $this->db->limit($limit, $index);
        
        return $this->db->get('accepted_report');
        
    }

    public function get_total($attr = array(), $search = '') {

        $this->db->select('count(order_id) AS total');

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('branch_destination') != '' ? $this->session_filter() : false;

        $this->sql_base($attr);
        
        $data = $this->db->get('sale_comission_branch_destination_detail')->row();

        return $data->total;
        
    }

    private function search($search = '') {

        $str = 'CONCAT('
        . 'sale_comission_branch_destination_detail.branch_id, " ", '
        . 'sale_comission_branch_destination_detail.order_id, " ", '
        . 'sale_comission_branch_destination_detail.order_status, " ", '
        . 'sale_comission_branch_destination_detail.omset, " ", '
        . 'sale_comission_branch_destination_detail.customer_name, " ", '
        . 'sale_comission_branch_destination_detail.branch_name_origin, " ", '
        . 'sale_comission_branch_destination_detail.branch_name_destination, " ", '
        . 'from_unixtime(sale_comission_branch_destination_detail.order_date, "%d %b %Y %h:%i:%s")'
        . ')';

        $this->db->like($str, $search);
        
    }
    
    private function sql_base($attr = array()){
        if(key_exists('branch_id', $attr)){
            $this->db->where('branch_id', $attr['branch_id']);
        }
    }

    public function session_filter() {

        if ($this->session->userdata('branch_destination')['date_start'] && $this->session->userdata('branch_destination')['date_end']) {
            $start  = $this->session->userdata('branch_destination')['date_start'];
            $end    = $this->session->userdata('branch_destination')['date_end'];
            $this->db->where('sale_comission_branch_destination_detail.order_date >=', strtotime($start));
            $this->db->where('sale_comission_branch_destination_detail.order_date <=', strtotime($end));
        }

        if ($this->session->userdata('branch_destination')['branch_origin_name'] && $this->session->userdata('branch_destination')['branch_origin_name'] != '') {
            $origin = $this->session->userdata('branch_destination')['branch_origin_name'];
            $this->db->where('sale_comission_branch_destination_detail.branch_name_origin', $origin);
        }
        
        if ($this->session->userdata('branch_destination')['branch_destination_name'] && $this->session->userdata('branch_destination')['branch_destination_name'] != '') {
            $destination = $this->session->userdata('branch_destination')['branch_destination_name'];
            $this->db->where('sale_comission_branch_destination_detail.branch_name_destination', $destination);
        }

    }

    public function order($column = 0, $mode = 'desc') {

        switch ($column) {
            case 1 :
            $this->db->order_by('sale_comission_branch_destination_detail.order_date', $mode);
            break;
            case 2 :
            $this->db->order_by('sale_comission_branch_destination_detail.order_id', $mode);
            break;
            case 3 :
            $this->db->order_by('sale_comission_branch_destination_detail.customer_name', $mode);
            break;
            case 4 :
            $this->db->order_by('sale_comission_branch_destination_detail.branch_name_origin', $mode);
            break;
            case 5 :
            $this->db->order_by('sale_comission_branch_destination_detail.branch_name_destination', $mode);
            break;
            case 6 :
            $this->db->order_by('sale_comission_branch_destination_detail.omset', $mode);
            break;
            case 7 :
            $this->db->order_by('sale_comission_branch_destination_detail.omset', $mode);
            break;
        }
    }

}