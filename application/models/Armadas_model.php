<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Armadas_model extends CI_Model {

    private $table = 'armadas';
    private $id = 'armada_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        // Search
        key_exists('search', $attributes) ? $this->search($attributes['search']) : false;

        // Order by
        key_exists('dt_order', $attributes) ? $this->order(
                                $attributes['dt_order'][0]['column'], $attributes['dt_order'][0]['dir']
                        ) : false;

        $query = $this->db->get($this->table);
        return $query;
    }

    public function get_total($search = '') {

        $this->db->select('count(*) As count');

        $this->db->where('deleted_at', 0);

        // Search query
        $search != '' ? $this->search($search) : false;

        $result = $this->db->get($this->table)->row();

        return $result->count;
    }

    private function search($search = '') {

        $str = 'CONCAT('
                . 'armadas.armada_id, " ", '
                . 'armadas.armada_name, " ", '
                . 'armadas.armada_license_plate, " ", '
                . 'CASE armadas.armada_status '
                . 'WHEN 1 THEN "Aktif"  '
                . 'WHEN 2 THEN "Tidak Aktif"  '
                . 'END , " ", '
                . 'from_unixtime(armadas.created_at, "%d %b %Y")'
                . ')';

        $this->db->like($str, $search);
    }

    private function order($column = 0, $mode = 'desc') {
        switch ($column) {
            case 0 :
                $this->db->order_by('armada_id', $mode);
                break;
            case 1 :
                $this->db->order_by('armada_name', $mode);
                break;
            case 2 :
                $this->db->order_by('armada_license_plate', $mode);
                break;
            case 3 :
                $this->db->order_by('armada_status', $mode);
                break;
            case 4 :
                $this->db->order_by('created_at', $mode);
                break;
        }
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);
        return $this->db->get($this->table)->row();
    }

    public function get_joined($where_array = array(), $attributes = array()) {

        $this->db->join('user', 'armadas.armada_driver = user.user_id', 'left');
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id', 'left');

        $this->db->where($where_array);
        $this->db->where('armadas.deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);

        return $query;
    }

    public function update_last_location($armada_id, $last_location) {
        $this->update([
            'armada_id' => $armada_id,
            'armada_last_positin' => $last_location
        ]);
    }

    public function update_all_lastlocation($data = array()) {
        if (!empty($data)) {
            foreach ($data as $key => $item) {

                $array = [
                    'armada_last_positin' => $item[1] . ',' . $item[2]
                ];

                $this->db->where('traccar_device_id', $item['uniqueid']);
                $this->db->update($this->table, $array);
            }
        }
    }

    public function get_armada_id($unique_id) {
        $this->db->select('armada_id');
        $this->db->where('traccar_device_id', $unique_id);
        $result = $this->db->get($this->table)->row();
        return isset($result->armada_id) ? $result->armada_id : '';
    }

    public function get_name($armada_id = 0) {
        $this->db->select('armada_name');
        $this->db->where('armada_id', $armada_id);
        $data = $this->db->get($this->table)->row();
        return isset($data->armada_name) ? $data->armada_name : '';
    }
    
    public function get_license_plate($armada_id = 0) {
        $this->db->select('armada_license_plate');
        $this->db->where('armada_id', $armada_id);
        $data = $this->db->get($this->table)->row();
        return isset($data->armada_license_plate) ? $data->armada_license_plate : '';
    }

}
