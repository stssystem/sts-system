<?php

class Sync_package_model extends CI_Model {

    private $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('wp_packages_model');
        $this->load->model('packages_model');
        $this->load->model('det_orders_model');
        $this->load->model('package_pic_model');
    }

    public function set($params) {
        $this->data = json_decode($params, true);
    }

    public function get() {
        return $this->wp_packages_model->get(['ss_pick_up_order_id' => pickup_order_get('array')->id])->result_array();
    }

    public function execute() {
        $this->sync($this->get());
    }

    private function sync($params = []) {
        $package_parent = 0;
        if (!empty($params)) {
            foreach ($params as $key => $item) {
                $package_id = $this->bundling_package($item);
                if ($key == 0) {
                    $package_parent = $package_id;
                }
                $this->bundling_det_orders($item, $package_id, $package_parent);
                $this->bundling_package_pic($package_id, $item['foto_paket']);
            }
        }
    }

    public function bundling_package($data = array()) {

        $width = $data['lebar'] == null ? 0 : $data['lebar'];
        $height = $data['tinggi'] == null ? 0 : $data['tinggi'];
        $length = $data['panjang'] == null ? 0 : $data['panjang'];
        $weight = $data['berat'] == null ? 0 : $data['berat'];

        $array = [
            'package_title' => $data['nama_jenis'],
            'package_status' => 0,
            'package_type' => 4,
            'package_origin' => 0,
            'package_destination' => 0,
            'package_content' => '',
            'package_width' => $width,
            'package_height' => $height,
            'package_lenght' => $length,
            'package_size' => ($width * $height * $length),
            'package_weight' => $weight,
            'created_at' => time()
        ];

        return $this->packages_model->insert($array);
    }

    private function bundling_det_orders($data = array(), $package_id = 0, $package_parent = 0) {

        $array = [
            'package_id' => $package_id,
            'order_id' => pickup_id_get(),
            'det_order_total' => $data['harga'],
            'det_order_qty' => $data['jumlah'],
            'det_order_sell_total' => $data['harga'],
            'det_order_package_parent' => $package_id
        ];

        $this->det_orders_model->insert($array);
    }

    private function bundling_package_pic($package_id = 0, $url = '') {
        
        if($url == ''){
            return false;
        }

        $ext = pathinfo($url)['extension'];
        $filename = date('ymdhis') . rand(1000, 9999) . '.' . $ext;
        file_put_contents('./assets/upload/' . $filename, fopen($url, 'r'));

        $package = [
            'package_id' => $package_id,
            'package_pic' => $filename
        ];

        $this->package_pic_model->insert($package);
        
        return true;
        
    }

}
