<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Armada_trips_model extends CI_Model {

    private $table = 'armadatrips';
    private $id = 'armadatrip_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);
        return $this->db->get($this->table)->row();
    }

    public function sql_loaded($armada_id) {
        $this->db->join('trips', 'armadatrips.armadatrip_armada_id = trips.trip_id');
        $this->db->join('packages', 'armadatrips.armadatrip_package_id = packages.package_id');
        $this->db->where('packages.package_status', 4);
        $this->db->where('trips.armada_id', $armada_id);
    }

    public function get_order_loaded($armada_id) {

        $this->db->select('orders.*');
        $this->sql_loaded($armada_id);
        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->join('orders', 'det_order.order_id = orders.order_id');
        $this->db->group_by('orders.order_id');

        return $this->db->get($this->table)->result_array();
    }

    public function get_package_loaded($armada_id, $geo_branch) {

        $data = [
            'packages.package_last_location' => $geo_branch
        ];
        $this->db->set($data);
        $this->db->where('packages.package_status', 4);
        $this->db->where('trips.armada_id', $armada_id);
        $this->db->update(" ( ". $this->table . " "
                . "JOIN trips ON  armadatrips.armadatrip_armada_id = trips.trip_id "
                . "JOIN packages ON armadatrips.armadatrip_package_id = packages.package_id )");

        return true;
    }
    
    public function test($armada_id) {

        $this->db->select('orders.*');
        $this->sql_loaded($armada_id);
        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->join('orders', 'det_order.order_id = orders.order_id');
        $this->db->group_by('orders.order_id');

        return $this->db->get($this->table)->result_array();
    }

}
