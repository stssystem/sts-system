<?php

class Report_sale_comission_up_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_data($where_array = array(), $attr = array(), $attributes = array()) {

        $this->session->userdata('comission_up') != '' ? $this->session_filter() : false;

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        if (key_exists('order', $attr)) {
            $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']);
        }

        $this->sql_base($attr);

        return $this->db->get('sale_comission_report_up_detail');
    }

    public function get_total($where_array = array(), $attr = array(), $attributes = array()) {

        $this->session->userdata('comission_up') != '' ? $this->session_filter() : false;

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        $this->sql_base($attr);

        $data = $this->db->get('sale_comission_report_up_detail')->row();

        return $this->db->count_all_results();
    }
    
    public function get_sum($branch_id = 0) {
        
        // Select
        $this->db->select(''
                . 'FORMAT(SUM(sale_comission_report_up_detail.omset),0,"de_DE") AS total_omset, '
                . 'FORMAT(SUM(ROUND(((branches.branch_comission / 100) * sale_comission_report_up_detail.omset))),0,"de_DE") AS comission_agent '
                . '');

        $this->sql_base(['branch_id' => $branch_id]);

        return $this->db->get('sale_comission_report_up_detail')->row_array();
        
    }

    private function concat_like($data = '') {

        $str = 'CONCAT('
        . 'sale_comission_report_up_detail.branch_id, " ", '
        . 'sale_comission_report_up_detail.order_id, " ", '
        . 'sale_comission_report_up_detail.order_status, " ", '
        . 'sale_comission_report_up_detail.omset, " ", '
        . 'sale_comission_report_up_detail.customer_name, " ", '
        . 'sale_comission_report_up_detail.branch_name_origin, " ", '
        . 'sale_comission_report_up_detail.branch_name_destination, " ", '
        . 'from_unixtime(sale_comission_report_up_detail.order_date, "%d %b %Y %h:%i:%s")'
        . ')';

        $this->db->like($str, $data);
        
    }
    
    private function sql_base($attr = array()){
        if(key_exists('branch_id', $attr)){
            $this->db->where('branch_id', $attr['branch_id']);
        }
    }

    public function session_filter() {

        if ($this->session->userdata('comission_up')['date_start'] && $this->session->userdata('comission_up')['date_end']) {
            $start  = $this->session->userdata('comission_up')['date_start'];
            $end    = $this->session->userdata('comission_up')['date_end'];
            $this->db->where('sale_comission_report_up_detail.order_date >=', strtotime($start));
            $this->db->where('sale_comission_report_up_detail.order_date <=', strtotime($end));
        }

        if ($this->session->userdata('comission_up')['branch_origin_name'] && $this->session->userdata('comission_up')['branch_origin_name'] != '') {
            $origin = $this->session->userdata('comission_up')['branch_origin_name'];
            $this->db->where('sale_comission_report_up_detail.branch_name_origin', $origin);
        }
        
        if ($this->session->userdata('comission_up')['branch_destination_name'] && $this->session->userdata('comission_up')['branch_destination_name'] != '') {
            $destination = $this->session->userdata('comission_up')['branch_destination_name'];
            $this->db->where('sale_comission_report_up_detail.branch_name_destination', $destination);
        }

    }

    public function order($column = 0, $mode = 'desc') {

        switch ($column) {
            case 1 :
            $this->db->order_by('sale_comission_report_up_detail.order_date', $mode);
            break;
            case 2 :
            $this->db->order_by('sale_comission_report_up_detail.order_id', $mode);
            break;
            case 3 :
            $this->db->order_by('sale_comission_report_up_detail.customer_name', $mode);
            break;
            case 4 :
            $this->db->order_by('sale_comission_report_up_detail.branch_name_origin', $mode);
            break;
            case 5 :
            $this->db->order_by('sale_comission_report_up_detail.branch_name_destination', $mode);
            break;
            case 6 :
            $this->db->order_by('sale_comission_report_up_detail.omset', $mode);
            break;
            case 7 :
            $this->db->order_by('sale_comission_report_up_detail.omset', $mode);
            break;
        }
    }

}