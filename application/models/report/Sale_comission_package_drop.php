<?php

class Sale_comission_package_drop extends CI_Model {

    public function get($attributes = array(), $column_export = array()) {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select(''
                    . 'branches.branch_id, '
                    . 'branches.branch_name, '
                    . 'branches.branch_comission, '
                    . 'sale_comission_report_down.total_order, '
                    . 'sale_comission_report_down.total_omset'
                    . '');
        }

        $this->db->join('sale_comission_report_down', 'sale_comission_report_down.branch_id = branches.branch_id', 'left');

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (key_exists('where', $attributes)) {
            $this->db->where($attributes['where']);
        }

        if (key_exists('search', $attributes)) {
            $this->search($attributes['search']);
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('branches');
        return $query;
    }

    public function get_total($search = '') {

        $this->db->select('count(*) AS total');

        $this->db->join('sale_comission_report_down', 'sale_comission_report_down.branch_id = branches.branch_id', 'left');

        if ($search != '') {
            $this->search($search);
        }

        $query = $this->db->get('branches')->row();

        return $query->total;
    }
    
      public function get_sum() {

        $this->db->select(''
                . 'FORMAT(SUM(sale_comission_report_down.total_order),0,"de_DE") AS total_order, '
                . 'FORMAT(SUM(sale_comission_report_down.total_omset),0,"de_DE") AS total_omset, '
                . 'FORMAT(SUM(ROUND(((branches.branch_comission / 100) * sale_comission_report_down.total_omset))),0,"de_DE") AS comission_agent '
                . '');

        $this->db->join('sale_comission_report_down', 'sale_comission_report_down.branch_id = branches.branch_id', 'left');

        $query = $this->db->get('branches')->row_array();

        return $query;
        
    }

    public function search($search = '') {
        $str = 'branches.branch_name';
        $this->db->like($str, $search);
    }

    public function download($column_export = array()) {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select(''
                    . 'branches.branch_code AS `Kode Cabang`, '
                    . 'branches.branch_name AS `Nama Cabang`, '
                    . 'sale_comission_report_down.total_order AS `Total Order`, '
                    . 'sale_comission_report_down.total_omset AS `Total Omset`, '
                    . 'ROUND(((branches.branch_comission / 100) * sale_comission_report_down.total_omset)) AS `Komisi Agen`'
                    . '');
        }

        $this->db->join('sale_comission_report_down', 'sale_comission_report_down.branch_id = branches.branch_id', 'left');

        $query = $this->db->get('branches');

        return $query;
    }

}
