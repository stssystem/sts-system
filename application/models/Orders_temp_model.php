<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_temp_model extends CI_Model {

    private $data;

    public function __construct() {
        parent::__construct();
    }

    public function get_temp($order_id = '') {

        // Get orders
        $this->data['orders'] = $this->get_order($order_id);

        // Get customers
        $this->data['customers'] = $this->get_costumers($this->data['orders']->customer_id);
        
        // Get packages
        $this->data['packages'] = $this->get_package($order_id);
        
        // Get det order extra
        $this->data['det_order_extra'] = $this->get_order_extra($order_id);

        return $this->data;
        
    }

    private function get_order($order_id = '') {
        $this->db->where('order_id', $order_id);
        return $this->db->get('orders')->row();
    }

    private function get_costumers($costumer_id = '') {
        $this->db->where('customers.customer_id', $costumer_id);
        $this->db->join('customers_origin', 'customers.customer_id = customers_origin.customers_origin_province_id', 'left');        
        return $this->db->get('customers')->row();
    }
    
    private function get_package($order_id = ''){
        $this->db->join('det_order', 'packages.package_id = det_order.package_id', 'left');
        $this->db->where('det_order.order_id', $order_id);
        return $this->db->get('packages')->result();
    }
    
    private function get_order_extra($order_id = ''){
        $this->db->where('order_id', $order_id);
        return $this->db->get('det_orders_extra')->result();
    }

}
