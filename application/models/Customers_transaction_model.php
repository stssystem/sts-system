<?php

class Customers_transaction_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '', $column_export = array(), $date_start = '', $date_end = '', $where = array()) {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        }

        if (!empty($date_start) && !empty($date_end)) {
            $this->db->where('customers_transaction_report.last_transaction >=', strtotime($date_start));
            $this->db->where('customers_transaction_report.last_transaction <=', strtotime($date_end));
        }

        if (!empty($date_start)) {
            $this->db->where('customers_transaction_report.last_transaction >=', strtotime($date_start));
        }

        if (!empty($date_end)) {
            $this->db->where('customers_transaction_report.last_transaction <=', strtotime($date_end));
        }

        if (!empty($where)) {
            $this->db->where($where);
        }

        $this->default_date();

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('customers_report_filter') != '' ? $this->session_filter() : false;
        key_exists('order', $attr) ? $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']) : false;
        $this->db->limit($limit, $index);

        return $this->db->get('customers_transaction_report');
    }

    public function get_download($where = array(), $column = array()) {

        if (!empty($column)) {
            foreach ($column as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select('customer_id AS `ID Pelanggan`');
            $this->db->select('customer_name AS `Nama Pelanggan`');
            $this->db->select('city_name AS `Nama Kota`');
            $this->db->select('branch_name AS `Nama Cabang`');
            $this->db->select('customer_referral AS `No Referal`');
            $this->db->select('total_transaction AS `Total`');
            $this->db->select('from_unixtime(last_transaction, "%d %b %Y") AS `Transaksi Terakhir`');
        }

        if (!empty($where)) {
            $this->db->where($where);
        }

        $this->default_date();

        $this->session->userdata('customers_report_filter') != '' ? $this->session_filter() : false;

        return $this->db->get('customers_transaction_report');
        
    }

    public function get_total($search = '', $where = array()) {

        if (!empty($where)) {
            $this->db->where($where);
        }

        $this->db->select('count(customer_id) AS total');

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('customers_report_filter') != '' ? $this->session_filter() : false;

        $this->default_date();

        $data = $this->db->get('customers_transaction_report')->row();

        return $data->total;
    }

    public function get_total_price($attr = array(), $search = '') {
        $this->db->select('SUM(customers_transaction_report.total_transaction) AS total');
        $data = $this->db->get('customers_transaction_report')->row();
        return $data->total;
    }

    private function search($search = '') {

        $str = 'CONCAT('
                . 'customers_transaction_report.customer_id, " ", '
                . 'customers_transaction_report.customer_name, " ", '
                . 'customers_transaction_report.customer_referral'
                . ')';

        $this->db->like($str, $search);
    }

    public function session_filter() {

        if ($this->session->userdata('customers_report_filter')['date_start'] && $this->session->userdata('customers_report_filter')['date_end']) {
            $start = $this->session->userdata('customers_report_filter')['date_start'];
            $end = $this->session->userdata('customers_report_filter')['date_end'];
            $this->db->where('customers_transaction_report.last_transaction >=', strtotime($start));
            $this->db->where('customers_transaction_report.last_transaction <=', strtotime($end));
        }

        if ($this->session->userdata('customers_report_filter')['city_name'] && $this->session->userdata('customers_report_filter')['city_name'] != '') {
            $destination = $this->session->userdata('customers_report_filter')['city_name'];
            $this->db->where('customers_transaction_report.city_name', $destination);
        }

        if ($this->session->userdata('customers_report_filter')['branch_name'] && $this->session->userdata('customers_report_filter')['branch_name'] != '') {
            $origin = $this->session->userdata('customers_report_filter')['branch_name'];
            $this->db->where('customers_transaction_report.branch_name', $origin);
        }
    }

    private function order($column = 0, $mode = 'desc') {
        switch ($column) {
            case 0 :
                $this->db->order_by('customer_id', $mode);
                break;
            case 1 :
                $this->db->order_by('customer_name', $mode);
                break;
            case 2 :
                $this->db->order_by('city_name', $mode);
                break;
            case 3 :
                $this->db->order_by('branch_name', $mode);
                break;
            case 4 :
                $this->db->order_by('customer_referral', $mode);
                break;
            case 5 :
                $this->db->order_by('total_transaction', $mode);
                break;
            case 6 :
                $this->db->order_by('last_transaction', $mode);
                break;
        }
    }

    public function default_date() {

        if (!$this->session->userdata('customers_report_filter')['date_start'] && !$this->session->userdata('customers_report_filter')['date_end']) {
            $this->db->where('customers_transaction_report.last_transaction >=', strtotime(date('01-m-Y')));
            $this->db->where('customers_transaction_report.last_transaction <=', strtotime(date('t-m-Y')));
        }
    }

}
