<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_types_model extends CI_Model {

    private $table = 'user_types';
    private $id = 'user_type_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);
    }

    public function is_branch_access($user_type_id) {
        $this->db->select('branch_access');
        $this->db->where($this->id, $user_type_id);
        $result = $this->db->get($this->table)->row();
        return isset($result->branch_access) ? true : false;
    }

}
