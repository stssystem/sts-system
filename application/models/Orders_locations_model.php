<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_locations_model extends CI_Model {

    private $table = 'orders_locations';
    private $id = 'orders_locations_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);
        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);
        return $this->db->get($this->table)->row();
    }

    public function update_last_location($order_id, $geo_branch = '', $city_id = '', $branch_id = '', $noted = '') {
        $this->insert([
            'orders_locations_order_id' => $order_id,
            'orders_locations_last' => $geo_branch,
            'orders_locations_city_id' => $city_id,
            'orders_locations_branch_id' => $branch_id,
            'note' => $noted,
            'created_at' => time()
        ]);
    }

    public function update_last_locations($orders = array(), $geo_branch = '', $city_id = '', $branch_id = '', $noted = '') {
        if (!empty($orders)) {
            foreach ($orders as $key => $item) {
                $this->update_last_location($item['order_id'], $geo_branch, $city_id, $branch_id, $noted);
            }
        }
        return true;
    }

    public function get_order($where_array = array()) {

        $this->db->select('
            orders_locations.orders_locations_order_id as orders_locations_order_id,
            orders_locations.orders_locations_last as orders_locations_last, 
            orders_locations.created_at as created_at,
            orders_locations.updated_at as updated_at,
            orders_locations.deleted_at as deleted_at, 
            orders_locations.orders_locations_city_id as orders_locations_city_id,
            orders_locations.orders_locations_branch_id as orders_locations_branch_id,
            orders_locations.note as note, 
            cities.city_id as city_id,
            cities.city_name as city_name, 
            cities.province_id as province_id
            ');

        $this->db->where($where_array);
        $this->db->where('orders_locations.deleted_at', 0);

        $this->db->join('cities', 'orders_locations.orders_locations_city_id = cities.city_id', 'left');

        $query = $this->db->get($this->table);
        return $query;
    }

    public function get_last($id = '') {
        $this->db->select('*');
        $this->db->where('orders_locations.orders_locations_order_id', $id);
        $this->db->order_by('created_at', 'desc');
        return $this->db->get($this->table)->row();
    }

}
