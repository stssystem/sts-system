<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_price_model extends CI_Model {

    private $table = 'customers_price';
    private $id = 'customer_price_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $result = $this->db->get($this->table)->row();

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);

        return $result;
    }

    public function get_joined($where_array = array(), $attributes = array()) {

        $str = 'customers_price.customer_price_id AS customer_price_id, '
                . 'cities_origin.city_name AS customer_origin, cities_origin.city_id AS city_id_origin, '
                . 'cities_destination.city_name AS customer_destination, cities_destination.city_id AS city_id_destination, '
                . 'branch_origin.branch_name as branch_origin_name, '
                . 'branch_destination.branch_name as branch_destination_name, '
                . 'customers_price.customer_price as customer_price, '
                . 'customers_price.created_at as created_at';

        $this->db->select($str);
        $this->db->join('customers', 'customers_price.customer_id = customers.customer_id', 'left');
        $this->db->join('branches as branch_origin', 'customers_price.customer_branch_id_origin = branch_origin.branch_id', 'left');
        $this->db->join('branches as branch_destination', 'customers_price.customer_branch_id_destination = branch_destination.branch_id', 'left');
        $this->db->join('cities as cities_origin', 'branch_origin.branch_city = cities_origin.city_id', 'left');
        $this->db->join('cities as cities_destination', 'branch_destination.branch_city = cities_destination.city_id', 'left');

        $this->db->where($where_array);
        $this->db->where('customers_price.deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        if (key_exists('group_by', $attributes)) {
            $this->db->group_by($attributes['group_by']);
        }

        $query = $this->db->get($this->table);

        return $query;
    }

    public function mass_insert($customer_id = null, $branches_origin = [], $branches_destination = [], $price = null) {
        if (!empty($branches_origin)) {
            foreach ($branches_origin as $key => $item) {

                if (!empty($branches_destination)) {
                    foreach ($branches_destination as $k => $i) {

                        /** Prices create */
                        $set_array = array(
                            'customer_id' => $customer_id,
                            'customer_branch_id_origin' => $item->branch_id,
                            'customer_branch_id_destination' => $i->branch_id,
                            'customer_price' => $price,
                            'created_at' => time(),
                        );

                        $id = $this->insert($set_array);
                    }
                }
            }
        }
    }

    public function destroy_perdelivery($customer_id = null, $origin = null, $destination = null) {

        /** Get all list customer price that will delete */
        $listed = $this->get_joined([
                    'cities_origin.city_id' => $origin,
                    'cities_destination.city_id' => $destination,
                    'customers_price.customer_id' => $customer_id
                ])->result();

        /** Deleting all first */
        if (!empty($listed)) {
            foreach ($listed as $key => $item) {
                $this->destroy($item->customer_price_id);
            }
        }
    }

    public function destroy($id = null) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    public function get_price($customer_id, $origin, $destination) {
        $this->db->where('customer_id', $customer_id);
        $this->db->where('customer_branch_id_origin', $origin);
        $this->db->where('customer_branch_id_destination', $destination);
        $result = $this->db->get($this->table)->row();
        return isset($result->customer_price) ? $result->customer_price : null;
    }

    public function insert_price($customer_id, $origin_branch, $destination_branch, $price) {

        $set_array = array(
            'customer_id' => $customer_id,
            'customer_branch_id_origin' => $origin_branch,
            'customer_branch_id_destination' => $destination_branch,
            'customer_price' => $price,
            'created_at' => time(),
        );

        return $this->insert($set_array);
    }

    public function update_price($id, $price) {
        $this->update([
            $this->id => $id,
            'customer_price' => $price
        ]);
        return true;
    }

}
