<?php

class Report_sale_branch_destination_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '') {

        $this->db->select(''
                . 'sale_comission_branch_destination_detail.*,'
                . 'branches.branch_comission'
                . '');

        $search != '' ? $this->search($search) : false;

        key_exists('order', $attr) ? $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']) : false;

        $this->sql_base($attr);

        $this->db->limit($limit, $index);

        return $this->db->get('sale_comission_branch_destination_detail');
    }

    public function get_total($attr = array(), $search = '') {
        $this->db->select('count(sale_comission_branch_destination_detail.order_id) AS total');

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('branch_destination') != '' ? $this->session_filter() : false;

        $this->sql_base($attr);

        $data = $this->db->get('sale_comission_branch_destination_detail')->row();

        return $data->total;
    }

    public function get_sum($branch_id = 0) {

        // Select
        $this->db->select(''
                . 'FORMAT(SUM(sale_comission_branch_destination_detail.omset),0,"de_DE") AS total_omset, '
                . 'FORMAT(SUM(ROUND(((branches.branch_comission / 100) * sale_comission_branch_destination_detail.omset))),0,"de_DE") AS comission_agent '
                . '');

        $this->sql_base(['branch_id' => $branch_id]);

        return $this->db->get('sale_comission_branch_destination_detail')->row_array();
    }

    private function search($search = '') {

        $str = 'CONCAT('
                . 'sale_comission_branch_destination_detail.branch_id, " ", '
                . 'sale_comission_branch_destination_detail.order_id, " ", '
                . 'sale_comission_branch_destination_detail.order_status, " ", '
                . 'sale_comission_branch_destination_detail.omset, " ", '
                . 'sale_comission_branch_destination_detail.customer_name, " ", '
                . 'sale_comission_branch_destination_detail.branch_name_origin, " ", '
                . 'sale_comission_branch_destination_detail.branch_name_destination, " ", '
                . 'from_unixtime(sale_comission_branch_destination_detail.order_date, "%d %b %Y %h:%i:%s")'
                . ')';

        $this->db->like($str, $search);
    }

    private function sql_base($attr = array()) {

        // Join table
        $this->db->join('branches', 'sale_comission_branch_destination_detail.branch_id = branches.branch_id', 'left');

        // Branch date
        if (key_exists('branch_id', $attr)) {
            $this->db->where('sale_comission_branch_destination_detail.branch_id', $attr['branch_id']);
        }

        // Default date
        $this->default_date();

        // Filter session
        $this->session->userdata('filter_report_sale_branch_destination') != '' ? $this->session_filter() : false;
    }

    public function session_filter() {

        if ($this->session->userdata('filter_report_sale_branch_destination')['date_start'] && $this->session->userdata('filter_report_sale_branch_destination')['date_end']) {
            $start = $this->session->userdata('filter_report_sale_branch_destination')['date_start'];
            $end = $this->session->userdata('filter_report_sale_branch_destination')['date_end'];
            $this->db->where('sale_comission_branch_destination_detail.order_date >=', strtotime(str_replace('/', '-', $start)));
            $this->db->where('sale_comission_branch_destination_detail.order_date <=', strtotime(str_replace('/', '-', $end)));
        }

        if ($this->session->userdata('filter_report_sale_branch_destination')['branch_origin_name'] && $this->session->userdata('filter_report_sale_branch_destination')['branch_origin_name'] != '') {
            $origin = $this->session->userdata('filter_report_sale_branch_destination')['branch_origin_name'];
            $this->db->where('sale_comission_branch_destination_detail.branch_name_origin', $origin);
        }

        if ($this->session->userdata('filter_report_sale_branch_destination')['branch_destination_name'] && $this->session->userdata('filter_report_sale_branch_destination')['branch_destination_name'] != '') {
            $destination = $this->session->userdata('filter_report_sale_branch_destination')['branch_destination_name'];
            $this->db->where('sale_comission_branch_destination_detail.branch_name_destination', $destination);
        }
    }

    public function default_date() {
        if (!$this->session->userdata('filter_report_sale_branch_destination')['date_start'] && !$this->session->userdata('filter_report_sale_branch_destination')['date_end']) {
            $this->db->where('sale_comission_branch_destination_detail.order_date >=', strtotime(date('01-m-Y')));
            $this->db->where('sale_comission_branch_destination_detail.order_date <=', strtotime(date('t-m-Y')));
        }
    }

    public function order($column = 0, $mode = 'desc') {

        switch ($column) {
            case 1 :
                $this->db->order_by('sale_comission_branch_destination_detail.order_date', $mode);
                break;
            case 2 :
                $this->db->order_by('sale_comission_branch_destination_detail.order_id', $mode);
                break;
            case 3 :
                $this->db->order_by('sale_comission_branch_destination_detail.customer_name', $mode);
                break;
            case 4 :
                $this->db->order_by('sale_comission_branch_destination_detail.branch_name_origin', $mode);
                break;
            case 5 :
                $this->db->order_by('sale_comission_branch_destination_detail.branch_name_destination', $mode);
                break;
            case 6 :
                $this->db->order_by('sale_comission_branch_destination_detail.omset', $mode);
                break;
            case 7 :
                $this->db->order_by('sale_comission_branch_destination_detail.omset', $mode);
                break;
        }
    }

    public function download($branch_id = 0, $column_export = array()) {

        // Select list table
        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select(''
                    . 'FROM_UNIXTIME(sale_comission_branch_destination_detail.order_date, "%d %b %Y") AS `Tanggal Transaksi`, '
                    . 'sale_comission_branch_destination_detail.order_id AS `Nomor Resi`, '
                    . 'sale_comission_branch_destination_detail.customer_name AS `Nama Pelanggan`, '
                    . 'sale_comission_branch_destination_detail.branch_name_origin AS `Cabang/Agen Asal`, '
                    . 'sale_comission_branch_destination_detail.branch_name_destination AS `Cabang/Agen Tujuan`, '
                    . 'sale_comission_branch_destination_detail.omset AS `Omset`, '
                    . 'ROUND(((branches.branch_comission / 100) * sale_comission_branch_destination_detail.omset)) AS `Komisi Agen`'
                    . '');
        }

        $this->sql_base(['branch_id' => $branch_id]);

        return $this->db->get('sale_comission_branch_destination_detail');
    }

}
