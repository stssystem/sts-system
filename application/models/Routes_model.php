<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Routes_model extends CI_Model {

    private $table = 'routes';
    private $id = 'route_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        // Search
        key_exists('search', $attributes) ? $this->search($attributes['search']) : false;

        // Order 
        key_exists('dt_order', $attributes) ? $this->order(
                                $attributes['dt_order'][0]['column'], $attributes['dt_order'][0]['dir']
                        ) : false;

        $query = $this->db->get($this->table);
        
        return $query;
        
    }

    public function get_total($search = '') {

        $this->db->select('count(*) as count');

        $this->db->where('deleted_at', 0);

        $search != '' ? $this->search($search) : false;

        $result = $this->db->get($this->table)->row();

        return $result->count;
    }

    private function search($data = '') {

        $str = 'CONCAT'
                . '('
                . 'routes.route_id, " ", '
                . 'routes.route_name, " ", '
                . 'CASE '
                . 'routes.route_status '
                . 'WHEN 1 THEN "Aktif" '
                . 'WHEN 0 THEN "Tidak Aktif" '
                . 'END, " ",'
                . 'from_unixtime(routes.created_at, "%d %b %Y %h:%i:%s")'
                . ')';

        $this->db->like($str, $data);
    }

    private function order($column = 0, $mode = 'desc') {
        switch ($column) {
            case 0 :
                $this->db->order_by('route_id', $mode);
                break;
            case 1 :
                $this->db->order_by('route_name', $mode);
                break;
            case 2 :
                $this->db->order_by('route_status', $mode);
                break;
            case 3 :
                $this->db->order_by('created_at', $mode);
                break;
        }
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $result = $this->db->get($this->table)->row();

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);

        return $result;
    }

}
