<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_sale_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //---------------------------- laporan komisi agen -> paket naik --------------------------------

    public function get_comission_up($where_array = array(), $attributes = array(), $column_export = array()) {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select('
            branches.branch_id, 
            branches.branch_name, 
            sale_comission_report_up.total_order, 
            sale_comission_report_up.total_omset'
            );
        }

        $this->db->join('sale_comission_report_up', 'sale_comission_report_up.branch_id = branches.branch_id', 'left');

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('branches');
        return $query;
    }

    public function get_value_comission_up($where_array = array()) {

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        $query = $this->db->get('sale_comission_report_up');
        return $query;
    }

    public function get_comission_up_detail($where_array = array(), $attributes = array()) {   

        if (!empty($where)) {
            $this->db->where($where);
        }    

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('sale_comission_report_up_detail');
        return $query;
    }

    //---------------------------- laporan komisi agen paket turun ----------------------------------

    public function get_comission_down($where_array = array(), $attributes = array(), $column_export = array()) {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select('
            branches.branch_id, 
            branches.branch_name, 
            sale_comission_report_down.total_order, 
            sale_comission_report_down.total_omset'
            );
        }

        $this->db->join('sale_comission_report_down', 'sale_comission_report_down.branch_id = branches.branch_id', 'left');

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('branches');
        return $query;
    }

    public function get_value_comission_down($where_array = array()) {

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        $query = $this->db->get('sale_comission_report_down');
        return $query;
    }

    public function get_comission_down_detail($where_array = array(), $attributes = array()) {   

        if (!empty($where)) {
            $this->db->where($where);
        }    

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('sale_comission_report_down_detail');
        return $query;
    }

    // ----------------------------- laporan komisi agen -> cabang asal -------------------------------

    public function get_branch_origin($where_array = array(), $attributes = array(), $column_export = array()) {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select('
            branches.branch_id, 
            branches.branch_name, 
            sale_comission_branch_origin.total_order, 
            sale_comission_branch_origin.total_omset'
            );
        }

        $this->db->join('sale_comission_branch_origin', 'sale_comission_branch_origin.branch_id = branches.branch_id', 'left');

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('branches');
        return $query;
    }

    public function get_value_branch_origin($where_array = array()) {

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        $query = $this->db->get('sale_comission_branch_origin');
        return $query;
    }

    public function get_branch_origin_detail($where_array = array(), $attributes = array()) {   

        if (!empty($where)) {
            $this->db->where($where);
        }    

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('sale_comission_branch_origin_detail');
        return $query;
    }

        // ----------------------------- laporan komisi agen -> cabang tujuan -------------------------------

    public function get_branch_destination($where_array = array(), $attributes = array(), $column_export = array()) {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select('
            branches.branch_id, 
            branches.branch_name, 
            sale_comission_branch_destination.total_order, 
            sale_comission_branch_destination.total_omset'
            );
        }

        $this->db->join('sale_comission_branch_destination', 'sale_comission_branch_destination.branch_id = branches.branch_id', 'left');

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('branches');
        return $query;
    }

    public function get_value_branch_destination($where_array = array()) {

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        $query = $this->db->get('sale_comission_branch_destination');
        return $query;
    }

    public function get_branch_destination_detail($where_array = array(), $attributes = array()) {   

        if (!empty($where)) {
            $this->db->where($where);
        }    

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get('sale_comission_branch_destination_detail');
        return $query;
    }

    

}
