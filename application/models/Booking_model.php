<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_model extends CI_Model {

    private $table = 'orders';
    private $id = 'order_id';

    public function __construct() {
        parent::__construct();
    }

    public function get_data($arr = array(), $index = 0, $limit = 0, $search = '') {
        $this->select();
        $this->join();
        $this->db->where('order_status', -4);
        $this->db->limit($index, $limit);
        $this->db->group_by('orders.order_id');
        return $this->db->get($this->table);
    }

    public function get_total($search = '') {
        $this->db->select('count(orders.order_id) AS c');
        $this->db->where('order_status', -4);
        $result = $this->db->get($this->table)->row_array();
        return isset($result['c']) ? $result['c'] : 0;
    }

    private function select() {
        $this->db->select('orders.order_id AS order_id');
        $this->db->select('orders.order_manual_id AS order_manual_id');
        $this->db->select('orders.order_date AS order_date');
        $this->db->select('customers.customer_name AS customer_name');
        $this->db->select('branches_origin.branch_name AS branch_name_origin');
        $this->db->select('branches_destination.branch_name AS branch_name_destination');
        $this->db->select('count(packages.package_id) AS package_count');
    }

    private function join() {
        $this->db->join('det_order', 'orders.order_id = det_order.order_id', 'left');
        $this->db->join('packages', 'det_order.package_id = packages.package_id', 'left');
        $this->db->join('branches as branches_origin', 'orders.order_origin = branches_origin.branch_id', 'left');
        $this->db->join('branches as branches_destination', 'orders.order_destination = branches_destination.branch_id', 'left');
        $this->db->join('customers', 'orders.customer_id = customers.customer_id', 'left');
    }

    public function count_package($order_id = '') {

        $this->db->select('count(packages.package_id) AS total');
        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->where('det_order.order_id', $order_id);
        $data = $this->db->get('packages')->row();

        return $data->total;
    }

    public function update_package($order_id = '', $value = array()) {
        $this->db->set($value);
        $this->db->where('det_order.order_id', $order_id);
        $this->db->update("(packages JOIN det_order ON packages.package_id = det_order.package_id)");
        return true;
    }

    public function delete_package($order_id = '', $limit = '') {

        $str = "DELETE packages, det_order  "
                . "FROM packages "
                . "JOIN det_order ON packages.package_id = det_order.package_id "
                . "JOIN ( SELECT packages.package_id AS package_id FROM packages "
                . "JOIN det_order ON packages.package_id = det_order.package_id "
                . "WHERE det_order.order_id = '{$order_id}' "
                . "ORDER BY packages.package_id DESC LIMIT {$limit} ) AS package_det_order "
                . "ON package_det_order.package_id = packages.package_id";

        $this->db->query($str);

        return true;
        
    }

}
