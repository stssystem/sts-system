<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Det_orders_model extends CI_Model {

    private $table = 'det_order';
    private $id = 'det_order_id';

    public function __construct() {
        parent::__construct();
        $this->load->model('prices_model');
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        // $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array(), $update_by = array()) {

        if (empty($update_by)) {
            $this->db->where($this->id, $set_array[$this->id]);
        } else {
            $this->db->where(key($update_by), $update_by[key($update_by)]);
        }

        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($set_array);
        $result = $this->db->get($this->table);

        $this->db->where($set_array);
        $this->db->delete($this->table);

        return $result;
    }

    public function delete_byparent($parent_id = 0) {
        $this->db->where('det_order_package_parent', $parent_id);
        $this->db->delete($this->table);
    }

    public function get_order_id($package_id) {
        return $this->get(['package_id' => $package_id])->row()->order_id;
    }

    public function retrieve_input($package_id = null, $order_id = null, $update_price = 'true') {

        $origin = $this->orders_model->get_delivery($order_id)['origin'];
        $destination = $this->orders_model->get_delivery($order_id)['destination'];
        $volume = $this->input->post('package_width') * $this->input->post('package_height') * $this->input->post('package_lenght');
        $weight = $this->input->post('package_weight');
        $qty = $this->input->post('package_qty');

        if ($update_price == 'true') {
            $total = $this->prices_model->get_price($origin, $destination, $weight, $volume, 2, $qty);
        }else{
            $total = '';
        }

        $set_array = array(
            'package_id' => $package_id,
            'order_id' => $order_id,
            'det_order_total' => $total,
            'det_order_qty' => $qty,
            'det_order_sell_total' => $total,
            'det_order_package_parent' => $package_id
        );

        $this->insert($set_array);
    }

    private function sql_total($order_id) {
        $this->db->select('det_order_total, det_order_sell_total');
        $this->db->where('det_order.order_id', $order_id);
        $this->db->group_by('det_order_package_parent');
        return $this->db->get($this->table)->result_array();
    }

    public function get_total($order_id = null) {
        $total = 0;
        $result = $this->sql_total($order_id);
        if (!empty($result)) {
            foreach ($result as $key => $item) {
                $total += $item['det_order_total'];
            }
        }
        return $total;
    }

    public function get_total_sell($order_id = null) {
        $total = 0;
        $result = $this->sql_total($order_id);
        if (!empty($result)) {
            foreach ($result as $key => $item) {
                $total += $item['det_order_sell_total'];
            }
        }
        return $total;
    }
    
    public function update_package($where = array())
    {

    }

}
