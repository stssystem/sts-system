<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Trips_model extends CI_Model {

    private $table = 'trips';
    private $id = 'trip_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $result = $this->db->get($this->table)->row();

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);

        return $result;
    }

    public function get_joined_datatable($where_array = array(), $attributes = array()){
        
        $this->db->select('trips.trip_id, trips.route_id, routes.route_name, armadas.armada_name, userprofile.userprofile_fullname, trips.created_at');

        $this->get_joined();
        
        $this->db->order_by('trips.created_at', 'DESC');

        return $this->db->get($this->table);
        
    }

    private function get_joined($where_array = array(), $attributes = array()) {

        $this->db->join('routes', 'trips.route_id = routes.route_id', 'left');
        $this->db->join('armadas', 'trips.armada_id = armadas.armada_id', 'left');
        $this->db->join('user', 'trips.user_id = user.user_id', 'left');
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id', 'left');

        $this->db->where($where_array);
        $this->db->where('trips.deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        // return $this->db->get($this->table);
    }

    /** hendri's code starts here */

    /**
      public function get_armada_per_trip($branch_id) {
      $this->db->join('trips', 'trips.trip_id = det_trips.trip_id');
      $this->db->join('armadas', 'trips.armada_id = armadas.armada_id');
      $this->db->where('det_trips.det_trip_city_start', $branch_id);
      $query = $this->db->get('det_trips');
      return $query;
      } */
    //hendri's code starts here

    public function get_armada_transited() {
        $this->db->join('trips', 'trips.trip_id = det_trips.trip_id');
        $this->db->join('armadas', 'trips.armada_id = armadas.armada_id');
        $query = $this->db->get('det_trips');
        return $query;
    }

    public function get_armada_per_trip($origin, $destination) {

        $str = 'select * '
                . "from (select * from det_trips where det_trips.det_trip_city_start = {$origin}) as r "
                . "join (select * from det_trips where det_trips.det_trip_city_stop = {$destination}) as d  "
                . 'ON r.trip_id = d.trip_id '
                . 'join trips on r.trip_id = trips.trip_id '
                . 'join armadas on trips.armada_id = armadas.armada_id';

        return $this->db->query($str);
        
    }

    public function get_armada_visited($origin= 0) {

        $str = 'select * '
                . "from (select * from det_trips where det_trips.det_trip_city_start = {$origin}) as r "
                . 'join trips on r.trip_id = trips.trip_id '
                . 'join armadas on trips.armada_id = armadas.armada_id';

        return $this->db->query($str);
        
    }

    public function get_startend_branch($id = 0) {

        $this->db->join('det_trips', 'trips.trip_id = det_trips.trip_id');
        $this->db->where('trips.trip_id', $id);
        $result = $this->db->get($this->table)->result();

        $data = [];
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                if ($key == 0) {
                    $data['start'] = $value->det_trip_city_start;
                    $data['armada_id'] = $value->armada_id;
                }
                if ($key == (count($result) - 1)) {
                    $data['stop'] = $value->det_trip_city_stop;
                }
            }
        }

        return $data;
    }

}
