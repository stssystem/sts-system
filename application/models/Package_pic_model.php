<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package_pic_model extends CI_Model {

    private $table = 'package_pics';
    private $id = 'package_pic_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($set_array);
        $result = $this->db->get($this->table);

        $this->db->where($set_array);
        $this->db->delete($this->table);

        return $result;
    }

    public function delete_byparent($parent_id = 0) {

        $this->db->where('det_order.det_order_package_parent', $parent_id);
        $this->db->join('packages', 'packages.package_id = package_pics.package_id');
        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $result = $this->db->get($this->table)->result();

        $str = 'DELETE  package_pics '
                . 'FROM package_pics '
                . 'INNER JOIN packages ON packages.package_id = package_pics.package_id '
                . 'INNER JOIN det_order ON packages.package_id = det_order.package_id '
                . 'WHERE det_order.det_order_package_parent = ' . $parent_id;

        $this->db->query($str);

        if (!empty($result)) {
            foreach ($result as $key => $value) {
                unlink("./assets/upload/" . $value->package_pic);
            }
        }
    }

    /**
     * Function to delete all package 
     * picture from pickup order when session switched 
     */
    public function delete_pickup_foreign() {

        /** Get all package id parent list  */
        $this->load->model('packages_model');
        $result = $this->packages_model->get_parents_list(get_sessorder());

        /** Delete one by one of package picture by package parent id */
        if (!empty($result)) {
            foreach ($result as $key => $item) {
                $this->delete_byparent($item['parent_id']);
            }
        }
        
        /** Function return */
        return !empty($result) ? true : false;
        
    }

}
