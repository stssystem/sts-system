<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Regionals_model extends CI_Model {

    private $table = 'regionals';
    private $id = 'regional_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->select('regionals.regional_id as reg_id,regionals.created_at as regional_created_at, regionals.*, userprofile.*');

        $this->join_user();
        $this->where($where_array);

        $this->db->where('regionals.deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function get_regional_manager($id = 0) {
        $this->join_user();
        $this->where(['regionals.regional_id' => $id]);
        $result = $this->db->get($this->table)->row();
        return !empty($result) ? $result : null;
    }

    private function where($where = array()) {
        $this->db->where($where);
        $this->db->where($this->table . '.deleted_at', 0);
    }

    private function join_user() {
        $this->db->join('user', 'user.user_id = regionals.regional_manager', 'left');
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id', 'left');
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);

        $this->db->where($this->id, $set_array[$this->id]);
        return $this->db->get($this->table)->row();
    }

    public function get_branches_id($regional_id = 0) {
        $this->db->select('branch_id');
        $this->db->where('branch_regional_id', $regional_id);
        return $this->db->get('branches')->result_array();
    }

    public function get_branches_id_array($regional_id = 0){

        $data = $this->get_branches_id($regional_id);

        $result = [];
        if(!empty($data)){
            foreach($data as $key => $item){
                $result[] = $item['branch_id'];
            }
        }
        
        return $result;

    }

    public function get_branches_name($regional_id = 0) {
        
        $this->db->select('branch_name');
        $this->db->where('branch_regional_id', $regional_id);
        $data = $this->db->get('branches')->result_array();
        
        $result = [];
        if(!empty($data)){
            foreach($data as $key => $item){
                $result[] = $item['branch_name'];
            }
        }
        
        return $result;
        
    }

}
