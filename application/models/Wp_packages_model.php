<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Wp_packages_model extends CI_Model {

    private $table = 'wp_ss_pick_up_order_paket';
    private $id = 'id';
    private $db;

    public function __construct() {
        parent::__construct();
        $this->db = db_connect('web');
    }

    public function get($where_array = array(), $attributes = array()) {

        /** Do when select key available */
        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        $this->db->where($where_array);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array(), $updated_by = array()) {

        if (empty($updated_by)) {
            $this->db->where($this->id, $set_array[$this->id]);
        } else {
            $this->db->where(key($updated_by), $updated_by[key($updated_by)]);
        }

        $this->db->update($this->table, $set_array);
    }

}
