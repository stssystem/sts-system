<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Det_orders_extra_model extends CI_Model {

    private $table = 'det_orders_extra';
    private $id = 'det_order_extra_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($set_array);
        $this->db->delete($this->table);

        return $result;
    }

    public function delete_pickup_foreign(){
        $this->db->where('order_id', get_sessorder());
        $this->db->delete($this->table);
    }


}
