<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_model extends CI_Model {

    protected $table = 'orders';
    protected $id = 'order_id';
    protected $regional = array();

    public function __construct() {

        parent::__construct();
        $this->load->model('sync_package_model');
        $this->load->model('det_orders_extra_model');

        /** Generate branch of regional */
        $this->load->model('regionals_model', 'rm');
        $branch_id = $this->rm->get_branches_id(profile()->regional_id);
        if (!empty($branch_id)) {
            foreach ($branch_id as $key => $item) {
                $this->regional[] = $item['branch_id'];
            }
        }
    }

    public function get($where_array = array(), $attributes = array()) {

        /** Do when select key available */
        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);

        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array(), $update_by = array()) {

        if (empty($update_by)) {
            $this->db->where($this->id, $set_array[$this->id]);
        } else {
            $this->db->where($update_by);
        }

        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $result = $this->db->get($this->table);

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);

        return $result;
    }

    public function destroy($id = '') {

        $this->db->where($this->id, $id);
        $this->db->delete($this->table);

        return true;
    }

    public function get_joined($where_array = array(), $attributes = array(), $branch_based = false, $regional_based = false, $session_filter = false) {

        $str = 'orders.*, '
                . 'customers.customer_name, '
                . 'cities_origin.city_name as city_name_origin, '
                . 'cities_destination.city_name as city_name_destination, '
                . 'branches_origin.branch_name as branch_name_origin, '
                . 'branches_destination.branch_name as branch_name_destination, '
                . 'userprofile.userprofile_fullname,  '
                . 'orders.order_sell_total as order_sell_total, '
                . '(SELECT packages.package_status '
                . 'FROM packages, det_order '
                . 'WHERE packages.package_id = det_order.package_id LIMIT 1'
                . ') As package_status '
//                . 'packages.package_status AS package_status '
//                . 'packages.package_size AS package_size, '
//                . 'packages.package_weight AS package_weight, '
//                . 'packages.total_package AS total_package '
                . ' ';

//                . 'armadas.armada_name, '
//                . 'armadas.armada_license_plate, '
//                . 'SUM(det_orders_extra.det_order_extra_total) As extra_total,'
//                . 'COUNT(det_order.det_order_id) AS total_package,'
//                . 'SUM(packages.package_size) As total_package_size,'
//                . 'SUM(packages.package_weight) As total_package_weight ';

        $this->db->select($str);
        $this->get_join();

        $this->db->order_by('order_date', 'desc');

        $status = null;
        if (!key_exists('orders.order_status', $where_array)) {
            $this->db->where($where_array);
        } else {
            $this->status_based($where_array['orders.order_status']);
            $status = $where_array['orders.order_status'];
        }

        $this->db->where('orders.deleted_at', 0);

        /** Filter by session */
        if ($session_filter == true) {
            $this->session_filter();
        }

        if ($regional_based != false) {
            $regional_based != false ? $this->regional_based($regional_based, $status) : true;
        } else {
            $branch_based != false ? $this->branch_based($branch_based, $status) : true;
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        $this->db->group_by('orders.order_id');

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_total($where_array = array(), $attributes = array(), $branch_based = false, $regional_based = false, $session_filter = false) {

        $this->get_join();

        $status = null;
        if (!key_exists('orders.order_status', $where_array)) {
            $this->db->where($where_array);
        } else {
            $this->status_based($where_array['orders.order_status']);
            $status = $where_array['orders.order_status'];
        }

        $this->db->where('orders.deleted_at', 0);

        /** Filter by session */
        if ($session_filter == true) {
            $this->session_filter();
        }

        if ($regional_based != false) {
            $regional_based != false ? $this->regional_based($regional_based, $status) : true;
        } else {
            $branch_based != false ? $this->branch_based($branch_based, $status) : true;
        }

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    private function get_join() {

        $this->db->join('customers', 'orders.customer_id = customers.customer_id', 'left');
        $this->db->join('branches AS branches_origin', 'orders.order_origin = branches_origin.branch_id', 'left');
        $this->db->join('branches AS branches_destination', 'orders.order_destination = branches_destination.branch_id', 'left');
        $this->db->join('cities AS cities_origin', 'branches_origin.branch_city = cities_origin.city_id', 'left', 'left');
        $this->db->join('cities AS cities_destination', 'branches_destination.branch_city = cities_destination.city_id', 'left');
        $this->db->join('user', 'orders.staff_id = user.user_id', 'left');
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id', 'left');

        $this->db->join('('
                . 'SELECT '
                . 'packages.package_status AS package_status, '
                . 'det_order.order_id AS order_id '
                . 'FROM det_order, packages '
                . 'WHERE det_order.package_id = packages.package_id '
                . ') AS packages', 'packages.order_id = orders.order_id');
    }

    private function concat_like($data = '') {

        $str = 'CONCAT'
                . '(orders.order_id, " ", '
                . 'customers.customer_name, " ", '
                . 'cities_origin.city_name, " - ", '
                . 'cities_destination.city_name, " - ", '
                . 'from_unixtime(orders.order_date, "%d %b %Y")'
                . ')';

        $this->db->like($str, $data);
    }

    public function session_filter() {

        if ($this->session->userdata('filter')['date_start'] && $this->session->userdata('filter')['date_end']) {
            $start = $this->session->userdata('filter')['date_start'];
            $end = $this->session->userdata('filter')['date_end'];
            $this->db->where('orders.order_date >=', strtotime($start));
            $this->db->where('orders.order_date <=', strtotime($end));
        }

        if ($this->session->userdata('filter')['branch_origin'] && $this->session->userdata('filter')['branch_origin'] != 0) {
            $origin = $this->session->userdata('filter')['branch_origin'];
            $this->db->where('orders.order_origin', $origin);
        }

        if ($this->session->userdata('filter')['branch_destination'] && $this->session->userdata('filter')['branch_destination'] != 0) {
            $destination = $this->session->userdata('filter')['branch_destination'];
            $this->db->where('orders.order_destination', $destination);
        }
    }

    public function open() {

        /** When pickup is available update order_id */
        if (pickup_isset()) {
            $this->pickup_open();
        }

        /** Check wheter open order is exist */
        $open = $this->get([
                    'order_id' => get_sessorder(),
                    'order_status' => -1
                ])->row();

        if (count($open) > 0 && has_sessorder() == 1) {
            return $open->order_id;
        } else {
            if (pickup_isset()) {
                pickup_down();
                return $this->create_resi_number(get_sessorder());
            } else {
                return $this->create_resi_number();
            }
        }
    }

    public function delete_pickup_foreign() {
        $staff_id = profile()->user_id;
        $this->db->where('order_id', pickup_id_get());
        $this->db->where('order_status', -1);
        $this->db->where('staff_id', $staff_id);
        $this->db->delete($this->table);
    }

    public function close() {

        /** Delete data det_order_extra  */
        $this->det_orders_extra_model->delete_pickup_foreign();

        /** Delete data order */
        $this->delete_pickup_foreign();

        /** Delete package image */
        $this->package_pic_model->delete_pickup_foreign();

        /** Delete data packages */
        $this->packages_model->delete_pickup_foreign();

        /** Destroy session of pickup order */
        pickup_destroy();
    }

    public function pickup_open() {

        if (get_sessorder() != '') {
            $data = [
                'order_id' => pickup_id_get(),
                'order_manual_id' => pickup_id_get(),
                'order_status' => -1
            ];

            $this->update($data, ['order_id' => get_sessorder()]);
        } else {
            $this->create_resi_number(pickup_id_get());
        }

        set_sessorder(pickup_id_get());

        if (pickup_isunlock()) {
            $this->sync_package_model->execute();
            pickup_lock();
        }
    }

    public function generate_resi() {

        if (!empty(branch())) {
            $order_id = branch()['branch_code'] . rand(1000000000, 9999999999);
        } else {
            $order_id = '000' . rand(1000000000, 9999999999);
        }

        return $order_id;
    }

    public function create_resi_number($manual_number = '') {

        if (!empty(branch())) {
            $order_id = branch()['branch_code'] . rand(1000000000, 9999999999);
        } else {
            $order_id = '000' . rand(1000000000, 9999999999);
        }

        if ($manual_number != '') {
            $order_id = $manual_number;
        }

        set_sessorder($order_id);

        $data = [
            'order_id' => (string) $order_id,
            'order_status' => -1,
            'staff_id' => profile()->user_id
        ];

        $this->insert($data);

        return $order_id;
    }

    public function status_based($status) {

        if ($status == 0) {
            $this->db->where("orders.order_status", 0);
        }

        if ($status == 1) {
            $this->db->where("(orders.order_status = 0 OR orders.order_status = 1)");
            $this->db->where("package_status <", 5);
        }

        if ($status == 2) {
            $this->db->where("orders.order_status", 1);
            $this->db->where("package_status", 5);
        }

        if ($status == 3) {
            $this->db->where("orders.order_status", 1);
            $this->db->where("package_status", 7);
        }

        if ($status == 4) {
            $this->db->where("orders.order_status", 1);
        }

        if ($status == 5) {
            $this->db->where("orders.order_status", 2);
        }

//        if ($status == 2 || $status == 3) {
//            $this->db->where("orders.order_status", $status);
//        }
    }

    public function branch_based($branch_id = 0, $status = null) {

        if ($status == null) {
            $this->db->where("orders.order_origin", $branch_id);
        }

        if ($status == 0) {
            $this->db->where("orders.order_origin", $branch_id);
        }

        if ($status == 4) {
            $this->db->where("orders.order_origin", $branch_id);
        }

        if ($status == 1 || $status == 2 || $status == 3) {
            $this->db->where("orders.order_destination", $branch_id);
        }
    }

    public function regional_based($regional_id = 0, $status = null) {

        //  Start condition 
        if (empty($this->regional)) {
            $this->regional = array(NULL);
        }

        if ($status == null) {
            $this->db->where_in("orders.order_origin", $where);
        }

        if ($status == 0) {
            $this->db->where_in("orders.order_origin", $where);
        }

        if ($status == 4) {
            $this->db->where_in("orders.order_origin", $where);
        }

        if ($status == 1 || $status == 2 || $status == 3) {
            $this->db->where_in("orders.order_destination", $where);
        }
    }

    private function generate_regional_branch($regional_id) {

        $this->load->model('regionals_model');
        $branch_id = $this->regionals_model->get_branches_id($regional_id);
    }

    public function get_join_package($resi_number = 0) {
        $this->db->select('orders.*, packages.package_status as package_status');
        $this->db->join('det_order', 'orders.order_id = det_order.order_id');
        $this->db->join('packages', 'det_order.package_id = packages.package_id');
        $this->db->where('orders.order_id', $resi_number);
        $this->db->group_by('orders.order_id');
        return $this->db->get($this->table);
    }

    public function get_delivery($order_id = null) {
        $this->db->select(''
                . 'orders.order_id AS order_id, '
                . 'orders.order_origin AS origin, '
                . 'orders.order_destination AS destination'
                . '');
        $this->db->where('orders.order_id', $order_id);
        $result = $this->db->get($this->table)->row_array();
        return $result;
    }

    public function update_total_price($order_id = null) {

        /** Load model resource */
        $this->load->model('det_orders_model');

        $set_array = [
            'order_total' => $this->det_orders_model->get_total($order_id),
            'order_sell_total' => $this->det_orders_model->get_total_sell($order_id),
        ];

        $this->db->where($this->id, $order_id);
        $this->db->update($this->table, $set_array);

        return true;
    }

    public function update_status($order_id, $status) {
        $set_array = ['order_status' => $status];
        $this->db->where($this->id, $order_id);
        $this->db->update($this->table, $set_array);
        return true;
    }

    public function update_load_date($order_id, $date) {
        $set_array = ['order_load_date' => $date];
        $this->db->where($this->id, $order_id);
        $this->db->update($this->table, $set_array);
        return true;
    }

    public function update_unload_date($order_id, $date) {
        $set_array = ['order_unload_date' => $date];
        $this->db->where($this->id, $order_id);
        $this->db->update($this->table, $set_array);
        return true;
    }

    public function update_delivered_date($order_id, $date) {
        $set_array = ['order_delivered_date' => $date];
        $this->db->where($this->id, $order_id);
        $this->db->update($this->table, $set_array);
        return true;
    }

    public function update_received_date($order_id, $date) {
        $set_array = ['order_received_date' => $date];
        $this->db->where($this->id, $order_id);
        $this->db->update($this->table, $set_array);
        return true;
    }

    public function get_payment_type($order_id = null) {
        $data = $this->get(['order_id' => $order_id])->row();
        return isset($data->order_payment_type) ? $data->order_payment_type : null;
    }

    public function update_payment_status($order_id, $status) {
        $set_array = ['order_payment_status' => $status];
        $this->db->where($this->id, $order_id);
        $this->db->update($this->table, $set_array);
        return true;
    }

    public function destroy_order_session($resi_number = '') {

        $this->close();

        $this->db->where($this->id, $resi_number);
        $this->db->delete($this->table);

        return true;
    }

    public function order_isexist($resi_number = '') {
        $this->db->where($this->id, $resi_number);
        $this->db->from($this->table);
        return $this->db->count_all_results() > 0 ? true : false;
    }

    public function update_last_location($data = array()) {

        /** Load all resource needed */
        $this->load->model('armadas_model');
        $this->load->model('locations_update_model');

        /** Update order */
        if (!empty($data)) {
            foreach ($data as $key => $item) {
                $armada_id = $this->armadas_model->get_armada_id($item['uniqueid']);
                $this->locations_update_model->update_last_location($armada_id, $item[1] . ',' . $item[2]);
            }
        }
    }

    public function count_regional_order($regional_id) {
        $this->db->select('count(*) As total');
        $this->db->join('branches as branch_origin', 'orders.order_origin = branch_origin.branch_id', 'left');
        $this->db->join('regionals', 'branch_origin.branch_regional_id = regionals.regional_id', 'left');
        $this->db->where('regionals.regional_id', $regional_id);
        $count = $this->db->get($this->table)->row();
        return isset($count->total) ? $count->total : '';
    }

    public function get_order_id($order_manual = '') {
        $this->db->select('order_id');
        $this->db->where('order_manual_id', $order_manual);
        $this->db->or_where('order_id', $order_manual);
        $data = $this->db->get($this->table)->row();
        return isset($data->order_id) ? $data->order_id : NULL;
    }

    public function is_franco($order_id = 0) {

        $this->db->select('order_payment_type');
        $this->db->where('order_id', $order_id);

        $data = $this->db->get($this->table)->row();

        if (isset($data->order_payment_type)) {
            return $data->order_payment_type == 2 ? true : false;
        } else {
            return false;
        }
    }

    public function search($id) {

        $str = 'orders.*, '
                . 'customers.customer_name, '
                . 'cities_origin.city_name as city_name_origin, '
                . 'cities_destination.city_name as city_name_destination, '
                . 'branches_origin.branch_name as branch_name_origin, '
                . 'branches_destination.branch_name as branch_name_destination, '
                . 'userprofile.userprofile_fullname,  '
                . 'orders.order_sell_total as order_sell_total, '
                . '(SELECT packages.package_status '
                . 'FROM packages, det_order '
                . 'WHERE packages.package_id = det_order.package_id LIMIT 1'
                . ') As package_status '
                . ' ';

        $this->db->select($str);
        $this->get_join();

        $this->db->order_by('order_date', 'desc');

        $this->db->where('orders.deleted_at', 0);

        $this->db->where('orders.order_id', $id);
        $this->db->or_where('orders.order_manual_id', $id);

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_address_text($order_id = '') {
        $this->db->select('order_origin_text, order_destination_text');
        $this->db->where('order_id', $order_id);
        return $this->db->get($this->table)->row();
    }

    public function get_packages($order_id = '') {
        $this->db->select('packages.package_id AS package_id');
        $this->db->join('det_order', 'packages.package_id = det_order.package_id');
        $this->db->where('det_order.order_id', $order_id);
        return $this->db->get('packages')->result();
    }

    public function get_detail($order_id = '') {
        $this->db->where($this->id, $order_id);
        return $this->db->get($this->table)->row();
    }

    public function get_package_info($order_id = '') {

        $this->db->select('SUM(packages.package_weight) AS weight');
        $this->db->select('SUM(packages.package_size) / 1000000 AS size');
        $this->db->select('COUNT(packages.package_id) AS koli');

        $this->db->join('det_order', 'packages.package_id = det_order.package_id');

        $this->db->where('det_order.order_id', $order_id);
        return $this->db->get('packages')->row();
    }

    // Temporary function 
    public function get_mistake_address() {
        $this->db->select('order_id');
        $this->db->like('order_destination_text', 'province_id_destination');
        return $this->db->get($this->table)->result();
    }

}
