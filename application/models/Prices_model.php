<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prices_model extends CI_Model {

    private $table = 'prices';
    private $id = 'price_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        $this->db->where($where_array);
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $result = $this->db->get($this->table)->row();

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);

        return $result;
    }

    public function destroy($id = 0) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    public function get_joined($where_array = array(), $attributes = array()) {

        $str = 'prices.*, branches_origin.branch_name as branch_origin_name, '
                . 'branches_destination.branch_name as branch_destination_name, '
                . 'cities_origin.city_name as city_origin_name, '
                . 'cities_destination.city_name as city_destination_name, '
                . 'cities_origin.city_id as city_origin_id, '
                . 'cities_destination.city_id as city_destination_id ';

        $this->db->select($str);

        $this->db->join('branches as branches_origin', 'prices.price_branch_origin_id = branches_origin.branch_id');
        $this->db->join('branches as branches_destination', 'prices.price_branch_destination_id = branches_destination.branch_id');
        $this->db->join('cities as cities_origin', 'branches_origin.branch_city = cities_origin.city_id');
        $this->db->join('cities as cities_destination', 'branches_destination.branch_city = cities_destination.city_id');

        $this->db->where($where_array);
        $this->db->where('prices.deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        if (key_exists('group_by', $attributes)) {
            $this->db->group_by($attributes['group_by']);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function delete_joined($set_array = array()) {

        $this->db->join('branches as branches_origin', 'prices.price_branch_origin_id = branches_origin.branch_id');
        $this->db->join('branches as branches_destination', 'prices.price_branch_destination_id = branches_destination.branch_id');
        $this->db->join('cities as cities_origin', 'branches_origin.branch_city = cities_origin.city_id');
        $this->db->join('cities as cities_destination', 'branches_destination.branch_city = cities_destination.city_id');

        $this->db->where('r.city_origin_id', $set_array['cities_origin.city_id']);
        $this->db->where('r.city_destination_id', $set_array['cities_destination.city_id']);

        $str = "(select "
                . "cities_origin.city_id as city_origin_id, "
                . "cities_destination.city_id as city_destination_id "
                . 'from prices '
                . 'join branches as branches_origin on prices.price_branch_origin_id = branches_origin.branch_id '
                . 'join branches as branches_destination on prices.price_branch_destination_id = branches_destination.branch_id '
                . 'join cities as cities_origin on branches_origin.branch_city = cities_origin.city_id '
                . 'join cities as cities_destination on branches_destination.branch_city = cities_destination.city_id) As r';

        $this->db->delete($str);
    }

    public function get_price_list($where = array()) {

        $where_str = "((prices.price_branch_origin_id = {$where['branch_id_origin']} AND prices.price_branch_destination_id = {$where['branch_id_destination']}) OR ";
        $where_str .= "(prices.price_branch_origin_id = {$where['branch_id_destination']} AND prices.price_branch_destination_id = {$where['branch_id_origin']})) AND ";
        $where_str.= "deleted_at = 0";

        $this->db->where($where_str);

        return $this->db->get($this->table);
        
    }

    public function get_price($origin = 0, $destination = 0, $weight = 0, $volume = 0, $qty = 1) {

        /** Load model resource */
        $this->load->model('prices_model');

        /** main parameter */
        $weight = ceil(($weight * $qty));
        $volume = ($volume * $qty);
        $volume = ceil($volume / 400);

        $final_weight = ($weight > $volume) ? $weight : $volume;

        $result = [];
        if ($origin != '' && $destination != '') {
            $where = [
                'branch_id_origin' => $origin,
                'branch_id_destination' => $destination
            ];
            $result = $this->prices_model->get_price_list($where)->row();
        }

        $minimum = isset($result->price_minimum) ? (int) $result->price_minimum : 0;
        $price_kg_retail = isset($result->price_kg_retail) ? (int) $result->price_kg_retail : 0;

        if ($final_weight <= 5) {
            return $minimum;
        } else {
            return ($minimum + (($final_weight - 5) * $price_kg_retail));
        }
        
    }
    
  

}
