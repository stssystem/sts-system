<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pickup_orders_model extends CI_Model {

    private $table = 'wp_ss_pick_up_order';
    private $id = 'id';
    private $db;

    public function __construct() {
        parent::__construct();
        $this->db = db_connect('web');
        $this->load->model('wp_packages_model');
    }

    public function get($where_array = array(), $attributes = array(), $branch_based = false) {

        $this->db->where($where_array);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        $branch_based != false ? $this->branch_based($branch_based) : true;

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_joined($where_array = array(), $attributes = array(), $branch_based = false) {

        $this->db->where($where_array);

        $this->join_kabupaten();

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        $branch_based != false ? $this->branch_based($branch_based) : true;

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_total($where_array = array(), $attributes = array(), $branch_based = false) {

        $this->db->where($where_array);

        $this->join_kabupaten();

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        $branch_based != false ? $this->branch_based($branch_based) : true;

        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    public function get_row($where_array = array(), $attributes = array()) {

        $str = 'wp_ss_pick_up_order.*, '
                . 'kbp_asal.Nama as kbp_asal_nama, '
                . 'kbp_tujuan.Nama as kbp_tujuan_nama, '
                . 'kbp_asal.IDKabupaten as kbp_asal_id, '
                . 'kbp_tujuan.IDKabupaten as kbp_tujuan_id ';

        $this->db->select($str);

        $this->join_kabupaten();

        $this->db->where($where_array);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        $query = $this->db->get($this->table);

        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
    }

    public function update($set_array = array()) {
        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, $set_array);
    }

    public function destroy($id = '') {

        $data = $this->get(['id' => $id])->row();

        $str = "DELETE wp_ss_pick_up_order, wp_ss_pick_up_order_paket "
                . "FROM wp_ss_pick_up_order "
                . "JOIN wp_ss_pick_up_order_paket "
                . "ON wp_ss_pick_up_order.id = wp_ss_pick_up_order_paket.ss_pick_up_order_id "
                . "WHERE wp_ss_pick_up_order.id = $id";

        $this->db->query($str);

        return $data;
    }

    public function branch_based($city_name = 0) {
        if (substr($city_name, 0, 4) == "KOTA") {
            $city_name = substr($city_name, 5);
        }
        $this->db->where("( kbp_asal.Nama like '%$city_name%' )");
    }

    public function join_kabupaten() {
        $this->db->join('wp_ss_cargo_kabupaten as c_kbp_asal', 'wp_ss_pick_up_order.asal = c_kbp_asal.Nama', 'left');
        $this->db->join('wp_ss_cargo_kabupaten as c_kbp_tujuan', 'wp_ss_pick_up_order.tujuan = c_kbp_tujuan.Nama', 'left');
        $this->db->join('wp_ss_kabupaten as kbp_asal', 'c_kbp_asal.IDKabupaten = kbp_asal.IDKabupaten', 'left');
        $this->db->join('wp_ss_kabupaten as kbp_tujuan', 'c_kbp_tujuan.IDKabupaten = kbp_tujuan.IDKabupaten', 'left');
    }

    public function join_kecamatan() {
        $this->db->join('wp_ss_kecamatan as kcm_asal', 'wp_ss_pick_up_order.kecamatan_asal = kcm_asal.Nama');
        $this->db->join('wp_ss_kecamatan as kcm_tujuan', 'wp_ss_pick_up_order.kecamatan_tujuan = kcm_tujuan.Nama');
        $this->db->join('wp_ss_kelurahan as klh_asal', 'wp_ss_pick_up_order.kelurahan_asal = klh_asal.Nama');
        $this->db->join('wp_ss_kelurahan as klh_tujuan', 'wp_ss_pick_up_order.kelurahan_tujuan = klh_tujuan.Nama');
    }

    public function str_select() {
        $str = 'wp_ss_pick_up_order.*, '
                . 'kbp_asal.Nama as kbp_asal_nama, '
                . 'kbp_tujuan.Nama as kbp_tujuan_nama,'
                . 'kcm_asal.IDKabupaten as kbp_asal_id, '
                . 'kcm_tujuan.IDKabupaten as kbp_tujuan_id, '
                . 'kcm_asal.IDKecamatan as kcm_asal_id, '
                . 'kcm_tujuan.IDKecamatan as kcm_tujuan_id, '
                . 'klh_asal.IDKelurahan as klh_asal_id, '
                . 'klh_tujuan.IDKelurahan as klh_tujuan_id';
        return $str;
    }

    public function get_id($no_resi) {
        return $this->get(['no_resi' => $no_resi])->row()->id;
    }

    public function update_pickup_status($id, $status) {
        $return = $this->get(['id' => $id])->row();
        $this->update([
            'id' => $id,
            'pickup_status' => $status
        ]);
        return $return;
    }

    private function concat_like($search = '') {

        $str = 'CONCAT'
                . '('
                . 'wp_ss_pick_up_order.no_resi, " ", '
                . 'wp_ss_pick_up_order.nama_pengirim, " ", '
                . 'wp_ss_pick_up_order.perusahaan, " ", '
                . 'wp_ss_pick_up_order.email_pengirim, " ", '
                . 'wp_ss_pick_up_order.telp_pengirim, " ", '
                . 'wp_ss_pick_up_order.alamat_pengirim, " ", '
                . 'wp_ss_pick_up_order.kabupaten_pengirim, " ", '
                . 'wp_ss_pick_up_order.asal, " ", '
                . 'wp_ss_pick_up_order.tujuan, " ", '
                . 'wp_ss_pick_up_order.jumlah_paket, " ", '
                . 'wp_ss_pick_up_order.total_harga, " ", '
                . 'wp_ss_pick_up_order.waktu_pengambilan_tanggal, "-", '
                . 'wp_ss_pick_up_order.waktu_pengambilan_jam'
                . ')';

        $this->db->like($str, $search);
    }

    public function get_report_logistik_pickup_orders($where_array = array(), $attributes = array(), $column_export = array()) 
    {
        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select('wp_ss_pick_up_order.asal , count(wp_ss_pick_up_order.no_resi) as total_order');
        }

        $this->db->where('wp_ss_pick_up_order.pickup_status', 2);
        $this->db->group_by('wp_ss_pick_up_order.asal');

        if (!empty($where_array)) {
            $this->db->where($where_array);
        } 

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Order by */
        if (key_exists('order', $attributes)) {
            if (!empty($attributes['order'])) {
                foreach ($attributes['order'] as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }

        $query = $this->db->get($this->table);

        return $query;
        
    }

    public function pickup_detail_get_orders_detail($attr = array(), $limit = 10, $index = 0, $search = '')
    {

      $this->db->select('wp_ss_pick_up_order.*');
       
        $this->db->where('wp_ss_pick_up_order.pickup_status', 2);

        $search != '' ? $this->pickup_detail_search($search) : false;
        $this->session->userdata('report_pickup_orders') != '' ? $this->pickup_detail_session_filter() : false;

        // key_exists('order', $attr) ? $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']) : false;

        $this->pickup_detail_sql_base($attr);

        $this->db->limit($limit, $index);
        
        return $query = $this->db->get($this->table);
        
    }

    public function pickup_detail_get_total($attr = array(), $search = '') {

        $this->db->select('count(no_resi) AS total');

        $search != '' ? $this->pickup_detail_search($search) : false;
        $this->session->userdata('report_pickup_orders') != '' ? $this->pickup_detail_session_filter() : false;

        $this->pickup_detail_sql_base($attr);
        
        $data = $this->db->get($this->table)->row();

        return $data->total;
        
    }

    private function pickup_detail_search($search = '') {

        $str = 'CONCAT('
        . 'wp_ss_pick_up_order.no_resi, " ", '
        . 'wp_ss_pick_up_order.perusahaan, " ", '
        . 'wp_ss_pick_up_order.alamat_pengirim, " ", '
        . 'wp_ss_pick_up_order.asal, " ", '
        . 'wp_ss_pick_up_order.tujuan, " ", '
        . 'wp_ss_pick_up_order.jumlah_paket, " ", '
        . ')';

        $this->db->like($str, $search);
        
    }

    private function pickup_detail_sql_base($attr = array()){
        if(key_exists('asal', $attr)){
            $this->db->where('asal', $attr['asal']);
        }
    }

     public function pickup_detail_session_filter() {
        if ($this->session->userdata('report_pickup_orders')['date_start'] && $this->session->userdata('report_pickup_orders')['date_end']) {
            $start  = $this->session->userdata('report_pickup_orders')['date_start'];
            $end    = $this->session->userdata('report_pickup_orders')['date_end'];
            $this->db->where('wp_ss_pick_up_order.waktu_pengambilan_tanggal >=', strtotime($start));
            $this->db->where('wp_ss_pick_up_order.waktu_pengambilan_tanggal <=', strtotime($end));
        }
    }

    public function pickup_detail_order($column = 0, $mode = 'desc') {

        switch ($column) {
            case 1 :
            $this->db->order_by('sale_comission_branch_destination_detail.order_date', $mode);
            break;
            case 2 :
            $this->db->order_by('sale_comission_branch_destination_detail.order_id', $mode);
            break;
            case 3 :
            $this->db->order_by('sale_comission_branch_destination_detail.customer_name', $mode);
            break;
            case 4 :
            $this->db->order_by('sale_comission_branch_destination_detail.branch_name_origin', $mode);
            break;
            case 5 :
            $this->db->order_by('sale_comission_branch_destination_detail.branch_name_destination', $mode);
            break;
            case 6 :
            $this->db->order_by('sale_comission_branch_destination_detail.omset', $mode);
            break;
            case 7 :
            $this->db->order_by('sale_comission_branch_destination_detail.omset', $mode);
            break;
        }
    }

}
