<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package_edit_model extends CI_Model {

  private $table = 'package_edit';
  private $id = 'package_edit_id';

  public function __construct() {
    parent::__construct();
  }

  public function insert($set_array = array()) {
    $this->db->insert($this->table, $set_array);
    return $this->db->insert_id();
  }

  public function approve($set_array = array())
  {
    $this->db->where($this->id, $set_array[$this->id]);
    $this->db->update($this->table, $set_array);
  }

  public function get_package_edit($where_array = array()) {

    if (!empty($where_array)) {
      $this->db->where($where_array);
    } 

    $this->db->where('package_edit.order_id !=', null);
    $this->db->where('package_edit.package_id !=', null);
    
    $this->db->select('
      package_edit.package_edit_id as package_edit_id,
      package_edit.package_edit_date_updated as package_edit_date_updated,
      package_edit.package_edit_date_approved as package_edit_date_approved,
      package_edit.order_id as order_id,
      package_edit.branch_id as branch_id,
      package_edit.regional_id as regional_id,
      customers.customer_name as customer_name,
      branches_origin.branch_name as branch_origin_name,
      branches_destination.branch_name as branch_destination_name,
      package_edit.user_staff_id as request_user_staff_id,
      user_requested_staff.username as request_staff_name,
      user_requested_staff.user_type_id as request_user_type_id,
      requested_user_types.user_type_name as request_user_type_name,
      requested_userprofile.branch_id as request_user_branch_id,
      requested_user_branches.branch_name as request_user_branch_name,
      user_approved_staff.username as approved_staff_name,
      approved_user_types.user_type_name as approved_user_type_name,
      package_edit.order_status as order_status,
      package_edit.package_id as package_id,
      package_edit.package_edit_title as package_edit_title,
      package_edit.package_edit_type as package_edit_type,
      package_edit.package_edit_content as package_edit_content,
      package_edit.package_edit_width as package_edit_width,
      package_edit.package_edit_height as package_edit_height,
      package_edit.package_edit_lenght as package_edit_lenght,
      package_edit.package_edit_weight as package_edit_weight,
      package_edit.package_edit_size as package_edit_size, 
      package_edit.package_qty as package_qty,
      package_edit.package_notes as package_notes,
      ');
    
    $this->db->order_by('package_edit.package_edit_date_updated', 'desc');
    $this->db->join('user AS user_requested_staff', 'user_requested_staff.user_id = package_edit.user_staff_id', 'left');
    $this->db->join('user_types AS requested_user_types', 'user_requested_staff.user_type_id = requested_user_types.user_type_id', 'left');
    $this->db->join('userprofile AS requested_userprofile', 'user_requested_staff.user_id = requested_userprofile.userprofile_id', 'left');
    $this->db->join('branches AS requested_user_branches', 'requested_userprofile.branch_id = requested_user_branches.branch_id', 'left');
    $this->db->join('user AS user_approved_staff', 'user_approved_staff.user_id = package_edit.user_approved_staff_id', 'left');
    $this->db->join('user_types AS approved_user_types', 'user_approved_staff.user_type_id = approved_user_types.user_type_id', 'left');
    $this->db->join('orders', 'orders.order_id = package_edit.order_id', 'left');
    $this->db->join('customers', 'orders.customer_id = customers.customer_id', 'left');
    $this->db->join('branches AS branches_origin', 'orders.order_origin = branches_origin.branch_id', 'left');
    $this->db->join('branches AS branches_destination', 'orders.order_destination = branches_destination.branch_id', 'left');
    $query = $this->db->get($this->table);
    return $query;
  }

  public function get_order_status($where_array = array())
  {
    $this->db->select('order_status');
    $this->db->where($where_array);
    $this->db->join('user', 'user.user_id = package_edit.user_staff_id', 'left');
    $query = $this->db->get($this->table);
    return $query;
  }

  public function get_notif($where_array = array())
  {
    if (!empty($where_array)) {
      $this->db->where($where_array);
    } 
    $this->db->select('count(*) As total');
    $count = $this->db->get($this->table)->row();
    return isset($count->total) ? $count->total : '';
  }


}
