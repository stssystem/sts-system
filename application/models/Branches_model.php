<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Branches_model extends CI_Model {

    private $table = 'branches';
    private $id = 'branch_id';

    public function __construct() {
        parent::__construct();
    }

    public function get($where_array = array(), $attributes = array()) {

        if (key_exists('select', $attributes)) {
            $this->db->select($attributes['select']);
        }

        if (is_array($where_array)) {
            $this->db->where($where_array);
        }
        $this->db->where('deleted_at', 0);

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        $query = $this->db->get($this->table);
        return $query;
    }

    public function insert($set_array = array()) {
        $this->db->insert($this->table, $set_array);
        return $this->db->insert_id();
    }

    public function update($set_array = array(), $update_by = array()) {

        if (empty($update_by)) {
            $this->db->where($this->id, $set_array[$this->id]);
        } else {
            $this->db->where(key($update_by), $update_by[key($update_by)]);
        }

        $this->db->where('deleted_at', 0);

        $this->db->update($this->table, $set_array);
    }

    public function delete($set_array = array()) {

        $this->db->where($this->id, $set_array[$this->id]);
        $result = $this->db->get($this->table)->row();

        $this->db->where($this->id, $set_array[$this->id]);
        $this->db->update($this->table, ['deleted_at' => time()]);

        return $result;
    }

    public function get_joined($where_array = array(), $attr = array(), $attributes = array()) {

        $str = 'branches.*, user.*, cities.*, provinces.*, regionals.*, '
        . 'userprofile.userprofile_fullname, '
        . 'userprofile.userprofile_photo, '
        . 'CONCAT(branches.branch_name, " - ", branches.branch_code) AS branch_name_code'
        . '';

        $this->db->select($str);

        $this->db->join('cities', 'branches.branch_city = cities.city_id', 'left');
        $this->db->join('provinces', 'cities.province_id = provinces.province_id', 'left');
        $this->db->join('user', 'branches.branch_manager = user.user_id', 'left');
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id', 'left');
        $this->db->join('regionals', 'branches.branch_regional_id = regionals.regional_id', 'left');

        $this->session->userdata('branches') != '' ? $this->session_filter() : false;

        /** Do when limit key availabe */
        if (key_exists('limit', $attributes)) {
            $index = key_exists('index', $attributes) ? $attributes['index'] : 0;
            $this->db->limit($attributes['limit'], $index);
        }

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        if (key_exists('order', $attr)) {
            $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']);
        }

        if (!empty($where_array)) {
            $this->db->where($where_array);
        }
        $this->db->where('branches.deleted_at', 0);

        $query = $this->db->get($this->table);
        return $query;
    }

    public function get_joined_cities($where_array = array()) {

        $this->db->select('branch_id, CONCAT(city_name, " - ", branch_name ) AS branch_name', FALSE);
        $this->db->join('cities', 'branches.branch_city = cities.city_id');

        $this->db->where($where_array);
        $this->db->where('branches.deleted_at', 0);

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_joined_address($where_array = array()) {

        $this->db->join('cities', 'branches.branch_city = cities.city_id', 'left');
        $this->db->join('districts', 'cities.city_id = districts.city_id', 'left');
        $this->db->join('villages', 'districts.districts_id = villages.district_id', 'left');

        $this->db->where($where_array);
        $this->db->where('branches.deleted_at', 0);

        $query = $this->db->get($this->table);

        return $query;
    }

    public function get_geobranch($branch_id = '') {
        return $this->get(['branch_id' => $branch_id])->row()->branch_geo;
    }

    public function get_branch_name($branch_id = 0) {
        $this->db->select('branch_name');
        $this->db->where('branch_id', $branch_id);
        $data = $this->db->get($this->table)->row();
        return isset($data->branch_name) ? $data->branch_name : '';
    }

     public function get_branch_total($where_array = array(), $attributes = array()) {

         $str = 'branches.*, user.*, cities.*, provinces.*, regionals.*, '
        . 'userprofile.userprofile_fullname, '
        . 'userprofile.userprofile_photo, '
        . 'CONCAT(branches.branch_name, " - ", branches.branch_code) AS branch_name_code'
        . '';

        $this->db->select($str);

        $this->db->join('cities', 'branches.branch_city = cities.city_id');
        $this->db->join('provinces', 'cities.province_id = provinces.province_id');
        $this->db->join('user', 'branches.branch_manager = user.user_id', 'left');
        $this->db->join('userprofile', 'user.user_id = userprofile.user_id');
        $this->db->join('regionals', 'branches.branch_regional_id = regionals.regional_id', 'left');

        $this->session->userdata('branches') != '' ? $this->session_filter() : false;

        /** Do when limit key availabe */
        if (key_exists('concat_like', $attributes)) {
            $this->concat_like($attributes['concat_like']);
        }

        if (!empty($where_array)) {
            $this->db->where($where_array);
        }
        $this->db->where('branches.deleted_at', 0);

        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    private function concat_like($data = '') {

        $str = 'CONCAT'
                . '(branches.branch_id, " ", '
                . 'branches.branch_name, " ", '
                . 'branches.branch_code, " " '
                // . 'regionals.regional, " " '
                . ')';

        $this->db->like($str, $data);
    }

    private function order($column = 0, $mode = 'desc') {
        switch ($column) {
            case 0 :
                $this->db->order_by('branches.branch_id', $mode);
                break;
            case 1 :
                $this->db->order_by('branches.branch_code', $mode);
                break;
            case 2 :
                $this->db->order_by('branches.branch_name', $mode);
                break;
            case 3 :
                $this->db->order_by('regionals.regional', $mode);
                break;
            case 4 :
                $this->db->order_by('branches.branch_type', $mode);
                break;
            case 5 :
                $this->db->order_by('branches.branch_status', $mode);
                break;
            case 6 :
            $this->db->order_by('branches.created_at', $mode);
            break;

        }
    }

}
