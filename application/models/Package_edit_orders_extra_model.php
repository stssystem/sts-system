<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package_edit_orders_extra_model extends CI_Model {

  private $table = 'package_edit_orders_extra';
  private $id = 'pckg_edit_orders_extra_id';

  public function __construct() {
    parent::__construct();
  }

  public function insert($set_array = array()) {
    $this->db->insert($this->table, $set_array);
    return $this->db->insert_id();
  }

  public function approve($set_array = array())
  {
    $this->db->where($this->id, $set_array[$this->id]);
    $this->db->update($this->table, $set_array);
  }

  public function get_package_edit($where_array = array()) {

    if (!empty($where_array)) {
      $this->db->where($where_array);
    } 
    $this->db->select('
      package_edit_orders_extra.pckg_edit_orders_extra_id as pckg_edit_orders_extra_id,
      package_edit_orders_extra.date_updated as date_updated,
      package_edit_orders_extra.date_approved as date_approved,
      package_edit_orders_extra.order_id as order_id,
      package_edit_orders_extra.branch_id as branch_id,
      package_edit_orders_extra.regional_id as regional_id,
      package_edit_orders_extra.order_status as order_status,
      package_edit_orders_extra.orders_extra_id as orders_extra_id,
      package_edit_orders_extra.orders_extra_name as orders_extra_name,
      package_edit_orders_extra.orders_extra_total as orders_extra_total,
      package_edit_orders_extra.package_notes as package_notes,
      customers.customer_name as customer_name,
      branches_origin.branch_name as branch_origin_name,
      branches_destination.branch_name as branch_destination_name,
      package_edit_orders_extra.user_staff_id as request_user_staff_id,
      user_requested_staff.username as request_staff_name,
      requested_user_types.user_type_id as request_user_type_id,
      requested_user_types.user_type_name as request_user_type_name,
      requested_userprofile.branch_id as request_user_branch_id,
      requested_user_branches.branch_name as request_user_branch_name,
      user_approved_staff.username as approved_staff_name,
      approved_user_types.user_type_name as approved_user_type_name,
      ');
    $this->db->order_by('package_edit_orders_extra.date_updated', 'desc');
    $this->db->join('user AS user_requested_staff', 'user_requested_staff.user_id = package_edit_orders_extra.user_staff_id', 'left');
    $this->db->join('user_types AS requested_user_types', 'user_requested_staff.user_type_id = requested_user_types.user_type_id', 'left');
    $this->db->join('userprofile AS requested_userprofile', 'user_requested_staff.user_id = requested_userprofile.userprofile_id', 'left');
    $this->db->join('branches AS requested_user_branches', 'requested_userprofile.branch_id = requested_user_branches.branch_id', 'left');
    $this->db->join('user AS user_approved_staff', 'user_approved_staff.user_id = package_edit_orders_extra.user_approved_staff_id', 'left');
    $this->db->join('user_types AS approved_user_types', 'user_approved_staff.user_type_id = approved_user_types.user_type_id', 'left');
    $this->db->join('orders', 'orders.order_id = package_edit_orders_extra.order_id', 'left');
    $this->db->join('customers', 'orders.customer_id = customers.customer_id', 'left');
    $this->db->join('branches AS branches_origin', 'orders.order_origin = branches_origin.branch_id', 'left');
    $this->db->join('branches AS branches_destination', 'orders.order_destination = branches_destination.branch_id', 'left');
    $query = $this->db->get($this->table);
    return $query;
  }

  public function get_notif($where_array = array())
  {
    if (!empty($where_array)) {
      $this->db->where($where_array);
    } 
    $this->db->select('count(*) As total');
    $count = $this->db->get($this->table)->row();
    return isset($count->total) ? $count->total : '';
  }
  
}
