<?php

class Finance_report_model extends CI_Model {

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '', $column_export = array(), $date_start = '', $date_end = '') {

        if (!empty($column_export)) {
            foreach ($column_export as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select(
                    'finance_report.order_id AS order_id, '
                    . 'finance_report.order_status AS order_status, '
                    . 'finance_report.order_date AS order_date, '
                    . 'finance_report.order_use_tax AS order_use_tax, '
                    . 'finance_report.order_sell_total AS order_sell_total, '
                    . 'finance_report.customer_name AS customer_name, '
                    . 'finance_report.branch_name_origin as branch_name_origin, '
                    . 'finance_report.branch_name_destination as branch_name_destination, '
                    . 'finance_report.extra_total as extra_total '
            );
        }

        $this->default_date();

        if (!empty($date_start) && !empty($date_end)) {
            $this->db->where('finance_report.order_date >=', strtotime($date_start));
            $this->db->where('finance_report.order_date <=', strtotime($date_end));
        }

        if (!empty($date_start)) {
            $this->db->where('finance_report.order_date >=', strtotime($date_start));
        }

        if (!empty($date_end)) {
            $this->db->where('finance_report.order_date >=', strtotime($date_end));
        }

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('filter') != '' ? $this->session_filter() : false;

        // Order section
        key_exists('order', $attr) ? $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']) : false;

        $this->db->limit($limit, $index);

        return $this->db->get('finance_report');
    }

    public function get_total($search = '') {

        $this->db->select('count(finance_report.order_id) As total');

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('filter') != '' ? $this->session_filter() : false;

        $this->default_date();

        return $this->db->get('finance_report')->row()->total;
    }

    public function get_sum() {

        $this->db->select('SUM(CASE finance_report.order_use_tax WHEN 1 THEN  finance_report.order_sell_total ELSE  finance_report.order_sell_total  + (finance_report.order_sell_total * (1 / 100))   END) AS order_sell_total');
        $this->db->select('SUM(finance_report.extra_total) AS extra_total');

        $this->session->userdata('filter') != '' ? $this->session_filter() : false;

        $this->default_date();

        return $this->db->get('finance_report')->row();
    }

    public function get_download($column = array()) {

        if (!empty($column)) {
            foreach ($column as $key => $value) {
                $this->db->select($value);
            }
        } else {
            
            $this->db->select('finance_report.order_id AS `Nomor Resi`', false);
            $this->db->select('from_unixtime(finance_report.order_date, "%d %b %Y") AS `Tanggal Resi`', false);
            $this->db->select('finance_report.customer_name AS `Nama Pelanggan`', false);
            $this->db->select('finance_report.branch_name_origin AS `Asal`', false);
            $this->db->select('finance_report.branch_name_destination AS `Tujuan`', false);
            $this->db->select('finance_report.order_status AS `Status Order`', false);
            $this->db->select('finance_report.order_sell_total AS `Total`', false);
            $this->db->select('finance_report.extra_total AS `Biaya Tambahan`', false);
            $this->db->select('CASE finance_report.order_status '
                    . 'WHEN 0 THEN "Baru Diterima"  '
                    . 'WHEN 1 THEN "Dalam Proses"  '
                    . 'WHEN 2 THEN "Selesai" '
                    . 'WHEN 3 THEN "Belum Bayar"  '
                    . 'WHEN 4 THEN "Akan Sampai"  '
                    . 'END AS `Status Order`', false);
            
        }

        $this->default_date();

        if (!empty($date_start) && !empty($date_end)) {
            $this->db->where('finance_report.order_date >=', strtotime($date_start));
            $this->db->where('finance_report.order_date <=', strtotime($date_end));
        }

        if (!empty($date_start)) {
            $this->db->where('finance_report.order_date >=', strtotime($date_start));
        }

        if (!empty($date_end)) {
            $this->db->where('finance_report.order_date >=', strtotime($date_end));
        }

        $this->session->userdata('filter') != '' ? $this->session_filter() : false;

        return $this->db->get('finance_report');
    }

    private function search($search = '') {

        /** Not used temporary */
        $str = 'CONCAT('
                . 'finance_report.order_id, " ", '
                . 'finance_report.customer_name, " ", '
                . 'finance_report.branch_name_origin, " ", '
                . 'finance_report.branch_name_destination, " ", '
                . 'CASE finance_report.order_status '
                . 'WHEN 0 THEN "Baru Diterima"  '
                . 'WHEN 1 THEN "Dalam Proses"  '
                . 'WHEN 2 THEN "Selesai"  '
                . 'WHEN 3 THEN "Belum Bayar"  '
                . 'WHEN 4 THEN "Akan Sampai"  '
                . 'END , " ", '
                . 'from_unixtime(finance_report.order_date, "%d %b %Y %h:%i:%s")'
                . ')';

        $this->db->like($str, $search);
    }

    public function session_filter() {

        if ($this->session->userdata('filter')['date_start'] && $this->session->userdata('filter')['date_end']) {
            $start = $this->session->userdata('filter')['date_start'];
            $end = $this->session->userdata('filter')['date_end'];
            $this->db->where('finance_report.order_date >=', strtotime($start));
            $this->db->where('finance_report.order_date <=', strtotime($end));
        }

        if ($this->session->userdata('filter')['branch_origin_name'] && $this->session->userdata('filter')['branch_origin_name'] != '') {
            $origin = $this->session->userdata('filter')['branch_origin_name'];
            $this->db->where('finance_report.branch_name_origin', $origin);
        }

        if ($this->session->userdata('filter')['branch_destination_name'] && $this->session->userdata('filter')['branch_destination_name'] != '') {
            $destination = $this->session->userdata('filter')['branch_destination_name'];
            $this->db->where('finance_report.branch_name_destination', $destination);
        }
    }

    public function default_date() {
        if (!$this->session->userdata('filter')['date_start'] && !$this->session->userdata('filter')['date_end']) {
            $this->db->where('finance_report.order_date >=', strtotime(date('01-m-Y')));
            $this->db->where('finance_report.order_date <=', strtotime(date('t-m-Y')));
        }
    }

    public function order($column = 0, $mode = 'desc') {

        switch ($column) {
            case 0 :
                $this->db->order_by('finance_report.order_date', $mode);
                $this->db->order_by('finance_report.order_id', $mode);
                break;
            case 1 :
                $this->db->order_by('finance_report.customer_name', $mode);
                break;
            case 2 :
                $this->db->order_by('finance_report.branch_name_origin', $mode);
                break;
            case 3 :
                $this->db->order_by('finance_report.branch_name_destination', $mode);
                break;
            case 4 :
                $this->db->order_by('finance_report.order_sell_total', $mode);
                break;
            case 5 :
                $this->db->order_by('finance_report.extra_total', $mode);
                break;
            case 6 :
                $this->db->order_by('(finance_report.extra_total + finance_report.order_sell_total)', $mode);
                break;
            case 7 :
                $this->db->order_by('finance_report.order_status', $mode);
                break;
        }
    }

}
