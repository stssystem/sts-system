<?php

class Customers_transaction_detail_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '') {

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('customers_report_detail_filter') != '' ? $this->session_filter() : false;

        $this->sql_base($attr);
        $this->db->limit($limit, $index);

        return $this->db->get('customers_report_detail');
    }

    public function get_download($customer_id = 0) {

        $this->db->select('from_unixtime(customers_report_detail.order_date, "%d %b %Y") AS `Tanggal Transaksi`');
        $this->db->select('customers_report_detail.order_id AS `No Resi`');
        $this->db->select('customers_report_detail.branch_origin_name AS `Asal`');
        $this->db->select('customers_report_detail.branch_destination_name AS `Tujuan`');
        $this->db->select('customers_report_detail.total_koli AS `Jumlah Koli`');
        $this->db->select('CAST(customers_report_detail.total_weight AS DECIMAL(65,2)) AS `Total Berat`');
        $this->db->select('customers_report_detail.total_size AS `Total Volume`');

        $this->session->userdata('customers_report_detail_filter') != '' ? $this->session_filter() : false;

        $this->sql_base(['customer_id' => $customer_id]);

        return $this->db->get('customers_report_detail');
    }

    public function get_total($attr = array(), $search = '') {

        $this->db->select('count(*) AS total');

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('customers_report_detail_filter') != '' ? $this->session_filter() : false;

        $this->sql_base($attr);

        $data = $this->db->get('customers_report_detail')->row();

        return $data->total;
    }

    private function search($search = '') {

        $str = 'CONCAT('
                . 'customers_report_detail.order_id, " ", '
                . 'customers_report_detail.branch_origin_name, " ", '
                . 'customers_report_detail.branch_destination_name, " ", '
                . 'customers_report_detail.total_koli, " ", '
                . 'customers_report_detail.total_weight, " ", '
                . 'customers_report_detail.order_sell_total, " ", '
                . 'from_unixtime(customers_report_detail.order_date, "%d %b %Y")'
                . ')';

        $this->db->like($str, $search);
    }

    private function sql_base($attr = array()) {

        if (key_exists('customer_id', $attr)) {
            $this->db->where('customer_id', $attr['customer_id']);
        }

        // Default date
        if (!isset($this->session->userdata('customers_report_detail_filter')['date_start']) &&
                !isset($this->session->userdata('customers_report_detail_filter')['date_end'])) {
            $this->default_date();
        }
    }

    public function default_date() {
        $this->db->where('customers_report_detail.order_date >=', strtotime(date('01-m-Y')));
        $this->db->where('customers_report_detail.order_date <=', strtotime(date('t-m-Y')));
    }

    public function session_filter() {

        if ($this->session->userdata('customers_report_detail_filter')['date_start'] && $this->session->userdata('customers_report_detail_filter')['date_end']) {
            $start = $this->session->userdata('customers_report_detail_filter')['date_start'];
            $end = $this->session->userdata('customers_report_detail_filter')['date_end'];
            $this->db->where('customers_report_detail.order_date >=', $start);
            $this->db->where('customers_report_detail.order_date <=', $end);
        }

        if ($this->session->userdata('customers_report_detail_filter')['branch_origin_name'] && $this->session->userdata('customers_report_detail_filter')['branch_origin_name'] != '') {
            $destination = $this->session->userdata('customers_report_detail_filter')['branch_origin_name'];
            $this->db->where('customers_report_detail.branch_origin_name', $destination);
        }

        if ($this->session->userdata('customers_report_detail_filter')['branch_destination_name'] && $this->session->userdata('customers_report_detail_filter')['branch_destination_name'] != '') {
            $destination = $this->session->userdata('customers_report_detail_filter')['branch_destination_name'];
            $this->db->where('customers_report_detail.branch_destination_name', $destination);
        }
    }

}
