<?php

class Report_logistic_deliveries_3_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_data($attr = array(), $limit = 10, $index = 0, $search = '') {

        $this->db->select('*');

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('deliveries_3') != '' ? $this->session_filter() : false;

        key_exists('order', $attr) ? $this->order($attr['order'][0]['column'], $attr['order'][0]['dir']) : false;

        $this->sql_base($attr);

        $this->db->limit($limit, $index);
        
        return $this->db->get('report_logistic_deliveries_3_detail');
        
    }

    public function get_total($attr = array(), $search = '') {

        $this->db->select('count(order_id) AS total');

        $search != '' ? $this->search($search) : false;
        $this->session->userdata('deliveries_3') != '' ? $this->session_filter() : false;

        $this->sql_base($attr);
        
        $data = $this->db->get('report_logistic_deliveries_3_detail')->row();

        return $data->total;
        
    }

    private function search($search = '') {

        $str = 'CONCAT('
        . 'report_logistic_deliveries_3_detail.order_id, " ", '
        . 'report_logistic_deliveries_3_detail.note, " ", '
        . 'from_unixtime(report_logistic_deliveries_3_detail.created_at, "%d %b %Y %h:%i:%s")'
        . ')';

        $this->db->like($str, $search);
        
    }
    
    private function sql_base($attr = array()){
        if(key_exists('branch_id', $attr)){
            $this->db->where('branch_id', $attr['branch_id']);
        }
    }

    public function session_filter() {

        if ($this->session->userdata('deliveries_3')['date_start'] && $this->session->userdata('deliveries_3')['date_end']) {
            $start  = $this->session->userdata('deliveries_3')['date_start'];
            $end    = $this->session->userdata('deliveries_3')['date_end'];
            $this->db->where('report_logistic_deliveries_3_detail.created_at >=', strtotime($start));
            $this->db->where('report_logistic_deliveries_3_detail.created_at <=', strtotime($end));
        }

    }

    public function order($column = 0, $mode = 'desc') {

        switch ($column) {
            case 1 :
            $this->db->order_by('report_logistic_deliveries_3_detail.order_id', $mode);
            break;
            case 3 :
            $this->db->order_by('report_logistic_deliveries_3_detail.created_at', $mode);
            break;
            case 3 :
            $this->db->order_by('report_logistic_deliveries_3_detail.order_id', $mode);
            break;
            case 4 :
            $this->db->order_by('report_logistic_deliveries_3_detail.note', $mode);
            break;
        }
    }

}