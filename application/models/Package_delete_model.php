<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package_delete_model extends CI_Model {

  private $table = 'package_delete';
  private $id = 'package_delete_id';

  public function __construct() {
    parent::__construct();
  }

  public function insert($set_array = array()) {
    $this->db->insert($this->table, $set_array);
    return $this->db->insert_id();
  }

  public function approve($set_array = array())
  {
    $this->db->where($this->id, $set_array[$this->id]);
    $this->db->update($this->table, $set_array);
  }

  public function get_package_delete($where_array = array()) {

    if (!empty($where_array)) {
      $this->db->where($where_array);
    } 

    $this->db->where('package_delete.order_id !=', null);
    $this->db->where('package_delete.package_id !=', null);
    
    $this->db->select('
      package_delete.package_delete_id as package_delete_id,
      package_delete.date_updated as date_updated,
      package_delete.date_approved as date_approved,
      package_delete.order_id as order_id,
      package_delete.package_id as package_id,
      package_delete.branch_id as branch_id,
      package_delete.regional_id as regional_id,
      branches_origin.branch_name as branch_origin_name,
      branches_destination.branch_name as branch_destination_name,
      package_delete.user_staff_id as request_user_staff_id,
      user_requested_staff.username as request_staff_name,
      user_requested_staff.user_type_id as request_user_type_id,
      requested_user_types.user_type_name as request_user_type_name,
      requested_userprofile.branch_id as request_user_branch_id,
      requested_user_branches.branch_name as request_user_branch_name,
      user_approved_staff.username as approved_staff_name,
      approved_user_types.user_type_name as approved_user_type_name,
      package_delete.package_delete_status as package_delete_status,
      package_delete.package_notes as package_notes,
      packages.package_title as package_name,
      ');
    
    $this->db->order_by('package_delete.date_updated', 'desc');
    $this->db->join('user AS user_requested_staff', 'user_requested_staff.user_id = package_delete.user_staff_id', 'left');
    $this->db->join('user_types AS requested_user_types', 'user_requested_staff.user_type_id = requested_user_types.user_type_id', 'left');
    $this->db->join('userprofile AS requested_userprofile', 'user_requested_staff.user_id = requested_userprofile.userprofile_id', 'left');
    $this->db->join('branches AS requested_user_branches', 'requested_userprofile.branch_id = requested_user_branches.branch_id', 'left');
    $this->db->join('user AS user_approved_staff', 'user_approved_staff.user_id = package_delete.user_approved_staff_id', 'left');
    $this->db->join('user_types AS approved_user_types', 'user_approved_staff.user_type_id = approved_user_types.user_type_id', 'left');
    $this->db->join('orders', 'orders.order_id = package_delete.order_id', 'left');
    $this->db->join('branches AS branches_origin', 'orders.order_origin = branches_origin.branch_id', 'left');
    $this->db->join('branches AS branches_destination', 'orders.order_destination = branches_destination.branch_id', 'left');
    $this->db->join('packages', 'packages.package_id = package_delete.package_id', 'left');
    $query = $this->db->get($this->table);
    return $query;
  }

  public function get_package_delete_status($where_array = array())
  {
    $this->db->select('package_delete_status');
    $this->db->where($where_array);
    $this->db->join('user', 'user.user_id = package_delete.user_staff_id', 'left');
    $query = $this->db->get($this->table);
    return $query;
  }

  public function get_notif($where = array())
  {
    if (!empty($where)) {
      $this->db->where($where);
    } 
    $this->db->select('count(*) As total');
    $count = $this->db->get($this->table)->row();
    return isset($count->total) ? $count->total : '';
  }


}
