<?php

class Track_url {

    public function index() {

        $ci = & get_instance();

        if ($ci->session->userdata('user_meta') != '') {
            
            $data = array(
                'url' => $ci->uri->uri_string(),
                'username' => profile()->username,
                'date' => date('Y-m-d H:i:s')
            );

            $data['ajax'] = $ci->input->is_ajax_request() ? 'YES' : 'NO';
            $data['post_get'] = json_encode($_POST);

            $ci->db->insert('track_url', $data);
            
        }
    }

}
