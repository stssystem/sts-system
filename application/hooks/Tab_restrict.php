<?php

class Tab_restrict {

    private $boundary;
    private $except;

    public function __construct() {
        $this->boundary = ['orders/orders'];
        $this->except = ['welcome'];
    }

    public function index() {

        $ci = & get_instance();
        $ci->load->helper('urlstore');

        // Save last url
        if (!in_array($ci->uri->uri_string(), $this->except) && !$ci->input->is_ajax_request()) {
            set_last_url($ci->uri->uri_string());
        }

        // Unlock new order page
        if (!in_array($ci->uri->uri_string(), $this->boundary) && !in_array($ci->uri->uri_string(), $this->except)) {
            $this->unlock_page();
        }

        // Redirect to autoclose when exist tab
        if (!(isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0')) {
            if (url_exist($ci->uri->uri_string()) && !$ci->input->is_ajax_request()) {
                redirect('program/autoclose');
            }
        }

        // Store order page url
        if (in_array($ci->uri->uri_string(), $this->boundary) && !$ci->input->is_ajax_request() && !url_exist($ci->uri->uri_string())) {
            url_store($ci->uri->uri_string());
        }
        
    }

    private function unlock_page() {

        $ci = & get_instance();
        $ci->load->helper('urlstore');
        $urlarr_store = $ci->session->userdata('url-arr-temp');

        if (!$ci->input->is_ajax_request()) {
            if (is_array($urlarr_store) && key_exists(get_last_tab(), $urlarr_store) && $urlarr_store[get_last_tab()] != $ci->uri->uri_string()) {
                url_remove('orders/orders');
            }
        }
    }

}
