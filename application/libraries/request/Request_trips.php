<?php

class Request_trips extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'route_id',
                'label' => 'Rute',
                'rules' => 'required'
            ],
            [
                'field' => 'armada_id',
                'label' => 'Armada',
                'rules' => 'required'
            ],
            [
                'field' => 'armada_id',
                'label' => 'Armada',
                'rules' => 'required'
            ]
        );

        return $this->execute();
    }

}
