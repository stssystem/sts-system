<?php

class Request_prices extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'city_origin',
                'label' => 'Cabang / Kota Asal',
                'rules' => 'required'
            ], [
                'field' => 'city_destination',
                'label' => 'Cabang / Kota Tujuan',
                'rules' => 'required'
            ], [
                'field' => 'price_branch_origin_id',
                'label' => 'Cabang / Kota Asal'
            ], [
                'field' => 'price_branch_destination_id',
                'label' => 'Cabang / Kota Tujuan'
            ], [
                'field' => 'price_kg_retail',
                'label' => 'Harga / kg Retail',
                'rules' => 'required'
            ], [
                'field' => 'price_kg_partai',
                'label' => 'Harga / kg Partai',
                'rules' => 'required'
            ], [
                'field' => 'price_volume_retail',
                'label' => 'Harga / m3 Retail',
                'rules' => 'required'
            ], [
                'field' => 'price_volume_partai',
                'label' => 'Harga / m3 Partai',
                'rules' => 'required'
            ], [
                'field' => 'price_minimum',
                'label' => 'Harga Minimal',
                'rules' => 'required'
            ]
        );

        return $this->execute();
    }

}
