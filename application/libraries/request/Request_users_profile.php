<?php

class Request_users_profile extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'userprofile_fullname',
                'label' => 'Nama Lengkap',
                'rules' => 'required|max_length[100]'
            ], [
                'field' => 'userprofile_address',
                'label' => 'Alamat Lengkap',
            ], [
                'field' => 'userprofile_photo',
                'label' => 'Foto Profil',
            ], [
                'field' => 'branch_id',
                'label' => 'Cabang',
            ]
        );

        return $this->execute();
    }

}
