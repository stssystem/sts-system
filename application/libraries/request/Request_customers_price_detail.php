<?php

class Request_customers_price_detail extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation($attribute = null) {

        $this->config = array(
            [
                'field' => 'customer_price',
                'label' => 'Harga Langganan',
                'rules' => 'numeric|required'
            ]
        );

        if ($attribute != null) {
            $this->config = array_merge($this->config, $attribute);
        }

        return $this->execute();
    }

}
