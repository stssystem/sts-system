<?php

class Request_regionals extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'regional',
                'label' => 'Nama Regional',
                'rules' => 'required|max_length[100]'
            ]
        );

        return $this->execute();
    }

}
