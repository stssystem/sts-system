<?php

class Request_branches extends Request {

    public function __construct() {
        parent::__construct();
    }

    //validation branches
    public function validation() {

        $this->config = array(
            [
                'field' => 'branch_name',
                'label' => 'Nama Cabang',
                'rules' => 'required|max_length[100]'
            ], [
                'field' => 'branch_address',
                'label' => 'Alamat Cabang',
                'rules' => 'required|max_length[200]'
            ], [
                'field' => 'branch_geo',
                'label' => 'Lokasi Geografis'
            ], [
                'field' => 'branch_parent',
                'label' => 'Cabang Induk'
            ], [
                'field' => 'branch_manager',
                'label' => 'Kepala Cabang'
            ], [
                'field' => 'branch_status',
                'label' => 'Status',
                'rules' => 'required'
            ], [
                'field' => 'branch_city',
                'label' => 'Kota Cabang',
                'rules' => 'required'
            ], [
                'field' => 'branch_regional_id',
                'label' => 'Kota Regional',
                'rules' => 'required'
            ], [
                'field' => 'branch_phone',
                'label' => 'Nomor Telepon/HP Cabang',
                'rules' => 'numeric'
            ], [
                'field' => 'branch_email',
                'label' => 'Email Cabang',
                'rules' => 'valid_email'
            ]
        );

        return $this->execute();
    }

}
