<?php

class Request_routes extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'route_name',
                'label' => 'Nama Rute',
                'rules' => 'required|max_length[100]'
            ], [
                'field' => 'route_status',
                'label' => 'Status',
                'rules' => 'required'
            ], [
                'field' => 'det_route'
            ]
        );

        return $this->execute();
    }

}
