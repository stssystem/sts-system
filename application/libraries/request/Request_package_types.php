<?php

class Request_package_types extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'package_type_name',
                'label' => 'Nama Tipe Paket',
                'rules' => 'required|max_length[100]'
            ]
        );

        return $this->execute();
    }

}
