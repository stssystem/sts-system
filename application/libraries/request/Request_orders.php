<?php

class Request_orders extends Request {

    private $ci;

    public function __construct() {
        parent::__construct();
        $this->ci = & get_instance();
    }

    public function validation($attribute = array()) {

        $this->config = array(
            [
                'field' => 'customer_name',
                'label' => 'Nama Pengirim',
                'rules' => 'required|max_length[100]'
            ], [
                'field' => 'customer_web_id',
                'label' => 'ID Web',
                'rules' => 'numeric'
            ], [
                'field' => 'customer_address',
                'label' => 'Alamat Pengirim',
                'rules' => 'max_length[200]'
            ], [
                'field' => 'customer_code',
                'label' => 'Kode Pelanggan'
            ], [
                'field' => 'customer_phone',
                'label' => 'No. Telp',
                'rules' => 'max_length[30]'
            ], [
                'field' => 'customer_type',
                'label' => 'Tipe Customer'
            ], [
                'field' => 'customer_city',
                'label' => 'Kota',
            ], [
                'field' => 'customer_id_number',
                'label' => 'Nomor Pengenal'
            ], [
                'field' => 'customer_id_type',
                'label' => 'Tipe Pengenal'
            ], [
                'field' => 'customer_id_type_other',
                'label' => 'Tipe Pengenal',
                'rules' => 'max_length[50]'
            ], [
                'field' => 'customer_birth_date',
                'label' => 'Tanggal Lahir',
            ], [
                'field' => 'customer_freq_sending',
                'label' => 'Frekuensi Pengiriman Paket',
                'rules' => 'numeric'
            ], [
                'field' => 'customer_sending_quota',
                'label' => 'Jumlah Pengiriman Paket',
                'rules' => 'numeric'
            ], [
                'field' => 'customer_company_type',
                'label' => 'Jenis Perusahaan',
                'rules' => 'numeric'
            ], [
                'field' => 'customer_company_type_other',
                'label' => 'Jenis Perusahaan Lainnya',
                'rules' => 'max_length[50]'
            ], [
                'field' => 'customer_gender',
                'label' => 'Jenis Kelamin'
            ], [
                'field' => 'customer_referral',
                'label' => 'No Refferal',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'customer_company_name',
                'label' => 'Nama Perusahaan',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'customer_company_website',
                'label' => 'Nama Perusahaan',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'customer_company_telp',
                'label' => 'Nomor Telepon Perusahaan',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'customer_company_email',
                'label' => 'Alamat Email Perusahaan',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'customer_company_address',
                'label' => 'Alamat Perusahaan',
                'rules' => 'max_length[200]'
            ],
            /** Origin */
            [
                'field' => 'old_province_id_origin'
            ], [
                'field' => 'province_id_origin',
                'label' => 'Provinsi'
            ], [
                'field' => 'city_id_origin',
                'label' => 'Kota',
                'rules' => 'required'
            ], [
                'field' => 'districts_id_origin',
                'label' => 'Kecamatan',
            ], [
                'field' => 'village_id_origin',
                'label' => 'Kelurahan',
            ], [
                'field' => 'branch_id_destination',
                'label' => 'Cabang / Agen',
                'rules' => 'required'
            ], [
                'field' => 'order_origin_text',
                'label' => 'Detail Pengirim',
            ],
            /** Destination */
            [
                'field' => 'old_province_id_destination',
            ], [
                'field' => 'province_id_destination',
                'label' => 'Provinsi'
            ], [
                'field' => 'city_id_destination',
                'label' => 'Kota',
                'rules' => 'required'
            ], [
                'field' => 'districts_id_destination',
                'label' => 'Kecamatan',
            ], [
                'field' => 'village_id_destination',
                'label' => 'Kelurahan',
            ], [
                'field' => 'destination_name',
                'label' => 'Nama Tujuan',
            ], [
                'field' => 'destination_telp',
                'label' => 'Nomor Telp. Tujuan',
            ], [
                'field' => 'order_destination_text',
                'label' => 'Alamat Detail Tujuan',
            ], [
                'field' => 'order_manual_id',
                'label' => 'No Resi Manual',
                'rules' => 'required'
            ], [
                'field' => 'order_payment_type',
                'label' => 'Metode Pembayaran'
            ], [
                'field' => 'order_notes',
                'label' => 'Catatan Tambahan'
            ], [
                'field' => 'long_due_date',
                'label' => 'Lama Masa Jatuh Tempo'
            ], [
                'field' => 'order_due_date',
                'label' => 'Tanggal Jatuh Tempo'
            ], [
                'field' => 'origin_branch_code'
            ], [
                'field' => 'destination_branch_code'
            ], [
                'field' => 'order_date',
                'rules' => 'required'
            ]
        );

        // Custom validation
        if (empty(branch())) {
            $this->config[] = [
                'field' => 'branch_id_origin',
                'label' => 'Cabang / Agen',
                'rules' => 'required'
            ];
        }

        // Duplicate validation
        $this->ci->load->model('orders_model');
        $no_resi_manual_ = $this->ci->input->post('branch_code') . $this->ci->input->post('order_manual_id');

        $where = array();
        if (!key_exists('id', $attribute)) {
            $where = ['order_manual_id' => $no_resi_manual_];
        } else {
            $where = ['order_manual_id' => $no_resi_manual_, 'order_id <>' => $attribute['id']];
        }
        
        $result = $this->ci->orders_model->get($where)->result();
        if (count($result) > 0) {
            $this->config[] = [
                'field' => 'order_manual_temp',
                'rules' => 'required'
            ];
        }
        // End duplicate validation

        if (key_exists('add_validation', $attribute)) {
            $this->config = array_merge($this->config, $attribute['add_validation']);
        }

        return $this->execute();
    }

}
