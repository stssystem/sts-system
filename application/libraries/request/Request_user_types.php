<?php

class Request_User_Types extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'user_type_name',
                'label' => 'Tipe User',
                'rules' => 'required|max_length[30]'
            ]
        );

        return $this->execute();
    }

}
