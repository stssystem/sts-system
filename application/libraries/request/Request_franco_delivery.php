<?php

class Request_franco_delivery extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'order_id',
                'label' => 'Order Id',
                'rules' => 'required|max_length[50]'
            ],[
                'field' => 'armada',
                'label' => 'Armada',
                'rules' => 'required'
            ]
        );

        return $this->execute();
    }

}
