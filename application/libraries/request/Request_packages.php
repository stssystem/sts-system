<?php

class Request_packages extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'package_title',
                'label' => 'Nama Paket',
                'rules' => 'required|max_length[150]'
            ], [
                'field' => 'package_type',
                'label' => 'Tipe Paket',
                'rules' => 'required'
            ], [
                'field' => 'package_origin',
                'label' => 'Asal Paket',
                'rules' => 'required'
            ], [
                'field' => 'package_destination',
                'label' => 'Tujuan Paket',
                'rules' => 'required'
            ], [
                'field' => 'package_content',
                'label' => 'Isi Paket',
                'rules' => 'required|max[50000]'
            ],[
                'field' => 'package_width',
                'label' => 'Lebar Paket',
                'rules' => 'required'
            ],[
                'field' => 'package_height',
                'label' => 'Tinggi Paket',
                'rules' => 'required'
            ],[
                'field' => 'package_lenght',
                'label' => 'Panjang Paket',
                'rules' => 'required'
            ],[
                'field' => 'package_size',
                'label' => 'Ukuran Paket',
                'rules' => 'required'
            ],[
                'field' => 'package_weight',
                'label' => 'Berat Paket',
                'rules' => 'required'
            ]
        );

        return $this->execute();
    }

}
