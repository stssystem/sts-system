<?php

class Request_cities extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'city_name',
                'label' => 'Nama Kota',
                'rules' => 'required|max_length[100]'
            ]
        );

        return $this->execute();
    }

}
