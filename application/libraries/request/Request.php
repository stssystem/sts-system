<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Request {

    protected $CI;
    protected $validator;
    protected $config;
    protected $error_form = [];
    protected $session;
    protected $except = [];

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->library('form_validation');
        $this->validator = $this->CI->form_validation;
        $this->session = $this->CI->session;
    }

    protected function execute() {

        /** Except field rule */
        if (!empty($this->except)) {
            $this->set_except();
        }

        $this->validator->set_rules($this->config);

        $result = $this->validator->run();        
        
        /** Set session for error and old input */
        if (!$result)
            $this->set_session();

        return $result;
    }

    private function set_error() {

        $result = [];
        if (!empty($this->config)) {
            foreach ($this->config as $key => $value) {
                $err = form_error($value['field']);
                if ($err != '') {
                    $result[$value['field']] = $err;
                }
            }
        }
        
        return $result;
    }

    private function set_oldinput() {
        $result = [];
        if (!empty($this->config)) {
            foreach ($this->config as $key => $value) {
                $values = set_value($value['field']);
                if ($values != '') {
                    $result[$value['field']] = $values;
                }
            }
        }

        return $result;
    }

    private function set_session() {
        $this->session->set_flashdata('error', $this->set_error());
        $this->session->set_flashdata('oldinput', $this->set_oldinput());
    }

    private function set_except() {
        $result = [];
        if (!empty($this->config)) {
            foreach ($this->config as $key => $value) {
                if (!in_array($value['field'], $this->except)) {
                    $result[] = $value;
                }
            }
        }
        $this->config = $result;
    }

}
