<?php

class Request_costumer extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'customer_name',
                'label' => 'Nama Pengirim',
                'rules' => 'required|max_length[100]'
            ], [
                'field' => 'customer_web_id',
                'label' => 'ID Web',
                'rules' => 'numeric'
            ], [
                'field' => 'customer_address',
                'label' => 'Alamat Pengirim',
                'rules' => 'max_length[200]'
            ], [
                'field' => 'customer_phone',
                'label' => 'No. Telp',
                'rules' => 'max_length[30]'
            ], [
                'field' => 'customer_type',
                'label' => 'Tipe Customer',
            ], [
                'field' => 'customer_city',
                'label' => 'Kota',
            ], [
                'field' => 'customer_id_number',
                'label' => 'Nomor Pengenal',
                'rules' => 'max_length[50]'
            ], [
                'field' => 'customer_id_type',
                'label' => 'Tipe Pengenal',
            ], [
                'field' => 'customer_id_type_other',
                'label' => 'Tipe Pengenal',
                'rules' => 'max_length[50]'
            ], [
                'field' => 'customer_birth_date',
                'label' => 'Tanggal Lahir',
            ], [
                'field' => 'customer_freq_sending',
                'label' => 'Frekuensi Pengiriman Paket',
                'rule' => 'numeric'
            ], [
                'field' => 'customer_sending_quota',
                'label' => 'Jumlah Pengiriman Paket',
                'rule' => 'numeric'
            ], [
                'field' => 'customer_company_type',
                'label' => 'Jenis Perusahaan',
                'rule' => 'numeric'
            ], [
                'field' => 'customer_company_type_other',
                'label' => 'Jenis Perusahaan Lainnya',
                'rule' => 'max_length[50]'
            ], [
                'field' => 'customer_gender',
                'label' => 'Jenis Kelamin',
            ], [
                'field' => 'customer_referral',
                'label' => 'No Refferal',
                'rule' => 'max_length[100]'
            ], [
                'field' => 'customer_company_name',
                'label' => 'Nama Perusahaan',
                'rule' => 'max_length[100]|alpha|callback_alpha_dash_space'
            ], [
                'field' => 'customer_company_website',
                'label' => 'Nama Perusahaan',
                'rule' => 'max_length[100]'
            ], [
                'field' => 'customer_company_telp',
                'label' => 'Nomor Telepon Perusahaan',
                'rule' => 'max_length[100]'
            ], [
                'field' => 'customer_company_email',
                'label' => 'Alamat Email Perusahaan',
                'rule' => 'max_length[100]'
            ], [
                'field' => 'customer_company_address',
                'label' => 'Alamat Perusahaan',
                'rule' => 'max_length[200]'
            ], [
                'field' => 'customer_company_email',
                'label' => 'Alamat Email Perusahaan',
                'rule' => 'valid_email'
            ]
            // , [
            //     'field' => 'user_branch_id',
            //     'label' => 'Cabang / Agen',
            //     'rule' => 'required'
            // ], [
            //     'field' => 'branch_created_id',
            //     'label' => 'Cabang / Agen',
            //     'rule' => 'required'
            // ]
        );

        return $this->execute();
        
    }


    // function alpha_dash_space($str_in){
    //     if (! preg_match("/^([-a-z0-9_ ])+$/i", $str_in)) {
    //         $this->form_validation->set_message('alpha_dash_space', 'The %s field may only contain alpha-numeric characters, spaces, underscores, and dashes.');
    //         return FALSE;
    //     } else {
    //         return TRUE;
    //     }
    // }

    function alpha_dash_space($str)
    {
        return ( ! preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;
    } 

    

}
