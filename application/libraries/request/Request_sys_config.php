<?php

class Request_sys_config extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'company_name',
                'label' => 'Nama Perusahaan',
                'rules' => 'required|max_length[100]'
            ], [
                'field' => 'company_address',
                'label' => 'Alamat',
            ], [
                'field' => 'online_url',
                'label' => 'Alamat Website',
                'rules' => 'max_length[150]'
            ], [
                'field' => 'traccar_url',
                'label' => 'Alamat URL Track Car',
                'rules' => 'max_length[150]'
            ], [
                'field' => 'traccar_db',
                'label' => 'Database Track Car',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'traccar_db_username',
                'label' => 'Username Database Track Car',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'traccar_db_password',
                'label' => 'Password Database Track Car',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'web_url',
                'label' => 'Alamat URL Web',
                'rules' => 'max_length[150]'
            ], [
                'field' => 'web_db',
                'label' => 'Database Web',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'web_db_username',
                'label' => 'Username Database Web',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'web_db_password',
                'label' => 'Password Database Web',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'license_key',
                'label' => 'Nomor Lisensi',
                'rules' => 'max_length[100]'
            ],
        );

        return $this->execute();
    }

}
