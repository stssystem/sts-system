<?php

class Request_armadas extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'traccar_device_id',
                'label' => 'Track Car Device',
                'rules' => 'required|max_length[100]'
            ], [
                'field' => 'armada_name',
                'label' => 'Nama Armada',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'armada_v_width',
                'label' => 'Lebar Armada',
                'rules' => 'numeric|max_length[1000000]'
            ], [
                'field' => 'armada_v_long',
                'label' => 'Panjang Armada',
                'rules' => 'numeric|max_length[1000000]'
            ], [
                'field' => 'armada_v_height',
                'label' => 'Tinggi Armada',
                'rules' => 'numeric|max_length[1000000]'
            ], [
                'field' => 'armada_v_m3',
                'label' => 'Volume Armada',
                'rules' => 'numeric|max_length[1000000]'
            ], [
                'field' => 'armada_last_positin',
                'label' => 'Posisi Terakhir',
                'rules' => 'max_length[200]'
            ], [
                'field' => 'armada_license_plate',
                'label' => 'Plat Lisensi',
                'rules' => 'max_length[20]'
            ], [
                'field' => 'armada_reg_date',
                'label' => 'Tanggal Registrasi',
            ], [
                'field' => 'armada_custom_code',
                'label' => 'Kostume Kode',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'armada_status',
                'label' => 'Status',
                'rules' => 'max_length[100]'
            ], [
                'field' => 'armada_driver',
                'label' => 'Supir Armada',
            ]
        );

        return $this->execute();
    }

}
