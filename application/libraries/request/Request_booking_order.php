<?php

class Request_booking_order extends Request {

    private $ci;

    public function __construct() {
        parent::__construct();
        $this->ci = & get_instance();
    }

    public function validation($attr = array()) {

        $this->config = array(
            [
                'field' => 'branch_origin',
                'label' => 'Branch Origin',
                'rules' => 'required'
            ], [
                'field' => 'branch_destination',
                'label' => 'Branch Destination',
                'rules' => 'required'
            ], [
                'field' => 'branch_code',
                'label' => 'Kode Cabang',
                'rules' => 'numeric'
            ], [
                'field' => 'resi_manual',
                'label' => 'No Resi Manual',
                'rules' => 'required|numeric'
            ], [
                'field' => 'customer_name',
                'label' => 'Nama Pengirim',
                'rules' => 'required'
            ], [
                'field' => 'order_date',
                'label' => 'Tanggal Order',
                'rules' => 'required|date'
            ], [
                'field' => 'package_qty',
                'label' => 'Jumlah Paket',
                'rules' => 'required|numeric'
            ]
        );

        // Duplicate validation
        $this->ci->load->model('orders_model');
        $no_resi_manual_ = $this->ci->input->post('branch_code') . $this->ci->input->post('resi_manual');

        if (!isset($attr['id'])) {

            $result = $this->ci->orders_model->get(['order_manual_id' => $no_resi_manual_])->result();

            if (count($result) > 0) {
                $this->config[] = [
                    'field' => 'order_manual_temp',
                    'rules' => 'required'
                ];
            }
        } else {

            $result = $this->ci->orders_model->get(['order_manual_id' => $no_resi_manual_, 'order_id <>' => $attr['id']])->result();

            if (count($result) > 0) {
                $this->config[] = [
                    'field' => 'order_manual_temp',
                    'rules' => 'required'
                ];
            }
        }

        return $this->execute();
    }

}
