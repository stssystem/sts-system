<?php

class Request_prices_detail extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation($attribute = array()) {

        $this->config = array(
            [
                'field' => 'price_kg_retail',
                'label' => 'Harga / kg Retail',
                'rules' => 'required'
            ], [
                'field' => 'price_kg_partai',
                'label' => 'Harga / kg Partai',
                'rules' => 'required'
            ], [
                'field' => 'price_volume_retail',
                'label' => 'Harga / m3 Retail',
                'rules' => 'required'
            ], [
                'field' => 'price_volume_partai',
                'label' => 'Harga / m3 Partai',
                'rules' => 'required'
            ], [
                'field' => 'price_minimum',
                'label' => 'Harga Minimal',
                'rules' => 'required'
            ]
        );
        
        if($attribute != null){
            $this->config = array_merge($this->config, $attribute);
        }

        return $this->execute();
    }

}
