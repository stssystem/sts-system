<?php

class Request_users extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation($atribute = []) {
        
        if(key_exists('except', $atribute)){
            $this->except = $atribute['except'];
        }

        $this->config = array(
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|max_length[30]'
            ], [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|max_length[50]'
            ], [
                'field' => 'user_email',
                'label' => 'Email',
                'rules' => 'required|max_length[100]'
            ], [
                'field' => 'user_status',
                'label' => 'Status',
                'rules' => 'required|numeric'
            ], [
                'field' => 'user_type_id',
                'label' => 'Tipe User',
                'rules' => 'required|numeric'
            ], [
                'field' => 'userprofile_fullname',
                'label' => 'Nama Lengkap',
                'rules' => 'required|max_length[100]'
            ]
        );

        return $this->execute();
    }

}
