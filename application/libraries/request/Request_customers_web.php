<?php

class Request_customers_web extends Request {

    public function __construct() {
        parent::__construct();
    }

    public function validation() {

        $this->config = array(
            [
                'field' => 'first_name',
                'label' => 'Nama Lengkap',
                'rules' => 'required|max_length[100]'
            ],
            [
                'field' => 'company',
                'label' => 'Perusahaan',
                'rules' => 'max_length[100]'
            ],
            [
                'field' => 'nickname',
                'label' => 'Email',
                'rules' => 'max_length[100]'
            ],
            [
                'field' => 'phone',
                'label' => 'Telepon',
                'rules' => 'max_length[100]'
            ],
            [
                'field' => 'address',
                'label' => 'Alamat Lengkap',
                'rules' => 'max_length[100]'
            ]
        );

        return $this->execute();
    }

}
