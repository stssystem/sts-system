<?php

class Form {

    protected $CI;
    protected $load;
    protected $path;
    protected $attribute = [];

    public function __construct() {

        $this->CI = & get_instance();

        $this->load = $this->CI->load;

        /** Load form folder location */
        $this->CI->config->load('config');
        $this->path = $this->CI->config->item('form_path');
    }

    public function select($set_array = array()) {
        $this->attribute = $set_array;
        $this->load->view($this->path . '/' . __FUNCTION__, $this->attribute);
    }

    public function select_h($set_array = array()) {
        $this->attribute = $set_array;
        $this->load->view($this->path . '/' . __FUNCTION__, $this->attribute);
    }
    
    public function cabang_h($set_array = array()) {
        $this->attribute = $set_array;
        $this->load->view($this->path . '/' . __FUNCTION__, $this->attribute);
    }

    public function text($set_array = array()) {
        $this->attribute = $set_array;
        $this->clear();
        $this->load->view($this->path . '/' . __FUNCTION__, $this->attribute);
    }

    public function text_h($set_array = array()) {
        $this->attribute = $set_array;
        $this->clear();
        $this->load->view($this->path . '/' . __FUNCTION__, $this->attribute);
    }

    public function textarea($set_array = array()) {
        $this->attribute = $set_array;
        $this->load->view($this->path . '/' . __FUNCTION__, $this->attribute);
    }

    public function textarea_h($set_array = array()) {
        $this->attribute = $set_array;
        $this->load->view($this->path . '/' . __FUNCTION__, $this->attribute);
    }

    public function clear() {
        
        if (!key_exists('out_attribute', $this->attribute)) {
            $this->attribute['out_attribute'] = null;
        }

        if (!key_exists('script', $this->attribute)) {
            $this->attribute['script'] = null;
        }
        
    }

}
