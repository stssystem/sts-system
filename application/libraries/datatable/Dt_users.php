<?php

class Dt_users extends Datatables {

    private $set_array = [];
    private $attr = [];

    public function __construct() {
        parent::__construct();

        $this->profile = profile();
        $this->CI->load->model('users_model');
    }

    public function get($obj, $request = array()) {

        $this->attr['order'] = $request['order'];

        /** Preparation datatable */
        $total = $this->CI->users_model->get_user_total([], ['concat_like' => $request['search']["value"]]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];
        
        // Get data
        $this->set_array = $this->CI->users_model->get_user_profile(
                    [],
                    $this->attr, 
                    [
                        'limit' => $request['length'],
                        'index' => $request['start'],
                        'concat_like' => $request['search']["value"]
                    ]
                )->result();

        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_users', $page);

        $number = $request['start'];

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $number++;
                $this->result['data'][$key]['no']               = $number; 
                $this->result['data'][$key]['user_id'] = $value->user_id;
                $this->result['data'][$key]['userprofile_fullname'] = $value->userprofile_fullname;
                $this->result['data'][$key]['username'] = $value->username;
                $this->result['data'][$key]['branch_name'] = $value->branch_name;
                $this->result['data'][$key]['created_at'] = mdate('%d %M %Y %H:%i:%s', $value->created_at);
                $this->result['data'][$key]['last_login'] = ($value->last_login > 0 ) ? mdate('%d %M %Y %H:%i:%s', $value->last_login) : '-';
                $this->result['data'][$key]['user_status'] = user_status($value->user_status);
                $this->result['data'][$key]['action']      = ' <div class="btn-group pull-right">'
                        . '<a href="' . site_url('users/users/view/' . $value->user_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-search"></i>'
                        . '</a>';


                if ($value->user_id != profile()->user_id && $value->user_type_id != 99) {
                
                    $this->result['data'][$key]['action'] .= '<input name="edit-value" type="hidden" '
                            . "value='" . json_encode($value) . "' />"
                            . '<a href = "' . site_url('users/users/edit/' . $value->user_id) . '" class = "btn btn-info btn-sm">'
                            . '<i class = "fa fa-pencil"></i>'
                            . '</a>'
                            . '<a href = "#" data-toggle = "modal" data-target = "#modal-delete-alert" class = "btn btn-danger btn-sm btn-delete">'
                            . '<i class = "fa fa-trash"></i>'
                            . '</a>';
                }

                $this->result['data'][$key]['action'] .= '</div>';
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }


    // private $set_array = [];

    // public function __construct() {
    //     parent::__construct();
    //     $this->CI->load->model('users_model');
    //     $this->set_array = $this->CI->users_model->get_profile()->result();
    // }

    // public function get($obj) {

    //     if (!empty($this->set_array)) {
    //         foreach ($this->set_array as $key => $value) {

    //             $this->result[$key]['user_id'] = $value->user_id;
    //             $this->result[$key]['userprofile_fullname'] = $value->userprofile_fullname;
    //             $this->result[$key]['username'] = $value->username;
    //             $this->result[$key]['branch_name'] = $value->branch_name;
    //             $this->result[$key]['created_at'] = mdate('%d %M %Y %H:%i:%s', $value->created_at);
    //             $this->result[$key]['last_login'] = ($value->last_login > 0 ) ? mdate('%d %M %Y %H:%i:%s', $value->last_login) : '-';
    //             $this->result[$key]['user_status'] = user_status($value->user_status);

    //             $this->result[$key]['action'] = ' <div class="btn-group pull-right">'
    //                     . '<a href="' . site_url('users/users/view/' . $value->user_id) . '" class="btn btn-info btn-sm">'
    //                     . '<i class="fa fa-search"></i>'
    //                     . '</a>';


    //             if ($value->user_id != profile()->user_id && $value->user_type_id != 99) {
    //                 $this->result[$key]['action'] .= '<input name="edit-value" type="hidden" '
    //                         . "value='" . json_encode($value) . "' />"
    //                         . '<a href = "' . site_url('users/users/edit/' . $value->user_id) . '" class = "btn btn-info btn-sm">'
    //                         . '<i class = "fa fa-pencil"></i>'
    //                         . '</a>'
    //                         . '<a href = "#" data-toggle = "modal" data-target = "#modal-delete-alert" class = "btn btn-danger btn-sm btn-delete">'
    //                         . '<i class = "fa fa-trash"></i>'
    //                         . '</a>';
    //             }

    //             $this->result[$key]['action'] .= '</div>';
    //         }
    //     }

    //     return json_encode($this->result);
    // }

}


