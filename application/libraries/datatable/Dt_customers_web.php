<?php

class Dt_customers_web extends Datatables {

    private $set_array = [];

    public function __construct() {

        parent::__construct();
        $this->CI->load->model('customers_web_model');      
    }

    public function get($obj, $request) {            

        $this->set_array = $this->CI->customers_web_model->get_joined([], [
                    'limit' => $request['length'],
                    'index' => $request['start'],
                    'search' => $request['search']["value"],
                    'dt_order' => $request['order']
                ])->result();

        $total = $this->CI->customers_web_model->get_total($request['search']["value"]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];
        
         // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_customer_web', $page);      

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result['data'][$key]['user_id'] = $value->user_id;
                $this->result['data'][$key]['fullname'] = $value->fullname;
                $this->result['data'][$key]['user_pass'] = $value->user_pass;
                $this->result['data'][$key]['user_email'] = $value->user_email;
                $this->result['data'][$key]['user_status'] = $value->user_status;
                $this->result['data'][$key]['created_at'] = date("d F Y - h:i:s", strtotime($value->created_at));
                $this->result['data'][$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group btn-sm pull-right">'
                        . '<a href="' . site_url($obj->controller_path . '/edit/' . $value->user_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a></div>';
            }
        }

        return json_encode($this->result);
    }

}
