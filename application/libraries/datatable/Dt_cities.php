<?php

class Dt_cities extends Datatables {

    private $set_array = [];
    private $attr = [];

    public function __construct() {
        parent::__construct();

        $this->profile = profile();
        $this->CI->load->model('cities_model');
    }

    public function get($obj, $request = array()) {

        $this->attr['order'] = $request['order'];

        /** Preparation datatable */
        $total = $this->CI->cities_model->get_city_total([], ['concat_like' => $request['search']["value"]]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];
        
        // Get data
        $this->set_array = $this->CI->cities_model->get(
                    [],
                    $this->attr, 
                    [
                        'limit' => $request['length'],
                        'index' => $request['start'],
                        'concat_like' => $request['search']["value"]
                    ]
                )->result();

        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_cities', $page);

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result['data'][$key]['city_id'] = $value->city_id;
                $this->result['data'][$key]['created_at'] = (!empty($value->created_at)) ? mdate('%d %M %Y %H:%i:%s', $value->created_at) : '-' ;
                $this->result['data'][$key]['city_name'] = $value->city_name;
                $this->result['data'][$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group">'
                        . '<a href="' . site_url($obj->controller_path . '/edit/' . $value->city_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a></div>';

            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }


    // private $set_array = [];

    // public function __construct() {
    //     parent::__construct();
    //     $this->CI->load->model('cities_model');
    //     $this->set_array = $this->CI->cities_model->get()->result();
    // }

    // public function get($obj) {

        // if (!empty($this->set_array)) {
        //     foreach ($this->set_array as $key => $value) {
        //         $this->result[$key]['city_id'] = $value->city_id;
        //         $this->result[$key]['created_at'] = (!empty($value->created_at)) ? mdate('%d %M %Y %H:%i:%s', $value->created_at) : '-' ;
        //         $this->result[$key]['city_name'] = $value->city_name;
        //         $this->result[$key]['action'] = ' <input name="edit-value" type="hidden" '
        //                 . "value='" . json_encode($value) . "' />"
        //                 . '<div class="btn-group">'
        //                 . '<a href="' . site_url($obj->controller_path . '/edit/' . $value->city_id) . '" class="btn btn-info btn-sm">'
        //                 . '<i class="fa fa-pencil"></i></a>'
        //                 . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
        //                 . '<i class="fa fa-trash"></i></a></div>';
        //     }
        // }

    //     return json_encode($this->result);
    // }

}
