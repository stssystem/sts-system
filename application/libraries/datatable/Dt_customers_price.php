<?php

class Dt_customers_price extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('customers_price_model');
    }

    public function get($obj, $customer_id) {

        $where['customers_price.customer_id'] = $customer_id;
        $this->set_array = $this->CI->customers_price_model->get_joined($where, [
                    'group_by' => 'cities_origin.city_id, cities_destination.city_id'
                ])->result();

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result[$key]['customer_price_id'] = $value->customer_price_id;
                $this->result[$key]['customer_origin'] = $value->customer_origin;
                $this->result[$key]['customer_destination'] = $value->customer_destination;
                $this->result[$key]['customer_price'] = number_format($value->customer_price, 0, ',', '.');
                $this->result[$key]['created_at'] = mdate('%d %M %Y %H:%i:%s', $value->created_at);

                $this->result[$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">'
                        . '<a href="' . site_url('customers/customers_price_detail/index/'.$customer_id.'/'.$value->city_id_origin.'/'.$value->city_id_destination) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-search"></i></a>'
                        . '<a href="' . site_url($obj->controller_path . '/edit/'.$customer_id.'/'.$value->city_id_origin.'/'.$value->city_id_destination) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a>'
                        . '</div>';
            }
        }

        return json_encode($this->result);
    }

}
