<?php

class Dt_report_tax extends Datatables {

    private $set_array = [];
    private $attr = [];

    public function __construct() {
        parent::__construct();

        $this->profile = profile();
        $this->CI->load->model('report_tax_model');
    }

    public function get($obj, $request = array()) {

        $this->attr['order'] = $request['order'];

        /** Preparation datatable */
        $total = $this->CI->report_tax_model->get_total($request['search']["value"]);

        // Set total order
        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        // Get data
        $this->set_array = $this->CI->report_tax_model->get_data(
                        $this->attr, $request['length'], $request['start'], $request['search']["value"]
                )->result();

        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];

        $this->CI->session->set_userdata('dt_report_tax', $page);

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result['data'][$key]['branch'] = $value->branch;
                $this->result['data'][$key]['order_date'] = $value->order_date;
                $this->result['data'][$key]['dpp'] = number_format(round($value->dpp, 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
                $this->result['data'][$key]['ppn'] = number_format(round($value->ppn, 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
                $this->result['data'][$key]['total'] = number_format(round($value->total, 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
                $this->result['data'][$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">'
                        . '<a href="' . site_url('report/report_tax_detail/index/' . $value->order_origin) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-search"></i></a>'
                        . '</div>';
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
