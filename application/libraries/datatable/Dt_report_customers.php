<?php

class Dt_report_customers extends Datatables {

    private $set_array = [];
    private $profile = array();

    public function __construct() {
        parent::__construct();
        $this->profile = profile();
    }

    public function get($obj, $request = array()) {

        $this->CI->load->model('customers_transaction_model');

        $user_type_id = $this->profile->user_type_id;

        $user_regional = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }
        $user_branch = profile()->branch_id;

        if ($user_type_id == 99) { // super admin & Admin Pusat
            $where = [];
        } else if (profile()->regional_id != '') { // Regional
            $where = [
                'customer_regional_id' => $user_regional
            ];
        } else if (profile()->branch_id != '') { // Cabang
            $where = [
                'customer_branch_id' => $user_branch
            ];
        } else {
            $where = [
                'customer_regional_id' => 0,
                'customer_branch_id' => 0
            ];
        }

        $attr['order'] = $request['order'];

        $this->set_array = $this->CI->customers_transaction_model->get_data($attr, $request['length'], $request['start'], $request['search']["value"], array(), '', '', $where)->result_array();

        $total = $this->CI->customers_transaction_model->get_total($request['search']["value"], $where);

        // Save to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        
        $this->CI->session->set_userdata('dt_report_customers', $page);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        $user_type_id = $this->profile->user_type_id;

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result['data'][$key]['customer_id'] = $value['customer_id'];
                $this->result['data'][$key]['customer_name'] = $value['customer_name'];
                $this->result['data'][$key]['city_name'] = $value['city_name'];
                $this->result['data'][$key]['branch_name'] = $value['branch_name'];
                $this->result['data'][$key]['customer_referral'] = $value['customer_referral'];
                $this->result['data'][$key]['total_transaction'] = number_format($value['total_transaction'], 0, ',', '.');
                $this->result['data'][$key]['last_transaction'] = mdate('%d %M %Y', $value['last_transaction']);

                $action = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">';

                $action .= '<a href="' . site_url('report/report_customers_detail/index/' . $value['customer_id']) . '" class="btn btn-info btn-sm btn-edit">'
                        . '<i class="fa fa-search"></i></a>';

                $action .= '</div>';

                $this->result['data'][$key]['action'] = $action;
            }
        } else {
            $this->result['data'] = [];
        }


        return json_encode($this->result);
    }

}
