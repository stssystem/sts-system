<?php

class Dt_booking extends Datatables {

    private $set_array = [];
    private $profile = array();

    public function __construct() {
        parent::__construct();
        $this->profile = profile();
        $this->CI->load->model('booking_model');
    }

    public function get($obj, $request) {

        // Parmater preparing
        $attr = array();
        $attr['branch'] = $this->profile->branch_name;
        $attr['branch_access'] = $this->profile->branch_access;
        $attr['order'] = $request['order'];

        if ($this->profile->regional_id != '') {
            // Load all resource needed
            $this->CI->load->model('regionals_model');
            $attr['regional'] = $this->CI->regionals_model->get_branches_name($this->profile->regional_id);
        }

        // Preparing datatable
        $total = $this->CI->booking_model->get_total($attr, $request['search']["value"]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        $this->set_array = $this->CI
                ->booking_model
                ->get_data($attr, $request['length'], $request['start'], $request['search']["value"])
                ->result();

        // Cek sql
        $this->result['sql'] = $this->CI->db->last_query();

        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_booking', $page);

        $total = 0;
        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result['data'][$key]['order_id'] = $value->order_id;
                $this->result['data'][$key]['order_manual_id'] = $value->order_manual_id;
                $this->result['data'][$key]['origin_destination'] = $value->branch_name_origin.' - '.$value->branch_name_destination;
                $this->result['data'][$key]['customer'] = $value->customer_name;
                $this->result['data'][$key]['package_count'] = $value->package_count;
                $this->result['data'][$key]['created_at'] = mdate('%d %M %Y', $value->order_date);

                $action = ' <input name = "edit-value" type = "hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class = "btn-group pull-right">';
                $action .= '<a href = "' . site_url($obj->controller_path . '/create_order/' . $value->order_id) . '" class = "btn btn-info btn-sm btn-edit" data-toggle="tooltip" data-placement="top" title="Buat Order" >'
                        . '<i class = "fa fa-shopping-cart"></i></a>';
                $action .= '<a href = "' . site_url($obj->controller_path . '/edit/' . $value->order_id) . '" class = "btn btn-info btn-sm btn-edit" data-toggle="tooltip" data-placement="top" title="Edit Booking Resi" >'
                        . '<i class = "fa fa-edit"></i></a>';
                 $action .= '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete" data-placement="top" title="Hapus Booking Resi" >'
                        . '<i class = "fa fa-times"></i></a>';
                 
                $action .= '</div>';
                $this->result['data'][$key]['action'] = $action;
                
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
