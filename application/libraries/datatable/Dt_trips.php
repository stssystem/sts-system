<?php

class Dt_trips extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('trips_model');
        $this->CI->load->model('det_trips_model');
    }

    public function get($obj, $request) {

        $this->set_array = $this->CI->trips_model->get_joined_datatable()->result();

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result[$key]['trip_id'] = $value->trip_id;
                $this->result[$key]['route_name'] = ($value->route_id != -1) ? $value->route_name : '<span class="label label-info">Charter</span>';
                $this->result[$key]['armada_name'] = $value->armada_name;
                $this->result[$key]['userprofile_fullname'] = $value->userprofile_fullname;
                $this->result[$key]['created_at'] = mdate('%d %M %Y %H:%i:%s', $value->created_at);
                $this->result[$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right"><a href="' . site_url($obj->controller_path . '/edit/' . $value->trip_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a></div>';
            }
        }

        return json_encode($this->result);
    }

}
