<?php

class Dt_report_logistic_armadas extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('report_sale_model');
    }

    public function get($obj) {
        $this->set_array = $this->CI->report_logistic_model->get_logistic_armadas()->result();

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result[$key]['armada_id'] = $value->armada_id;
                $this->result[$key]['armada_name'] = '<strong>'.$value->armada_name.'</strong>';
                $armada_status = ($value->armada_status == '1') ? '<span class="label label-success">Aktif</span>' : '<span class="label label-danger">Tidak Aktif</span>';
                $this->result[$key]['armada_status'] = $armada_status;
                $this->result[$key]['start_name'] = $value->start_name;
                $this->result[$key]['stop_name'] = $value->stop_name;
                $this->result[$key]['schedule'] = mdate('%d %M %Y %H:%i:%s', $value->trip_start_date).' - '.mdate('%d %M %Y %H:%i:%s', $value->trip_end_date);
                $this->result[$key]['action'] = ' <input name="detail-value" type="hidden" '
                . "value='" . json_encode($value) . "' />"
                . '<a href="' . site_url($obj->controller_path . '/view_logistic_armadas/'. $value->trip_id) . '" class="btn btn-info btn-sm">'
                . '<i class="fa fa-search"></i></a>';
            }
        } 

        return json_encode($this->result);
    }

}
