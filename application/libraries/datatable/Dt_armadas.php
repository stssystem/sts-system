<?php

class Dt_armadas extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('armadas_model');
    }

    public function get($obj, $request) {

        $this->set_array = $this->CI->armadas_model->get([], [
                    'limit' => $request['length'],
                    'index' => $request['start'],
                    'search' => $request['search']["value"],
                    'dt_order' => $request['order']
                ])->result();

        /** Preparation datatable */
        $total = $this->CI->armadas_model->get_total($request['search']["value"]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_armadas', $page);

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result['data'][$key]['armada_id'] = $value->armada_id;
                $this->result['data'][$key]['armada_name'] = $value->armada_name;
                $this->result['data'][$key]['armada_status'] = $value->armada_status == 1 ? 'Aktif' : 'Tidak Aktif';
                $this->result['data'][$key]['armada_license_plate'] = $value->armada_license_plate;
                $this->result['data'][$key]['created_at'] = mdate('%d %M %Y', $value->created_at);
                $this->result['data'][$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">'
                        . '<a href="' . site_url($obj->controller_path . '/view/' . $value->armada_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-search"></i></a>'
                        . '<a href="' . site_url($obj->controller_path . '/edit/' . $value->armada_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a></div>';
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
