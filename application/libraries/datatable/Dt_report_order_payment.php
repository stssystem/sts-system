<?php

class Dt_report_order_payment extends Datatables {

    private $set_array = [];
    private $profile = array();
    private $attr = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('report_order_payment_model');
        $this->profile = profile();
    }

    public function get($obj, $request = array()) {

        $this->attr['order']      = $request['order'];

        $total = $this->CI->report_order_payment_model->get_total([], ['concat_like' => $request['search']["value"]]);

        $this->result['recordsTotal']       = $total;
        $this->result['recordsFiltered']    = $total;
        $this->result['draw']               = $request['draw'];

        $this->set_array = $this->CI->report_order_payment_model->get(
                    [],
                    $this->attr, 
                    [
                        'limit' => $request['length'],
                        'index' => $request['start'],
                        'concat_like' => $request['search']["value"]
                    ]
                )->result_array();
        
        $this->result['sql'] = $this->CI->db->last_query();

        $user_type_id                       = $this->profile->user_type_id;

        // Save to session
        $page['start']                      = $request['start'];
        $page['lenght']                     = $request['length'];
        $page['order']['columns']           = $request['order'][0]['column'];
        $page['order']['mode']              = $request['order'][0]['dir'];
        $page['search']                     = $request['search']["value"];
        $this->CI->session->set_userdata('dt_report_order_payment', $page);

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result['data'][$key]['order_date'] = mdate('%d %M %Y', $value['order_date']);
                $this->result['data'][$key]['order_id'] = '<strong>'.$value['order_id'].'</strong>';
                $this->result['data'][$key]['customer_name'] = $value['customer_name'];
                $this->result['data'][$key]['branch_origin_name'] = $value['branch_origin_name'];
                $this->result['data'][$key]['branch_destination_name'] = $value['branch_destination_name'];
                $this->result['data'][$key]['order_payment_type'] = payment_methode($value['order_payment_type']);
                $status = '';
                if ($value['order_payment_status'] == 1) {
                    $status = '<span class="label label-success">Lunas</span>';
                } else {
                    $status = '<span class="label label-danger">Belum Lunas</span>';
                }
                $this->result['data'][$key]['status'] = $status;

                if ($value['order_payment_type'] == 1) {
                    $orders_due_date = '<span class="label label-success">Tidak ada</span>';
                } else {
                    $orders_due_date = (!empty($value['orders_due_date'])) ? mdate('%d %M %Y', $value['orders_due_date']) : '-';
                }

                $this->result['data'][$key]['orders_due_date'] = $orders_due_date;
                $this->result['data'][$key]['action'] = ' <input name="detail-value" type="hidden" '
                . "value='" . json_encode($value) . "' />"
                . '<a href="' . site_url('orders/order/view/' . $value['order_id']) . '" class="btn btn-info btn-sm">'
                . '<i class="fa fa-search"></i></a>';
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
