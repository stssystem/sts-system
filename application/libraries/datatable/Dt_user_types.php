<?php

class Dt_user_types extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('user_types_model');
        $this->set_array = $this->CI->user_types_model->get()->result();
    }

    public function get($obj) {

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result[$key]['user_type_id'] = $value->user_type_id;
                $this->result[$key]['created_at'] = mdate('%d %M %Y %H:%i:%s', $value->created_at);
                $this->result[$key]['user_type_name'] = $value->user_type_name;
                $this->result[$key]['branch_access'] = $value->branch_access == 1?'<span class="label label-info">Tidak Dibatasi</span>':'<span class="label label-warning">Dibatasi</span>';
                $this->result[$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">'
                        . '<a href="' . site_url($obj->controller_path . '/edit/' . $value->user_type_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a></div>';
            }
        }

        return json_encode($this->result);
    }

}
