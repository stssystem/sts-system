<?php

class Dt_report_gps_detail extends Datatables {

    private $set_array = [];
    private $profile = array();

    public function __construct() {
        parent::__construct();
        $this->profile = profile();
    }

    public function get($obj, $request = array(), $armada_id = '') {

        $this->CI->load->model('report_gps_detail_model');

        $attr['armada_id'] = $armada_id;
        $attr['order'] = $request['order'];
        
        $this->CI->session->set_userdata('f', $request['search']["value"]);

        $this->set_array = $this->CI->report_gps_detail_model->get_data($attr, $request['length'], $request['start'], $request['search']["value"])->result_array();
        
         $this->result['sql'] = $this->CI->db->last_query();

        $total = $this->CI->report_gps_detail_model->get_total($attr, $request['search']["value"]);

        // Save to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_report_gps_detail', $page);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        $user_type_id = $this->profile->user_type_id;

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result['data'][$key]['trips_id'] = $value['trips_id'];
                $this->result['data'][$key]['trip_start_date'] = mdate('%d %M %Y', $value['trip_start_date']);
                $this->result['data'][$key]['trip_end_date'] = mdate('%d %M %Y', $value['trip_end_date']);
                $this->result['data'][$key]['driver_fullname'] = $value['driver_fullname'];
                $this->result['data'][$key]['codriver_fullname'] = $value['codriver_fullname'];
                $this->result['data'][$key]['armada_name'] = $value['armada_name'];
                $this->result['data'][$key]['route_name'] = $value['route_name'];
                $this->result['data'][$key]['created_at'] = mdate('%d %M %Y', $value['created_at']);

                $action = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">';
                $action .= '<a href="3" class="btn btn-danger btn-sm btn-edit">'
                        . '<i class="fa fa-times"></i></a>';
                $action .= '</div>';

                $this->result['data'][$key]['action'] = $action;
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
