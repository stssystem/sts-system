<?php

class Dt_order_approval_orders_extra extends Datatables {

    private $set_array = [];
    private $profile = array();

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('package_edit_orders_extra_model');
        $this->profile = profile();
    }

    public function get_orders_extra($obj, $status)
    {

        $user_type_id = $this->profile->user_type_id;

        $user_regional       = '';
        if (!empty(profile()->regional_id)) {
            $user_regional = profile()->regional_id;
        } else {
            $user_regional = profile()->branch_regional_id;
        }
        
        if ($user_type_id == 99 || $user_type_id == 120) { // super admin & Admin Pusat

            $where = ['package_edit_orders_extra.order_status' => $status];

        } else if ($user_type_id == 124) { // Regional

            $where = [
            'package_edit_orders_extra.order_status' => $status, 
            'requested_user_types.user_type_id' => 114,
            'package_edit_orders_extra.regional_id' => $user_regional
            ];

        } else if ($user_type_id == 114) { // Cabang

            $where = [
            'package_edit_orders_extra.order_status' => $status, 
            'requested_user_types.user_type_id' => 114,
            'package_edit_orders_extra.branch_id' => profile()->branch_id
            ];

        } else {
            $where = ['package_edit_orders_extra.order_status' => 0];
        }

        $this->set_array = $this->CI->package_edit_orders_extra_model->get_package_edit($where)->result();

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {


                $this->result[$key]['id'] = '<a href="' . site_url('orders/order/view/' . $value->order_id) . '">'
                . "<strong>" . $value->order_id . "</strong></a>";

                $date = '';
                if ($status == 2 || $status == 3) {
                    $date = date('d M Y - h:i:s', $value->date_updated);
                } else {
                    $date = date('d M Y - h:i:s', $value->date_approved);
                }
                
                $this->result[$key]['date']                     = $date;
                $this->result[$key]['customer_name']            = $value->customer_name;
                $this->result[$key]['orders_extra_id']          = $value->orders_extra_id;
                $this->result[$key]['orders_extra_name']        = $value->orders_extra_name;
                $this->result[$key]['request_staff_name']       = $value->request_staff_name;
                $this->result[$key]['request_user_type_name']   = $value->request_user_type_name;
                $this->result[$key]['notes']                    = $value->package_notes;
                $this->result[$key]['branch']                   = $value->request_user_branch_name;
                $this->result[$key]['request_staff']            = $value->request_staff_name.' - '.$value->request_user_type_name.' - '.$value->request_user_branch_name;
                $this->result[$key]['approved_staff']           = $value->approved_staff_name.' - '.$value->approved_user_type_name;
                
                if ($value->order_status == '1') {
                    $label          = 'label-success';
                    $check          = 'fa fa-check';
                    $order_status   = 'updated';
                } else if ($value->order_status == '2') {
                    $label          = 'label-danger';
                    $check          = 'fa fa-times';
                    $order_status   = 'need approval'; 
                } else if ($value->order_status == '3') {
                    $label          = 'label-danger';
                    $check          = 'fa fa-times';
                    $order_status   = 'rejected'; 
                } else {
                    $label          = 'label-warning';
                    $check          = '';
                    $order_status   = 'status undefined'; 
                }
                
                $this->result[$key]['status'] = '<span class="label '.$label.'"><i class="'.$check.'"></i> '.$order_status.'</span>';
                
                $btn_approve = '';
                $btn_reject = '';
                $btn_view = '';

                if ($user_type_id == 114) {

                    if ($status == 2 || $status == 3) {
                        $btn_view = '<a href="#"  data-target="#modal-orders-extra-view" data-toggle="modal" class="btn btn-primary btn-sm btn-flat btn-view_4">'
                        . '<i class="fa fa-search"></i>'
                        . '</a>';
                    }

                } else {

                    if ($status == 2 || $status == 3) {
                        $btn_approve = '<a href="#"  data-target="#modal-orders-extra-approve-alert" data-toggle="modal" class="btn btn-success btn-sm btn-flat btn-confirm_4">'
                        . '<i class="fa fa-check"></i>'
                        . '</a>';
                    }
                    if ($status == 2) {
                        $btn_reject = '<a href="#"  data-target="#modal-orders-extra-reject-alert" data-toggle="modal" class="btn btn-danger btn-sm btn-flat btn-reject_4">'
                        . '<i class="fa fa-times"></i>'
                        . '</a>';
                    }

                }

                $this->result[$key]['action'] = '<input type="hidden" name="orders-extra-edit-data" '
                . "value='" . json_encode($value) . "' /> "
                . '<div class="btn-group pull-right">'
                . $btn_approve . ''. $btn_reject . '' .$btn_view
                . '</div>';

            }
        } 

        return json_encode($this->result);
    }

}
