<?php

class Dt_report_customers_detail extends Datatables {

    private $set_array = [];
    private $profile = array();

    public function __construct() {
        parent::__construct();
        $this->profile = profile();
    }

    public function get($obj, $request = array(), $customers_id = 0) {

        $this->CI->load->model('customers_transaction_detail_model');

        $attr['customer_id'] = $customers_id;

        $this->set_array = $this->CI->customers_transaction_detail_model->get_data($attr, $request['length'], $request['start'], $request['search']["value"])->result_array();
        $this->result['sql'] = $this->CI->db->last_query();

        $total = $this->CI->customers_transaction_detail_model->get_total($attr, $request['search']["value"]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        $number = $request['start'];

        $user_type_id = $this->profile->user_type_id;

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $number++;
                $this->result['data'][$key]['no'] = $number;
                $this->result['data'][$key]['order_id'] = $value['order_id'];
                $this->result['data'][$key]['branch_name'] = $value['branch_origin_name'] . ' - ' . $value['branch_destination_name'];
                $this->result['data'][$key]['total_koli'] = $value['total_koli'];
                $this->result['data'][$key]['total_weight'] = round($value['total_weight'], 3);
                $this->result['data'][$key]['total_size'] = round($value['total_size'] / 1000000, 3);
                $this->result['data'][$key]['order_sell_total'] = number_format($value['order_sell_total'], 0, ',', '.');
                $this->result['data'][$key]['order_date'] = mdate('%d %M %Y', $value['order_date']);

                $action = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">';

                $action .= '<a href="' . site_url('orders/order/view/' . $value['order_id']) . '" class="btn btn-danger btn-sm btn-edit">'
                        . '<i class="fa fa-times"></i></a>';

                $action .= '</div>';

            }
        } else {
            $this->result['data'] = [];
        }


        return json_encode($this->result);
    }

}
