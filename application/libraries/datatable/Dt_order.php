<?php

class Dt_order extends Datatables {

    private $set_array = [];
    private $profile = array();

    public function __construct() {
        parent::__construct();
        $this->profile = profile();
        $this->CI->load->model('order_list_model');
    }

    public function get($obj, $status = 0, $request) {

        // Parmater preparing
        $attr = array();
        $attr['status'] = $status;
        $attr['branch'] = $this->profile->branch_name;
        $attr['branch_access'] = $this->profile->branch_access;
        $attr['order'] = $request['order'];

        if ($this->profile->regional_id != '') {
            // Load all resource needed
            $this->CI->load->model('regionals_model');
            $attr['regional'] = $this->CI->regionals_model->get_branches_name($this->profile->regional_id);
        }

        // Preparing datatable
        $total = $this->CI->order_list_model->get_total($attr, $request['search']["value"]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        $this->set_array = $this->CI
                ->order_list_model
                ->get_data($attr, $request['length'], $request['start'], $request['search']["value"])
                ->result();
        
        $this->result['sql'] = $this->CI->db->last_query();

        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_order', $page);

        $total = 0;
        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result['data'][$key]['order_id'] = $value->order_id;
                $this->result['data'][$key]['order_load_date'] = (($value->order_load_date == 0 ) ? '-' : mdate('%d %M %Y', $value->order_load_date));
                $this->result['data'][$key]['order_unload_date'] = ($value->order_unload_date == 0 ) ? '-' : mdate('%d %M %Y', $value->order_unload_date);
                $this->result['data'][$key]['order_delivered_date'] = ($value->order_delivered_date == 0 ) ? '-' : mdate('%d %M %Y', $value->order_delivered_date);
                $this->result['data'][$key]['order_received_date'] = ($value->order_received_date == 0 ) ? '-' : mdate('%d %M %Y', $value->order_received_date);
                $this->result['data'][$key]['customer_name'] = $value->customer_name;
                $this->result['data'][$key]['district'] = $value->branch_name_origin . ' - ' . $value->branch_name_destination;
                $this->result['data'][$key]['qty'] = $value->total_koli;
                $this->result['data'][$key]['total_weight'] = round($value->total_weight, 2);
                $this->result['data'][$key]['volume'] = round(($value->total_size / 1000000), 2);
                $this->result['data'][$key]['resi_ages'] = $value->resi_age;

                $status_label = '';
                if ($value->package_status == 4) {
                    $status_label = '<span class="label label-success">Sudah Naik</span>';
                } else {
                    $status_label = '<span class="label label-danger">Belum Naik</span>';
                }

                $this->result['data'][$key]['status'] = $status_label;

                $origin_status_label = '';

                if ($value->package_status == 4) {
                    $origin_status_label = '<span class="label label-default">Sudah Naik</span><br>';
                    $this->result['data'][$key]['order_update_date'] = $this->result['data'][$key]['order_load_date'];
                }

                if ($value->package_status == 5) {
                    $origin_status_label = '<span class="label label-success">Sudah Diturunkan</span>';
                    $this->result['data'][$key]['order_update_date'] = $this->result['data'][$key]['order_unload_date'];
                }

                if ($value->package_status == 6) {
                    $origin_status_label = '<span class="label label-warning">Barang Transit</span>';
                    $this->result['data'][$key]['order_update_date'] = $this->result['data'][$key]['order_unload_date'];
                }

                if ($value->package_status == 7) {
                    $origin_status_label = '<span class="label label-info">Pengantaran</span>';
                    $this->result['data'][$key]['order_update_date'] = $this->result['data'][$key]['order_delivered_date'];
                }

                $this->result['data'][$key]['origin_status'] = $origin_status_label;

                $this->result['data'][$key]['created_at'] = mdate('%d %M %Y', $value->order_date);

                $action = ' <input name = "edit-value" type = "hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class = "btn-group pull-right">';

                $action .= '<a href = "' . site_url($obj->controller_path . '/view/' . $value->order_id) . '" class = "btn btn-info btn-sm btn-edit" data-toggle="tooltip" data-placement="top" title="Lihat Detail" >'
                        . '<i class = "fa fa-search"></i></a>';

                if ($value->order_status == 3) {
                    $action .= '<a href = "#" data-toggle = "modal" data-target = "#closed_final" class = "btn btn-success btn-sm btn-closed-final">'
                            . '<i class = "fa fa-check"></i></a>';
                }

                if ($status == 2) {
                    $action .= '<a href = "#"  class="btn btn-warning btn-sm btn-self-take" data-toggle="tooltip" data-placement="top" title="Ambil Sendiri"  >'
                            . '<i class = "fa fa-gift"></i></a>';
                }

                if ($status == 3) {
                    $action .= '<a href = "#"  class = "btn btn-warning btn-sm btn-package-rollback" data-toggle="tooltip" data-placement="top" title="Kembali"  >'
                            . '<i class = "fa fa-arrow-left"></i></a>';
                    $action .= '<a href = "#"  class = "btn btn-warning btn-sm btn-delivery" data-toggle="tooltip" data-placement="top" title="Telah Diterima"  >'
                            . '<i class = "fa fa-check"></i></a>';
                }

                if ($this->profile->user_type_id == 99) {
                    $action .= '<a href = "#" data-toggle = "modal" data-target = "#modal-delete-alert" class = "btn btn-danger btn-sm btn-delete">'
                            . '<i class = "fa fa-trash"></i></a>';
                }

                $action .= '</div>';

                $this->result['data'][$key]['action'] = $action;
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
