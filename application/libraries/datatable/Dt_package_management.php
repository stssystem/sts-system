<?php

class Dt_package_management extends Datatables {

    private $set_array = [];
    private $attr = [];

    public function __construct() {
        parent::__construct();
        $this->profile = profile();
        $this->CI->load->model('package_management_model');
    }

    public function get($obj, $request = array(), $status) {

        // Branch Restricted
        $attr = array();
        $attr['branch'] = $this->profile->branch_id;
        $attr['branch_access'] = $this->profile->branch_access;
        $attr['type_do'] = $this->convert_status($status);

        // Set status
        $this->attr['order'] = $request['order'];

        // Regional Restricted
        if ($this->profile->regional_id != '') {
            // Load all resource needed
            $this->CI->load->model('regionals_model');
            $attr['regional'] = $this->CI->regionals_model->get_branches_name($this->profile->regional_id);
        }

        /** Preparation datatable */
        $total = $this->CI->package_management_model->get_total($attr, $request['search']["value"]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        // Get data
        $this->set_array = $this->CI->package_management_model->get_data(
                        $attr, $request['length'], $request['start'], $request['search']["value"]
                )->result();
        $this->result['sql'] = $this->CI->db->last_query();

        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_package_management', $page);

        $number = $request['start'];

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result['data'][$key]['resi_number'] = $value->resi_number;
                #$this->result['data'][$key]['resi_number'] = $value->resi_number . ' | ' . " origin : {$value->origin} - destination : {$value->destination} - branch-do : {$value->branch_id_do} |";
                $this->result['data'][$key]['resi_date'] = mdate('%d %M %Y %H:%i:%s', $value->resi_date);
                $this->result['data'][$key]['do_date'] = mdate('%d %M %Y %H:%i:%s', $value->do_date);
                $this->result['data'][$key]['armada_plate_number'] = $value->armada_plate_number;
                $this->result['data'][$key]['courier'] = $value->courier;
                $this->result['data'][$key]['koli'] = $value->koli;
                $this->result['data'][$key]['weight'] = $value->weight;
                $this->result['data'][$key]['volume'] = $value->volume;
                $this->result['data'][$key]['action'] = ' <div class="btn-group pull-right">';

                if (profile()->user_id != 99) {
                    $this->result['data'][$key]['action'] .= '<input name="edit-value" type="hidden" '
                            . "value='" . json_encode($value) . "' />"
                            . '<a href = "#" data-toggle = "modal" data-target = "#modal-delete-alert" class = "btn btn-danger btn-sm btn-delete">'
                            . '<i class = "fa fa-trash"></i>'
                            . '</a>';
                }

                $this->result['data'][$key]['action'] .= '</div>';
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

    private function convert_status($status = '') {
        switch ($status) {
            case 0:
                return 'load';
                break;
            case 1:
                return 'unload';
                break;
            case 2:
                return 'transit';
                break;
            case 3:
                return 'delivery';
                break;
        }
    }

}
