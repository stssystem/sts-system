<?php

class Dt_report_gps extends Datatables {

    private $set_array = [];
    private $profile = array();

    public function __construct() {
        parent::__construct();
        $this->profile = profile();
        $r = array();
    }

    public function get($obj, $request = array()) {

        // Load all resource needed
        $this->CI->load->model('report_gps_model');
        $this->CI->load->helper('geo');

        $attr['order'] = $request['order'];

        $this->set_array = $this->CI->report_gps_model->get_data($attr, $request['length'], $request['start'], $request['search']["value"])->result_array();

        $this->CI->session->set_userdata('total', $request['search']["value"]);

        $total = $this->CI->report_gps_model->get_total([], $request['search']["value"]);

        $this->result['sql'] = $this->CI->db->last_query();

        // Save to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_report_gps', $page);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        $user_type_id = $this->profile->user_type_id;

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $str = ''
                        . 'armadas.armada_id AS armada_id, '
                        . 'armadas.armada_name AS armada_name, '
                        . 'armadas.armada_license_plate AS armada_license_plate, '
                        . 'armadas.traccar_device_id AS traccar_device_id,'
                        . 'armadas.armada_last_positin AS  armada_last_positin '
                        . '';

                $this->result['data'][$key]['armada_id'] = $value['armada_id'];
                $this->result['data'][$key]['armada_name'] = $value['armada_name'];
                $this->result['data'][$key]['armada_license_plate'] = $value['armada_license_plate'];
                $this->result['data'][$key]['traccar_device_id'] = $value['traccar_device_id'];
                $this->result['data'][$key]['armada_last_positin'] = get_geocity_bycurl($value['armada_last_positin']);

                $action = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">';

                $action .= '<a href="' . site_url('report/report_gps_detail/index/' . $value['armada_id']) . '" class="btn btn-info btn-sm btn-edit">'
                        . '<i class="fa fa-search"></i></a>';

                $action .= '</div>';

                $this->result['data'][$key]['action'] = $action;
            }
        } else {
            $this->result['data'] = [];
        }


        return json_encode($this->result);
    }

}
