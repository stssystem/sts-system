<?php

class Dt_orders extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('packages_model');
    }

    public function get($obj, $order_id = 0) {

        $this->set_array = $this->CI->packages_model->get_joined(['order_id' => $order_id], ['group_by' => ['det_order.det_order_package_parent']])->result();
        $total = 0;
        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result[$key]['package_id'] = $value->package_id;
                $this->result[$key]['package_title'] = $value->package_title;
                $this->result[$key]['package_type'] = $value->package_type_name;
                $this->result[$key]['package_weight'] = $value->package_weight;
                $this->result[$key]['qty'] = $value->qty;
                $this->result[$key]['package_weight_total'] = ($value->qty * $value->package_weight);
                $total_volume = (float) (($value->package_size / 1000000) * $value->qty);
                $this->result[$key]['total_volume'] = round($total_volume, 2);
                $this->result[$key]['sell_total'] = 'Rp ' . number_format($value->sell_total, 0, ',', '.');
                $this->result[$key]['created_at'] = mdate('%d %M %Y %H:%i:%s', $value->created_at);
                $this->result[$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . "<input type='hidden' name='total_orders' value='" . $value->total . "' />"
                        . "<input type='hidden' name='sell_total_orders' value='" . ($value->sell_total) . "' />"
                        . '<div class="btn-group pull-right">'
                        . '<a href="#detail-package-form" class="btn btn-info btn-sm btn-edit">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a></div>';
            }
        }

        return json_encode($this->result);
    }

}
