<?php

class Dt_report_sale_branch_destination extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
    }

    public function get($obj, $request = array()) {

        // Load all resource needed
        $this->CI->load->model('report/sale_comission_branch_destination');
        $this->set_array = $this->CI
                        ->sale_comission_branch_destination
                        ->get([
                            'search' => $request['search']["value"],
                            'limit' => $request['length'],
                            'index' => $request['start']
                        ])->result();

        // Get total record
        $total = $this->CI->sale_comission_branch_destination->get_total($request['search']["value"]);
        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        // Save to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_cms_branch_destination', $page);

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result['data'][$key]['branch_id'] = '<strong>' . $value->branch_id . '</strong>';
                $this->result['data'][$key]['branch_name'] = $value->branch_name;
                $total_order = '';
                if (!empty($value->total_order)) {
                    $total_order = $value->total_order;
                } else {
                    $total_order = '<span class="label label-warning">Tidak Ada Order</span>';
                }
                $this->result['data'][$key]['total_order'] = $total_order;
                $total_omset = '';
                if (!empty($value->total_omset)) {
                    $total_omset = 'Rp. ' . number_format($value->total_omset);
                } else {
                    $total_omset = '<span class="label label-danger">Omset Tidak Tersedia</span>';
                }
                $this->result['data'][$key]['total_omset'] = $total_omset;
                $comission = '';
                if (!empty($value->total_omset)) {
                    $comission = 'Rp. ' . number_format(($value->branch_comission / 100) * $value->total_omset);
                } else {
                    $comission = '<span class="label label-danger">Tidak ada komisi agen</span>';
                }
                $this->result['data'][$key]['comission'] = $comission;
                $this->result['data'][$key]['action'] = ' <input name="detail-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<a href="' . site_url('report/report_sale_branch_destination/index/' . $value->branch_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-search"></i></a>';
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
        
    }

}
