<?php

class Dt_report_tax_detail extends Datatables {

    private $set_array = [];
    private $attr = [];

    public function __construct() {
        parent::__construct();

        $this->profile = profile();
        $this->CI->load->model('report_tax_detail_model');
    }

    public function get($obj, $order_origin = '', $request = array()) {

        $this->attr['order'] = $request['order'];

        /** Preparation datatable */
        $total = $this->CI->report_tax_detail_model->get_total($order_origin, $request['search']["value"]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        // Get data
        $this->set_array = $this->CI->report_tax_detail_model->get_data(
                        $order_origin, $this->attr, $request['length'], $request['start'], $request['search']["value"]
                )->result();

        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];

        $this->CI->session->set_userdata('dt_report_tax_detail', $page);

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result['data'][$key]['order_date'] = mdate('%d %F %Y',$value->order_date);
                $this->result['data'][$key]['order_id'] = $value->order_id;
                $this->result['data'][$key]['customer_name'] = $value->customer_name;
                $this->result['data'][$key]['branch_name'] = $value->branch_name;
                $this->result['data'][$key]['dpp'] = number_format(round($value->dpp, 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
                $this->result['data'][$key]['ppn'] = number_format(round($value->ppn, 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
                $this->result['data'][$key]['total'] = number_format(round($value->total, 0, PHP_ROUND_HALF_DOWN), 0, ',', '.');
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
