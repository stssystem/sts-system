<?php

class Dt_report_logistic_deliveries_1 extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('report_logistic_model');
    }

    public function get($obj) {
        $this->set_array = $this->CI->report_logistic_model->get_logistic_deliveries_1()->result();

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result[$key]['branch_name'] = '<a href="' . site_url('report/report_logistic_deliveries/index/' . $value->branch_id_origin) . '">'
                . '<strong>'.$value->branch_name_origin.'</strong></a>';

                $this->result[$key]['total'] = '<strong>'.$value->total_order.'</strong></a>';
            }
        }

        return json_encode($this->result);
    }

}
