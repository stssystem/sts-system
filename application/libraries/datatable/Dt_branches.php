<?php

class Dt_branches extends Datatables {

    private $set_array = [];
    private $attr = [];

    public function __construct() {
        parent::__construct();

        $this->profile = profile();
        $this->CI->load->model('branches_model');
    }

    public function get($obj, $request = array()) {

        $this->attr['order'] = $request['order'];

        /** Preparation datatable */
        $total = $this->CI->branches_model->get_branch_total([], ['concat_like' => $request['search']["value"]]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];
        
        // Get data
        $this->set_array = $this->CI->branches_model->get_joined(
                    [],
                    $this->attr, 
                    [
                        'limit' => $request['length'],
                        'index' => $request['start'],
                        'concat_like' => $request['search']["value"]
                    ]
                )->result();

        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_branches', $page);

        if (!empty($this->set_array)) {

            foreach ($this->set_array as $key => $value) {
                $this->result['data'][$key]['branch_id'] = $value->branch_id;
                $this->result['data'][$key]['branch_name'] = $value->branch_name;
                $this->result['data'][$key]['branch_regional'] = $value->regional;
                $this->result['data'][$key]['branch_code'] = $value->branch_code;
                $this->result['data'][$key]['branch_type'] = branch_type($value->branch_type);
                $this->result['data'][$key]['branch_status'] = user_status($value->branch_status);
                $this->result['data'][$key]['created_at'] = mdate('%d %M %Y %H:%i:%s', $value->created_at);
                $this->result['data'][$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right"><a href="' . site_url($obj->controller_path . '/edit/' . $value->branch_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a>'
                        . '<a href="' . site_url('branch/branch/staff/' . $value->branch_id) . '" class="btn btn-success btn-sm"><i class="fa fa-users"></i></a>'
                        . '</div>';
            }

        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

    // private $set_array = [];

    // public function __construct() {
    //     parent::__construct();
    //     $this->CI->load->model('branches_model');
    //     $this->set_array = $this->CI->branches_model->get_joined()->result();
    // }

    // public function get($obj) {

    //     if (!empty($this->set_array)) {
    //         foreach ($this->set_array as $key => $value) {
    //             $this->result[$key]['branch_id'] = $value->branch_id;
    //             $this->result[$key]['branch_name'] = $value->branch_name;
    //             $this->result[$key]['branch_regional'] = $value->regional;
    //             $this->result[$key]['branch_code'] = $value->branch_code;
    //             $this->result[$key]['branch_type'] = branch_type($value->branch_type);
    //             $this->result[$key]['branch_status'] = user_status($value->branch_status);
    //             $this->result[$key]['created_at'] = mdate('%d %M %Y %H:%i:%s', $value->created_at);
    //             $this->result[$key]['action'] = ' <input name="edit-value" type="hidden" '
    //                     . "value='" . json_encode($value) . "' />"
    //                     . '<div class="btn-group pull-right"><a href="' . site_url($obj->controller_path . '/edit/' . $value->branch_id) . '" class="btn btn-info btn-sm">'
    //                     . '<i class="fa fa-pencil"></i></a>'
    //                     . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
    //                     . '<i class="fa fa-trash"></i></a>'
    //                     . '<a href="' . site_url('branch/branch/staff/' . $value->branch_id) . '" class="btn btn-success btn-sm"><i class="fa fa-users"></i></a>'
    //                     . '</div>';
    //         }
    //     }

    //     return json_encode($this->result);
    // }

}
