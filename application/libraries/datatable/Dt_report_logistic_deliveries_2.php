<?php

class Dt_report_logistic_deliveries_2 extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('report_logistic_model');
    }

    public function get($obj) {
        $this->set_array = $this->CI->report_logistic_model->get_logistic_deliveries_2()->result();

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result[$key]['branch_name'] = '<a href="' . site_url('report/report_logistic_deliveries_2/index/' . $value->branch_id) . '">'
                . '<strong>'.$value->branch_origin_name.'</strong></a>';

                $this->result[$key]['total'] = '<strong>'.$value->total_order.'</strong></a>';
            }
        }

        return json_encode($this->result);
    }

}
