<?php

class Dt_report_logistic_cities extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('report_logistic_model');
    }

    public function get($obj) {
        $this->set_array = $this->CI->report_logistic_model->get_logistic_cities()->result();

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result[$key]['city_name'] = ' <input name="detail-value" type="hidden" '
                . "value='" . json_encode($value) . "' />"
                . '<a href="' . site_url('report/report_logistic_cities/index/' . $value->city_id) . '">'
                . '<strong>'.$value->city_name.'</strong></a>';

                $total_weight = '';
                if (!empty($value->total_weight)) {
                    $total_weight = round($value->total_weight).' kg';
                } else {
                    $total_weight = '<span class="label label-danger">Data Tidak Tersedia</span>';
                }

                $this->result[$key]['total_weight'] = $total_weight;

                $total_size = '';
                if (!empty($value->total_size)) {
                    $total_size = round($value->total_size / 1000000, 2).' m &sup3';
                } else {
                    $total_size = '<span class="label label-danger">Data Tidak Tersedia</span>';
                }
                
                $this->result[$key]['total_size'] = $total_size;

            }
        } 

        return json_encode($this->result);
    }

}
