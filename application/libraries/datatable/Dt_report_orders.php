<?php

class Dt_report_orders extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('report_orders_model');
    }

    public function get($year = 0) {

        $year = ($year == 0) ? date('Y') : $year;

        $this->set_array = $this->CI->report_orders_model->get_report($year);
        $total = 0;
        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result[$key]['branch_name'] = $value['branch'];
                foreach ($value['value'] as $k => $i) {
                    $this->result[$key][$k] = number_format($i, 0, ',', '.');
                }
            }
        }

        return json_encode($this->result);
    }

}
