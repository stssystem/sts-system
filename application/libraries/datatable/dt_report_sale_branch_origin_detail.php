<?php

class Dt_report_sale_branch_origin_detail extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('report_sale_branch_origin_model');
    }

    public function get($obj, $request = array(), $branch_id = 0) {

        $attr['order']      = $request['order'];
        $attr['branch_id']  = $branch_id;

        $this->set_array    = $this->CI->report_sale_branch_origin_model->get_data($attr, $request['length'], $request['start'], $request['search']["value"])->result_array();

        $total = $this->CI->report_sale_branch_origin_model->get_total($attr, $request['search']["value"]);

        $this->result['recordsTotal']       = $total;
        $this->result['recordsFiltered']    = $total;
        $this->result['draw']               = $request['draw'];

        // Save to session
        $page['start']              = $request['start'];
        $page['lenght']             = $request['length'];
        $page['order']['columns']   = $request['order'][0]['column'];
        $page['order']['mode']      = $request['order'][0]['dir'];
        $page['search']             = $request['search']["value"];
        $this->CI->session->set_userdata('dt_report_sale_branch_origin_detail', $page);

        $number = $request['start'];

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $number++;
                $this->result['data'][$key]['no'] = $number;
                $this->result['data'][$key]['order_date'] = mdate('%d %M %Y %H:%i:%s', $value['order_date']);
                $this->result['data'][$key]['order_id'] = '<strong>'.$value['order_id'].'</strong>';
                $this->result['data'][$key]['customer_name'] = $value['customer_name'];
                $this->result['data'][$key]['branch_origin'] = $value['branch_name_origin'];
                $this->result['data'][$key]['branch_destination'] = $value['branch_name_destination'];
                $omset = '';
                if (!empty($value['omset'])) {
                    $omset = 'Rp. '.number_format($value['omset']);
                } else {
                    $omset = '<span class="label label-danger">Omset Tidak Tersedia</span>';
                }
                $this->result['data'][$key]['omset'] = $omset;
                $comission = '';
                if (!empty($value['omset'])) {
                    $comission = 'Rp. '.number_format((20/100)*$value['omset']);
                } else {
                    $comission = '<span class="label label-danger">Tidak ada komisi agen</span>';
                }
                $this->result['data'][$key]['comission'] = $comission;
                $this->result['data'][$key]['action'] = ' <input name="detail-value" type="hidden" '
                . "value='" . json_encode($value) . "' />"
                . '<a href="' . site_url('orders/order/view/' . $value['order_id']) . '" class="btn btn-info btn-sm">'
                . '<i class="fa fa-search"></i></a>';
            }

        }

        return json_encode($this->result);
    }

}
