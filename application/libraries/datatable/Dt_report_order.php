<?php

class Dt_report_order extends Datatables {

    private $set_array = [];
    private $profile = array();

    public function __construct() {
        parent::__construct();
        $this->profile = profile();
    }

    public function get($obj, $request = array()) {

        // Load all reource needed
        $this->CI->load->model('finance_report_model');

        // Order definition
        $attr['order'] = $request['order'];

        $this->set_array = $this->CI->finance_report_model->get_data(
                        $attr, $request['length'], $request['start'], $request['search']["value"]
                )->result_array();

        $total = $this->CI->finance_report_model->get_total($request['search']["value"]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        $user_type_id = $this->profile->user_type_id;

        // Save to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_report_order', $page);

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                if ($value['order_use_tax'] != 1) {
                    $value['order_sell_total'] = $value['order_sell_total'] + ($value['order_sell_total'] * (1 / 100));
                }
                
                $this->result['data'][$key]['order_id'] = '<strong>' . $value['order_id'] . '</strong><br/>' . mdate('%d %M %Y', $value['order_date']);
                $this->result['data'][$key]['customer_name'] = $value['customer_name'];
                $this->result['data'][$key]['status'] = order_status($value['order_status']);
                $this->result['data'][$key]['order_origin'] = $value['branch_name_origin'];
                $this->result['data'][$key]['order_destination'] = $value['branch_name_destination'];
                $this->result['data'][$key]['order_sell_total'] = number_format($value['order_sell_total'], 0, ',', '.');
                $this->result['data'][$key]['extra_total'] = number_format($value['extra_total'], 0, ',', '.');
                $this->result['data'][$key]['total'] = number_format($value['extra_total'] + $value['order_sell_total'], 0, ',', '.');

                $action = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">';

                $action .= '<a href="' . site_url('orders/order/view/' . $value['order_id']) . '" class="btn btn-info btn-sm btn-edit">'
                        . '<i class="fa fa-search"></i></a>';

                if ($user_type_id == 99) {
                    $action .= '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                            . '<i class="fa fa-trash"></i></a>';
                }

                $action .= '</div>';

                $this->result['data'][$key]['action'] = $action;
            }
        }


        return json_encode($this->result);
    }

}
