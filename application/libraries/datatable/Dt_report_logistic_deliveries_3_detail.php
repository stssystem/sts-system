<?php

class Dt_report_logistic_deliveries_3_detail extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('report_logistic_deliveries_3_model');
    }

    public function get($obj, $request = array(), $branch_id = 0) {

        $attr['order']      = $request['order'];
        $attr['branch_id']  = $branch_id;

        $this->set_array    = $this->CI->report_logistic_deliveries_3_model->get_data($attr, $request['length'], $request['start'], $request['search']["value"])->result_array();

        $total = $this->CI->report_logistic_deliveries_3_model->get_total($attr, $request['search']["value"]);

        $this->result['recordsTotal']       = $total;
        $this->result['recordsFiltered']    = $total;
        $this->result['draw']               = $request['draw'];

        // Save to session
        $page['start']              = $request['start'];
        $page['lenght']             = $request['length'];
        $page['order']['columns']   = $request['order'][0]['column'];
        $page['order']['mode']      = $request['order'][0]['dir'];
        $page['search']             = $request['search']["value"];
        $this->CI->session->set_userdata('dt_report_logistic_deliveries_3_detail', $page);

        $number = $request['start'];

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $number++;
                $this->result['data'][$key]['no']                   = $number;
                $this->result['data'][$key]['order_id']             = '<strong>'.$value['order_id'].'</strong>';
                $this->result['data'][$key]['deliveries_date']      = mdate('%d %M %Y %H:%i:%s', $value['created_at']);
                $this->result['data'][$key]['note']                 = 'Paket kembali ke Agen';
                $this->result['data'][$key]['note_detail']          = $value['note'];
                $this->result['data'][$key]['action'] = '<a href="' . site_url('orders/order/view/' . $value['order_id']) . '" class="btn btn-info btn-sm">'
                . '<i class="fa fa-search"></i></a>';
            }

        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
