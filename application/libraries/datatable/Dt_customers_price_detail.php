<?php

class Dt_customers_price_detail extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('customers_price_model');
    }

    public function get($obj, $customer_id, $origin, $destination) {

        $where['customers_price.customer_id'] = $customer_id;
        $where['cities_origin.city_id'] = $origin;
        $where['cities_destination.city_id'] = $destination;
        
        $this->set_array = $this->CI->customers_price_model->get_joined($where)->result();

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result[$key]['customer_price_id'] = $value->customer_price_id;
                $this->result[$key]['customer_origin'] = $value->branch_origin_name;
                $this->result[$key]['customer_destination'] = $value->branch_destination_name;
                $this->result[$key]['customer_price'] = number_format($value->customer_price, 0, ',', '.');
                $this->result[$key]['created_at'] = mdate('%d %M %Y %H:%i:%s', $value->created_at);

                $this->result[$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">'
                        . '<a href="' . site_url($obj->controller_path . '/edit/'.$value->customer_price_id.'/'.$customer_id.'/'.$value->city_id_origin.'/'.$value->city_id_destination) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a>'
                        . '</div>';
            }
        }

        return json_encode($this->result);
    }

}
