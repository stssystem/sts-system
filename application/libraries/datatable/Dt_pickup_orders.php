<?php

class Dt_pickup_orders extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('pickup_orders_model');
        $this->CI->load->model('orders_model');
    }

    public function get($obj, $pickup_status = 0, $request) {

        /** Data filtering */
        $branch_based = (!empty(branch())) ? branch()['city_name'] : false;
        $where = ['pickup_status' => $pickup_status];

        // Data sorting 
        $sort_column = $request['order'][0]['column'];
        $sort_dir = $request['order'][0]['dir'];
        $sorting = array(
            ['no_resi' => $sort_dir],
            ['nama_pengirim' => $sort_dir],
            ['alamat_pengirim' => $sort_dir],
            ['asal' => $sort_dir],
            ['tujuan' => $sort_dir],
            ['jumlah_paket' => $sort_dir],
            ['total_harga' => $sort_dir],
            [
                'waktu_pengambilan_tanggal' => $sort_dir,
                'waktu_pengambilan_jam' => $sort_dir,
            ]
        );

        /** Get data from model */
        $this->set_array = $this->CI
                        ->pickup_orders_model
                        ->get_joined($where, [
                            'order' => $sorting[$sort_column],
                            'limit' => $request['length'],
                            'index' => $request['start'],
                            'concat_like' => $request['search']["value"]
                                ], $branch_based)->result();

        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_pickuporder', $page);

        /** Get total row  */
        $total = $this->CI
                ->pickup_orders_model
                ->get_total(
                $where, ['concat_like' => $request['search']["value"]], $branch_based
        );

        /** Preparing datatable value */
        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                /** Sender tag format */
                $html_sender = "<strong>$value->nama_pengirim</strong><br/>"
                        . "$value->perusahaan <br/>"
                        . "$value->email_pengirim<br/>"
                        . "$value->telp_pengirim<br/>";

                /** Sender address */
                $html_address = "$value->alamat_pengirim, <br/>"
                        . "$value->kabupaten_pengirim";

                /** Set inline notification */
                $style = $this->CI->orders_model->order_isexist($value->no_resi) ? 'style="color:red;"' : '';

                $this->result['data'][$key]['id'] = "<strong $style >" . $value->no_resi . "</strong>";
                $this->result['data'][$key]['sender'] = $html_sender;
                $this->result['data'][$key]['sender_address'] = $html_address;
                $this->result['data'][$key]['origin'] = $value->asal;
                $this->result['data'][$key]['destination'] = $value->tujuan;
                $this->result['data'][$key]['quantity'] = $value->jumlah_paket;
                $this->result['data'][$key]['total'] = number_format($value->total_harga, 0, ',', '.');
                $this->result['data'][$key]['pickup_date'] = $value->waktu_pengambilan_tanggal . ' ' . $value->waktu_pengambilan_jam;
                $this->result['data'][$key]['status'] = order_status($value->status);

                $btn_approve = '<a href="#"  data-target="#modal-update-alert" data-toggle="modal" class="btn btn-success btn-sm btn-flat btn-confirm">'
                        . '<i class="fa fa-check"></i>'
                        . '</a>';

                $btn_approve2 = '<a href="#"  data-target="#modal-update2-alert" data-toggle="modal" class="btn btn-success btn-sm btn-flat btn-confirm">'
                        . '<i class="fa fa-check"></i>'
                        . '</a>';

                $btn_delete = '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-flat btn-delete">'
                        . '<i class="fa fa-trash"></i>'
                        . '</a>';

                $btn_view = '<a href="#" data-toggle="modal" data-target="#modal-detail-alert" class="btn btn-success btn-sm btn-flat btn-views">'
                        . '<i class="fa fa-search"></i>'
                        . '</a>';

                $btn_new_orders = '<a href="' . site_url($obj->controller_path . '/edit/' . $value->no_resi) . '" class="btn btn-info btn-sm btn-flat">'
                        . '<i class="fa fa-archive"></i>'
                        . '</a>';


                $btn = '<div class="btn-group pull-right">';

                if ($pickup_status == 0) {
                    $btn .= $btn_view;
                    $btn .= $btn_approve;
                    $btn .= $btn_delete;
                }

                if ($pickup_status == 1) {
                    $btn .=$btn_approve2;
                    $btn .= $btn_delete;
                }

                if ($pickup_status == 2) {
                    $btn .= $btn_new_orders;
                    $btn .= $btn_delete;
                }
                $btn .= '</div>';

                $this->result['data'][$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />" . $btn;
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
