<?php

class Dt_regionals extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('regionals_model');
        $this->set_array = $this->CI->regionals_model->get()->result();
    }

    public function get($obj) {

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result[$key]['regional_id'] = $value->reg_id;
                $this->result[$key]['created_at'] = mdate('%d %M %Y %H:%i:%s', $value->regional_created_at);
                $this->result[$key]['regional'] = $value->regional;
                $this->result[$key]['regional_manager'] = ucwords($value->userprofile_fullname);
                $this->result[$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">'
                        . '<a href="' . site_url($obj->controller_path . '/edit/' . $value->reg_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a>'
                        . '<a href="' . site_url('branch/regionals/staff/' . $value->reg_id) . '" class="btn btn-success btn-sm">'
                        . '<i class="fa fa-users"></i></a>'
                        . '</div>';
            }
        }

        return json_encode($this->result);
    }

}
