<?php

class Dt_det_orders_extra extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('det_orders_extra_model');
    }

    public function get($obj, $order_id = 0) {

        $this->set_array = $this->CI->det_orders_extra_model->get(['order_id' => $order_id])->result();

        $total = 0;
        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                
                /** Calculate */
                $total += $value->det_order_extra_total;
                
                /** Row formated */
                $this->result[$key]['det_order_extra_id'] = $value->det_order_extra_id;
                $this->result[$key]['det_order_extra_name'] = $value->det_order_extra_name;
                $this->result[$key]['det_order_extra_total'] = 'Rp. '.number_format($value->det_order_extra_total);
                $this->result[$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . "<input type='hidden' name='total_order_extra' value='" . $total . "' />"
                        . '<div class="btn-group pull-right">'
                        . '<a href="#detail-package-form" class="btn btn-info btn-sm btn-edit btn-edit-order-extra">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a class="btn btn-danger btn-sm btn-delete-order-extra">'
                        . '<i class="fa fa-trash"></i></a></div>';
                        
            }
        }

        return json_encode($this->result);
    }

}
