<?php

class Dt_report_logistic_pickup_orders extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('pickup_orders_model');
        $this->CI->load->model('orders_model');
    }

    public function get($obj, $id) {
        $this->set_array = $this->CI->pickup_orders_model->get_report_logistik_pickup_orders()->result();

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                $this->result[$key]['branch_name'] = '<a href="' . site_url('report/report_logistic_pickup_orders/index/' . $value->asal) . '">'
                . '<strong>'.$value->asal.'</strong></a>';

                $this->result[$key]['total'] = '<strong>'.$value->total_order.'</strong></a>';
            }
        }

        return json_encode($this->result);
    }

}
