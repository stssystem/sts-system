<?php

class Dt_franco_list extends Datatables {

    private $set_array = [];
    private $profile = array();

    public function __construct() {
        parent::__construct();
        $this->profile = profile();
        $this->CI->load->model('franco_list_model');
    }
    
    public function get($obj, $status = 0, $request) {

        // Parmater preparing
        $attr = array();
        $attr['status'] = $status;
        $attr['branch'] = $this->profile->branch_id;
        $attr['branch_access'] = $this->profile->branch_access;
        $attr['order'] = $request['order'];

        if ($this->profile->regional_id != '') {
            // Load all resource needed
            $this->CI->load->model('regionals_model');
            $attr['regional'] = $this->CI->regionals_model->get_branches_id_array($this->profile->regional_id);
        }

        // Preparing datatable
        $total = $this->CI->franco_list_model->get_total($attr, $request['search']["value"]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];

        $this->set_array = $this->CI
                ->franco_list_model
                ->get_data($attr, $request['length'], $request['start'], $request['search']["value"])
                ->result();        
        
        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_franco_list', $page);

        $total = 0;
        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {

                // Set status label
                $label = '';
                if ($status == 1) {
                    if ($value->status == 1) {
                        $label = '<span class="label label-default">Akan Diproses</span>';
                    }
                }

                if ($status == 2) {
                    if ($value->status == 1) {
                        $label = '<span class="label label-default">Akan Diproses</span>';
                    }
                    if ($value->status == 2) {
                        $label = '<span class="label label-info">Sudah Dikirim</span>';
                    }
                }

                if ($status == 3) {
                    if ($value->status == 3) {
                        $label = '<span class="label label-info">Sudah Diterima</span>';
                    }
                    if ($value->status == 4) {
                        $label = '<span class="label label-success">Sudah Diantar</span>';
                    }
                }

                if ($status == 4) {
                    if ($value->status == 2) {
                        $label = '<span class="label label-default">Sudah Dikirim</span>';
                    }
                    if ($value->status == 3) {
                        $label = '<span class="label label-info">Sudah Diterima</span>';
                    }
                    if ($value->status == 4) {
                        $label = '<span class="label label-success">Sudah Diantar</span>';
                    }
                }

                if ($status == 5) {
                    if ($value->status == 5) {
                        $label = '<span class="label label-success">Selesai</span>';
                    }
                }

                $this->result['data'][$key]['franco_id'] = $value->franco_id;
                $this->result['data'][$key]['order_id'] = $value->order_id;
                $this->result['data'][$key]['customer_name'] = $value->customer_name;
                $this->result['data'][$key]['origin_branch_name'] = $value->origin_branch_name;
                $this->result['data'][$key]['destination_branch_name'] = $value->destination_branch_name;
                $this->result['data'][$key]['status'] = $label;
                $this->result['data'][$key]['last_update'] = $value->last_update;

                // Action column
                $action = ' <input name = "edit-value" type = "hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class = "btn-group pull-right">';

                if ($this->profile->user_type_id == 99) {
                    $action .= '<a href = "#" data-toggle = "modal" data-target = "#modal-delete-alert" class = "btn btn-danger btn-sm btn-delete">'
                            . '<i class = "fa fa-trash"></i></a>';
                }

                if ($status == 3) {
                    $action .= '<a href = "#" data-toggle = "modal" data-target = "#finish-modal" class = "btn btn-warning btn-sm btn-finish">'
                            . '<i class = "fa fa-check"></i></a>';
                }

                $action .= '</div>';

                $this->result['data'][$key]['action'] = $action;
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
