<?php

class Dt_report_logistic_pickup_orders_detail extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('report_logistic_pickup_orders_model');
        $this->CI->load->model('pickup_orders_model');
    }

    public function get($obj, $request = array(), $asal = 0) {

        $attr['order']      = $request['order'];
        $attr['asal']  = $asal;

        // $this->set_array    = $this->CI->pickup_orders_model->get_data($attr, $request['length'], $request['start'], $request['search']["value"])->result_array();


        $this->set_array    = $this->CI->pickup_orders_model->pickup_detail_get_orders_detail($attr, $request['length'], $request['start'], $request['search']["value"])->result_array();


        // var_dump($this->set_array);
        // die();

        $total = $this->CI->pickup_orders_model->pickup_detail_get_total($attr, $request['search']["value"]);

        $this->result['recordsTotal']       = $total;
        $this->result['recordsFiltered']    = $total;
        $this->result['draw']               = $request['draw'];

        // Save to session
        $page['start']              = $request['start'];
        $page['lenght']             = $request['length'];
        $page['order']['columns']   = $request['order'][0]['column'];
        $page['order']['mode']      = $request['order'][0]['dir'];
        $page['search']             = $request['search']["value"];
        $this->CI->session->set_userdata('dt_report_pickup_orders_detail', $page);

        $number = $request['start'];

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $number++;
                $this->result['data'][$key]['no']                   = $number;

                // $this->result['data'][$key]['order_date']           = mdate('%d %M %Y %H:%i:%s', $value['order_date']);
                // $this->result['data'][$key]['order_id']             = '<strong>'.$value['order_id'].'</strong>';
                // $this->result['data'][$key]['customer_name']        = $value['customer_name'];
                // $this->result['data'][$key]['branch_origin']        = $value['branch_name_origin'];
                // $this->result['data'][$key]['branch_destination']   = $value['branch_name_destination'];
                // $this->result['data'][$key]['action'] = '<a href="' . site_url('orders/order/view/' . $value['order_id']) . '" class="btn btn-info btn-sm">'
                // . '<i class="fa fa-search"></i></a>';


                /** Sender tag format */
                // $html_sender = "<strong>$value->nama_pengirim</strong><br/>"
                //         . "$value['perusahaan'] <br/>"
                //         . "$value['email_pengirim']<br/>"
                //         . "$value['telp_pengirim']<br/>";

                /** Sender address */
                // $html_address = "$value->alamat_pengirim, <br/>"
                //         . "$value->kabupaten_pengirim";

                /** Set inline notification */
                $style = $this->CI->orders_model->order_isexist($value['no_resi']) ? 'style="color:red;"' : '';

                $this->result['data'][$key]['id'] = "<strong $style >" . $value['no_resi'] . "</strong>";
                $this->result['data'][$key]['sender'] = $value['perusahaan'];
                $this->result['data'][$key]['sender_address'] = ['alamat_pengirim'];
                $this->result['data'][$key]['origin'] = $value['asal'];
                $this->result['data'][$key]['destination'] = $value['tujuan'];
                $this->result['data'][$key]['quantity'] = $value['jumlah_paket'];
                $this->result['data'][$key]['total'] = number_format($value['total_harga'], 0, ',', '.');
                $this->result['data'][$key]['pickup_date'] = $value['waktu_pengambilan_tanggal'] . ' ' . $value['waktu_pengambilan_jam'];
                $this->result['data'][$key]['status'] = order_status($value['status']);


                $btn = '<div class="btn-group pull-right">';

                // if ($pickup_status == 2) {
                //     $btn .= $btn_new_orders;
                //     $btn .= $btn_delete;
                // }
                $btn .= '</div>';

                $this->result['data'][$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />" . $btn;

            }

        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
