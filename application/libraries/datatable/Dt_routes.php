<?php

class Dt_routes extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('routes_model');
        $this->CI->load->model('det_routes_model');
    }

    public function get($obj, $request) {

        $this->set_array = $this->CI->routes_model->get([], [
                    'limit' => $request['length'],
                    'index' => $request['start'],
                    'search' => $request['search']["value"],
                    'dt_order' => $request['order']
                ])->result();

        $total = $this->CI->routes_model->get_total($request['search']["value"]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];
        
         // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_routes', $page);

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result['data'][$key]['route_id'] = $value->route_id;
                $this->result['data'][$key]['route_name'] = $value->route_name;
                $this->result['data'][$key]['route_status'] = user_status($value->route_status);
                $this->result['data'][$key]['created_at'] = mdate('%d %M %Y %H:%i:%s', $value->created_at);
                $this->result['data'][$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right"><a href="' . site_url($obj->controller_path . '/edit/' . $value->route_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a></div>';
            }
        }

        return json_encode($this->result);
    }

}
