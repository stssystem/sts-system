<?php

class Dt_prices_detail extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('prices_model');
    }

    public function get($obj, $origin = null, $destination = null) {
        
        /** Get all data */
        $where = ['cities_origin.city_id' => $origin, 'cities_destination.city_id' => $destination];
        $this->set_array = $this->CI->prices_model->get_joined($where)->result();

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $this->result[$key]['price_id'] = $value->price_id;
                $this->result[$key]['price_branch_origin_id'] = $value->branch_origin_name;
                $this->result[$key]['price_branch_destination_id'] = $value->branch_destination_name;
                $this->result[$key]['price_kg_retail'] = number_format($value->price_kg_retail);
                $this->result[$key]['price_kg_partai'] = number_format($value->price_kg_partai);
                $this->result[$key]['price_volume_retail'] = number_format($value->price_volume_retail);
                $this->result[$key]['price_volume_partai'] = number_format($value->price_volume_partai);
                $this->result[$key]['price_minimum'] = number_format($value->price_minimum);
                $this->result[$key]['action'] = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group pull-right">'
                        . '<a href="' . site_url($obj->controller_path . '/edit/'. $value->price_id .'/' . $value->city_origin_id . '/' . $value->city_destination_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a></div>';
            }
        }

        return json_encode($this->result);
    }

}
