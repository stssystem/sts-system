<?php

class Dt_report_logistic_cities_detail extends Datatables {

    private $set_array = [];

    public function __construct() {
        parent::__construct();
        $this->CI->load->model('report_logistic_cities_model');
    }


    public function get($obj, $request = array(), $city_id = 0) {

        $attr['order']      = $request['order'];
        $attr['city_id']    = $city_id;

        $this->set_array    = $this->CI->report_logistic_cities_model->get_data($attr, $request['length'], $request['start'], $request['search']["value"])->result_array();

        $total = $this->CI->report_logistic_cities_model->get_total($attr, $request['search']["value"]);

        $this->result['recordsTotal']       = $total;
        $this->result['recordsFiltered']    = $total;
        $this->result['draw']               = $request['draw'];

        // Save to session
        $page['start']              = $request['start'];
        $page['lenght']             = $request['length'];
        $page['order']['columns']   = $request['order'][0]['column'];
        $page['order']['mode']      = $request['order'][0]['dir'];
        $page['search']             = $request['search']["value"];
        $this->CI->session->set_userdata('dt_report_logistic_cities_detail', $page);

        $number = $request['start'];

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $number++;
                $this->result['data'][$key]['no']           = $number;
                $this->result['data'][$key]['order_date']   = mdate('%d %M %Y', $value['order_date']);
                $this->result['data'][$key]['order_id']     = '<strong>'.$value['order_id'].'</strong>';
                $this->result['data'][$key]['branch_origin']= $value['branch_origin_name'];
                $this->result['data'][$key]['customer_name']= $value['customer_name'];

                $total_weight = '';
                if (!empty($value['total_weight'])) {
                    $total_weight = round($value['total_weight']).' kg';
                } else {
                    $total_weight = '<span class="label label-danger">Data Tidak Tersedia</span>';
                }
                $this->result['data'][$key]['total_weight'] = $total_weight;

                $total_size = '';
                if (!empty($value['total_size'])) {
                    $total_size = round($value['total_size'] / 1000000, 2).' m &sup3';
                } else {
                    $total_size = '<span class="label label-danger">Data Tidak Tersedia</span>';
                }
                $this->result['data'][$key]['total_size'] = $total_size;

                $this->result['data'][$key]['action']       = '<a href="' . site_url('orders/order/view/' . $value['order_id']) . '" class="btn btn-info btn-sm">'
                . '<i class="fa fa-search"></i></a>';
            }

        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

}
