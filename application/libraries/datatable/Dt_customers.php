<?php

class Dt_customers extends Datatables {

    private $set_array = [];
    private $attr = [];

    public function __construct() {
        parent::__construct();

        $this->profile = profile();
        $this->CI->load->model('costumers_model');
    }

    public function get($obj, $request = array()) {

        /** Parmater preparing */
        $this->attr_base($request);

        /** Preparation datatable */
        $total = $this->CI->costumers_model->get_total([], ['concat_like' => $request['search']["value"]]);

        $this->result['recordsTotal'] = $total;
        $this->result['recordsFiltered'] = $total;
        $this->result['draw'] = $request['draw'];
        
        // Get data
        $this->set_array = $this->CI->costumers_model->get_joined(
                    [],
                    $this->attr, 
                    [
                        'limit' => $request['length'],
                        'index' => $request['start'],
                        'concat_like' => $request['search']["value"]
                    ]
                )->result();

        // Save datatables to session
        $page['start'] = $request['start'];
        $page['lenght'] = $request['length'];
        $page['order']['columns'] = $request['order'][0]['column'];
        $page['order']['mode'] = $request['order'][0]['dir'];
        $page['search'] = $request['search']["value"];
        $this->CI->session->set_userdata('dt_customers', $page);

        $number = $request['start'];

        if (!empty($this->set_array)) {
            foreach ($this->set_array as $key => $value) {
                $number++;
                $this->result['data'][$key]['no']               = $number; 
                $this->result['data'][$key]['customer_id']      = $value->customer_id;
                $this->result['data'][$key]['customer_name']    = $value->customer_name;
                $this->result['data'][$key]['customer_city']    = $value->city_name;
                $this->result['data'][$key]['customer_address'] = $value->customer_address;
                $this->result['data'][$key]['customer_phone']   = $value->customer_phone;
                $this->result['data'][$key]['pic_name']         = $value->pic_first_name.' '.$value->pic_last_name;
                $this->result['data'][$key]['pic_birth_date']   = ($value->pic_birth_date) ? mdate('%d %M %Y %H:%i:%s', $value->pic_birth_date) : '-';
                $this->result['data'][$key]['action']           = ' <input name="edit-value" type="hidden" '
                        . "value='" . json_encode($value) . "' />"
                        . '<div class="btn-group">'
                        . '<a href="' . site_url($obj->controller_path . '/edit/' . $value->customer_id) . '" class="btn btn-info btn-sm">'
                        . '<i class="fa fa-pencil"></i></a>'
                        . '<a href="#" data-toggle="modal" data-target="#modal-delete-alert" class="btn btn-danger btn-sm btn-delete">'
                        . '<i class="fa fa-trash"></i></a>'
                        . '<a href="' . site_url('customers/customers_price/index/' . $value->customer_id) . '" class="btn btn-sm btn-success"><i class="fa fa-usd"></i></a>'
                        . '</div>';
            }
        } else {
            $this->result['data'] = [];
        }

        return json_encode($this->result);
    }

    private function attr_base($request = array()) {

        // $this->attr['branch'] = $this->profile->branch_name;
        // $this->attr['branch_access'] = $this->profile->branch_access;
        $this->attr['order'] = $request['order'];
    }

}
