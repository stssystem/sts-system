<?php

$config['form_rules/users'] = [
    [
        'field' => 'username',
        'label' => 'Username',
        'rules' => 'required|max_length[30]'
    ], [
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'required|max_length[50]'
    ], [
        'field' => 'user_email',
        'label' => 'Accademic Year',
        'rules' => 'required|max_length[100]'
    ]
];
