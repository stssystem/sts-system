<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

$config['email'] = array(
    'protocol' => 'smtp',
    'smtp_host' => 'localhost',
    'smtp_port' => '25',
    'smtp_timeout' => '7',
    'charset' => 'utf-8',
    'newline' => "\r\n",
    'mailtype' => 'html',
    'wordwrap' => 'TRUE',
    'validation' => FALSE
);

//$config['email'] = array(
//    'protocol' => 'smtp',
//    'smtp_host' => 'ssl://smtp.yahoo.com',
//    'smtp_port' => '995',
//    'smtp_timeout' => '7',
//    'smtp_user' => 'stsnoreply@yahoo.com',
//    'smtp_pass' => '123456789abc',
//    'charset' => 'utf-8',
//    'newline' => "\r\n",
//    'mailtype' => 'text',
//    'validation' => FALSE
//);

