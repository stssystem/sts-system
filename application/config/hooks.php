<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | Hooks
  | -------------------------------------------------------------------------
  | This file lets you define "hooks" to extend CI without hacking the core
  | files.  Please see the user guide for info:
  |
  |	https://codeigniter.com/user_guide/general/hooks.html
  |
 */

$hook['post_controller'][] = array(
    'class' => 'Tab_restrict',
    'function' => 'index',
    'filename' => 'Tab_restrict.php',
    'filepath' => 'hooks'
);

$hook['post_controller'][] = array(
    'class' => 'Track_url',
    'function' => 'index',
    'filename' => 'Track_url.php',
    'filepath' => 'hooks'
);

