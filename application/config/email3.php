<?php
//$this->load->library('email');

$this->email->initialize(array(
  'protocol' => 'smtp',
  'smtp_host' => 'localhost',
  'smtp_user' => 'noreply',
  'smtp_port' => 25,
  'crlf' => "\r\n",
  'newline' => "\r\n"
));

$this->email->from('dwippoer@gmail.com', 'Dwi Purnomo');
$this->email->to('dwi@softwareseni.com');
$this->email->subject('Email Test');
$this->email->message('Testing the email class.');
$this->email->send();

echo $this->email->print_debugger();
?>
