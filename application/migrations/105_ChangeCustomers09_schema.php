<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomers09_schema extends CI_Migration {

    public function up() {

        /** Add columns at packages table */
        $fields = array(
            'customer_province_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'customer_districts_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'customer_village_id' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        );

        $this->dbforge->add_column('customers', $fields);
    }

    public function down() {

        /** Add columns at customers table */
        $this->dbforge->drop_column('customers', 'customer_province_id');
        $this->dbforge->drop_column('customers', 'customer_districts_id');
        $this->dbforge->drop_column('customers', 'customer_village_id');

    }

}
