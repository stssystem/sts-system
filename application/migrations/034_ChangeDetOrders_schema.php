<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeDetOrders_schema extends CI_Migration {

    public function up() {

        /** Add columns at det_order table */
        $fields = array(
            'det_order_qty' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            /**  The number to be filled manually after automatic calculation */
            'det_order_sell_total' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );
        $this->dbforge->add_column('det_order', $fields);
    }

    public function down() {
        /** Drop columns at det_order table */
        $this->dbforge->drop_column('det_order', 'det_order_qty');
        $this->dbforge->drop_column('det_order', 'det_order_sell_total');
    }

}
