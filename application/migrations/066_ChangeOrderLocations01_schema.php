<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrderLocations01_schema extends CI_Migration {

    public function up() {

        /** Add columns at orders locations table */
        $fields = array(
            'orders_locations_city_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'orders_locations_branch_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('orders_locations', $fields);
        
    }

    public function down() {

        /** Drop columns at orders table */
        $this->dbforge->drop_column('orders_locations', 'orders_locations_city_id');
        $this->dbforge->drop_column('orders_locations', 'orders_locations_branch_id');
        
    }

}
