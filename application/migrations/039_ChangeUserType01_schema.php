<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeUserType01_schema extends CI_Migration {

    public function up() {

        /** Add columns at det_order table */
        $fields = array(
            'user_type_access' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
            )
        );

        $this->dbforge->add_column('user_types', $fields);
        
    }

    public function down() {

        /** Drop columns at orders table */
        $this->dbforge->drop_column('user_types', 'user_type_access');
        
    }

}