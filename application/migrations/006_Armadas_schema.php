<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Armadas_schema extends CI_Migration {

    public function up() {

        /** Armada column definition */
        $this->dbforge->add_field(
                array(
                    'armada_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'traccar_device_id' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'armada_name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'armada_v_width' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'armada_v_long' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'armada_v_height' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'armada_v_m3' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'armada_last_positin' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '200'
                    ),
                    'armada_photo' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'armada_license_plate' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '20'
                    ),
                    'armada_reg_date' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'armada_custom_code' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'armada_status' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );
        $this->dbforge->add_key('armada_id', TRUE);
        $this->dbforge->create_table('armadas');
    }

    public function down() {
        /** Drop sys_config table */
        $this->dbforge->drop_table('armadas');
    }

}
