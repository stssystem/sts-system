<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrders04_schema extends CI_Migration {

    public function up() {

        /** Add columns at armadas table */
        $fields = array(
            'order_id' => array(
                'name' => 'order_id',
                'type' => 'VARCHAR',
                'constraint' => 100
            )
        );
        $this->dbforge->modify_column('orders', $fields);
    }

    public function down() {

        /** Change columns at armadas table */
        $fields = array(
            'order_id' => array(
                'name' => 'order_id',
                'type' => 'INT',
                'constraint' => 11
            )
        );
        $this->dbforge->modify_column('orders', $fields);
    }

}
