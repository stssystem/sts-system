<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Regional_schema extends CI_Migration {

    public function up() {

        /** Add columns at regional table */
        $this->dbforge->add_field(array(
            'regional_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'regional' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'deleted_at' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        ));

        $this->dbforge->add_key('regional_id', TRUE);
        $this->dbforge->create_table('regionals');
    }

    public function down() {

        /** Drop regional table */
        $this->dbforge->drop_table('regionals');
    }

}
