<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangePackages_schema extends CI_Migration {

    public function up() {

        /** Add column branch_id at column userprofile */
        $fields = array(
            'package_pre_manifest_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'package_manifest_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('packages', $fields);
    }

    public function down() {

        /** Drop branch_id column at userprofile table */
        $this->dbforge->drop_column('packages', 'package_pre_manifest_id');
        $this->dbforge->drop_column('packages', 'packages');
        
    }

}
