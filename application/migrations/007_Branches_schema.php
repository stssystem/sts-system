<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Branches_schema extends CI_Migration {

    public function up() {

        /** Branches column definition */
        $this->dbforge->add_field(
                array(
                    'branch_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'branch_name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'branch_address' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '200'
                    ),
                    'branch_geo' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '200'
                    ),
                    'branch_parent' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'branch_manager' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'branch_status' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );
        $this->dbforge->add_key('branch_id', TRUE);
        $this->dbforge->create_table('branches');
    }

    public function down() {
        /** Drop branches table */
        $this->dbforge->drop_table('branches');
    }

}
