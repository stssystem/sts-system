<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangePackages02_schema extends CI_Migration {

    public function up() {

        /** Add columns at packages table */
        $fields = array(
            'package_width' => array(
                'type' => 'FLOAT',
                'constraint' => 11
            ),
            'package_height' => array(
                'type' => 'FLOAT',
                'constraint' => 11
            ),
            'package_lenght' => array(
                'type' => 'FLOAT',
                'constraint' => 11
            ),
            'package_size' => array(
                'type' => 'FLOAT',
                'constraint' => 11
            ),
            'package_weight' => array(
                'type' => 'FLOAT',
                'constraint' => 11
            )
        );

        $this->dbforge->modify_column('packages', $fields);
    }

    public function down() {

        /** Add columns at packages table */
        $fields = array(
            'package_width' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'package_height' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'package_lenght' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'package_size' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'package_weight' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        );

        $this->dbforge->modify_column('packages', $fields);
    }

}
