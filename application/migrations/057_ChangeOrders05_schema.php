<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrders05_schema extends CI_Migration {

    public function up() {

        /** Add columns at orders table */
        $fields = array(
            'orders_due_date' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('orders', $fields);
    }

    public function down() {

        /** Drop columns at orders table */
        $this->dbforge->drop_column('orders', 'orders_due_date');
        
    }

}
