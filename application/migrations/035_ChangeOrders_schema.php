<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrders_schema extends CI_Migration {

    public function up() {

        /** Add columns at det_order table */
        $fields = array(
            /** Sum of det_order_sell_total */
            'order_sell_total' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            /** Values :  1 -> no, 2 -> yes */
            'order_use_tax' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );
        $this->dbforge->add_column('orders', $fields);
    }

    public function down() {

        /** Drop columns at det_order table */
        $fields = array(
            'order_sell_total',
            'order_use_tax'
        );

        $this->dbforge->drop_column('orders', 'order_sell_total');
        $this->dbforge->drop_column('orders', 'order_use_tax');
    }

}
