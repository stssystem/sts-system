<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Province_schema extends CI_Migration {

	public function up() {

        /** Armada column definition */
        $this->dbforge->add_field(
                array(
                    'province_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'province_name' => array(
                    	'type' => 'VARCHAR',
                    	'constraint' => 50
                    )
                )
        );
        $this->dbforge->add_key('province_id', TRUE);
        $this->dbforge->create_table('provinces');

        /** add province_id field */

        /** Add columns at armadas table */
        $fields = array(
            'province_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('cities', $fields);
    }

    public function down() {
        /** Drop sys_config table */
        $this->dbforge->drop_table('provinces');
        $this->dbforge->drop_column('cities', 'province_id');
    }
}