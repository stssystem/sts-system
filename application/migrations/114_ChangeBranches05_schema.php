<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeBranches05_schema extends CI_Migration {

    public function up() {

        /** Load report column definition */
        $fields = array(
            'branch_comission' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE
            )
        );

        $this->dbforge->add_column('branches', $fields);
        
    }

    public function down() {

        /** Drop sys_config table */
        $this->dbforge->drop_column('branch_comission', 'branches');
        
    }

}
