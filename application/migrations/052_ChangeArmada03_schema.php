<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeArmada03_schema extends CI_Migration {

    public function up() {

        /** Add columns at armadas table */
        $fields = array(
            'armada_kir_number' => array(
                'name' => 'armada_kir_number',
                'type' => 'INT',
                'constraint' => 11
            ),
            'armada_stnk_number' => array(
                'name' => 'armada_stnk_number',
                'type' => 'INT',
                'constraint' => 11
            )
        );
        $this->dbforge->modify_column('armadas', $fields);
    }

    public function down() {

        /** Change columns at armadas table */
        $fields = array(
            'armada_kir_number' => array(
                'name' => 'armada_kir_number',
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'armada_stnk_number' => array(
                'name' => 'armada_stnk_number',
                'type' => 'VARCHAR',
                'constraint' => 100
            )
        );
        $this->dbforge->modify_column('armadas', $fields);
    }

}
