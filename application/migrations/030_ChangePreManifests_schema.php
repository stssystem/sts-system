<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangePreManifests_schema extends CI_Migration {

    public function up() {

        /** Add column pre_manifest_status at column pre_manifest */
        $fields = array(
            'pre_manifest_status' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('pre_manifest', $fields);
        
    }

    public function down() {

        /** Drop pre_manifest_status column at pre_manifest table */
        $this->dbforge->drop_column('pre_manifest', 'pre_manifest_status');
        
    }

}
