<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeUserProfile_schema extends CI_Migration {

    public function up() {

        /** Add column branch_id at column userprofile */
        $fields = array(
            'branch_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('userprofile', $fields);
    }

    public function down() {

        /** Drop branch_id column at userprofile table */
        $this->dbforge->drop_column('userprofile', 'branch_id');
        
    }

}
