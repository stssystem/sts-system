<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeTrips01_schema extends CI_Migration {

    public function up() {

        /** Add columns at trips table */
        $fields = array(
            'trips_loaded_volume' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'trips_loaded_weight' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
        );

        $this->dbforge->add_column('trips', $fields);
    }

    public function down() {

        /** Drop columns at trips table */
        $this->dbforge->drop_column('trips', 'trips_loaded_volume');
        $this->dbforge->drop_column('trips', 'trips_loaded_weight');
    }

}
