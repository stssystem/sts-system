<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_PackageManagement_schema extends CI_Migration {

    public function up() {

        /** Load report column definition */
        $this->dbforge->add_field(
                array(
                    'package_management_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'resi_number' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '50',
                        'null' => TRUE,
                    ),
                    'resi_number_manual' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '50',
                        'null' => TRUE,
                    ),
                    'resi_date' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    'do_date' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    'armada_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    'armada_name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100',
                         'null' => TRUE,
                    ),
                    'armada_plate_number' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '20',
                         'null' => TRUE,
                    ),
                    'courier_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    'courier' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '20',
                         'null' => TRUE,
                    ),
                    'origin' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    'destination' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    'branch_id_do' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    'branch_do' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '50',
                         'null' => TRUE,
                    ),
                    'city_do' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '50',
                         'null' => TRUE,
                    ),
                    'koli' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    'weight' => array(
                        'type' => 'FLOAT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    'volume' => array(
                        'type' => 'FLOAT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    // Value : load, unload, transit, delivery
                    'type_do' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '20',
                         'null' => TRUE,
                    ),
                    'note' => array(
                        'type' => 'TEXT',
                         'null' => TRUE,
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                         'null' => TRUE,
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'null' => TRUE,
                    )
                )
        );

        $this->dbforge->add_key('package_management_id', TRUE);
        $this->dbforge->create_table('package_management');
    }

    public function down() {

        /** Drop sys_config table */
        $this->dbforge->drop_table('package_management');
    }

}
