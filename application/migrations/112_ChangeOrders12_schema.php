<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrders12_schema extends CI_Migration {

    public function up() {

        /** Add columns at track_url table */
        $fields = array(
            'order_load_date' => array(
                'type' => 'INT'
            ),
            'order_unload_date' => array(
                'type' => 'INT'
            ),
            'order_delivered_date' => array(
                'type' => 'INT'
            ),
            'order_received_date' => array(
                'type' => 'INT'
            )
        );

        $this->dbforge->add_column('orders', $fields);
    }

    public function down() {
        /** Add columns at customers table */
        $this->dbforge->drop_column('order_load_date', 'orders');
        $this->dbforge->drop_column('order_unload_date', 'orders');
        $this->dbforge->drop_column('order_delivered_date', 'orders');
        $this->dbforge->drop_column('order_received_date', 'orders');
    }

}
