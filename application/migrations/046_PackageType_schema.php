<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_PackageType_schema extends CI_Migration {

    public function up() {

        /** Cities column definition */
        $this->dbforge->add_field(
                array(
                    'package_type_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'package_type_name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );
        
        $this->dbforge->add_key('package_type_id', TRUE);
        
        $this->dbforge->create_table('package_types');

    }

    public function down() {

        /** Drop cities table */
        $this->dbforge->drop_table('package_types');

    }

}
