<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_PackageEditLocation_schema extends CI_Migration {

    public function up() {

        /** prices detail column definition */
        $this->dbforge->add_field(
            array(
                'pckg_edit_loc_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                    ),
                'order_id' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 100,
                    ),
                'pckg_edit_loc_type' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'pckg_edit_loc_origin' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'pckg_edit_loc_origin_text' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 150
                    ),
                'pckg_edit_loc_destination' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'pckg_edit_loc_destination_text' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 150
                    ),
                'city_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'branch_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'user_staff_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'user_approved_staff_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'pckg_edit_loc_date_updated' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    ),
                'pckg_edit_loc_date_approved' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    ),
                'branch_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'regional_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'order_status' => array(
                    'type' => 'INT',
                    'constraint' => 2,
                    ),
                'pckg_edit_loc_status' => array(
                    'type' => 'INT',
                    'constraint' => 2,
                    ),
                'created_at' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'updated_at' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    )
                )
            );

        $this->dbforge->add_key('pckg_edit_loc_id', TRUE);
        $this->dbforge->create_table('package_edit_location');
    }

    public function down() {

        /** Drop prices detail table */
        $this->dbforge->drop_table('package_edit_location');
    }

}
