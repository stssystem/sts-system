<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangePackageEdit01_schema extends CI_Migration {

    public function up() {

        /** Add columns at regional table */
        $fields = array(
            'order_status' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        if (!$this->db->field_exists('order_status', 'package_edit')) {
            $this->dbforge->add_column('package_edit', $fields);
        }
        
    }

    public function down() {

        /** Drop regional table */
        if ($this->db->field_exists('order_status', 'package_edit')) {
            $this->dbforge->drop_column('package_edit', 'order_status');
        }
        
    }

}
