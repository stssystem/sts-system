<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Orders_schema extends CI_Migration {

    public function up() {

        /** Orders column definition */
        $this->dbforge->add_field(
                array(
                    'order_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'order_date' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'order_manual_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'customer_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'order_status' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'staff_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'order_notes' => array(
                        'type' => 'TEXT'
                    ),
                    'order_origin' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'order_origin_text' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    ),
                    'order_destination' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'order_destination_text' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    ),
                    'order_total' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('order_id', TRUE);
        $this->dbforge->create_table('orders');
    }

    public function down() {

        /** Drop orders table */
        $this->dbforge->drop_table('orders');
        
    }

}
