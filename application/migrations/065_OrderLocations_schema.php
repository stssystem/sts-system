<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_OrderLocations_schema extends CI_Migration {

    public function up() {

        /** Orders locations column definition */
        $this->dbforge->add_field(
                array(
                    'orders_locations_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'orders_locations_order_id' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'orders_locations_last' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );
        
        $this->dbforge->add_key('orders_locations_id', TRUE);
        
        $this->dbforge->create_table('orders_locations');
        
    }

    public function down() {

        /** Drop cities table */
        $this->dbforge->drop_table('orders_locations');
        
    }

}
