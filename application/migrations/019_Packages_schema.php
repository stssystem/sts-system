<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Packages_schema extends CI_Migration {

    public function up() {

        /** Packages detail column definition */
        $this->dbforge->add_field(
                array(
                    'package_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'package_title' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    ),
                    'package_type' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'package_origin' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'package_destination' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'package_status' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'package_location' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    ),
                    'package_last_location' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    ),
                    'package_content' => array(
                        'type' => 'TEXT'
                    ),
                    'package_width' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'package_height' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'package_lenght' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'package_size' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'package_weight' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('package_id', TRUE);
        $this->dbforge->create_table('packages');
    }

    public function down() {

        /** Drop packages detail table */
        $this->dbforge->drop_table('packages');
    }

}
