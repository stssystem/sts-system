<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Message01_schema extends CI_Migration {

    public function up() {

        /** Add columns at orders table */
        $fields = array(
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('messages', $fields);
    }

    public function down() {

        /** Drop columns at orders table */
        $this->dbforge->drop_column('messages', 'user_id');
        
    }

}
