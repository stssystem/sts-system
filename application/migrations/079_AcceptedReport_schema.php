<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_AcceptedReport_schema extends CI_Migration {

    public function up() {

        /** Add columns at regional table */
        $this->dbforge->add_field(array(
            'accepted_report_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'type' => array(
                'type' => 'INT',
                'constraint' => 11, /** 1 : Ambil Sendiri, 2 : Pengantaran */
            ),
            'accepted_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 11, 
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'deleted_at' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        ));

        $this->dbforge->add_key('accepted_report_id', TRUE);
        $this->dbforge->create_table('accepted_report');
        
    }

    public function down() {

        /** Drop regional table */
        $this->dbforge->drop_table('accepted_report');
    }

}
