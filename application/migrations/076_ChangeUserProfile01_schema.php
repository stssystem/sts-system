<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeUserProfile01_schema extends CI_Migration {

    public function up() {

        /** Add column regional_id at column userprofile */
        $fields = array(
            'regional_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('userprofile', $fields);
    }

    public function down() {

        /** Drop regional_id column at userprofile table */
        $this->dbforge->drop_column('userprofile', 'regional_id');
        
    }

}
