<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrders13_schema extends CI_Migration {

    public function up() {

        /** Load report column definition */
        $fields = array(
            'order_payment_status' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE
            )
        );

        $this->dbforge->add_column('orders', $fields);
        
    }

    public function down() {

        /** Drop sys_config table */
        $this->dbforge->drop_column('order_payment_status', 'orders');
        
    }

}
