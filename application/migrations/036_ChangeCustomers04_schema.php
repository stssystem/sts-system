<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomers04_schema extends CI_Migration {

    public function up() {

        /** Modify costumer_name column at costumer table */
        $fields = array(
            'customer_gende' => array(
                'name' => 'customer_gender',
                'type' => 'INT',
                'constraint' => 11
            ),
        );
        $this->dbforge->modify_column('customers', $fields);
    }

    public function down() {
        
       /** Drop costumer_name column at costumer table */
        $fields = array(
            'customer_gender' => array(
                'name' => 'customer_gende',
                'type' => 'INT',
                'constraint' => 11
            ),
        );
        
        $this->dbforge->modify_column('customers', $fields);
        
    }

}
