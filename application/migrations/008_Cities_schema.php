<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Cities_schema extends CI_Migration {

    public function up() {

        /** Cities column definition */
        $this->dbforge->add_field(
                array(
                    'city_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'city_name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );
        $this->dbforge->add_key('city_id', TRUE);
        $this->dbforge->create_table('cities');

        /** Add branch_city column in branch table */
        $fields = array(
            'branch_city' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('branches', $fields);
    }

    public function down() {

        /** Drop cities table */
        $this->dbforge->drop_table('cities');

        /** Drio colum branch_city on table branches */
        $this->dbforge->drop_column('branches', 'branch_city');
    }

}
