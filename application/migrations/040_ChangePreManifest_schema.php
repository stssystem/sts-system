<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangePreManifest_schema extends CI_Migration {

    public function up() {

        /** Add columns at det_order table */
        $fields = array(
            'pre_manifest_manifest_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('pre_manifest', $fields);
        
    }

    public function down() {

        /** Drop columns at orders table */
        $this->dbforge->drop_column('pre_manifest', 'pre_manifest_manifest_id');
        
    }

}