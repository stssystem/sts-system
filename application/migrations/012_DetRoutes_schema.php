<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_DetRoutes_schema extends CI_Migration {

    public function up() {

        /** Costumer column definition */
        $this->dbforge->add_field(
                array(
                    'det_route_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'route_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'route_city_start' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'route_city_end' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('det_route_id', TRUE);
        $this->dbforge->create_table('det_routes');
    }

    public function down() {

        /** Drop costumer table */
        $this->dbforge->drop_table('det_routes');
    }

}
