<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Syslogs_schema extends CI_Migration {

    public function up() {

        /** Syslogs column definition */
        $this->dbforge->add_field(
                array(
                    'syslog_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'syslog_date' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                    ),
                    'syslog_user' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'syslog_message' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '200'
                    ),
                    'sysllog_for' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );
        
        $this->dbforge->add_key('syslog_id', TRUE);
        $this->dbforge->create_table('syslogs');
        
    }

    public function down() {

        /** Drop syslogs table */
        $this->dbforge->drop_table('syslogs');
        
    }

}
