<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Price_schema extends CI_Migration {

    public function up() {

        /** Price column definition */
        $this->dbforge->add_field(
                array(
                    'price_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'price_branch_origin_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'price_branch_destination_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'price_kg_retail' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'price_kg_partai' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'price_volume_retail' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'price_volume_partai' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'minimum' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );
        $this->dbforge->add_key('price_id', TRUE);
        $this->dbforge->create_table('prices');
    }

    public function down() {

        /** Drop costumer_name column at costumer table */
        $this->dbforge->drop_table('prices');
        
    }

}
