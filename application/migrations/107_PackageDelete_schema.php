<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_PackageDelete_schema extends CI_Migration {

    public function up() {

        /** pckgs detail column definition */
        $this->dbforge->add_field(
            array(
                'package_delete_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                    ),
                'order_id' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 50,
                    ),
                'package_id' => array(
                    'type' => 'INT',
                    'constraint' => 50
                    ),
                'date_updated' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    ),
                'date_approved' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    ),
                'branch_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'regional_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'package_delete_status' => array(
                    'type' => 'INT',
                    'constraint' => 2,
                    ),
                'user_staff_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'user_approved_staff_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'package_notes' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    ),
                'created_at' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'updated_at' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    )
                )
            );

        $this->dbforge->add_key('package_delete_id', TRUE);
        $this->dbforge->create_table('package_delete');
    }

    public function down() {

        /** Drop pckgs detail table */
        $this->dbforge->drop_table('package_delete');
    }

}
