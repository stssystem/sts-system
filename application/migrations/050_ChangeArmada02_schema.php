<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeArmada02_schema extends CI_Migration {

    public function up() {

        /** Add columns at armadas table */
        $fields = array(
            'armada_driver' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('armadas', $fields);
    }

    public function down() {

        /** Drop columns at armadas table */
        $this->dbforge->drop_column('armadas', 'armada_driver');
        
    }

}
