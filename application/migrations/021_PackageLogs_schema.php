<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_PackageLogs_schema extends CI_Migration {

    public function up() {

        /** Package logs detail column definition */
        $this->dbforge->add_field(
                array(
                    'package_log_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'package_log_date' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'package_log_notes' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '200'
                    ),
                    'package_log_user' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('package_log_id', TRUE);
        $this->dbforge->create_table('package_log');
    }

    public function down() {

        /** Drop package pics detail table */
        $this->dbforge->drop_table('package_log');
    }

}
