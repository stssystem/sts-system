<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_PackageEdit_schema extends CI_Migration {

    public function up() {

        /** Packages detail column definition */
        $this->dbforge->add_field(
            array(
                'package_edit_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                    ),
                'order_id' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 100,
                    'unsigned' => TRUE,
                    ),
                'package_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'package_edit_title' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '150'
                    ),
                'package_edit_type' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'package_edit_content' => array(
                    'type' => 'TEXT'
                    ),
                'package_edit_width' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'package_edit_height' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'package_edit_lenght' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'package_edit_weight' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'package_edit_size' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'user_staff_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'user_approved_staff_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'package_edit_date_updated' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    ),
                'package_edit_date_approved' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    ),
                'branch_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'regional_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    ),
                'order_status' => array(
                    'type' => 'INT',
                    'constraint' => 3,
                    ),
                'package_edit_status' => array(
                    'type' => 'INT',
                    'constraint' => 3,
                    ),
                'created_at' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    ),
                'updated_at' => array(
                    'type' => 'INT',
                    'constraint' => 11
                    )
                )
            );

        $this->dbforge->add_key('package_edit_id', TRUE);
        $this->dbforge->create_table('package_edit');
    }

    public function down() {

        /** Drop packages detail table */
        $this->dbforge->drop_table('package_edit');
    }

}
