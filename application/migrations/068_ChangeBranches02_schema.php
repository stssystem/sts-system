<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeBranches02_schema extends CI_Migration {

    public function up() {

        /** Add columns at orders table */
        $fields = array(
            'branch_pickup_available' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('branches', $fields);
        
    }

    public function down() {

        /** Drop columns at orders table */
        $this->dbforge->drop_column('branches', 'branch_pickup_available');
        
    }

}
