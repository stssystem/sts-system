<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangePackageEdit02_schema extends CI_Migration {

    public function up() {

        /** Add columns at regional table */
        $fields = array(
            'user_staff_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        if (!$this->db->field_exists('user_staff_id', 'package_edit')) {
            $this->dbforge->add_column('package_edit', $fields);
        }
    }

    public function down() {

        /** Drop regional table */
        if ($this->db->field_exists('user_staff_id', 'package_edit')) {
            $this->dbforge->drop_column('package_edit', 'user_staff_id');
        }
        
    }

}
