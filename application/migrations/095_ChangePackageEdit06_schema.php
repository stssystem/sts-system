<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangePackageEdit06_schema extends CI_Migration {

    public function up() {

        /** Add column regional_id at column userprofile */
        $fields = array(
            'package_qty' => array(
                'type' => 'INT',
                'constraint' => 11,
                ),
            );

        $this->dbforge->add_column('package_edit', $fields);
    }

    public function down() {

        /** Drop regional_id column at package_edit table */
        $this->dbforge->drop_column('package_edit', 'package_qty');
        
    }

}