<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrders07_schema extends CI_Migration {

    public function up() {

        /** Add columns at orders table */
        $fields = array(
            'order_id_web' => array(
                'type' => 'VARCHAR',
                'constraint' => '15',
            )
        );

        $this->dbforge->add_column('orders', $fields);
        
    }

    public function down() {

        /** Drop columns at orders table */
        $this->dbforge->drop_column('orders', 'order_id_web');
        
    }

}
