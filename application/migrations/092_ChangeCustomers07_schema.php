<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomers07_schema extends CI_Migration {

    public function up() {

        /** Add columns at packages table */
        $fields = array(
            'customer_company_type_of_business' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'customer_company_owner_first_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'customer_company_owner_last_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'customer_company_province_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'customer_company_city_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'customer_company_districts_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'customer_company_village_id' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        );

        $this->dbforge->add_column('customers', $fields);
    }

    public function down() {

        /** Add columns at customers table */
        $this->dbforge->drop_column('customers', 'customer_company_type_of_business');
        $this->dbforge->drop_column('customers', 'customer_company_owner_first_name');
        $this->dbforge->drop_column('customers', 'customer_company_owner_last_name');
        $this->dbforge->drop_column('customers', 'customer_company_province_id');
        $this->dbforge->drop_column('customers', 'customer_company_city_id');
        $this->dbforge->drop_column('customers', 'customer_company_districts_id');
        $this->dbforge->drop_column('customers', 'customer_company_village_id');

    }

}
