<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomers06_schema extends CI_Migration {

    public function up() {

        /** Add columns at regional table */
        $fields = array(
            'branch_created' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('customers', $fields);
        
    }

    public function down() {

        /** Drop regional table */
        $this->dbforge->drop_column('customers', 'branch_created');
        
    }

}