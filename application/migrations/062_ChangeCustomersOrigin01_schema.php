<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomersOrigin01_schema extends CI_Migration {

    public function up() {

        /**
         * Modify customers_origin_user_id and 
         * customers_origin_address_id column 
         * in customers_origin table 
         */
        $fields = array(
            'customers_origin_user_id' => array(
                'name' => 'customers_origin_customer_id',
                'type' => 'INT',
                'constraint' => 11
            ),
            'customers_origin_address_id' => array(
                'name' => 'customers_origin_address',
                'type' => 'TEXT'
            )
        );

        $this->dbforge->modify_column('customers_origin', $fields);
    }

    public function down() {

        /** Rollback customers_origin table */
        $fields = array(
            'customers_origin_customer_id' => array(
                'name' => 'customers_origin_user_id',
                'type' => 'INT',
                'constraint' => 11
            ),
            'customers_origin_address' => array(
                'name' => 'customers_origin_address_id',
                'type' => 'TEXT'
            )
        );

        $this->dbforge->modify_column('customers_origin', $fields);
        
    }

}
