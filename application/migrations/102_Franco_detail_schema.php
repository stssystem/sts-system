<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Franco_detail_schema extends CI_Migration {

    public function up() {

        /** Orders detail column definition */
        $this->dbforge->add_field(
                array(
                    'franco_detail_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'franco_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'armada_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'nopol' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '20'
                    ),
                    'vendor_name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'last_branch' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'last_city' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'last_geo' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    ),
                    'note' => array(
                        'type' => 'TEXT',
                    ),
                    // From table user, define who is deliver
                    'courier' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'depart_date' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'achieve_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    // 1 : Antar Resi |  2 : Kembalikan Resi | 3 : Pengantaran Resi | 4 : Selesai
                    'status' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('franco_detail_id', TRUE);

        if (!$this->db->table_exists('franco_detail'))
            $this->dbforge->create_table('franco_detail');
    }

    public function down() {

        /** Drop orders detail table */
        if (!$this->db->table_exists('franco_detail'))
            $this->dbforge->drop_table('franco_detail');
    }

}
