<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomers02_schema extends CI_Migration {

    public function up() {

        /** Add columns at customers table */
        $fields = array(
            'customer_id_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            /** Values : 1 => KTP, 2=> SIM , 3=> NPWP, 4 => Others */
            'customer_id_type' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'customer_id_type_other' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'customer_birth_date' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            /** Values :  => 1 => Harian, 2 => Mingguan, 3 => Bulanan */
            'customer_freq_sending' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'customer_sending_quota' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            /** Values :  1 -> Manufaktur, 2-> Jasa, 3 -> Ritel, 4-> Lainnya */
            'customer_company_type' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'customer_company_type_other' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            /** Values :  1=> Laki-laki, 2=> Perempuan */
            'customer_gende' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'customer_referral' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
        );
        $this->dbforge->add_column('customers', $fields);
    }

    public function down() {

        /** Drop columns at customers table */
        $fields = array(
            'customer_type',
            'customer_id_number',
            'customer_id_type',
            'customer_id_type_other',
            'customer_birth_date',
            'customer_freq_sending',
            'customer_sending_quota',
            'customer_company_type',
            'customer_company_type_other',
            'customer_gender',
            'customer_referral',
        );

        $this->dbforge->drop_column('customers', $fields);
    }

}
