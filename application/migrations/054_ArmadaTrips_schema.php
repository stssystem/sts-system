<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ArmadaTrips_schema extends CI_Migration {

    public function up() {

        /** Armada column definition */
        $this->dbforge->add_field(
                array(
                    'armadatrip_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'armadatrip_armada_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'armadatrip_package_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );
        $this->dbforge->add_key('armadatrip_id', TRUE);
        $this->dbforge->create_table('armadatrips');
    }

    public function down() {
        /** Drop sys_config table */
        $this->dbforge->drop_table('armadatrips');
    }

}
