<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangePackageEditOrdersExtra01_schema extends CI_Migration {

    public function up() {

        /** Add column regional_id at column package_edit_orders_extra */
        $fields = array(
            'package_notes' => array(
                'type' => 'VARCHAR',
                'constraint' => 225,
                ),
            );

        $this->dbforge->add_column('package_edit_orders_extra', $fields);
    }

    public function down() {

        /** Drop regional_id column at package_edit_orders_extra table */
        $this->dbforge->drop_column('package_edit_orders_extra', 'package_notes');
        
    }

}