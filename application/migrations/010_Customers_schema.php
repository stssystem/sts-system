<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Customers_schema extends CI_Migration {

    public function up() {

        /** Costumer column definition */
        $this->dbforge->add_field(
                array(
                    'customer_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'customer_name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => 11,
                    ),
                    'customer_web_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'customer_address' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '200'
                    ),
                    'customer_phone' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '30'
                    ),
                      /** Values :  1 => Personal, 2 => Company */
                    'customer_type' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'customer_city' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'customer_last_transaction' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'customer_status' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('customer_id', TRUE);
        $this->dbforge->create_table('customers');
    }

    public function down() {

        /** Drop costumer table */
        $this->dbforge->drop_table('customers');
    }

}
