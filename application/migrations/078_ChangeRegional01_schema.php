<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeRegional01_schema extends CI_Migration {

    public function up() {

        /** Add columns at regional table */
        $fields = array(
            'regional_manager' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('regionals', $fields);
        
    }

    public function down() {

        /** Drop regional table */
        $this->dbforge->drop_column('regionals', 'regional_manager');
        
    }

}