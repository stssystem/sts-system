<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_SysConfig_schema extends CI_Migration {

    public function up() {

        /** System configuration column definition */
        $this->dbforge->add_field(
                array(
                    'main_logo' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'company_name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'company_address' => array(
                        'type' => 'TEXT'
                    ),
                    'online_url' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    ),
                    'traccar_url' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    ),
                    'traccar_db' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'traccar_db_username' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'traccar_db_password' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'web_url' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    ),
                    'web_db' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'web_db_username' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'web_db_password' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'license_key' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                )
        );

        $this->dbforge->create_table('sys_config');
    }

    public function down() {
        /** Drop sys_config table */
        $this->dbforge->drop_table('sys_config');
    }

}
