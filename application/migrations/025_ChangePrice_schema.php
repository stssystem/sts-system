<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangePrice_schema extends CI_Migration {

    public function up() {

        /** Add column branch_id at column userprofile */
        $fields = array(
            'minimum' => array(
                'name' => 'price_minimum',
                'type' => 'INT',
                'constraint' => 11
            )
        );
        $this->dbforge->modify_column('prices', $fields);
    }

    public function down() {

        /** Drop branch_id column at userprofile table */
        $fields = array(
            'price_minimum' => array(
                'name' => 'minimum',
                'type' => 'INT',
                'constraint' => 11
            )
        );
        
        $this->dbforge->modify_column('prices', $fields);
    }

}
