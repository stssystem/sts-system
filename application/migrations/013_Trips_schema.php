<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Trips_schema extends CI_Migration {

    public function up() {

        /** Costumer column definition */
        $this->dbforge->add_field(
                array(
                    'trip_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'route_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'armada_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'user_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'trip_start_date' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'trip_end_date' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('trip_id', TRUE);
        $this->dbforge->create_table('trips');
    }

    public function down() {

        /** Drop costumer table */
        $this->dbforge->drop_table('trips');
    }

}
