<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomers_schema extends CI_Migration {

    public function up() {

        /** Modify costumer_name column at costumer table */
        $fields = array(
            'customer_name' => array(
                'name' => 'customer_name',
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
        );
        $this->dbforge->modify_column('customers', $fields);
    }

    public function down() {
        
       /** Drop costumer_name column at costumer table */
        $fields = array(
            'customer_name' => array(
                'name' => 'customer_name',
                'type' => 'VARCHAR',
                'constraint' => 11
            ),
        );
        
        $this->dbforge->modify_column('customers', $fields);
        
    }

}
