<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Franco_schema extends CI_Migration {

    public function up() {

        /** Orders detail column definition */
        $this->dbforge->add_field(
                array(
                    'franco_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'order_id' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '50'
                    ),
                    'costumer_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'manual_resi' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '50'
                    ),
                    'origin' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'destination' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'note' => array(
                        'type' => 'TEXT'
                    ),
                    // 1 : Baru |  2 : Sudah Dikirm |  3 : Sudah Diterima | 4 : Diantarkan | 5 : Selesai
                    'status' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('franco_id', TRUE);

        if (!$this->db->table_exists('franco'))
            $this->dbforge->create_table('franco');
    }

    public function down() {

        /** Drop orders detail table */
        if ($this->db->table_exists('franco'))
            $this->dbforge->drop_table('franco');
    }

}
