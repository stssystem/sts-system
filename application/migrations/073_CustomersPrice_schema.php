<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_CustomersPrice_schema extends CI_Migration {

    public function up() {

        /** Add columns at price table */
        $this->dbforge->add_field(array(
            'customer_price_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'customer_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'customer_branch_id_origin' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'customer_branch_id_destination' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'customer_price' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'deleted_at' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        ));

        $this->dbforge->add_key('customer_price_id', TRUE);
        $this->dbforge->create_table('customers_price');
    }

    public function down() {

        /** Drop customers price table */
        $this->dbforge->drop_table('customers_price');
    }

}
