<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_User_schema extends CI_Migration {

    public function up() {

        //create table user
        $this->dbforge->add_field(array(
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => '30'
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '110'
            ),
            'last_login' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'user_status' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'user_email' => array(
                'type' => 'VARCHAR',
                'constraint' => 25
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'deleted_at' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        ));
        $this->dbforge->add_key('user_id', TRUE);
        $this->dbforge->create_table('user');
    }

    public function down() {
        //drop the table
        $this->dbforge->drop_table('user');
    }

}
