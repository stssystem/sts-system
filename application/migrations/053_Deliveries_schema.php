<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Deliveries_schema extends CI_Migration {

    public function up() {

        /** Armada column definition */
        $this->dbforge->add_field(
                array(
                    'delivery_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'delivery_package_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    /** Get from user */
                    'delivery_courier' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'delivery_depart_time' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'delivery_arrive_time' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'delivery_armada_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );
        $this->dbforge->add_key('delivery_id', TRUE);
        $this->dbforge->create_table('deliveries');
    }

    public function down() {
        /** Drop sys_config table */
        $this->dbforge->drop_table('deliveries');
    }

}
