<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeTrips02_schema extends CI_Migration {

    public function up() {

        /** Add columns at armadas table */
        $fields = array(
            'trip_codriver' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('trips', $fields);
    }

    public function down() {

        /** Drop columns at armadas table */
        $this->dbforge->drop_column('trips', 'trip_codriver');
        
    }

}
