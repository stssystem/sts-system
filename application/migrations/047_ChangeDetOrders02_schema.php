<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeDetOrders02_schema extends CI_Migration {

    public function up() {

        /** Add columns at det_order table */
        $fields = array(
            'det_order_package_parent' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
        );

        $this->dbforge->add_column('det_order', $fields);
        
    }

    public function down() {

        /** Drop columns at det_order table */
        $this->dbforge->drop_column('det_order', 'det_order_package_parent');
        
    }

}
