<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeArmadas01_schema extends CI_Migration {

    public function up() {

        /** Add columns at armadas table */
        $fields = array(
            'armada_tonase' => array(
                'type' => 'INT',
                'constraint' => 11,
            ), 
            'armada_kir_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ), 
            'armada_stnk_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            )
        );

        $this->dbforge->add_column('armadas', $fields);
    }

    public function down() {

        /** Drop columns at armadas table */
        $this->dbforge->drop_column('armadas', 'armada_tonase');
        $this->dbforge->drop_column('armadas', 'armada_kir_number');
        $this->dbforge->drop_column('armadas', 'armada_stnk_number');
        
    }

}
