<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomers08_schema extends CI_Migration {

    public function up() {

        /** Add columns at packages table */
        $fields = array(
            'pic_first_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'pic_last_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'pic_gender' => array(
                'type' => 'INT',
                'constraint' => 3
            ),
            'pic_identity_type' => array(
                'type' => 'INT',
                'constraint' => 3
            ),
            'pic_identity_type_other' => array(
                'type' => 'INT',
                'constraint' => 3
            ),
            'pic_identity_number' => array(
                'type' => 'INT',
                'constraint' => 3
            ),
            'pic_religion' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'pic_birth_date' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'pic_address' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'pic_province_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'pic_city_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'pic_districts_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'pic_village_id' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        );

        $this->dbforge->add_column('customers', $fields);
    }

    public function down() {

        /** Add columns at customers table */
        $this->dbforge->drop_column('customers', 'pic_first_name');
        $this->dbforge->drop_column('customers', 'pic_last_name');
        $this->dbforge->drop_column('customers', 'pic_gender');
        $this->dbforge->drop_column('customers', 'pic_identity_type');
        $this->dbforge->drop_column('customers', 'pic_identity_type_other');
        $this->dbforge->drop_column('customers', 'pic_identity_number');
        $this->dbforge->drop_column('customers', 'pic_religion');
        $this->dbforge->drop_column('customers', 'pic_birth_date');
        $this->dbforge->drop_column('customers', 'pic_address');
        $this->dbforge->drop_column('customers', 'pic_province_id');
        $this->dbforge->drop_column('customers', 'pic_city_id');
        $this->dbforge->drop_column('customers', 'pic_districts_id');
        $this->dbforge->drop_column('customers', 'pic_village_id');

    }

}
