<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeTrackUrl01_schema extends CI_Migration {

    public function up() {

        /** Add columns at track_url table */
        $fields = array(
            'post_get' => array(
                'type' => 'TEXT'
            ),
            'ajax' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            )
        );

        $this->dbforge->add_column('track_url', $fields);
    }

    public function down() {
        /** Add columns at customers table */
        $this->dbforge->drop_column('post_get', 'track_url');
        $this->dbforge->drop_column('ajax', 'track_url');
    }

}
