<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrders06_schema extends CI_Migration {

    public function up() {

        /** Modify order_manual_id column at det_order table */
        $fields = array(
            'order_manual_id' => array(
                'name' => 'order_manual_id',
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
        );
        
        $this->dbforge->modify_column('orders', $fields);
        
    }

    public function down() {

        /** Rollback order_id column at det_order table */
        $fields = array(
            'order_manual_id' => array(
                'name' => 'order_manual_id',
                'type' => 'INT',
                'constraint' => 11
            ),
        );

        $this->dbforge->modify_column('orders', $fields);
    }

}
