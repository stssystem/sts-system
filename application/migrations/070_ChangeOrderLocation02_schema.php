<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrderLocation02_schema extends CI_Migration {

    public function up() {

        /** Add columns at orders table */
        $fields = array(
            'note' => array(
                'type' => 'TEXT'
            )
        );

        $this->dbforge->add_column('orders_locations', $fields);
        
    }

    public function down() {
        /** Drop columns at orders table */
        $this->dbforge->drop_column('orders_locatons', 'note');
    }

}