<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_DetTrips_schema extends CI_Migration {

    public function up() {

        /** Costumer column definition */
        $this->dbforge->add_field(
                array(
                    'det_trip_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'trip_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'det_trip_city_start' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'det_trip_city_stop' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('det_trip_id', TRUE);
        $this->dbforge->create_table('det_trips');
    }

    public function down() {

        /** Drop costumer table */
        $this->dbforge->drop_table('det_trips');
    }

}
