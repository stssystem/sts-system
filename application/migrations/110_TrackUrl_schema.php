<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_TrackUrl_schema extends CI_Migration {

    public function up() {

        /** Armada column definition */
        $this->dbforge->add_field(
                array(
                    'track_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'url' => array(
                        'type' => 'TEXT'
                    ),
                    'username' => array(
                        'type' => 'VARCHAR',
                        'constraint' => 50
                    ),
                    'date' => array(
                        'type' => 'DATETIME'
                    )
                )
        );
        
        $this->dbforge->add_key('track_id', TRUE);
        $this->dbforge->create_table('track_url');
        
    }

    public function down() {
        /** Drop sys_config table */
        $this->dbforge->drop_table('track_url');
    }

}
