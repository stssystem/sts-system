<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeUser01_schema extends CI_Migration {

    public function up() {

        //create table user
        $fields = array(
            'user_email' => array(
                'name' => 'user_email',
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
        );
        $this->dbforge->modify_column('user', $fields);
    }

    public function down() {
        //drop the table
        $fields = array(
            'user_email' => array(
                'name' => 'user_email',
                'type' => 'VARCHAR',
                'constraint' => 25
            ),
        );
        $this->dbforge->modify_column('user', $fields);
        
    }

}
