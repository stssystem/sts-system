<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeManifest03_schema extends CI_Migration {

    public function up() {

        /** Add columns at det_order table */
        $fields = array(
            'manifest_status' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('manifest', $fields);
        
    }

    public function down() {

        /** Drop columns at orders table */
        $this->dbforge->drop_column('manifest', 'manifest_status');
        
    }

}