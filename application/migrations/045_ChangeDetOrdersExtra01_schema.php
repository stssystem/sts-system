<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeDetOrdersExtra01_schema extends CI_Migration {

    public function up() {

        /** Modify costumer_name column at costumer table */
        $fields = array(
            'det_order_extra_name' => array(
                'name' => 'det_order_extra_name',
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
        );
        $this->dbforge->modify_column('det_orders_extra', $fields);
    }

    public function down() {

        /** Modify costumer_name column at costumer table */
        $fields = array(
            'det_order_extra_name' => array(
                'name' => 'det_order_extra_name',
                'type' => 'VARCHAR',
                'constraint' => 11
            ),
        );
        $this->dbforge->modify_column('det_orders_extra', $fields);
    }

}
