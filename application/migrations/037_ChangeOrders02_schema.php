<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrders02_schema extends CI_Migration {

    public function up() {

        /** Add columns at det_order table */
        $fields = array(
            'destination_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            /**  The number to be filled manually after automatic calculation */
            'destination_telp' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            )
        );

        $this->dbforge->add_column('orders', $fields);
        
    }

    public function down() {

        /** Drop columns at orders table */
        $this->dbforge->drop_column('orders', 'destination_name');
        $this->dbforge->drop_column('orders', 'destination_telp');
        
    }

}
