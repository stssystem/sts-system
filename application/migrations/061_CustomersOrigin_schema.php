<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_CustomersOrigin_schema extends CI_Migration {

    public function up() {

        /** Customers origin column definition */
        $this->dbforge->add_field(
                array(
                    'customers_origin_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'customers_origin_user_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'customers_origin_province_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'customers_origin_city_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'customers_origin_districts_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'customers_origin_villages_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'customers_origin_branch_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'customers_origin_address_id' => array(
                        'type' => 'TEXT',
                    )
                )
        );
        
        $this->dbforge->add_key('customers_origin_id', TRUE);
        $this->dbforge->create_table('customers_origin');

    }

    public function down() {

        /** Drop customers origin table */
        $this->dbforge->drop_table('customers_origin');

    }

}
