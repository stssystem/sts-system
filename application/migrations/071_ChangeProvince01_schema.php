<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeProvince01_schema extends CI_Migration {

    public function up() {

        /** Add columns at orders table */
        $fields = array(
            'show' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('provinces', $fields);
        
    }

    public function down() {
        /** Drop columns at orders table */
        $this->dbforge->drop_column('provinces', 'show');
    }

}
