<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Message_schema extends CI_Migration {

    public function up() {

        /** Cities column definition */
        $this->dbforge->add_field(
                array(
                    'message_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'message_title' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'message_content' => array(
                        'type' => 'TEXT'
                    ),
                    'message_parent' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'message_status' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );
        $this->dbforge->add_key('message_id', TRUE);
        $this->dbforge->create_table('messages');

    }

    public function down() {

        /** Drop cities table */
        $this->dbforge->drop_table('messages');

    }

}
