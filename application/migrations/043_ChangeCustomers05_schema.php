<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomers05_schema extends CI_Migration {

    public function up() {

        /** Add columns at customers table */
        $fields = array(
            'customer_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '25',
            )
        );

        $this->dbforge->add_column('customers', $fields);
        
    }

    public function down() {

        /** Drop columns at customers table */
        $this->dbforge->drop_column('customers', 'customer_code');
        
    }

}