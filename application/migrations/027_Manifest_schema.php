<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Manifest_schema extends CI_Migration {

    public function up() {

        /** Add column branch_id at column userprofile */
        $this->dbforge->add_field(array(
            'manifest_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'manifest_date' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'manifest_exdate' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'manifest_created_by' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'manifest_origin' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'manifest_destination' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'manifest_armada_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'deleted_at' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        ));

        $this->dbforge->add_key('manifest_id', TRUE);

        $this->dbforge->create_table('manifest');
    }

    public function down() {

        /** Drop branch_id column at userprofile table */
        $this->dbforge->drop_table('manifest');
    }

}
