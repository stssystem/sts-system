<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangePackageEdit05_schema extends CI_Migration {

    public function up() {

        /** Add columns at package_edit table */
        $fields = array(
            'order_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '50'
            )
        );
        
        $this->dbforge->modify_column('package_edit', $fields);
        
    }

    public function down() {

        /** Add columns at package_edit table */
        $fields = array(
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        );
        
        $this->dbforge->modify_column('package_edit', $fields);
        
    }

}
