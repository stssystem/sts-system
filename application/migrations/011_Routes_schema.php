<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Routes_schema extends CI_Migration {

    public function up() {

        /** Costumer column definition */
        $this->dbforge->add_field(
                array(
                    'route_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'route_name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'route_status' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'created_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'updated_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'deleted_at' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('route_id', TRUE);
        $this->dbforge->create_table('routes');
    }

    public function down() {

        /** Drop costumer table */
        $this->dbforge->drop_table('routes');
    }

}
