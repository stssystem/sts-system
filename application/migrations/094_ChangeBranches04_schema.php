<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeBranches04_schema extends CI_Migration {

    public function up() {

        /** Add column regional_id at column userprofile */
        $fields = array(
            'branch_phone' => array(
                'type' => 'VARCHAR',
                'constraint' => 30,
                ),
            'branch_email' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                ),
            );

        $this->dbforge->add_column('branches', $fields);
    }

    public function down() {

        /** Drop regional_id column at branches table */
        $this->dbforge->drop_column('branches', 'branch_phone');
        $this->dbforge->drop_column('branches', 'branch_email');
        
    }

}