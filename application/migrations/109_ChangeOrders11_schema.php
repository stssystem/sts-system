<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrders11_schema extends CI_Migration {

    public function up() {

        /** Add columns at orders table */
        $fields = array(
            'order_branch_code' => array(
                'type' => 'VARCHAR',
                'constraint' => 10
            ),
            'order_manual_resi_pure' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
        );

        $this->dbforge->add_column('orders', $fields);
    }

    public function down() {

        /** Add columns at orders table */
        $this->dbforge->drop_column('orders', 'order_branch_code');
        $this->dbforge->drop_column('orders', 'order_manual_resi_pure');
        
    }

}
