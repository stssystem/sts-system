<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeBranches03_schema extends CI_Migration {

    public function up() {

        /** Add columns at branches table */
        $fields = array(
            'branch_regional_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('branches', $fields);
        
    }

    public function down() {

        /** Drop columns at branches table */
        $this->dbforge->drop_column('branches', 'branch_regional_id');
        
    }

}
