<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_UserProfile_schema extends CI_Migration {

    public function up() {

        //create table user
        $this->dbforge->add_field(array(
            'userprofile_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'userprofile_fullname' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'userprofile_address' => array(
                'type' => 'TEXT'
            ),
            'userprofile_photo' => array(
                'type' => 'VARCHAR',
                'constraint' => '200'
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'deleted_at' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        ));

        $this->dbforge->add_key('userprofile_id', TRUE);
        $this->dbforge->create_table('userprofile');
    }

    public function down() {
        //drop the table
        $this->dbforge->drop_table('userprofile');
    }

}
