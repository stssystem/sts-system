<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_DetOrders_schema extends CI_Migration {

    public function up() {

        /** Orders detail column definition */
        $this->dbforge->add_field(
                array(
                    'det_order_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'order_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'package_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'det_order_total' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'det_order_notes' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    )
                )
        );

        $this->dbforge->add_key('det_order_id', TRUE);
        $this->dbforge->create_table('det_order');
    }

    public function down() {

        /** Drop orders detail table */
        $this->dbforge->drop_table('det_order');
    }

}
