<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Districts_schema extends CI_Migration {

    public function up() {

        /** Costumer column definition */
        $this->dbforge->add_field(
                array(
                    'district_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'district_name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    ),
                    'city_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('district_id', TRUE);
        $this->dbforge->create_table('districts');
        
    }

    public function down() {

        /** Drop costumer table */
        $this->dbforge->drop_table('districts');
    }

}
