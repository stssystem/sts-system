<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_Villages_schema extends CI_Migration {

    public function up() {

        /** Costumer column definition */
        $this->dbforge->add_field(
                array(
                    'village_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'village_name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    ),
                    'district_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    )
                )
        );

        $this->dbforge->add_key('village_id', TRUE);
        $this->dbforge->create_table('villages');
        
    }

    public function down() {

        /** Drop costumer table */
        $this->dbforge->drop_table('villages');
    }

}
