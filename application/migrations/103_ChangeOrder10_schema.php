<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeOrder10_schema extends CI_Migration {

    public function up() {

        /** Add column regional_id at column package_edit_orders_extra */
        $fields = array(
            'order_origin_text' => array(
                'name' => 'order_origin_text',
                'type' => 'TEXT'
            ),
            'order_destination_text' => array(
                'name' => 'order_destination_text',
                'type' => 'TEXT'
            )
        );

        $this->dbforge->modify_column('orders', $fields);
    }

    public function down() {

        /** Drop regional_id column at package_edit_orders_extra table */
        /** Add column regional_id at column package_edit_orders_extra */
        $fields = array(
            'order_origin_text' => array(
                'name' => 'order_origin_text',
                'type' => 'INT',
                'constraint' => 11
            ),
            'order_destination_text' => array(
                'name' => 'order_destination_text',
                'type' => 'INT',
                'constraint' => 11
            )
        );

        $this->dbforge->modify_column('orders', $fields);
        
    }

}
