<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_CancelReport_schema extends CI_Migration {

    public function up() {

        /** Add columns at cancel report table */
        $this->dbforge->add_field(array(
            'cancel_report_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'note' => array(
                'type' => 'TEXT',
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'deleted_at' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        ));

        $this->dbforge->add_key('cancel_report_id', TRUE);
        $this->dbforge->create_table('cancel_report');
        
    }

    public function down() {

        /** Drop cancel report table */
        $this->dbforge->drop_table('cancel_report');
    }

}
