<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomers10_schema extends CI_Migration {

    public function up() {

        /** Add columns at packages table */
        $fields = array(
            'pic_phone' => array(
                'type' => 'VARCHAR',
                'constraint' => 30
            ),
        );

        $this->dbforge->add_column('customers', $fields);
    }

    public function down() {

        /** Add columns at customers table */
        $this->dbforge->drop_column('customers', 'pic_phone');

    }

}
