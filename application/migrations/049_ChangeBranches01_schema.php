<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeBranches01_schema extends CI_Migration {

    public function up() {

        /** Add columns at branches table */
        $fields = array(
            'branch_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '15',
            ),
            'branch_type' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
        );

        $this->dbforge->add_column('branches', $fields);
    }

    public function down() {

        /** Drop columns at branches table */
        $this->dbforge->drop_column('branches', 'branch_code');
        $this->dbforge->drop_column('branches', 'branch_type');
    }

}
