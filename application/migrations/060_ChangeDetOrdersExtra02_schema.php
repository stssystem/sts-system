<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeDetOrdersExtra02_schema extends CI_Migration {

    public function up() {

        /** Modify order_id column at det_order table */
        $fields = array(
            'order_id' => array(
                'name' => 'order_id',
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
        );
        
        $this->dbforge->modify_column('det_orders_extra', $fields);
        
    }

    public function down() {

        /** Rollback order_id column at det_order table */
        $fields = array(
            'order_id' => array(
                'name' => 'order_id',
                'type' => 'INT',
                'constraint' => 11
            ),
        );

        $this->dbforge->modify_column('det_orders_extra', $fields);
        
    }

}