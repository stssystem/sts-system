<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeUserType02_schema extends CI_Migration {

    public function up() {

        /** Add columns at det_order table */
        $fields = array(
            'branch_access' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );

        $this->dbforge->add_column('user_types', $fields);
        
    }

    public function down() {

        /** Drop columns at orders table */
        $this->dbforge->drop_column('user_types', 'branch_access');
        
    }

}