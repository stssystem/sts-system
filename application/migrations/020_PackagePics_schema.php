<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_PackagePics_schema extends CI_Migration {

    public function up() {

        /** Package pics detail column definition */
        $this->dbforge->add_field(
                array(
                    'package_pic_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'package_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
                    'package_pic' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '150'
                    )
                )
        );

        $this->dbforge->add_key('package_pic_id', TRUE);
        $this->dbforge->create_table('package_pics');
    }

    public function down() {

        /** Drop package pics detail table */
        $this->dbforge->drop_table('package_pics');
    }

}
