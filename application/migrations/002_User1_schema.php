<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_User1_schema extends CI_Migration {

    public function up() {

        //create table user
        $this->dbforge->add_field(array(
            'user_type_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'user_type_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '30'
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'deleted_at' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        ));
        $this->dbforge->add_key('user_type_id', TRUE);
        $this->dbforge->create_table('user_types');

        //add field user_type_id in table user
        $fields = array(
            'user_type_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        );
        $this->dbforge->add_column('user', $fields);
    }

    public function down() {
        //drop the table
        $this->dbforge->drop_table('user_types');
        //drop the field
        $this->dbforge->drop_column('user', 'user_type_id');
    }

}
