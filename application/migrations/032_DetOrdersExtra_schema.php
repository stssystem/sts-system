<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_DetOrdersExtra_schema extends CI_Migration {

    public function up() {

        /** List of columns on det_orders_extra table */
        $this->dbforge->add_field(array(
            'det_order_extra_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'det_order_extra_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 11
            ),
            'det_order_extra_total' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
        ));

        /** Grant det_orders_extra_id as primary key */
        $this->dbforge->add_key('det_order_extra_id', TRUE);

        /** Created det_order_extra table */
        $this->dbforge->create_table('det_orders_extra');
        
    }

    public function down() {

        /** Droped det_order_extra table */
        $this->dbforge->drop_table('det_orders_extra');
        
    }

}
