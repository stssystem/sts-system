<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomers03_schema extends CI_Migration {

    public function up() {

        /** Add columns at det_order table */
        $fields = array(
            /** Sum of det_order_sell_total */
            'customer_company_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'customer_company_website' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'customer_company_telp' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'customer_company_email' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'customer_company_address' => array(
                'type' => 'VARCHAR',
                'constraint' => '200'
            ),
        );
        $this->dbforge->add_column('customers', $fields);
    }

    public function down() {

        /** Drop columns at det_order table */
        $fields = array(
            'customer_company_name',
            'customer_company_website',
            'customer_company_telp',
            'customer_company_email',
            'customer_company_address'
        );
        foreach($fields as $field)
        {
            $this->dbforge->drop_column('customers', $field);
        }
    }

}
