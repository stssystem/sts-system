<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migration_ChangeCustomers11_schema extends CI_Migration {

    public function up() {

        /** Add columns at packages table */
        $fields = array(
            'pic_email' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
        );

        $this->dbforge->add_column('customers', $fields);
    }

    public function down() {

        /** Add columns at customers table */
        $this->dbforge->drop_column('customers', 'pic_email');

    }

}
