create view finance_report as SELECT `orders`.`order_id` AS `order_id`, `orders`.`order_status` AS `order_status`, `orders`.`order_date` AS `order_date`, `orders`.`order_sell_total` AS `order_sell_total`, `customers`.`customer_name` AS `customer_name`, `branches_origin`.`branch_name` as `branch_name_origin`, `branches_destination`.`branch_name` as `branch_name_destination`, SUM(`det_orders_extra`.`det_order_extra_total`) as `extra_total`
FROM `orders`
LEFT JOIN branches AS branches_origin ON `orders`.`order_origin` = `branches_origin`.`branch_id`
LEFT JOIN branches AS branches_destination ON `orders`.`order_origin` = `branches_destination`.`branch_id`
LEFT JOIN customers ON `customers`.`customer_id` = `orders`.`customer_id`
LEFT JOIN det_orders_extra ON `orders`.`order_id` = `det_orders_extra`.`order_id`
WHERE `orders`.`order_status` > '-1'
AND `orders`.`deleted_at` =0
GROUP BY orders.order_id
ORDER BY `orders`.`order_date` DESC

CREATE VIEW orders_view AS 
SELECT 
orders.order_id AS order_id,
orders.order_date AS order_date,
orders.order_status AS order_status,
customers.customer_name AS customer_name,
branches_origin.branch_name AS branch_name_origin,
branches_destination.branch_name AS branch_name_destination
FROM orders
LEFT JOIN customers ON orders.customer_id = customers.customer_id
LEFT JOIN branches AS branches_origin ON orders.order_origin = branches_origin.branch_id
LEFT JOIN branches AS branches_destination ON orders.order_destination = branches_destination.branch_id

CREATE VIEW packages_view AS 
SELECT 
det_order.order_id,
SUM(det_order.det_order_qty) AS total_koli, 
SUM(packages.package_weight) AS total_weight,
SUM(packages.package_size) AS total_size,
packages.package_status AS package_status
FROM det_order 
LEFT JOIN packages ON det_order.package_id = packages.package_id
GROUP BY det_order.order_id

-- SQL Tax Report :

-- Alternative 1 
SELECT 
branches.branch_name AS branch, 
FROM_UNIXTIME(orders.order_date, '%d %m %Y') AS order_date,
SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total - (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))))  ELSE orders.order_sell_total  END) as dpp,
SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))) ELSE (orders.order_sell_total * (1 / 100))  END)as ppn,
SUM(CASE orders.order_use_tax WHEN 1 THEN orders.order_sell_total  ELSE orders.order_sell_total + (orders.order_sell_total * (1 / 100)) END) as total
FROM orders
LEFT JOIN branches ON orders.order_origin = branches.branch_id
WHERE orders.order_status > -1 AND (orders.order_date >= 1470070800 AND  orders.order_date <=1470070800)
GROUP BY order_origin
ORDER BY branches.branch_name;

-- Alternative 2
SELECT branches.branch_name, orders.*
FROM branches
LEFT JOIN (
SELECT
FROM_UNIXTIME(orders.order_date, '%d %m %Y') AS order_date,
orders.order_origin AS order_origin,
SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total - (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))))  ELSE orders.order_sell_total  END) as dpp,
SUM(CASE orders.order_use_tax WHEN 1 THEN (orders.order_sell_total  - ( orders.order_sell_total * (100 / 101))) ELSE (orders.order_sell_total * (1 / 100))  END)as ppn,
SUM(CASE orders.order_use_tax WHEN 1 THEN orders.order_sell_total  ELSE orders.order_sell_total + (orders.order_sell_total * (1 / 100)) END) as total
FROM orders
LEFT JOIN branches ON orders.order_origin = branches.branch_id
WHERE orders.order_status > -1 AND (orders.order_date >= 1470070800 AND  orders.order_date <=1470070800)
) AS orders ON branches.branch_id = orders.order_origin
ORDER BY branches.branch_name;

